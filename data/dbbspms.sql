-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2017 at 05:42 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bspms_db_forupload`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblaccesscontrollist`
--

CREATE TABLE `tblaccesscontrollist` (
  `acc_id` int(11) NOT NULL,
  `acc_access_level` int(11) DEFAULT NULL,
  `acc_access_name` varchar(100) DEFAULT NULL,
  `acc_description` varchar(1000) DEFAULT NULL,
  `acc_isactive` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblaccesscontrollist`
--

INSERT INTO `tblaccesscontrollist` (`acc_id`, `acc_access_level`, `acc_access_name`, `acc_description`, `acc_isactive`) VALUES
(1, 1, 'libraries', 'Access to Library', 1),
(2, 2, 'libraries', 'Access to Library', 0),
(3, 1, 'user_accounts', 'Access to User Accounts', 1),
(4, 1, 'delete_scientist', 'Delete Scientist (Sys Admin)', 1),
(5, 3, 'limit_scientist', 'Limit view of Scientists', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblactions`
--

CREATE TABLE `tblactions` (
  `action_id` int(11) NOT NULL,
  `action_dateFrom` date DEFAULT NULL,
  `action_dateTo` date DEFAULT NULL,
  `action_secretariat` int(11) DEFAULT NULL,
  `action_remarks` varchar(1000) DEFAULT NULL,
  `action_isRemove` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbladdtlreq`
--

CREATE TABLE `tbladdtlreq` (
  `req_id` int(11) NOT NULL,
  `req_label` varchar(500) DEFAULT NULL,
  `req_isRemove` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladdtlreq`
--

INSERT INTO `tbladdtlreq` (`req_id`, `req_label`, `req_isRemove`) VALUES
(1, 'Terminal Report on previous visit', 0),
(2, 'Accomplished Impact Assessment Survey', 0),
(3, 'Other documents, pls. specify:', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblapplicationpackage`
--

CREATE TABLE `tblapplicationpackage` (
  `app_id` int(11) NOT NULL,
  `app_label` varchar(500) DEFAULT NULL,
  `app_isRemove` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblapplicationpackage`
--

INSERT INTO `tblapplicationpackage` (`app_id`, `app_label`, `app_isRemove`) VALUES
(1, 'Accomplished BSP Form #1 (Application Form)', 0),
(2, 'Endorsement letter from the host', 0),
(3, 'Terms of Reference (for Short Term) duly signed by the host (BSP Form #2)', 0),
(4, 'Quarterly Activity Plan (for Long Term) duly signed by the host (BSP Form #2)', 0),
(5, 'PRC Clearance (License)', 0),
(6, 'Detailed Curriculum Vitae', 0),
(7, 'Certification of Academic Achievement', 0),
(8, 'Photocopy of Valid ID (Passport)', 0),
(9, 'Draft Contract of Award', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblcalendar`
--

CREATE TABLE `tblcalendar` (
  `cal_id` int(11) NOT NULL,
  `cal_title` varchar(500) DEFAULT NULL,
  `cal_dateStart` date DEFAULT NULL,
  `cal_dateEnd` date DEFAULT NULL,
  `cal_timeStart` time DEFAULT NULL,
  `cal_timeEnd` time DEFAULT NULL,
  `cal_days` int(11) DEFAULT NULL,
  `cal_isAllday` int(11) NOT NULL DEFAULT '0',
  `cal_bgcolor` varchar(100) DEFAULT NULL,
  `cal_url` varchar(250) DEFAULT NULL,
  `cal_addedby` int(11) DEFAULT NULL,
  `cal_addedDate` datetime DEFAULT NULL,
  `cal_lastUpdatedBy` int(11) DEFAULT NULL,
  `cal_lastUpdatedDate` datetime DEFAULT NULL,
  `cal_isRemove` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcitizenship`
--

CREATE TABLE `tblcitizenship` (
  `cit_id` int(5) NOT NULL,
  `cit_type` int(1) NOT NULL,
  `cit_particulars` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcouncils`
--

CREATE TABLE `tblcouncils` (
  `cil_id` int(5) NOT NULL,
  `cil_code` varchar(10) NOT NULL,
  `cil_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcountries`
--

CREATE TABLE `tblcountries` (
  `countr_id` int(11) NOT NULL,
  `countr_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblcourses`
--

CREATE TABLE `tblcourses` (
  `cou_id` int(10) NOT NULL,
  `cou_code` varchar(20) NOT NULL,
  `cou_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbleducationallevels`
--

CREATE TABLE `tbleducationallevels` (
  `elev_id` int(5) NOT NULL,
  `elev_code` varchar(10) NOT NULL,
  `elev_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblexpertise`
--

CREATE TABLE `tblexpertise` (
  `exp_id` int(5) NOT NULL,
  `exp_code` varchar(10) NOT NULL,
  `exp_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblfields`
--

CREATE TABLE `tblfields` (
  `fld_id` int(10) NOT NULL,
  `fld_code` varchar(10) NOT NULL,
  `fld_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblglobalregions`
--

CREATE TABLE `tblglobalregions` (
  `glo_id` int(5) NOT NULL,
  `glo_continent` int(5) NOT NULL,
  `glo_country` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblinstitutions`
--

CREATE TABLE `tblinstitutions` (
  `ins_id` int(5) NOT NULL,
  `ins_itype_id` varchar(5) NOT NULL,
  `ins_loc_id` varchar(5) NOT NULL,
  `ins_code` varchar(10) NOT NULL,
  `ins_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblinstitutiontypes`
--

CREATE TABLE `tblinstitutiontypes` (
  `itype_id` int(5) NOT NULL,
  `itype_code` varchar(10) NOT NULL,
  `itype_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbllocalregions`
--

CREATE TABLE `tbllocalregions` (
  `loc_id` int(5) NOT NULL,
  `loc_region` int(11) NOT NULL,
  `loc_province` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbloutcomes`
--

CREATE TABLE `tbloutcomes` (
  `out_id` int(5) NOT NULL,
  `out_number` int(3) NOT NULL,
  `out_desc` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbloutputs`
--

CREATE TABLE `tbloutputs` (
  `out_id` int(5) NOT NULL,
  `out_act_id` int(5) NOT NULL,
  `out_code` varchar(10) NOT NULL,
  `out_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblparticipants`
--

CREATE TABLE `tblparticipants` (
  `par_id` int(5) NOT NULL,
  `par_code` varchar(10) NOT NULL,
  `par_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpriorityareas`
--

CREATE TABLE `tblpriorityareas` (
  `pri_id` int(5) NOT NULL,
  `pri_number` int(3) NOT NULL,
  `pri_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblprofessions`
--

CREATE TABLE `tblprofessions` (
  `prof_id` int(5) NOT NULL,
  `prof_code` varchar(10) NOT NULL,
  `prof_desc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblreports`
--

CREATE TABLE `tblreports` (
  `rep_id` int(5) NOT NULL,
  `rep_stat` char(1) NOT NULL DEFAULT '0',
  `rep_group` varchar(50) NOT NULL,
  `rep_desc` varchar(200) NOT NULL,
  `rep_usr_level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblreports`
--

INSERT INTO `tblreports` (`rep_id`, `rep_stat`, `rep_group`, `rep_desc`, `rep_usr_level`) VALUES
(1, '1', 'Group A: Profile Experts', 'Experts Profile', ''),
(2, '1', 'Group A: Profile Experts', 'Balik Scientist Program - Gender Data by Period', ''),
(3, '1', 'Group A: Profile Experts', 'List of Balik Scientists by Gender by Area of Expertise by Year', ''),
(4, '1', 'Group A: Profile Experts', 'Distribution of BSP Awardees According to Profession by Year', ''),
(5, '1', 'Group A: Profile Experts', 'List of BSP Awardees According to Profession by Year', ''),
(6, '1', 'Group A: Profile Experts', 'Distribution of BSP Awardees According to Professional License by Year', ''),
(7, '1', 'Group A: Profile Experts', 'List of BSP Awardees According to Professional License by Year', ''),
(8, '1', 'Group A: Profile Experts', 'Distribution of BSP Awardees According to Area of Expertise', ''),
(9, '1', 'Group A: Profile Experts', 'Distribution of BSP Awardees per Local Region', ''),
(10, '1', 'Group A: Profile Experts', 'Distribution of BSP Awardees per Global Region', ''),
(11, '1', 'Group B: Profile of Host Institutions', 'Distribution of BSP Awardee by Type of Host Institution', ''),
(12, '1', 'Group C: Program Accomplishments', 'List of New BSP Awardees by Year', ''),
(13, '1', 'Group C: Program Accomplishments', 'List of Continuing BSP Awardees and Engagements For Implementation by Year', ''),
(14, '1', 'Group C: Program Accomplishments', 'List of BSP Awardees with Completed Service Obligations (Total of Short Term + Long Term)', ''),
(15, '1', 'Group C: Program Accomplishments', 'List of Ongoing BSP Awardees by Year', ''),
(16, '1', 'Group C: Program Accomplishments', 'Summary Report of BSP', ''),
(17, '1', 'Group C: Program Accomplishments', 'List of Balik Scientist Awardees by Period', ''),
(18, '1', 'Group C: Program Accomplishments', 'List of Exit Report Presentations (ERPs) by Year', ''),
(19, '1', 'Group C: Program Accomplishments', 'List of Pending Exit Report Presentations (ERPs) by Year', ''),
(20, '1', 'Group D: Balik Scientist Accomplishments', 'Awardees and their Accomplishments', ''),
(21, '1', 'Group D: Balik Scientist Accomplishments', 'BSP Lectures/Seminars/Forum Conducted', ''),
(22, '1', 'Group D: Balik Scientist Accomplishments', 'BSP Trainings/Workshops/Demonstrations Conducted', ''),
(23, '1', 'Group D: Balik Scientist Accomplishments', 'Project Assisted by BSP', ''),
(24, '1', 'Group D: Balik Scientist Accomplishments', 'Publication by Balik Scientist Awardees to their BSP Service', ''),
(25, '1', 'Group D: Balik Scientist Accomplishments', 'Students Mentored by Balik Scientist Awardees', ''),
(26, '1', 'Group D: Balik Scientist Accomplishments', 'Curriculum/Course Development by Balik Scientist Awardee', ''),
(27, '1', 'Group D: Balik Scientist Accomplishments', 'Network and Linkages Formed by Balik Scientist Awardee with Other Professionals and Institutions', ''),
(28, '1', 'Group D: Balik Scientist Accomplishments', 'Research and Development', ''),
(29, '1', 'Group D: Balik Scientist Accomplishments', 'Other Accomplishmetns and Outputs', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblscieducations`
--

CREATE TABLE `tblscieducations` (
  `educ_id` int(10) NOT NULL,
  `educ_sci_id` int(10) NOT NULL,
  `educ_level_id` int(10) NOT NULL,
  `educ_school` varchar(100) NOT NULL,
  `educ_countryorigin_id` int(10) NOT NULL,
  `educ_course_id` int(10) NOT NULL,
  `educ_year_graduated` int(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsciemployment`
--

CREATE TABLE `tblsciemployment` (
  `emp_id` int(10) NOT NULL,
  `emp_sci_id` int(10) NOT NULL,
  `emp_company` varchar(100) NOT NULL,
  `emp_position` varchar(100) NOT NULL,
  `emp_datefrom` date NOT NULL,
  `emp_dateto` date DEFAULT NULL,
  `ispresent` int(11) NOT NULL DEFAULT '0',
  `emp_countryorigin_id` int(10) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblscientist`
--

CREATE TABLE `tblscientist` (
  `sci_id` int(5) NOT NULL,
  `sci_first_name` varchar(50) NOT NULL,
  `sci_middle_name` varchar(50) NOT NULL,
  `sci_middle_initial` varchar(2) DEFAULT NULL,
  `sci_last_name` varchar(50) NOT NULL,
  `sci_ext_name` varchar(5) NOT NULL,
  `sci_title` varchar(20) NOT NULL,
  `sci_birthdate` date NOT NULL,
  `sci_gender` varchar(6) NOT NULL,
  `sci_status` varchar(10) NOT NULL,
  `sci_expertise_id` varchar(20) NOT NULL,
  `sci_specialization_id` varchar(20) NOT NULL,
  `sci_profession_id` varchar(20) NOT NULL,
  `sci_license` varchar(50) NOT NULL,
  `sci_contact` varchar(200) NOT NULL,
  `sci_email` varchar(200) NOT NULL,
  `sci_postal_address` varchar(100) NOT NULL,
  `sci_picturename` varchar(100) DEFAULT NULL,
  `sci_isdeleted` int(11) NOT NULL DEFAULT '0',
  `isrepatriated` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsciservice`
--

CREATE TABLE `tblsciservice` (
  `srv_id` int(10) NOT NULL,
  `srv_sci_id` int(10) NOT NULL,
  `srv_classification` varchar(50) NOT NULL,
  `srv_approval_date` date NOT NULL,
  `srv_isyearonly` int(1) NOT NULL DEFAULT '0',
  `srv_approval_yr` varchar(4) DEFAULT NULL,
  `srv_change_sched` varchar(50) DEFAULT NULL,
  `srv_sched_approvaldate` date DEFAULT NULL,
  `srv_change_sched_extension` date DEFAULT NULL,
  `srv_change_sched_shortening` date DEFAULT NULL,
  `srv_change_sched_deferral` date DEFAULT NULL,
  `srv_glo_id` varchar(5) NOT NULL,
  `srv_cit_id` varchar(5) NOT NULL,
  `srv_typeofaward` varchar(20) NOT NULL,
  `srv_cil_id` varchar(5) NOT NULL COMMENT 'for tblcouncils',
  `srv_status` int(11) NOT NULL,
  `srv_year_completed` int(4) DEFAULT NULL,
  `srv_with_erp` tinyint(1) DEFAULT NULL,
  `srv_erp_reason` varchar(100) DEFAULT NULL,
  `srv_erp_date` date DEFAULT NULL,
  `srv_erp_venue` varchar(100) DEFAULT NULL,
  `srv_erp_participants` int(10) DEFAULT NULL,
  `srv_erp_attachments` varchar(100) DEFAULT NULL,
  `srv_year_repatriated` int(4) DEFAULT NULL,
  `srv_type_contract` varchar(25) NOT NULL,
  `srv_cont_startDate` date DEFAULT NULL COMMENT 'Start contract date for continuous contract type',
  `srv_cont_endDate` date DEFAULT NULL COMMENT 'End contract date for continuous contract type',
  `srv_con_id` int(10) DEFAULT NULL COMMENT 'for tblsrvcontractdates',
  `srv_approved_days` int(5) DEFAULT NULL,
  `srv_ins_id` varchar(20) NOT NULL,
  `srv_out_id` varchar(10) NOT NULL,
  `srv_pri_id` varchar(10) NOT NULL,
  `srv_isdeleted` int(11) NOT NULL DEFAULT '0',
  `srv_dateCreated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `srv_createdBy` int(11) DEFAULT NULL COMMENT 'current user',
  `srv_processed_by` int(11) DEFAULT NULL,
  `srv_processed_date` date DEFAULT NULL,
  `srv_processed_remarks` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblspecializations`
--

CREATE TABLE `tblspecializations` (
  `spe_id` int(5) NOT NULL,
  `spe_code` varchar(10) NOT NULL,
  `spe_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvcontractdates`
--

CREATE TABLE `tblsrvcontractdates` (
  `con_id` int(5) NOT NULL,
  `con_srv_id` int(10) NOT NULL,
  `con_iscontinuous` int(50) NOT NULL DEFAULT '0',
  `con_date_from` date NOT NULL,
  `con_date_to` date NOT NULL,
  `con_total_number` int(10) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvcurriculum`
--

CREATE TABLE `tblsrvcurriculum` (
  `cur_id` int(10) NOT NULL,
  `cur_srv_id` int(10) NOT NULL,
  `cur_field` int(11) NOT NULL,
  `cur_institution` int(10) NOT NULL,
  `cur_output` varchar(1000) NOT NULL,
  `cur_target` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvmentoring`
--

CREATE TABLE `tblsrvmentoring` (
  `men_id` int(10) NOT NULL,
  `men_srv_id` int(10) NOT NULL,
  `men_field` int(10) NOT NULL,
  `men_institution` int(10) NOT NULL,
  `men_course` varchar(100) NOT NULL,
  `men_notargetstudents` int(10) NOT NULL,
  `men_noactualstudents` int(10) NOT NULL,
  `men_output` varchar(1000) NOT NULL,
  `men_attachment` varchar(100) DEFAULT NULL,
  `men_target` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvnetworks`
--

CREATE TABLE `tblsrvnetworks` (
  `net_id` int(5) NOT NULL,
  `net_srv_id` int(5) NOT NULL,
  `net_field` int(10) NOT NULL,
  `net_output` varchar(1000) NOT NULL,
  `net_target` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvothers`
--

CREATE TABLE `tblsrvothers` (
  `oth_id` int(10) NOT NULL,
  `oth_srv_id` int(10) NOT NULL,
  `oth_field` int(10) NOT NULL,
  `oth_output` varchar(100) NOT NULL,
  `oth_target` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvpaper`
--

CREATE TABLE `tblsrvpaper` (
  `pap_id` int(10) NOT NULL,
  `pap_srv_id` int(10) NOT NULL,
  `pap_field` int(10) DEFAULT NULL,
  `pap_title` varchar(100) NOT NULL,
  `pap_name` varchar(100) NOT NULL,
  `pap_submission_date` date DEFAULT NULL,
  `pap_published_date` date DEFAULT NULL,
  `pap_nopages` int(10) DEFAULT NULL,
  `pap_authors` varchar(100) DEFAULT NULL,
  `pap_isbn` varchar(100) DEFAULT NULL,
  `pap_volume` varchar(100) DEFAULT NULL,
  `pap_issue` date DEFAULT NULL,
  `pap_edition` varchar(100) DEFAULT NULL,
  `pap_target` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvprojects`
--

CREATE TABLE `tblsrvprojects` (
  `prj_id` int(10) NOT NULL,
  `prj_srv_id` int(10) NOT NULL,
  `prj_field` int(10) NOT NULL,
  `prj_title` varchar(100) NOT NULL,
  `prj_objective` varchar(100) NOT NULL,
  `prj_agency` varchar(100) NOT NULL,
  `prj_status` varchar(100) NOT NULL,
  `prj_contributions` varchar(1000) NOT NULL,
  `prj_target` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvreports`
--

CREATE TABLE `tblsrvreports` (
  `srp_id` int(5) NOT NULL,
  `srp_srv_id` int(10) DEFAULT NULL,
  `srp_progress` varchar(2) DEFAULT NULL,
  `srp_progress_date` date DEFAULT NULL,
  `srp_terminal` varchar(2) DEFAULT NULL,
  `srp_terminal_date` date DEFAULT NULL,
  `srp_bspfeedback` varchar(2) DEFAULT NULL,
  `srp_bspfeedback_date` date DEFAULT NULL,
  `srp_feedback` varchar(2) DEFAULT NULL,
  `srp_feedback_date` date DEFAULT NULL,
  `srp_evaluation` varchar(2) DEFAULT NULL,
  `srp_evaluation_date` date DEFAULT NULL,
  `srp_implementation` varchar(2) DEFAULT NULL,
  `srp_implementation_date` date DEFAULT NULL,
  `srp_comments` varchar(1000) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvresearches`
--

CREATE TABLE `tblsrvresearches` (
  `res_id` int(10) NOT NULL,
  `res_srv_id` int(10) NOT NULL,
  `res_field` int(10) NOT NULL,
  `res_output` varchar(100) NOT NULL,
  `res_target` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvseminars`
--

CREATE TABLE `tblsrvseminars` (
  `sem_id` int(10) NOT NULL,
  `sem_srv_id` int(10) NOT NULL,
  `sem_field` int(10) NOT NULL,
  `sem_title` varchar(100) NOT NULL,
  `sem_date` date DEFAULT NULL,
  `sem_venue` varchar(100) DEFAULT NULL,
  `sem_participantstype` int(10) NOT NULL,
  `sem_targetparticipants` int(10) NOT NULL,
  `sem_actualparticipants` int(10) DEFAULT NULL,
  `sem_output` varchar(1000) NOT NULL,
  `sem_attachment` varchar(100) DEFAULT NULL,
  `sem_target` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvseminarsattachments`
--

CREATE TABLE `tblsrvseminarsattachments` (
  `attch_id` int(11) NOT NULL,
  `attch_srv_id` int(11) NOT NULL,
  `attch_sem_id` int(11) NOT NULL,
  `attch_filename` varchar(100) NOT NULL,
  `attch_filetype` varchar(10) NOT NULL,
  `attch_path` varchar(225) NOT NULL,
  `attch_uploadedby` int(11) NOT NULL,
  `attch_uploadeddate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvstatus`
--

CREATE TABLE `tblsrvstatus` (
  `srv_stat_id` int(11) NOT NULL,
  `srv_stat_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblsrvtrainings`
--

CREATE TABLE `tblsrvtrainings` (
  `tra_id` int(10) NOT NULL,
  `tra_srv_id` int(10) NOT NULL,
  `tra_field` int(10) NOT NULL,
  `tra_title` varchar(100) NOT NULL,
  `tra_date` date DEFAULT NULL,
  `tra_venue` varchar(100) DEFAULT NULL,
  `tra_participantstype` int(10) NOT NULL,
  `tra_targetparticipants` int(10) NOT NULL,
  `tra_actualparticipants` int(10) DEFAULT NULL,
  `tra_output` varchar(1000) NOT NULL,
  `tra_attachment` varchar(100) DEFAULT NULL,
  `tra_target` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `lastupdated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbltba`
--

CREATE TABLE `tbltba` (
  `tba_id` int(11) NOT NULL,
  `tba_sci_id` int(11) NOT NULL,
  `tba_host_institution` int(11) DEFAULT NULL,
  `tba_text` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbltitles`
--

CREATE TABLE `tbltitles` (
  `tit_id` int(5) NOT NULL,
  `tit_abbreviation` varchar(20) NOT NULL,
  `tit_desc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbltrackingsheet`
--

CREATE TABLE `tbltrackingsheet` (
  `ts_id` int(11) NOT NULL,
  `ts_calendaryr` int(11) DEFAULT NULL,
  `ts_scientistname` varchar(200) DEFAULT NULL,
  `ts_concernid` int(11) DEFAULT NULL,
  `ts_assignedSecretariat` int(11) DEFAULT NULL COMMENT 'secretariat id from tblusers',
  `ts_typeofaward` int(11) DEFAULT NULL,
  `ts_applicationpackage` varchar(100) DEFAULT NULL COMMENT 'id from tblapplicationpackage',
  `ts_addtionalreq` int(11) DEFAULT NULL COMMENT 'id from tbladdtlreq',
  `ts_hostistitution` int(11) DEFAULT NULL COMMENT 'id from tblhostinstitutions',
  `ts_proposed_duration_startdate` date DEFAULT NULL,
  `ts_proposed_duration_enddate` date DEFAULT NULL,
  `ts_addedby` int(11) DEFAULT NULL,
  `ts_addeddate` datetime DEFAULT NULL,
  `ts_lastupdatedby` int(11) DEFAULT NULL,
  `ts_lastupdateddate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbluniversities`
--

CREATE TABLE `tbluniversities` (
  `uni_id` int(11) NOT NULL,
  `uni_abbrv` varchar(50) DEFAULT NULL,
  `uni_name` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `usr_user_id` int(10) NOT NULL,
  `usr_fname` varchar(100) DEFAULT NULL,
  `usr_mname` varchar(2) DEFAULT NULL,
  `usr_lname` varchar(100) DEFAULT NULL,
  `usr_user_login` varchar(20) NOT NULL,
  `usr_user_passwd` varchar(250) NOT NULL,
  `usr_user_level` int(1) NOT NULL DEFAULT '0',
  `usr_council` int(11) DEFAULT NULL,
  `usr_isactive` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblaccesscontrollist`
--
ALTER TABLE `tblaccesscontrollist`
  ADD PRIMARY KEY (`acc_id`);

--
-- Indexes for table `tblactions`
--
ALTER TABLE `tblactions`
  ADD PRIMARY KEY (`action_id`);

--
-- Indexes for table `tbladdtlreq`
--
ALTER TABLE `tbladdtlreq`
  ADD PRIMARY KEY (`req_id`);

--
-- Indexes for table `tblapplicationpackage`
--
ALTER TABLE `tblapplicationpackage`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `tblcalendar`
--
ALTER TABLE `tblcalendar`
  ADD PRIMARY KEY (`cal_id`);

--
-- Indexes for table `tblcitizenship`
--
ALTER TABLE `tblcitizenship`
  ADD PRIMARY KEY (`cit_id`);

--
-- Indexes for table `tblcouncils`
--
ALTER TABLE `tblcouncils`
  ADD PRIMARY KEY (`cil_id`);

--
-- Indexes for table `tblcountries`
--
ALTER TABLE `tblcountries`
  ADD PRIMARY KEY (`countr_id`);

--
-- Indexes for table `tblcourses`
--
ALTER TABLE `tblcourses`
  ADD PRIMARY KEY (`cou_id`);

--
-- Indexes for table `tbleducationallevels`
--
ALTER TABLE `tbleducationallevels`
  ADD PRIMARY KEY (`elev_id`);

--
-- Indexes for table `tblexpertise`
--
ALTER TABLE `tblexpertise`
  ADD PRIMARY KEY (`exp_id`);

--
-- Indexes for table `tblfields`
--
ALTER TABLE `tblfields`
  ADD PRIMARY KEY (`fld_id`);

--
-- Indexes for table `tblglobalregions`
--
ALTER TABLE `tblglobalregions`
  ADD PRIMARY KEY (`glo_id`);

--
-- Indexes for table `tblinstitutions`
--
ALTER TABLE `tblinstitutions`
  ADD PRIMARY KEY (`ins_id`);

--
-- Indexes for table `tblinstitutiontypes`
--
ALTER TABLE `tblinstitutiontypes`
  ADD PRIMARY KEY (`itype_id`);

--
-- Indexes for table `tbllocalregions`
--
ALTER TABLE `tbllocalregions`
  ADD PRIMARY KEY (`loc_id`);

--
-- Indexes for table `tbloutcomes`
--
ALTER TABLE `tbloutcomes`
  ADD PRIMARY KEY (`out_id`);

--
-- Indexes for table `tbloutputs`
--
ALTER TABLE `tbloutputs`
  ADD PRIMARY KEY (`out_id`);

--
-- Indexes for table `tblparticipants`
--
ALTER TABLE `tblparticipants`
  ADD PRIMARY KEY (`par_id`);

--
-- Indexes for table `tblpriorityareas`
--
ALTER TABLE `tblpriorityareas`
  ADD PRIMARY KEY (`pri_id`);

--
-- Indexes for table `tblprofessions`
--
ALTER TABLE `tblprofessions`
  ADD PRIMARY KEY (`prof_id`);

--
-- Indexes for table `tblreports`
--
ALTER TABLE `tblreports`
  ADD PRIMARY KEY (`rep_id`);

--
-- Indexes for table `tblscieducations`
--
ALTER TABLE `tblscieducations`
  ADD PRIMARY KEY (`educ_id`);

--
-- Indexes for table `tblsciemployment`
--
ALTER TABLE `tblsciemployment`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `tblscientist`
--
ALTER TABLE `tblscientist`
  ADD PRIMARY KEY (`sci_id`);

--
-- Indexes for table `tblsciservice`
--
ALTER TABLE `tblsciservice`
  ADD PRIMARY KEY (`srv_id`);

--
-- Indexes for table `tblspecializations`
--
ALTER TABLE `tblspecializations`
  ADD PRIMARY KEY (`spe_id`);

--
-- Indexes for table `tblsrvcontractdates`
--
ALTER TABLE `tblsrvcontractdates`
  ADD PRIMARY KEY (`con_id`);

--
-- Indexes for table `tblsrvcurriculum`
--
ALTER TABLE `tblsrvcurriculum`
  ADD PRIMARY KEY (`cur_id`);

--
-- Indexes for table `tblsrvmentoring`
--
ALTER TABLE `tblsrvmentoring`
  ADD PRIMARY KEY (`men_id`);

--
-- Indexes for table `tblsrvnetworks`
--
ALTER TABLE `tblsrvnetworks`
  ADD PRIMARY KEY (`net_id`);

--
-- Indexes for table `tblsrvothers`
--
ALTER TABLE `tblsrvothers`
  ADD PRIMARY KEY (`oth_id`);

--
-- Indexes for table `tblsrvpaper`
--
ALTER TABLE `tblsrvpaper`
  ADD PRIMARY KEY (`pap_id`);

--
-- Indexes for table `tblsrvprojects`
--
ALTER TABLE `tblsrvprojects`
  ADD PRIMARY KEY (`prj_id`);

--
-- Indexes for table `tblsrvreports`
--
ALTER TABLE `tblsrvreports`
  ADD PRIMARY KEY (`srp_id`);

--
-- Indexes for table `tblsrvresearches`
--
ALTER TABLE `tblsrvresearches`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `tblsrvseminars`
--
ALTER TABLE `tblsrvseminars`
  ADD PRIMARY KEY (`sem_id`);

--
-- Indexes for table `tblsrvseminarsattachments`
--
ALTER TABLE `tblsrvseminarsattachments`
  ADD PRIMARY KEY (`attch_id`),
  ADD UNIQUE KEY `attch_id` (`attch_id`);

--
-- Indexes for table `tblsrvstatus`
--
ALTER TABLE `tblsrvstatus`
  ADD PRIMARY KEY (`srv_stat_id`);

--
-- Indexes for table `tblsrvtrainings`
--
ALTER TABLE `tblsrvtrainings`
  ADD PRIMARY KEY (`tra_id`);

--
-- Indexes for table `tbltba`
--
ALTER TABLE `tbltba`
  ADD PRIMARY KEY (`tba_id`);

--
-- Indexes for table `tbltitles`
--
ALTER TABLE `tbltitles`
  ADD PRIMARY KEY (`tit_id`);

--
-- Indexes for table `tbltrackingsheet`
--
ALTER TABLE `tbltrackingsheet`
  ADD PRIMARY KEY (`ts_id`);

--
-- Indexes for table `tbluniversities`
--
ALTER TABLE `tbluniversities`
  ADD PRIMARY KEY (`uni_id`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`usr_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblaccesscontrollist`
--
ALTER TABLE `tblaccesscontrollist`
  MODIFY `acc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tblactions`
--
ALTER TABLE `tblactions`
  MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbladdtlreq`
--
ALTER TABLE `tbladdtlreq`
  MODIFY `req_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblapplicationpackage`
--
ALTER TABLE `tblapplicationpackage`
  MODIFY `app_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tblcalendar`
--
ALTER TABLE `tblcalendar`
  MODIFY `cal_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblcitizenship`
--
ALTER TABLE `tblcitizenship`
  MODIFY `cit_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblcouncils`
--
ALTER TABLE `tblcouncils`
  MODIFY `cil_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblcountries`
--
ALTER TABLE `tblcountries`
  MODIFY `countr_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblcourses`
--
ALTER TABLE `tblcourses`
  MODIFY `cou_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbleducationallevels`
--
ALTER TABLE `tbleducationallevels`
  MODIFY `elev_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblexpertise`
--
ALTER TABLE `tblexpertise`
  MODIFY `exp_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblfields`
--
ALTER TABLE `tblfields`
  MODIFY `fld_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblglobalregions`
--
ALTER TABLE `tblglobalregions`
  MODIFY `glo_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblinstitutions`
--
ALTER TABLE `tblinstitutions`
  MODIFY `ins_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblinstitutiontypes`
--
ALTER TABLE `tblinstitutiontypes`
  MODIFY `itype_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbllocalregions`
--
ALTER TABLE `tbllocalregions`
  MODIFY `loc_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbloutcomes`
--
ALTER TABLE `tbloutcomes`
  MODIFY `out_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbloutputs`
--
ALTER TABLE `tbloutputs`
  MODIFY `out_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblparticipants`
--
ALTER TABLE `tblparticipants`
  MODIFY `par_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblpriorityareas`
--
ALTER TABLE `tblpriorityareas`
  MODIFY `pri_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblprofessions`
--
ALTER TABLE `tblprofessions`
  MODIFY `prof_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblreports`
--
ALTER TABLE `tblreports`
  MODIFY `rep_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tblscieducations`
--
ALTER TABLE `tblscieducations`
  MODIFY `educ_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsciemployment`
--
ALTER TABLE `tblsciemployment`
  MODIFY `emp_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblscientist`
--
ALTER TABLE `tblscientist`
  MODIFY `sci_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsciservice`
--
ALTER TABLE `tblsciservice`
  MODIFY `srv_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblspecializations`
--
ALTER TABLE `tblspecializations`
  MODIFY `spe_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvcontractdates`
--
ALTER TABLE `tblsrvcontractdates`
  MODIFY `con_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvcurriculum`
--
ALTER TABLE `tblsrvcurriculum`
  MODIFY `cur_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvmentoring`
--
ALTER TABLE `tblsrvmentoring`
  MODIFY `men_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvnetworks`
--
ALTER TABLE `tblsrvnetworks`
  MODIFY `net_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvothers`
--
ALTER TABLE `tblsrvothers`
  MODIFY `oth_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvpaper`
--
ALTER TABLE `tblsrvpaper`
  MODIFY `pap_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvprojects`
--
ALTER TABLE `tblsrvprojects`
  MODIFY `prj_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvreports`
--
ALTER TABLE `tblsrvreports`
  MODIFY `srp_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvresearches`
--
ALTER TABLE `tblsrvresearches`
  MODIFY `res_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvseminars`
--
ALTER TABLE `tblsrvseminars`
  MODIFY `sem_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvseminarsattachments`
--
ALTER TABLE `tblsrvseminarsattachments`
  MODIFY `attch_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvstatus`
--
ALTER TABLE `tblsrvstatus`
  MODIFY `srv_stat_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblsrvtrainings`
--
ALTER TABLE `tblsrvtrainings`
  MODIFY `tra_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbltba`
--
ALTER TABLE `tbltba`
  MODIFY `tba_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbltitles`
--
ALTER TABLE `tbltitles`
  MODIFY `tit_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbltrackingsheet`
--
ALTER TABLE `tbltrackingsheet`
  MODIFY `ts_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbluniversities`
--
ALTER TABLE `tbluniversities`
  MODIFY `uni_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `usr_user_id` int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
