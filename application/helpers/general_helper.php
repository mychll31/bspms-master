<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
if ( ! function_exists('checkSession'))
{	
	function checkSession()
	{
		$CI = & get_instance();
		$CI->load->database();
		if(!$CI->session->userdata('sessBoolLoggedIn'))
		{
			redirect(base_url('login'));	
		}	
	}	
}

if ( ! function_exists('getNumbering'))
{	
	function getNumbering($intNum)
	{
		switch($intNum)
		{
			case 1:
				return 'st';
				break;
			case 2:
				return 'nd';
				break;
			case 3:
				return 'rd';
			case $intNum>=4:
				return 'th';
				break;
		}
	}	
}

if ( ! function_exists('getClassification')) {
	function getClassification($class)
    {
        if($class==0)
            return 'First Time Applicant';
        else
            return $class.getNumbering($class).' Subsequent';
    }
}

if ( ! function_exists('getAwardtype')) {
	function getAwardtype($award)
    {
        if($award==0)
            return 'Short Term';
        else
            return 'Long Term';
    }
}

if ( ! function_exists('getContracttype')) {
	function getContracttype($contract)
    {
        if($contract==0)
            return 'Continous';
        else
            return 'Staggered / Non-continous';
    }
}

if ( ! function_exists('getByDesc')) {
	function getByDesc($id, $array, $fieldname) {
		foreach ($array as $key => $val) {
			if ($val[$fieldname] === $id)
				return $val;
		}
		return null;
	}
}

if ( ! function_exists('dd')) {
	function dd($array) {
		echo '<pre>';
		return die(print_r($array));
	}
}

if ( ! function_exists('pthis')) {
	function pthis($array) {
		echo '<pre>';
		return print_r($array);
		echo '<pre>';
	}
}

if ( ! function_exists('getApprovalDate')) {
	function getApprovalDate($approvaldate, $schedapprovaldate) {
		if($schedapprovaldate=='' or $schedapprovaldate=='0000-00-00' or $schedapprovaldate==NULL)
			return $approvaldate;
		else
			return $schedapprovaldate;
	}
}

if ( ! function_exists('getFullname')) {
	function getFullname($lname, $fname, $midname, $midini) {
		if($midini == '' and $midname == '')
			return ucfirst($lname.' '.$fname);
		else
			if($midini != '')
				return utf8_decode(ucfirst($lname).' '.strtoupper($midini.'.').' '.ucfirst($fname));
			else
				return utf8_decode(ucfirst($lname).' '.strtoupper($midname[0].'.').' '.ucfirst($fname));
	}
}

if ( ! function_exists('getYearDuration')) {
	function getYearDuration($txtYear1, $txtYear2) {
		return ($txtYear1 == $txtYear2) ? $txtYear1 : $txtYear1.' to '.$txtYear2;
	}
}


if ( ! function_exists('getCouncil')) {
	function getCouncil($isadmin, $pcaard, $pcieerd, $pchrd) {
		$councils = '';
		if($isadmin){
			if($pcaard and $pcieerd and !$pchrd)
				$councils = '(PCAARD, PCIEERD)';
			elseif($pcaard and $pchrd and !$pcieerd)
				$councils = '(PCAARD, PCHRD)';
			elseif($pcieerd and $pchrd and !$pcaard)
				$councils = '(PCIEERD, PCHRD)';
			else
				$councils = '';
			return $councils;
		}else{
			if($pcaard)
				return ' ( PCAARD )';
			if($pcieerd)
				return ' ( PCIEERD )';
			if($pchrd)
				return ' ( PCHRD )';
		}
	}
}

if ( ! function_exists('getDuration')) {
	function getDuration($sdate, $edate){
			// duration
			$date1 = date('d-M-Y', strtotime($sdate));
			$date2 = date('d-M-Y', strtotime($edate));

			$diff = abs(strtotime($date2) - strtotime($date1));
			$years = floor($diff / (365*60*60*24));
			$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

			return array(
				'days' => $days,
				'months' => $months,
				'years' => $years
				);
			// return array($days,$months,$years);
		}
}

if ( ! function_exists('getDuration_caption')) {
	function getDuration_caption($days, $months, $years){
		$dateDuration = ($years == 0 ? '' : ($years == 1 ? $years.' year ': $years.' years '));
		$dateDuration .= ($months == 0 ? '' : ($months == 1 ? $months.' month ': $months.' months '));
		$dateDuration .= ($days == 0 ? '' : ($days == 1 ? $days.' day ': $days.' days '));
			return $dateDuration;
		}
}

if ( ! function_exists('checkAccess')) {
	function checkAccess($accesslvl, $accessname){
			$CI =& get_instance();
        	$CI->load->database();

			$CI->db->where("acc_access_level", $accesslvl);
			$CI->db->where("acc_access_name", $accessname);
			$CI->db->where("acc_isactive", 1);
			$query = $CI->db->get('tblaccesscontrollist');
			return $query->num_rows();
		}
}

if ( ! function_exists('getSecretariatName')) {
	function getSecretariatName($usr_fname, $usr_mname, $usr_lname){
			$usr_lname = $usr_lname!='' ? $usr_lname.', ' : '';
			$usr_mname = $usr_mname!='' ? $usr_mname.' ' : '';
			$usr_fname = $usr_fname!='' ? $usr_fname : '';

			return utf8_decode($usr_lname.$usr_mname.$usr_fname);
		}
}

if ( ! function_exists('getExpertiseName')) {
	function getExpertiseName($strExpert){
			$CI =& get_instance();
        	$CI->load->database();

			$query = $CI->db->get('tblexpertise');
			$arrExpertise = $query->result_array();
			$res = '';
			foreach (explode('|', $strExpert) as $key => $expert) {
				$key = array_search($expert, array_column($arrExpertise, 'exp_id'));
				$res.='<li>'.$arrExpertise[$key]['exp_desc'].'</li>';
			}
			return $res;
		}
}

if ( ! function_exists('getExpertiseNameToArray')) {
	function getExpertiseNameToArray($strExpert){
			$CI =& get_instance();
        	$CI->load->database();

			$query = $CI->db->get('tblexpertise');
			$arrExpertise = $query->result_array();
			$res = array();
			foreach (explode('|', $strExpert) as $key => $expert) {
				if($expert != ''){
					$key = array_search($expert, array_column($arrExpertise, 'exp_id'));
					array_push($res, ucwords($arrExpertise[$key]['exp_desc']));
				}
			}
			return $res;
		}
}

if ( ! function_exists('cleanHtml')) {
	function cleanHtml($strtba){
			$tags = array('<br>','</span>', '</blockquote>', '<!--v-html-->');
			foreach($tags as $tag):
				$strtba = str_replace($tag, '', $strtba);
			endforeach;
			$removeTag = array('<span', '<blockquote');
			foreach($removeTag as $tag):
				if(strpos($strtba,$tag) !== false){
					for ($i=0; $i < count(explode($tag, $strtba)); $i++) { 
						$str = strlen($strtba);
						$start = strpos($strtba, $tag);
						$end = strpos(substr($strtba, $start), '>');
						$toremove = substr($strtba, $start,$end+1);
						$strtba = str_replace($toremove, '', $strtba);
					}
				}
			endforeach;
			return $strtba;
	}
}

if ( ! function_exists('getHostInstitution')) {
	function getHostInstitution($instid){
			$CI =& get_instance();
        	$CI->load->database();

        	$CI->db->where("ins_id", $instid);
			$query = $CI->db->get('tblinstitutions');
			return $query->result_array();
		}
}

if ( ! function_exists('getSqlAccess')) {
	function getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd) {
		$arrAccess = array();
		$sqlAccess = '';
		if($padmin)
			array_push($arrAccess, "tblusers.usr_council is null");
		if($pcaard)
			array_push($arrAccess, "tblusers.usr_council = 1");
		if($pchrd)
			array_push($arrAccess, "tblusers.usr_council = 2");
		if($pcieerd)
			array_push($arrAccess, "tblusers.usr_council = 3");

		if(count($arrAccess) > 1)
			$sqlAccess = ' AND ('.implode(' OR ', $arrAccess).')';
		if(count($arrAccess) == 1)
			$sqlAccess = ' AND ('.$arrAccess[0].')';

		return $sqlAccess;
	}
}

if ( ! function_exists('checkUserKey')) {
	function checkUserKey($userid){

			$tbldata = 0;
			$CI =& get_instance();
        	$CI->load->database();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblscieducations');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsciemployment');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblscientist');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsciservice');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvcurriculum');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvmentoring');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvnetworks');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvothers');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvpaper');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvprojects');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvreports');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvresearches');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvseminars');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("created_by", $userid);
			$CI->db->or_where("lastupdated_by", $userid);
			$query = $CI->db->get('tblsrvtrainings');
			$tbldata = $tbldata + $query->num_rows();

			$CI->db->where("attch_uploadedby", $userid);
			$query = $CI->db->get('tblsrvseminarsattachments');
			$tbldata = $tbldata + $query->num_rows();

			return $tbldata;
		}
}

if ( ! function_exists('getUserById')) {
	function getUserById($userid){
			$CI =& get_instance();
        	$CI->load->database();

        	$CI->db->where("usr_user_id", $userid);
			$query = $CI->db->get('tblusers');
			$rs = $query->result_array();
			return $rs[0];
		}
}

if ( ! function_exists('getReportName')) {
	function getReportName($reportid){
			$CI =& get_instance();
        	$CI->load->database();

			$CI->db->where('rep_id', $reportid);
			$query = $CI->db->get('tblreports');
			$res = $query->result_array();
			return $res[0]['rep_desc'];
		}
}

?>