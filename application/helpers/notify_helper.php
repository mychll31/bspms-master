<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 //ini_set('display_errors','On');

if ( ! function_exists('notifyUser'))
{
    function notifyUser($strMessage, $notifyType)
    {
        $CI =& get_instance();
		$strMsg = '<div class="container">
								<div class="col-xs-4">&nbsp;</div>
								<div class="col-xs-4">';
		switch($notifyType)
		{
			case 'success':
				$strMsg .= '<div class="alert alert-success alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Success!</strong> '.$strMessage.'
							</div>';
			break;
			case 'info':
				$strMsg .= '<div class="alert alert-info alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								'.$strMessage.'
							</div>';
			break;
			case 'warning':
				$strMsg .= '<div class="alert alert-warning alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>warning!</strong> '.$strMessage.'
							</div>';
			break;
			case 'danger': 
				$strMsg .= '<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
								<strong>Error!</strong> '.$strMessage.'
							</div>';							
			break;	
			
		}
		$strMsg .= '</div><div class="col-xs-4">&nbsp;</div>
							</div>';
        return $strMsg;
    }
}



/* End of file script_helper.php */
/* Location: ./application/helpers/script_helper.php */