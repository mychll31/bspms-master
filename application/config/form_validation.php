<?php 
   $config = array(
      'addProfile' => array(
         array(
            'field' => 'txtFirstName', 
            'label' => 'Firstname', 
            'rules' => 'required|alpha'
            )
         ),
      'addEducation' => array(
         array(
            'field' => 'txtYearGraduated', 
            'label' => 'Year Graduated', 
            'rules' => 'required|numeric|less_than['.date("Y").']',
            'rules' => 'required|numeric|greater_than[1906]',
            )
         ),
      'addEmployment' => array(
         array(
            'field' => 'txtCompany', 
            'label' => 'Company', 
            'rules' => 'required|alpha'
            ),
         ),
      );

 ?>