<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Citizenship extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_citizenship'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('citizenship_model');		
	}

	public function index()
	{
		$citizenId = $this->uri->segment(3);
		$citizentype = array(
							''  => 'Choose Option',
							'1' => 'Filipino',
							'2'   => 'Foreign',
							'3'   => 'Dual Citizen'
							);
		$citizenships = $this->citizenship_model->getAll();
		foreach ($citizenships as $key=>$cit) {
			$citizenships[$key]['cit_type_name'] = $citizentype[$cit['cit_type']];
		}
		$this->arrTemplateData['arrCitizenships'] = $citizenships;
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'citizenship/citizenship_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrCitizenshipData = array(
									'cit_type'			=> $this->input->post('txtCode'),
									'cit_particulars'	=> ucwords($this->input->post('txtDesc'))
									);
		$resultAddCitizenship = $this->citizenship_model->addCitizenship($arrCitizenshipData);
		if($resultAddCitizenship) {
			$this->session->set_flashdata('Notification', 'Citizenship Saved Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('citizenship');
	}

	public function edit()
	{
		$arrCitizenshipData = array(
									'cit_type'			=>  $this->input->post('txtCode'),
									'cit_particulars'	=>  ucwords($this->input->post('txtDesc'))
									);
		$resultEditCitizenship = $this->citizenship_model->editCitizenship($this->input->post('txtSpeId'), $arrCitizenshipData);		
		if($resultEditCitizenship)
		{
			$this->session->set_flashdata('Notification', 'Citizenship Updated Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('citizenship');
	}
	
	public function delete()
	{
		$citizenshipId = $this->uri->segment(3);
		
		$deleteResult = $this->citizenship_model->deleteCitizenship($this->input->post('txtTempId'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Citizenship Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('citizenship');
	}

	//fetchdesc
	public function fetchDesc()
	{
		$citizenshipId = $this->uri->segment(3);
		$citizenships = $this->citizenship_model->getCitizenCode($citizenshipId);
		$arr_citizenships = array();
		foreach($citizenships as $citizenship){
			array_push($arr_citizenships, $citizenship['cit_particulars']);
		}
		echo json_encode($arr_citizenships);
	}

	public function fetchCitizen()
	{
		$citizentype = array(
							''  => 'Choose Option',
							'1' => 'Filipino',
							'2'   => 'Foreign',
							'3'   => 'Dual Citizen'
							);
		$citizenships = $this->citizenship_model->getAll();
		foreach ($citizenships as $key=>$cit) {
			$citizenships[$key]['cit_type_name'] = $citizentype[$cit['cit_type']];
		}
		echo json_encode($citizenships);
	}

	public function fetchvalidCit()
	{
		$validcit = '';
		foreach ($this->citizenship_model->getValidCit() as $cit) {
			$strcit = str_replace(',','',$cit['valcit']);
			$strcit = str_replace(' ','',$strcit);
			$validcit.=strtolower($strcit).'|';
		}
		echo json_encode($validcit);
	}

	public function getCitizen($cit_id)
	{
		$citizenships = $this->citizenship_model->getCitizenship($cit_id);
		echo json_encode($citizenships);
	}

	
}