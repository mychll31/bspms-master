<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Globalregions extends CI_Controller { 

	public function __construct()   
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model('globalregions_model');
	}

	public function index()
	{
		$globalregionsId = $this->uri->segment(3);
		$continents = array(
					''  => 'Choose Option',
    				'1' => 'Africa',
    				'2' => 'Asia',
    				'3' => 'Australia',
    				'4' => 'Europe',
    				'5' => 'Middle East',
    				'6' => 'North America',
    				'7' => 'Oceania',
    				'8' => 'South America'
    				);
		$continentList = $this->globalregions_model->getAll();
		foreach ($continentList as $key=>$cont) {
			$continentList[$key]['glo_cont_name'] = $continents[$cont['glo_continent']];
		}
		$this->arrTemplateData['arrGlobalregions'] = $continentList;
   		$this->arrTemplateData['globalregionsEdit'] = $this->globalregions_model->getGlobalregions($globalregionsId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'globalregions/globalregions_view', $this->arrTemplateData);
 	
	}
	
	public function add() 
	{
			$arrGlobalregionsData = array( 
										'glo_continent' =>  $this->input->post('cmbContinent'),
										'glo_country'	=>  $this->input->post('txtCountry')										
										 );
			
			$resultAddGlobalregions= $this->globalregions_model->addGlobalregions($arrGlobalregionsData);		
			if($resultAddGlobalregions)
			{
				$this->session->set_flashdata('Notification', 'Global Region Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('globalregions');
	} 
	
	
	
	
	public function edit()
	{
			$arrGlobalregionsData = array( 
									 	'glo_continent'	=>  $this->input->post('cmbContinent'),
										'glo_country'	=>  $this->input->post('txtCountry')	 									
										 );
			$resultEditGlobalregions	= $this->globalregions_model->editGlobalregions($this->input->post('txtGloId'), $arrGlobalregionsData);		
			if($resultEditGlobalregions)
			{
				$this->session->set_flashdata('Notification', 'Global Region Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('globalregions');
	}
	 
	public function delete()
	{
		
		$deleteResult = $this->globalregions_model->deleteGlobalregions($this->input->post('txtdelglobal'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Global Region Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('globalregions');
		
	}

	public function fetchdesc()
	{
		$continents = array(
					''  => 'Choose Option',
    				'1' => 'Africa',
    				'2' => 'Asia',
    				'3' => 'Australia',
    				'4' => 'Europe',
    				'5' => 'Middle East',
    				'6' => 'North America',
    				'7' => 'Oceania',
    				'8' => 'South America'
    				);
		$continentList = $this->globalregions_model->getAll();
		foreach ($continentList as $key=>$cont) {
			$continentList[$key]['glo_cont_name'] = $continents[$cont['glo_continent']];
		}
		echo json_encode($continentList);
	}
	
	public function getGlobals($glo_id)
	{
		echo json_encode($this->globalregions_model->getGlobalregions($glo_id));
	}

	public function fetchvalidglobal()
	{
		$validglobal = '';
		foreach ($this->globalregions_model->getValidGlobal() as $global) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validglobal.=strtolower($global['valglobal']).'|';
		}
		echo json_encode($validglobal);
	}

}
