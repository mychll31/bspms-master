<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Outcome extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('outcome_model');		
	}

	public function index()
	{
		$outcomeId = $this->uri->segment(3);
		$this->arrTemplateData['arrOutcome'] = $this->outcome_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['outcomeEdit'] = $this->outcome_model->getOutcome($outcomeId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'outcome/outcome_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrOutcomeData = array(
								'out_number'	=>  $this->input->post('txtCode'),
								'out_desc'		=>  $this->input->post('txtDesc')						
								);

		$resultAddOutcome = $this->outcome_model->addOutcome($arrOutcomeData);
		if($resultAddOutcome) {
			$this->session->set_flashdata('Notification', 'Outcomes Saved Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('outcome');
	}
	
	public function edit()
	{
		$arrOutcomeData = array(
								'out_number' 	=>  $this->input->post('txtCode'),
								'out_desc'		=>  $this->input->post('txtDesc')
								);

		$resultEditOutcome	= $this->outcome_model->editOutcome($this->input->post('txtSpeId'), $arrOutcomeData);
		if($resultEditOutcome) {
			$this->session->set_flashdata('Notification', 'Outcome Updated Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('outcome');		
	}
	
	public function delete()
	{
		$outcomeId = $this->uri->segment(3); 
		
		$deleteResult = $this->outcome_model->deleteOutcome($this->input->post('txtTempId'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Outcome Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('outcome'); 
		
	}
	
	public function fetchDesc()
	{
		echo json_encode($this->outcome_model->getAll());
	}
	
	public function getOutcome($out_id)
	{
		$citizenships = $this->outcome_model->getOutcome($out_id);
		echo json_encode($citizenships);
	}

	public function fetchvalidout()
	{
		$validout = '';
		foreach ($this->outcome_model->getValidOut() as $out) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validout.=strtolower($out['valout']).'|';
		}
		echo json_encode($validout);
	}
	
}