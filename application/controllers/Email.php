<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Email extends CI_Controller { 

	public function __construct()
	{
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model(array('Mancom_model', 'Calendar_model'));
	}

	public function index()
	{
		$members = $this->Mancom_model->getAll();
		$events = $this->Calendar_model->getAllEventsByMonth(date('Y-m'));

		$this->load->library('email'); 
		$config = array(
			'protocol' => 'smtp',
		    'smtp_host' => 'liham.icto.dost.gov.ph',
		    'smtp_port' => 587,
		    'smtp_user' => 'bspms-noreply@dost.gov.ph',
		    'smtp_pass' => 'dost123',
		    'mailtype'  => 'html', 
		    'charset'   => 'iso-8859-1'
		);
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");

		foreach($events as $event):
			foreach($members as $member):
				$htmlBody = 'Dear '.$member['tit_abbreviation'].' '.$member['man_lastname']
							.',<br><br>It is indeed a pleasure to inform you that you are invited to our event entitled <b>'.$event['cal_title']
							.'</b>. The event will be organized on <b>'.date('F d, Y', strtotime($event['cal_dateStart']))
							.'</b> at <b>'.date('h:i A', strtotime($event['cal_dateStart']))
							.'</b>, the event located at <b>'.$event['cal_location'].'</b>.<br>'.$event['cal_remarks']
							.'<br><br>Sincerely yours,<br><b>Balik Scientist Program</b><br><br><hr><small>This is an automatically generated email, please do not reply.</small>';

				$this->email->from('mmalcorin@dost.gov.ph', 'Balik Scientist Program');
				$this->email->to($member['man_email']);
				$this->email->subject($event['cal_title']);
				$this->email->message($htmlBody); 

	         	$this->email->send();
	             echo $this->email->print_debugger();
	         endforeach;
         endforeach;

	}

}
