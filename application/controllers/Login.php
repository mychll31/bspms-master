<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		
		$this->load->model('login_model');	
		$this->load->library('encrypt');		
	}
	
	public function index()
	{
		$strUsername = $this->input->post('txtUserName'); 
		$strPassword = $this->input->post('txtPassword');
		$strPassword2  = $this->encrypt->encode($strPassword);
		$strPassword2  = $this->encrypt->decode($strPassword2);
		$blnRemember = '';

		if(!$strUsername OR !$strPassword)
		{
			$this->load->view('login/login_view'); 
		}
		else
		{
			
			$authenticateAccount = $this->login_model->authenticate($strUsername,$strPassword);
			if(count($authenticateAccount)>0)
			{
				$pssword = $authenticateAccount[0]['usr_user_passwd'];
				$pssword = $this->encrypt->decode($pssword);
			 	
				if($authenticateAccount[0]['usr_isactive']==1){
					if ($strPassword == $pssword){
						$sessData = array(
						 'sessUserId'  			=> $authenticateAccount[0]['usr_user_id'],
						 'sessAccessLevel'  	=> $authenticateAccount[0]['usr_user_level'],
						 'sessUserName'  		=> $authenticateAccount[0]['usr_user_login'],
						 'sessUserPassword' 	=> $authenticateAccount[0]['usr_user_passwd'],
						 'sessName'  			=> $authenticateAccount[0]['usr_user'],
						 'sessCouncil'  		=> $authenticateAccount[0]['cil_desc'],
						 'sessCouncilCode'  	=> $authenticateAccount[0]['cil_code'],
						 'sessCouncilid'	  	=> $authenticateAccount[0]['usr_council'],
						 'sessBoolLoggedIn' 	=> TRUE,
						 'sessFname'			=> ucfirst($authenticateAccount[0]['usr_fname']),
						 'sessMname' 			=> strtoupper($authenticateAccount[0]['usr_mname']),
						 'sessLname' 			=> ucfirst($authenticateAccount[0]['usr_lname']),
						);
						
						$this->session->set_userdata($sessData);
						redirect('scientists');
					}else{
						$this->load->view('login/login_view', array('uname'=>$strUsername, 'error'=>'Invalid username/password.'));
					}
				}else{
					$this->load->view('login/login_view', array('uname'=>$strUsername, 'error'=>'Account is invalid. Please contact Adminstrator.'));
				}
				
			}else{
				$this->load->view('login/login_view', array('uname'=>$strUsername, 'error'=>'Invalid username/password.'));
			}
		}
	}
	
	public function code()
	{
		$myPassword ="dost";  
		echo "ENCRYPT: " .$this->encrypt->encode('dost');
		$myPassword = $this->encrypt->encode($myPassword);
		echo "<br/>DECRYPT: " .$this->encrypt->decode($myPassword);	
			 echo "<br/>ENCRYPT: " .$this->encrypt->encode($myPassword);
		echo "<br/>" .hash('sha256', 'dost' , SALT) ;
		
			
	}
	
}

?>