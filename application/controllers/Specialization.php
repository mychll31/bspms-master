<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Specialization extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('specialization_model');		
	}

	public function index()
	{
		$specializationId = $this->uri->segment(3);
		$this->arrTemplateData['arrSpecialization'] = $this->specialization_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['specializationEdit'] = $this->specialization_model->getSpecialization($specializationId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'specialization/specialization_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
			$arrSpecializationData = array( 
										// 'spe_code'	=>  $this->input->post('txtCode'),
										'spe_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultAddSpecialization = $this->specialization_model->addSpecialization($arrSpecializationData);		
			if($resultAddSpecialization)
			{
				$this->session->set_flashdata('Notification', 'Specializations Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('specialization');
	}
	
	
	
	
	public function edit()
	{
			$arrSpecializationData = array( 
										// 'spe_code'	=>  $this->input->post('txtCode'),
										'spe_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultEditSpecialization	= $this->specialization_model->editSpecialization($this->input->post('txtSpefId'), $arrSpecializationData);		
			if($resultEditSpecialization)
			{
				$this->session->set_flashdata('Notification', 'Specialization Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('specialization');
	}
	
	public function delete()
	{
		$specializationId = $this->uri->segment(3);  
		
		$deleteResult = $this->specialization_model->deleteSpecialization($this->input->post('txtdelspe'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Specialization Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('specialization');
		
	}

	public function fetchdesc()
	{
		echo json_encode($this->specialization_model->getAll());
	}

	public function fetchvalidSpe()
	{
		$validspe = '';
		foreach ($this->specialization_model->getValidSpe() as $spe) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validspe.=strtolower($spe['valspe']).'|';
		}
		echo json_encode($validspe);
	}

	public function getSpe($spe_id)
	{
		echo json_encode($this->specialization_model->getSpecialization($spe_id));
	}
	
}