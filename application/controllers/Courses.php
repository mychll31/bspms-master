<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courses extends CI_Controller {

	public function __construct()   
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('courses_model');	 
	}

	public function index()
	{
		$courseId = $this->uri->segment(3);
		$this->arrTemplateData['arrCourse'] = $this->courses_model->getAll(); // calling Post model method getPosts()
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'courses/courses_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrCourseData = array(
								'cou_code'	=> ltrim($this->input->post('txtCode')),
								'cou_desc'	=> ltrim($this->input->post('txtDesc'))
								);

		$resultAddCourse = $this->courses_model->addCourse($arrCourseData);
		if($resultAddCourse) {
			$this->session->set_flashdata('Notification', 'Course Saved Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('courses');
	}
	
	
	
	
	public function edit()
	{
		$arrCourseData = array(
								'cou_code'	=>  ltrim($this->input->post('txtCode')),
								'cou_desc'	=>  ltrim($this->input->post('txtDesc'))
								);
		$resultEditCourse = $this->courses_model->editCourse($this->input->post('txtSpeId'), $arrCourseData);
		if($resultEditCourse) {
			$this->session->set_flashdata('Notification', 'Course Updated Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('courses');
	}
	
	public function delete()
	{
		$courseId = $this->uri->segment(3);  
		
		$deleteResult = $this->courses_model->deleteCourse($this->input->post('txtempid'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Course Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success'); 
		}
		redirect('courses');
		
	}

	//fetchdesc
	public function fetchdesc()
	{
		// $couid = $this->uri->segment(3);
		// $courses = $this->courses_model->getCourdesc($couid);
		// $arr_course = array();
		// foreach($courses as $course){
		// 	array_push($arr_course, $course['cou_desc']);
		// }
		echo json_encode($this->courses_model->getAll());
	}

	public function getCourses($couid)
	{
		echo json_encode($this->courses_model->getCourse($couid));
	}

	public function fetchvalidCourse()
	{
		$validcourse = '';
		foreach ($this->courses_model->getValidCourse() as $course) {
			// $strcour = str_replace(',','',$course['valcou']);
			// $strcour = str_replace(' ','',$strcour);
			$validcourse.=strtolower($course['valcou']).'|';
		}
		echo json_encode($validcourse);
	}
	
}