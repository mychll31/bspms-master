<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model('country_model');		
	}

	public function index()
	{
		$countryId = $this->uri->segment(3);
		$this->arrTemplateData['arrCountry'] = $this->country_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['countryEdit'] = $this->country_model->getCountry($countryId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'global_regions/country_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrCountryData = array(
								'glo_continent' =>  $this->input->post('cmbContinent'),
								'glo_country'	=>  $this->input->post('txtCountry')
								);
		$resultAddCountry = $this->country_model->addCountry($arrCountryData);		
			if($resultAddCountry)
			{
				$this->session->set_flashdata('Notification', 'Countrys Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('country');
	}
	
	
	
	
	public function edit()
	{
		if($this->input->post('btnSubmitCountry')=="edit")
		{
			$arrCountryData = array( 
										'glo_continent'	=>  $this->input->post('cmbContinent'),
										'glo_country'	=>  $this->input->post('txtCountry')	 									
										 );
			
			$resultEditCountry	= $this->country_model->editCountry($this->input->post('txtGloId'), $arrCountryData);		
			if($resultEditCountry)
			{
				$this->session->set_flashdata('Notification', 'Country Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('country');
		}
		else
		{
			redirect('country');	
		}
	}
	
	public function delete()
	{
		$countryId = $this->uri->segment(3);  
		
		$deleteResult = $this->country_model->deleteCountry($countryId);
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Country Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('country');
		
	}


	// Api country

	public function insertCountry()
	{
		$arrCountryData = array( 
								'glo_continent'	=>  $this->input->post('glo_continent'),
								'glo_country'	=>  $this->input->post('glo_country')
								);
		$resultAddCountry = $this->country_model->addCountry($arrCountryData);
	}

	public function fetchCountry()
	{
		echo json_encode($this->country_model->getAll());
	}

	public function fetchCountryList()
	{
		$countyList = array();
		foreach ($this->country_model->getAll() as $country) {
			array_push($countyList, $country['glo_country']);
		}
		echo json_encode($countyList);
	}

	public function fetchCountriesList()
	{
		$countriesList = array();
		foreach ($this->country_model->getCountriesList() as $country) {
			array_push($countriesList, $country['countr_name']);
		}
		echo json_encode($countriesList);
	}

	public function fetchCountryContinent()
	{
		$countCon = array();
		foreach ($this->country_model->getCountCon() as $country) {
			array_push($countCon, $country['councon']);
		}
		echo json_encode($countCon);
	}
	
}