<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Titles extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model('title_model');		
	}

	public function index()
	{
		$titleId = $this->uri->segment(3);
		$this->arrTemplateData['arrTitle'] = $this->title_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['titleEdit'] = $this->title_model->getTitle($titleId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'titles/titles_view', $this->arrTemplateData);
 	
	}
	
	public function add() 
	{
			$arrTitleData = array( 
										'tit_abbreviation'	=>  $this->input->post('txtCode'), 
										'tit_desc'			=>	$this->input->post('txtDesc')										
										 );
			
			$resultAddTitle = $this->title_model->addTitle($arrTitleData);		
			if($resultAddTitle)
			{
				$this->session->set_flashdata('Notification', 'Title Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('titles'); 
	}
	
	
	
	
	public function edit()
	{
			$arrTitleData = array( 
										'tit_abbreviation'	=>  $this->input->post('txtCode'),
										'tit_desc'			=>  $this->input->post('txtDesc')										
										 );
			
			$resultEditTitle	= $this->title_model->editTitle($this->input->post('txtTitleId'), $arrTitleData);		
			if($resultEditTitle)
			{
				$this->session->set_flashdata('Notification', 'Title Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('titles'); 
	}
	
	public function delete()
	{
		
		$deleteResult = $this->title_model->deleteTitle($this->input->post('txtdeltitle'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', 'Title Deleted Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('titles');
		
	}

	public function fetchDesc()
	{
		echo json_encode($this->title_model->getAll());
	}
	
	public function getValidTitle()
	{
		$validtitle = '';
		foreach ($this->title_model->getValidTitle() as $title) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validtitle.=strtolower($title['valtitle']).'|';
		}
		echo json_encode($validtitle);
	}

	public function getTitle($titl_id)
	{
		echo json_encode($this->title_model->getTitle($titl_id));
	}
	
}