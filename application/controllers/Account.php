<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('account_model');		
	}

	public function index()
	{
		
		$this->arrTemplateData['arrAccount'] = $this->account_model->getAccount($this->session->userdata('sessUserId'));
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'account/account_view', $this->arrTemplateData);
		
	}
	
	// public function update()
	// {
	// 	$strPost = $this->input->post();
		
	// 	if(isset($strPost))
	// 	{
	// 		$arrAccountData = array( 'usr_user'	=>  $this->input->post('txtName') );
	// 		$resultUpdateAccount = $this->account_model->updateAccount($arrAccountData, $this->session->userdata("sessUserId"));		
	// 		if($resultUpdateAccount)
	// 		{
	// 			$this->session->set_flashdata('Notification', 'Profile Saved Successfully');
	// 			$this->session->set_flashdata('NotificationBox', 'success');
	// 		}
	// 		redirect('account');
	// 	}	
		
	// }
	
}