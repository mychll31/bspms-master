<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Mancom extends CI_Controller { 

	public function __construct()
	{
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model(array('Title_model', 'Mancom_model'));
	}

	public function index()
	{
		$this->arrTemplateData = array(
								"titles"		=> $this->Title_model->getAll(),
								"mancom"		=> $this->Mancom_model->getAll(),
								);
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		$this->template->load('template/main_tpl', 'mancom/mancom_view', $this->arrTemplateData);
	}

	public function add() {
		$arrMancom = array(
				'man_title'				=>	$_POST['title'],
				'man_firstname'			=>	$_POST['fname'],
				'man_midname'			=>	$_POST['mname'],
				'man_lastname'			=>	$_POST['lname'],
				'man_email'				=>	$_POST['email'],
				'man_createdby'			=>	$_SESSION['sessUserId'],
				'man_createddate'		=>	date("Y-m-d H:i:s", time())
		);
		$res = $this->Mancom_model->addMember($arrMancom);
		redirect('mancom');
	}

	public function getMancom($manid) {
		echo json_encode($this->Mancom_model->getMancom($manid));
	}

	public function edit($manid) {
		$isactive = isset($_POST['chkisactive']) ? 1 : 0;
		$arrMancom = array(
				'man_title'				=>	$_POST['title'],
				'man_firstname'			=>	$_POST['fname'],
				'man_midname'			=>	$_POST['mname'],
				'man_lastname'			=>	$_POST['lname'],
				'man_email'				=>	$_POST['email'],
				'man_email'				=>	$_POST['email'],
				'man_updatedby'			=>	$_SESSION['sessUserId'],
				'man_updateddate'		=>	date("Y-m-d H:i:s", time()),
				'man_isactive'			=>  $isactive
		);
		$res = $this->Mancom_model->editMancom($manid, $arrMancom);
		redirect('mancom');
	}

	public function delete() {
		$arrMancom = array(
				'man_isRemove'			=>  1
		);
		$res = $this->Mancom_model->editMancom($_POST['txtdelfield'], $arrMancom);
		redirect('mancom');
	}

}
