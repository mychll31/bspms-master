<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Specialization extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('specialization_model');		
	}

	public function index()
	{
		$specializationId = $this->uri->segment(3);
		$this->arrTemplateData['arrSpecialization'] = $this->specialization_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['specializationEdit'] = $this->specialization_model->getSpecialization($specializationId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'specialization/specialization_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		if($this->input->post('btnSubmitSpecialization')=="add")
		{
			$arrSpecializationData = array( 
										'spe_code'			=>  $this->input->post('txtCode'),
										'spe_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultAddSpecialization = $this->specialization_model->addSpecialization($arrSpecializationData);		
			if($resultAddSpecialization)
			{
				$this->session->set_flashdata('Notification', 'Specializations Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('specialization');
		}
		else
		{
			redirect('specialization');
		}
	}
	
	
	
	
	public function edit()
	{
		if($this->input->post('btnSubmitSpecialization')=="edit")
		{
			$arrSpecializationData = array( 
										'spe_code'	=>  $this->input->post('txtCode'),
										'spe_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultEditSpecialization	= $this->specialization_model->editSpecialization($this->input->post('txtSpeId'), $arrSpecializationData);		
			if($resultEditSpecialization)
			{
				$this->session->set_flashdata('Notification', 'Specialization Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('specialization');
		}
		else
		{
			redirect('specialization');	
		}
	}
	
	public function delete()
	{
		$specializationId = $this->uri->segment(3);  
		
		$deleteResult = $this->specialization_model->deleteSpecialization($specializationId);
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Specialization Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('specialization');
		
	}
	
}