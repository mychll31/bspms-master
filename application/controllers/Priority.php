<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


class Priority extends CI_Controller {

	public function __construct() 
	{ 
		parent::__construct();  
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_priority'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model('priority_model');	 	
	}

	public function index()
	{
		$priorityId = $this->uri->segment(3);
		$this->arrTemplateData['arrPriority'] = $this->priority_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['priorityEdit'] = $this->priority_model->getPriority($priorityId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'priority/priority_view', $this->arrTemplateData);
 	 
	}
	
	public function add() 
	{
		$arrPriorityData = array(
								'pri_number' 	=> $this->input->post('txtCode'),
								'pri_desc'		=> $this->input->post('txtDesc')
								);

		$resultAddPriority = $this->priority_model->addPriority($arrPriorityData);
		if($resultAddPriority) {
			$this->session->set_flashdata('Notification', 'Priority Area Saved Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('priority');
	}
	
	public function edit()
	{
		$arrPriorityData = array(
								'pri_number'	=>  $this->input->post('txtCode'),
								'pri_desc'		=>  $this->input->post('txtDesc')
								);

		$resultEditPriority	= $this->priority_model->editPriority($this->input->post('txtSpeId'), $arrPriorityData);

		if($resultEditPriority) {
			$this->session->set_flashdata('Notification', 'Priority Area Updated Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('priority'); 
	}
	
	public function delete()
	{
		$deleteResult = $this->priority_model->deletePriority($this->input->post('txtdelprio'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Priority Area Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'Success');
		}
		redirect('priority');
		
	}
	
	public function fetchdesc()
	{
		echo json_encode($this->priority_model->getAll());
	}

	public function getPriorities($prio_id)
	{
		echo json_encode($this->priority_model->getPriority($prio_id));
	}

	public function fetchvalidprio()
	{
		$validprio = '';
		foreach ($this->priority_model->getValidPrio() as $prio) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validprio.=strtolower($prio['valpri']).'|';
		}
		echo json_encode($validprio);
	}
	
	
}