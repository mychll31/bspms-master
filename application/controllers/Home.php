<?php 
/**
* File Name: home.php
* ----------------------------------------------------------------------------------------------------
* Purpose of this file: 
* home controller
* ----------------------------------------------------------------------------------------------------
* System Name: DOST Information System Collaborative Portal
* ----------------------------------------------------------------------------------------------------
* Author: Emma Cristina Obenza
* ----------------------------------------------------------------------------------------------------
* Date of revision: April 29, 2016
* ----------------------------------------------------------------------------------------------------
* Copyright Notice:
* Copyright (C) 2014 by the Department of Science and Technology Central Office
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> $this->uri->segment(1),
			'form_title'      	=> SYSTEM_NAME
		);
		
		checkSession();
		
	}
	
	public function index()
	{
		redirect('scientists');
		// $this->template->load('template/main_tpl', 'home/home_view', $this->arrTemplateData);	
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */