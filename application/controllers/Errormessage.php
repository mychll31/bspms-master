<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Errormessage extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('Errormessage_model');		
	}

	//fetch Error Messages
	public function fetchErrorMessage()
	{
		$arr_errors = $this->Errormessage_model->getMessage();
		$messages = array();
		foreach ($arr_errors as $mess) {
			$messages[$mess['err_fieldname']] = $mess['err_message'];
		}
		echo json_encode($messages);
	}
	
}