<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {
	
	protected $arrTemplateData;
	
	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model(array('reports_model','scientists_model','Institutions_model'));
		$this->load->library('fpdf_gen');		
	}

	public function index()
	{		
		$rsReports = $this->reports_model->getReports();
		$arrReports['']='';
		foreach($rsReports as $row):
			$arrReports[$row['rep_group']][$row['rep_id']]=$row['rep_desc'];
		endforeach;
		$this->arrTemplateData['arrReports']=$arrReports;

		$this->arrTemplateData['arrScientists']=$this->scientists_model->getScientists();
		
		$arrYear['']='';
		for($i=date('Y');$i>=2013;$i--):
			$arrYear[$i]=$i;
		endfor;
		$this->arrTemplateData['arrYear']=$arrYear;
		
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'reports/reports_view', $this->arrTemplateData);
		
	}

	public function exportToExcel(){
		$rpt_id=$this->uri->segment(3);
		switch($rpt_id)
		{
			case 1: 
				$this->load->model(array('reports/a_expert_profile_rpt_model'));
				$this->a_expert_profile_rpt_model->generate();
			break;
			case 2:
				$this->load->model(array('reports/profile_gdp_rpt_model'));
				$this->profile_gdp_rpt_model->generate();
			break;
			case 3:
				$this->load->model(array('reports/profile_gae_rpt_model'));
				$this->profile_gae_rpt_model->generate();
			break;
			case 4:
				$this->load->model(array('reports/a_distribution_bsp_awardees_profession'));
				$this->a_distribution_bsp_awardees_profession->generate();
			break;
			case 5:
				$this->load->model(array('reports/a_list_bsp_awardees_profession'));
				$this->a_list_bsp_awardees_profession->generate();
			break;
			case 6:
				$this->load->model(array('reports/a_distribution_bsp_professionalLicense'));
				$this->a_distribution_bsp_professionalLicense->generate();
			break;
			case 7:
				$this->load->model(array('reports/a_list_bsp_professionalLicense'));
				$this->a_list_bsp_professionalLicense->generate();
			break;
			case 8:
				$this->load->model(array('reports/a_distribution_bsp_areaofExpertise'));
				$this->a_distribution_bsp_areaofExpertise->generate();
			break;
			case 9:
				$this->load->model(array('reports/a_distribution_bsp_localRegion'));
				$this->a_distribution_bsp_localRegion->generate();
			break;
			case 10:
				$this->load->model(array('reports/a_distribution_bsp_globalRegion'));
				$this->a_distribution_bsp_globalRegion->generate();
			break;
			case 11:
				$this->load->model(array('reports/b_bsp_distribution_byInstitution'));
				$this->b_bsp_distribution_byInstitution->generate();
			break;
			case 12:
				$this->load->model(array('reports/c_listof_NewBSP_awardees_by_year_rpt_model'));
				$this->c_listof_NewBSP_awardees_by_year_rpt_model->generate();
			break;
			case 13:
				$this->load->model(array('reports/c_listofContinuing_bsp_awardees_and_engagements_rpt_model'));
				$this->c_listofContinuing_bsp_awardees_and_engagements_rpt_model->generate();
			break;
			case 14:
				$this->load->model(array('reports/c_listofBsp_awardees_withCompleted_serviceObligations_rpt_model'));
				$this->c_listofBsp_awardees_withCompleted_serviceObligations_rpt_model->generate();
			break;
			case 15:
				$this->load->model(array('reports/c_list_ongoing_bsp_awardees_rpt_model'));
				$this->c_list_ongoing_bsp_awardees_rpt_model->generate();
			break;
			
		}
	}
	
	public function generate()
	{
		$rpt_id=$this->uri->segment(3);
		$arrGet=$this->input->get();
		switch($rpt_id)
		{
			case 1: 
				$this->load->model(array('reports/a_expert_profile_rpt_model'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtScientist'=>$arrGet['txtScientist']);
				$this->a_expert_profile_rpt_model->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 2: 
				$this->load->model(array('reports/profile_gdp_rpt_model'));
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->profile_gdp_rpt_model->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 3: 
				$this->load->model(array('reports/profile_gae_rpt_model'));	
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->profile_gae_rpt_model->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 4: 
				$this->load->model(array('reports/a_distribution_bsp_awardees_profession'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->a_distribution_bsp_awardees_profession->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 5: 
				$this->load->model(array('reports/a_list_bsp_awardees_profession'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->a_list_bsp_awardees_profession->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 6: 
				$this->load->model(array('reports/a_distribution_bsp_professionalLicense'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->a_distribution_bsp_professionalLicense->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 7: 
				$this->load->model(array('reports/a_list_bsp_professionalLicense'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->a_list_bsp_professionalLicense->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 8: 
				$this->load->model(array('reports/a_distribution_bsp_areaofExpertise'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->a_distribution_bsp_areaofExpertise->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 9: 
				$this->load->model(array('reports/a_distribution_bsp_localRegion'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->a_distribution_bsp_localRegion->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 10: 
				$this->load->model(array('reports/a_distribution_bsp_globalRegion'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->a_distribution_bsp_globalRegion->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 11: 
				$this->load->model(array('reports/b_bsp_distribution_byInstitution'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->b_bsp_distribution_byInstitution->generate($arrData);
				echo $this->fpdf->Output();		
			break;
			case 12: 
				$this->load->model(array('reports/c_listof_NewBSP_awardees_by_year_rpt_model'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->c_listof_NewBSP_awardees_by_year_rpt_model->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 13: 
				$this->load->model(array('reports/c_listofContinuing_bsp_awardees_and_engagements_rpt_model'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->c_listofContinuing_bsp_awardees_and_engagements_rpt_model->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 14: 
				$this->load->model(array('reports/c_listofBsp_awardees_withCompleted_serviceObligations_rpt_model'));				
				$this->load->library('Fpdf_template');
				$this->fpdf = new Fpdf_template();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->c_listofBsp_awardees_withCompleted_serviceObligations_rpt_model->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 15: 
				$this->load->model(array('reports/c_list_ongoing_bsp_awardees_rpt_model'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->c_list_ongoing_bsp_awardees_rpt_model->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 16: 
				$this->load->model(array('reports/c_summary_report'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->c_summary_report->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 17:
				if($arrGet['optrad2']=='bychrono'){
					$this->load->model(array('reports/c_awardees_byPeriod_inChronologicalOrder_rpt_model'));				
					$this->fpdf = new FPDF();
					$this->fpdf->AliasNbPages();
					$this->fpdf->Open();
					$arrData=array('txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
					$this->c_awardees_byPeriod_inChronologicalOrder_rpt_model->generate($arrData);
					echo $this->fpdf->Output();
				}elseif($arrGet['optrad2']=='byperphase'){
					$this->load->model(array('reports/c_bsp_awardees_byPeriod_inAlphabeticalOrder_perPhase_rpt_model'));				
					$this->fpdf = new FPDF();
					$this->fpdf->AliasNbPages();
					$this->fpdf->Open();
					$arrData=array('txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
					$this->c_bsp_awardees_byPeriod_inAlphabeticalOrder_perPhase_rpt_model->generate($arrData);
					echo $this->fpdf->Output();	
				}elseif($arrGet['optrad2']=='bypercateg'){
					$this->load->model(array('reports/c_bsp_awardees_byPeriod_inAlphabeticalOrder_perCategory_rpt_model'));				
					$this->fpdf = new FPDF();
					$this->fpdf->AliasNbPages();
					$this->fpdf->Open();
					$arrData=array('txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
					$this->c_bsp_awardees_byPeriod_inAlphabeticalOrder_perCategory_rpt_model->generate($arrData);
					echo $this->fpdf->Output();	
				}else{}
			break;
			case 18: 
				$this->load->model(array('reports/c_list_erp'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->c_list_erp->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 19: 
				$this->load->model(array('reports/c_list_pending_erp'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->c_list_pending_erp->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 20: 
				$this->load->model(array('reports/c_awardees_accomplishments'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear']);
				$this->c_awardees_accomplishments->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 21: 
				$this->load->model(array('reports/d_bsp_seminars'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_seminars->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 22: 
				$this->load->model(array('reports/d_bsp_trainings'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_trainings->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 23: 
				$this->load->model(array('reports/d_bsp_project'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_project->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 24: 
				$this->load->model(array('reports/d_bsp_publication'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_publication->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 25: 
				$this->load->model(array('reports/d_bsp_studentsMentored'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_studentsMentored->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 26: 
				$this->load->model(array('reports/d_bsp_curriculumCourseDev'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_curriculumCourseDev->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 27: 
				$this->load->model(array('reports/d_bsp_networkAndLinks'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_networkAndLinks->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 28: 
				$this->load->model(array('reports/d_bsp_researchAndDev'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_researchAndDev->generate($arrData);
				echo $this->fpdf->Output();	
			break;
			case 29:
				$this->load->model(array('reports/d_bsp_others'));				
				$this->fpdf = new FPDF();
				$this->fpdf->AliasNbPages();
				$this->fpdf->Open();
				$arrData=array('txtYear'=>$arrGet['txtYear'],'optradio'=>$arrGet['optradio'],'txtYear1'=>$arrGet['txtYear1'],'txtYear2'=>$arrGet['txtYear2']);
				$this->d_bsp_others->generate($arrData);
				echo $this->fpdf->Output();	
			break;

		}
	}

	public function getdate_approval(){
		$eddate = $this->reports_model->getreportYear();
		$enddate = $eddate[0]['edate'];
		$startdate = $eddate[0]['sdate'];

		if($startdate=='0000')
			$startdate = '1970';
		$alldate = array();
		for ($i=$startdate; $i <= $enddate ; $i++) { 
			array_push($alldate, intval($i));
		}
		echo json_encode($alldate);
	}

	public function getdate_range($yr,$period){
		$eddate = $this->reports_model->getreportDate();
		$enddate = date('Y', strtotime($eddate[0]['enddate']));
		$startdate = ($period==0) ? $yr : $yr-1;
		$alldate = array();
		for ($i=$startdate+1; $i <= $enddate ; $i++) { 
			array_push($alldate, intval($i));
		}
		echo json_encode($alldate);
	}	
	
}