<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  TrackingSheet extends CI_Controller { 

	public function __construct()
	{
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model(array('user_model', 'TrackingSheet_model', 'Institutions_model', 'Council_model'));
	}

	public function index()
	{
		$level = 0;
		if(checkAccess($_SESSION['sessAccessLevel'], 'limit_scientist')>0)
			$level = 0;
		else
			$level = 1;

		$this->arrTemplateData = array();
		$this->arrTemplateData = array(
								"users"			=> $this->user_model->getAll(),
								"apppackage" 	=> $this->TrackingSheet_model->getAll(),
								"trackingsheet" => $this->TrackingSheet_model->getAllTracks($level),
								"reqs"			=> $this->TrackingSheet_model->getAllAddtlReqs(),
								"hosts"			=> $this->Institutions_model->getInstitutionList()
								);
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		$this->template->load('template/main_tpl', 'trackingsheet/trackingsheet_view', $this->arrTemplateData);
	}

	public function add()
	{	
		$visitdate = explode(' - ', $_POST['visitdate']);
		$arrTrackSheet = array(
				'ts_calendaryr'					=>	date("Y", time()),
				'ts_scientistname'				=>	$_POST['scientist'],
				'ts_concernid'					=>	$_POST['chkconcern'],
				'ts_concern_others'				=>	$_POST['txtconcern_others'],
				'ts_assignedSecretariat'		=>	$_POST['asssec'],
				'ts_typeofaward'				=>	$_POST['chktypeofaward'],
				'ts_applicationpackage'			=>	$_POST['chkapppackage'],
				'ts_addtionalreq'				=>	$_POST['chkaddtlreqs'],
				'ts_addtionalreq_others'		=>	$_POST['txtaddtlreq_others'],
				'ts_hostistitution'				=>	$_POST['host'],
				'ts_proposed_duration_startdate' =>	date("Y-m-d", strtotime($visitdate[0])),
				'ts_proposed_duration_enddate'	=>	date("Y-m-d", strtotime($visitdate[1])),
				'ts_addedby'					=>	$_SESSION['sessUserId'],
				'ts_addeddate'					=>	date("Y-m-d H:i:s", time())
		);
		$resEvent = $this->TrackingSheet_model->addTrackSheet($arrTrackSheet);
		redirect('trackingSheet');
	}

	public function edit($trackid)
	{	
		$visitdate = explode(' - ', $_POST['visitdate']);
		$arrTrackSheet = array(
				'ts_calendaryr'					=>	date("Y", time()),
				'ts_scientistname'				=>	$_POST['scientist'],
				'ts_concernid'					=>	$_POST['chkconcern'],
				'ts_concern_others'				=>	$_POST['txtconcern_others'],
				'ts_assignedSecretariat'		=>	$_POST['asssec'],
				'ts_typeofaward'				=>	$_POST['chktypeofaward'],
				'ts_applicationpackage'			=>	$_POST['chkapppackage'],
				'ts_addtionalreq'				=>	$_POST['chkaddtlreqs'],
				'ts_addtionalreq_others'		=>	$_POST['txtaddtlreq_others'],
				'ts_hostistitution'				=>	$_POST['host'],
				'ts_proposed_duration_startdate' =>	date("Y-m-d", strtotime($visitdate[0])),
				'ts_proposed_duration_enddate'	=>	date("Y-m-d", strtotime($visitdate[1])),
				'ts_lastupdatedby'				=>	$_SESSION['sessUserId'],
				'ts_lastupdateddate'			=>	date("Y-m-d H:i:s", time())
		);
		$resEvent = $this->TrackingSheet_model->editTrackSheet($arrTrackSheet, $trackid);
		redirect('trackingSheet');
	}

	public function delete()
	{
		$arrTrackSheet = array(
				'ts_isRemove'					=>	1,
				'ts_lastupdatedby'				=>	$_SESSION['sessUserId'],
				'ts_lastupdateddate'			=>	date("Y-m-d H:i:s", time())
		);
		$resEvent = $this->TrackingSheet_model->editTrackSheet($arrTrackSheet, $_POST['txtdelfield']);
		redirect('trackingSheet');
	}

	public function view($trackid)
	{
		$this->arrTemplateData = array(
								"users"			=> $this->user_model->getAll(),
								"apppackage" 	=> $this->TrackingSheet_model->getAll(),
								"trackingsheet" => $this->TrackingSheet_model->getAllTracksById($trackid),
								"reqs"			=> $this->TrackingSheet_model->getAllAddtlReqs(),
								"hosts"			=> $this->Institutions_model->getInstitutionList(),
								"actions"		=> $this->TrackingSheet_model->getAllTracksAction($trackid),
								"recievers"		=> $this->TrackingSheet_model->getAllrecievers(),
								"recievers2"	=> $this->Council_model->getAll()
								);
		$this->template->load('template/main_tpl', 'trackingsheet/trackingsheet_view_trackid', $this->arrTemplateData);
	}

	public function receive()
	{
		$this->arrTemplateData = array(
								"trackingsheet" => $this->TrackingSheet_model->getReceiveTracks(),
								"users"			=> $this->user_model->getAll(),
								"apppackage" 	=> $this->TrackingSheet_model->getAll(),
								"reqs"			=> $this->TrackingSheet_model->getAllAddtlReqs(),
								"hosts"			=> $this->Institutions_model->getInstitutionList()
								);
		$this->template->load('template/main_tpl', 'trackingsheet/trackingsheet_receive', $this->arrTemplateData);
	}

	public function add_trackaction($trackid)
	{
		$visitdate = explode(' - ', $_POST['visitdate']);
		$arrTrackSheet = array(
				'action_trackid'		=>	$trackid,
				'action_dateFrom'		=>	date("Y-m-d", strtotime($visitdate[0])),
				'action_dateTo'			=>	date("Y-m-d", strtotime($visitdate[1])),
				'action_forwardTo'		=>	$_POST['recipient'],
				'action_secretariat'	=>	$_SESSION['sessUserId'],
				'action_remarks'		=>	$_POST['remarks'],
				'action_createdby'		=>	$_SESSION['sessUserId'],
				'action_createddate'	=>	date("Y-m-d H:i:s", time())
		);
		$resEvent = $this->TrackingSheet_model->addTrackSheet_action($arrTrackSheet);
		redirect('trackingSheet/view/'.$trackid);
	}

	public function getAction($actionid)
	{
		$action = $this->TrackingSheet_model->getActionById($actionid);
		$visitF = date('m/d/Y', strtotime($action['action_dateFrom']));
		$visitT = date('m/d/Y', strtotime($action['action_dateTo']));
		$action['visitdate'] = $visitF.' - '.$visitT;
		echo json_encode($action);
	}

	public function edit_trackaction($actionid, $trackid)
	{
		$visitdate = explode(' - ', $_POST['visitdate']);
		$arrTrackAction = array(
				'action_trackid'		=>	$trackid,
				'action_dateFrom'		=>	date("Y-m-d", strtotime($visitdate[0])),
				'action_dateTo'			=>	date("Y-m-d", strtotime($visitdate[1])),
				'action_secretariat'	=>	$_SESSION['sessUserId'],
				'action_remarks'		=>	$_POST['remarks'],
				'action_forwardTo'		=>	$_POST['recipient'],
				'action_lastupdatedby'	=>	$_SESSION['sessUserId'],
				'action_lastupdateddate' =>	date("Y-m-d H:i:s", time())
		);
		$resEvent = $this->TrackingSheet_model->editTrackSheet_action($arrTrackAction, $actionid);
		redirect('trackingSheet/view/'.$trackid);
	}

	public function delete_action($trackid)
	{
		$arrTrackAction = array(
				'action_isRemove'		=>	1		
		);
		$resEvent = $this->TrackingSheet_model->editTrackSheet_action($arrTrackAction, $_POST['txtdelfield']);
		redirect('trackingSheet/view/'.$trackid);
	}

	public function receive_action($trackid)
	{
		$arrTrackAction = array(
				'action_isReceived'		=>	1,
				'action_receivedBy'		=>	$_SESSION['sessUserId'],
				'action_receivedDate'	=>	date("Y-m-d H:i:s", time())
		);

		$resEvent = $this->TrackingSheet_model->editTrackSheet_action($arrTrackAction, $_POST['txtrecaction']);
		redirect('trackingSheet/view/'.$trackid);
	}

	public function allReceived(){
		$this->arrTemplateData = array(
								"trackingsheet" => $this->TrackingSheet_model->getReceiveTracks(1)
								);
		$this->template->load('template/main_tpl', 'trackingsheet/trackingsheet_receive', $this->arrTemplateData);
	}

	public function gettsheet($trackid)
	{
		$track = $this->TrackingSheet_model->getAllTracksById($trackid);
		$visitF = date('m/d/Y', strtotime($track['ts_proposed_duration_startdate']));
		$visitT = date('m/d/Y', strtotime($track['ts_proposed_duration_enddate']));
		$track['visitdate'] = $visitF.' - '.$visitT;
		echo json_encode($track);
	}

	public function processCompleted()
	{
		$this->arrTemplateData = array(
								"trackingsheet" => $this->TrackingSheet_model->getCompleteTracks()
								);
		$this->template->load('template/main_tpl', 'trackingsheet/trackingsheet_receive', $this->arrTemplateData);
	}

	public function complete($tsid)
	{
		$arrTrackAction = array(
				'ts_iscomplete'		=>	1,
				'ts_completeby'		=>	$_SESSION['sessUserId'],
				'ts_completeddate'	=>	date("Y-m-d H:i:s", time())
		);
		$resEvent = $this->TrackingSheet_model->editTrackSheet($arrTrackAction, $tsid);
		redirect('trackingSheet/view/'.$tsid);
	}

}
