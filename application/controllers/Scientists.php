<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scientists extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model(array('Citizenship_model','Council_model','Institutions_model','Outcome_model','Priority_model','Serviceawards_model','Education_model','Title_model','scientists_model', 'User_model', 'Universities_model'));
		$this->load->library('fpdf_gen');		

	}
	
	public function index()
	{
		$this->arrTemplateData = array(
								"arrSpecialization"		=> $this->scientists_model->getSpecializations(),
								"arrExpertise" 			=> $this->scientists_model->getAreaExpertise(),
								"arrScientists" 		=> $this->scientists_model->getScientists()
								);
		// echo '<pre>';
		// print_r($this->arrTemplateData['arrScientists']);
		// print_r($_SESSION);

		if(checkAccess($_SESSION['sessAccessLevel'], 'limit_scientist')>0){
    		foreach ($this->arrTemplateData['arrScientists'] as $key=>$scientist) {
    			if($scientist['usr_council']!=$_SESSION['sessCouncilid']){
    				unset($this->arrTemplateData['arrScientists'][$key]);
    			}
    		}
  		}
  		// die();
		$this->template->load('template/main_tpl', 'scientists/scientist_view', $this->arrTemplateData);
		
	}

//---------------------------------------------------------ADD FUNCTIONS--------------------------------------------------------------------------------------------------	
	public function add()
	{
		$this->load->library('form_validation');
		$this->arrTemplateData= array(
										"arrAreaExpertise" 		=> $this->scientists_model->getAreaExpertise(),
										"arrSpecializations"	=> $this->scientists_model->getSpecializations(),
										"arrProfessions"		=> $this->scientists_model->getProfessions(),
										"arrTitles"				=> $this->Title_model->getAll()
										); 
										
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		
		if($this->input->post('btnSubmit')=="add")
		{
			$imagefilename = '';
			
			$config['upload_path']          = 'data/sci_images/';
            $config['allowed_types']        = 'gif|jpg|png';
            $this->load->library('upload', $config);
            
			if ( ! $this->upload->do_upload('file-image')){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
				$imagefilename = $data['upload_data']['file_name'];
			}

			$contacts = "";
			foreach ($this->input->post('arrContact') as $contact) {
				$contacts .= $contact.';';
			}
			$emails = "";
			foreach ($this->input->post('arrEmail') as $email) {
				$emails .= $email.';';
			}
			$specialization = "";
			$areaOfExpertise = "";
			foreach($this->input->post('cmbAreaOfExpertise') as $cmbAreaofExpertise ):
				$areaOfExpertise .= $cmbAreaofExpertise."|";
			endforeach;
			
			foreach($this->input->post('cmbSpecialization') as $cmbSpecialization ):
				$specialization .= $cmbSpecialization."|";
			endforeach;

			$arrScientistData = array( 
										'sci_first_name'		=>  $this->input->post('txtFirstName'),
										'sci_middle_name'		=>  $this->input->post('txtMiddleName'),
										'sci_middle_initial'	=>  $this->input->post('txtMiddleInitial'),
										'sci_last_name'			=>  $this->input->post('txtLastName'),
										'sci_ext_name'			=>  $this->input->post('txtExtName'),
										'sci_title'				=>  $this->input->post('cmbTitle'),
										'sci_birthdate'			=>  $this->input->post('txtBirthDate'),
										'sci_gender'			=>  $this->input->post('cmbGender'),
										'sci_status'			=>  $this->input->post('cmbStatus'),
										'sci_expertise_id'		=>  $areaOfExpertise,
										'sci_specialization_id'	=>  $specialization,
										'sci_profession_id'		=>  $this->input->post('cmbProfession'),
										'sci_license'			=>  $this->input->post('txtProfessionalLicense'),
										'sci_contact'			=>  $contacts,
										'sci_email'				=>  $emails,
										'sci_postal_address'	=>  $this->input->post('txtPostalAddress'),
										'sci_picturename'		=> 	$imagefilename,
										'created_at'			=>  date("Y-m-d H:i:s"),
										'created_by'			=>  $this->session->userdata("sessUserId")
										 );

			$resultAddScientist = $this->scientists_model->addScientist($arrScientistData);		
			if($resultAddScientist)
			{
				$this->session->set_flashdata('Notification', 'Scientist Profile Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('scientists/addSciEducation/'.$resultAddScientist);
		}
		else
		{
			$this->template->load('template/main_tpl', 'scientists/scientist_add_view', $this->arrTemplateData);	
		}
	}

	public function addSciEducation()
	{
		$scientistId = $this->uri->segment(3);
		$this->arrTemplateData = array(
										"arrScientist"			=> $this->scientists_model->getScientist($scientistId),
										"arrScientistEducation" => $this->scientists_model->getScientistEducation($scientistId),
										"arrEducationLevels" 	=> $this->scientists_model->getEducationLevels(),
										"arrEducationLevels1" 	=> $this->scientists_model->getEducationLevels(),
										"arrCountry" 			=> $this->scientists_model->getCountry(),
										"arrUniversity"			=> $this->Universities_model->getAll(),
										"arrCourse" 			=> $this->scientists_model->getCourse()
										);

		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;

		if($this->input->post('btnSubmitEducation')=="add" OR $this->input->post('btnSubmitEducation')=="addEdit")
			{
				$arrScientistEducationData = array( 
											'educ_sci_id'			=>  $scientistId,
											'educ_level_id'			=>  $this->input->post('cmbLevel'),
											'educ_school'			=>  $this->input->post('txtSchool'),
											'educ_countryorigin_id'	=>  $this->input->post('cmbCountry'),
											'educ_course_id'		=>  $this->input->post('cmbCourse'),
											'educ_year_graduated'	=>  $this->input->post('txtYearGraduated'),
											'created_at'			=>  date("Y-m-d H:i:s"),
											'created_by'			=>  $this->session->userdata("sessUserId")
											 );
				$resultAddScientistEducation = $this->scientists_model->addScientistEducation($arrScientistEducationData);
						
				if($resultAddScientistEducation)
				{
					$this->session->set_flashdata('Notification', "Scientist's Education Saved Successfully");
					$this->session->set_flashdata('NotificationBox', 'success');
				}
				
				if($this->input->post('btnSubmitEducation')=="addEdit")
				{
					redirect('scientists/edit/'.$scientistId);
				}
				else
				{
					redirect('scientists/addSciEducation/'.$scientistId);
				}
		}

		$this->template->load('template/main_tpl', 'scientists/scientist_education_view', $this->arrTemplateData);
	}
	
	public function addEducation()
	{
		$scientistId = $this->uri->segment(3);
		$arrScientistEducationData = array( 
											'educ_sci_id'			=>  $scientistId,
											'educ_level_id'			=>  $this->input->post('cmbLevel'),
											'educ_school'			=>  $this->input->post('txtSchool'),
											'educ_countryorigin_id'	=>  $this->input->post('cmbCountry'),
											'educ_course_id'		=>  $this->input->post('cmbCourse'),
											'educ_year_graduated'	=>  $this->input->post('txtYearGraduated'),
											'created_at'			=>  date("Y-m-d H:i:s"),
											'created_by'			=>  $this->session->userdata("sessUserId")
											 );
		$resultAddScientistEducation = $this->scientists_model->addScientistEducation($arrScientistEducationData);
		redirect('scientists/edit/'.$scientistId.'/educ');

		$this->template->load('template/main_tpl', 'scientists/scientist_education_view', $this->arrTemplateData);
		
	}

	public function addScitba()
	{
		$scientistId = $this->uri->segment(3);
		$this->arrTemplateData = array(
										"arrScientist"	=> $this->scientists_model->getScientist($scientistId),
										"arrSciTba"		=> $this->scientists_model->getSciTba($scientistId),
										"arrInstitution"=> $this->Institutions_model->getInstitutionList()
										);
		$this->template->load('template/main_tpl', 'scientists/scientist_tba_view', $this->arrTemplateData);
	}
	
	public function addScientistTba(){
		$arrScientistData = array(
							'tba_sci_id'	=> $this->uri->segment(3),
							'tba_host_institution' => $this->input->post('hostinst'),
							'tba_text' 		=> $this->input->post('descr')
							);
		$resultEditScientistProfile = $this->scientists_model->addScientistTba($arrScientistData);
		if($this->uri->segment(4)=='profile'){
			redirect('scientists/editServiceAward/'.$this->uri->segment(3).'/tba?subs='.$_GET['subs']);
		}
		redirect('scientists/editServiceAward/'.$this->uri->segment(3).'/tba?subs='.$_GET['subs']);
	}

	public function editScientistTba(){
		$arrScientistData = array(
							'tba_host_institution' => $this->input->post('hostinst'),
							'tba_text' 		=> $this->input->post('descr')
							);
		$resultEditScientistProfile = $this->scientists_model->editScientistTba($arrScientistData, $this->uri->segment(3));
		redirect('scientists/editServiceAward/'.$this->uri->segment(4).'/tba?subs='.$_GET['subs']);
	}

	public function addScientistProfileTba(){
		$arrScientistData = array('sci_tba' => $this->input->post('descr'));
		$resultEditScientistProfile = $this->scientists_model->editProfile($arrScientistData , $this->uri->segment(3));
		redirect('scientists/edit/'.$this->uri->segment(3).'/tba');
	}

	public function addSciEmployment()
	{
		$scientistId = $this->uri->segment(3);
		$this->arrTemplateData = array(
										"arrScientist"			=> $this->scientists_model->getScientist($scientistId),
										"arrScientistEmployment"=> $this->scientists_model->getScientistEmployment($scientistId),
										"arrCountry" 			=> $this->scientists_model->getCountry()
										);
		$this->load->library('form_validation');
		
		if ($this->form_validation->run('addEmployment') == TRUE)
		{
			if($this->input->post('btnSubmitEmployment')=="add" OR $this->input->post('btnSubmitEmployment')=="addEdit")
			{
				$arrScientistEducationData = array( 
											'emp_sci_id'			=>  $scientistId,
											'emp_company'			=>  $this->input->post('txtCompany'),
											'emp_position'			=>  $this->input->post('txtPosition'),
											'emp_countryorigin_id'	=>  $this->input->post('cmbCountry'),
											'emp_datefrom'			=>  $this->input->post('txtDateFrom'),
											'emp_dateto'			=>  $this->input->post('txtDateTo'),
											'created_at'			=>  date("Y-m-d H:i:s"),
											'ispresent'				=>  ($this->input->post('chkPresent')=='on') ? 1 : 0,
											'created_by'			=>  $this->session->userdata("sessUserId")
											 );
				$resultAddScientistEducation = $this->scientists_model->addScientistEmployment($arrScientistEducationData);
						
				if($resultAddScientistEducation)
				{
					$this->session->set_flashdata('Notification', "Scientist's Education Saved Successfully");
					$this->session->set_flashdata('NotificationBox', 'success');
				}
				
				if($this->input->post('btnSubmitEmployment')=="addEdit")
				{
					redirect('scientists/edit/'.$scientistId);
				}
				else
				{
					redirect('scientists/addSciEmployment/'.$scientistId);
				}
			}
		}
		$this->template->load('template/main_tpl', 'scientists/scientist_employment_view', $this->arrTemplateData);
	}
	public function addEmployment()
	{
		$scientistId = $this->uri->segment(3);
		$arrScientistEmploymentData = array( 
										'emp_sci_id'			=>  $scientistId,
										'emp_company'			=>  $this->input->post('txtco'),
										'emp_position'			=>  $this->input->post('txtpos'),
										'emp_countryorigin_id'	=>  $this->input->post('cmbCountry'),
										'emp_datefrom'			=>  $this->input->post('txtdfrom'),
										'emp_dateto'			=>  $this->input->post('txtdto'),
										'ispresent'				=>  ($this->input->post('chkPresent')=='on') ? 1 : 0,
										'created_at'			=>  date("Y-m-d H:i:s"),
										'created_by'			=>  $this->session->userdata("sessUserId")
										 );
		$resultAddScientistEmployment = $this->scientists_model->addScientistEmployment($arrScientistEmploymentData);
		if($this->uri->segment(4) == "newsci")
			redirect('scientists/addSciEmployment/'.$scientistId);
		else
			redirect('scientists/edit/'.$scientistId.'/employ');
	}
	
	
	public function addSuccess()
	{
		$scientistId = $this->uri->segment(3);
		$this->arrTemplateData = array(
										"arrScientist"			=> $this->scientists_model->getScientist($scientistId)
										);
		$this->template->load('template/main_tpl', 'scientists/scientist_notification_view', $this->arrTemplateData);
			
	}

	public function addServiceAward()
	{
		$subs = '';
		$scientistId = $this->uri->segment(3);
		$this->arrTemplateData = array(
										"arrServiceAwards"		=> $this->Serviceawards_model->getEmptyServiceAwards(),
										"arrUsers"				=> $this->User_model->getAll(),
										"arrCountry" 			=> $this->scientists_model->getCountry(),
										"arrCouncil"			=> $this->Council_model->getAll(),
										"arrstat"				=> $this->Serviceawards_model->getAllStat(),
										"arrInstitution"		=> $this->Institutions_model->getInstitutionList(),
										"arrOutcomes"			=> $this->Outcome_model->getAll(),
										"arrPriority"			=> $this->Priority_model->getAll(),
										"arrReports"			=> $this->Serviceawards_model->getEmptyReports(),
										"arrSeminarList" 		=> $this->Serviceawards_model->getSeminars($scientistId, 3),
										"arrTrainingList" 		=> $this->Serviceawards_model->getTrainings($scientistId, 3),
										"arrProjectList" 		=> $this->Serviceawards_model->getProjects($scientistId, 3),
										"arrPaper"				=> $this->Serviceawards_model->getPaper($scientistId, 3),
										"arrMentor"				=> $this->Serviceawards_model->getMentoring($scientistId, 3),
										"arrCurr"				=> $this->Serviceawards_model->getCurriculum($scientistId, 3),
										"arrNetwork"			=> $this->Serviceawards_model->getNetworks($scientistId, 3),
										"arrResearch"			=> $this->Serviceawards_model->getResearch($scientistId, 3),
										"arrOther"				=> $this->Serviceawards_model->getOthers($scientistId, 3),
										"arrParticipants"		=> $this->Serviceawards_model->getParticipants(),
										"arrFields"				=> $this->Serviceawards_model->getFields(),
										"arrCitizenship"		=> $this->Citizenship_model->getAll(),
										);
		if($this->input->post('add')=="addSAProfile") {
			$subs = $this->input->post('txtsubs');
			$ctrClass = $this->Serviceawards_model->countClassification($scientistId);
			$ctrClass = $ctrClass[0]['lastinsertedClass'];
			$arrScientstServceAward = array( 
										'srv_sci_id'				=>  $scientistId,
										'srv_classification'		=>  $ctrClass+1,
										'srv_approval_date'			=>  (null !== $this->input->post('approvalyear')) ? '' : $this->input->post('txtApprovalDate'),
										'srv_approval_yr'			=>  (null !== $this->input->post('approvalyear')) ? $this->input->post('txtapprovalyear') : '',
										'srv_isyearonly'			=>  (null !== $this->input->post('approvalyear')) ? 1 : 0,
										'srv_change_sched'			=>  $this->input->post('cmbSAChangeSched'),
										'srv_sched_approvaldate'	=>  $this->input->post('txtChangeSchedApprovalDate'),
										'srv_glo_id'				=>  $this->input->post('cmbSACountry'),
										'srv_cit_id'				=>  $this->input->post('cmbSACitizenship'),
										'srv_typeofaward'			=>  $this->input->post('cmbSATypeofAward'),
										'srv_cil_id'				=>  $this->input->post('cmbSACouncil'),
										'srv_status'				=>  $this->input->post('cmbstatus'),
										'srv_type_contract'			=>  $this->input->post('cmbTypeOfContract'),
										'srv_ins_id'				=>  ($this->input->post('cmbInstitutions')!=null) ? implode('|', $this->input->post('cmbInstitutions')) : '',
										'srv_approved_days'			=>  $this->input->post('txtTotalDaysYears'),
										'srv_out_id'				=>  ($this->input->post('cmbAccOutcomes')!=null) ? implode('|', $this->input->post('cmbAccOutcomes')) : '',
										'srv_pri_id'				=>  ($this->input->post('cmbAccPriority')!=null) ? implode('|', $this->input->post('cmbAccPriority')) : '',
										'created_at'				=>  date("Y-m-d H:i:s"),
										'created_by'				=>  $this->session->userdata("sessUserId")
										 );
			if($this->input->post('cmbTypeOfContract')==0){
				$phaseDate = explode(';', $this->input->post('txtphasesdate'));
				$arrScientstServceAward1 = array(
										 'srv_cont_startDate'		=> $phaseDate[0],
										 'srv_cont_endDate'			=> $phaseDate[1],
										 );
				$arrScientstServceAward  = array_merge($arrScientstServceAward, $arrScientstServceAward1);
			}
			$resultAddScientist = $this->Serviceawards_model->addServiceAward($arrScientstServceAward);
			$phases_arr = explode(';',$this->input->post('txtphasesdate'));
			$phaseCtr = 0;
			$phase = '';
			$arr_contractDate = array();
			foreach ($phases_arr as $pha) {
				if($phaseCtr++ % 2 == 0){
					$con_dateFrom = $pha;
				}else{
					$con_dateTo = $pha;
					if($con_dateFrom != '' and $con_dateTo != ''){
						$arr_contractDate = array(
						'con_srv_id' 		=> $resultAddScientist,
						'con_iscontinuous' 	=> $this->input->post('cmbTypeOfContract'),
						'con_date_from'		=> $con_dateFrom,
						'con_date_to'		=> $con_dateTo,
						'con_total_number'	=> ''
					);
					$arrAddContracts = $this->Serviceawards_model->addContractDates($arr_contractDate);
					$con_dateFrom = '';
					$con_dateTo = '';
					}
				}
			}
			redirect(base_url('scientists/editServiceAward/'.$resultAddScientist.'/tba?subs='.$subs));
		}
		// echo '<pre>';
		// die(print_r($this->arrTemplateData));
		$this->template->load('template/main_tpl', 'scientists/scientist_sa_add_view', $this->arrTemplateData);
	}

	public function editServiceAward()
	{
		$subs = '';
		$scientistId = $this->uri->segment(3);
		$this->arrTemplateData = array(
										"arrServiceAwards"		=> $this->Serviceawards_model->getScientistServiceAwards($scientistId),
										"arrUsers"				=> $this->User_model->getAll(),
										"arrContractDates"		=> $this->Serviceawards_model->getContractDates($scientistId),
										"arrCountry" 			=> $this->scientists_model->getCountry(),
										"arrCouncil"			=> $this->Council_model->getAll(),
										"arrstat"				=> $this->Serviceawards_model->getAllStat(),
										"arrInstitution"		=> $this->Institutions_model->getInstitutionList(),
										"arrOutcomes"			=> $this->Outcome_model->getAll(),
										"arrPriority"			=> $this->Priority_model->getAll(),
										"arrReports"			=> $this->Serviceawards_model->getServiceReport($scientistId),
										"arrSeminarList" 		=> $this->Serviceawards_model->getSeminars($scientistId, 3),
										"arrTrainingList" 		=> $this->Serviceawards_model->getTrainings($scientistId, 3),
										"arrProjectList" 		=> $this->Serviceawards_model->getProjects($scientistId, 3),
										"arrPaper"				=> $this->Serviceawards_model->getPaper($scientistId, 3),
										"arrMentor"				=> $this->Serviceawards_model->getMentoring($scientistId, 3),
										"arrCurr"				=> $this->Serviceawards_model->getCurriculum($scientistId, 3),
										"arrNetwork"			=> $this->Serviceawards_model->getNetworks($scientistId, 3),
										"arrResearch"			=> $this->Serviceawards_model->getResearch($scientistId, 3),
										"arrOther"				=> $this->Serviceawards_model->getOthers($scientistId, 3),
										"arrParticipants"		=> $this->Serviceawards_model->getParticipants(),
										"arrFields"				=> $this->Serviceawards_model->getFields(),
										"arrCitizenship"		=> $this->Citizenship_model->getAll()
			);
		if($this->arrTemplateData["arrReports"]==null){
			$this->arrTemplateData["arrReports"] = $this->Serviceawards_model->getEmptyReports();
		}
		if($this->input->post('add')=="addSAProfile") {
			$subs = $this->input->post('txtsubs');
			$arrScientstServceAward = array(
										'srv_approval_date'			=>  ($this->input->post('approvalyear')!=null) ? '' : $this->input->post('txtApprovalDate'),
										'srv_approval_yr'			=>  ($this->input->post('approvalyear')!=null) ? $this->input->post('txtapprovalyear') : '',
										'srv_isyearonly'			=>  ($this->input->post('approvalyear')!=null) ? 1 : 0,
										'srv_change_sched'			=>  $this->input->post('cmbSAChangeSched'),
										'srv_sched_approvaldate'	=>  $this->input->post('txtChangeSchedApprovalDate'),
										'srv_glo_id'				=>  $this->input->post('cmbSACountry'),
										'srv_cit_id'				=>  $this->input->post('cmbSACitizenship'),
										'srv_typeofaward'			=>  $this->input->post('cmbSATypeofAward'),
										'srv_cil_id'				=>  $this->input->post('cmbSACouncil'),
										'srv_status'				=>  $this->input->post('cmbstatus'),
										'srv_type_contract'			=>  $this->input->post('cmbTypeOfContract'),
										'srv_ins_id'				=>  implode('|', $this->input->post('cmbInstitutions')),
										'srv_approved_days'			=>  $this->input->post('txtTotalDaysYears'),
										'srv_out_id'				=>  implode('|', $this->input->post('cmbAccOutcomes')),
										'srv_pri_id'				=>  implode('|', $this->input->post('cmbAccPriority')),
										'srv_year_completed'		=>  $this->input->post('txtyrcompleted'),
										'srv_with_erp'				=>  $this->input->post('radwitherp'),
										'srv_erp_reason'			=>  $this->input->post('txterpreason'),
										'srv_erp_date'				=>  $this->input->post('txterpdate'),
										'srv_erp_venue'				=>  $this->input->post('txterpvenue'),
										'srv_erp_participants'		=>  $this->input->post('txterpnopart'),
										'srv_year_repatriated'		=>  $this->input->post('txtyrrepatriated'),
										'updated_at'				=>  date("Y-m-d H:i:s"),
										'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
										 );
			// request for change in schedule
			if($this->input->post('cmbSAChangeSched') == 1){
				$arrScientstServceAward  = array_merge($arrScientstServceAward, array('srv_change_sched_extension' 	=> $this->input->post('txtChangeSchedApprovalDate')));
			}else if($this->input->post('cmbSAChangeSched') == 2){
				$arrScientstServceAward  = array_merge($arrScientstServceAward, array('srv_change_sched_shortening' => $this->input->post('txtChangeSchedApprovalDate')));
			}else if($this->input->post('cmbSAChangeSched') == 3){
				$arrScientstServceAward  = array_merge($arrScientstServceAward, array('srv_change_sched_deferral' 	=> $this->input->post('txtChangeSchedApprovalDate')));
			}else{}

			if($this->input->post('cmbTypeOfContract')==0){
				$phaseDate = explode(';', $this->input->post('txtphasesdate'));
				$arrScientstServceAward1 = array(
										 'srv_cont_startDate'		=> $phaseDate[0],
										 'srv_cont_endDate'			=> $phaseDate[1],
										 );
				$arrScientstServceAward  = array_merge($arrScientstServceAward, $arrScientstServceAward1);
			}
			$resultAddScientist = $this->Serviceawards_model->editServiceAward($scientistId, $arrScientstServceAward);
			$resultAddScientist = $this->Serviceawards_model->deleteContractDates($scientistId);
			$phases_arr = explode(';',$this->input->post('txtphasesdate'));
			$phaseCtr = 0;
			$phase = '';
			$arr_contractDate = array();
			foreach ($phases_arr as $pha) {
				if($phaseCtr++ % 2 == 0){
					$con_dateFrom = $pha;
				}else{
					$con_dateTo = $pha;
					if($con_dateFrom != '' and $con_dateTo != ''){
						$arr_contractDate = array(
						'con_srv_id' 		=> $scientistId,
						'con_iscontinuous' 	=> $this->input->post('cmbTypeOfContract'),
						'con_date_from'		=> $con_dateFrom,
						'con_date_to'		=> $con_dateTo,
						'con_total_number'	=> ''
						);
						print_r($arr_contractDate);
						echo '<br>';
					$arrAddContracts = $this->Serviceawards_model->addContractDates($arr_contractDate);
					$con_dateFrom = '';
					$con_dateTo = '';
					}
				}
			}
			$this->session->set_flashdata('Notification', "Service Awards General Information Updated Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
			redirect(base_url('scientists/editServiceAward/'.$scientistId.'?subs='.$subs));
		}
		if($this->session->flashdata('Notification')!=""): 
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		$this->template->load('template/main_tpl', 'scientists/scientist_sa_add_view', $this->arrTemplateData);
	}

//---------------------------------------------------------View FUNCTIONS--------------------------------------------------------------------------------------------------
	public function editsec(){
		$data = array(
			'srv_processed_by'	 => $this->input->post('cmbusers'),
			'srv_processed_date' => $this->input->post('txtprocessdate'),
			'srv_processed_remarks' => $this->input->post('txtprocessremarks'),
			);
		$this->Serviceawards_model->editServiceAward($this->uri->segment(3), $data);
		redirect(base_url('scientists/editServiceAward/'.$this->uri->segment(3).'/sec?subs='.$_GET['subs']));
	}

	public function view()
	{
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		$this->arrTemplateData = array(
										"arrScientists"			=> $this->scientists_model->getScientists()
										);
		$this->template->load('template/main_tpl', 'scientists/scientist_view', $this->arrTemplateData);
	}
	


//---------------------------------------------------------Edit FUNCTIONS--------------------------------------------------------------------------------------------------


	public function edit()
	{
		$scientistId = $this->uri->segment(3);
		$this->arrTemplateData = array(
										"arrScientist"			=> $this->scientists_model->getScientist($scientistId),
										"arrInstitution"		=> $this->Institutions_model->getInstitutionList(),
										"arrAreaExpertise" 		=> $this->scientists_model->getAreaExpertise(),
										"arrOutcomes"			=> $this->Outcome_model->getAll(),
										"arrPriority"			=> $this->Priority_model->getAll(),
										"arrSpecializations"	=> $this->scientists_model->getSpecializations(),
										"arrProfessions"		=> $this->scientists_model->getProfessions(),
										"arrScientistEducation" => $this->scientists_model->getScientistEducation($scientistId),
										"arrEducationLevels" 	=> $this->scientists_model->getEducationLevels(),
										"arrCountry" 			=> $this->scientists_model->getCountry(),
										"arrCourse" 			=> $this->scientists_model->getCourse(),
										"arrTitles"				=> $this->Title_model->getAll(),
										"arrScientistEmployment"=> $this->scientists_model->getScientistEmployment($scientistId),
										"arrServiceAwards"		=> $this->scientists_model->getScientistServiceAward($scientistId),
										"arrSciTba"				=> $this->scientists_model->getSciTba($scientistId),
										"arrUniversity"			=> $this->Universities_model->getAll(),
										"arrTotalSciAwards"		=> $this->scientists_model->countSciAwards($scientistId)
										);
		// echo '<pre>';
		// die(print_r($this->arrTemplateData['arrServiceAwards']));
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		$this->arrTemplateData["strProfile"] = 	$this->load->view('scientists/scientist_profile_edit_view', $this->arrTemplateData, TRUE);
		$this->arrTemplateData["strEducation"] = 	$this->load->view('scientists/scientist_education_edit_view', $this->arrTemplateData, TRUE);
		$this->arrTemplateData["strEmployment"] = 	$this->load->view('scientists/scientist_employment_edit_view', $this->arrTemplateData, TRUE);
		$this->arrTemplateData["strServiceAwards"] = 	$this->load->view('scientists/scientist_sa_view', $this->arrTemplateData, TRUE);
		$this->template->load('template/main_tpl', 'scientists/scientist_edit_view', $this->arrTemplateData);
	}
	
	
	public function editProfile()
	{
		$this->session->set_flashdata('mode', "profile");
		$scientistId = $this->input->post('txtScientistId');
		
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		if($this->input->post('btnSubmitEditProfile')=="editProfile")
		{
			$imagefilename = '';
			
			$config['upload_path']          = 'data/sci_images/';
            $config['allowed_types']        = 'gif|jpg|png';
            $this->load->library('upload', $config);
            $this->load->library('image_lib');

			if ( ! $this->upload->do_upload('file-image')){
				$error = array('error' => $this->upload->display_errors());
			}else{
				$data = array('upload_data' => $this->upload->data());
				$config =  array(
	              'image_library'   => 'gd2',
	              'source_image'    =>  $this->upload->upload_path.$this->upload->file_name,
	              'maintain_ratio'  =>  TRUE,
	              'width'           =>  250,
	              'height'          =>  250,
	            );
	            $config['overwrite'] = TRUE;
	            $this->image_lib->clear();
	            $this->image_lib->initialize($config);
	            $this->image_lib->resize();

				$imagefilename = $data['upload_data']['file_name'];
			}

			$specialization = "";
			$areaOfExpertise = "";
			$contacts = "";
			foreach ($this->input->post('arrContact') as $contact) {
				$contacts .= $contact.';';
			}
			$emails = "";
			foreach ($this->input->post('arrEmail') as $email) {
				$emails .= $email.';';
			}
			foreach($this->input->post('cmbAreaOfExpertise') as $cmbAreaofExpertise ):
				$areaOfExpertise .= $cmbAreaofExpertise."|";
			endforeach;
			
			foreach($this->input->post('cmbSpecialization') as $cmbSpecialization ):
				$specialization .= $cmbSpecialization."|";
			endforeach;
			$arrScientistData = array( 
										'sci_first_name'		=>  $this->input->post('txtFirstName'),
										'sci_middle_name'		=>  $this->input->post('txtMiddleName'),
										'sci_middle_initial'	=>  $this->input->post('txtMiddleInitial'),
										'sci_last_name'			=>  $this->input->post('txtLastName'),
										'sci_ext_name'			=>  $this->input->post('txtExtName'),
										'sci_title'				=>  $this->input->post('cmbTitle'),
										'sci_birthdate'			=>  $this->input->post('txtBirthDate'),
										'sci_gender'			=>  $this->input->post('cmbGender'),
										'sci_status'			=>  $this->input->post('cmbStatus'),
										'sci_expertise_id'		=>  $areaOfExpertise,
										'sci_specialization_id'	=>  $specialization,
										'sci_profession_id'		=>  $this->input->post('cmbProfession'),
										'sci_license'			=>  $this->input->post('txtProfessionalLicense'),
										'sci_contact'			=>  $contacts,
										'sci_email'				=>  $emails,
										'sci_postal_address'	=>  $this->input->post('txtPostalAddress'),
										'isrepatriated'			=>  ($this->input->post('chkrepatriated') == 'on') ? 1 : 0,
										'updated_at'			=>  date("Y-m-d H:i:s"),
										'lastupdated_by'		=>  $this->session->userdata("sessUserId"),
										 );
				
				if($imagefilename!=''){
					$arrScientistData1 = array(
										 'sci_picturename'		=> 	$imagefilename
										 );
					$arrScientistData  = array_merge($arrScientistData, $arrScientistData1);
				}
			
			 
			$resultEditScientistProfile = $this->scientists_model->editProfile($arrScientistData , $scientistId);
				
			if($resultEditScientistProfile)
			{
				$this->session->set_flashdata('Notification', 'Scientist Profile Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect(base_url('scientists/edit/'. $scientistId));
		
		}
		
			
			
	}
	
	
	public function editEducation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_message('numeric', 'Invalid year graduated');
		$this->form_validation->set_message('less_than', 'Invalid year');
		$scientistId = $this->uri->segment(3);
		$arrScientistEducationData = array( 
											'educ_level_id'			=>  $this->input->post('cmbLevel'),
											'educ_school'			=>  $this->input->post('txtSchool'),
											'educ_countryorigin_id'	=>  $this->input->post('cmbCountry'),
											'educ_course_id'		=>  $this->input->post('cmbCourse'),
											'educ_year_graduated'	=>  $this->input->post('txtYearGraduated'),
											'updated_at'			=>  date("Y-m-d H:i:s"),
											'lastupdated_by'		=>  $this->session->userdata("sessUserId"),
											 );
		$educationDataEditResult = $this->scientists_model->editEducation($arrScientistEducationData,$this->input->post('txteducid_edit'));
		redirect('scientists/edit/'.$scientistId.'/educ');	
	}
	
	public function editEmployment()
	{
		$scientistId = $this->uri->segment(3);
		$arrScientistEmploymentData = array(
										'emp_company'			=>  $this->input->post('txtco'),
										'emp_position'			=>  $this->input->post('txtpos'),
										'emp_countryorigin_id'	=>  $this->input->post('cmbCountry'),
										'emp_datefrom'			=>  $this->input->post('txtdfrom'),
										'emp_dateto'			=>  $this->input->post('txtdto'),
										'ispresent'				=>  ($this->input->post('chkPresent')=='on') ? 1 : 0,
										'updated_at'			=>  date("Y-m-d H:i:s"),
										'lastupdated_by'		=>  $this->session->userdata("sessUserId"),
										 );

		$employmentDataEditResult = $this->scientists_model->editEmployment($arrScientistEmploymentData, $this->input->post('txtempid_edit'));

		redirect('scientists/edit/'.$scientistId.'/employ');
	}

//---------------------------------DELETE FUNCTIONS-----------------------------------------//

	public function deleteScientist()
	{
		$scientistId = $this->input->post('txtempid');
		
		$deleteResult = $this->scientists_model->deleteScientist($scientistId);
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Scientist's Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('scientists');
	}
	
	
	public function deleteScientistEducation()
	{

		$scientistId = $this->uri->segment(3);
		$deleteResult = $this->scientists_model->deleteScientistEducation($_GET['txteducid']);
		redirect('scientists/edit/'.$scientistId.'/educ');
	}

	public function deleteScientistTba()
	{
		$scientistId = $this->uri->segment(3);
		$deleteResult = $this->scientists_model->deleteScientistTba($_GET['txttba']);
		redirect('scientists/editServiceAward/'.$this->uri->segment(3).'/tba?subs='.$_GET['txtsubs']);
	}

	public function deleteScientistEmployment()
	{

		$scientistId = $this->uri->segment(3);
		$deleteResult = $this->scientists_model->deleteScientistEmployment($_GET['txtempid']);
		redirect('scientists/edit/'.$scientistId.'/employ');
	}

	// Api - Education
	public function fetchEducation()
	{
		echo json_encode($this->scientists_model->getScientistEducation($this->uri->segment(3)));
	}
	public function fetchLevel()
	{
		echo json_encode($this->scientists_model->getEducationLevels());
	}
	public function fetchCountry()
	{
		echo json_encode($this->scientists_model->getCountry());
	}
	public function fetchCourse()
	{
		echo json_encode($this->scientists_model->getCourse());
	}
	public function fetchScientists()
	{
		echo json_encode($this->scientists_model->getScientists());
	}

	// Api - Employment
	public function fetchEmployment()
	{
		echo json_encode($this->scientists_model->getScientistEmployment($this->uri->segment(3)));
	}

	// Api - Fetch email address for checking
	public function fetchEmailaddress()
	{
		$exist_email = $_GET['email'];
		$eadds = $this->scientists_model->getScientistEmails();
		$emails = array();
		foreach ($eadds as $email) {
			$email['sci_email'] = str_replace(';', ',', $email['sci_email']); //replace ; with ,
			$em = preg_split('/[\x{ff0c},]/u', $email['sci_email']);
			foreach ($em as $email_address) {
				if (strpos($exist_email, $email_address) !== false){
					// return true
				}
				else{
					array_push($emails, $email_address);
				}
			}
		}
		echo json_encode($emails);
	}

	// Api - Fetch totalcount of sci education
	public function fetchEducCount()
	{
		$totalEd = $this->scientists_model->countSciEduc($this->uri->segment(3));
		echo json_encode($totalEd[0]['educall']);
	}

	// Api - Fetch TBA
	public function fetchTba()
	{
		$arrSciTba = $this->scientists_model->getSciTba($this->uri->segment(3));
		foreach($arrSciTba as $key=>$tba):
			$tba['tba_text'] = cleanHtml($tba['tba_text']);
			$arrSciTba[$key]['tba_text'] = $tba['tba_text'];
		endforeach;
		echo json_encode($arrSciTba);
	}

	// Api - Fetch totalcount of sci education
	public function fetchEmpCount()
	{
		$totalEmp = $this->scientists_model->countSciEmp($this->uri->segment(3));
		echo json_encode($totalEmp[0]['empall']);
	}

	public function bspawardeesList()
	{
		$year = $this->uri->segment(3);
		$this->arrTemplateData = array(
								"arrScientists" 		=> $this->scientists_model->getbspSci($year,0),
								"arrScientists1" 		=> $this->scientists_model->getbspSci($year,1)
								);
		if(checkAccess($_SESSION['sessAccessLevel'], 'limit_scientist')>0){
    		foreach ($this->arrTemplateData['arrScientists'] as $key=>$scientist) {
    			if($scientist['created_by']!=$_SESSION['sessUserId']){
    				unset($this->arrTemplateData['arrScientists'][$key]);
    			}
    		}
  		}
		$this->template->load('template/plain_tpl', 'scientists/bsplist', $this->arrTemplateData);
		// $this->load->view('scientists/bsplist', $this->arrTemplateData);
		
	}

	public function bspawardeesPdf()
	{
		$this->load->model(array('reports/expert_profile'));				
		$this->fpdf = new FPDF();
		$this->fpdf->AliasNbPages();
		$this->fpdf->Open();
		$this->expert_profile->generate();
		echo $this->fpdf->Output();	
	}

	//fetch expertise
	public function getExpertise()
	{
		echo '<pre>';
		$expertiseList = $this->scientists_model->getAreaExpertise();
		foreach ($expertiseList as $expert) {
			$scientist = $this->scientists_model->getScientistByExpertise($expert['exp_id']);
			print_r($scientist);

		}

		die();
	}


}





