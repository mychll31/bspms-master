<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Licenses extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct();  
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('license_model');		
	}

	public function index()
	{ 
		$licenseId = $this->uri->segment(3);
		$this->arrTemplateData['arrLicense'] = $this->license_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['licenseEdit'] = $this->license_model->getLicense($licenseId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'licenses/licenses_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		if($this->input->post('btnSubmitLicense')=="add")
		{
			$arrLicenseData = array( 
										'lic_code'			=>  $this->input->post('txtCode'),
										'lic_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultAddLicense = $this->license_model->addLicense($arrLicenseData);		
			if($resultAddLicense)
			{
				$this->session->set_flashdata('Notification', 'License Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('licenses');
		}
		else
		{
			redirect('licenses');
		}
	}
	
	
	
	
	public function edit()
	{
		if($this->input->post('btnSubmitLicense')=="edit")
		{
			$arrLicenseData = array( 
										'lic_code'	=>  $this->input->post('txtCode'),
										'lic_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultEditLicense	= $this->license_model->editLicense($this->input->post('txtLicId'), $arrLicenseData);		
			if($resultEditLicense)
			{
				$this->session->set_flashdata('Notification', 'License Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('licenses');
		}
		else
		{
			redirect('licenses');	
		}
	}
	
	public function delete()
	{
		$licenseId = $this->uri->segment(3);  
		
		$deleteResult = $this->license_model->deleteLicense($this->input->post('txtempid'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "License Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('licenses');
		
	}

	//fetchdesc
	public function fetchdesc()
	{
		$licid = $this->uri->segment(3);
		$licenses = $this->license_model->getLicDesc($licid);
		$arr_profs = array();
		foreach($licenses as $licenses){
			array_push($arr_licenses, $licenses['lic_desc']);
		}
		echo json_encode($arr_licenses);
	}
	
}