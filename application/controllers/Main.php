<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('login_model');		
	}
	
	public function index()
	{
		$this->template->load('template/main_tpl', 'home/home_view', $this->arrTemplateData);	
		
	}
	
}