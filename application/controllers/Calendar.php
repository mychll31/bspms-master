<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Calendar extends CI_Controller { 

	public function __construct()
	{
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model('calendar_model');
	}

	public function index()
	{
		$this->arrTemplateData = array();
		// $this->arrTemplateData= array(
		// 	"arrUniversities" => $this->Universities_model->getAll() );
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		$this->template->load('template/main_tpl', 'calendar/calendar_view', $this->arrTemplateData);
	}

	public function save(){
		
		$url = $_GET['url'];
		$title = $_GET['title'];
		$start = substr($_GET['sdatetime'],0,10);
		$end = substr($_GET['edatetime'],0,10);
		$t_start = substr($_GET['sdatetime'],11);
		$t_end = substr($_GET['edatetime'],11);
		$allday = $_GET['allday'];
		$loc = $_GET['loc'];
		$remarks = $_GET['remarks'];
		
		$arrCalendar = array(
				'cal_title'				=>	$title,
				'cal_dateStart'			=>	$start == ':' ? NULL : date("Y-m-d", strtotime($start)),
				'cal_dateEnd'			=>	$end == ':' ? NULL : date("Y-m-d", strtotime($end)),
				'cal_timeStart'			=>	$t_start == ':' ? NULL : date("H:i", strtotime($t_start)),
				'cal_timeEnd'			=>	$t_end == ':' ? NULL : date("H:i", strtotime($t_end)),
				'cal_isAllday'			=>	$allday,
				'cal_url'				=>	$url,
				'cal_location'			=>	$loc,
				'cal_remarks'			=>	$remarks,
				'cal_addedby'			=>	$_SESSION['sessUserId'],
				'cal_addedDate'			=>	date("Y-m-d H:i:s", time())
		);
		$resEvent = $this->calendar_model->addEvent($arrCalendar);
		echo $resEvent;
	}

	public function update_calendarDate(){
		//update start date of the event
		$evid = $_GET['evid'];
		$url = $_GET['url'];
		$title = $_GET['title'];
		$start = substr($_GET['start'],0,10);
		$end = substr($_GET['end'],0,10);
		$t_start = substr($_GET['start'],11);
		$t_end = substr($_GET['end'],11);
		$allday = $_GET['allday'];
		
		$arrCalendar = array(
				'cal_title'				=>	$title,
				'cal_dateStart'			=>	$start == ':' ? NULL : date("Y-m-d", strtotime($start)),
				'cal_dateEnd'			=>	$end == ':' ? NULL : date("Y-m-d", strtotime($end)),
				'cal_timeStart'			=>	$t_start == ':' ? NULL : date("H:i", strtotime($t_start)),
				'cal_timeEnd'			=>	$t_end == ':' ? NULL : date("H:i", strtotime($t_end)),
				'cal_isAllday'			=>	$allday,
				'cal_url'				=>	$url,
				'cal_addedby'			=>	$_SESSION['sessUserId'],
				'cal_addedDate'			=>	date("Y-m-d H:i:s", time())
		);

		$resEvent = $this->calendar_model->updateEvent($evid, $arrCalendar);
		print_r($arrCalendar);
	}

	public function getAllEvents() {
		//fetch all the events
		$events = $this->calendar_model->getAllEvents();
		$arrEvents = array();
		foreach ($events as $event) {
			$e = array();
			if($event['cal_dateStart'] != $event['cal_dateEnd']) 
				$event['cal_dateEnd'] = date('Y-m-d', strtotime(date("Y-m-d", strtotime($event['cal_dateEnd'])) . " +1 day"));
			$e['id'] = $event['cal_id'];
			$e['title'] = $event['cal_title'];
			$e['url'] = ''; //$event['cal_url'];
			$e['start'] = $event['cal_dateStart'];
			$e['end'] = $event['cal_dateEnd'];
			$e['starttime'] = $event['cal_timeStart'] == null ? '' : date('h:i A', strtotime($event['cal_timeStart']));
			$e['startdate'] = $event['cal_dateStart'];
			$e['endtime'] = $event['cal_timeEnd'] == null ? '' : date('h:i A', strtotime($event['cal_timeEnd']));
			$e['enddate'] = $event['cal_dateEnd'];
			$e['allDay'] = $event['cal_isAllday'] == 0 ? false : true;
			array_push($arrEvents, $e);
		}
		// pthis($arrEvents);
		echo json_encode($arrEvents);
	}

	public function getLastInsertedEvent($calid) {
		//fetch all the events
		$events = $this->calendar_model->getLastInsertedEvent($calid);
		$arrEvents = array();
		foreach ($events as $event) {
			$e = array();
			if($event['cal_dateStart'] != $event['cal_dateEnd']) 
				$event['cal_dateEnd'] = date('Y-m-d', strtotime(date("Y-m-d", strtotime($event['cal_dateEnd'])) . " +1 day"));
			$e['id'] = $event['cal_id'];
			$e['title'] = $event['cal_title'];
			$e['url'] = ''; //$event['cal_url'];
			$e['start'] = $event['cal_dateStart'];
			$e['end'] = $event['cal_dateEnd'];
			$e['starttime'] = $event['cal_timeStart'] == null ? '' : date('h:i A', strtotime($event['cal_timeStart']));
			$e['startdate'] = $event['cal_dateStart'];
			$e['endtime'] = $event['cal_timeEnd'] == null ? '' : date('h:i A', strtotime($event['cal_timeEnd']));
			$e['enddate'] = $event['cal_dateEnd'];
			$e['allDay'] = $event['cal_isAllday'] == 0 ? false : true;
			array_push($arrEvents, $e);
		}
		// pthis($arrEvents);
		echo json_encode($arrEvents);
	}

	public function deleteEvent() {
		// $this->calendar_model->deleteEvent($_GET['evid']);
		$arrCalendar = array(
				'cal_isRemove'			=>	1
		);
		$resEvent = $this->calendar_model->updateEvent($_GET['evid'], $arrCalendar);
	}

}
