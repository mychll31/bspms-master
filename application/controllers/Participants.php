<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participants extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct();  
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_pacticipant'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('participant_model');		
	}

	public function index() 
	{
		$participantId = $this->uri->segment(3);
		$this->arrTemplateData['arrParticipant'] = $this->participant_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['participantEdit'] = $this->participant_model->getParticipant($participantId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'participants/participants_view', $this->arrTemplateData);
 	
	}
	 
	public function add()
	{
			$arrParticipantData = array( 
										// 'par_code'	=>  $this->input->post('txtCode'),
										'par_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultAddParticipant = $this->participant_model->addParticipant($arrParticipantData);		
			if($resultAddParticipant)
			{
				$this->session->set_flashdata('Notification', 'Participant Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('participants');
	}
	
	public function edit()
	{
			$arrParticipantData = array( 
										// 'par_code'	=>  $this->input->post('txtCode'),
										'par_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultEditParticipant	= $this->participant_model->editParticipant($this->input->post('txtSpeId'), $arrParticipantData);		
			if($resultEditParticipant)
			{
				$this->session->set_flashdata('Notification', 'Participant Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('participants');
	}
	
	public function delete()
	{
		$deleteResult = $this->participant_model->deleteParticipant($this->input->post('txtdelpart'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Participant Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('participants');
		
	}
	
	public function fetchDesc()
	{
		echo json_encode($this->participant_model->getAll());
	}

	public function fetchvalidParticipant()
	{
		$validPart = '';
		foreach ($this->participant_model->getValidParticipant() as $part) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validPart.=strtolower($part['valpart']).'|';
		}
		echo json_encode($validPart);
	}

	public function getPartic($par_id)
	{
		echo json_encode($this->participant_model->getParticipant($par_id));
	}
	
}