<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Institutions extends CI_Controller { 
 
	public function __construct()   
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model('institutions_model');
	}

	public function index() 
	{
		$institutionId = $this->uri->segment(3);
//		$this->arrTemplateData['arrInstitutions'] = $this->institutions_model->getAll(); // calling Post model method getPosts()
		$this->arrTemplateData= array(
										"arrInstitutions" 		=> $this->institutions_model->getAll(),
										"arrInstitutionTypes" 	=> $this->institutions_model->getInstitutionTypes(),
										"arrLocalRegions"	 	=> $this->institutions_model->getLocalRegions() 
										);
		$local_regions = array(
                    ''   => 'Choose Option',
                    '1'  => 'NCR',
                    '2'  => 'Region I',
                    '3'  => 'CAR',
                    '4'  => 'Region II',
                    '5'  => 'Region III',
                    '6'  => 'Region IV-A',
                    '7'  => 'Region IV-B',
                    '8'  => 'Region V',
                    '9'  => 'Region VI',
                    '10' => 'Region VII',
                    '11' => 'Region VIII',
                    '12' => 'Region IX',
                    '13' => 'Region X',
                    '14' => 'Region XI',
                    '15' => 'Region XII',
                    '16' => 'Region XIII',
                    '17' => 'Region NIR',
                    '18' => 'ARMM',
                    );

		$regions = $this->institutions_model->getInstitutions($institutionId);

		foreach ($regions as $key => $reg) {
			$regions[$key]['loc_reg_name'] = $local_regions[$reg['ins_loc_id']];
		}
		$this->arrTemplateData['institutionEdit'] = $regions;

		if($this->session->flashdata('Notification')!=""): 
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'institutions/institutions_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrInstitutionData = array(
									'ins_itype_id'	=>  $this->input->post('cmbInsType'),
									'ins_loc_id'	=>  $this->input->post('cmbLocRegion'),
									'ins_code' 		=>  $this->input->post('txtCode'),
									'ins_desc'		=>  $this->input->post('txtDesc')
									);
		$resultAddInstitution = $this->institutions_model->addInstitution($arrInstitutionData);
		$this->session->set_flashdata('Notification', "Institution Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		redirect('institutions');
	}
	
	public function edit()
	{
		$institutionId = $this->input->post('txtinsid');
		$arrInstitutionData = array(
									'ins_itype_id'	=>  $this->input->post('cmbInsType'),
									'ins_loc_id'	=>  $this->input->post('cmbLocRegion'),
									'ins_code'	=>  $this->input->post('txtCode'),
									'ins_desc'	=>  $this->input->post('txtDesc')
									);
		$resultEditInstitution	= $this->institutions_model->editInstitution($institutionId, $arrInstitutionData);		
		$this->session->set_flashdata('Notification', "Institution Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		redirect('institutions');
	}

	public function delete()
	{
		$institutionId = $this->input->post('target_id');
		$deleteResult = $this->institutions_model->deleteInstitution($institutionId);
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Institution Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success'); 
		}
		redirect('institutions');
		
	}

	//fetch Institution
	public function fetchInst()
	{
		echo json_encode($this->institutions_model->getAll());
	}


	public function fetchInstTypes()
	{
		echo json_encode($this->institutions_model->getInstitutionTypes());
	}
	
	public function fetchValidhostInst()
	{
		$validhost = '';
		foreach ($this->institutions_model->getValidHost() as $host) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validhost.=strtolower($host['valhost']).'|';
		}
		echo json_encode($validhost);
	}

	public function getInst($ins_id)
	{
		echo json_encode($this->institutions_model->getInstitutions($ins_id));
	}

}