<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Education extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('education_model');		
	}

	public function index()
	{
		$educationId = $this->uri->segment(3);
		$this->arrTemplateData['arrEducation'] = $this->education_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['educationEdit'] = $this->education_model->getEducation($educationId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'education/education_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrEducationData = array(
									'elev_code'	=> $this->input->post('txtCode'),
									'elev_desc'	=> $this->input->post('txtDesc')
									);

		$resultAddEducation = $this->education_model->addEducation($arrEducationData);
		if($resultAddEducation) {
			$this->session->set_flashdata('Notification', 'Educational Level Saved Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('education');
	}
	
	public function edit()
	{
		$arrEducationData = array(
									'elev_code'	=>  $this->input->post('txtCode'),
									'elev_desc'	=>  $this->input->post('txtDesc')
									);

		$resultEditEducation = $this->education_model->editEducation($this->input->post('txtSpeId'), $arrEducationData);
		if($resultEditEducation) {
			$this->session->set_flashdata('Notification', 'Educationalal Level Updated Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('education');

	}
	
	public function delete()
	{
		$deleteResult = $this->education_model->deleteEducation($this->input->post('txtdeledu'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Educational Level Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('education');
		
	}
	
	public function fetchdesc() 
	{
		echo json_encode($this->education_model->getAll());
	}

	public function getEducations($educ_id)
	{
		echo json_encode($this->education_model->getEducation($educ_id));
	}

	public function fetchValidEduc()
	{
		$valideduc = '';
		foreach ($this->education_model->getValidEduc() as $educ) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$valideduc.=strtolower($educ['valeduc']).'|';
		}
		echo json_encode($valideduc);
	}
	
	
}