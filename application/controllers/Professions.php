<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professions extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct();  
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('profession_model');		
	}

	public function index()
	{
		$professionId = $this->uri->segment(3);
		$this->arrTemplateData['arrProfession'] = $this->profession_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['professionEdit'] = $this->profession_model->getProfession($professionId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'professions/professions_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
			$arrProfessionData = array( 
										// 'prof_code'			=>  $this->input->post('txtCode'),
										'prof_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultAddProfession = $this->profession_model->addProfession($arrProfessionData);		
			if($resultAddProfession)
			{
				$this->session->set_flashdata('Notification', 'Profession Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('professions');
	}
	
	public function edit()
	{
			$arrProfessionData = array( 
										// 'prof_code'	=>  $this->input->post('txtCode'),
										'prof_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultEditProfession	= $this->profession_model->editProfession($this->input->post('txtProfId'), $arrProfessionData);		
			if($resultEditProfession)
			{
				$this->session->set_flashdata('Notification', 'Profession Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('professions');
	}
	
	public function delete()
	{
		$professionId = $this->uri->segment(3);  
		
		$deleteResult = $this->profession_model->deleteProfession($this->input->post('txtdelpro'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Profession Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('professions');
		
	}

	public function fetchdesc()
	{
		echo json_encode($this->profession_model->getAll());
	}
	
	public function fetchvalidProf()
	{
		$validprof = '';
		foreach ($this->profession_model->getValidProf() as $prof) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validprof.=strtolower($prof['valprof']).'|';
		}
		echo json_encode($validprof);
	}

	public function getProf($prof_id)
	{
		echo json_encode($this->profession_model->getProfession($prof_id));
	}
}
