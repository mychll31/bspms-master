<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  Users extends CI_Controller { 

	public function __construct()   
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession();
		$this->load->model(array('user_model', 'Council_model'));
		$this->load->library('encrypt');
	}

	public function index()
	{

		$usersId = $this->uri->segment(3);
		$this->arrTemplateData['arrUsers'] = $this->user_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['usersEdit'] = $this->user_model->getUsers($usersId); // get per data for editing
   		$this->arrTemplateData['councils'] = $this->Council_model->getAll();
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'users/users_view', $this->arrTemplateData);
		
 	
	}
	
	/*
$plain_text = 'This is a plain-text message!';
$ciphertext = $this->encryption->encrypt($plain_text);

// Outputs: This is a plain-text message!
echo $this->encryption->decrypt($ciphertext);	
	
	*/
	public function add() 
	{
		if($this->input->post('btnSubmitUsers')=="add") 
		{
			if ($_SESSION['sessAccessLevel'] != 1) {
			// Display error message
				$this->session->set_flashdata('Notification', 'You are not allowed to add user account!');
				$this->session->set_flashdata('NotificationBox', 'warning');
				redirect('users');
			}
			
			$arrUsersData = array(
								'usr_fname' =>  ltrim($this->input->post('txtfname')),
								'usr_mname' =>  ltrim($this->input->post('txtmname')),
								'usr_lname' =>  ltrim($this->input->post('txtlname')),
								'usr_user_login' =>  ltrim($this->input->post('txtUserLogin')),
								'usr_user_passwd' => $this->encrypt->encode($this->input->post('txtUserPassword')),
								'usr_user_level' =>  $this->input->post('cmbUserLevel'),
								'usr_council' =>  $this->input->post('cmbCouncil')); 
			
			$resultAddUsers= $this->user_model->addUsers($arrUsersData);		
			if($resultAddUsers)
			{
				$this->session->set_flashdata('Notification', 'User Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('users');
		}
		else
		{
			redirect('users');
		}
	} 
	
	public function edit()
	{
		if($this->input->post('btnSubmitUsers')=="edit")
		{
			$tmpUserId =  $this->input->post('txtUsrId');
			if($_SESSION['sessUserId'] == $tmpUserId and $this->input->post('active')==null){
				$this->session->set_flashdata('Notification', 'You are not allowed to deactivate your own account! Please contact Administrator.');
				$this->session->set_flashdata('NotificationBox', 'warning');
				redirect('users');
			}else{
				if ($_SESSION['sessAccessLevel'] != 1 && $_SESSION['sessUserId'] != $tmpUserId )  {
				// Display error message
					$this->session->set_flashdata('Notification', 'You are not allowed to edit user account!');
					$this->session->set_flashdata('NotificationBox', 'warning');
					redirect('users');
				}
				$isactive = 0;
				if($this->input->post('active')!=null){
					$isactive = 1;
				}
				$arrUsersData = array(
								'usr_fname' =>  ltrim($this->input->post('txtfname')),
								'usr_mname' =>  ltrim($this->input->post('txtmname')),
								'usr_lname' =>  ltrim($this->input->post('txtlname')),
								'usr_user_login' =>  ltrim($this->input->post('txtUserLogin')),
								'usr_user_passwd' => $this->encrypt->encode($this->input->post('txtUserPassword')),
								'usr_user_level' =>  $this->input->post('cmbUserLevel'),
								'usr_isactive' =>  $isactive,
								'usr_council' =>  $this->input->post('cmbCouncil'));

				$resultEditUsers	= $this->user_model->editUsers($this->input->post('txtUsrId'), $arrUsersData);		
				if($resultEditUsers)
				{
					$this->session->set_flashdata('Notification', 'User Updated Successfully');
					$this->session->set_flashdata('NotificationBox', 'success');
				}
				redirect('users');
			}
		}
		else
		{
			redirect('users');	
		}
	}
	 
	public function delete() 
	{
		// deleting own account
		if ($_SESSION['sessUserId'] == $this->input->post('txtempid')) {
			$this->session->set_flashdata('Notification', 'You are not allowed to delete this user account!');
			$this->session->set_flashdata('NotificationBox', 'danger');
			redirect('users');
		}

		// deleting admin
		if ($this->input->post('txtempid') == 1) {
			$this->session->set_flashdata('Notification', 'Unable to delete Super Admin Account!');
			$this->session->set_flashdata('NotificationBox', 'danger');
			redirect('users');
		}
		
		// deleting user account with data
		if (checkUserKey($this->input->post('txtempid'))){
			$this->session->set_flashdata('Notification', 'Unable to delete this account, use Active or Inactive account instead.');
			$this->session->set_flashdata('NotificationBox', 'warning');
			redirect('users');
		}

		$deleteResult = $this->user_model->deleteUsers($this->input->post('txtempid'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "User Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('users');
		
	}

	//fetchuser
	public function fetchuser()
	{
		$userid = $this->uri->segment(3);
		$username = $this->user_model->getUsername($userid);
		$users = array();
		foreach($username as $user){
			array_push($users, $user['usr_user_login']);
		}
		echo json_encode($users);
	}

	public function editSumProfile(){

		$arrScientistData = array(
							'usr_fname'	=> $this->input->post('fname'),
							'usr_mname'	=> $this->input->post('mname'),
							'usr_lname'	=> $this->input->post('lname'));
		if($this->input->post('pword')!=''){
			$arrScientistData['usr_user_passwd'] = $this->encrypt->encode($this->input->post('pword'));
		}
		$resultEditScientistProfile = $this->user_model->editUsers($this->input->post('authid'), $arrScientistData);
		$this->session->set_userdata(array(
			'sessFname' => ucfirst($this->input->post('fname')),
			'sessMname' => strtoupper($this->input->post('mname')),
			'sessLname' => ucfirst($this->input->post('lname')),
			));
	}

	/*
	ACCESS CONTROL LIST
	***/

	public function acl(){
		$this->template->load('template/main_tpl', 'users/acl_view');
		
	}

	
}
