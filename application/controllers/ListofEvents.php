<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class  ListofEvents extends CI_Controller { 

	public function __construct()
	{
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		$this->load->model(array('Calendar_model'));
	}

	public function index()
	{
		$events = array();
		$events['events'] = $this->Calendar_model->getEventList();
		$this->load->view('calendar/listofevents', $events);
	}

	public function checkdata(){
		phpinfo();
	}
}
