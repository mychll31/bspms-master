<?php


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {	
	 
	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> $this->uri->segment(1),
			'form_title'      	=> SYSTEM_NAME
		);
	}
	
	public function index()
	{
		//$this->template->load('template/main_tpl', 'home/home_view', $this->arrTemplateData);	
		$this->session->sess_destroy();				
		redirect(base_url(''), 'refresh');	
	}
}