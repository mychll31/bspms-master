<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activity extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('activity_model');		
	}

	public function index()
	{
		$citizenId = $this->uri->segment(3);
		$this->arrTemplateData['arrActivity'] = $this->activity_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['activityEdit'] = $this->activity_model->getActivity($activityId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'activity/activity_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		if($this->input->post('btnSubmitActivity')=="add")
		{
			$arrCitizenshipData = array( 
										'cit_type'			=>  $this->input->post('cmbType'),
										'cit_particulars'	=>  $this->input->post('txtNationality')
										
										 );
			
			$resultAddCitizenship = $this->citizenship_model->addCitizenship($arrCitizenshipData);		
			if($resultAddCitizenship)
			{
				$this->session->set_flashdata('Notification', 'Citizenship Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('citizenship');
		}
		else
		{
			redirect('citizenship');
		}
	}
	
	
	
	
	public function edit()
	{
		if($this->input->post('btnSubmitCitizen')=="edit")
		{
			$arrCitizenshipData = array( 
										'cit_type'			=>  $this->input->post('cmbType'),
										'cit_particulars'	=>  $this->input->post('txtNationality')
										
										 );
			
			$resultEditCitizenship = $this->citizenship_model->editCitizenship($this->input->post('txtCitId'), $arrCitizenshipData);		
			if($resultEditCitizenship)
			{
				$this->session->set_flashdata('Notification', 'Citizenship Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('citizenship');
		}
		else
		{
			redirect('citizenship');	
		}
	}
	
	public function delete()
	{
		$citizenshipId = $this->uri->segment(3);
		
		$deleteResult = $this->citizenship_model->deleteCitizenship($citizenshipId);
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Citizenship Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('citizenship');
		
	}
	
}