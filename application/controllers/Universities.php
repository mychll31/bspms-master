<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Universities extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model(array('Universities_model'));
	}

	public function index()
	{
		$this->arrTemplateData= array(
			"arrUniversities" => $this->Universities_model->getAll() );
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		$this->template->load('template/main_tpl', 'universities/university_view', $this->arrTemplateData);
	}

	public function add()
	{
		$arrTemplateData = array(
								'uni_abbrv'	=> ltrim($this->input->post('txtabbrv')),
								'uni_name'	=> ltrim($this->input->post('txtname'))
								);

		$resultAddUniversity = $this->Universities_model->addUniversity($arrTemplateData);
		if($resultAddUniversity) {
			$this->session->set_flashdata('Notification', 'University Saved Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('universities');
	}

	public function edit()
	{
		$uniid = $this->input->post('txtuniid');
		$arrTemplateData = array(
									'uni_abbrv'	=>  $this->input->post('txtabbrv'),
									'uni_name'	=>  $this->input->post('txtname'),
									);
		$resultEditInstitution	= $this->Universities_model->editUniversity($uniid, $arrTemplateData);		
		$this->session->set_flashdata('Notification', "University Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		redirect('universities');
	}
	
	public function delete()
	{
		$uniid = $this->uri->segment(3);  
		
		$deleteResult = $this->Universities_model->deleteUniversity($this->input->post('txtdeluni'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "University Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success'); 
		}
		redirect('universities');
		
	}

	public function getUniversity($uniid)
	{
		echo json_encode($this->Universities_model->getUniversity($uniid));
	}

	public function fetchvalidUnis() {
		$validUnis = '';
		foreach ($this->Universities_model->getValidUnis() as $unis) {
			$validUnis.=strtolower($unis['valunis']).'|';
		}
		echo json_encode($validUnis);
	}

}