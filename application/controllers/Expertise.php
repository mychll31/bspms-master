<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expertise extends CI_Controller { 

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('expertise_model');
	}

	public function index()
	{
		$expertiseId = $this->uri->segment(3);
		$this->arrTemplateData['arrExpertise'] = $this->expertise_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['expertiseEdit'] = $this->expertise_model->getExpertise($expertiseId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'expertise/expertise_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrExpertiseData = array(
								// 'exp_code'	=>  $this->input->post('txtCode'),
								'exp_desc'	=>  $this->input->post('txtDesc')
								);

		$resultAddExpertise = $this->expertise_model->addExpertise($arrExpertiseData);
		$this->session->set_flashdata('Notification', 'Expertise Saved Successfully');
		$this->session->set_flashdata('NotificationBox', 'success');
		redirect('expertise');
	}
	
	public function edit()
	{
		$arrExpertiseData = array(
								// 'exp_code'	=>  $this->input->post('txtCode'),
								'exp_desc'	=>  $this->input->post('txtDesc')
								);
		$resultEditExpertise = $this->expertise_model->editExpertise($this->input->post('txtSpeId'), $arrExpertiseData);
		$this->session->set_flashdata('Notification', 'Expertise Updated Successfully');
		$this->session->set_flashdata('NotificationBox', 'success');
		redirect('expertise');
	}
	
	public function delete()
	{  
		$deleteResult = $this->expertise_model->deleteExpertise($this->input->post('txtTempId'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Expertise Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success'); 
		}
		redirect('expertise');
		
	}
	
	public function fetchDesc() 
	{
		echo json_encode($this->expertise_model->getAll());
	}
	
	public function fetchValidExpert() 
	{
		$expertise = $this->expertise_model->getAll();
		$arr_expertise = '';
		foreach ($expertise as $expert) {
			$arr_expertise = $arr_expertise.'|'.strtolower($expert['exp_desc']);
		}
		echo json_encode($arr_expertise);
	}

	public function getExpertise($exp_id) 
	{
		$expertise = $this->expertise_model->getExpertise($exp_id);
		echo json_encode($expertise);
	}
	
}