<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Serviceawards extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('Serviceawards_model');
		$this->load->helper('date');
	}

	// -------------- Begin Seminar --------------//
	public function fetchSeminar()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getSeminars($scientistId, $this->uri->segment(4)));
	}

	public function fetchFiles()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getFiles($scientistId));
	}

	public function fetchparticipantstype()
	{
		echo json_encode($this->Serviceawards_model->getParticipants());
	}

	public function fetchfields()
	{
		echo json_encode($this->Serviceawards_model->getFields());
	}

	// Action - Seminar
	public function addSeminars()
	{
		$scientistId = $this->uri->segment(3);
		$targetPart = ($this->input->post('txttarget')=='') ? 0 : $this->input->post('txttarget');
		$arrSeminarData = array( 
								'sem_srv_id'				=>  $scientistId,
								'sem_field'					=>  $this->input->post('cmbfield'),
								'sem_title'					=>  $this->input->post('txttitle'),
								'sem_participantstype'		=>  $this->input->post('cmbpart'),
								'sem_targetparticipants'	=>  $targetPart,
								'sem_output'				=>  $this->input->post('txtoutput'),
								'sem_date'					=>	$this->input->post('txtsemDate'),
								'sem_venue'					=>	$this->input->post('txtvenue'),
								'sem_actualparticipants'	=>	$this->input->post('txtactual'),
								'sem_target'				=>	$this->input->post('istarget'),
								'created_at'			=>  date("Y-m-d H:i:s"),
								'created_by'			=>  $this->session->userdata("sessUserId")
								);
		$resultaddSeminar = $this->Serviceawards_model->addSeminar($arrSeminarData);
		// bgein upload file
		$total = count($_FILES['file-upload']['name']);
		$target_dir = "attachments/";

		for($i=0; $i<$total; $i++) {
			$tmpFilePath = $_FILES['file-upload']['tmp_name'][$i];
			if ($tmpFilePath != ""){
				$newFilePath = $target_dir . basename($_FILES['file-upload']['name'][$i]);
				$arr_file = explode('.', $_FILES['file-upload']['name'][$i]);
				$file_ext = end($arr_file); //filetype
				$arrFile = array(
								'attch_sem_id'				=> $resultaddSeminar,
								'attch_srv_id'				=> $scientistId,
								'attch_filename'			=> $_FILES['file-upload']['name'][$i],
								'attch_filetype'			=> $file_ext,
								'attch_path'				=> $newFilePath,
								'attch_uploadedby'			=> $this->session->userdata('sessUserId'),
								'attch_uploadeddate'		=> date('Y-m-d H:i:s',now())

					);
				if(move_uploaded_file($tmpFilePath, $newFilePath)) {}
				$resultaddFile = $this->Serviceawards_model->addFile($arrFile);
			}
		}
		$this->session->set_flashdata('Notification', "Seminar Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		// end upload file
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/sem_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/sem?subs='.$_GET['subs']);
		}
	}

	public function editSeminars()
	{
		$removefiles = explode(';', $this->input->post('removefiles'));
		foreach ($removefiles as $file_r) {
			if($file_r!=''){
				$resultDelete = $this->Serviceawards_model->deleteFile($file_r);
			}
		}
		$scientistId = $this->uri->segment(3);
		$targetPart = ($this->input->post('txttarget')=='') ? 0 : $this->input->post('txttarget');
		$arrSeminarData = array(
								'sem_field'					=>  $this->input->post('cmbfield'),
								'sem_title'					=>  $this->input->post('txttitle'),
								'sem_participantstype'		=>  $this->input->post('cmbpart'),
								'sem_targetparticipants'	=>  $targetPart,
								'sem_output'				=>  $this->input->post('txtoutput'),
								'sem_date'					=>	$this->input->post('txtsemDate'),
								'sem_venue'					=>	$this->input->post('txtvenue'),
								'sem_actualparticipants'	=>	$this->input->post('txtactual'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		$resulteditSeminar = $this->Serviceawards_model->editSeminar($arrSeminarData, $this->input->post('semid_edit'));
		// begin upload file
		$total = count($_FILES['file-upload']['name']);
		$target_dir = "attachments/";

		for($i=0; $i<$total; $i++) {
			$tmpFilePath = $_FILES['file-upload']['tmp_name'][$i];
			if ($tmpFilePath != ""){
				$newFilePath = $target_dir . basename($_FILES['file-upload']['name'][$i]);
				$arr_file = explode('.', $_FILES['file-upload']['name'][$i]);
				$file_ext = end($arr_file); //filetype
				$arrFile = array(
								'attch_sem_id'				=> $this->input->post('semid_edit'),
								'attch_srv_id'				=> $scientistId,
								'attch_filename'			=> $_FILES['file-upload']['name'][$i],
								'attch_filetype'			=> $file_ext,
								'attch_path'				=> $newFilePath,
								'attch_uploadedby'			=> $this->session->userdata('sessUserId'),
								'attch_uploadeddate'		=> date('Y-m-d H:i:s',now())

					);
				if(move_uploaded_file($tmpFilePath, $newFilePath)) {}
				$resultaddFile = $this->Serviceawards_model->addFile($arrFile);
			}
		}
		$this->session->set_flashdata('Notification', "Seminar Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		// end upload file
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/sem_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/sem?subs='.$_GET['subs']);
		}
	}

	// -------------- End Seminar --------------//


	// -------------- Begin Training --------------//
	public function fetchTrainings()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getTrainings($scientistId, $this->uri->segment(4)));
	}

	// Action - Training
	public function addTraining()
	{
		// die(print_r($this->input->post('istarget')));
		$tradate = ($this->input->post('istarget')==0) ? '0000-00-00' : $this->input->post('txtsemDate');
		$travenue = ($this->input->post('istarget')==0) ? '' : $this->input->post('txtvenue');
		$targetP = ($this->input->post('istarget')==0) ? $this->input->post('txttarget') : 0;
		$actualP = ($this->input->post('istarget')==1) ? $this->input->post('txtactual') : 0;
		$scientistId = $this->uri->segment(3);
		$arrTrainingData = array( 
								'tra_srv_id'				=>  $scientistId,
								'tra_field'					=>  $this->input->post('cmbfield'),
								'tra_title'					=>  $this->input->post('txttitle'),
								'tra_date'					=>  $tradate,
								'tra_venue'					=>  $travenue,
								'tra_participantstype'		=>  $this->input->post('cmbpart'),
								'tra_targetparticipants'	=>  $targetP,
								'tra_actualparticipants'	=>  $actualP,
								'tra_output'				=>  $this->input->post('txtoutput'),
								'tra_target'				=>  $this->input->post('istarget'),
								'created_at'				=>  date("Y-m-d H:i:s"),
								'created_by'				=>  $this->session->userdata("sessUserId")
								);
		$resultaddTraining = $this->Serviceawards_model->addTraining($arrTrainingData);
		// bgein upload file
		$total = count($_FILES['file-upload']['name']);
		$target_dir = "attachments/";

		for($i=0; $i<$total; $i++) {
			$tmpFilePath = $_FILES['file-upload']['tmp_name'][$i];
			if ($tmpFilePath != ""){
				$newFilePath = $target_dir . basename($_FILES['file-upload']['name'][$i]);
				$arr_file = explode('.', $_FILES['file-upload']['name'][$i]);
				//TODO::ADD file_exists("test.txt")
				$file_ext = end($arr_file); //filetype
				$arrFile = array(
								'attch_sem_id'				=> $resultaddTraining,
								'attch_srv_id'				=> $scientistId,
								'attch_filename'			=> $_FILES['file-upload']['name'][$i],
								'attch_filetype'			=> $file_ext,
								'attch_path'				=> $newFilePath,
								'attch_uploadedby'			=> $this->session->userdata('sessUserId'),
								'attch_uploadeddate'		=> date('Y-m-d H:i:s',now())

					);
				if(move_uploaded_file($tmpFilePath, $newFilePath)) {}
				$resultaddFile = $this->Serviceawards_model->addFile($arrFile);
			}
		}
		// end upload file
		$this->session->set_flashdata('Notification', "Training Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/tra_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/tra?subs='.$_GET['subs']);
		}
	}

	public function editTraining()
	{
		$scientistId = $this->uri->segment(3);
		$arrTrainingData = array(
								'tra_field'					=> $this->input->post('cmbfield'),
								'tra_title'					=> $this->input->post('txttitle'),
								'tra_participantstype'		=> $this->input->post('cmbpart'),
								'tra_targetparticipants'	=> $this->input->post('txttarget'),
								'tra_output'				=> $this->input->post('txtoutput'),
								'tra_target'				=> $this->input->post('istarget'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		if($this->input->post('istarget') == 1){
			$arrTrainingData = array(
								'tra_date'					=> $this->input->post('txtsemDate'),
								'tra_venue'					=> $this->input->post('txtvenue'),
								'tra_actualparticipants'	=> $this->input->post('txtactual'),
								'tra_actualparticipants'	=> $this->input->post('txtactual'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		}
		$resulteditSeminar = $this->Serviceawards_model->editTraining($arrTrainingData, $this->input->post('semid_edit'));
		$this->session->set_flashdata('Notification', "Training Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/tra_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/tra?subs='.$_GET['subs']);
		}
	}

	// -------------- End Training --------------//

	// -------------- Begin Project --------------//
	public function fetchProjects()
	{
		$scientistId = $this->uri->segment(3);
		$proj_status = array(
							 '' => 'Choose Option',
                             1  => 'Proposal for Funding',
                             2  => 'Ongoing',
                             3  => 'Continuing',
                             4  => 'Completed',
                             5  => 'Others'
                           );
		$projects = $this->Serviceawards_model->getProjects($scientistId, $this->uri->segment(4));
		foreach ($projects as $key => $proj) {
			$projects[$key]['prj_status_name'] = $proj_status[$proj['prj_status']];
		}
		echo json_encode($projects);
	}

	public function fetchAgency()
	{
		echo json_encode($this->Serviceawards_model->getImplentingAgency());
	}

	// Action - Project
	public function addProject()
	{
		$scientistId = $this->uri->segment(3);
		$arrProjectData = array( 
								'prj_srv_id'		=>  $scientistId,
								'prj_field'			=>  $this->input->post('cmbfield'),
								'prj_title'			=>  $this->input->post('txttitle'),
								'prj_objective'		=>  $this->input->post('txtobjectives'),
								'prj_agency'		=>  $this->input->post('cmbagency'),
								'prj_status'		=>  $this->input->post('txtstat'),
								'prj_contributions'	=>  $this->input->post('txtcontri'),
								'prj_target'		=>  $this->input->post('istarget'),
								'created_at'		=>  date("Y-m-d H:i:s"),
								'created_by'		=>  $this->session->userdata("sessUserId")
								);
		$resultaddProject = $this->Serviceawards_model->addProject($arrProjectData);
		$this->session->set_flashdata('Notification', "Project Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/proj_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/proj?subs='.$_GET['subs']);
		}
	}

	public function editProject()
	{
		$scientistId = $this->uri->segment(3);
		$arrProjectData = array(
								'prj_field'			=>  $this->input->post('cmbfield'),
								'prj_title'			=>  $this->input->post('txttitle'),
								'prj_objective'		=>  $this->input->post('txtobjectives'),
								'prj_agency'		=>  $this->input->post('cmbagency'),
								'prj_status'		=>  $this->input->post('txtstat'),
								'prj_contributions'	=>  $this->input->post('txtcontri'),
								'prj_target'		=>  $this->input->post('istarget'),
								'updated_at'		=>  date("Y-m-d H:i:s"),
								'lastupdated_by'	=>  $this->session->userdata("sessUserId"),
								);
		$resulteditSeminar = $this->Serviceawards_model->editProject($arrProjectData, $this->input->post('semid_edit'));
		$this->session->set_flashdata('Notification', "Project Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/proj_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/proj?subs='.$_GET['subs']);
		}
	}
	// -------------- End Project --------------//

	// -------------- Begin paper --------------//
	public function fetchPaper()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getPaper($scientistId, $this->uri->segment(4)));
	}

	// Action - Paper
	public function addPaper()
	{
		$scientistId = $this->uri->segment(3);
		$arrPaperData = array( 
								'pap_srv_id'		=>  $scientistId,
								'pap_title'			=>  $this->input->post('txttitle'),
								'pap_name'			=>  $this->input->post('txtname'),
								'pap_target'		=> $this->input->post('istarget'),
								'created_at'		=>  date("Y-m-d H:i:s"),
								'created_by'		=>  $this->session->userdata("sessUserId")
								);
		if($this->input->post('istarget') == 1){
			$arrPaperData1 = array(
								'pap_field'				=> $this->input->post('cmbfield'),
								'pap_isbn'				=> $this->input->post('txtisbn'),
								'pap_submission_date'	=> $this->input->post('txtsubdate'),
								'pap_published_date	'	=> $this->input->post('txtpubdate'),
								'pap_nopages'			=> $this->input->post('txtnopages'),
								'pap_authors'			=> $this->input->post('txtauthors'),
								'created_at'			=>  date("Y-m-d H:i:s"),
								'created_by'			=>  $this->session->userdata("sessUserId")
								);
			$arrPaperData = array_merge($arrPaperData, $arrPaperData1);
		}
		$resultaddPaper = $this->Serviceawards_model->addPaper($arrPaperData);
		$this->session->set_flashdata('Notification', "Paper Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/paper_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/paper?subs='.$_GET['subs']);
		}
	}

	public function editPaper()
	{
		$scientistId = $this->uri->segment(3);
		$arrPaperData = array(
								'pap_title'			=>  $this->input->post('txttitle'),
								'pap_name'			=>  $this->input->post('txtname'),
								'pap_target'		=> $this->input->post('istarget'),
								'updated_at'		=>  date("Y-m-d H:i:s"),
								'lastupdated_by'	=>  $this->session->userdata("sessUserId"),
								);
		if($this->input->post('istarget') == 1){
			$arrPaperData1 = array(
								'pap_field'				=> $this->input->post('cmbfield'),
								'pap_isbn'				=> $this->input->post('txtisbn'),
								'pap_submission_date'	=> $this->input->post('txtsubdate'),
								'pap_published_date	'	=> $this->input->post('txtpubdate'),
								'pap_nopages'			=> $this->input->post('txtnopages'),
								'pap_authors'			=> $this->input->post('txtauthors'),
								'updated_at'		=>  date("Y-m-d H:i:s"),
								'lastupdated_by'	=>  $this->session->userdata("sessUserId"),
								);
			$arrPaperData = array_merge($arrPaperData, $arrPaperData1);
		}
		$resulteditSeminar = $this->Serviceawards_model->editPaper($arrPaperData, $this->input->post('semid_edit'));
		$this->session->set_flashdata('Notification', "Paper Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/paper_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/paper?subs='.$_GET['subs']);
		}
	}
	// -------------- End Paper --------------//

	// -------------- Begin Mentoring --------------//
	public function fetchMentoring()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getMentoring($scientistId, $this->uri->segment(4)));
	}

	// Action - Mentoring
	public function addMentoring()
	{
		$scientistId = $this->uri->segment(3);
		$targetPart = ($this->input->post('txttarget')=='') ? 0 : $this->input->post('txttarget');
		$arrMentorData = array( 
								'men_srv_id'				=>  $scientistId,
								'men_field'					=>  $this->input->post('cmbfield'),
								'men_institution'			=>  $this->input->post('txtinstitution'),
								'men_course'				=>  $this->input->post('txtcourse'),
								'men_notargetstudents'		=>  $targetPart,
								'men_output'				=>  $this->input->post('txtoutput'),
								'men_target'				=>  $this->input->post('istarget'),
								'created_at'				=>  date("Y-m-d H:i:s"),
								'created_by'				=>  $this->session->userdata("sessUserId")
								);
		if($this->input->post('istarget') == 1){
			$arrMentorData1 = array(
								'men_noactualstudents'		=> $this->input->post('txtactual')
								);
			$arrMentorData = array_merge($arrMentorData, $arrMentorData1);
		}
		$resultaddMentor = $this->Serviceawards_model->addMentor($arrMentorData);
		// begin upload file
		$total = count($_FILES['file-upload']['name']);
		$target_dir = "attachments/";

		for($i=0; $i<$total; $i++) {
			$tmpFilePath = $_FILES['file-upload']['tmp_name'][$i];
			if ($tmpFilePath != ""){
				$newFilePath = $target_dir . basename($_FILES['file-upload']['name'][$i]);
				$arr_file = explode('.', $_FILES['file-upload']['name'][$i]);
				$file_ext = end($arr_file); //filetype
				$arrFile = array(
								'attch_sem_id'				=> $resultaddMentor,
								'attch_srv_id'				=> $scientistId,
								'attch_filename'			=> $_FILES['file-upload']['name'][$i],
								'attch_filetype'			=> $file_ext,
								'attch_path'				=> $newFilePath,
								'attch_uploadedby'			=> $this->session->userdata('sessUserId'),
								'attch_uploadeddate'		=> date('Y-m-d H:i:s',now())

					);
				if(move_uploaded_file($tmpFilePath, $newFilePath)) {}
				$resultaddFile = $this->Serviceawards_model->addFile($arrFile);
			}
		}
		// end upload file
		$this->session->set_flashdata('Notification', "Students Mentoring Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/stud_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/stud?subs='.$_GET['subs']);
		}
	}

	public function editMentoring()
	{
		$removefiles = explode(';', $this->input->post('removefiles'));
		foreach ($removefiles as $file_r) {
			if($file_r!=''){
				$resultDelete = $this->Serviceawards_model->deleteFile($file_r);
			}
		}
		$scientistId = $this->uri->segment(3);
		$arrMentorData = array(
								'men_field'					=>  $this->input->post('cmbfield'),
								'men_institution'			=>  $this->input->post('txtinstitution'),
								'men_course'				=>  $this->input->post('txtcourse'),
								'men_notargetstudents'		=>  $this->input->post('txttarget'),
								'men_output'				=>  $this->input->post('txtoutput'),
								'men_target'				=>  $this->input->post('istarget'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		if($this->input->post('istarget') == 1){
			$arrMentorData1 = array(
								'men_noactualstudents'		=> $this->input->post('txtactual')
								);
			$arrMentorData = array_merge($arrMentorData, $arrMentorData1);
		}
		$resultaddMentor = $this->Serviceawards_model->editMentor($arrMentorData, $this->input->post('semid_edit'));
		// begin upload file
		$total = count($_FILES['file-upload']['name']);
		$target_dir = "attachments/";

		for($i=0; $i<$total; $i++) {
			$tmpFilePath = $_FILES['file-upload']['tmp_name'][$i];
			if ($tmpFilePath != ""){
				$newFilePath = $target_dir . basename($_FILES['file-upload']['name'][$i]);
				$arr_file = explode('.', $_FILES['file-upload']['name'][$i]);
				$file_ext = end($arr_file); //filetype
				$arrFile = array(
								'attch_sem_id'				=> $this->input->post('semid_edit'),
								'attch_srv_id'				=> $scientistId,
								'attch_filename'			=> $_FILES['file-upload']['name'][$i],
								'attch_filetype'			=> $file_ext,
								'attch_path'				=> $newFilePath,
								'attch_uploadedby'			=> $this->session->userdata('sessUserId'),
								'attch_uploadeddate'		=> date('Y-m-d H:i:s',now())

					);
				if(move_uploaded_file($tmpFilePath, $newFilePath)) {}
				$resultaddFile = $this->Serviceawards_model->addFile($arrFile);
			}
		}
		// end upload file
		$this->session->set_flashdata('Notification', "Students Mentoring Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/stud_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/stud?subs='.$_GET['subs']);
		}
	}

	// -------------- End Mentoring --------------//

	// -------------- Begin Curiculum --------------//
	public function fetchCuriculum()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getCurriculum($scientistId, $this->uri->segment(4)));
	}

	// Action - Curiculum
	public function addCuriculum()
	{
		$scientistId = $this->uri->segment(3);
		$arrCursData = array( 
								'cur_srv_id'				=>  $scientistId,
								'cur_institution'			=>  $this->input->post('txtinst'),
								'cur_field'					=>  $this->input->post('cmbfield'),
								'cur_output'				=>  $this->input->post('txtoutput'),
								'cur_target'				=>  $this->input->post('istarget'),
								'created_at'				=>  date("Y-m-d H:i:s"),
								'created_by'				=>  $this->session->userdata("sessUserId")
								);
		$resultarrCursData = $this->Serviceawards_model->addCurr($arrCursData);
		$this->session->set_flashdata('Notification', "Curiculum Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/cur_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/cur?subs='.$_GET['subs']);
		}
	}

	public function editCuriculum()
	{
		$scientistId = $this->uri->segment(3);
		$arrCursData = array(
								'cur_institution'			=>  $this->input->post('txtinst'),
								'cur_field'					=>  $this->input->post('cmbfield'),
								'cur_output'				=>  $this->input->post('txtoutput'),
								'cur_target'				=>  $this->input->post('istarget'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		$resultaddMentor = $this->Serviceawards_model->editCurr($arrCursData, $this->input->post('semid_edit'));
		$this->session->set_flashdata('Notification', "Curiculum Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/cur_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/cur?subs='.$_GET['subs']);
		}
	}

	// -------------- End Curiculum --------------//

	// -------------- Begin Networks --------------//
	public function fetchNetwork()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getNetworks($scientistId ,$this->uri->segment(4)));
	}

	// Action - Networks
	public function addNetwork()
	{
		$scientistId = $this->uri->segment(3);
		$arrCursData = array( 
								'net_srv_id'				=>  $scientistId,
								'net_field'					=>  $this->input->post('cmbfield'),
								'net_output'				=>  $this->input->post('txtoutput'),
								'net_target'				=>  $this->input->post('istarget'),
								'created_at'				=>  date("Y-m-d H:i:s"),
								'created_by'				=>  $this->session->userdata("sessUserId")
								);
		$resultarrCursData = $this->Serviceawards_model->addNetwork($arrCursData);
		$this->session->set_flashdata('Notification', "Network and Linkages Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/net_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/net?subs='.$_GET['subs']);
		}
	}

	public function editNetwork()
	{
		$scientistId = $this->uri->segment(3);
		$arrCursData = array(
								'net_field'					=>  $this->input->post('cmbfield'),
								'net_output'				=>  $this->input->post('txtoutput'),
								'net_target'				=>  $this->input->post('istarget'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		$resultaddMentor = $this->Serviceawards_model->editNetwork($arrCursData, $this->input->post('semid_edit'));
		$this->session->set_flashdata('Notification', "Network and Linkages Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/net_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/net?subs='.$_GET['subs']);
		}
	}

	// -------------- End Networks --------------//

	// -------------- Begin Research --------------//
	public function fetchResearch()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getResearch($scientistId, $this->uri->segment(4)));
	}

	// Action - Research
	public function addResearch()
	{
		$scientistId = $this->uri->segment(3);
		$arrResData = array( 
								'res_srv_id'				=>  $scientistId,
								'res_field'					=>  $this->input->post('cmbfield'),
								'res_output'				=>  $this->input->post('txtoutput'),
								'res_target'				=>  $this->input->post('istarget'),
								'created_at'				=>  date("Y-m-d H:i:s"),
								'created_by'				=>  $this->session->userdata("sessUserId")
								);
		$resultarrResData = $this->Serviceawards_model->addResearch($arrResData);
		$this->session->set_flashdata('Notification', "Research Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/res_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/res?subs='.$_GET['subs']);
		}
	}

	public function editResearch()
	{
		$scientistId = $this->uri->segment(3);
		$arrResData = array(
								'res_field'					=>  $this->input->post('cmbfield'),
								'res_output'				=>  $this->input->post('txtoutput'),
								'res_target'				=>  $this->input->post('istarget'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		$resultarrResData = $this->Serviceawards_model->editResearch($arrResData, $this->input->post('semid_edit'));
		$this->session->set_flashdata('Notification', "Research Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/res_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/res?subs='.$_GET['subs']);
		}
	}

	// -------------- End Research --------------//

	// -------------- Begin Other --------------//
	public function fetchOther()
	{
		$scientistId = $this->uri->segment(3);
		echo json_encode($this->Serviceawards_model->getOthers($scientistId, $this->uri->segment(4)));
	}

	// Action - Other
	public function addOther()
	{
		$scientistId = $this->uri->segment(3);
		$arrOthData = array( 
								'oth_srv_id'				=>  $scientistId,
								'oth_field'					=>  $this->input->post('cmbfield'),
								'oth_output'				=>  $this->input->post('txtoutput'),
								'oth_target'				=>  $this->input->post('istarget'),
								'created_at'				=>  date("Y-m-d H:i:s"),
								'created_by'				=>  $this->session->userdata("sessUserId")
								);
		$resultarrOthData = $this->Serviceawards_model->addOther($arrOthData);
		$this->session->set_flashdata('Notification', "Other Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/oth_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/oth?subs='.$_GET['subs']);
		}
	}

	public function editOther()
	{
		$scientistId = $this->uri->segment(3);
		$arrOthData = array(
								'oth_field'					=>  $this->input->post('cmbfield'),
								'oth_output'				=>  $this->input->post('txtoutput'),
								'oth_target'				=>  $this->input->post('istarget'),
								'updated_at'				=>  date("Y-m-d H:i:s"),
								'lastupdated_by'			=>  $this->session->userdata("sessUserId"),
								);
		$resultarrOthData = $this->Serviceawards_model->editOther($arrOthData, $this->input->post('semid_edit'));
		$this->session->set_flashdata('Notification', "Other Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		if($this->input->post('istarget') == 1){
			redirect('scientists/editServiceAward/'.$scientistId.'/accomp'.'/oth_a?subs='.$_GET['subs']);
		}else{
			redirect('scientists/editServiceAward/'.$scientistId.'/target'.'/oth?subs='.$_GET['subs']);
		}
	}

	// -------------- End Other --------------//

	//
	public function removeServiceAwards()
	{
		$data = array(
			'srv_isdeleted' => 1,
    	);
		$this->Serviceawards_model->removeServiceAwards($this->input->post('txtsrvid'), $data);
		redirect('scientists/edit/'.$this->input->post('txtsciid').'/srv');
	}

	// API - Delete Service Awards
	public function deleteServiceAwards()
	{
		$targetName = $this->input->post('target_name');
		if($targetName == 'sem' or $targetName == 'sem_a'){
			$deleteResult = $this->Serviceawards_model->deleteSeminar($this->input->post('target_id'));
		}else if($targetName == 'tra' or $targetName == 'tra_a'){
			$deleteResult = $this->Serviceawards_model->deleteTraining($this->input->post('target_id'));
		}else if($targetName == 'proj' or $targetName == 'proj_a'){
			$deleteResult = $this->Serviceawards_model->deleteProject($this->input->post('target_id'));
		}else if($targetName == 'paper' or $targetName == 'paper_a'){
			$deleteResult = $this->Serviceawards_model->deletePaper($this->input->post('target_id'));
		}else if($targetName == 'stud' or $targetName == 'stud_a'){
			$deleteResult = $this->Serviceawards_model->deleteMentor($this->input->post('target_id'));
		}else if($targetName == 'cur' or $targetName == 'cur_a'){
			$deleteResult = $this->Serviceawards_model->deleteCurr($this->input->post('target_id'));
		}else if($targetName == 'net' or $targetName == 'net_a'){
			$deleteResult = $this->Serviceawards_model->deleteNetwork($this->input->post('target_id'));
		}else if($targetName == 'res' or $targetName == 'res_a'){
			$deleteResult = $this->Serviceawards_model->deleteResearch($this->input->post('target_id'));
		}else if($targetName == 'oth' or $targetName == 'oth_a'){
			$deleteResult = $this->Serviceawards_model->deleteOther($this->input->post('target_id'));
		}
		// removefile
		$fileList = $this->Serviceawards_model->getFilesBySemId($this->input->post('target_id'));
		foreach ($fileList as $file) {
			unlink($file['attch_path']);
			$deleteFileResult = $this->Serviceawards_model->deleteFile($file['attch_id']);
		}
		redirect('scientists/editServiceAward/'.$this->uri->segment(3).'/'.$this->input->post('targetPane').'/'.$targetName.'?subs='.$this->input->post('subs'));
	}


	// -------------- begin Report --------------//
	public function addReportSubmitted(){
		dd('asdf');
		$scientistId = $this->uri->segment(3);
		$arrReportData = array( 
								'srp_srv_id'					=>  $scientistId,
								'srp_progress'					=>  $this->input->post('radProgReport'),
								'srp_progress_date'				=>  ($this->input->post('radProgReport')=='y') ? $this->input->post('progreportSubDate') : null,
								'srp_terminal'					=>  $this->input->post('radtermReport'),
								'srp_terminal_date'				=>  ($this->input->post('radtermReport')=='y') ? $this->input->post('termreportSubDate') : null,
								'srp_bspfeedback'				=>  $this->input->post('radbspReport'),
								'srp_bspfeedback_date'			=>  ($this->input->post('radbspReport')=='y') ? $this->input->post('bspreportSubDate') : null,
								'srp_feedback'					=>  $this->input->post('radbspInsReport'),
								'srp_feedback_date'				=>  ($this->input->post('radbspInsReport')=='y') ? $this->input->post('bspInsreportSubDate') : null,
								'srp_evaluation'				=>  $this->input->post('radevalForm'),
								'srp_evaluation_date'			=>  ($this->input->post('radevalForm')=='y') ? $this->input->post('evalFormSubDate') : null,
								'srp_implementation'			=>  $this->input->post('radimpeval'),
								'srp_implementation_date'		=>  ($this->input->post('radimpeval')=='y') ? $this->input->post('impevalSubDate') : null,
								'srp_comments'					=>  $this->input->post('txtcomment'),
								'created_at'					=>  date("Y-m-d H:i:s"),
								'created_by'					=>  $this->session->userdata("sessUserId")
								);
		$resultarrReportData = $this->Serviceawards_model->addReport($arrReportData);
		$this->session->set_flashdata('Notification', "Report Submitted Added Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		redirect('scientists/editServiceAward/'.$scientistId.'/report?subs='.$_GET['subs']);
	}

	public function editReportSubmitted(){
		$srvAwardsId = $this->uri->segment(3);
		$arrReportData = array(
								'srp_progress'					=>  $this->input->post('radProgReport'),
								'srp_progress_date'				=>  ($this->input->post('radProgReport')=='y') ? $this->input->post('progreportSubDate') : null,
								'srp_terminal'					=>  $this->input->post('radtermReport'),
								'srp_terminal_date'				=>  ($this->input->post('radtermReport')=='y') ? $this->input->post('termreportSubDate') : null,
								'srp_bspfeedback'				=>  $this->input->post('radbspReport'),
								'srp_bspfeedback_date'			=>  ($this->input->post('radbspReport')=='y') ? $this->input->post('bspreportSubDate') : null,
								'srp_feedback'					=>  $this->input->post('radbspInsReport'),
								'srp_feedback_date'				=>  ($this->input->post('radbspInsReport')=='y') ? $this->input->post('bspInsreportSubDate') : null,
								'srp_evaluation'				=>  $this->input->post('radevalForm'),
								'srp_evaluation_date'			=>  ($this->input->post('radevalForm')=='y') ? $this->input->post('evalFormSubDate') : null,
								'srp_implementation'			=>  $this->input->post('radimpeval'),
								'srp_implementation_date'		=>  ($this->input->post('radimpeval')=='y') ? $this->input->post('impevalSubDate') : null,
								'srp_comments'					=>  $this->input->post('txtcomment'),
								'updated_at'					=>  date("Y-m-d H:i:s"),
								'lastupdated_by'				=>  $this->session->userdata("sessUserId")
								);
		$arrReportData['srp_srv_id'] = $srvAwardsId;
		$reportCount = $this->Serviceawards_model->report_isExist($srvAwardsId);
		if($reportCount>0){
			$resultarrReportData = $this->Serviceawards_model->editReport($arrReportData, $this->input->post('reportId'));
		}else{
			$resultarrReportData = $this->Serviceawards_model->addReport($arrReportData);
		}
		$this->session->set_flashdata('Notification', "Report Submitted Updated Successfully");
		$this->session->set_flashdata('NotificationBox', 'success');
		redirect('scientists/editServiceAward/'.$srvAwardsId.'/report?subs='.$_GET['subs']);
	}
	// -------------- End Report --------------//

	
	
}