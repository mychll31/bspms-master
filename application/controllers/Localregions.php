<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Localregions extends CI_Controller { 
 
	public function __construct()   
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '',  
			'form_title'      	=> SYSTEM_NAME 
		);
		checkSession(); 
		$this->load->model('localregions_model');		
	}

	public function index()
	{
		$localregionsId = $this->uri->segment(3);
		$local_regions = array(
                    ''   => 'Choose Option',
                    '1'  => 'NCR',
                    '2'  => 'Region I',
                    '3'  => 'CAR',
                    '4'  => 'Region II',
                    '5'  => 'Region III',
                    '6'  => 'Region IV-A',
                    '7'  => 'Region IV-B',
                    '8'  => 'Region V',
                    '9'  => 'Region VI',
                    '10' => 'Region VII',
                    '11' => 'Region VIII',
                    '12' => 'Region IX',
                    '13' => 'Region X',
                    '14' => 'Region XI',
                    '15' => 'Region XII',
                    '16' => 'Region XIII',
                    '17' => 'Region NIR',
                    '18' => 'ARMM',
                    );

		$regions = $this->localregions_model->getAll();

		foreach ($regions as $key => $reg) {
			$regions[$key]['loc_reg_name'] = $local_regions[$reg['loc_region']];
		}
		$this->arrTemplateData['arrLocalregions'] = $regions;
   		$this->arrTemplateData['localregionsEdit'] = $this->localregions_model->getLocalregions($localregionsId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'localregions/localregions_view', $this->arrTemplateData);
 	
	}
	
	public function add() 
	{
			$arrLocalregionsData = array(
										'loc_region' 	=> $this->input->post('cmbRegion'),
										'loc_province'	=> ltrim($this->input->post('txtProvince'))
										);
			
			$resultAddLocalregions= $this->localregions_model->addLocalregions($arrLocalregionsData);		
			if($resultAddLocalregions)
			{
				$this->session->set_flashdata('Notification', 'Local Region Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('localregions');
	} 
	
	public function edit()
	{
			$arrLocalregionsData = array(
										'loc_region' 	=> $this->input->post('cmbRegion'),
										'loc_province'	=> ltrim($this->input->post('txtProvince')) 
										);
			$resultEditLocalregions	= $this->localregions_model->editLocalregions($this->input->post('txtLocId'), $arrLocalregionsData);		
			if($resultEditLocalregions)
			{
				$this->session->set_flashdata('Notification', 'Local Region Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('localregions');
	}
	 
	public function delete()
	{
		$deleteResult = $this->localregions_model->deleteLocalregions($this->input->post('txtdelregion'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Local Region Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('localregions');
		
	}

	public function fetchLocalRegs()
	{
		$local_regions = array(
                    ''   => 'Choose Option',
                    '1'  => 'NCR',
                    '2'  => 'Region I',
                    '3'  => 'CAR',
                    '4'  => 'Region II',
                    '5'  => 'Region III',
                    '6'  => 'Region IV-A',
                    '7'  => 'Region IV-B',
                    '8'  => 'Region V',
                    '9'  => 'Region VI',
                    '10' => 'Region VII',
                    '11' => 'Region VIII',
                    '12' => 'Region IX',
                    '13' => 'Region X',
                    '14' => 'Region XI',
                    '15' => 'Region XII',
                    '16' => 'Region XIII',
                    '17' => 'Region NIR',
                    '18' => 'ARMM',
                    );

		$regions = $this->localregions_model->getAll();

		foreach ($regions as $key => $reg) {
			$regions[$key]['loc_reg_name'] = $local_regions[$reg['loc_region']];
		}
		echo json_encode($regions);
	}

	public function fetchvalidlocal()
	{
		$validlocal = '';
		foreach ($this->localregions_model->getValidLocal() as $local) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validlocal.=strtolower($local['vallocal']).'|';
		}
		echo json_encode($validlocal);
	}

	public function getLocal($loc_id)
	{
		echo json_encode($this->localregions_model->getLocalregions($loc_id));
	}
	
}