<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Council extends CI_Controller {

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('council_model');		
	}

	public function index()
	{
		$councilId = $this->uri->segment(3);
		$this->arrTemplateData['arrCouncil'] = $this->council_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['councilEdit'] = $this->council_model->getCouncil($councilId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'council/council_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		
			$arrCouncilData = array( 
										'cil_code'			=>  $this->input->post('txtCode'),
										'cil_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultAddCouncil = $this->council_model->addCouncil($arrCouncilData);		
			if($resultAddCouncil)
			{
				$this->session->set_flashdata('Notification', 'Councils Saved Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('council');
	}
	
	
	
	
	public function edit()
	{
		
			$arrCouncilData = array( 
										'cil_code'	=>  $this->input->post('txtCode'),
										'cil_desc'	=>  $this->input->post('txtDesc')										
										 );
			
			$resultEditCouncil	= $this->council_model->editCouncil($this->input->post('txtCouId'), $arrCouncilData);		
			if($resultEditCouncil)
			{
				$this->session->set_flashdata('Notification', 'Council Updated Successfully');
				$this->session->set_flashdata('NotificationBox', 'success');
			}
			redirect('council');
	}
	
	public function delete()
	{
		$councilId = $this->uri->segment(3);  
		
		$deleteResult = $this->council_model->deleteCouncil($this->input->post('txtdelcouncil'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Council Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('council');
		
	}

	public function fetchdesc()
	{
		echo json_encode($this->council_model->getAll());
	}

	public function fetchvalidCouncil()
	{
		$validcouncil = '';
		foreach ($this->council_model->getValidCouncil() as $council) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validcouncil.=strtolower($council['valcouncil']).'|';
		}
		echo json_encode($validcouncil);
	}

	public function getCounc($cou_id)
	{
		echo json_encode($this->council_model->getCouncil($cou_id));
	}
	
}