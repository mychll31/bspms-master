<?php

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fields extends CI_Controller { 

	public function __construct()  
	{ 
		parent::__construct(); 
		$this->arrTemplateData = array(		
			'strNotification' 	=> '',
			'strActive'			=> '', 
			'form_title'      	=> SYSTEM_NAME
		);
		checkSession();
		$this->load->model('field_model');		
	}

	public function index()
	{
		$fieldId = $this->uri->segment(3);
		$this->arrTemplateData['arrField'] = $this->field_model->getAll(); // calling Post model method getPosts()
   		$this->arrTemplateData['fieldEdit'] = $this->field_model->getField($fieldId); // get per data for editing
		if($this->session->flashdata('Notification')!=""):
			$this->arrTemplateData['strNotification'] = notifyuser($this->session->flashdata('Notification'),$this->session->flashdata('NotificationBox'));
		endif;
		
		$this->template->load('template/main_tpl', 'fields/fields_view', $this->arrTemplateData);
 	
	}
	
	public function add()
	{
		$arrFieldData = array(
								// 'fld_code'	=>  $this->input->post('txtCode'),
								'fld_desc'	=>  $this->input->post('txtDesc')
								);

		$resultAddField = $this->field_model->addField($arrFieldData);		
		if($resultAddField) {
			$this->session->set_flashdata('Notification', 'Field Saved Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('fields');
		
	}
	
	public function edit()
	{
		$arrFieldData = array(
							// 'fld_code'	=>  $this->input->post('txtCode'),
							'fld_desc'	=>  $this->input->post('txtDesc')
							);
		
		$resultEditField = $this->field_model->editField($this->input->post('txtSpeId'), $arrFieldData);
		if($resultEditField) {
			$this->session->set_flashdata('Notification', 'Field Updated Successfully');
			$this->session->set_flashdata('NotificationBox', 'success');
		}
		redirect('fields');
	}
	
	public function delete()
	{
		$deleteResult = $this->field_model->deleteField($this->input->post('txtdelfield'));
		if($deleteResult)
		{
			$this->session->set_flashdata('Notification', "Field Deleted Successfully");
			$this->session->set_flashdata('NotificationBox', 'Success');
		}
		redirect('fields');
		
	}

	public function fetchdesc()
	{
		echo json_encode($this->field_model->getAll());
	}
	
	public function getFields($field_id)
	{
		echo json_encode($this->field_model->getField($field_id));
	}

	public function fetchvalidfield()
	{
		$validfield = '';
		foreach ($this->field_model->getValidField() as $field) {
			// $strout = str_replace(',','',$out['valout']);
			// $strout = str_replace(' ','',$strout);
			$validfield.=strtolower($field['valfield']).'|';
		}
		echo json_encode($validfield);
	}
	
}