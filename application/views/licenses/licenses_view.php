<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
table tbody td:last-child{
    background-color: #fff !important; 
}
td.sorting {
    display: none;
}
</style>
<div id="licenses">
<?php echo isset($strNotification)?$strNotification:""; ?>
<?php
if(count($licenseEdit)>0):
	$form =  base_url('licenses/edit');
	$button = '<button id="send" type="submit" class="btn btn-primary" name="btnSubmitLicense" value="edit" v-on="click:getValidate">Save</button>';
	$code = $licenseEdit[0]['lic_code']; 
	$desc = $licenseEdit[0]['lic_desc'];
	$hidden = '<input type="hidden" name="txtLicId" value="'.$licenseEdit[0]['lic_id'].'">';
else:
	$form = base_url('licenses/add');
	$button = '<button id="send" type="submit" class="btn btn-primary" name="btnSubmitLicense" value="add" v-on="click:getValidate">Save</button>';
	$code = "";
	$desc = '';
	$hidden = ""; 

endif; 
?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_content">
      <form class="form-horizontal form-label-left" action="<?=$form?>" method="post" novalidate>
        <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
        <input type="hidden" v-model='speid' value="<?=$this->uri->segment(3)?>">
        <span class="section">Professional Licenses</span>
        <div class="form-group {{ haserror.code }}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Code<span class="required">*</span></label>
          <div class="col-sm-3">
            <input id="txtCode" class="form-control" v-model="newSpe.code" name="txtCode"  maxlength="10" value="<?=$code?>" required type="text">
            <span class="help-block" v-if="validCode">{{ Error.code }}</span>
          </div>
        </div>
        <div class="item form-group {{ haserror.description }}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description<span class="required">*</span></label>
          <div class="col-sm-3">
            <input id="txtDesc" class="form-control" v-model="newSpe.description" name="txtDesc" maxlength="100" value="<?=$desc?>" required type="text">
            <span class="help-block" v-if="validDesc">{{ Error.description }}</span>
          </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <button type="submit" class="btn btn-default">Cancel</button>
            <?=$button?><?=$hidden?>
          </div>
        </div>          
      </form>
     </div>
   </div>
   <div class="x_panel">
    <h4>Professional Licenses</h4>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>No.</th>
          <th>Code</th>
          <th>Description</th>
          <td></td>
        </tr>
      </thead>
      <tbody>
      <?php $no=1; foreach($arrLicense as $license): if($this->uri->segment(3) != $license['lic_id']):?>
        <tr data-id="<?=$license['lic_id']?>">
          <td><?=$no++?></td>
          <td><?=$license['lic_code'];?></td>
          <td><?=$license['lic_desc'];?></td>
          <td align="center"><a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
        </tr>
      <?php endif; endforeach;?>
      </tbody>
    </table>
  </div>
</div>
</div>
<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('licenses/delete')?>" method="post">
                <input type="hidden" name="txtempid" id="txtempid"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_library.js')?>"></script>

<script>
  $(document).ready(function() {
    // delete scientist
    $("tr td:last-child").click(function(){
        $('#txtempid').val($(this).closest("tr").data("id"));
    });
    // data tables
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
    setTimeout(function() { $(".alert").alert('close'); }, 2000);
    $("tr td:not(:last-child)").click(function(){
        window.location = "<?=base_url().'licenses/index/'?>" + $(this).closest("tr").data("id");
    });
    
  });
</script>