<?php
echo isset($strNotification)?$strNotification:""; 

?>
	<!-- Bootstrap -->
    <link href="<?=base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?=base_url('assets/production/css/custom.css')?>" rel="stylesheet">
	
    <link href="<?=base_url('assets/vendors/select2/dist/css/select2.min.css')?>" rel="stylesheet">
 
<!doctype html>
<html>
<head>
<meta charset="utf-8">

</head>
<body>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
	<div class="x_content">
	    <form class="form-horizontal form-label-left" action="<?=base_url('experts/add')?>" method="post" >
    	    <span class="section">Add Expert's Profile</span>
            <div class="form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title<span class="required">*</span></label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbTitle" class="form_control" required>
                        <option></option>
                        <option value="Mr.">Mr.</option>
                        <option value="Ms.">Ms.</option>
                        <option value="Dr.">Dr.</option>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">First Name <span class="required">*</span></label>
                <div class="col-sm-3">
                	<input id="txtFirstName" class="form-control " name="txtFirstName" required type="text">
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Middle Name </label>
                <div class="col-sm-3">
                	<input id="txtMiddleName" class="form-control" name="txtMiddleName" type="text">
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Last Name <span class="required">*</span></label>
                <div class="col-sm-3">
                	<input id="txtLastName" class="form-control" name="txtLastName" type="text" required>
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Extension Name </label>
                <div class="col-sm-1">
                	<input id="txtExtName" class="form-control" name="txtExtName" type="text">
                </div>
            </div>
            
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date of Birth <span class="required">*</span></label>
                <div class="col-sm-3">
                	<input id="txtBirthDate" class="date-picker form-control col-md-7 col-xs-12 active" name="txtBirthDate" type="text" data-parsley-id="8987" placeholder="mm/dd/yyy" required>
                    <ul class="parsley-errors-list" id="parsley-id-1996"></ul>
                </div>
            </div>
            <div class="form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gender<span class="required">*</span></label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbGender" class="form_control" required>
                        <option></option>
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                 </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Area of Expertise</label>
                <div class="col-sm-6">
                  <select class="select2_multiple form-control" name="txtAreaOfExpertise" multiple="multiple">
                    <option>Choose option</option>
                    <?php foreach($arrAreaExpertise as $areaExpertise):?>
                    <option value="<?=$areaExpertise["exp_id"]?>"><?php echo $areaExpertise["exp_desc"];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Specialization</label>
                <div class="col-sm-6">
                  <select class="select2_multiple form-control" name="txtSpecialization" multiple="multiple">
                    <option>Choose option</option>
                    <?php foreach($arrSpecializations as $specialization):?>
                    <option value="<?=$specialization["exp_id"]?>"><?php echo $specialization["exp_desc"];?></option>
                    <?php endforeach;?>
                  </select>
                </div>
            </div>
             
            
            
            <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>          
         </form>
     </div>
   </div>
</div>
</body>
</html>





 <!-- jQuery -->
    <script src="<?=base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap -->
    <script src="<?=base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
    <!-- Select2 -->
    <script src="<?=base_url('assets/vendors/select2/dist/js/select2.full.min.js')?>"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?=base_url('assets/production/js/custom.js')?>"></script>
    
    
    
     <script>
      $(document).ready(function() {
        $(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
      });
    </script>
