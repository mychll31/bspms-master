 <div class="col-sm-12">
                <div class="x_panel">
                  
                  <div class="x_content">

                    <div class="col-xs-2">
                      <!-- required for floating -->
                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs tabs-left">
                        <li class="active"><a href="#seminars" data-toggle="tab">Seminars</a>
                        </li>
                        <li><a href="#trainings" data-toggle="tab">Trainings</a>
                        </li>
                        <li><a href="#projects" data-toggle="tab">Projects</a>
                        </li>
                        <li><a href="#papers" data-toggle="tab">Papers</a>
                        </li>
                        <li><a href="#mentoring" data-toggle="tab">Mentoring</a>
                        </li>
                        <li ><a href="#curriculum" data-toggle="tab">Curriculum</a>
                        </li>
                        <li><a href="#network" data-toggle="tab">Network</a>
                        </li>
                        <li><a href="#research" data-toggle="tab">Research</a>
                        </li>
                        <li><a href="#others" data-toggle="tab">Others</a>
                        </li>
                      </ul>
                    </div>

                    <div class="col-xs-10">
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active" id="seminars"><?=$strAccSeminars?></div>
                        <div class="tab-pane" id="trainings"><?=$strAccTrainings?></div>
                        <div class="tab-pane" id="projects">projects assisted</div>
                        <div class="tab-pane" id="papers"><?=$strAccPapers?></div>
                        <div class="tab-pane" id="mentoring">Settings Tab.</div>
                        <div class="tab-pane" id="curriculum">Home</div>
                        <div class="tab-pane" id="network">Profile Tab.</div>
                        <div class="tab-pane" id="research">Messages Tab.</div>
                        <div class="tab-pane" id="others">Settings Tab.</div>
                      </div>
                    </div>

                   

                  </div>
                </div>
</div>
