<?php echo isset($strNotification)?$strNotification:""; ?>
<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<script type="text/javascript" src="<?=base_url('assets/production/js/jquery.maskedinput.min.js')?>"></script>
<style>
.daterangepicker{z-index:1151 !important;}
div#example_filter {
    display: none !important;
}
</style>
<?php
$mini = ($arrScientist[0]['sci_middle_initial'] != '') ? $arrScientist[0]['sci_middle_initial'].'.' : '';
$mname = ($arrScientist[0]['sci_middle_name']!= '') ? $arrScientist[0]['sci_middle_name'][0].'.' : '';
 ?>
<div id="sciEmployment" v-cloak>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
      <div class="x_content">
        <span class="section">Scientist's Employment </span>
          <form class="form-horizontal form-label-left" action="<?=base_url('scientists/addEmployment/'.$arrScientist[0]["sci_id"].'/newsci')?>" method="post" >
          <?php #if($arrScientistEmployment):?>
          <div class="form-group">
            <div>
              <div class="col-md-8">Scientist Name :
                <h2>
                  <?=ucfirst($arrScientist[0]["sci_first_name"])?>
                  <?=strtoupper($mini != '' ? $mini : $mname)?>
                  <?=ucfirst($arrScientist[0]["sci_last_name"])?>
                </h2>
              </div>
              <a v-if="addEmpCount > 0 || nofile" class="btn btn-success pull-right" data-toggle="modal" data-target="#sciModal" href="#"> Finish <span class="fa fa-check"></span></a>
            </div>
          </div>
          <div class="ln_solid"></div>
          <?php #endif;?>
              <input type="hidden" value="<?=$this->uri->segment(3)?>" v-model="sci_id">
              <input type="hidden" value="<?=base_url()?>" v-model="baseUrl">
              
              <div class="item form-group" v-if="addEmpCount < 1" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                   <div class="col-sm-3">
                    <label><input type="checkbox" v-model="nofile" name=""> <b>No record on file</b></label>
                  </div>
              </div>

              <div class="item form-group">
                <div class="item form-group {{ haserror.company }}">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Company <span class="required">*</span></label>
                  <div class="col-sm-3">
                    <input id="txtCompany" v-attr="disabled: nofile" class="form-control" maxlength="100" v-model="newEmployment.company" name="txtco" type="text">
                    <span class="help-block" v-if="validCompany">{{ Error.company }}</span>
                  </div>
                </div>
              </div>
              <div class="item form-group {{ haserror.position }}">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Position <span class="required">*</span></label>
                <div class="col-sm-3">
                  <input id="txtPosition" v-attr="disabled: nofile" v-model="newEmployment.position" maxlength="100" class="form-control" name="txtpos" type="text">
                  <span class="help-block" v-if="validPosition">{{ Error.position }}</span>
                </div>
              </div>
              <div class="item form-group {{ haserror.datefrom }}">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date From </label>
                <div class="col-sm-3">
                  <input id="txtDateFrom" v-attr="disabled: nofile" v-model="newEmployment.datefrom" placeholder="YYYY-MM-DD" class="form-control" name="txtdfrom" maxlength="10" type="text">
                  <span class="help-block" v-if="validDateFrom">{{ Error.datefrom }}</span>
                </div>
              </div>
              <script type="text/javascript">$(function () { $('#txtDateFrom').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
              <div class="item form-group {{ haserror.dateto }}">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date To </label>
                <div class="col-sm-3" style="display: inline-flex;">
                  <input id="txtDateTo" maxlength="10" v-model="newEmployment.dateto" placeholder="YYYY-MM-DD" class="form-control" name="txtdto" type="text">
                  <span class="help-block" v-if="validDateTo">{{ Error.dateto }}</span>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkPresent" name="chkPresent"> <label>Present</label>
                </div>
              </div>
              <script type="text/javascript">$(function () { $('#txtDateTo').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
              <div class="item form-group {{ haserror.country }}">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country <span class="required">*</span></label>
                  <div class="col-sm-3">
                      <select v-attr="disabled: nofile" v-model="newEmployment.country" name="cmbCountry" class="form-control" value="<?php echo set_value('cmbCountry'); ?>">
                          <option value="">Choose Option</option>
                          <?php foreach($arrCountry as $country):?>
                          <option value="<?=$country["glo_id"]?>"
                                <?=($country["glo_id"] == set_value('cmbCountry')) ? 'selected' : ''; ?>>
                                <?=$country["glo_country"]?></option>
                          <?php endforeach;?>
                      </select>
                      <span class="help-block" v-if="validCountry">{{ Error.country }}</span>
                   </div>
              </div>
              <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-12 col-md-offset-5">
                    <button v-attr="disabled: nofile" type="reset" class="btn btn-default" name="btnSubmitEmployment" value="cancel">Reset</button>
                    <button v-attr="disabled: nofile" id="add" type="submit" class="btn btn-primary" name="btnSubmitEmployment" value="add" v-on="click: getvalidate">Save</button>
                  </div>
                </div>          
          </form>

          <?php if($arrScientistEmployment):?>
            <div class="form-group">
              <div class="col-sm-12">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Company</th>
                      <th>Position</th>
                      <th>Date Started</th>
                      <th>Date End</th>
                      <th>Country</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $no=1; foreach($arrScientistEmployment as $scientistEmployment):?>
                      <tr>
                        <td><?=$no++?></td>
                        <td><?=$scientistEmployment['emp_company']?></td>
                        <td><?=$scientistEmployment['emp_position']?></td>
                        <td><?=($scientistEmployment['emp_datefrom'] == '0000-00-00' ? '' : $scientistEmployment['emp_datefrom'])?></td>
                        <td>
                          <?php 
                              if($scientistEmployment['ispresent']==1){
                                  echo 'Present';
                              }else{
                                  if($scientistEmployment['emp_dateto'] == '0000-00-00')
                                    echo '';
                                  else
                                    echo $scientistEmployment['emp_dateto'];
                              }
                           ?>
                           </td>
                        <td><?=$scientistEmployment['glo_country']?></td>
                      </tr>
                      <?php endforeach;?>
                  </tbody>
                </table>
              </div>
            </div>
            <?php endif;?>
      </div>
   </div>
</div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="sciModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('scientists')?>" method="post">
                <b>SCIENTIST SUCCESSFULLY CREATED</b><br><br>
                Click Okay to continue.
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Okay </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">Cancel </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_addsci_vuejs.js')?>"></script>
<script src="<?=base_url('assets/production/js/custom/custom.js')?>"></script>

<script>
  $(document).ready(function() {
    setTimeout(function(){$('#example').DataTable();}, 0);

    $('#chkPresent').change(function(){
      $("#txtDateTo").prop("disabled", $(this).is(':checked'));
    });
	});
</script>