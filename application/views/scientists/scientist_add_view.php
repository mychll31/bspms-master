<?php echo isset($strNotification)?$strNotification:""; ?>
<!-- Bootstrap -->
<link href="<?=base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
<!-- Custom Theme Style -->
<link href="<?=base_url('assets/production/css/custom.css')?>" rel="stylesheet">
<link href="<?=base_url('assets/vendors/select2/dist/css/select2.min.css')?>" rel="stylesheet">
<script type="text/javascript" src="<?=base_url('assets/production/js/jquery.maskedinput.min.js')?>"></script>
<style type="text/css">
    .error-message{
        color: #a94442;
    }
    a.btnremove {
        color: red;
        font-weight: bold;
    }
    a.btnremove:hover, a.btnremove:focus {
        text-decoration: none;
        color: #a94444;
    }
    .profile-pic {
        max-width: 200px;
        max-height: 200px;
        display: block;
    }

    .profile-pic:hover {
        border: 1px solid #ddd;
        padding: 4px !important;
    }

    .file-upload {
        display: none;
    }
</style> 
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_content">
        <!-- begin form -->
        <div id="sciProfile" v-cloak>
           <input type="hidden" v-model="base_url" value="<?=base_url()?>">
           <span class="section">Add Scientist's Profile</span>
           <form class="form-horizontal form-label-left" action="<?=base_url('scientists/add')?>" method="post" enctype="multipart/form-data">
            <div class="col-md-8">
                <div class="form-group {{ haserror.title }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title<span class="required">*</span></label>
                     <div class="col-sm-4">
                        <select name="cmbTitle" class="form-control" v-model="title">
                            <option value="">Choose Option</option>
                            <?php foreach ($arrTitles as $title) : ?>
                                <option value="<?=$title['tit_id']?>"><?=$title['tit_abbreviation']?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                     </div>
                </div>

                <div class="item form-group {{ haserror.fname }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">First Name <span class="required">*</span></label>
                    <div class="col-sm-4">
                        <input id="txtFirstName" class="form-control " v-model="fname" name="txtFirstName" type="text" maxlength="50">
                        <span class="help-block" v-if="ErrorValidation.fname">{{ Error.fname }}</span>
                    </div>
                </div>
                <div class="item form-group {{ haserror.middlename }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Middle Name </label>
                    <div class="col-sm-4">
                        <input id="txtMiddleName" class="form-control" v-model="middlename" name="txtMiddleName" type="text" maxlength="50">
                        <span class="help-block" v-if="ErrorValidation.middlename">{{ Error.middlename }}</span>
                    </div>
                </div>
                <div class="item form-group {{ haserror.middleinitial }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Middle Initial </label>
                    <div class="col-sm-1">
                        <input id="txtMiddleInitial" class="form-control" v-model="middleinitial" name="txtMiddleInitial" type="text" maxlength="3">
                        <span class="help-block" v-if="ErrorValidation.middleinitial">{{ Error.middleinitial }}</span>
                    </div>
                </div>
                <div class="item form-group {{ haserror.lname }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Last Name <span class="required">*</span></label>
                    <div class="col-sm-4">
                        <input id="txtLastName" class="form-control" v-model="lname" name="txtLastName" type="text" maxlength="50">
                        <span id="txtFirstName" class="help-block" v-if="ErrorValidation.lname">{{ Error.lname }}</span>
                    </div>
                </div>
                <div class="item form-group {{ haserror.extname }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Extension Name </label>
                    <div class="col-sm-1">
                        <input id="txtExtName" class="form-control" maxlength="5" name="txtExtName" type="text" v-model="extname">
                        <span class="help-block" v-if="ErrorValidation.extname">{{ Error.extname }}</span>
                    </div>
                </div>
            
                <div class="item form-group" id="divbirthday">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date of Birth <span class="required">*</span></label>
                    <div class="col-sm-4 checkerror">
                        <input style="background-color: rgba(255, 255, 255, 0);" id="txtBirthDate" class="date-picker form-control col-md-7 col-xs-12 active" name="txtBirthDate" type="text" data-parsley-id="8987" placeholder="YYYY-MM-DD" maxlength="10" v-model="newProfile.bday">
                        <span class="help-block" id="spanbirthday"></span>
                    </div>
                </div>
                <script type="text/javascript">$(function () { $('#txtBirthDate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                <div class="form-group {{ haserror.gender }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gender<span class="required">*</span></label>
                     <div class="col-sm-4">
                        <select name="cmbGender" class="form-control" v-model="gender">
                            <option value="">Choose Option</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                        <span class="help-block" v-if="ErrorValidation.gender">{{ Error.gender }}</span>
                     </div>
                </div>
                <div class="form-group {{ haserror.status }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Civil Status<span class="required">*</span></label>
                     <div class="col-sm-4">
                        <select name="cmbStatus" class="form-control" v-model="status">
                            <option value="">Choose Option</option>
                            <option value="Single">Single</option>
                            <option value="Married">Married</option>
                            <option value="Widowed">Widowed</option>
                            <option value="Divorced">Divorced</option>
                        </select>
                        <span class="help-block" v-if="ErrorValidation.status">{{ Error.status }}</span>
                     </div>
                </div>
            
                <div class="form-group" id="divexpertise">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Area of Expertise <span class="required">*</span></label>
                    <div class="col-sm-6">
                      <select class="select2_multiple form-control" id="cmbAreaOfExpertise" name="cmbAreaOfExpertise[]" multiple="multiple">
                        <?php foreach($arrAreaExpertise as $areaExpertise):?>
                        <option value="<?=$areaExpertise["exp_id"]?>"><?php echo $areaExpertise["exp_desc"];?></option>
                        <?php endforeach;?>
                      </select>
                      <span class="help-block" id="spanexpertise"></span>
                    </div>
                </div>
                <div class="form-group" id='divspecialization'>
                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Specialization <span class="required">*</span></label>
                    <div class="col-sm-6">
                      <select class="select2_multiple form-control" id="cmbSpecialization" name="cmbSpecialization[]" multiple="multiple">
                        <?php foreach($arrSpecializations as $specialization):?>
                        <option value="<?=$specialization["spe_id"]?>"><?php echo $specialization["spe_desc"];?></option>
                        <?php endforeach;?>
                      </select>
                      <span class="help-block" id="spanspecialization"></span>
                    </div>
                </div>
                <div class="item form-group {{ haserror.profession }}" id="professionClass">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Profession <span class="required">*</span></label>
                    <div class="col-sm-6">
                        <select id="cmbProfession" class="form-control" v-model="profession" name="cmbProfession">
                            <option value="">Choose Option</option>
                            <?php foreach($arrProfessions as $profession):?>
                            <option value="<?=$profession["prof_id"]?>"><?php echo $profession["prof_desc"];?></option>
                            <?php endforeach;?>
                        </select>
                        <span class="help-block" v-if="ErrorValidation.profession">{{ Error.profession }}</span>
                    </div>
                </div>
                <div class="item form-group {{ haserror.license }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Professional License <span class="required">*</span></label>
                    <div class="col-sm-4">
                        <input id="txtProfessionLicense" class="form-control" v-model="license" name="txtProfessionalLicense" type="text" maxlength="50">
                        <span class="help-block" v-if="ErrorValidation.license">{{ Error.license }}</span>
                    </div>
                </div>
                 <div class="item form-group {{ haserror.allContacts }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Contact Number <span class="required">*</span></label>
                    <div class="col-sm-9">
                        <!-- Contact number -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#divAddContact" v-on="click: btnaddContact">Add Contact</button>
                        <div class="modal fade bs-example-modal-sm" tabindex="-1" id="divAddContact" role="dialog" aria-labelledby="mySmallModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Add Contact</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="item form-group {{ contact_haserror }}" id="professionClass">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Contact Number <span class="required">*</span></label>
                                            <div class="col-sm-6">
                                                <input id="txtmodalContact" class="form-control" maxlength="20" name="txtmodalContact" type="text" v-model="modalContact">
                                                <span class="help-block" v-if="contact_ErrorValidation">{{ contact_Error }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" v-on="click: clearcontact">Close</button>
                                        <button type="button" class="btn btn-primary" v-on="click: getContact">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Contact number -->
                        <table>
                            <tr v-repeat="contact: contacts">
                                <td><input type="text" class="form-control" maxlength="20" name="arrContact[]" value="{{ contact }}"></td>
                                <td>&nbsp;&nbsp;<a class="btnremove" id="btnremovecon5" href="#" v-on="click: removeContact(contact)">X</a></td>
                            </tr>
                        </table>
                        <span class="help-block" v-if="ErrorValidation.allContacts">{{ Error.allContacts }}</span>
                    </div>
                </div>
                 <div class="item form-group {{ haserror.allEmails }}}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" id="lblemail">Email Address <span class="required">*</span></label>
                    <div class="col-sm-9">
                        <!-- Email Address -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#divAddEmail" v-on="click: btnaddEmail">Add Email</button>
                        <div class="modal fade bs-example-modal-sm" tabindex="-1" id="divAddEmail" role="dialog" aria-labelledby="mySmallModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Add Email</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="item form-group {{ email_haserror }}">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email Address <span class="required">*</span></label>
                                            <div class="col-sm-6">
                                                <input id="txtmodalEmail" class="form-control" name="txtmodalEmail" type="text" v-model="modalEmail">
                                                <span class="help-block" v-if="email_ErrorValidation">{{ email_Error }}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" v-on="click: clearEmail">Close</button>
                                        <button type="button" class="btn btn-primary" v-on="click: getEmail">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Email Address -->
                        <table>
                            <tr v-repeat="email: emails">
                                <td><input type="text" class="form-control" name="arrEmail[]" value="{{ email }}"></td>
                                <td>&nbsp;&nbsp;<a class="btnremove" id="btnremovecon5" href="#" v-on="click: removeEmail(email)">X</a></td>
                            </tr>
                        </table>
                        <span class="help-block" v-if="ErrorValidation.allEmails">{{ Error.allEmails }}</span>
                    </div>
                </div>
                <div class="item form-group {{ haserror.postal }}">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Postal Address <span class="required">*</span></label>
                    <div class="col-sm-4">
                        <input id="txtPostalAddress" class="form-control" v-model="postal" name="txtPostalAddress" type="text" maxlength="100">
                        <span class="help-block" v-if="ErrorValidation.postal">{{ Error.postal }}</span>
                    </div>
                </div>
            </div>
            <!-- upload scientist picture -->
            <div class="col-md-4">
                <h2>Scientist's Picture</h2>
                <img class="profile-pic img-rounded" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<i class='fa fa-pencil'> <em>Click to change Picture</em></i>" src="<?= base_url('assets/images/logodost.png')?>" />
                <input class="file-upload" name="file-image" type="file"/>
                <br><br>
            </div>
            <!-- end upload scientist picture -->

              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                  <br>
                  <a href="<?=base_url().'scientists'?>" class="btn btn-default">Cancel</a>
                  <?php if($_SESSION['sessAccessLevel']==1): ?>
                    <button id="add" type="submit" class="btn btn-success" name="btnSubmit" value="add">Next</button>
                  <?php else: ?>
                    <button id="add" type="submit" class="btn btn-success" name="btnSubmit" value="add" v-on="click: getValidate">Next</button>
                  <?php endif; ?>

                </div>
              </div>          
         </form>
        </div>
        <!-- end form -->
     </div>
   </div>
</div>

 <!-- jQuery -->
    <!-- <script src="<?=base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script> -->
    <!-- Bootstrap -->
    <!-- <script src="<?=base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script> -->
    <!-- Select2 -->
    <!-- <script src="<?=base_url('assets/vendors/select2/dist/js/select2.full.min.js')?>"></script> -->
    <!-- <script src="<?=base_url('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.js')?>"></script> -->
    <!-- vuejs -->
    <script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
    <script src="<?=base_url('assets/vuejs/validation_addsci_vuejs.js')?>"></script>
    <script src="<?=base_url('assets/production/js/custom/custom.js')?>"></script>

     <script>
      $(document).ready(function() {
        $(".select2_single").select2({
          placeholder: "Select a state",
          allowClear: true
        });
        $(".select2_group").select2({});
        $("#cmbAreaOfExpertise, #cmbSpecialization").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });

        // begin Multiple contact
        $('#txtcontact2').hide();$('#tblContact2').hide();
        $('#txtcontact3').hide();$('#tblContact3').hide();
        $('#txtcontact4').hide();$('#tblContact4').hide();
        $('#txtcontact5').hide();$('#tblContact5').hide();
        var totalTextbox = 1;
        $('#addContact').click(function() {
            totalTextbox += 1;
            if(totalTextbox <= 5){
                $('#txtcontact'+totalTextbox).show();
                $('#tblContact'+totalTextbox).show();
            }
            if(totalTextbox == 5)
                $('#addContact').addClass('disabled');
        });

        $('#btnremovecon2').click(function() {
            $('#txtcontact'+totalTextbox).val('');
            $('#txtcontact'+totalTextbox).hide();$('#tblContact'+totalTextbox).hide();
            totalTextbox -= 1;
            $('#addContact').removeClass('disabled');
        });
        $('#btnremovecon3').click(function() {
            $('#txtcontact'+totalTextbox).val('');
            $('#txtcontact'+totalTextbox).hide();$('#tblContact'+totalTextbox).hide();
            totalTextbox -= 1;
            $('#addContact').removeClass('disabled');
        });
        $('#btnremovecon4').click(function() {
            $('#txtcontact'+totalTextbox).val('');
            $('#txtcontact'+totalTextbox).hide();$('#tblContact'+totalTextbox).hide();
            totalTextbox -= 1;
            $('#addContact').removeClass('disabled');
        });
        $('#btnremovecon5').click(function() {
            $('#txtcontact'+totalTextbox).val('');
            $('#txtcontact'+totalTextbox).hide();$('#tblContact'+totalTextbox).hide();
            totalTextbox -= 1;
            $('#addContact').removeClass('disabled');
        });

        // end Multiple contact

        // Multiple Email
        $('#txtemail2').hide();$('#tbl2').hide();
        $('#txtemail3').hide();$('#tbl3').hide();
        $('#txtemail4').hide();$('#tbl4').hide();
        $('#txtemail5').hide();$('#tbl5').hide();
        var totalTextboxEadd = 1;
        $('#addEmail').click(function() {
            for (var i = 2; i <= 10; i++) {
              if($('#tbl'+i).is(":visible") == false){
                  $('#tbl'+i).show();
                  $('#txtemail'+i).show();
                  break;
              }
            }
        });
        // $('#addEmail').click(function() {
        //     totalTextboxEadd += 1;
        //     if(totalTextboxEadd <= 5){
        //         $('#txtemail'+totalTextboxEadd).show();
        //         $('#tbl'+totalTextboxEadd).show();
        //     }
        //     if(totalTextboxEadd == 5){
        //         $('#addEmail').addClass('disabled');
        //     }
        // });

        $('#btnremove2').click(function() {
            $('#txtemail'+totalTextboxEadd).val('');
            $('#txtemail'+totalTextboxEadd).hide();$('#tbl'+totalTextboxEadd).hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        $('#btnremove3').click(function() {
            $('#txtemail'+totalTextboxEadd).val('');
            $('#txtemail'+totalTextboxEadd).hide();$('#tbl'+totalTextboxEadd).hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        $('#btnremove4').click(function() {
            $('#txtemail'+totalTextboxEadd).val('');
            $('#txtemail'+totalTextboxEadd).hide();$('#tbl'+totalTextboxEadd).hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        $('#btnremove5').click(function() {
            $('#txtemail'+totalTextboxEadd).val('');
            $('#txtemail'+totalTextboxEadd).hide();$('#tbl'+totalTextboxEadd).hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        // Multiple Email

        /*Upload scientist picture*/
        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.profile-pic').attr('src', e.target.result);
                }
        
                reader.readAsDataURL(input.files[0]);
            }
        }
        

        $(".file-upload").on('change', function(){
            readURL(this);
        });
        
        $(".profile-pic").on('click', function() {
           $(".file-upload").click();
        });
        
    });
    </script>
