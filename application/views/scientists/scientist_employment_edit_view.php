<?php
echo isset($strNotification)?$strNotification:""; 

?>

 <!-- jQuery -->
    <script src="<?=base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
    <!-- Bootstrap -->
    <script src="<?=base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>
<!doctype html>
<html>
<head>
<meta charset="utf-8" content="text/plain">

</head>
<body>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
  <div class="x_content">
    
      <form class="form-horizontal form-label-left" action="<?=base_url('scientists/addSciEmployment/'.$arrScientist[0]["sci_id"])?>" method="post" >
    
          <span class="section">Scientist's Employment </span>
            
            
            
            <?php if($arrScientistEmployment):?>
    
        <div class="form-group">
              <label class="col-md-12" for="name">Scientist Name : <?= $arrScientist[0]["sci_first_name"].' '.$arrScientist[0]["sci_middle_name"].' '.$arrScientist[0]["sci_last_name"]?>
              <a href="<?=base_url('scientists/addSuccess/'.$arrScientist[0]["sci_id"]);?>" class="btn btn-primary pull-right">NEXT</a></label>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                      <table class="table table-striped">
                          <tr>
                              <th></th>
                                <th>Company</th>
                                <th>Position</th>
                                <th>Date Started</th>
                                <th>Date End</th>
                                <th>Country</th>
                            </tr>
                            <tbody>
                            <?php foreach($arrScientistEmployment as $scientistEmployment):?>
                            <tr>
                              <td></td>
                                <td><?=$scientistEmployment['emp_company']?></td>
                                <td><?=$scientistEmployment['emp_position']?></td>
                                <td><?=$scientistEmployment['emp_datefrom']?></td>
                                <td><?=$scientistEmployment['emp_dateto']?></td>
                                <td><?=$scientistEmployment['glo_country']?></td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                            
                        </table>
                 </div>
            </div>
            <?php endif;?>
            
            
             <div class="ln_solid"></div>
            <div class="item form-group">
              <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Company <span class="required">*</span></label>
                <div class="col-sm-6">
                  <input id="txtCompany" class="form-control" name="txtCompany" type="text" value="<?=set_value('txtCompany')?>" required>
                  <?php echo form_error('txtCompany'); ?>
                </div>
            </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Position <span class="required">*</span></label>
                <div class="col-sm-6">
                  <input id="txtPosition" class="form-control" name="txtPosition" value="<?=set_value('txtPosition')?>"type="text" required>
                </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date From <span class="required">*</span></label>
                 <div class="col-sm-3">
                  <input id="txtDateFrom" value="<?=set_value('txtDateFrom')?>" class="form-control" name="txtDateFrom" type="text" required placeholder="YYYY-MM-DD">
                  <?php echo form_error('txtDateFrom'); ?>
                </div>
            </div>
             <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date To <span class="required">*</span></label>
                 <div class="col-sm-3">
                  <input id="txtDateTo" class="form-control" value="<?=set_value('txtDateTo')?>" name="txtDateTo" type="text" required placeholder="YYYY-MM-DD">
                  <?php echo form_error('txtDateTo'); ?>
                </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country <span class="required">*</span></label>
                <div class="col-sm-3">
                    <select name="cmbCountry" class="form-control" required>
                        <option value="">Choose Option</option>
                        <?php foreach($arrCountry as $country):?>
                        <option value="<?=$country["glo_id"]?>"
                          <?=($country["glo_id"] == set_value('cmbCountry')) ? 'selected' : ''; ?>>
                          <?=$country["glo_country"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-12 col-md-offset-5">
                  <button type="submit" class="btn btn-primary" name="btnSubmitEmployment" value="cancel">Reset</button>
                  <button id="add" type="submit" class="btn btn-success" name="btnSubmitEmployment" value="add">Add</button>
                </div>
              </div>          
         </form>
     </div>
   </div>
</div>
</body>
</html>

 <script>
      $(document).ready(function() {
      
    // $('#txtDateFrom, #txtDateTo').daterangepicker({
    //       singleDatePicker: true,
    //   format:'YYYY-MM-DD',
    //       calender_style: "picker_4"
    //     }, function(start, end, label) {
    //       console.log(start.toISOString(), end.toISOString(), label);
    //     });
    
  });
</script>