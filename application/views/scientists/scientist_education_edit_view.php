<?php
if(isset($arrScientistEditEducation))
{
	$level = $arrScientistEditEducation[0]["educ_level_id"];
	$school = $arrScientistEditEducation[0]["educ_school"];
	$course = $arrScientistEditEducation[0]["educ_course_id"];
	$countryorigin = $arrScientistEditEducation[0]["educ_countryorigin_id"];
	$yeargraduated = $arrScientistEditEducation[0]["educ_year_graduated"];	
	$hiddenId = "<input type='hidden' name='txtEducId' value=".$arrScientistEditEducation[0]["educ_id"]."/>";
	$button = '<button id="edit" type="submit" class="btn btn-success" name="btnSubmitEditEducation" value="editEducation">Save</button>';
	$form = base_url('scientists/editEducation/'.$arrScientist[0]["sci_id"]);
}
else
{
	$level = "";
	$school = "";
	$course = "";
	$countryorigin = "";
	$yeargraduated = "";	
	$hiddenId = "";
	$button = '<button id="add" type="submit" class="btn btn-success" name="btnSubmitEducation" value="addEdit">Add</button>';
	$form = base_url('scientists/addEducation/'.$arrScientist[0]["sci_id"]);
}
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8" content="text/plain">

</head>
<body>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
	<div class="x_content">
    
	    <form class="form-horizontal form-label-left" action="<?=$form?>" method="post" >
    
    	    <?php if($arrScientistEducation):?>
    
    		 <div class="form-group">
            	  <div class="col-sm-12">
                   		<table class="table table-striped">
                        	<tr>
                            	<th></th>
                                <th>Level</th>
                                <th>School</th>
                                <th>Country</th>
                                <th>Course</th>
                                <th>Year Graduated</th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tbody>
                            <?php foreach($arrScientistEducation as $scientistEducation):?>
                            <tr>
                            	<td></td>
                                <td><?=$scientistEducation['elev_desc']?></td>
                                <td><?=$scientistEducation['educ_school']?></td>
                                <td><?=$scientistEducation['glo_country']?></td>
                                <td><?=$scientistEducation['cou_desc']?></td>
                                <td><?=$scientistEducation['educ_year_graduated']?></td>
                                <td><a href="<?=base_url('scientists/editEducation/'.$arrScientist[0]["sci_id"].'/'.$scientistEducation['educ_id'])?>"><i class="fa fa-edit"></i></a></td>
                                <td><a href="#" onClick="deleteScientistEducation(<?=$arrScientist[0]["sci_id"]?>,<?=$scientistEducation['educ_id']?>,'<?=base_url('')?>')"><i class="fa fa-trash"></i></a></td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                            
                        </table>
                 </div>
            </div>
            <?php endif;?>
             <div class="ln_solid"></div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level</label>
               <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbLevel" class="form_control" required>
                        <option>Choose Option</option>
                        <?php foreach($arrEducationLevels as $educationlevels):?>
                        <option value="<?=$educationlevels["elev_id"]?>" <?php echo $level==$educationlevels["elev_id"]?"selected":"";?>><?=$educationlevels["elev_desc"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">School </label>
                <div class="col-sm-6">
                	<input id="txtSchool" class="form-control" name="txtSchool" type="text" value="<?=$school;?>">
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbCountry" class="form_control" required>
                        <option>Choose Option</option>
                        <?php foreach($arrCountry as $country):?>
                        <option value="<?=$country["glo_id"]?>" <?php echo $countryorigin==$country["glo_id"]?"selected":"";?>><?=$country["glo_country"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Course </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbCourse" class="form_control" required>
                        <option>Choose Option</option>
                        <?php foreach($arrCourse as $courses):?>
                        <option value="<?=$courses["cou_id"]?>" <?php echo $course==$courses["cou_id"]?"selected":"";?>><?=$courses["cou_desc"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
            
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Year Graduated <span class="required">*</span></label>
                 <div class="col-sm-3">
                	<input id="txtYearGraduated" class="form-control" name="txtYearGraduated" type="text" value="<?=$yeargraduated;?>">
                </div>
            </div>
           
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-12 col-md-offset-5">
                  <?php echo $hiddenId;?>
                  <button type="submit" class="btn btn-primary" name="btnSubmitEditEducation" value="cancel">Reset</button>
                  <?php echo $button;?>
                </div>
              </div>          
         </form>
     </div>
   </div>
</div>
</body>
</html>

<script>
function deleteScientistEducation(id,educId,strUrl)
{
	var ans = confirm("Are you sure you want to delete this Scientist's education?");
	if(ans)
	{
		//alert(strUrl+"project/deleteinstaller/"+id);
		$.ajax ({
			type: "GET",
			url: strUrl+"scientists/deleteScientistEducation/"+educId,
			cache: false,
			success: function(html)
				{
					//alert(html);
					window.location.replace(strUrl+"scientists/edit/"+id);
					//$(location).attr(strUrl+'/user/');
				} 
		});	
	}
}
</script>
