<!-- Select2 -->
   <script>
   $(document).ready(function() {
		 
		$(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
		
	});
   </script>
<?php echo isset($strNotification)?$strNotification:""; 
$urlName = $this->uri->segment(2);
$isactive = false;
if($urlName == 'edit'){
  $isactive = true;
}else if($urlName == 'editEmployment'){
  $isactive = true;
}else if($urlName=='editEducation'){
  $isactive = true;
}
$info = strstr(strtoupper($this->uri->segment(2)),'EDIT');
// $education = strstr(strtoupper($this->uri->segment(2)),'EDUCATIO/N');
$employment = strstr(strtoupper($this->uri->segment(2)),'EMPLOYMENT');
$serviceawards = strstr(strtoupper($this->uri->segment(2)),'SERVICEAWARD');
?>
<div class="col-md-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-child"></i> Scientist's Profile</h2>
        <div class="clearfix"></div>
      </div>
      
      <div class="x_content">
		  <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <li role="presentation" class="<?=$this->uri->segment(4) != 'srv' ? 'active' : ''?>""><a href="<?=($this->arrTemplateData['arrServiceAwards']!=null)?base_url().'scientists/edit/'.$this->arrTemplateData['arrServiceAwards'][0]['srv_sci_id']:''?>">Information</a></li>
            <li role="presentation" class="<?=$this->uri->segment(4) == 'srv' ? 'active' : ''?>"><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Service Awards</a>
          </li>
          </ul>
          
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade <?=$this->uri->segment(4) != 'srv' ? 'active in' : ''?>" id="tab_content1" aria-labelledby="home-tab">
              <?=$strProfile;?>
            </div>
            <div role="tabpanel" class="tab-pane fade <?=$this->uri->segment(4) == 'srv' ? 'active in' : ''?>" id="tab_content4" aria-labelledby="profile-tab">
              <?=$strServiceAwards;?>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>







