<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<script type="text/javascript" src="<?=base_url('assets/production/js/jquery.maskedinput.min.js')?>"></script>
<style>
.daterangepicker{z-index:1151 !important;}
/** remove search */
div#example_filter {
    display: none !important;
}
</style>
<?php
$mini = ($arrScientist[0]['sci_middle_initial'] != '') ? $arrScientist[0]['sci_middle_initial'].'.' : '';
$mname = ($arrScientist[0]['sci_middle_name']!= '') ? $arrScientist[0]['sci_middle_name'][0].'.' : '';
 ?>
<div id="sciEducation" v-cloak>
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
  	<div class="x_content">
      <?php echo form_open('scientists/addSciEducation/'.$arrScientist[0]["sci_id"], array("class" => "form-horizontal form-label-left")); ?>
      	<span class="section">Scientist's Education </span>
        <?php #if($arrScientistEducation):?>
      		<div class="form-group">
            <div>
              <div class="col-md-8">Scientist Name :
                <h2>
                  <?=ucfirst($arrScientist[0]["sci_first_name"])?>
                  <?=strtoupper($mini != '' ? $mini : $mname)?>
                  <?=ucfirst($arrScientist[0]["sci_last_name"])?>
                </h2>
              </div>
              <a v-if="addEducCount > 0 || nofile" class="btn btn-success pull-right" href="<?=base_url('scientists/addSciEmployment/'.$arrScientist[0]["sci_id"]);?>"> NEXT <span class="fa fa-step-forward"></span></a>
            </div>
          </div>
          <div class="ln_solid"></div>
          <?php #endif;?>
              <input type="hidden" value="<?=$this->uri->segment(3)?>" v-model="sci_id">
              <input type="hidden" value="<?=base_url()?>" v-model="baseUrl">
              
              <div class="item form-group" v-if="addEducCount < 1" >
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                   <div class="col-sm-3">
                    <label><input type="checkbox" v-model="nofile" name=""> <b>No record on file</b></label>
                  </div>
              </div>

              <div class="item form-group {{ haserror.level }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Level <span class="required">*</span></label>
                 <div class="col-sm-3">
                    <select v-attr="disabled: nofile" v-model="newEducation.level" name="cmbLevel" class="form-control" value="<?php echo set_value('cmbLevel'); ?>">
                        <option value="">Choose Option</option>
                        <?php foreach($arrEducationLevels as $educationlevels):?>
                            <option value="<?=$educationlevels["elev_id"]?>"
                                <?=($educationlevels["elev_id"] == set_value('cmbLevel')) ? 'selected' : ''; ?>>
                                <?=$educationlevels["elev_desc"]?>
                            </option>
                        <?php endforeach;?>
                    </select>
                    <span class="help-block" v-if="validLevel">{{ Error.level }}</span>
                  </div>
              </div>
              <div class="item form-group {{ haserror.school }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Institute/University <span class="required">*</span></label>
                  <div class="col-sm-3">
                  	<!-- <input v-attr="disabled: nofile" v-model="newEducation.school" id="txtSchool" class="form-control" value="<?php echo set_value('txtSchool'); ?>" name="txtSchool" type="text" maxlength="100" > -->
                    <select v-attr="disabled: nofile" v-model="newEducation.school" name="txtSchool" class="form-control" value="<?php echo set_value('txtSchool'); ?>">
                          <option value="">Choose Option</option>
                          <?php foreach($arrUniversity as $uni):?>
                          <option value="<?=$uni["uni_id"]?>"
                                <?=($uni["uni_id"] == set_value('txtSchool')) ? 'selected' : ''; ?>>
                                <?=$uni["uni_name"]?></option>
                          <?php endforeach;?>
                      </select>
                    <span class="help-block" v-if="validSchool">{{ Error.school }}</span>
                  </div>
              </div>
              <div class="item form-group {{ haserror.country }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country <span class="required">*</span></label>
                  <div class="col-sm-3">
                      <select v-attr="disabled: nofile" v-model="newEducation.country" name="cmbCountry" class="form-control" value="<?php echo set_value('cmbCountry'); ?>">
                          <option value="">Choose Option</option>
                          <?php foreach($arrCountry as $country):?>
                          <option value="<?=$country["glo_id"]?>"
                                <?=($country["glo_id"] == set_value('cmbCountry')) ? 'selected' : ''; ?>>
                                <?=$country["glo_country"]?></option>
                          <?php endforeach;?>
                      </select>
                      <span class="help-block" v-if="validCountry">{{ Error.country }}</span>
                   </div>
              </div>
              <div class="item form-group {{ haserror.course }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Course <span class="required">*</span></label>
                  <div class="col-sm-3">
                      <select v-attr="disabled: nofile" v-model="newEducation.course" name="cmbCourse" class="form-control" value="<?php echo set_value('cmbCourse'); ?>">
                          <option value="">Choose Option</option>
                          <?php foreach($arrCourse as $course):?>
                          <option value="<?=$course["cou_id"]?>"
                              <?=($course["cou_id"] == set_value('cmbCourse')) ? 'selected' : ''; ?>>
                              <?=$course["cou_desc"]?></option>
                          <?php endforeach;?>
                      </select>
                      <span class="help-block" v-if="validCourse">{{ Error.course }}</span>
                   </div>
              </div>
              
              <div class="item form-group {{ haserror.yrgraduate }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Year Graduated </label>
                   <div class="col-sm-3">
                  	<input v-attr="disabled: nofile" maxlength="4" v-model="newEducation.yrgraduate" id="txtYearGraduated" class="form-control" name="txtYearGraduated" type="text" placeholder="YYYY" value="<?php echo set_value('txtYearGraduated'); ?>">
                    <span class="help-block" v-if="validYrgraduate">{{ Error.yrgraduate }}</span>
                  </div>
              </div>
              <script type="text/javascript">$(function () { $('#txtYearGraduated').datetimepicker({ format: 'YYYY' });});</script>

              <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-12 col-md-offset-5">
                    <button type="reset" v-attr="disabled: nofile" class="btn btn-default" name="btnSubmitEducation" value="cancel">Reset</button>
                    <button id="add" v-attr="disabled: nofile" type="submit" class="btn btn-primary" name="btnSubmitEducation" value="add" v-on="click: getvalidate">Save</button>
                  </div>
                </div>  
                <?php if($arrScientistEducation):?>
                <div class="form-group">
                  <div class="col-sm-12">
                        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>No</th>
                                  <th>Level</th>
                                  <th>School</th> 
                                  <th>Country</th>
                                  <th>Course</th>
                                  <th>Year Graduated</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php $no=1; foreach($arrScientistEducation as $scientistEducation):?>
                              <tr>
                                  <td><?=$no++?></td>
                                  <td><?=$scientistEducation['elev_desc']?></td>
                                  <td><?=$scientistEducation['uni_name']?></td>
                                  <td><?=$scientistEducation['glo_country']?></td>
                                  <td><?=$scientistEducation['cou_desc']?></td>
                                  <td><?=($scientistEducation['educ_year_graduated'] == '0' ? '' : $scientistEducation['educ_year_graduated'])?></td>
                              </tr>
                              <?php endforeach;?>
                          </tbody>  
                          </table>
                   </div>
              </div>
              <?php endif;?>      
           </form>
       </div>
     </div>
  </div>
</div>

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_addsci_vuejs.js')?>"></script>
<script src="<?=base_url('assets/production/js/custom/custom.js')?>"></script>

<script>
$(document).ready(function() {
    setTimeout(function(){$('#example').DataTable();}, 0);
});
</script>