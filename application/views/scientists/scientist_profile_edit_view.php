<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/production/js/jquery.maskedinput.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
    .daterangepicker{z-index:1151 !important;}
    .tbldata tr {
        cursor: pointer;
    }
    .tbldata tbody tr.highlight td {
        background-color: #ccc;
    }
    .tbldata tbody tr:hover{
        background-color: #ccc !important; 
    }
    .error-message{
        color: #a94442;
    }
    a.btnremove {
        color: red;
        font-weight: bold;
    }
    a.btnremove:hover, a.btnremove:focus {
        text-decoration: none;
        color: #a94444;
    }
    .profile-pic {
        max-width: 200px;
        max-height: 200px;
        display: block;
    }

    .profile-pic:hover {
        border: 1px solid #ddd;
        padding: 4px !important;
    }

    .file-upload {
        display: none;
    }
    .richtextsci {
        width: 70% !important;
        margin: 0 auto;
    }
</style>
<?php
$no = 1;
$areaOfExpertise = explode('|',$arrScientist[0]["sci_expertise_id"]);
$specialize = explode('|',$arrScientist[0]["sci_specialization_id"]);
$sciContact = explode(';', $arrScientist[0]["sci_contact"]);
$sciEmail = explode(';', $arrScientist[0]["sci_email"]);
$emailCount = 0;
$sciemailList = array();
foreach ($sciEmail as $sciemail) {
    if($sciemail != ''){
        $emailCount += 1;
        $sciemailList[$emailCount] = $sciemail;
    }
}
$contactCount = 0;
$scicontactList = array();
foreach ($sciContact as $scicontact) {
    if($scicontact != ''){
        $contactCount += 1;
        $scicontactList[$contactCount] = $scicontact;
    }
}
?>
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingpersonal">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#accorPersonal" aria-expanded="true" aria-controls="accorPersonal">
          Personal
        </a>
      </h4>
    </div>
    <div id="accorPersonal" class="panel-collapse collapse <?=($this->uri->segment(4)==''? 'in': '')?>" role="tabpanel" aria-labelledby="headingpersonal">
      <div class="panel-body">
        <div class="row">
            <div id="editsciProfile" v-cloak>
                <form class="form-horizontal form-label-left" action="<?=base_url('scientists/editProfile')?>" method="post" enctype="multipart/form-data">
                    <div class="col-md-8">
                        <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                        <input type="hidden" v-model="emailexist" value="<?=$arrScientist[0]["sci_email"]?>">
                        <div class="item form-group {{ haserror.title }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title<span class="required">*</span></label>
                             <div class="col-sm-3">
                                <select name="cmbTitle" v-model="title" class="form-control">
                                    <option value="">Choose Option</option>
                                    <?php foreach($arrTitles as $title): ?>
                                        <option value="<?=$title['tit_id']?>" <?=($arrScientist[0]["sci_title"]==$title['tit_id']) ? 'selected' : ''?>><?=$title['tit_abbreviation']?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span id="helpBlock2" class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                             </div>
                        </div>
                        <div class="item form-group {{ haserror.fname }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">First Name <span class="required">*</span></label>
                            <div class="col-sm-3">
                                <input id="txtFirstName" v-model="fname" class="form-control " name="txtFirstName" type="text" maxlength="50" value="<?php echo $arrScientist[0]["sci_first_name"];?>">
                                <span id="helpBlock2" v-if="ErrorValidation.fname" class="help-block">{{ Error.fname }}</span>
                            </div>
                        </div>
                        <div class="item form-group {{ haserror.middlename }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Middle Name </label>
                            <div class="col-sm-3">
                                <input id="txtMiddleName" v-model="middlename" class="form-control" name="txtMiddleName" type="text" maxlength="50" value="<?php echo $arrScientist[0]["sci_middle_name"];?>">
                                <span class="help-block" v-if="ErrorValidation.middlename">{{ Error.middlename }}</span>
                            </div>
                        </div>
                        <div class="item form-group {{ haserror.middleinitial }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Middle Initial </label>
                            <div class="col-sm-1">
                                <input id="txtMiddleInitial" class="form-control" v-model="middleinitial" maxlength="3" name="txtMiddleInitial" type="text" maxlength="5" value="<?=$arrScientist[0]["sci_middle_initial"]?>">
                                <span class="help-block" v-if="ErrorValidation.middleinitial">{{ Error.middleinitial }}</span>
                            </div>
                        </div>
                        <div class="item form-group {{ haserror.lname }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Last Name <span class="required">*</span></label>
                            <div class="col-sm-3">
                                <input id="txtLastName" v-model="lname" class="form-control" name="txtLastName" type="text" maxlength="50" value="<?php echo $arrScientist[0]["sci_last_name"];?>">
                                <span id="txtFirstName" class="help-block" v-if="ErrorValidation.lname">{{ Error.lname }}</span>
                            </div>
                        </div>
                        <div class="item form-group {{ haserror.extname }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Extension Name </label>
                            <div class="col-sm-1">
                                <input id="txtExtName" maxlength="5" class="form-control" v-model="extname" name="txtExtName" type="text" value="<?php echo $arrScientist[0]["sci_ext_name"];?>">
                                <span class="help-block" v-if="ErrorValidation.extname">{{ Error.extname }}</span>
                            </div>
                        </div>
                        
                        <div class="item form-group {{ haserror.birthday }}" id="divbirthday">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date of Birth <span class="required">*</span></label>
                            <div class="col-sm-3">
                                <input id="txtBirthDate"  v-model="birthday" class="date-picker form-control col-md-7 col-xs-12 active" name="txtBirthDate" type="text" data-parsley-id="8987" maxlength="10" placeholder="YYYY-MM-DD"  value="<?php echo $arrScientist[0]["sci_birthdate"];?>">
                                <span class="help-block" id="spanbirthday">{{ Error.birthday }}</span>
                            </div>
                        </div>
                        <script type="text/javascript">$(function () { $('#txtBirthDate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                        <div class="form-group {{ haserror.gender }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Gender<span class="required">*</span></label>
                             <div class="col-sm-3">
                                <select name="cmbGender" v-model="gender" class="form-control">
                                    <option value="">Choose Option</option>
                                    <option value="M" <?php echo $arrScientist[0]["sci_gender"]=="M"?"selected":"";?>>Male</option>
                                    <option value="F" <?php echo $arrScientist[0]["sci_gender"]=="F"?"selected":"";?>>Female</option>
                                </select>
                                <span class="help-block" v-if="ErrorValidation.gender">{{ Error.gender }}</span>
                             </div>
                        </div>
                        <div class="form-group {{ haserror.status }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status<span class="required">*</span></label>
                             <div class="col-sm-3">
                                <select name="cmbStatus" v-model="status" class="form-control">
                                    <option value="">Choose Option</option>
                                    <option value="Single" <?php echo $arrScientist[0]["sci_status"]=="Single"?"selected":"";?>>Single</option>
                                    <option value="Married" <?php echo $arrScientist[0]["sci_status"]=="Married"?"selected":"";?>>Married</option>
                                    <option value="Widowed"  <?php echo $arrScientist[0]["sci_status"]=="Widowed"?"selected":"";?>>Widowed</option>
                                    <option value="Divorced"  <?php echo $arrScientist[0]["sci_status"]=="Divorced"?"selected":"";?>>Divorced</option>
                                </select>
                                <span class="help-block" v-if="ErrorValidation.status">{{ Error.status }}</span>
                             </div>
                        </div>
                        
                        <div class="form-group" id="divexpertise">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Area of Expertise <span class="required">*</span></label>
                            <div class="col-sm-3">
                              <select class="select2_multiple form-control" id="cmbAreaOfExpertise" name="cmbAreaOfExpertise[]" multiple="multiple">
                                <?php foreach($arrAreaExpertise as $areaExpertise):?>
                                        <option value="<?=$areaExpertise["exp_id"]?>" <?php echo in_array($areaExpertise["exp_id"],$areaOfExpertise)?"selected":"";?>><?php echo $areaExpertise["exp_desc"];?></option>
                                <?php endforeach;?>
                              </select>
                              <span class="help-block" id="spanexpertise"></span>
                            </div>
                        </div>

                        <div class="form-group" id='divspecialization'>
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Specialization <span class="required">*</span></label>
                            <div class="col-sm-3">
                              <select class="select2_multiple form-control" id="cmbSpecialization" name="cmbSpecialization[]" multiple="multiple">
                                <?php foreach($arrSpecializations as $specialization):?>
                                <option value="<?=$specialization["spe_id"]?>" <?php echo in_array($specialization["spe_id"],$specialize)?"selected='selected'":"";?>><?php echo $specialization["spe_desc"];?></option>
                                <?php endforeach;?>
                              </select>
                              <span class="help-block" id="spanspecialization"></span>
                            </div>
                        </div>

                         <div class="item form-group {{ haserror.profession }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Profession <span class="required">*</span></label>
                            <div class="col-sm-3">
                                <select id="cmbProfession" class="form-control" name="cmbProfession" v-model="profession">
                                    <option value="">Choose option</option>
                                    <?php foreach($arrProfessions as $profession):?>
                                    <option value="<?=$profession["prof_id"]?>" <?php echo $profession["prof_id"]==$arrScientist[0]["sci_profession_id"]?"selected='selected'":"";?>><?php echo $profession["prof_desc"];?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block" v-if="ErrorValidation.profession">{{ Error.profession }}</span>
                            </div>
                        </div>
                        <div class="item form-group {{ haserror.license }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Professional License <span class="required">*</span></label>
                            <div class="col-sm-3">
                                <input id="txtProfessionLicense" class="form-control" v-model="license" maxlength="50" name="txtProfessionalLicense" type="text" value="<?php echo $arrScientist[0]["sci_license"];?>" >
                                <span class="help-block" v-if="ErrorValidation.license">{{ Error.license }}</span>
                            </div>
                        </div>

                        <!-- Begin Contact number -->
                         <div class="item form-group {{ haserror.allContacts }}">
                            <input type="hidden" value="<?=$arrScientist[0]['sci_contact']?>" v-model="editcontacts">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Contact Number <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#divAddContact" v-on="click: btnaddContact">Add Contact</button>
                                <div class="modal fade bs-example-modal-sm" tabindex="-1" id="divAddContact" role="dialog" aria-labelledby="mySmallModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Add Contact</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="item form-group {{ contact_haserror }}" id="professionClass">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Contact Number <span class="required">*</span></label>
                                                    <div class="col-sm-6">
                                                        <input id="txtmodalContact" class="form-control" maxlength="20" name="txtmodalContact" type="text" v-model="modalContact">
                                                        <span class="help-block" v-if="contact_ErrorValidation">{{ contact_Error }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" v-on="click: clearcontact">Close</button>
                                                <button type="button" class="btn btn-primary" v-on="click: getContact">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table>
                                    <tr v-repeat="contact: contacts">
                                        <td><input type="text" class="form-control" maxlength="20" name="arrContact[]" value="{{ contact }}"></td>
                                        <td>&nbsp;&nbsp;<a class="btnremove" id="btnremovecon5" href="#" v-on="click: removeContact(contact)">X</a></td>
                                    </tr>
                                </table>
                                <span class="help-block" v-if="ErrorValidation.allContacts">{{ Error.allContacts }}</span>
                            </div>
                        </div>
                        <!-- End Contact number -->

                        <!-- Email Address -->
                        <div class="item form-group {{ haserror.allEmails }}}">
                            <input type="hidden" value="<?=$arrScientist[0]['sci_email']?>" v-model="editemails">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name" id="lblemail">Email Address <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#divAddEmail" v-on="click: btnaddEmail">Add Email</button>
                                <div class="modal fade bs-example-modal-sm" tabindex="-1" id="divAddEmail" role="dialog" aria-labelledby="mySmallModalLabel">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Add Email</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="item form-group {{ email_haserror }}">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Email Address <span class="required">*</span></label>
                                                    <div class="col-sm-6">
                                                        <input id="txtmodalEmail" class="form-control" name="txtmodalEmail" type="text" v-model="modalEmail">
                                                        <span class="help-block" v-if="email_ErrorValidation">{{ email_Error }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" v-on="click: clearEmail">Close</button>
                                                <button type="button" class="btn btn-primary" v-on="click: getEmail">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <table>
                                    <tr v-repeat="email: emails">
                                        <td><input type="text" class="form-control" maxlength="20" name="arrEmail[]" value="{{ email }}"></td>
                                        <td>&nbsp;&nbsp;<a class="btnremove" id="btnremovecon5" href="#" v-on="click: removeEmail(email)">X</a></td>
                                    </tr>
                                </table>
                                <span class="help-block" v-if="ErrorValidation.allEmails">{{ Error.allEmails }}</span>
                            </div>
                        </div>
                        <!-- Email Address -->
                        
                        <div class="item form-group {{ haserror.postal }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Postal Address <span class="required">*</span></label>
                            <div class="col-sm-3">
                                <input id="txtPostalAddress" maxlength="100" v-model="postal" class="form-control" name="txtPostalAddress" type="text" value="<?php echo $arrScientist[0]["sci_postal_address"];?>">
                                <span class="help-block" v-if="ErrorValidation.postal">{{ Error.postal }}</span>
                            </div>
                        </div>

                        <div class="item form-group {{ haserror.postal }}">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
                            <div class="col-sm-3">
                                <input type="checkbox" name="chkrepatriated" <?=($arrScientist[0]["isrepatriated"] == 1) ? 'checked' : ''?>> <b>Repatriated</b>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <h2>Scientist's Picture</h2>
                        <img class="profile-pic img-rounded" data-toggle="tooltip" data-placement="bottom" data-html="true" title="<i class='fa fa-pencil'> <em>Click to change Picture</em></i>" src="<?= base_url(($arrScientist[0]["sci_picturename"])!='' ? 'data/sci_images/'.$arrScientist[0]["sci_picturename"] : 'assets/images/logodost.png')?>" />
                        <input class="file-upload" name="file-image" type="file"/>
                        <br><br>
                    </div>

                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                        <br><br>
                        <input type="hidden" name="txtScientistId" value="<?php echo $arrScientist[0]["sci_id"];?>" >
                          <button type="button" class="btn btn-default" id="btncancelProf" value="cancel">Cancel</button>
                          <button id="add" type="submit" class="btn btn-primary" name="btnSubmitEditProfile" value="editProfile" v-on="click: getValidate">Save</button>
                        </div>
                      </div>          
                </form>
            </div>
        </div>
      </div>
    </div>
  </div>

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#accorEducation" aria-expanded="false" aria-controls="accorEducation">
          Education
        </a>
      </h4>
    </div>
    <div id="accorEducation" class="panel-collapse collapse <?=($this->uri->segment(4)=='educ'? 'in': '')?>" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        <!-- Begin ProfInfo -->
        <div id="profEduc" v-cloak>
            <!-- Begin Add Education -->
            <div class="modal fade" id="addEducation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Education</h4>
                    </div>
                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3))?>">
                        <input type="hidden" id="txteducid_edit" name="txteducid_edit">
                        <div class="modal-body">
                            <div class="form-group {{ haserror.level }}">
                                <label class="control-label" for="cmblevel">Level <span class="required">*</span> </label>
                                <select name="cmbLevel" id="cmbLevel" class="form-control" v-model="neweduc.level">
                                    <option value="">Choose Option</option>
                                    <option v-repeat="level: levels" value="{{ level.elev_id }}">{{ level.elev_desc }}</option>
                                </select>
                                <span class="help-block" v-if="validLevel">{{ Error.level }}</span>
                            </div>
                            <div class="form-group {{ haserror.school }}">
                                <label class="control-label" for="txtSchool">Institute/University <span class="required">*</span> </label>
                                <!-- <input id="txtSchool" v-model="neweduc.school" class="form-control" name="txtSchool" type="text" maxlength="100"> -->
                                <select v-attr="disabled: nofile" v-model="neweduc.school" name="txtSchool" class="form-control">
                                    <option value="">Choose Option</option>
                                    <?php foreach($arrUniversity as $uni):?>
                                        <option value="<?=$uni["uni_id"]?>"><?=$uni["uni_name"]?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="help-block" v-if="validSchool">{{ Error.school }}</span>
                            </div>
                            <div class="form-group {{ haserror.country }}">
                                <label class="control-label" for="cmbcountry">Country <span class="required">*</span> </label>
                                <select name="cmbCountry" id="cmbCountry" class="form-control" v-model="neweduc.country">
                                    <option value="">Choose Option</option>
                                    <option v-repeat="country: countries" value="{{ country.glo_id }}">{{ country.glo_country }}</option>
                                </select>
                                <span class="help-block" v-if="validCountry">{{ Error.country }}</span>
                            </div>
                            <div class="form-group {{ haserror.course }}">
                                <label class="control-label" for="name">Course <span class="required">*</span></label>
                                <select name="cmbCourse" id="cmbCourse" class="form-control" v-model="neweduc.course">
                                    <option value="">Choose Option</option>
                                    <option v-repeat="cour: courses" value="{{ cour.cou_id }}">{{ cour.cou_desc }}</option>
                                </select>
                                <span class="help-block" v-if="validCourse">{{ Error.course }}</span>
                            </div>
                            <div class="form-group {{ haserror.yrGrd }}">
                                <label class="control-label" for="name">Year Graduated </label>
                                <input id="txtYearGraduated" v-model="neweduc.yrGrd" class="form-control" name="txtYearGraduated" type="text" maxlength="4" placeholder="YYYY">
                                <span class="help-block" v-if="validYrGrd">{{ Error.yrGrd }}</span>
                            </div>
                            <script type="text/javascript">$(function () { $('#txtYearGraduated').datetimepicker({ format: 'YYYY' });});</script>          
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target="#deleteModal" v-if="action == 'edit'">Delete <span class="glyphicon glyphicon-trash"></span></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                            <button type="submit" class="btn btn-primary" v-if="action == 'add'" v-on="click: getValidateEducation">Save</button>
                            <button type="submit" class="btn btn-primary" v-if="action == 'edit'" v-on="click: getValidateEducation">Save <span class="glyphicon glyphicon-pencil"></span></button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
            <!-- end Add Education -->
            <button type="button" class="btn btn-primary pull-right" v-on="click: addEducation" data-toggle="modal" data-target="#addEducation">Add Education</button>
            <br><br><br>
            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
            <input type="hidden" id="sciid" value="<?=$this->uri->segment(3)?>">
             <div class="form-group">
                <div class="col-sm-12">
                    <table id="example" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Level</th>
                            <th>School</th>
                            <th>Country</th>
                            <th>Course</th>
                            <th>Year Graduated</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr v-repeat="educ: educations" data-target="#addEducation" data-toggle="modal" data_value="{{ educ.id }}" v-on="click: getRowValue(educ)">
                                <td>{{ ($index + 1) }}</td>
                                <td>{{ educ.elev_desc }}</td>
                                <td>{{ educ.uni_name }}</td>
                                <td>{{ educ.glo_country }}</td>
                                <td>{{ educ.cou_desc }}</td>
                                <td><span v-if="educ.educ_year_graduated != 0">{{ educ.educ_year_graduated }}</span></td>
                            </tr>
                        </tbody>    
                    </table>
                 </div>
            </div>
        </div>
        <!-- End ProfInfo -->
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#accorEmployment" aria-expanded="false" aria-controls="accorEmployment">
          Employment
        </a>
      </h4>
    </div>
    <div id="accorEmployment" class="panel-collapse collapse  <?=($this->uri->segment(4)=='employ'? 'in': '')?>" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <!-- begin profEmp -->
        <div id="profEmp" v-cloak>
            <!-- Begin Add Employment -->
            <div class="modal fade" id="addEmploy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Employment</h4>
                    </div>
                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3))?>">
                        <input type="hidden" id="txtempid_edit" name="txtempid_edit" v-model="eid">
                        <div class="modal-body">
                            <div class="form-group {{ haserror.co }}">
                                <label class="control-label" for="txtco">Company <span class="required">*</span> </label>
                                <input id="txtco" v-model="co" class="form-control" name="txtco" type="text" maxlength="100">
                                <span class="help-block" v-if="ErrorValidation.co">{{ Error.co }}</span>
                            </div>
                            <div class="form-group {{ haserror.pos }}">
                                <label class="control-label" for="txtpos">Position <span class="required">*</span> </label>
                                <input id="txtpos" v-model="pos" class="form-control" name="txtpos" type="text" maxlength="100">
                                <span class="help-block" v-if="ErrorValidation.pos">{{ Error.pos }}</span>
                            </div>
                            <div class="form-group {{ haserror.dfrom }}">
                                <label class="control-label" for="txtdfrom">Date From </label>
                                <input id="txtdfrom" v-model="dfrom" class="date-picker form-control col-md-7 col-xs-12 active" name="txtdfrom" type="text" data-parsley-id="8987" placeholder="YYYY-DD-MM" maxlength="10">
                                <span class="help-block" id="spandfrom"></span>
                            </div>
                            <script type="text/javascript">$(function () { $('#txtdfrom').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                            <div class="form-group {{ haserror.dto }}">
                                <label class="control-label" for="txtdto">Date To </label>
                                <input id="txtdto" v-model="dto" class="date-picker form-control col-md-7 col-xs-12 active" name="txtdto" type="text" placeholder="YYYY-DD-MM" maxlength="10">
                                <span class="help-block" id="spandto"></span>
                            </div>
                            <div class="form-group {{ haserror.dto }}">
                                &nbsp;&nbsp;&nbsp;<input type="checkbox" id="chkPresent" name="chkPresent"> <label>Present</label>
                                <span class="help-block" id="spandto"></span>
                            </div>
                            <script type="text/javascript">$(function () { $('#txtdto').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                            <div class="form-group {{ haserror.country }}">
                                <label class="control-label" for="cmbcountry">Country <span class="required">*</span> </label>
                                <select name="cmbCountry" id="cmbCountry" class="form-control" v-model="country">
                                    <option value="">Choose Option</option>
                                    <option v-repeat="country: countries" value="{{ country.glo_id }}">{{ country.glo_country }}</option>
                                </select>
                                <span class="help-block" v-if="ErrorValidation.country">{{ Error.country }}</span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger pull-left" data-toggle="modal" data-target="#deleteModalEmp" v-if="action == 'edit'">Delete <span class="glyphicon glyphicon-trash"></span></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                            <button type="submit" class="btn btn-primary" v-if="action == 'add'" v-on="click: getValidateEmployment">Save</button>
                            <button type="submit" class="btn btn-primary" v-if="action == 'edit'" v-on="click: getValidateEmployment">Save <span class="glyphicon glyphicon-pencil"></span></button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
            <!-- end Add Employment -->
            <button type="button" class="btn btn-primary pull-right" v-on="click: addEmployment" data-toggle="modal" data-target="#addEmploy">Add Employment</button>
            <br><br><br>
            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
            <input type="hidden" id="sciid" value="<?=$this->uri->segment(3)?>">
            
            <div class="form-group">
                <div class="col-sm-12">
                    <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Company</th>
                            <th>Position</th>
                            <th nowrap>Date Start</th>
                            <th nowrap>Date End</th>
                            <th>Country</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr v-repeat="emp: employment" data-target="#addEmploy" data-toggle="modal" data_value="{{ emp.id }}" v-on="click: getRowValue(emp)">
                                <td>{{ ($index + 1) }}</td>
                                <td>{{ emp.emp_company }}</td>
                                <td>{{ emp.emp_position }}</td>
                                <td><span v-if="emp.emp_datefrom != '0000-00-00'">{{ emp.emp_datefrom }}</span></td>
                                <td><span v-if="emp.ispresent==1">Present</span><span v-if="emp.ispresent==0">{{ emp.emp_dateto }}</span></td>
                                <td>{{ emp.glo_country }}</td>
                            </tr>
                        </tbody>    
                    </table>
                 </div>
            </div>
        </div>
        <!-- end profEmp -->
      </div>
    </div>
  </div>
</div>

<!-- Begin ToBeAccomplished Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('scientists/deleteScientistTba/'.$this->uri->segment(3))?>">
                <input type="hidden" name="txttba" id="txttba"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End ToBeAccomplished Delete Modal -->

<!-- Begin Education Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('scientists/deleteScientistEducation/'.$this->uri->segment(3))?>">
                <input type="hidden" name="txteducid" id="txteducid"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Education Delete Modal -->

<!-- Begin Employment Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('scientists/deleteScientistEmployment/'.$this->uri->segment(3))?>">
                <input type="hidden" name="txtempid" id="txtempid"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
  <!-- End Employment Delete Modal -->
</div>


<!-- begin vue js -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/profile_vuejs.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_addsci_vuejs.js')?>"></script>
<script src="<?=base_url('assets/production/js/custom/custom.js')?>"></script>
<script src="<?=base_url('assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')?>"></script>
<!-- end vue js -->
<script>
$(document).ready(function() {
    // cancel profile
    $('#btncancelProf').click(function(){
        parent.history.back();
    });
    // $('#example').DataTable();
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
    setTimeout(function() { $(".alert").alert('close'); }, 2000);
    $("#cmbAreaOfExpertise, #cmbSpecialization").select2({
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });

        // Begin Add Contact
        var contactCount = parseInt('<?=$contactCount?>');
        var totalTextboxContact = 0;

        if(contactCount == 0)
            totalTextboxContact = 1;
        else
            totalTextboxContact = contactCount;

        for (var y = contactCount+1; y <= 5; y++) {
            $('#txtcontact'+y).hide();$('#tblcontact'+y).hide();
        }

        $('#addContact').click(function() {
            totalTextboxContact += 1;
            for (var i = 2; i <= 5; i++) {
              if($('#tblcontact'+i).is(":visible") == false){
                  $('#txtcontact'+i).show();
                  $('#tblcontact'+i).show();
                  break;
              }
            }
            if(totalTextboxContact == 5){
                $('#addContact').addClass('disabled');
            }
        });

        $('#btnContactremove2').click(function() {
            $('#txtcontact'+totalTextboxContact).val('');
            $('#tblcontact'+totalTextboxContact).hide();$('#tblcontact'+totalTextboxContact).hide();
            totalTextboxContact -= 1;
            $('#addContact').removeClass('disabled');
        });
        $('#btnContactremove3').click(function() {
            $('#txtcontact'+totalTextboxContact).val('');
            $('#tblcontact'+totalTextboxContact).hide();$('#tblcontact'+totalTextboxContact).hide();
            totalTextboxContact -= 1;
            $('#addContact').removeClass('disabled');
        });
        $('#btnContactremove4').click(function() {
            $('#txtcontact'+totalTextboxContact).val('');
            $('#tblcontact'+totalTextboxContact).hide();$('#tblcontact'+totalTextboxContact).hide();
            totalTextboxEadd -= 1;
            $('#addContact').removeClass('disabled');
        });
        $('#btnContactremove5').click(function() {
            $('#txtcontact'+totalTextboxContact).val('');
            $('#tblcontact'+totalTextboxContact).hide();$('#tblcontact'+totalTextboxContact).hide();
            totalTextboxContact -= 1;
            $('#addContact').removeClass('disabled');
        });

        // End Add Contact

        // Multiple Email
        var emailCount = parseInt('<?=$emailCount?>');
        var totalTextboxEadd = 0;
        if(emailCount == 0)
            totalTextboxEadd = 1;
        else
            totalTextboxEadd = emailCount;
        
        for (var i = emailCount+1; i <= 5; i++) {
            $('#txtemail'+i).hide();$('#tbl'+i).hide();
        }

        $('#addEmail').click(function() {
            totalTextboxEadd += 1;
            for (var i = 2; i <= 5; i++) {
              if($('#tbl'+i).is(":visible") == false){
                  $('#txtemail'+i).show();
                  $('#tbl'+i).show();
                  break;
              }
            }
            if(totalTextboxEadd == 5){
                $('#addEmail').addClass('disabled');
            }
        });

        $('#btnremove2').click(function() {
            $('#txtemail2').val('');
            $('#txtemail2').hide();$('#tbl2').hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        $('#btnremove3').click(function() {
            $('#txtemail3').val('');
            $('#txtemail3').hide();$('#tbl3').hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        $('#btnremove4').click(function() {
            $('#txtemail5').val('');
            $('#txtemail5').hide();$('#tbl5').hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        $('#btnremove5').click(function() {
            $('#txtemail5').val('');
            $('#txtemail5').hide();$('#tbl5').hide();
            totalTextboxEadd -= 1;
            $('#addEmail').removeClass('disabled');
        });
        // Multiple Email

         /*Upload scientist picture*/
        var readURL = function(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.profile-pic').attr('src', e.target.result);
                }
        
                reader.readAsDataURL(input.files[0]);
            }
        }
        

        $(".file-upload").on('change', function(){
            readURL(this);
        });
        
        $(".profile-pic").on('click', function() {
           $(".file-upload").click();
        });

        $('#wizard').smartWizard();

        $('#wizard_verticle').smartWizard({
          transitionEffect: 'slide'
        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
    });
</script>

<script>
  $(document).ready(function() {
        $('#chkPresent').change(function(){
            $("#txtdto").prop("disabled", $(this).is(':checked'));
        });
  });
</script>