
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
	<div class="x_content">
	    <form class="form-horizontal form-label-left" action="<?=base_url('scientists/editServiceAwardsProfile')?>" method="post" >
    	    <div class="form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Classification<span class="required">*</span></label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbSATitle" class="form_control">
                        <option></option>
                        <option value="FTA" >First Time Applicant</option>
                        <?php
                        for($a=1;$a<10;$a++)
						{
						$showNumber = getNumbering($a);	
						?>
                        <option value="<?=$a?>" ><?=$a.$showNumber?> Subsequent</option>
                        <?php
                        }
						?>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date of Approval<span class="required">*</span></label>
                <div class="col-sm-3">
                	<input id="txtApprovalDate" class="form-control " name="txtApprovalDate" required type="text" value="<?php echo $arrServiceAwards[0]["srv_approval_date"];?>">
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Request for change in schedule</label>
                <div class="col-sm-3">
           			    <select name="cmbSAChangeSched" class="form_control">
                        <option></option>
                        <option value="Extension">Extension</option>
                        <option value="Shortening">Shortening</option>
                        <option value="Deferral">Deferral</option>
                    </select> 	
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Change Schedule Date of Approval</label>
                <div class="col-sm-3">
                	<input id="txtChangeSchedApprovalDate" class="form-control " name="txtChangeSchedApprovalDate" required type="text" value="<?php echo $arrServiceAwards[0]["srv_approval_date"];?>">
                </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country of Origin</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbSACountry" class="form_control" required>
                        <option>Choose Option</option>
                        <?php foreach($arrCountry as $sacountry):?>
                        <option value="<?=$sacountry["glo_id"]?>" <?php echo $arrServiceAwards[0]["srv_glo_id"]==$sacountry["glo_id"]?"selected":"";?>><?=$sacountry["glo_country"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Citizenship</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbSACitizenship" class="form_control" required>
                        <option>Choose Option</option>
                        <?php foreach($arrCitizenship as $citizenship):?>
                        <option value="<?=$citizenship["cit_id"]?>" <?php echo $arrServiceAwards[0]["srv_cit_id"]==$citizenship["cit_id"]?"selected":"";?>><?=$citizenship["cit_particulars"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type of Award</label>
                <div class="col-sm-3">
           			    <select name="cmbSATypeofAward" class="form_control">
                        <option></option>
                        <option value="Short">Short Term</option>
                        <option value="Long">Long Term</option>
                    </select> 	
                </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Council</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbSACouncil" class="form_control" required>
                        <option>Choose Option</option>
                        <?php foreach($arrCouncil as $council):?>
                        <option value="<?=$council["cil_id"]?>" <?php echo $arrServiceAwards[0]["srv_cil_id"]==$council["cil_id"]?"selected":"";?>><?=$council["cil_desc"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status</label>
                <div class="col-sm-3">
           			<select name="cmbSAStatus" class="form_control">
                        <option></option>
                        <option value="New">New</option>
                        <option value="Ongoing">Ongoing</option>
                        <option value="Continuing">Continuing</option>
                        <option value="Completed">Completed</option>
                        <option value="Completed">Disapproved</option>
                        <option value="Completed">Repatriated</option>
                    </select> 	
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">If status completed</label>
               	<label class="control-label col-md-2" for="name">Year Completed</label>
                <div class="col-sm-3">
           			   <input id="txtYearCompleted" class="form-control " name="txtYearCompleted" required type="text" value="<?php echo $arrServiceAwards[0]["srv_year_completed"];?>"> 	
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
               	<label class="control-label col-md-2" for="name">With Exit Report?</label>
                <div class="col-sm-3">
           			<select name="cmbSAERP" class="form_control">
                        <option></option>
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select> 		
                </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
               	<label class="control-label col-md-2" for="name">If with exit report</label>
                <label class="control-label col-md-2" for="name">Date of ERP</label>
                <div class="col-sm-3">
           			<input id="txtDateOfERP" class="form-control " name="txtDateOfERP" type="text" value="<?php echo $arrServiceAwards[0]["srv_erp_date"];?>">
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
               	<label class="control-label col-md-2" for="name"></label>
                <label class="control-label col-md-2" for="name">Venue</label>
                <div class="col-sm-3">
           			<input id="txtERPVenue" class="form-control " name="txtERPVenue"  type="text" value="<?php echo $arrServiceAwards[0]["srv_erp_venue"];?>">
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
               	<label class="control-label col-md-2" for="name"></label>
                <label class="control-label col-md-2" for="name">No. of Participants</label>
                <div class="col-sm-3">
           			<input id="txtERPNoOfParticipants" class="form-control " name="txtERPNoOfParticipants" type="text" value="<?php echo $arrServiceAwards[0]["srv_erp_participants"];?>">
                </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
               	<label class="control-label col-md-2" for="name"></label>
                <label class="control-label col-md-2" for="name">Attachment</label>
                <div class="col-sm-3">
           			<input id="txtERPNoOfParticipants" class="form-control " name="txtERPNoOfParticipants" type="file" value="<?php echo $arrServiceAwards[0]["srv_erp_participants"];?>">
                </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
               	<label class="control-label col-md-2" for="name">If without exit report</label>
                <label class="control-label col-md-2" for="name">Reason for Delay</label>
                <div class="col-sm-3">
           			<input id="txtERPReasonForDelay" class="form-control " name="txtERPReasonForDelay" type="text" value="<?php echo $arrServiceAwards[0]["srv_erp_reason"];?>">
                </div>
            </div>
              <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">If status repatriated</label>
               	<label class="control-label col-md-2" for="name">Year Completed</label>
                <div class="col-sm-3">
           			   <input id="txtYearRepatriated" class="form-control " name="txtYearRepatriated" required type="text" value="<?php echo $arrServiceAwards[0]["srv_year_repatriated"];?>"> 	
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type of contract</label>
                <div class="col-sm-3">
           			<select name="cmbTypeOfContract" class="form_control">
                        <option></option>
                        <option value="Continous">Continous</option>
                        <option value="Staggered/Non-continous">Staggered/Non-continous</option>
                    </select> 		
                </div>
            </div>
            
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Duration/Inclusive Dates</label>
                <div class="col-sm-3">
           			<select name="cmbTypeOfContract" class="form_control">
                        <option></option>
                        <option value="Continous">Continous</option>
                        <option value="Staggered/Non-continous">Staggered/Non-continous</option>
                    </select> 		
                </div>
            </div>
            
            
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                <input type="hidden" name="txtScientistId" value="<?php echo $arrScientist[0]["sci_id"];?>"/>
                  <button type="submit" class="btn btn-primary" name="btnSubmitEditProfile" value="cancel">Reset</button>
                  <button id="add" type="submit" class="btn btn-success" name="btnSubmitEditProfile" value="editProfile">Update</button>
                </div>
              </div>          
         </form>
     </div>
   </div>
</div>