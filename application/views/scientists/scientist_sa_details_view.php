<div class="row">
	<div class=" pull-right ">
		<a href="<?=base_url('scientists/edit/'.$arrScientist[0]['sci_id']);?>" class='btn btn-default'>BACK</a>
	</div>
</div>
 <div class="accordion" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel">
	            <a class="panel-heading collapsed" role="tab" id="headingprofile" data-toggle="collapse" data-parent="#accordion" href="#profile" aria-expanded="true" aria-controls="profile">
    	          <h4 class="panel-title">Profile</h4>
        	    </a>
            	<div id="profile" class="panel-collapse collapse" role="tabpanel" aria-labelledby="profile">
              		<div class="panel-body">
                		<?=$strServiceAwardProfile?>
              		</div>
            	</div>
          </div>
          
          <div class="panel">
 	          <a class="panel-heading collapsed" role="tab" id="headingtarget" data-toggle="collapse" data-parent="#accordion" href="#target" aria-expanded="true" aria-controls="target">
    	          <h4 class="panel-title">Target</h4>
        	    </a>
            	<div id="target" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingtarget">
              		<div class="panel-body">
                		<?=$strServiceAwardTargets?>
              		</div>
            	</div>
          </div>
          
          
          <div class="panel">
 	          <a class="panel-heading collapsed" role="tab" id="headingaccomplishments" data-toggle="collapse" data-parent="#accordion" href="#accomplishments" aria-expanded="true" aria-controls="accomplishments">
    	          <h4 class="panel-title">Accomplishments</h4>
        	    </a>
            	<div id="accomplishments" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingaccomplishments">
              		<div class="panel-body">
               			 <?=$strServiceAwardAccomplishments?>
              		</div>
            	</div>
          </div>
          
          
          
          <div class="panel">
 	          <a class="panel-heading collapsed" role="tab" id="headingreports" data-toggle="collapse" data-parent="#accordion" href="#reports" aria-expanded="true" aria-controls="reports">
    	          <h4 class="panel-title">Reports</h4>
        	    </a>
            	<div id="reports" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingreports">
              		<div class="panel-body">
                		<?=$strServiceAwardReports?>
              		</div>
            	</div>
          </div>
</div>