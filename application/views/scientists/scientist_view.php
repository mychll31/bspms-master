
<!-- jQuery -->
<!-- Bootstrap -->
<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
</style>

<?php if(checkAccess($_SESSION['sessAccessLevel'], 'delete_scientist')>0): ?>
  <style type="text/css">
  table tbody td:last-child{
      background-color: #fff !important; 
  }
  td.sorting {
      display: none;
  }
  </style>
<?php endif; ?>

<?php echo isset($strNotification)?$strNotification:""; ?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
   <div id="elscientistList">
    <div class="x_content">
      <span class="section">
        List of Balik Scientist Awardees
        <!-- scientists/add -->
        <a class="btn btn-primary pull-right" href="<?=base_url('addScientistsProfile')?>"><span class="fa fa-user-plus"></span> Add Scientist</a>
        <br><br>
      </span>
      <?php if($arrScientists): $count = 0; ?>
        <div class="form-group">
          <div class="col-sm-12">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Title</th>
                  <th>Name of Balik Scientist</th>
                  <th>Gender</th>
                  <th>Age</th>
                  <th>Area of Expertise</th>
                  <th>Specialization</th>
                  <th>Profession</th>
                  <?php if(checkAccess($_SESSION['sessAccessLevel'], 'delete_scientist')>0): ?>
                    <th>Council</th>
                    <td></td>
                  <?php endif; ?>
                </tr>
              </thead>
              <tbody>
                <?php foreach($arrScientists as $scientists): $count ++; ?>
                <tr data-id="<?=$scientists['sci_id']?>" title="Click to edit">
                  <td><?= $count;?></td>
                  <td><?=ucfirst($scientists['tit_abbreviation'])?></td>
                  <td>
                    <?php 
                      $ext = '';
                      if($scientists['sci_ext_name'] == null or $scientists['sci_ext_name'] == ''){ $ext = ''; }else{ $ext = ' '.$scientists['sci_ext_name'];}
                      echo getFullname($scientists['sci_first_name'], $scientists['sci_last_name'], $scientists['sci_middle_name'], $scientists['sci_middle_initial']);
                     ?>
                  </td>
                  <td><?=($scientists['sci_gender'] == 'M')? 'Male' : 'Female'?></td>
                  <td><?=date_diff(date_create($scientists['sci_birthdate']), date_create('today'))->y?></td>
                  <td>
                    <?php 
                        $expert = explode('|', $scientists['sci_expertise_id']);
                        foreach($expert as $key){
                            if($key != ''){
                                $exp = getByDesc($key, $arrExpertise, 'exp_id');
                                echo '<li>'.ucfirst($exp['exp_desc']).'</li>';
                            }
                        }
                     ?>  
                  </td>
                  <td>
                      <?php 
                        $spe = explode('|', $scientists['sci_specialization_id']);
                        foreach($spe as $key){
                            if($key != ''){
                                $espN = getByDesc($key, $arrSpecialization, 'spe_id');
                                echo '<li>'.$espN['spe_desc'].'</li>';
                            }
                        }
                     ?>
                  </td>
                  <td><?=$scientists['prof_desc']?></td>
                  <?php if(checkAccess($_SESSION['sessAccessLevel'], 'delete_scientist')>0): ?>
                    <th>
                        <?php 
                            if($scientists['usr_council']==1)
                              echo 'PCAARD';
                            else if($scientists['usr_council']==2)
                              echo 'PCHRD';
                            else if($scientists['usr_council']==3)
                              echo 'PCIEERD';
                            else
                              echo '';
                         ?>
                    </th>
                    <td align="center"><a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                  <?php endif; ?>
                </tr>
                <?php endforeach;?>
              </tbody>
            </table>
          </div>
        </div>
      <?php endif;?>
      </div>
    </div>
   </div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('scientists/deleteScientist')?>" method="post">
                <input type="hidden" name="txtempid" id="txtempid"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/others.js')?>"></script>

 <script>
  $(document).ready(function() {
    // delete scientist
    $("#example tr td:last-child").click(function(){
        $('#txtempid').val($(this).closest("tr").data("id"));
    });
    // data tables
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
		$("#example tr td:not(:last-child)").click(function(){
        window.location = "<?=base_url().'scientists/edit/'?>" + $(this).closest("tr").data("id");
        // console.log($(this).closest("tr").data("id"));
    });
		
		$(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
		
	});
</script>
