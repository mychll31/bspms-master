<?
if(isset($arrAccSeminar))
{
	
	$hiddenId = "<input type='hidden' = name='txtRepId' value='".$arrServiceReport[0]['srp_id']."'>";
	$progressreport = $arrServiceReport[0]["srp_progress"];
	$progressreportdate = $arrServiceReport[0]["srp_progress_date"];
	$terminal = $arrServiceReport[0]["srp_terminal"];
	$terminal_date = $arrServiceReport[0]["srp_terminal_date"];
	$feedback = $arrServiceReport[0]["srp_bspfeedback"];
	$feedbackdate = $arrServiceReport[0]["srp_bspfeedback_date"];
	$hostfeedback = $arrServiceReport[0]["srp_feedback"];
	$hostfeedbackdate = $arrServiceReport[0]["srp_feedback_date"];
	$evaluation = $arrServiceReport[0]["srp_evaluation"];
	$evaluationdate = $arrServiceReport[0]["srp_evaluation_date"];
	$implementation = $arrServiceReport[0]["srp_implementation"];
	$implementationdate = $arrServiceReport[0]["srp_implementation_date"];
}
else
{
	$progressreport = "";
	$progressreportdate = "";
	$terminal = "";
	$terminal_date = "";
	$feedback = "";
	$feedbackdate = "";
	$hostfeedback = "";
	$hostfeedbackdate = "";
	$evaluation = "";
	$evaluationdate = "";
	$implementation = "";
	$implementationdate = "";

	$hiddenId = "<input type='hidden' = name='txtScientistId' value='".$arrServiceAwards[0]['srv_id']."'>";
}

?>
<label for="name">Seminars/Lectures/Forums</label>
<div class="form-group">
	<div class="col-sm-12">
    	<table class="table table-striped">
            <tr>
                <th></th>
                <th>Field</th>
                <th>Title</th>
                <th>Date</th>
                <th>Venue</th>
                <th>Type of Participants</th>
                <th>No. of Participants</th>
                <th>List of Participants</th>
                <th>Output</th>
                <th></th>
                <th></th>
            </tr>
            <tbody>
            <?php 
			$count=0;
			foreach($strAccSeminarsData as $seminars):
            $count ++;
            ?>
            <tr>
                <td><?= $count;?></td>
                <td></td>
                <td><?=$seminars['sem_field']?></td>
                <td><?=$seminars['sem_title']?></td>
                <td><?=$seminars['sem_date']?></td>
                <td><?=$seminars['sem_venue']?></td>
                <td><?=$seminars['sem_participants']?></td>
                <td><?=$seminars['sem_noparticipants']?></td>
                <td><?=$seminars['sem_attachment']?></td>
                <td><?=$seminars['sem_output']?></td>
                <td><a href="<?=base_url('scientists/serviceAward/'.$arrScientist[0]["sci_id"]).'/'.$seminars['sem_id'] ?>"><i class="fa fa-edit"></i></a></td>
                <td><a href="#"><i class="fa fa-trash"></i></a></td>
                
            </tr>
            <?php endforeach;?>
            </tbody>
            
        </table>
    </div>
    
    
    <div class="col-sm-12">
    	 <form class="form-horizontal form-label-left" action="<?=base_url('')?>" method="post" >
    	  <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Field</label>
               <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbLevel" class="form_control" required>
                        <option>Choose Option</option>
                        <?php foreach($arrFields as $fields):?>
                        <option value="<?=$fields["fld_id"]?>"><?=$fields["fld_desc"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Title</label>
                <div class="col-sm-6">
                	<input id="txtAccTitle" class="form-control" name="txtAccTitle" type="text">
                </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Date</label>
                <div class="col-md-3">
                   <input id="txtAccDate" class="form-control" name="txtAccDate" type="text">
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Type of Participants</label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbAccParticipants" class="form_control" required>
                        <option>Choose Option</option>
                       <?php foreach($arrParticipants as $participants):?>
                        <option value="<?=$participants["par_id"]?>"><?=$participants["par_desc"]?></option>
                        <?php endforeach;?>
                    </select>
                 </div>
            </div>
            
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">List of Participants</label>
                 <div class="col-sm-3">
                	<input id="txtListParticipants" class="form-control" name="txtListParticipants" type="file">
                </div>
            </div>
             <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Output</label>
                <div class="col-md-3">
                   <input id="txtAccOutput" class="form-control" name="txtAccOutput" type="text">
                 </div>
            </div>
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-12 col-md-offset-5">
                  <button type="submit" class="btn btn-primary" name="btnSubmitEducation" value="cancel">Reset</button>
                  <button id="add" type="submit" class="btn btn-success" name="btnSubmitEducation" value="add">Add</button>
                </div>
              </div>          
         </form>
    </div> 
</div>