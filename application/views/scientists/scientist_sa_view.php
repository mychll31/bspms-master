<style>
.daterangepicker{z-index:1151 !important;}
#tblsciService tr {
    cursor: pointer;
}
#tblsciService tbody tr.highlight td {
    background-color: #ccc;
}
#tblsciService tbody tr:hover{
    background-color: #ccc !important; 
}
#tblsciService tbody td:last-child{
    background-color: #fff !important; 
}
/*table tbody td:nth-last-child(2){
    background-color: #fff !important; 
}*/
</style>
<?=isset($strNotification)?$strNotification:""?>
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_content">
            <span class="section col-md-6">
                List of Service Awards
                <div class="pull-right"><a href="<?=base_url('scientists/addServiceAward/'.$arrScientist[0]['sci_id']).'?subs='.($arrTotalSciAwards[0]['awards']+1);?>" class="btn btn-primary">Add Service Award</a></div>
            </span>
            <?php if($arrServiceAwards): $count = 0; ?>
                <div class="form-group">
                    <div class="col-sm-12">
                        <table id="tblsciService" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Classification</th>
                                    <th>Date of Approval</th>
                                    <th>Country of Origin</th>
                                    <th>Type of Award</th>
                                    <th>Monitoring Council</th>
                                    <th>Type of Contract</th>
                                    <th>Duration</th>
                                    <th>Host Institution</th>
                                    <th class="nosort">Dost<br>Outcomes</th>
                                    <th class="nosort">Dost<br>Priority Areas</th>
                                    <th class="nosort"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $subs=$arrTotalSciAwards[0]['awards']; 
                                      foreach($arrServiceAwards as $serviceAward): $count ++;?>
                                    <tr data-id="<?=$serviceAward['srv_id']?>" data-subs="<?=$subs?>">
                                        <td><?=$count;?></td>
                                        <td><?=($subs > 1)? getClassification($subs) : 'First Time Applicant'?></td>
                                        <td><?=($serviceAward['srv_isyearonly']==0) ? date("Y-m-d", strtotime($serviceAward['srv_approval_date'])) : $serviceAward['srv_approval_yr']?></td>
                                        <td><?=$serviceAward['glo_country']?></td>
                                        <td><?=getAwardtype($serviceAward['srv_typeofaward'])?></td>
                                        <td><?=$serviceAward['cil_code']?></td>
                                        <td><?=getContracttype($serviceAward['srv_type_contract'])?></td>
                                        <td><?=($serviceAward['srv_type_contract'] == 0) ? date("Y-m-d", strtotime($serviceAward['srv_cont_startDate'])) : date("Y-m-d", strtotime($serviceAward['contract_fromDate']))?>
                                             to <?=($serviceAward['srv_type_contract'] == 0) ? date("Y-m-d", strtotime($serviceAward['srv_cont_endDate'])) : date("Y-m-d", strtotime($serviceAward['contract_fromTo']))?></td>
                                        <td><?=$serviceAward['ins_desc']?></td>
                                        <td>
                                            <?php
                                                $out = '';
                                                $sa_outcomes = explode('|', $serviceAward['srv_out_id']);
                                                foreach($sa_outcomes as $key){
                                                    if($key != ''){
                                                        $outcom = getByDesc($key, $arrOutcomes, 'out_id');
                                                        $out .= '<li>'.$outcom['out_desc'].'</li>';
                                                    }
                                                }
                                                echo substr($out,0,90)."...";
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                $pareas = '';
                                                $prio_areas = explode('|', $serviceAward['srv_pri_id']);
                                                foreach ($prio_areas as $pkey) {
                                                    if($pkey != ''){
                                                        $priority = getByDesc($pkey, $arrPriority, 'pri_id');
                                                        $pareas .= '<li>'.$priority['pri_desc'].'</li>';
                                                    }
                                                }
                                                echo substr($pareas,0,20)."...";
                                                $subs--;
                                             ?>
                                        </td>
                                        <td align="center"><a data-toggle="modal" data-target="#deleteSciawards" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade" id="deleteSciawards" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <form action="<?=base_url('serviceawards/removeServiceAwards')?>" method="post">
            <input type="hidden" name="txtsrvid" id="txtsrvid"></input>
            <input type="hidden" value="<?=$this->uri->segment(3)?>" name="txtsciid" id="txtsciid"></input>
            Are you sure you want to delete this data?
            <br><br>
            <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
            <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<script>
$(document).ready(function() {
    // delete scientist
    $("tr td:last-child").click(function(){
        $('#txtsrvid').val($(this).closest("tr").data("id"));
    });
    // data tables
    $("#tblsciService tr td:not(:last-child)").click(function(){
        window.location = "<?=base_url().'scientists/editServiceAward/'?>" + $(this).closest("tr").data("id") + '?subs=' + $(this).closest("tr").data("subs");
        // console.log($(this).closest("tr").data("id"));
    });
    $('#tblsciService').dataTable( {
        "order": [],
        "columnDefs": [ {
          "targets"  : 'nosort',
          "orderable": false,
        }]
    });
});
</script>