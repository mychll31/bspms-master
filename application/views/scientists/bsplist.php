<h1>
	CY <?=$this->uri->segment(3)?>
	<br>
	BSP AWARDEES CY <?=$this->uri->segment(3)?>
</h1>

<table class="table table-striped">
  <thead><tr><td align="center" colspan="2"><b>SHORT-TERM CATEGORY</b></td></tr></thead>
  <tbody>
	<tr>
		<td><b>Name Scientist</b></td>
		<td><b>Expertise</b></td>
	</tr>
		<?php foreach ($arrScientists as $scientist): ?>
			<tr>
				<td><a target="_" href="<?=base_url('scientists/bspawardeesPdf/'.$scientist['sci_id'])?>"><?=getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial'])?></a></td>
				<td>
					<?=getExpertiseName($scientist['sci_expertise_id'])?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
  <thead><tr><td align="center" colspan="2"><b>LONG-TERM CATEGORY</b></td></tr></thead>
  <tbody>
	<tr>
		<td><b>Name Scientist</b></td>
		<td><b>Expertise</b></td>
	</tr>
		<?php foreach ($arrScientists1 as $scientist): ?>
			<tr>
				<td><a target="_" href="<?=base_url('scientists/bspawardeesPdf/'.$scientist['sci_id'])?>"><?=getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial'])?></a></td>
				<td>
					<?=getExpertiseName($scientist['sci_expertise_id'])?>
				</td>
			</tr>
		<?php endforeach; ?>	
  </tbody>
</table>