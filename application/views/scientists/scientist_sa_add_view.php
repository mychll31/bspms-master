  <script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
  <script type="text/javascript" src="<?=base_url('assets/production/js/jquery.maskedinput.min.js')?>"></script>
  <link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">

  <?php
    $sciSA = array(
        'class'             => '',
        'class_approval'    => '',
        'chg_sched'         => '',
        'chgsched_approval' => '',
        'country'           => '',
        'citizen'           => '',
        'award'             => '',
        'council'           => '',
        'contract'          => '',
        'institution'       => '',
        'outcomes'          => '',
        'priority'          => '',
        'approvedday'       => '',
        'constartDate'      => '',
        'conendDate'        => '',
        'yrcompleted'       => '',
        'witherp'           => '',
        'erpdate'           => '',
        'erpvenue'          => '',
        'erpnopart'         => '',
        'erpreason'         => '',
        'yrrepatriated'     => '',
        'processby'         => '',
        'processdate'       => '',
        'processremarks'    => '',
        'isyearonly'        => '',
        'srvyearapproval'   => '',
      );
    if($this->arrTemplateData['arrServiceAwards'] !=null){
        $sciSA['class']             = $this->arrTemplateData['arrServiceAwards'][0]['srv_classification'];
        $sciSA['class_approval']    = $this->arrTemplateData['arrServiceAwards'][0]['srv_approval_date'];
        $sciSA['chg_sched']         = $this->arrTemplateData['arrServiceAwards'][0]['srv_change_sched'];
        $sciSA['chgsched_approval'] = $this->arrTemplateData['arrServiceAwards'][0]['srv_sched_approvaldate'];
        $sciSA['country']           = $this->arrTemplateData['arrServiceAwards'][0]['srv_glo_id'];
        $sciSA['citizen']           = $this->arrTemplateData['arrServiceAwards'][0]['srv_cit_id'];
        $sciSA['award']             = $this->arrTemplateData['arrServiceAwards'][0]['srv_typeofaward'];
        $sciSA['council']           = $this->arrTemplateData['arrServiceAwards'][0]['srv_cil_id'];
        $sciSA['status']            = $this->arrTemplateData['arrServiceAwards'][0]['srv_status'];
        $sciSA['contract']          = $this->arrTemplateData['arrServiceAwards'][0]['srv_type_contract'];
        $sciSA['institution']       = $this->arrTemplateData['arrServiceAwards'][0]['srv_ins_id'];
        $sciSA['outcomes']          = $this->arrTemplateData['arrServiceAwards'][0]['srv_out_id'];
        $sciSA['priority']          = $this->arrTemplateData['arrServiceAwards'][0]['srv_pri_id'];
        $sciSA['approvedday']       = $this->arrTemplateData['arrServiceAwards'][0]['srv_approved_days'];
        $sciSA['constartDate']      = $this->arrTemplateData['arrServiceAwards'][0]['srv_cont_startDate'];
        $sciSA['conendDate']        = $this->arrTemplateData['arrServiceAwards'][0]['srv_cont_endDate'];
        $sciSA['yrcompleted']       = $this->arrTemplateData['arrServiceAwards'][0]['srv_year_completed'];
        $sciSA['witherp']           = $this->arrTemplateData['arrServiceAwards'][0]['srv_with_erp'];
        $sciSA['erpdate']           = $this->arrTemplateData['arrServiceAwards'][0]['srv_erp_date'];
        $sciSA['erpvenue']          = $this->arrTemplateData['arrServiceAwards'][0]['srv_erp_venue'];
        $sciSA['erpnopart']         = $this->arrTemplateData['arrServiceAwards'][0]['srv_erp_participants'];
        $sciSA['erpreason']         = $this->arrTemplateData['arrServiceAwards'][0]['srv_erp_reason'];
        $sciSA['yrrepatriated']     = $this->arrTemplateData['arrServiceAwards'][0]['srv_year_repatriated'];
        $sciSA['processby']         = $this->arrTemplateData['arrServiceAwards'][0]['srv_processed_by'];
        $sciSA['processdate']       = $this->arrTemplateData['arrServiceAwards'][0]['srv_processed_date'];
        $sciSA['processremarks']    = $this->arrTemplateData['arrServiceAwards'][0]['srv_processed_remarks'];
        $sciSA['isyearonly']        = $this->arrTemplateData['arrServiceAwards'][0]['srv_isyearonly'];
        $sciSA['srvyearapproval']   = $this->arrTemplateData['arrServiceAwards'][0]['srv_approval_yr'];
    } 

    $outcomes_arr = explode('|',$sciSA['outcomes']);
    $institutions_arr = explode('|',$sciSA['institution']);
    $priority_arr = explode('|',$sciSA['priority']);
    
    $actions = '';
    if($this->uri->segment(2) == 'editServiceAward')
      $actions = 'scientists/editServiceAward/'.$this->uri->segment(3);
    else
      $actions = 'scientists/addServiceAward/'.$this->uri->segment(3);
    $condatesfrom = array();
    $condatesto = array();
    $condatesTotal = 0;
    if(isset($arrContractDates)){
      if($arrContractDates!=''){
        foreach ($arrContractDates as $contractDate) {
            $condatesfrom[$condatesTotal] = $contractDate['con_date_from'];
            $condatesto[$condatesTotal] = $contractDate['con_date_to'];
            $condatesTotal += 1;
        }
      }
    }
   ?>
  <style type="text/css">
      a.btnremove {
          color: red;
          font-weight: bold;
      }
      a.btnremove:hover, a.btnremove:focus {
          text-decoration: none;
          color: #a94444;
      }
      .tbldata tbody td:last-child{
          background-color: #fff !important; 
      }
      td.sorting {
          display: none;
      }
      .nav-tabs>li {
          margin-bottom: -2px;
      }
    .daterangepicker{z-index:1151 !important;}
    .tbldata tr {
        cursor: pointer;
    }
    .tbldata tbody tr.highlight td {
        background-color: #ccc;
    }
    .tbldata tbody tr:hover{
        background-color: #ccc !important; 
    }
    li button {
      font-weight: bold;
      background: none;
      border: 0;
      color: red;
      padding: 0;
      float: right;
      margin-left: 10px;
    }
    .side-block {
      font-weight: normal;
      font-style: italic;
    }
    ol.breadcrumb {
      background-color: #fff;
      margin-bottom: 0px;
      font-style: italic;
    }
    .erp_error{
      color: #a94442;
    }
  </style>
  <?=isset($strNotification)?$strNotification:"";?>
  <div class="col-md-12">
    <div class="x_panel" id="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-child"></i> Scientist's Profile</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div class="" role="tabpanel" data-example-id="togglable-tabs">
          <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
            <?php if($this->uri->segment(2) == 'editServiceAward'):?>
            <li role="presentation" class=""><a href="<?=base_url().'scientists/edit/'.$this->arrTemplateData['arrServiceAwards'][0]['srv_sci_id']?>" id="home-tab" role="tab">Information</a></li>
            <?php else: ?>
            <li role="presentation" class=""><a href="<?=base_url().'scientists/edit/'.$this->uri->segment(3)?>" id="home-tab" role="tab">Information</a></li>
            <?php endif; ?>
            <li role="presentation" class="active"><a href="#tab_content4" role="tab" id="profile-tab3" data-toggle="tab" aria-expanded="false">Service Awards</a></li>
          </ul>
          <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <ol class="breadcrumb">
                <?php if($this->uri->segment(2) == 'editServiceAward'):?>
                  <li><a href="<?=base_url().'scientists/edit/'.$this->arrTemplateData['arrServiceAwards'][0]['srv_sci_id']?>/srv">List of Service Awards</a></li>
                  <li class="active"><?=($_GET['subs'] == "1") ? 'First Time Applicant' : $_GET['subs'].getNumbering($_GET['subs']).' Subsequent'?></li>
                  <?php else: ?>
                  <li><a href="<?=base_url().'scientists/edit/'.$this->uri->segment(3)?>/srv">List of Service Awards</a></li>
                  <li class="active">Add Service Award</li>
                  <?php endif; ?>
                </ol>
                <!-- begin General Information -->
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                      <a role="button" style="display: inherit;" data-toggle="collapse" data-parent="#accordion" href="#genInfo" aria-expanded="true" aria-controls="genInfo">
                        General Information
                      </a>
                    </h4>
                  </div>
                  <div id="genInfo" class="panel-collapse collapse <?=($this->uri->segment(4) == '' ? 'in' : '')?>" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                    <!-- begin srvContractDur -->
                    <div id="srvContractDur" v-cloak>
                        <form id="frmsrvawards" name="frmsrvawards" class="form-horizontal form-label-left" action="<?=base_url($actions)?>" method="post">
                            <input type="hidden" value="<?=$sciSA['isyearonly']?>" id="txtisyronly" v-model="txtisyronly" name="txtisyronly">
                            <input type="hidden" value="<?=$_GET['subs']?>" name="txtsubs">
                            <input type="hidden" value="<?=$condatesTotal?>" v-model="condatesTotal">
                            <input type="hidden" value="<?=$this->uri->segment(2)?>" id="modalaction">
                            <div class="form-group {{ haserror.dateofApproval }}" id="txtApprovalDatediv">
                                <label for="txtApprovalDate" class="col-sm-2 control-label">Date of Approval <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                    <table width="100%"><tr><td style="vertical-align: top !important; width: 75% !important">
                                      <div id="divaprdate"><input type='text' style="width: 95% !important" class="form-control" id='txtApprovalDate' name='txtApprovalDate' v-model="dateofApproval" placeholder="YYYY-MM-DD" value="<?=$sciSA['class_approval']?>"/></div>
                                      <div id="divapryear"><input type='text' style="width: 95% !important" class="form-control" id='txtapprovalyear' name='txtapprovalyear' v-model="txtapprovalyear" placeholder="YYYY" value="<?=$sciSA['srvyearapproval']?>"/></div>
                                    </td><td>
                                      <input type="checkbox" id="approvalyear" v-model="approvalyear" name="approvalyear" value="<?=$sciSA['isyearonly']?>" <?=$sciSA['isyearonly']==1 ? 'checked' : ''?>> Year
                                    </td></tr></table>
                                    <span class="help-block" id="txtApprovalDatespan"></span>
                                  </div>
                            </div>
                            <script type="text/javascript">$(function () { $('#txtApprovalDate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                            <script type="text/javascript">$(function () { $('#txtapprovalyear').datetimepicker({ format: 'YYYY' });});</script>
                            <?php if($this->uri->segment(2) == 'editServiceAward'): ?>
                            <div class="form-group {{ haserror.changeSched }}">
                                <label for="cmbSAChangeSched" class="col-sm-2 control-label">Request for change in schedule </label>
                                  <div class="col-sm-3">
                                      <?php 
                                        $options = array(
                                            ''  => 'Choose Option',
                                            '1' => 'Extension',
                                            '2' => 'Shortening',
                                            '3' => 'Deferral',
                                          );
                                        echo form_dropdown('cmbSAChangeSched', $options, $sciSA['chg_sched'], array('class' => 'form-control', 'v-model' => 'changeSched'));
                                       ?>
                                    <span class="help-block" v-if="ErrorValidation.changeSched">{{ Error.changeSched }}</span>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.changeSchedApproval }}">
                                <label for="txtChangeSchedApprovalDate" class="col-sm-2 control-label">Change Schedule Date of Approval </label>
                                  <div class="col-sm-3">
                                      <input id="txtChangeSchedApprovalDate" class="form-control" name="txtChangeSchedApprovalDate" type="text" v-model="changeSchedApproval" placeholder="YYYY-MM-DD" value="<?=$sciSA['chgsched_approval']?>">
                                      <span class="help-block {{ ErrorValidation.changeSchedApproval }}">{{ Error.changeSchedApproval }}</span>
                                      <script type="text/javascript">$(function () { $('#txtChangeSchedApprovalDate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                                  </div>
                            </div>
                            <?php endif; ?>
                            <div class="form-group form-inline {{ haserror.country }}" id="cmbcountrydiv">
                                <label for="cmbSACountry" class="col-sm-2 control-label">Country of Origin <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                    <table width="100%"><tr><td style="vertical-align: top !important; width: 75% !important">
                                      <div id="elcountry">
                                        <input type="hidden" v-model="baseUrlCountry" value="<?=base_url()?>">
                                        <input type="hidden" v-model="defCountry" value="<?=$sciSA['country']?>">
                                        <select name="cmbSACountry" id="cmbCountry" v-model="selectedCountry" class="form-control" style="width: 99% !important">
                                          <option value="">Choose Option</option>
                                          <option v-repeat="country: countries" value="{{ country.glo_id }}" selected="{{defCountry == country.glo_id}}">{{ country.glo_country }}</option>
                                        </select>
                                        <!-- Begin Country Modal -->
                                        <?php 
                                        $continents = array(
                                            ''              => 'Choose Option',
                                            'Africa'        => 'Africa',
                                            'Asia'          => 'Asia',
                                            'Australia'     => 'Australia',
                                            'Europe'        => 'Europe',
                                            'Middle East'   => 'Middle East',
                                            'North America' => 'North America',
                                            'Oceania'       => 'Oceania',
                                            'South America' => 'South America'
                                          );
                                         ?>

                                        <div class="modal fade bs-example-modal-sm" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                          <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header" style="padding: 15px 15px 0px 15px !important;">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Add Country</h4>
                                              </div>
                                              <form method="POST">
                                              <div class="modal-body" style="padding: 0px 15px 15px 15px !important;">
                                                <div class="form-group {{ haserror.continent }}">
                                                  <label class="control-label" for="cmbContinent">Continent <span class="required">*</span> </label>
                                                  <select name="cmbContinent" class="form-control" v-model="continent" style="width: 122% !important;">
                                                  <?php foreach($continents as $key=>$cont): ?>
                                                    <option value="<?=$key?>"><?=$cont?></option>
                                                  <?php endforeach; ?>
                                                  </select>
                                                  <span class="help-block" v-if="ErrorValidation.continent">{{ Error.continent }}</span>
                                                </div>
                                                <div class="form-group {{ haserror.country }}">
                                                  <label class="control-label" for="txtCountry">Country <span class="required">*</span> </label>
                                                  <select name="txtCountry" v-model="country" class="form-control" style="width: 99% !important">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="countL: countriesList" value="{{ countL }}" >{{ countL }}</option>
                                                  </select>
                                                  <span class="help-block" v-if="ErrorValidation.country">{{ Error.country }}</span>
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" v-on="click: onSubmitForm" class="btn btn-primary">Save changes</button>
                                              </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div>
                                        <!-- End Country Modal -->
                                      </div>
                                      </td><td>
                                      <!-- Button trigger modal country-->
                                      <?php if(checkAccess($_SESSION['sessAccessLevel'], 'libraries')>0): ?><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#countryModal">Add Country</button><?php endif; ?>
                                    </td></tr></table>
                                    <span id="cmbcountryspan" class="help-block" v-if="ErrorValidation.country">{{ Error.country }}</span>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.citizen }}" id="cmbcitizendiv">
                              <label for="cmbSACitizenship" class="col-sm-2 control-label">Citizenship <span class="required">*</span> </label>
                              <div class="col-sm-3">
                                <table width="100%">
                                  <tr><td style="vertical-align: top !important; width: 75% !important">
                                    <div id="elcitizen">
                                        <input type="hidden" v-model="baseUrlCitizen" value="<?=base_url()?>">
                                        <input type="hidden" v-model="defCitizen" value="<?=$sciSA['citizen']?>">
                                        <select name="cmbSACitizenship" id="cmbcitizen" v-model="selectedCitizen" class="form-control" style="width: 99% !important">
                                          <option value="">Choose Option</option>
                                          <option v-repeat="citizen: citizens" value="{{ citizen.cit_id }}" selected="{{defCitizen == citizen.cit_id}}">{{ citizen.cit_particulars }}</option>
                                        </select>
                                        <!-- Begin Citizen Modal -->
                                        <?php 
                                        $type = array(
                                            ''   => 'Choose Option',
                                            '1'   => 'Filipino',
                                            '2'   => 'Foreign',
                                            '3'   => 'Dual Citizen',
                                          );
                                         ?>

                                        <div class="modal fade bs-example-modal-sm" id="modalCitizenship" tabindex="-1" role="dialog" aria-labelledby="modalCitizenshipLabel">
                                          <div class="modal-dialog modal-sm" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header" style="padding: 15px 15px 0px 15px !important;">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="modalCitizenshipLabel">Add Citizen</h4>
                                              </div>
                                              <form method="POST">
                                              <div class="modal-body" style="padding: 0px 15px 15px 15px !important;">
                                                <div class="form-group {{ haserror.type }}">
                                                  <label class="control-label" for="txtCode">Type <span class="required">*</span> </label>
                                                  <select name="txtCode" class="form-control" v-model="type" style="width: 99% !important;">
                                                  <?php foreach($type as $key=>$cont): ?>
                                                    <option value="<?=$key?>"><?=$cont?></option>
                                                  <?php endforeach; ?>
                                                  </select>
                                                  <span class="help-block" v-if="ErrorValidation.type">{{ Error.type }}</span>
                                                </div>
                                                <div class="form-group {{ haserror.desc }}">
                                                  <label class="control-label" for="txtDesc">Nationality(ties) <span class="required">*</span> </label>
                                                  <input type="text" name="txtDesc" v-model="desc" class="form-control" style="width: 99% !important">
                                                  <span class="help-block" v-if="ErrorValidation.desc">{{ Error.desc }}</span>
                                                </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="submit" v-on="click: onSubmitForm" class="btn btn-primary">Save changes</button>
                                              </div>
                                              </form>
                                            </div>
                                          </div>
                                        </div>
                                        <!-- End Citizen Modal -->
                                    </div>
                                  </td><td>
                                    <!-- Button trigger modal -->
                                    <?php if(checkAccess($_SESSION['sessAccessLevel'], 'libraries')>0): ?><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalCitizenship">Add Citizenship</button><?php endif; ?>
                                  </td></tr>
                                </table>
                                <span id="cmbcitizenspan" class="help-block" v-if="ErrorValidation.citizen">{{ Error.citizen }}</span>
                              </div>
                            </div>
                            <div class="form-group {{ haserror.awardType }}">
                                <label for="cmbSATypeofAward" class="col-sm-2 control-label">Type of Award <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                      <?php 
                                        $optAwards = array(
                                            ''  => 'Choose Option',
                                            '0' => 'Short Term',
                                            '1' => 'Long Term',
                                          );

                                        echo form_dropdown('cmbSATypeofAward', $optAwards, $sciSA['award'], array('class' => 'form-control', 'v-model' => 'awardType'));
                                       ?>
                                       <span class="help-block" v-if="ErrorValidation.awardType">{{ Error.awardType }}</span>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.council }}">
                                <label for="cmbSACouncil" class="col-sm-2 control-label">Monitoring Council <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                      <select name="cmbSACouncil" class="form-control" v-model="council">
                                        <option value="">Choose Option</option>
                                        <?php foreach($arrCouncil as $council):?>
                                        <option <?=($sciSA['council'] == $council["cil_id"]) ? 'selected' : ''?> value="<?=$council["cil_id"]?>"><?=$council["cil_desc"]?></option>
                                        <?php endforeach;?>
                                      </select>
                                      <span class="help-block" v-if="ErrorValidation.council">{{ Error.council }}</span>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.status }}">
                                <label for="cmbstatus" class="col-sm-2 control-label">Status <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                      <select name="cmbstatus" class="form-control" v-model="status">
                                        <option value="">Choose Option</option>
                                        <?php foreach($arrstat as $stat):?>
                                            <option <?=($sciSA['status'] == $stat["srv_stat_id"]) ? 'selected' : ''?> value="<?=$stat["srv_stat_id"]?>"><?=$stat["srv_stat_name"]?></option>
                                        <?php endforeach;?>
                                      </select>
                                       <span class="help-block" v-if="ErrorValidation.status">{{ Error.status }}</span>
                                  </div>
                            </div>
                            <!-- Begin with ERP -->
                            <div class="form-group {{ haserror.yrcompleted }}" v-show="status==3" id="yrcompleteddiv">
                                <label for="txtyrcompleted" class="col-sm-2 control-label">Year Completed <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                      <input id="txtyrcompleted" class="form-control" name="txtyrcompleted" type="text" v-model="yrcompleted" placeholder="YYYY" value="<?=$sciSA['yrcompleted']?>">
                                      <script type="text/javascript">$(function () { $('#txtyrcompleted').datetimepicker({ format: 'YYYY' });});</script>
                                      <span class="help-block {{ ErrorValidation.yrcompleted }}" id="yrcompletedspan">{{ Error.yrcompleted }}</span>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.witherp }}" v-show="status==4">
                                <label for="txtyrcompleted" class="col-sm-2 control-label"></label>
                                  <div class="col-sm-6">
                                      <label for="txtyrcompleted" class="{{ erp_error }}">With ERP? <span class="required">*</span> </label>
                                      &nbsp;&nbsp;&nbsp;
                                      <input id="txtwitherp" name="radwitherp" type="radio" v-model="witherp" <?=$sciSA['witherp']==0 ? 'checked' : ''?> value="yes"> <span class="{{ erp_error }}">Yes</span>
                                      &nbsp;&nbsp;&nbsp;
                                      <input id="txtwitherp" name="radwitherp" type="radio" v-model="witherp" <?=$sciSA['witherp']==1 ? 'checked' : ''?> value="no"> <span class="{{ erp_error }}">No</span>
                                      <span class="help-block {{ ErrorValidation.witherp }}">{{ Error.witherp }}</span>
                                  </div>
                            </div>
                            <!-- Begin ERP Modal -->
                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="erpmodal" data-backdrop="static" data-keyboard="false">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    
                                    <h4 class="modal-title" id="myModalLabel">Exit Report Presentation (ERP)</h4>
                                  </div>
                                  <div class="modal-body">
                                    <div class="form-group {{ haserror.erpdate }}" v-show="witherp=='yes'">
                                      <label for="txterpdate" class="col-sm-4 control-label">Date of ERP <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                          <input id="txterpdate" class="form-control" name="txterpdate" type="text" v-model="erpdate" placeholder="YYYY-MM-DD" value="<?=$sciSA['erpdate']?>">
                                          <span class="help-block {{ ErrorValidation.erpdate }}">{{ Error.erpdate }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group {{ haserror.erpvenue }}" v-show="witherp=='yes'">
                                      <label for="txterpvenue" class="col-sm-4 control-label">Venue <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                          <input id="txterpvenue" class="form-control" name="txterpvenue" type="text" v-model="erpvenue" value="<?=$sciSA['erpvenue']?>">
                                          <span class="help-block {{ ErrorValidation.erpvenue }}">{{ Error.erpvenue }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group {{ haserror.erpnopart }}" v-show="witherp=='yes'">
                                      <label for="txterpnopart" class="col-sm-4 control-label">No. of Participants <span class="required">*</span> </label>
                                        <div class="col-sm-3">
                                          <input id="txterpnopart" class="form-control" name="txterpnopart" type="text" v-model="erpnopart" value="<?=$sciSA['erpnopart']?>">
                                          <span class="help-block {{ ErrorValidation.erpnopart }}">{{ Error.erpnopart }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group {{ haserror.erpreason }}" v-show="witherp=='no'">
                                      <label for="txterpreason" class="col-sm-4 control-label">Reason for Delay <span class="required">*</span> </label>
                                        <div class="col-sm-6">
                                          <textarea id="txterpreason" class="form-control" name="txterpreason" type="text" v-model="erpreason" value="<?=$sciSA['erpreason']?>"></textarea>
                                          <span class="help-block {{ ErrorValidation.erpreason }}">{{ Error.erpreason }}</span>
                                        </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" v-on="click: closeerp">Close</button>
                                    <button type="button" class="btn btn-primary" v-on="click: savewitherp">Save changes</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <!-- End ERP Modal -->
                            <!-- Repartriated -->
                            <div class="form-group {{ haserror.yrrepatriated }}" v-show="status==6" id="yrrepatriateddiv">
                                <label for="txtyrrepatriated" class="col-sm-2 control-label">Year <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                      <input id="txtyrrepatriated" class="form-control" name="txtyrrepatriated" type="text" v-model="yrrepatriated" placeholder="YYYY" value="<?=$sciSA['yrrepatriated']?>">
                                      <span class="help-block {{ ErrorValidation.yrrepatriated }}" id="yrrepatriatedspan">{{ Error.yrrepatriated }}</span>
                                      <script type="text/javascript">$(function () { $('#txtyrrepatriated').datetimepicker({ format: 'YYYY' });});</script>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.contractType }}">
                                <label for="cmbTypeOfContract" class="col-sm-2 control-label">Type of contract <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                      <?php 
                                        $optContracts = array(
                                            ''            => 'Choose Option',
                                            '0'           => 'Continous',
                                            '1'           => 'Staggered / Non-continous'
                                          );

                                        echo form_dropdown('cmbTypeOfContract', $optContracts, $sciSA['contract'], array('class' => 'form-control', 'id' => 'cmbTypeOfContract', 'v-model' => 'typecontract'));
                                       ?>
                                       <span class="help-block" v-if="ErrorValidation.contractType">{{ Error.contractType }}</span>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.phases }}">
                              <label for="" class="col-sm-2 control-label">Phases <span class="required">*</span> </label>
                              <div class="col-sm-3">
                                <input type="hidden" name="txtphasesdate" id='txtphasesdate' v-model="phases">
                                <div id="phasesContainer" v-cloak>
                                  <table><tr><td>
                                    <table id="tblphase1"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom1" class="form-control" name="txtContDurFrom" type="text" placeholder="YYYY-MM-DD"
                                    value="<?php
                                            if($sciSA['contract'] == 0){
                                              echo $sciSA['constartDate'];
                                            }else{
                                              if(isset($condatesfrom[0])){
                                                echo $condatesfrom[0];
                                              }else{
                                                echo '';
                                              }
                                            }
                                            ?>">
                                        <span class="input-group-addon">to</span><input id="txtContDurTo1" class="form-control" placeholder="YYYY-MM-DD" name="txtContDurTo" type="text"
                                          value="<?php
                                            if($sciSA['contract'] == 0){
                                              echo $sciSA['conendDate'];
                                            }else{
                                              if(isset($condatesto[0])){
                                                echo $condatesto[0];
                                              }else{
                                                echo '';
                                              }
                                            }
                                            ?>">
                                          </div></td><td>&nbsp;&nbsp;&nbsp;</td></tr></table>
                                    <table id="tblphase2"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom2" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[1]) ? $condatesfrom[1] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo2" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[1]) ? $condatesto[1] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove2" class="btnremove" href="#">X</a></td></tr></table>
                                    
                                    <table id="tblphase3"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom3" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[2]) ? $condatesfrom[2] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo3" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[2]) ? $condatesto[2] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove3" class="btnremove" href="#">X</a></td></tr></table>
                                    <table id="tblphase4"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom4" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[3]) ? $condatesfrom[3] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo4" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[3]) ? $condatesto[3] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove4" class="btnremove" href="#">X</a></td></tr></table>
                                    <table id="tblphase5"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom5" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[4]) ? $condatesfrom[4] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo5" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[4]) ? $condatesto[4] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove5" class="btnremove" href="#">X</a></td></tr></table>
                                    <table id="tblphase6"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom6" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[5]) ? $condatesfrom[5] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo6" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[5]) ? $condatesto[5] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove6" class="btnremove" href="#">X</a></td></tr></table>
                                    <table id="tblphase7"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom7" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[6]) ? $condatesfrom[6] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo7" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[6]) ? $condatesto[6] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove7" class="btnremove" href="#">X</a></td></tr></table>
                                    <table id="tblphase8"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom8" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[7]) ? $condatesfrom[7] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo8" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[7]) ? $condatesto[7] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove8" class="btnremove" href="#">X</a></td></tr></table>
                                    <table id="tblphase9"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom9" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[8]) ? $condatesfrom[8] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo9" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[8]) ? $condatesto[8] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove9" class="btnremove" href="#">X</a></td></tr></table>
                                    <table id="tblphase10"><tr><td><div class="input-group input-daterange" style="width: 99%"><input id="txtContDurFrom10" class="form-control" name="txtContDurFrom" type="text" value="<?=isset($condatesfrom[9]) ? $condatesfrom[9] : ''?>" placeholder="YYYY-MM-DD" >
                                        <span class="input-group-addon">to</span><input id="txtContDurTo10" class="form-control" name="txtContDurTo" type="text" value="<?=isset($condatesto[9]) ? $condatesto[9] : ''?>" placeholder="YYYY-MM-DD" ></div></td><td>&nbsp;<a id="btnremove10" class="btnremove" href="#">X</a></td></tr></table>
                                  </td><td style="vertical-align: top;"><a class="btn btn-warning btn-sm" id="btnaddphase">Add Phase</a></td></tr></table>
                                </div>
                                <span class="help-block" v-if="ErrorValidation.phases">{{ Error.phases }}</span>
                              </div>
                            </div>
                            <script type="text/javascript">
                              $(function () {$('#txtContDurTo1, #txtContDurFrom1, #txtContDurTo2, #txtContDurFrom2, #txtContDurTo3, #txtContDurFrom3, #txtContDurTo4, #txtContDurFrom4, #txtContDurTo5, #txtContDurFrom5, #txtContDurTo6, #txtContDurFrom6, #txtContDurTo7, #txtContDurFrom7, #txtContDurTo8, #txtContDurFrom8, #txtContDurTo9, #txtContDurFrom9, #txtContDurTo10, #txtContDurFrom10')
                                .datetimepicker({ format: 'YYYY-MM-DD' });});
                            </script>

                            <!-- <div class="form-group {{ haserror.approvedDY }}">
                                <label for="txtTotalDaysYears" class="col-sm-2 control-label">{{ messAppDy }} <span class="required">*</span> </label>
                                  <div class="col-sm-3">
                                      <div class="input-group input-daterange">
                                          <input id="txtTotalDaysYears" class="form-control" name="txtTotalDaysYears" type="text" v-model="approvedDY" value="<?=$sciSA['approvedday']?>">
                                          <span class="help-block" v-if="ErrorValidation.approvedDY">{{ Error.approvedDY }}</span>
                                      </div>
                                  </div>
                            </div> -->
                            <div class="form-group {{ haserror.institution }}" id="cmbHostInstitutiondiv">
                                <label for="cmbHostInstitution" class="col-sm-2 control-label">Host Institution <span class="required">*</span> </label>
                                  <div class="col-sm-9">
                                    <div id="elhostinst">
                                      <input type="hidden" id="txtdefInst" v-model="defInstitution" value="<?=$sciSA['institution']?>">
                                      <table><tr><td style="vertical-align: top !important; width: 75% !important">
                                      <select class="select2_multiple form-control" id="cmbInstitutions" name="cmbInstitutions[]" multiple="multiple" v-model="outcomes" style="width: 99% !important">
                                          <option v-repeat="inst: hostInst" value="{{ inst.ins_id }}" selected="{{ arrinst.indexOf(inst.ins_id) > -1 }}">{{ inst.ins_code }} {{ inst.ins_desc }} - {{ inst.itype_desc }}</option>
                                      </select>
                                      <span class="help-block" id="spancmbHostInstitution"></span>
                                      </td><td valign="top">
                                        <!-- Begin Modal Add Host Institution -->
                                          <input type="hidden" value="<?=base_url()?>" v-model="baseUrl">
                                          <div class="modal fade bs-example-modal-sm" id="modalHostInst" tabindex="-1" role="dialog" aria-labelledby="modalCitizenshipLabel">
                                            <div class="modal-dialog modal-sm" role="document">
                                              <div class="modal-content">
                                                <div class="modal-header" style="padding: 15px 15px 0px 15px !important;">
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                  <h4 class="modal-title" id="modalCitizenshipLabel">Add Host Institution</h4>
                                                </div>
                                                <form method="POST">
                                                <div class="modal-body" style="padding: 0px 15px 15px 15px !important;">
                                                  <div class="form-group {{ haserror.instype }}" style="margin-bottom: 0px !important;">
                                                    <label class="control-label" for="txtinstype">Institution Type <span class="required">*</span> </label>
                                                    <select name="cmbInsType" class="form-control" v-model="instype" style="width: 99% !important;">
                                                      <option value="">Choose Option</option>
                                                      <option v-repeat="itype: instiType" value="{{ itype.itype_id }}">{{ itype.itype_desc }}</option>
                                                    </select>
                                                    <span class="help-block" v-if="ErrorValidation.instype">{{ Error.instype }}</span>
                                                  </div>
                                                  <div class="form-group {{ haserror.locreg }}" style="margin-bottom: 0px !important;">
                                                    <label class="control-label" for="txtCode">Local Region <span class="required">*</span> </label>
                                                    <select name="cmbLocRegion" class="form-control" v-model="locreg" style="width: 99% !important;">
                                                      <option value="">Choose Option</option>
                                                      <option v-repeat="lreg: localRegion" value="{{ lreg.locid }}">{{ lreg.locreg }} - {{ lreg.locprov }}</option>
                                                    </select>
                                                    <span class="help-block" v-if="ErrorValidation.locreg">{{ Error.locreg }}</span>
                                                  </div>
                                                  <div class="form-group {{ haserror.code }}" style="margin-bottom: 0px !important;">
                                                    <label class="control-label" for="txtCode">Code <span class="required">*</span> </label>
                                                    <input type="text" name="txtCode" v-model="code" class="form-control" style="width: 99% !important">
                                                    <span class="help-block" v-if="ErrorValidation.code">{{ Error.code }}</span>
                                                  </div>
                                                  <div class="form-group {{ haserror.desc }}" style="margin-bottom: 0px !important;">
                                                    <label class="control-label" for="txtCode">Description <span class="required">*</span> </label>
                                                    <input type="text" name="txtDesc" v-model="desc" class="form-control" style="width: 99% !important">
                                                    <span class="help-block" v-if="ErrorValidation.desc">{{ Error.desc }}</span>
                                                  </div>
                                                </div>
                                                <div class="modal-footer">
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                  <button type="submit" v-on="click: onSubmitForm" class="btn btn-primary">Save changes</button>
                                                </div>
                                                </form>
                                              </div>
                                            </div>
                                          </div>
                                        <!-- End Modal Add Host Institution -->
                                        <?php if(checkAccess($_SESSION['sessAccessLevel'], 'libraries')>0): ?><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalHostInst">Add Host Institution</button><?php endif; ?>
                                      </td></tr></table>
                                      </div>
                                    <!-- End elhostinst -->
                                      <span class="help-block" v-if="ErrorValidation.institution" id="cmbHostInstitutionspan">{{ Error.institution }}</span>
                                    
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.outcomes }}">
                                <label for="cmbHostInstitution" class="col-sm-2 control-label">DOST Outcomes <span class="required">*</span> </label>
                                  <div class="col-sm-9">
                                      <select class="select2_multiple form-control" name="cmbAccOutcomes[]" multiple="multiple" v-model="outcomes">
                                          <?php foreach($arrOutcomes as $outcomes):?>
                                          <option
                                            <?=(in_array($outcomes["out_id"], $outcomes_arr)) ? 'selected' : ''?>
                                            value="<?=$outcomes["out_id"]?>"><?php echo $outcomes["out_desc"];?></option>
                                          <?php endforeach;?>
                                      </select>
                                      <span class="help-block" v-if="ErrorValidation.outcomes">{{ Error.outcomes }}</span>
                                  </div>
                            </div>
                            <div class="form-group {{ haserror.priority }}">
                                <label for="cmbHAccPriority" class="col-sm-2 control-label">DOST Priority Areas <span class="required">*</span> </label>
                                  <div class="col-sm-9">
                                      <select class="select2_multiple form-control" v-model="priority" name="cmbAccPriority[]" multiple="multiple">
                                          <?php foreach($arrPriority as $priority):?>
                                          <option
                                            <?=(in_array($priority["pri_id"], $priority_arr)) ? 'selected' : ''?>
                                            value="<?=$priority["pri_id"]?>"><?=$priority["pri_desc"];?></option>
                                          <?php endforeach;?>
                                      </select>
                                      <span class="help-block" v-if="ErrorValidation.priority">{{ Error.priority }}</span>
                                  </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <!-- <button type="reset" class="btn btn-default" name="btnSubmitAddProfile" value="cancel">Reset</button> -->
                                    <a href="" class="btn btn-default">Reset</a>
                                    <button type="button" class="btn btn-primary" name="validate" v-on="click: getvalidate">Save</button>

                                </div>
                            </div>
                            <!-- Begin Confirmation Service Award Information Modal -->
                            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="savesrvinfo">
                              <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-body">
                                      <p id="contentmodal"></p>
                                      <br><br>
                                      <!-- <button class="btn btn-primary" id="btnsavesrv" data-dismiss="modal" v-on="click: savesrv">Yes </button> -->
                                      <button id="add" type="submit" class="btn btn-primary" name="add" value="addSAProfile">Save</button>
                                      <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
                                    </div>
                                </div>
                              </div>  
                            </div>
                            <!-- End Confirmation Service Award Information Modal -->
                        </form>
                    </div>
                    <!-- end srvContractDur -->
                    </div>
                  </div>
                </div>
                <!-- begin General Information -->

  <!-- start tba -->
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="tba">
        <h4 class="panel-title">
            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#accortba" aria-expanded="false" aria-controls="accortba">
                Terms of Reference
            </a>
        </h4>
    </div>
    <div id="accortba" class="panel-collapse collapse <?=($this->uri->segment(4)=='tba'? 'in': '')?>" role="tabpanel" aria-labelledby="tba">
        <div class="panel-body">
            <div id="eltba" v-cloak>
                <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                <input type="hidden" id="sciid" value="<?=$this->uri->segment(3)?>">
                <!-- Begin tba -->
                <button type="button" class="btn btn-primary pull-right" v-on="click: addtba" data-toggle="modal" data-target="#editTba">Add Terms of Reference</button>
                <br><br><br>
                <div class="form-group">
                    <div class="col-sm-12">
                        <table id="example" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Host Institution</th>
                                    <th>Text</th>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-repeat="tba: tbas" data-target="#editTba" data-toggle="modal" data_value="{{ tba.tba_id }}" v-on="click: getRowValue(tba)">
                                    <td>{{ ($index + 1) }}</td>
                                    <td>{{ tba.ins_desc }}</td>
                                    <td>{{{ tba.tba_text | truncate 100 }}}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteTBAModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Modal tba start -->
                <div class="modal fade" id="editTba" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Terms of Reference</h4>
                            </div>
                            <div class="modal-body">
                            <form action="<?=base_url('{{ actionName }}').'?subs='.$_GET['subs']?>" method="post">
                            <div class="richtextsci">
                                <br>
                                <div class="item form-group {{ haserror.host }}">
                                    <label class="control-label" for="name">Host Institution </label>
                                    <select class="form-control" name="hostinst" v-model="host">
                                        <option value="">Select Options</option>
                                        <?php foreach($arrInstitution as $host): ?>
                                            <option value="<?=$host['ins_id']?>"><?=$host['ins_desc']?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="help-block" v-if="ErrorValidation.host">{{ Error.host }}</span>
                                </div>

                                <div id="alerts"></div>
                                <div class="btn-toolbar editor" data-role="editor-toolbar" data-target="#editor">
                                    <div class="btn-group">
                                        <a class="btn" data-edit="insertunorderedlist" title="Bullet list"><i class="fa fa-list-ul"></i></a>
                                        <a class="btn" data-edit="insertorderedlist" title="Number list"><i class="fa fa-list-ol"></i></a>
                                        <a class="btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="fa fa-dedent"></i></a>
                                        <a class="btn" data-edit="indent" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
                                    </div>
                                </div>

                                <div id="editor" style="{{ haserror.text }}">{{{ text }}}</div>
                                <textarea name="descr" id="descr" style="display:none;"></textarea>
                                <span class="help-block" v-if="ErrorValidation.text" style="color: #dd4b39;">{{ Error.text }}</span>
                            </div>
                            <!-- richtext end -->
                            <br>
                            <div class="form-group" align="center">
                                <button type="submit" class="btn btn-primary" name="btnSubmitEducation" v-on="click: getValidateTba" value="add">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal tba end -->
            </div>
        </div>
    </div>
  </div>
  <!-- end tba -->

                <!-- begin target deliverables -->
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                      <a class="collapsed" <?=$this->uri->segment(2)=='addServiceAward' ? 'data-toggle="modal" data-target="#notifModal"' : ''?> role="button" data-toggle="collapse" data-parent="#accordion" href="<?=$this->uri->segment(2)=='editServiceAward'?'#targetDeli':''?>" aria-expanded="false" aria-controls="targetDeli">
                        Target Deliverables
                      </a>
                    </h4>
                  </div>
                  <div id="targetDeli" class="panel-collapse collapse <?=($this->uri->segment(4) == 'target' ? 'in' : '')?>" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <!-- begin menu -->
                           <ul class="nav nav-tabs">
                              <li role="presentation" class="<?=($this->uri->segment(5)=='sem' or $this->uri->segment(5)=='' or $this->uri->segment(4)=='accomp') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/sem?subs='.$_GET['subs']).'"'?>>Seminars / Lectures /<br> Forums</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='tra') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/tra?subs='.$_GET['subs']).'"'?>>Trainings / Workshop /<br>Demonstrations</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='proj') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/proj?subs='.$_GET['subs']).'"'?>>Projects to be Assisted <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='paper') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/paper?subs='.$_GET['subs']).'"'?>>Paper to be Submitted <br>for publication</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='stud') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/stud?subs='.$_GET['subs']).'"'?>>Students Mentoring <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='cur') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/cur?subs='.$_GET['subs']).'"'?>>Curriculum / Course Development <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='net') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/net?subs='.$_GET['subs']).'"'?>>Network and Linkages <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='res') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/res?subs='.$_GET['subs']).'"'?>>Research and Development <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='oth') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/target/oth?subs='.$_GET['subs']).'"'?>>Others <br>&nbsp;</a></li>
                            </ul>
                        <!-- end menu -->

                        <input type="hidden" id="sciid" value="<?=$this->uri->segment(3)?>">
                      <?php if($this->uri->segment(5)=='sem' or $this->uri->segment(5)=='' or $this->uri->segment(4)=='accomp'): ?>
                          <div id="srvseminars" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit seminar -->
                            <div class="modal fade" id="addSeminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Seminar / Lecture / Forum</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text">
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.part }}">
                                                <label class="control-label" for="cmbpart">Type of Participants <span class="required">*</span> </label>
                                                <select name="cmbpart" id="cmbpart" class="form-control" v-model="part">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="part: participantstype" value="{{ part.par_id }}">{{ part.par_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.part">{{ Error.part }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.target }}">
                                                <label class="control-label" for="txttarget">No. of Target Participants </label>
                                                <input id="txttarget" v-model="target" class="form-control" name="txttarget" type="text">
                                                <span class="help-block" v-if="ErrorValidation.target">{{ Error.target }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Expected Output <span class="required">*</span></label>
                                                <input id="txtoutput" v-model="output" class="form-control" name="txtoutput" type="text">
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>
                                            <input type="hidden" name="istarget" value="0">        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit seminar -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addSeminar" data-toggle="modal" data-target="#addSeminar"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Seminar / Lecture / Forum</button>
                            <br><br><br>
                            <table id="tblSeminar" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Title</th>
                                    <th>Type of Participants</th>
                                    <th>No. of Target Participants</th>
                                    <th>Expected Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="sem: seminars" data_value="{{ sem.id }}" v-on="click: getRowValue(sem)">
                                    <td style="width: 10px !important;" data-target="#addSeminar" data-toggle="modal" >{{ ($index + 1) }}</td>
                                    <td data-target="#addSeminar" data-toggle="modal" >{{ sem.fld_desc }}</td>
                                    <td data-target="#addSeminar" data-toggle="modal" >{{ sem.sem_title | truncate 20 }}</td>
                                    <td data-target="#addSeminar" data-toggle="modal" >{{ sem.par_desc }}</td>
                                    <td data-target="#addSeminar" data-toggle="modal" >{{ sem.sem_targetparticipants }}</td>
                                    <td data-target="#addSeminar" data-toggle="modal" >{{ sem.sem_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Seminars -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='tra'): ?>
                          <div id="srvTrainings" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit training -->
                            <div class="modal fade" id="addTraining" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Training / Workshop/ Demonstration</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text">
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.part }}">
                                                <label class="control-label" for="cmbpart">Type of Participants <span class="required">*</span> </label>
                                                <select name="cmbpart" id="cmbpart" class="form-control" v-model="part">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="part: participantstype" value="{{ part.par_id }}">{{ part.par_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.part">{{ Error.part }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.target }}">
                                                <label class="control-label" for="txttarget">No. of Target Participants </label>
                                                <input id="txttarget" v-model="target" class="form-control" name="txttarget">
                                                <span class="help-block" v-if="ErrorValidation.target">{{ Error.target }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Expected Output <span class="required">*</span></label>
                                                <input id="txtoutput" v-model="output" class="form-control" name="txtoutput" type="text">
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>      
                                        </div>
                                        <input type="hidden" name="istarget" value="0">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit training -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addTraining" data-toggle="modal" data-target="#addTraining"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Training / Workshop / Demonstration </button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Title</th>
                                    <th>Type of Participants</th>
                                    <th>No. of Target Participants</th>
                                    <th>Expected Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="tra: trainings" data_value="{{ tra.id }}" v-on="click: getRowValue(tra)">
                                    <td style="width: 10px !important;" data-target="#addTraining" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addTraining" data-toggle="modal">{{ tra.fld_desc }}</td>
                                    <td data-target="#addTraining" data-toggle="modal">{{ tra.tra_title | truncate 20 }}</td>
                                    <td data-target="#addTraining" data-toggle="modal">{{ tra.par_desc }}</td>
                                    <td data-target="#addTraining" data-toggle="modal">{{ tra.tra_targetparticipants }}</td>
                                    <td data-target="#addTraining" data-toggle="modal">{{ tra.tra_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end training -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='proj'): ?>
                          <div id="srvProjects" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit project -->
                            <div class="modal fade" id="addproject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Project</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text">
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.obj }}">
                                                <label class="control-label" for="txtobjectives">Objectives <span class="required">*</span> </label>
                                                <input id="txtobjectives" v-model="obj" class="form-control" name="txtobjectives" type="text">
                                                <span class="help-block" v-if="ErrorValidation.obj">{{ Error.obj }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.agency }}">
                                                <label class="control-label" for="cmbagency">Implementing Agency <span class="required">*</span> </label>
                                                <input name="cmbagency" id="cmbagency" class="form-control" v-model="agency">
                                                <span class="help-block" v-if="ErrorValidation.agency">{{ Error.agency }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.stat }}">
                                                <?php 
                                                  $proj_status = array(
                                                      '' => 'Choose Option',
                                                      1  => 'Proposal for Funding',
                                                      2  => 'Ongoing',
                                                      3  => 'Continuing',
                                                      4  => 'Completed',
                                                      5  => 'Others'
                                                    );
                                                 ?>
                                                <label class="control-label" for="txtstat">Project Status <span class="required">*</span> </label>
                                                <select id="txtstat" v-model="stat" class="form-control" name="txtstat">
                                                  <?php foreach ($proj_status as $key=>$pstat): ?>
                                                    <option value="<?=$key?>"><?=$pstat?></option>
                                                  <?php endforeach; ?>  
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.stat">{{ Error.stat }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.contri }}">
                                                <label class="control-label" for="txtcontri">Expected Contribution <span class="required">*</span></label>
                                                <input id="txtcontri" v-model="contri" class="form-control" name="txtcontri" type="text">
                                                <span class="help-block" v-if="ErrorValidation.contri">{{ Error.contri }}</span>
                                            </div> 
                                            <input type="hidden" name="istarget" value="0">        
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit training -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addProject" data-toggle="modal" data-target="#addproject"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Project</button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Title</th>
                                    <th>Objectives</th>
                                    <th>Implementing Agency</th>
                                    <th>Project Status</th>
                                    <th>Expected Contribution</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="proj: projects" data_value="{{ proj.id }}" v-on="click: getRowValue(proj)">
                                    <td style="width: 10px !important;" data-target="#addproject" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addproject" data-toggle="modal">{{ proj.fld_desc }}</td>
                                    <td data-target="#addproject" data-toggle="modal">{{ proj.prj_title | truncate 20 }}</td>
                                    <td data-target="#addproject" data-toggle="modal">{{ proj.prj_objective | truncate 20 }}</td>
                                    <td data-target="#addproject" data-toggle="modal">{{ proj.prj_agency }}</td>
                                    <td data-target="#addproject" data-toggle="modal">{{ proj.prj_status_name }}</td>
                                    <td data-target="#addproject" data-toggle="modal">{{ proj.prj_contributions | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end project -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='paper'): ?>
                          <!-- begin paper -->
                          <div id="srvPapers" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit paper -->
                            <div class="modal fade" id="addpaper" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Paper</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text" placeholder="Paper/Article/Chapter/Book" required>
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.name }}">
                                                <label class="control-label" for="txtname">Name <span class="required">*</span> </label>
                                                <input id="txtname" v-model="name" class="form-control" name="txtname" type="text" placeholder="Journal/Magazine/ Newsletter/Newspaper/Book" required>
                                                <span class="help-block" v-if="ErrorValidation.name">{{ Error.name }}</span>
                                            </div>
                                            <input type="hidden" value="0" name="istarget">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit paper -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addpaper" data-toggle="modal" data-target="#addpaper"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Paper</button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Title</th>
                                    <th>Name</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="pap: papers" data_value="{{ pap.id }}" v-on="click: getRowValue(pap)">
                                    <td style="width: 10px !important;" data-target="#addpaper" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addpaper" data-toggle="modal">{{ pap.pap_title | truncate 20 }}</td>
                                    <td data-target="#addpaper" data-toggle="modal">{{ pap.pap_name | truncate 20 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end paper -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='stud'): ?>
                          <div id="srvMentorings" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit Mentoring -->
                            <div class="modal fade" id="addMentoring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Students Mentoring</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.inst }}">
                                                <label class="control-label" for="txtinstitution">Host Institution <span class="required">*</span> </label>
                                                <select id="txtinstitution" v-model="inst" class="form-control" name="txtinstitution">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="inst: insts" value="{{ inst.ins_id }}">{{ inst.ins_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.inst">{{ Error.inst }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.course }}">
                                                <label class="control-label" for="txtcourse">Course <span class="required">*</span> </label>
                                                <input id="txtcourse" v-model="course" class="form-control" name="txtcourse" type="text">
                                                <span class="help-block" v-if="ErrorValidation.course">{{ Error.course }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.target }}">
                                                <label class="control-label" for="txttarget">No. of Target Participants </label>
                                                <input id="txttarget" v-model="target" class="form-control" name="txttarget">
                                                <span class="help-block" v-if="ErrorValidation.target">{{ Error.target }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Expected Output <span class="required">*</span></label>
                                                <input id="txtoutput" v-model="output" class="form-control" name="txtoutput" type="text">
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>           
                                        </div>
                                        <input type="hidden" value="0" name="istarget">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click:getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit Mentoring -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addMentoring" data-toggle="modal" data-target="#addMentoring"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Students Mentoring</button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Host Institution</th>
                                    <th>Course</th>
                                    <th>No. of Target Participants</th>
                                    <th>Expected Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="ment: mentorings" data_value="{{ ment.id }}" v-on="click: getRowValue(ment)">
                                    <td style="width: 10px" data-target="#addMentoring" data-toggle="modal" style="width: 10px !important">{{ ($index + 1) }}</td>
                                    <td data-target="#addMentoring" data-toggle="modal">{{ ment.fld_desc }}</td>
                                    <td data-target="#addMentoring" data-toggle="modal">{{ ment.ins_desc }}</td>
                                    <td data-target="#addMentoring" data-toggle="modal">{{ ment.men_course | truncate 20 }}</td>
                                    <td data-target="#addMentoring" data-toggle="modal">{{ ment.men_notargetstudents }}</td>
                                    <td data-target="#addMentoring" data-toggle="modal">{{ ment.men_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Mentoring -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='cur'): ?>
                          <!-- begin curiculum -->
                          <div id="srvCurriculum" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit curiculum -->
                            <div class="modal fade" id="addcuriculum" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Curriculum / Course</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.inst }}">
                                                <label class="control-label" for="txtinst">Host Institution <span class="required">*</span> </label>
                                                <select id="txtinst" v-model="inst" class="form-control" name="txtinst">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="inst: insts" value="{{ inst.ins_id }}">{{ inst.ins_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.inst">{{ Error.inst }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="txtoutput">Expected Output <span class="required">*</span> </label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput" type="text"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>
                                        </div>
                                        <input type="hidden" value="0" name="istarget">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit curiculum -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addcuriculum" data-toggle="modal" data-target="#addcuriculum"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Curiculum / Course Development</button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Host Institution</th>
                                    <th>Expected Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="curs: curiculums" data_value="{{ curs.id }}" v-on="click: getRowValue(curs)">
                                    <td style="width:10px !important;" data-target="#addcuriculum" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addcuriculum" data-toggle="modal">{{ curs.fld_desc }}</td>
                                    <td data-target="#addcuriculum" data-toggle="modal">{{ curs.ins_desc }}</td>
                                    <td data-target="#addcuriculum" data-toggle="modal">{{ curs.cur_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end curiculum -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='net'): ?>
                          <div id="srvNetworks" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit network and linkages -->
                            <div class="modal fade" id="addNetwork" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Network and Linkage</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Expected Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>           
                                        </div>
                                        <input type="hidden" value=0 name="istarget">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit network and linkages -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addNetwork" data-toggle="modal" data-target="#addNetwork"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Network and Linkages</button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Expected Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="nets: networks" data_value="{{ nets.id }}" v-on="click: getRowValue(nets)">
                                    <td style="width:10px !important" data-target="#addNetwork" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addNetwork" data-toggle="modal">{{ nets.fld_desc }}</td>
                                    <td data-target="#addNetwork" data-toggle="modal">{{ nets.net_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end network and linkages -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='res'): ?>
                          <div id="srvResearch" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit Research -->
                            <div class="modal fade" id="addResearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Research and Development</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Expected Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>           
                                        </div>
                                        <input type="hidden" value="0" name="istarget">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit Research -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addResearch" data-toggle="modal" data-target="#addResearch"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Research and Development</button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Expected Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="res: research" data_value="{{ nets.id }}" v-on="click: getRowValue(res)">
                                    <td style="width:10px !important;" data-target="#addResearch" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addResearch" data-toggle="modal">{{ res.fld_desc }}</td>
                                    <td data-target="#addResearch" data-toggle="modal">{{ res.res_output }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Research -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='oth'): ?>
                          <div id="srvOthers" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit Others -->
                            <div class="modal fade" id="addOther" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Others</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field" required>
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Expected Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>
                                            <input type="hidden" value="0" name="istarget">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit Others -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addOther" data-toggle="modal" data-target="#addOther"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Other</button>
                            <br><br><br>
                            <table id="example1" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Expected Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="oth: others" data_value="{{ nets.id }}" v-on="click: getRowValue(oth)">
                                    <td style="width:10px !important" data-target="#addOther" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addOther" data-toggle="modal">{{ oth.fld_desc }}</td>
                                    <td data-target="#addOther" data-toggle="modal">{{ oth.oth_output }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Others -->
                          </div>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <!-- end target deliverables -->

                <!-- begin target Accomplishments -->
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingTwo">
                    <h4 class="panel-title">
                      <a class="collapsed" <?=$this->uri->segment(2)=='addServiceAward' ? 'data-toggle="modal" data-target="#notifModal"' : ''?> role="button" data-toggle="collapse" data-parent="#accordion" href="<?=$this->uri->segment(2)=='editServiceAward'?'#accorAccomp':''?>" aria-expanded="false" aria-controls="accorAccomp">
                        Accomplishments
                      </a>
                    </h4>
                  </div>
                  <div id="accorAccomp" class="panel-collapse collapse <?=($this->uri->segment(4) == 'accomp' ? 'in' : '')?>" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">
                        <!-- begin menu -->
                           <ul class="nav nav-tabs">
                              <li role="presentation" class="<?=($this->uri->segment(5)=='sem_a' or $this->uri->segment(5)=='' or $this->uri->segment(4)=='target') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/sem_a?subs='.$_GET['subs']).'"'?>>Seminars / Lectures /<br> Forums</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='tra_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/tra_a?subs='.$_GET['subs']).'"'?>>Trainings / Workshop /<br>Demonstrations</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='proj_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/proj_a?subs='.$_GET['subs']).'"'?>>Projects Assisted <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='paper_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/paper_a?subs='.$_GET['subs']).'"'?>>Paper Submitted <br>for publication</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='stud_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/stud_a?subs='.$_GET['subs']).'"'?>>Students Mentoring <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='cur_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/cur_a?subs='.$_GET['subs']).'"'?>>Curriculum / Course Development <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='net_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/net_a?subs='.$_GET['subs']).'"'?>>Network and Linkages <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='res_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/res_a?subs='.$_GET['subs']).'"'?>>Research and Development <br>&nbsp;</a></li>
                              <li role="presentation" class="<?=($this->uri->segment(5)=='oth_a') ? 'active' : ''?>"><a <?=($this->uri->segment(2) == 'addServiceAward') ? 'href="" data-toggle="modal" data-target="#informationModal"' : 'href="'.base_url($actions.'/accomp/oth_a?subs='.$_GET['subs']).'"'?>>Others <br>&nbsp;</a></li>
                            </ul>
                        <!-- end menu -->
                        <input type="hidden" id="sciid" value="<?=$this->uri->segment(3)?>">
                      <?php if($this->uri->segment(5)=='sem_a' or $this->uri->segment(5)=='' or $this->uri->segment(4)=='target'): ?>
                          <div id="accompSeminars" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit seminar -->
                            <div class="modal fade" id="addAccSeminar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Seminar - Accomplishments</h4>
                                    </div>
                                    <form action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>"  method="post" enctype="multipart/form-data">
                                        <input type="hidden" v-model="sid" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text">
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div id="divaccompDate" class="form-group {{ haserror.accompDate }}">
                                                <label class="control-label" for="txtsemDate">Date <span class="required">*</span> </label>
                                                <input id="txtsemDate" v-model="accompDate" class="form-control" name="txtsemDate" type="text" placeholder="YYYY-MM-DD">
                                                <span id="spanaccompDate" class="help-block" v-if="ErrorValidation.accompDate">{{ Error.accompDate }}</span>
                                            </div>
                                            <script type="text/javascript">$(function () { $('#txtsemDate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                                            <div class="form-group {{ haserror.venue }}">
                                                <label class="control-label" for="txtvenue">Venue <span class="required">*</span> </label>
                                                <input id="txtvenue" v-model="venue" class="form-control" name="txtvenue" type="text">
                                                <span class="help-block" v-if="ErrorValidation.venue">{{ Error.venue }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.part }}">
                                                <label class="control-label" for="cmbpart">Type of Participants <span class="required">*</span> </label>
                                                <select name="cmbpart" id="cmbpart" class="form-control" v-model="part">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="part: participantstype" value="{{ part.par_id }}">{{ part.par_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.part">{{ Error.part }}</span>
                                                <label for="file-upload" class="custom-file-upload"><i class="fa fa-upload"></i> Upload</label><input name="file-upload[]" id="file-upload" type="file" multiple />
                                                <ul id="fileList"></ul>
                                                <div v-if="action == 'edit'">
                                                  <b>Attachments</b>
                                                  <ul v-repeat="file: files" style="margin: 0em 0;">
                                                    <li id="fileno{{file.attch_id}}" v-if="semid == file.attch_sem_id">
                                                      {{ file.attch_filename }} &nbsp;&nbsp;
                                                      <a title="click to download" href="<?=base_url()?>{{ file.attch_path }}" target="_blank"><i style="color: #337ab7;" class="fa fa-download" aria-hidden="true"></i></a>&nbsp;&nbsp;<a id="btnRemoveAttach" class="btnremove" v-on="click: getAttachmentID(file.attch_id)" href="#"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                    </li>
                                                  </ul>
                                                  <input type="hidden" v-model="removefiles" name="removefiles">
                                                </div>
                                            </div>
                                            <div class="form-group {{ haserror.actual }}">
                                                <label class="control-label" for="txtactual">No. of Participants </label>
                                                <input id="txtactual" v-model="actual" class="form-control" name="txtactual">
                                                <span class="help-block" v-if="ErrorValidation.actual">{{ Error.actual }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>
                                            <input type="hidden" name="istarget" value="1">
                                        </div>
                                        <div class="modal-footer">
                                            <span style="color:red;font-weight:bold" class="help-block" v-if="btnsave=='Continue'">Some attachments may have been lost. Click continue to proceed.</span>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-{{ btnClass }}" v-on="click: getValidate">{{ btnsave }}</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit seminar -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccSeminar" data-toggle="modal" data-target="#addAccSeminar"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Seminar / Lecture / Forum</button>
                            <br><br><br>
                            <table id="tblSeminarAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Title</th>
                                    <th>Date</th>
                                    <th>Venue</th>
                                    <th>Type of Participants</th>
                                    <th>No. of Participants</th>
                                    <th>Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="sem: seminars" data_value="{{ sem.id }}" v-on="click: getRowValue(sem)">
                                    <td style="width:10px !important" data-target="#addAccSeminar" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccSeminar" data-toggle="modal">{{ sem.fld_desc }}</td>
                                    <td data-target="#addAccSeminar" data-toggle="modal">{{ sem.sem_title | truncate 20 }}</td>
                                    <td data-target="#addAccSeminar" data-toggle="modal">{{ sem.sem_date }}</td>
                                    <td data-target="#addAccSeminar" data-toggle="modal">{{ sem.sem_venue }}</td>
                                    <td data-target="#addAccSeminar" data-toggle="modal">{{ sem.par_desc }}</td>
                                    <td data-target="#addAccSeminar" data-toggle="modal">{{ sem.sem_actualparticipants }}</td>
                                    <td data-target="#addAccSeminar" data-toggle="modal">{{ sem.sem_output | truncate 40 }}
                                        <ul v-repeat="file: files">
                                          <li v-if="sem.sem_id == file.attch_sem_id">{{ file.attch_filename }}</li>
                                        </ul>
                                    </td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Seminars -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='tra_a'): ?>
                          <div id="accompTrainings" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit Training -->
                            <div class="modal fade" id="addAccTraining" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Training - Accomplishments</h4>
                                    </div>
                                    <form action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>"  method="post" enctype="multipart/form-data">
                                        <input type="hidden" v-model="sid" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text">
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div id="divaccompDate" class="form-group {{ haserror.accompDate }}">
                                                <label class="control-label" for="txtsemDate">Date <span class="required">*</span> </label>
                                                <input id="txtsemDate" v-model="accompDate" class="form-control" name="txtsemDate" type="text" placeholder="YYYY-MM-DD">
                                                <span id="spanaccompDate" class="help-block" v-if="ErrorValidation.accompDate">{{ Error.accompDate }}</span>
                                            </div>
                                            <script type="text/javascript">$(function () { $('#txtsemDate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                                            <div class="form-group {{ haserror.venue }}">
                                                <label class="control-label" for="txtvenue">Venue <span class="required">*</span> </label>
                                                <input id="txtvenue" v-model="venue" class="form-control" name="txtvenue" type="text">
                                                <span class="help-block" v-if="ErrorValidation.venue">{{ Error.venue }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.part }}">
                                                <label class="control-label" for="cmbpart">Type of Participants <span class="required">*</span> </label>
                                                <select name="cmbpart" id="cmbpart" class="form-control" v-model="part">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="part: participantstype" value="{{ part.par_id }}">{{ part.par_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.part">{{ Error.part }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.actual }}">
                                                <label class="control-label" for="txtactual">No. of Participants </label>
                                                <input id="txtactual" v-model="actual" class="form-control" name="txtactual">
                                                <span class="help-block" v-if="ErrorValidation.actual">{{ Error.actual }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>
                                            <input type="hidden" name="istarget" value="1">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-{{ btnClass }}" v-on="click: getValidate">{{ btnsave }}</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit Training -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccTraining" data-toggle="modal" data-target="#addAccTraining"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Training / Workshop / Demonstrations</button>
                            <br><br><br>
                            <table id="tblTrainingAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Title</th>
                                    <th>Date</th>
                                    <th>Venue</th>
                                    <th>Type of Participants</th>
                                    <th>No. of Participants</th>
                                    <th>Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="tra: trainings" data_value="{{ tra.id }}" v-on="click: getRowValue(tra)">
                                    <td style="width: 10px !important" data-target="#addAccTraining" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccTraining" data-toggle="modal">{{ tra.fld_desc }}</td>
                                    <td data-target="#addAccTraining" data-toggle="modal">{{ tra.tra_title | truncate 20 }}</td>
                                    <td data-target="#addAccTraining" data-toggle="modal">{{ tra.tra_date }}</td>
                                    <td data-target="#addAccTraining" data-toggle="modal">{{ tra.tra_venue }}</td>
                                    <td data-target="#addAccTraining" data-toggle="modal">{{ tra.par_desc }}</td>
                                    <td data-target="#addAccTraining" data-toggle="modal">{{ tra.tra_actualparticipants }}</td>
                                    <td data-target="#addAccTraining" data-toggle="modal">{{ tra.tra_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Training -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='proj_a'): ?>
                          <div id="accompProjects" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit project accomplishment -->
                            <div class="modal fade" id="addAccompProj" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Project - Accomplishments</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" v-model="pij" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text">
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.obj }}">
                                                <label class="control-label" for="txtobjectives">Objectives <span class="required">*</span> </label>
                                                <input id="txtobjectives" v-model="obj" class="form-control" name="txtobjectives" type="text">
                                                <span class="help-block" v-if="ErrorValidation.obj">{{ Error.obj }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.agency }}">
                                               <label class="control-label" for="cmbagency">Implementing Agency <span class="required">*</span> </label>
                                                <input type="text" name="cmbagency" id="cmbagency" class="form-control" v-model="agency">
                                                <span class="help-block" v-if="ErrorValidation.agency">{{ Error.agency }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.stat }}">
                                                <?php 
                                                  $proj_status = array(
                                                      '' => 'Choose Option',
                                                      1  => 'Proposal for Funding',
                                                      2  => 'Ongoing',
                                                      3  => 'Continuing',
                                                      4  => 'Completed',
                                                      5  => 'Others'
                                                    );
                                                 ?>
                                                <label class="control-label" for="txtstat">Project Status <span class="required">*</span> </label>
                                                <select id="txtstat" v-model="stat" class="form-control" name="txtstat">
                                                  <?php foreach ($proj_status as $key=>$pstat): ?>
                                                    <option value="<?=$key?>"><?=$pstat?></option>
                                                  <?php endforeach; ?>  
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.stat">{{ Error.stat }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.contri }}">
                                                <label class="control-label" for="txtcontri">Contribution to the Project<span class="required">*</span></label>
                                                <textarea id="txtcontri" v-model="contri" class="form-control" name="txtcontri"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.contri">{{ Error.contri }}</span>
                                            </div>
                                            <input type="hidden" name="istarget" value="1">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit project accomplishment -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccompProj" data-toggle="modal" data-target="#addAccompProj"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Project</button>
                            <br><br><br>
                            <table id="tblProjectAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Title</th>
                                    <th>Objectives</th>
                                    <th>Implementing Agency</th>
                                    <th>Project Status</th>
                                    <th>Contribution</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="proj: projects" data_value="{{ proj.id }}" v-on="click: getRowValue(proj)">
                                    <td style="width: 10px !important;" data-target="#addAccompProj" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccompProj" data-toggle="modal">{{ proj.fld_desc }}</td>
                                    <td data-target="#addAccompProj" data-toggle="modal">{{ proj.prj_title | truncate 20 }}</td>
                                    <td data-target="#addAccompProj" data-toggle="modal">{{ proj.prj_objective | truncate 20 }}</td>
                                    <td data-target="#addAccompProj" data-toggle="modal">{{ proj.prj_agency }}</td>
                                    <td data-target="#addAccompProj" data-toggle="modal">{{ proj.prj_status_name }}</td>
                                    <td data-target="#addAccompProj" data-toggle="modal">{{ proj.prj_contributions | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end project accomplishment -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='paper_a'): ?>
                          <!-- begin paper accomplishment-->
                          <div id="accompPapers" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit paper accomplishment-->
                            <div class="modal fade" id="addaccompPaper" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Paper - Accomplishments</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" v-model="pij" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.name }}">
                                                <label class="control-label" for="txtname">Name of Journal / Magazine / Newsletter / Newspaper / Book <span class="required">*</span> </label>
                                                <input id="txtname" v-model="name" class="form-control" name="txtname" type="text">
                                                <span class="help-block" v-if="ErrorValidation.name">{{ Error.name }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.isbn }}">
                                                <label class="control-label" for="txtisbn">ISBN No./Volume/Issue/Edition <span class="required">*</span> </label>
                                                <input id="txtisbn" v-model="isbn" class="form-control" name="txtisbn" type="text">
                                                <span class="help-block" v-if="ErrorValidation.isbn">{{ Error.isbn }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.title }}">
                                                <label class="control-label" for="txttitle">Title of Paper / Article / Chapter / Book <span class="required">*</span> </label>
                                                <input id="txttitle" v-model="title" class="form-control" name="txttitle" type="text">
                                                <span class="help-block" v-if="ErrorValidation.title">{{ Error.title }}</span>
                                            </div>
                                            <div id="divSubdate" class="form-group {{ haserror.subdate }}">
                                                <label class="control-label" for="txtsubdate">Date submitted for Publication <span class="required">*</span> </label>
                                                <input id="txtsubdate" v-model="subdate" class="form-control" name="txtsubdate" type="text" placeholder="YYYY-MM-DD">
                                                <span id="spanSubdate" class="help-block" v-if="ErrorValidation.subdate">{{ Error.subdate }}</span>
                                            </div>
                                            <script type="text/javascript">$(function () { $('#txtsubdate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                                            <div id="divPubldate" class="form-group {{ haserror.pubdate }}">
                                                <label class="control-label" for="txtpubdate">If published, Date of Publication <span class="required">*</span> </label>
                                                <input id="txtpubdate" v-model="pubdate" class="form-control" name="txtpubdate" type="text" placeholder="YYYY-MM-DD">
                                                <span id="spanPubldate" class="help-block" v-if="ErrorValidation.pubdate">{{ Error.pubdate }}</span>
                                            </div>
                                            <script type="text/javascript">$(function () { $('#txtpubdate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                                            <div class="form-group {{ haserror.nopages }}">
                                                <label class="control-label" for="txtnopages">No. of Pages <span class="required">*</span> </label>
                                                <input id="txtnopages" v-model="nopages" class="form-control" name="txtnopages">
                                                <span class="help-block" v-if="ErrorValidation.nopages">{{ Error.nopages }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.authors }}">
                                                <label class="control-label" for="txtauthors">Authors <span class="required">*</span> </label>
                                                <input id="txtauthors" v-model="authors" class="form-control" name="txtauthors" type="text">
                                                <span class="help-block" v-if="ErrorValidation.authors">{{ Error.authors }}</span>
                                            </div>
                                            <input type="hidden" value="1" name="istarget">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit paper accomplishment-->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addaccompPaper" data-toggle="modal" data-target="#addaccompPaper"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Paper</button>
                            <br><br><br>
                            <table id="tblPaperAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Name</th>
                                    <th>ISBN No./Volume/Issue/Edition</th>
                                    <th>Title</th>
                                    <th>Submission Date</th>
                                    <th>Published Date</th>
                                    <th>No. of Pages</th>
                                    <th>Authors</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="pap: papers" data_value="{{ pap.id }}" v-on="click: getRowValue(pap)">
                                    <td style="width: 10px !important;" data-target="#addaccompPaper" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.fld_desc }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.pap_name | truncate 20 }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.pap_isbn }}/{{ pap.pap_isbn }}/{{ pap.pap_volume }}/{{ pap.pap_issue }}/{{ pap.pap_edition }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.pap_title | truncate 20 }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.pap_submission_date }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.pap_published_date }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.pap_nopages }}</td>
                                    <td data-target="#addaccompPaper" data-toggle="modal">{{ pap.pap_authors }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end paper accomplishment-->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='stud_a'): ?>
                          <div id="accompMentorings" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit Mentoring accomplishment-->
                            <div class="modal fade" id="addAccompMentoring" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Students Mentoring</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>" enctype="multipart/form-data">
                                        <input type="hidden" v-model="mid" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.inst }}">
                                                <label class="control-label" for="txtinstitution">Host Institution <span class="required">*</span> </label>
                                                <select id="txtinstitution" v-model="inst" class="form-control" name="txtinstitution">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="inst: insts" value="{{ inst.ins_id }}">{{ inst.ins_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.inst">{{ Error.inst }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.target }}">
                                                <label class="control-label" for="txtactual">No. of Students <span class="required">*</span> </label>
                                                <input id="txtactual" v-model="target" class="form-control" name="txtactual">
                                                <span class="help-block" v-if="ErrorValidation.target">{{ Error.target }}</span>
                                            </div>
                                            <div id="divfileUpload" class="form-group {{ haserror.uploadfile }}">
                                                <br>
                                                <label for="file-upload" class="custom-file-upload"><i class="fa fa-upload"></i> Upload Names of Students Mentored</label>
                                                <input name="file-upload[]" id="file-upload" type="file" multiple />
                                                <ul id="fileList"></ul>
                                                <div v-if="action == 'edit'">
                                                  <b>Attachments</b>
                                                  <ul v-repeat="file: files" style="margin: 0em 0;">
                                                    <li id="fileno{{file.attch_id}}" v-if="mid == file.attch_sem_id">
                                                      {{ file.attch_filename }} &nbsp;&nbsp;
                                                      <a title="click to download" href="<?=base_url()?>{{ file.attch_path }}" target="_blank"><i style="color: #337ab7;" class="fa fa-download" aria-hidden="true"></i></a>&nbsp;&nbsp;<a id="btnRemoveAttach" class="btnremove" v-on="click: getAttachmentID(file.attch_id)" href="#"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>
                                                    </li>
                                                  </ul>
                                                  <input type="hidden" v-model="removefiles" name="removefiles">
                                                </div>
                                                <span id="spanfileUpload" class="help-block" v-if="ErrorValidation.uploadfile">{{ Error.uploadfile }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.course }}">
                                                <label class="control-label" for="txtcourse">Course </label>
                                                <input id="txtcourse" v-model="course" class="form-control" name="txtcourse" type="text">
                                                <span class="help-block" v-if="ErrorValidation.course">{{ Error.course }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>           
                                        </div>
                                        <div class="modal-footer">
                                            <span style="color:red;font-weight:bold" class="help-block" v-if="btnsave=='Continue'">Some attachments may have been lost. Click continue to proceed.</span>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-{{ btnClass }}" v-on="click: getValidate">{{ btnsave }}</button>
                                        </div>
                                        <input type="hidden" value="1" name="istarget">
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit Mentoring accomplishment-->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccompMentoring" data-toggle="modal" data-target="#addAccompMentoring"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Students Mentoring</button>
                            <br><br><br>
                            <table id="tblMentoringAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Host Institution</th>
                                    <th>No. of Students</th>
                                    <th>Course</th>
                                    <th>Output</th>
                                    <th>Attachments</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="ment: mentorings" data_value="{{ ment.id }}" v-on="click: getRowValue(ment)">
                                    <td style="width: 10px !important;" data-target="#addAccompMentoring" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccompMentoring" data-toggle="modal">{{ ment.fld_desc }}</td>
                                    <td data-target="#addAccompMentoring" data-toggle="modal">{{ ment.ins_desc }}</td>
                                    <td data-target="#addAccompMentoring" data-toggle="modal">{{ ment.men_noactualstudents }}</td>
                                    <td data-target="#addAccompMentoring" data-toggle="modal">{{ ment.men_course | truncate 20 }}</td>
                                    <td data-target="#addAccompMentoring" data-toggle="modal">{{ ment.men_output | truncate 40 }}</td>
                                    <td data-target="#addAccompMentoring" data-toggle="modal">
                                        <ul v-repeat="file: files">
                                          <li v-if="ment.men_id == file.attch_sem_id">{{ file.attch_filename }}</li>
                                        </ul>
                                    </td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Mentoring accomplishment-->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='cur_a'): ?>
                          <!-- begin curiculum -->
                          <div id="accompCurriculum" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit curiculum accomplishment -->
                            <div class="modal fade" id="addAccompcuriculum" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Curriculum / Course</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" v-model="pij" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.inst }}">
                                                <label class="control-label" for="txtinst">Host Institution <span class="required">*</span> </label>
                                                <select id="txtinst" v-model="inst" class="form-control" name="txtinst">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="inst: insts" value="{{ inst.ins_id }}">{{ inst.ins_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.inst">{{ Error.inst }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="txtoutput">Output <span class="required">*</span> </label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput" type="text"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                        <input type="hidden" value="1" name="istarget">
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit curiculum accomplishment -->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccompcuriculum" data-toggle="modal" data-target="#addAccompcuriculum"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Curiculum / Course Development</button>
                            <br><br><br>
                            <table id="tblCurriculumAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Host Institution</th>
                                    <th>Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="curs: curiculums" data_value="{{ curs.id }}" v-on="click: getRowValue(curs)">
                                    <td style="width:10px !important" data-target="#addcuriculum" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccompcuriculum" data-toggle="modal">{{ curs.fld_desc }}</td>
                                    <td data-target="#addAccompcuriculum" data-toggle="modal">{{ curs.ins_desc }}</td>
                                    <td data-target="#addAccompcuriculum" data-toggle="modal">{{ curs.cur_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end curiculum accomplishment -->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='net_a'): ?>
                          <div id="accompNetworks" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit network and linkages accomplishment-->
                            <div class="modal fade" id="addAccompNetwork" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Network and Linkages</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" v-model="nid" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field" required>
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>         
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                        <input type="hidden" value="1" name="istarget">
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit network and linkages accomplishment-->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccompNetwork" data-toggle="modal" data-target="#addAccompNetwork"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Network and Linkages</button>
                            <br><br><br>
                            <table id="tblNetworkAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="nets: networks" data_value="{{ nets.id }}" v-on="click: getRowValue(nets)">
                                    <td style="width: 10px" data-target="#addAccompNetwork" data-toggle="modal" style="width:10px !important">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccompNetwork" data-toggle="modal">{{ nets.fld_desc }}</td>
                                    <td data-target="#addAccompNetwork" data-toggle="modal">{{ nets.net_output | truncate 40 }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end network and linkages accomplishment-->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='res_a'): ?>
                          <div id="accompResearch" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit Research accomplishment-->
                            <div class="modal fade" id="addAccompResearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Research and Development</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" v-model="nid" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>           
                                        </div>
                                        <input type="hidden" value="1" name="istarget">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit Research accomplishment-->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccompResearch" data-toggle="modal" data-target="#addAccompResearch"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Research and Development</button>
                            <br><br><br>
                            <table id="tblResearchAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="res: research" data_value="{{ nets.id }}" v-on="click: getRowValue(res)">
                                    <td style="width: 10px !important" data-target="#addAccompResearch" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccompResearch" data-toggle="modal">{{ res.fld_desc }}</td>
                                    <td data-target="#addAccompResearch" data-toggle="modal">{{ res.res_output }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Research accomplishment-->
                          </div>
                      <?php endif; ?>

                      <?php if($this->uri->segment(5)=='oth_a'): ?>
                          <div id="accompOthers" v-cloak>
                            <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
                            <!-- begin Modal new/edit Others Accomplishment-->
                            <div class="modal fade" id="addAccompOther" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                    <h4 class="modal-title" id="myModalLabel">{{ action | capitalize }} Others</h4>
                                    </div>
                                    <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                                        <input type="hidden" v-model="nid" id="semid_edit" name="semid_edit">
                                        <div class="modal-body">
                                            <div class="form-group {{ haserror.field }}">
                                                <label class="control-label" for="cmbfield">Field <span class="required">*</span> </label>
                                                <select name="cmbfield" id="cmbfield" class="form-control" v-model="field">
                                                    <option value="">Choose Option</option>
                                                    <option v-repeat="field: fields" value="{{ field.fld_id }}">{{ field.fld_desc }}</option>
                                                </select>
                                                <span class="help-block" v-if="ErrorValidation.field">{{ Error.field }}</span>
                                            </div>
                                            <div class="form-group {{ haserror.output }}">
                                                <label class="control-label" for="name">Output <span class="required">*</span></label>
                                                <textarea id="txtoutput" v-model="output" class="form-control" name="txtoutput"></textarea>
                                                <span class="help-block" v-if="ErrorValidation.output">{{ Error.output }}</span>
                                            </div>           
                                        </div>
                                        <input type="hidden" value="1" name="istarget">
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                                            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                                        </div>
                                    </form>
                                </div>
                              </div>
                            </div>
                            <!-- End Modal new/edit Others Accomplishment-->
                            <br><br>
                            <button type="button" class="btn btn-primary pull-right" v-on="click: addAccompOther" data-toggle="modal" data-target="#addAccompOther"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Others</button>
                            <br><br><br>
                            <table id="tblOthersAccomp" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                  <tr>
                                    <th>No.</th>
                                    <th>Field</th>
                                    <th>Output</th>
                                    <td></td>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr v-repeat="oth: others" data_value="{{ nets.id }}" v-on="click: getRowValue(oth)">
                                    <td style="width: 10px !important" data-target="#addAccompOther" data-toggle="modal">{{ ($index + 1) }}</td>
                                    <td data-target="#addAccompOther" data-toggle="modal">{{ oth.fld_desc }}</td>
                                    <td data-target="#addAccompOther" data-toggle="modal">{{ oth.oth_output }}</td>
                                    <td style="width: 20px !important;" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
                                  </tr>
                                </tbody>
                            </table>
                            <!-- end Others Accomplishment-->
                          </div>
                      <?php endif; ?>
                    </div>
                  </div>
                </div>
                <!-- end target Accomplishments -->

                <!-- begin report submitted -->
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingThree">
                    <h4 class="panel-title">
                      <a class="collapsed" <?=$this->uri->segment(2)=='addServiceAward' ? 'data-toggle="modal" data-target="#notifModal"' : ''?> role="button" data-toggle="collapse" data-parent="#accordion" href="<?=$this->uri->segment(2)=='editServiceAward'?'#accompReportSub':''?>" aria-expanded="false" aria-controls="accompReportSub">
                        Reports Submitted
                      </a>
                    </h4>
                  </div>
                  <div id="accompReportSub" class="panel-collapse collapse <?=($this->uri->segment(4) == 'report' ? 'in' : '')?>" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <!-- Begin report -->
                      <div id="reports_sa" v-cloak>
                        <form method="POST" action="<?=base_url('{{ actionName }}'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                        <input type="hidden" v-model="urlAction" value="<?=$this->uri->segment(2)?>" name="">
                        <input type="hidden" name="reportId" value="<?=$arrReports[0]['srp_id']?>">
                        <div class="row">
                            <div class="col-sm-8">
                              <div class="col-md-4">Balik Scientist: Submitted Progress Report</div>
                              <div class="col-md-4" id="divProgress">
                                  <label class="radio-inline"><input type="radio" v-model="progreport" name="radProgReport" id="progReport" value='n'  <?=($arrReports[0]['srp_progress'] == 'n') ? 'checked' : '' ?>> N </label>
                                  <label class="radio-inline"><input type="radio" v-model="progreport" name="radProgReport" id="progReport" value='y'  <?=($arrReports[0]['srp_progress'] == 'y') ? 'checked' : '' ?>> Y </label>
                                  <label class="radio-inline"><input type="radio" v-model="progreport" name="radProgReport" id="progReport" value='na' <?=($arrReports[0]['srp_progress'] == 'na') ? 'checked' : '' ?>> NA</label>
                                  <span class="help-block required" v-if="ErrorValidation.progress">{{ Error.progress }}</span>
                              </div>
                              <div class="col-md-4" id="prdate">
                                Date Submitted: <input id="progreportSubDate" v-model="progreportSubDate" name="progreportSubDate" type="text" value="<?=$arrReports[0]['srp_progress_date']?>">
                                <span class="help-block required" v-if="ErrorValidation.progressDate" id="spanProgress">{{ Error.progressDate }}</span>
                              </div>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-8">
                              <div class="col-md-4">Balik Scientist: Submitted Terminal Report</div>
                              <div class="col-md-4" id="divTerminal">
                                  <label class="radio-inline"><input type="radio" v-model="termreport" name="radtermReport" id="termReport" value='n' <?=($arrReports[0]['srp_terminal'] == 'n') ? 'checked' : '' ?>> N </label>
                                  <label class="radio-inline"><input type="radio" v-model="termreport" name="radtermReport" id="termReport" value='y' <?=($arrReports[0]['srp_terminal'] == 'y') ? 'checked' : '' ?>> Y </label>
                                  <label class="radio-inline"><input type="radio" v-model="termreport" name="radtermReport" id="termReport" value='na' <?=($arrReports[0]['srp_terminal'] == 'na') ? 'checked' : '' ?>> NA</label>
                                  <span class="help-block required" v-if="ErrorValidation.termreport">{{ Error.termreport }}</span>
                              </div>
                              <div class="col-md-4" id="trdate">
                                Date Submitted: <input id="termreportSubDate" v-model="termDate" name="termreportSubDate" type="text" value="<?=$arrReports[0]['srp_terminal_date']?>">
                                <span class="help-block required" v-if="ErrorValidation.termDate" id="spanTerminal">{{ Error.termDate }}</span>
                              </div>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-8">
                              <div class="col-md-4">Balik Scientist: Submitted BSP Feedback Form</div>
                              <div class="col-md-4" id="divSubfeedback">
                                  <label class="radio-inline"><input type="radio" v-model="bspreport" name="radbspReport" id="bspReport" value='n' <?=($arrReports[0]['srp_bspfeedback'] == 'n') ? 'checked' : '' ?>> N </label>
                                  <label class="radio-inline"><input type="radio" v-model="bspreport" name="radbspReport" id="bspReport" value='y' <?=($arrReports[0]['srp_bspfeedback'] == 'y') ? 'checked' : '' ?>> Y </label>
                                  <label class="radio-inline"><input type="radio" v-model="bspreport" name="radbspReport" id="bspReport" value='na' <?=($arrReports[0]['srp_bspfeedback'] == 'na') ? 'checked' : '' ?>> NA</label>
                                  <span class="help-block required" v-if="ErrorValidation.bspreport">{{ Error.bspreport }}</span>
                              </div>
                              <div class="col-md-4" id="bspdate">
                                Date Submitted: <input id="bspreportSubDate" v-model="bspreportSubDate" name="bspreportSubDate" type="text" value="<?=$arrReports[0]['srp_bspfeedback_date']?>">
                                <span class="help-block required" v-if="ErrorValidation.bspDate" id="spanbspfeedback">{{ Error.bspDate }}</span>
                              </div>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-8">
                              <div class="col-md-4">Host Institution: Submitted BSP Feedback Form</div>
                              <div class="col-md-4" id="divInsfeedback">
                                  <label class="radio-inline"><input type="radio" v-model="bspInsreport" name="radbspInsReport" id="bspInsReport" value='n' <?=($arrReports[0]['srp_feedback'] == 'n') ? 'checked' : '' ?>> N </label>
                                  <label class="radio-inline"><input type="radio" v-model="bspInsreport" name="radbspInsReport" id="bspInsReport" value='y' <?=($arrReports[0]['srp_feedback'] == 'y') ? 'checked' : '' ?>> Y </label>
                                  <label class="radio-inline"><input type="radio" v-model="bspInsreport" name="radbspInsReport" id="bspInsReport" value='na' <?=($arrReports[0]['srp_feedback'] == 'na') ? 'checked' : '' ?>> NA</label>
                                  <span class="help-block required" v-if="ErrorValidation.bspInsreport">{{ Error.bspInsreport }}</span>
                              </div>
                              <div class="col-md-4" id="bspInsdate">
                                Date Submitted: <input id="bspInsreportSubDate" v-model="bspInsreportSubDate" name="bspInsreportSubDate" type="text" value="<?=$arrReports[0]['srp_feedback_date']?>">
                                <span class="help-block required" v-if="ErrorValidation.bspInsDate" id="spanbspinsfeedback">{{ Error.bspInsDate }}</span>
                              </div>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-8">
                              <div class="col-md-4">Host Institution: Submitted Host Evaluation Form</div>
                              <div class="col-md-4" id="divEvalform">
                                  <label class="radio-inline"><input type="radio" v-model="evalForm" name="radevalForm" id="evalForm" value='n' <?=($arrReports[0]['srp_evaluation'] == 'n') ? 'checked' : '' ?>> N </label>
                                  <label class="radio-inline"><input type="radio" v-model="evalForm" name="radevalForm" id="evalForm" value='y' <?=($arrReports[0]['srp_evaluation'] == 'y') ? 'checked' : '' ?>> Y </label>
                                  <label class="radio-inline"><input type="radio" v-model="evalForm" name="radevalForm" id="evalForm" value='na' <?=($arrReports[0]['srp_evaluation'] == 'na') ? 'checked' : '' ?>> NA</label>
                                  <span class="help-block required" v-if="ErrorValidation.evalForm">{{ Error.evalForm }}</span>
                              </div>
                              <div class="col-md-4" id="evalFormdate">
                                Date Submitted: <input id="evalFormSubDate" v-model="evalFormSubDate" name="evalFormSubDate" type="text" value="<?=$arrReports[0]['srp_feedback_date']?>">
                                <span class="help-block required" v-if="ErrorValidation.evalFormDate" id="spanevalform">{{ Error.evalFormDate }}</span>
                              </div>
                          </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-8">
                              <div class="col-md-4">Council: Submitted Post-Implementation Evaluation</div>
                              <div class="col-md-4" id="divimpeval">
                                  <label class="radio-inline"><input type="radio" v-model="impeval" name="radimpeval" id="impeval" value='n' <?=($arrReports[0]['srp_implementation'] == 'n') ? 'checked' : '' ?>> N </label>
                                  <label class="radio-inline"><input type="radio" v-model="impeval" name="radimpeval" id="impeval" value='y' <?=($arrReports[0]['srp_implementation'] == 'y') ? 'checked' : '' ?>> Y </label>
                                  <label class="radio-inline"><input type="radio" v-model="impeval" name="radimpeval" id="impeval" value='na' <?=($arrReports[0]['srp_implementation'] == 'na') ? 'checked' : '' ?>> NA</label>
                                  <span class="help-block required" v-if="ErrorValidation.impeval">{{ Error.impeval }}</span>
                              </div>
                              <div class="col-md-4" id="impevaldate">
                                Date Submitted: <input id="impevalSubDate" v-model="impevalSubDate" name="impevalSubDate" type="text" value="<?=$arrReports[0]['srp_implementation_date']?>">
                                <span class="help-block required" v-if="ErrorValidation.impevalSubDate" id="spanimpeval">{{ Error.impevalSubDate }}</span>
                              </div>
                          </div>
                        </div>
                        <br>

                        <div class="col-sm-8 form-group {{ commenthaserror }}">
                          <label class="control-label" for="txtoutput">Comments (if any): </label>
                          <textarea class="form-control" name="txtcomment" v-model="comments"><?=$arrReports[0]['srp_comments']?></textarea>
                          <span class="help-block" v-if="ErrorValidation.comments">{{ Error.comments }}</span>
                        </div>
                        <br>
                        <div class="col-sm-8">
                          <br>
                          <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                        </div>
                      </form>
                      </div>
                      <!-- End report -->
                    </div>
                  </div>
                </div>
                <!-- end report submitted -->

                <!-- begin report submitted -->
                <div class="panel panel-default">
                  <div class="panel-heading" role="tab" id="headingFour">
                    <h4 class="panel-title">
                      <a class="collapsed" <?=$this->uri->segment(2)=='addServiceAward' ? 'data-toggle="modal" data-target="#notifModal"' : ''?> role="button" data-toggle="collapse" data-parent="#accordion" href="<?=$this->uri->segment(2)=='editServiceAward'?'#accorAssSec':''?>" aria-expanded="false" aria-controls="accorAssSec">
                        Assigned Secretariat
                      </a>
                    </h4>
                  </div>
                  <div id="accorAssSec" class="panel-collapse collapse <?=($this->uri->segment(4) == 'sec' ? 'in' : '')?>" role="tabpanel" aria-labelledby="headingFour">
                    <div class="panel-body">
                      <!-- Begin assigned secretariat -->
                      <div id="assignsec" v-cloak>
                        <form class="form-horizontal form-label-left" method="POST" action="<?=base_url('scientists/editsec/'.$this->uri->segment(3).'?subs='.$_GET['subs'])?>">
                          <input type="hidden" value="<?=base_url()?>" v-model="baseUrl">
                          <div class="form-group {{ haserror.processby }}">
                            <input type="hidden" value="<?=$sciSA['processby']?>" v-model="defuser">
                            <label for="cmbSAChangeSched" class="col-sm-2 control-label">Processed By: </label>
                              <div class="col-sm-3">
                                  <select name="cmbusers" id="cmbusers" v-model="processby" class="form-control" style="width: 99% !important">
                                      <option value="">Choose Option</option>
                                      <?php foreach($this->arrTemplateData['arrUsers'] as $user): ?>
                                        <option value="<?=$user['usr_user_id']?>" <?=($user['usr_user_id']==$sciSA['processby']) ? 'selected' : ''?>><?=getFullname($user['usr_fname'], $user['usr_lname'], '', $user['usr_mname'])?></option>
                                      <?php endforeach; ?>
                                    </select>
                                <span class="help-block" v-if="ErrorValidation.processby">{{ Error.processby }}</span>
                              </div>
                          </div>
                          <div class="form-group {{ haserror.processdate }}" id="processdatediv">
                                <label for="txtprocessdate" class="col-sm-2 control-label">Date of Approval: </label>
                                  <div class="col-sm-3">
                                      <input id="txtprocessdate" class="form-control" name="txtprocessdate" type="text" placeholder="YYYY-MM-DD" value="<?=($sciSA['processdate']=='0000-00-00') ? '' : $sciSA['processdate']?>" v-model="processdate" >
                                      <span class="help-block" id="processdatespan">{{ Error.processdate }}</span>
                                  </div>
                            </div>
                            <script type="text/javascript">$(function () { $('#txtprocessdate').datetimepicker({ format: 'YYYY-MM-DD' });});</script>
                            <div class="form-group {{ haserror.processremarks }}">
                                <label for="txtprocessremarks" class="col-sm-2 control-label">Remarks: </label>
                                  <div class="col-sm-3">
                                      <textarea id="txtprocessremarks" class="form-control" name="txtprocessremarks" v-model="processremarks" ><?=$sciSA['processremarks']?></textarea>
                                      <span class="help-block">{{ Error.processremarks }}</span>
                                  </div>
                            </div>
                            <div class="form-group col-sm-3">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <button type="submit" class="btn btn-primary" v-on="click: getValidate">Save</button>
                            </div>
                      </form>
                      </div>
                      <!-- End assigned secretariat -->
                    </div>
                  </div>
                </div>
                <!-- end report submitted -->
              
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form method="post" action="<?=base_url('serviceawards/deleteServiceAwards/'.$this->uri->segment(3))?>">
                <input type="hidden" id="subs" name="subs" value="<?=$_GET['subs']?>">
                <input type="hidden" id="target_id" name="target_id">
                <!-- <input type="text" id="target_name" name="target_name"> -->
                <input type="hidden" name="target_name" value="<?=$this->uri->segment(5)?>">
                <input type="hidden" id="targetPane" name="targetPane">
                Are you sure you want to delete this data <span id="atachment"></span>?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- Begin ToBeAccomplished Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteTBAModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('scientists/deleteScientistTba/'.$this->uri->segment(3))?>">
                <input type="hidden" name="txttba" id="txttba"></input>
                <input type="hidden" name="txtsubs" id="txtsubs" value="<?=$_GET['subs']?>"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End ToBeAccomplished Delete Modal -->

<!-- Begin Notification Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="notifModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
          Unable to open. Please check General Information.
          <br><br>
          <button class="btn btn-success" id="btndupdate" data-dismiss="modal">Okay </button>
        </div>
    </div>
  </div>  
</div>
<!-- End Notification Modal -->

<!-- Begin Notification Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="notifsavesrv">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
          Information Empty!
          <br><br>
          <button class="btn btn-danger" id="btndupdate" data-dismiss="modal">Okay </button>
        </div>
    </div>
  </div>  
</div>
<!-- End Notification Modal -->

  <!-- begin vue js -->
  <script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
  <script src="<?=base_url('assets/vuejs/seminars_vuejs.js')?>"></script>
  <script src="<?=base_url('assets/vuejs/accomplishments_vuejs.js')?>"></script>
  <script src="<?=base_url('assets/vuejs/others.js')?>"></script>
  <script src="<?=base_url('assets/vuejs/citizen_vuejs.js')?>"></script>
  <script src="<?=base_url('assets/vuejs/add_contract_vuejs.js')?>"></script>
  <script src="<?=base_url('assets/production/js/custom/custom.js')?>"></script>
  <!-- end vue js -->

  <script>
    $(document).ready(function() {
      // $('#btnaddphase').hide();
      // $( "div.alert" ).hide();
      setTimeout(function() { $(".alert").alert('close'); }, 2000);
      setTimeout(function(){$('.tbldata').DataTable();}, 0);
      $("#tr td:not(:last-child)").click(function(){
          console.log($(this).closest("tr").data("id"));
      });

      $("#cmbInstitutions").select2({
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });

      var cdatesTotal = parseInt('<?=$condatesTotal?>');
      for (var i = 1; i <= cdatesTotal; i++) {
        $('#tblphase'+i).show();
      }

      $('#cmbTypeOfContract').on('change', function() {
        for (var i = 2; i < 10; i++) {
            $('#tblphase'+i).hide();
            $('#txtContDurFrom1').val('');
            $('#txtContDurTo1').val('');
        }
        for (var i = 1; i < 10; i++) {
            $('#txtContDurFrom'+i).val('');
            $('#txtContDurTo'+i).val('');
        }
      });

      $('#approvalyear').on('change', function() {
        if ($('#approvalyear').is(":checked")){
          $('#divapryear').show();
          $('#divaprdate').hide();
        }else{
          $('#divapryear').hide();
          $('#divaprdate').show();
        }
      });

  });
  </script>

<script src="<?=base_url('assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')?>"></script>
<!-- jQuery autocomplete -->
<script src="<?=base_url('assets/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js')?>"></script>
<script src="<?=base_url('assets/vendors/jquery.hotkeys/jquery.hotkeys.js')?>"></script>

<script>
$(document).ready(function() {
        $('#addrichtext').click(function() {
          $('#descr').val($('#editor').html());
        });
      });

      $(document).ready(function() {
        function initToolbarBootstrapBindings() {
          var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
              'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
              'Times New Roman', 'Verdana'
            ],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
          $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
          });
          $('a[title]').tooltip({
            container: 'body'
          });
          $('.dropdown-menu input').click(function() {
              return false;
            })
            .change(function() {
              $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
            })
            .keydown('esc', function() {
              this.value = '';
              $(this).change();
            });

          $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this),
              target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
          });

          if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $('#editor').offset();

            $('.voiceBtn').css('position', 'absolute').offset({
              top: editorOffset.top,
              left: editorOffset.left + $('#editor').innerWidth() - 35
            });
          } else {
            $('.voiceBtn').hide();
          }
        }

        function showErrorAlert(reason, detail) {
          var msg = '';
          if (reason === 'unsupported-file-type') {
            msg = "Unsupported format " + detail;
          } else {
            console.log("error uploading file", reason, detail);
          }
          $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
        }

        initToolbarBootstrapBindings();

        $('#editor').wysiwyg({
          fileUploadError: showErrorAlert
        });

        window.prettyPrint && prettyPrint();
      });
    </script>
    <!-- /bootstrap-wysiwyg -->

    <!-- jQuery autocomplete -->
    <script>
      $(document).ready(function() {
        var countries = { AD:"Andorra",A2:"Andorra Test",AE:"United Arab Emirates",AF:"Afghanistan",AG:"Antigua and Barbuda",AI:"Anguilla",AL:"Albania",AM:"Armenia",AN:"Netherlands Antilles",AO:"Angola",AQ:"Antarctica",AR:"Argentina",AS:"American Samoa",AT:"Austria",AU:"Australia",AW:"Aruba",AX:"Åland Islands",AZ:"Azerbaijan",BA:"Bosnia and Herzegovina",BB:"Barbados",BD:"Bangladesh",BE:"Belgium",BF:"Burkina Faso",BG:"Bulgaria",BH:"Bahrain",BI:"Burundi",BJ:"Benin",BL:"Saint Barthélemy",BM:"Bermuda",BN:"Brunei",BO:"Bolivia",BQ:"British Antarctic Territory",BR:"Brazil",BS:"Bahamas",BT:"Bhutan",BV:"Bouvet Island",BW:"Botswana",BY:"Belarus",BZ:"Belize",CA:"Canada",CC:"Cocos [Keeling] Islands",CD:"Congo - Kinshasa",CF:"Central African Republic",CG:"Congo - Brazzaville",CH:"Switzerland",CI:"Côte d’Ivoire",CK:"Cook Islands",CL:"Chile",CM:"Cameroon",CN:"China",CO:"Colombia",CR:"Costa Rica",CS:"Serbia and Montenegro",CT:"Canton and Enderbury Islands",CU:"Cuba",CV:"Cape Verde",CX:"Christmas Island",CY:"Cyprus",CZ:"Czech Republic",DD:"East Germany",DE:"Germany",DJ:"Djibouti",DK:"Denmark",DM:"Dominica",DO:"Dominican Republic",DZ:"Algeria",EC:"Ecuador",EE:"Estonia",EG:"Egypt",EH:"Western Sahara",ER:"Eritrea",ES:"Spain",ET:"Ethiopia",FI:"Finland",FJ:"Fiji",FK:"Falkland Islands",FM:"Micronesia",FO:"Faroe Islands",FQ:"French Southern and Antarctic Territories",FR:"France",FX:"Metropolitan France",GA:"Gabon",GB:"United Kingdom",GD:"Grenada",GE:"Georgia",GF:"French Guiana",GG:"Guernsey",GH:"Ghana",GI:"Gibraltar",GL:"Greenland",GM:"Gambia",GN:"Guinea",GP:"Guadeloupe",GQ:"Equatorial Guinea",GR:"Greece",GS:"South Georgia and the South Sandwich Islands",GT:"Guatemala",GU:"Guam",GW:"Guinea-Bissau",GY:"Guyana",HK:"Hong Kong SAR China",HM:"Heard Island and McDonald Islands",HN:"Honduras",HR:"Croatia",HT:"Haiti",HU:"Hungary",ID:"Indonesia",IE:"Ireland",IL:"Israel",IM:"Isle of Man",IN:"India",IO:"British Indian Ocean Territory",IQ:"Iraq",IR:"Iran",IS:"Iceland",IT:"Italy",JE:"Jersey",JM:"Jamaica",JO:"Jordan",JP:"Japan",JT:"Johnston Island",KE:"Kenya",KG:"Kyrgyzstan",KH:"Cambodia",KI:"Kiribati",KM:"Comoros",KN:"Saint Kitts and Nevis",KP:"North Korea",KR:"South Korea",KW:"Kuwait",KY:"Cayman Islands",KZ:"Kazakhstan",LA:"Laos",LB:"Lebanon",LC:"Saint Lucia",LI:"Liechtenstein",LK:"Sri Lanka",LR:"Liberia",LS:"Lesotho",LT:"Lithuania",LU:"Luxembourg",LV:"Latvia",LY:"Libya",MA:"Morocco",MC:"Monaco",MD:"Moldova",ME:"Montenegro",MF:"Saint Martin",MG:"Madagascar",MH:"Marshall Islands",MI:"Midway Islands",MK:"Macedonia",ML:"Mali",MM:"Myanmar [Burma]",MN:"Mongolia",MO:"Macau SAR China",MP:"Northern Mariana Islands",MQ:"Martinique",MR:"Mauritania",MS:"Montserrat",MT:"Malta",MU:"Mauritius",MV:"Maldives",MW:"Malawi",MX:"Mexico",MY:"Malaysia",MZ:"Mozambique",NA:"Namibia",NC:"New Caledonia",NE:"Niger",NF:"Norfolk Island",NG:"Nigeria",NI:"Nicaragua",NL:"Netherlands",NO:"Norway",NP:"Nepal",NQ:"Dronning Maud Land",NR:"Nauru",NT:"Neutral Zone",NU:"Niue",NZ:"New Zealand",OM:"Oman",PA:"Panama",PC:"Pacific Islands Trust Territory",PE:"Peru",PF:"French Polynesia",PG:"Papua New Guinea",PH:"Philippines",PK:"Pakistan",PL:"Poland",PM:"Saint Pierre and Miquelon",PN:"Pitcairn Islands",PR:"Puerto Rico",PS:"Palestinian Territories",PT:"Portugal",PU:"U.S. Miscellaneous Pacific Islands",PW:"Palau",PY:"Paraguay",PZ:"Panama Canal Zone",QA:"Qatar",RE:"Réunion",RO:"Romania",RS:"Serbia",RU:"Russia",RW:"Rwanda",SA:"Saudi Arabia",SB:"Solomon Islands",SC:"Seychelles",SD:"Sudan",SE:"Sweden",SG:"Singapore",SH:"Saint Helena",SI:"Slovenia",SJ:"Svalbard and Jan Mayen",SK:"Slovakia",SL:"Sierra Leone",SM:"San Marino",SN:"Senegal",SO:"Somalia",SR:"Suriname",ST:"São Tomé and Príncipe",SU:"Union of Soviet Socialist Republics",SV:"El Salvador",SY:"Syria",SZ:"Swaziland",TC:"Turks and Caicos Islands",TD:"Chad",TF:"French Southern Territories",TG:"Togo",TH:"Thailand",TJ:"Tajikistan",TK:"Tokelau",TL:"Timor-Leste",TM:"Turkmenistan",TN:"Tunisia",TO:"Tonga",TR:"Turkey",TT:"Trinidad and Tobago",TV:"Tuvalu",TW:"Taiwan",TZ:"Tanzania",UA:"Ukraine",UG:"Uganda",UM:"U.S. Minor Outlying Islands",US:"United States",UY:"Uruguay",UZ:"Uzbekistan",VA:"Vatican City",VC:"Saint Vincent and the Grenadines",VD:"North Vietnam",VE:"Venezuela",VG:"British Virgin Islands",VI:"U.S. Virgin Islands",VN:"Vietnam",VU:"Vanuatu",WF:"Wallis and Futuna",WK:"Wake Island",WS:"Samoa",YD:"People's Democratic Republic of Yemen",YE:"Yemen",YT:"Mayotte",ZA:"South Africa",ZM:"Zambia",ZW:"Zimbabwe",ZZ:"Unknown or Invalid Region" };

        var countriesArray = $.map(countries, function(value, key) {
          return {
            value: value,
            data: key
          };
        });

        // initialize autocomplete with custom appendTo
        $('#autocomplete-custom-append').autocomplete({
          lookup: countriesArray,
          appendTo: '#autocomplete-container'
        });
      });
    </script>
    <!-- /jQuery autocomplete -->