<?
if(isset($arrServiceReport))
{
	
	$hiddenId = "<input type='hidden' = name='txtRepId' value='".$arrServiceReport[0]['srp_id']."'>";
	$progressreport = $arrServiceReport[0]["srp_progress"];
	$progressreportdate = $arrServiceReport[0]["srp_progress_date"];
	$terminal = $arrServiceReport[0]["srp_terminal"];
	$terminal_date = $arrServiceReport[0]["srp_terminal_date"];
	$feedback = $arrServiceReport[0]["srp_bspfeedback"];
	$feedbackdate = $arrServiceReport[0]["srp_bspfeedback_date"];
	$hostfeedback = $arrServiceReport[0]["srp_feedback"];
	$hostfeedbackdate = $arrServiceReport[0]["srp_feedback_date"];
	$evaluation = $arrServiceReport[0]["srp_evaluation"];
	$evaluationdate = $arrServiceReport[0]["srp_evaluation_date"];
	$implementation = $arrServiceReport[0]["srp_implementation"];
	$implementationdate = $arrServiceReport[0]["srp_implementation_date"];
}
else
{
	$progressreport = "";
	$progressreportdate = "";
	$terminal = "";
	$terminal_date = "";
	$feedback = "";
	$feedbackdate = "";
	$hostfeedback = "";
	$hostfeedbackdate = "";
	$evaluation = "";
	$evaluationdate = "";
	$implementation = "";
	$implementationdate = "";

	$hiddenId = "<input type='hidden' = name='txtScientistId' value='".$arrServiceAwards[0]['srv_id']."'>";
}

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8" content="text/plain">

</head>
<body>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
	<div class="x_content">
    
	    <form class="form-horizontal form-label-left" action="<?=base_url('scientists/editReport')?>" method="post" >
    
    	    <div class="item form-group">
               <label class="control-label col-xs-3" for="name">BALIK SCIENTIST: </label>
               <label class="control-label col-xs-3" for="name">Submitted Progress Report</label>
               <div class="col-xs-3">
                    <select name="cmbProgRep" class="form_control" required>
                        <option>Choose Option</option>
                        <option value="0" <?php echo $progressreport==0?"selected":"";?>>No</option>
                        <option value="1" <?php echo $progressreport==1?"selected":"";?>>Yes</option>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-xs-3" for="name"></label>
               <label class="control-label col-xs-3" for="name">Date Submitted</label>
                <div class="col-xs-3">
                	<input id="txtProgressReportDate" class="form-control" name="txtProgressReportDate" type="text" value="<?=$progressreportdate?>">
                </div>
            </div>

            <div class="item form-group">
               <label class="control-label col-xs-3"  for="name"></label>
                <label class="control-label col-xs-3" for="name">Submitted Terminal Report</label>
               <div class="col-xs-3">
                    <select name="cmbTermRep" class="form_control" required>
                        <option>Choose Option</option>
                       <option value="0" <?php echo $terminal==0?"selected":"";?>>No</option>
                        <option value="1" <?php echo $terminal==1?"selected":"";?>>Yes</option>
                    </select>
                 </div>
            </div>
            
            <div class="item form-group">
            	<label class="control-label col-xs-3" for="name"></label>
               <label class="control-label col-xs-3" for="name">Date Submitted</label>
                <div class="col-xs-3">
                	<input id="txtTerminalReportDate" class="form-control" name="txtTerminalReportDate" type="text" value="<?=$terminal_date?>">
                </div>
            </div>
            <div class="item form-group">
               <label class="control-label col-xs-3"  for="name"></label>
                <label class="control-label col-xs-3"  for="name">Submitted BSP Feedback Form</label>
               <div class="col-xs-3">
                    <select name="cmbFeedback" class="form_control" required>
                        <option>Choose Option</option>
                        <option value="0" <?php echo $feedback==0?"selected":"";?>>No</option>
                        <option value="1" <?php echo $feedback==1?"selected":"";?>>Yes</option>
                    </select>
                 </div>
            </div>
 			<div class="item form-group">
            	<label class="control-label col-xs-3" for="name"></label>
               <label class="control-label col-xs-3" for="name">Date Submitted</label>
                <div class="col-xs-3">
                	<input id="txtFeedbackDate" class="form-control" name="txtFeedbackDate" type="text" value="<?=$feedbackdate?>">
                </div>
            </div>
            
            
            <div class="ln_solid"></div>
            <div class="item form-group">
               <label class="control-label col-xs-3" for="name">HOST INSTITUTION: </label>
               <label class="control-label col-xs-3" for="name">Submitted Feedback Form</label>
               <div class="col-xs-3">
                    <select name="cmbHostFeedback" class="form_control" required>
                        <option>Choose Option</option>
                       <option value="0" <?php echo $hostfeedback==0?"selected":"";?>>No</option>
                        <option value="1" <?php echo $hostfeedback==1?"selected":"";?>>Yes</option>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-xs-3" for="name"></label>
               <label class="control-label col-xs-3" for="name">Date Submitted</label>
                <div class="col-xs-3">
                	<input id="txtHostFeedbackDate" class="form-control" name="txtHostFeedbackDate" type="text" value="<?=$hostfeedbackdate?>">
                </div>
            </div>
              <div class="item form-group">
               <label class="control-label col-xs-3" for="name"> </label>
               <label class="control-label col-xs-3" for="name">Submitted Host Evaluation</label>
               <div class="col-xs-3">
                    <select name="cmbHostEvaluation" class="form_control" required>
                        <option>Choose Option</option>
                       <option value="0" <?php echo $evaluation==0?"selected":"";?>>No</option>
                        <option value="1" <?php echo $evaluation==1?"selected":"";?>>Yes</option>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-xs-3" for="name"></label>
               <label class="control-label col-xs-3" for="name">Date Submitted</label>
                <div class="col-xs-3">
                	<input id="txtHostEvaluationDate" class="form-control" name="txtHostEvaluationDate" type="text" value="<?=$evaluationdate?>">
                </div>
            </div>
            
            <div class="ln_solid"></div>
            <div class="item form-group">
               <label class="control-label col-xs-3" for="name">COUNCIL: </label>
               <label class="control-label col-xs-3" for="name">Submitted Post-Implementation Evaluation Report</label>
               <div class="col-xs-3">
                    <select name="cmbCouncilPostImple" class="form_control" required>
                        <option>Choose Option</option>
                      <option value="0" <?php echo $implementation==0?"selected":"";?>>No</option>
                        <option value="1" <?php echo $implementation==1?"selected":"";?>>Yes</option>
                    </select>
                 </div>
            </div>
            <div class="item form-group">
            	<label class="control-label col-xs-3" for="name"></label>
               <label class="control-label col-xs-3" for="name">Date Submitted</label>
                <div class="col-xs-3">
                	<input id="txtCouncilPostImpleDate" class="form-control" name="txtCouncilPostImpleDate" type="text" value="<?=$implementationdate?>">
                </div>
            </div>
            <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-12 col-md-offset-5">
                  <button type="submit" class="btn btn-primary" name="btnSubmitReport" value="cancel">Reset</button>
                   <button type="submit" class="btn btn-primary" name="btnSubmitReport" value="editReport">Reset</button>
				  <?=$hiddenId?>
                </div>
              </div>          
         </form>
     </div>
   </div>
</div>
</body>
</html>

<s