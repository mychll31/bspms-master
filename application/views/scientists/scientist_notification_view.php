<style type="text/css">
	a {
		color: #003bab;
		font-weight: bold;
	}
</style>
<h2 align="center">
	SUCCESSFULLY CREATED <br> <br>
	<a href="<?=base_url('scientists/edit/'.$this->uri->segment(3))?>">
		<?=ucfirst($arrScientist[0]["sci_first_name"])?>
		<?=strtoupper($arrScientist[0]["sci_middle_initial"] == "" ? $arrScientist[0]["sci_middle_name"][0] : $arrScientist[0]["sci_middle_initial"])?>.
		<?=ucfirst($arrScientist[0]["sci_last_name"])?>
	</a>
</h2>