<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/production/js/jquery.maskedinput.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">

<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
table tbody td:last-child{
    background-color: #fff !important; 
}
td.sorting {
    display: none; 
}
</style>
<div id="expertises" v-cloak>
<?php echo isset($strNotification)?$strNotification:""; ?>
<?php
if(count($expertiseEdit)>0):
  $form =  base_url('expertise/edit');
  $desc = $expertiseEdit[0]['exp_desc'];
  $hidden = '<input type="hidden" name="txtExpId" value="'.$expertiseEdit[0]['exp_id'].'">';
else:
  $form = base_url('expertise/add');
  $desc = '';
  $hidden = ""; 

endif; 
?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_content">
      <form class="form-horizontal form-label-left" action="{{ action }}" method="post" novalidate>
        <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
        <input type="hidden" v-model='speid' name="txtSpeId">

        <span class="section">Area of Expertise</span>
          <div class="item form-group {{ haserror.description }}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Expertise<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="txtDesc" class="form-control col-md-7 col-xs-12" v-model="description" name="txtDesc" maxlength="100"  value="<?=$desc?>" type="text">
              <span class="help-block" v-if="ErrorValidation.description">{{ Error.description }}</span>
            </div>
          </div> 
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
              <a href="<?=base_url().'expertise'?>" class="btn btn-default">Cancel</a>
              <button type="submit" class="btn btn-primary" name="btnSubmitInstitution" v-on="click: getValidate">Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="x_panel">
    <table id="example" class="tbldata table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th width="10px">No.</th>
          <!-- <th>Code</th> -->
          <th>Expertise</th> 
          <td></td>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; foreach ($this->arrTemplateData['arrExpertise'] as $expert): ?>
        <tr data-id="<?=$expert['exp_id']?>" v-on="click: getRowValue(<?=$expert['exp_id']?>)">
          <td><?=$no++?></td>
          <!--td><?php //$expert['exp_code'] ?></td-->
          <td><?=$expert['exp_desc']?></td>
          <td style="width: 10px !important;" align="center"><a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('expertise/delete')?>" method="post">
                <input type="hidden" name="txtTempId" id="txtTempId"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs --> 
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_library.js')?>"></script>

<script>
  $(document).ready(function() { 
     // data tables
    setTimeout(function(){$('.tbldata').DataTable();}, 0);
    // $(".tbldata").dataTable({})
    setTimeout(function() { $(".alert").alert('close'); }, 2000);

    // delete scientist
    // $("tr td:last-child").click(function(){
    //     $('#txtTempId').val($(this).closest("tr").data("id"));
    // });
    // $("tr td:not(:last-child)").click(function(){
    //     window.location = "<?=base_url().'expertise?id='?>" + $(this).closest("tr").data("id");
    // });
    
  });
</script>