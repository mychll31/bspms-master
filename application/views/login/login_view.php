
    <!-- Bootstrap -->
    <link href="<?=base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url('assets/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=base_url('assets/production/css/custom.css')?>" rel="stylesheet">
    <style type="text/css">
    .help-block{
        color: #a94442;
    }
    h1 {
      font-family: "Brush Script MT", cursive;
      font-size: 55px;
      font-style: normal;
      font-variant: normal;
      font-weight: bolder;
      line-height: 26.4px;
      text-align: center;
      line-height: 1em;
      color: #4a67ae;
      text-shadow: -3px 0 #f9f9f9, 0 3px #cecece;
    }
    body{
  		background-image:url('assets/images/Login.png') !important;
  		background-repeat: no-repeat !important;
  		background-position: right bottom !important;
  		background-attachment: fixed !important;
		}
    input.form-control {
      border-radius: 0 !important;
    }
    .loginForm {
      background-color: white;
      padding: 20px;
      margin: 0 auto;
      width: 450px;
    }
    </style>
  </head>

  <body style="background:#F7F7F7;"  >
    <br><br><br>
      <a class="hiddenanchor" id="toregister"></a>
      <a class="hiddenanchor" id="tologin"></a>
      <h1>Balik Scientist Program Management System</h1>
          <br><br>
          <div class="loginForm" id="ellogin" v-cloak>
            <h4>Login Form</h4>
            <?php echo form_open('login',array('method'=>'post'));?> 
            <div class="form-group <?=isset($error) ? 'has-error' : ''?> has-feedback">
              <span id="helpBlock2" class="help-block"><?=isset($error) ? $error : ''?></span>
            </div>
            <div class="form-group <?=isset($error) ? 'has-error' : ''?> has-feedback {{ haserror.logname }}">
              <input v-model="logname" type="text" name="txtUserName" id="txtUserName" class="form-control" id="exampleInputEmail1" maxlength="20" placeholder="Username" value="<?=isset($uname) ? $uname : ''?>">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <span class="help-block" v-if="ErrorValidation.logname">{{ Error.logname }}</span>
            <div class="form-group <?=isset($error) ? 'has-error' : ''?> has-feedback {{ haserror.logpass }}">
              <input v-model="logpass" type="password" name="txtPassword" id="txtPassword" class="form-control" id="exampleInputPassword1" maxlength="20" placeholder="Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <span class="help-block" v-if="ErrorValidation.logpass">{{ Error.logpass }}</span>
            <button type="submit" class="btn btn-primary" v-on="click: getValidate">Login</button>
            <?php echo form_close();?>
          </div>

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/others.js')?>"></script>
  <script>
    /**
  * @author ComFreek
  * @license MIT (c) 2013-2015 ComFreek <http://stackoverflow.com/users/603003/comfreek>
  * Please retain this author and license notice!
  */
  /*
  (function (exports) {
        function valOrFunction(val, ctx, args) {
            if (typeof val == "function") {
                return val.apply(ctx, args);
            } else {
                return val;
            }
        }

        function InvalidInputHelper(input, options) {
            input.setCustomValidity(valOrFunction(options.defaultText, window, [input]));

            function changeOrInput() {
                if (input.value == "") {
                    input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                } else {
                    input.setCustomValidity("");
                }
            }

            function invalid() {
                if (input.value == "") {
                    input.setCustomValidity(valOrFunction(options.emptyText, window, [input]));
                }
            }

            input.addEventListener("change", changeOrInput);
            input.addEventListener("input", changeOrInput);
        }
        exports.InvalidInputHelper = InvalidInputHelper;
    })(window);

    InvalidInputHelper(document.getElementById("txtUserName"), {
        defaultText: "This field is required.",
        emptyText: "This field is required.",
    });
    InvalidInputHelper(document.getElementById("txtPassword"), {
        defaultText: "This field is required.",
        emptyText: "This field is required.",
    });
    */
  </script>

</body>