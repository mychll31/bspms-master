<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<style type="text/css">
		.divevent {
		  display: table;
		  border: 1px solid #ccc;
		  margin: 5px;
		  /*padding-top:5px;*/
		  width: 200px;
		}
		.divdate {
			display: table-cell;
		}
		.datede {
			background-color: #b42e43;
		}
		.datedo {
			background-color: #4a67ae;
		}
		.dateme {
			background-color: #b42e43;
		}
		.datemo {
			background-color: #4a67ae;
		}
		.dated {
			font-size: 30px;
			vertical-align: middle;
			text-align: center;
			width:20%;
			color: #e4dfdf;
		}
		.datem {
			vertical-align: middle;
			text-align: center;
			width:20%;
			color: #e4dfdf;
		}
		.divtitle {
			padding: 5px;
			vertical-align: top;
			border: 1px solid #ccc;
		}
		span.title {
			font-weight: bolder;
		}
		table {
			border-collapse: collapse;
			width: 300px;
			margin: 5px;
		}
		a {
			text-decoration: none;
		}
	</style>
	<?php if(count($events) > 0): ?>
	<?php $c = false; foreach($events as $event): $c = !$c; ?>
		<table>
			<tr>
				<td class="dated <?=$c ? 'datedo' : 'datede'?>">
					<?=date('d', strtotime($event['cal_dateStart']))?>		
				</td>
				<td class="divtitle" rowspan="2">
					<span class="title"><?=($event['cal_url']=='') ? $event['cal_title'] : '<a title="click the link" href="'.$event['cal_url'].'">'.$event['cal_title'].'</a>' ?></span>
					<br>
					<small>Start: <?=date('M. d', strtotime($event['cal_dateStart']))?> <?=date('g:i a', strtotime($event['cal_timeStart']))?></small>
					<br>
					<small>Start: <?=date('M. d', strtotime($event['cal_dateEnd']))?> <?=date('g:i a', strtotime($event['cal_timeEnd']))?></small>
				</td>
			</tr>
			<tr>
				<td class="datem <?=$c ? 'datemo' : 'dateme'?>">
					<?=date('M', strtotime($event['cal_dateStart']))?>
				</td>
			</tr>
		</table>
	<?php endforeach; ?>
	<?php else: ?>
		<a href="#"> NO EVENTS AT THIS TIME</a>
		<br>
		<a href="#">Check back soon for upcoming events!</a>
	<?php endif; ?>
</body>
</html>