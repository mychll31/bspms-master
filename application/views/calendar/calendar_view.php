
<link href="<?=base_url('assets/production/plugins/simple-line-icons/simple-line-icons.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/plugins/fullcalendar/fullcalendar.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/css/components.min.css')?>" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url('assets/production/css/plugins.min.css')?>" rel="stylesheet" type="text/css" />

<style type="text/css">
    .input-group.bootstrap-timepicker.timepicker {
            width: 120px !important;
    }
    #calendarTrash {
        border: 1px solid #ccc;
        padding: 10px;
        height: 200px;
    }
    span.small {
        color: red;
        font-size: x-small;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit bordered calendar">
            <div class="portlet-title">
                <h3>
                    <i class=" icon-layers font-green"></i>
                    <span class="caption-subject font-green sbold uppercase">Calendar</span>
                </h3>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <!-- BEGIN DRAGGABLE EVENTS PORTLET-->
                        <h3 class="event-form-title margin-bottom-20">Draggable Events</h3>
                        <div id="extern -events">
                            <form class="inline-form">
                                <div class="form-group ">
                                    <label class="control-label">Event Title</label>
                                    <input type="text" value="" class="form-control" id="event_title" />
                                    <span class="small" id="erreventtitle"></span>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label">Event Url</label>
                                    <input type="text" value="" class="form-control" id="event_url" />
                                </div>

                                <div class="form-group ">
                                    <label class="control-label">Event Start</label>
                                    <div class='input-group date' name='eventstart' id='eventstart'>
                                        <input type='text' class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label">Event End</label>
                                    <div class='input-group date' name='eventend' id='eventend'>
                                        <input type='text' class="form-control" />
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label class="control-label">Event Location</label>
                                    <input type="text" value="" class="form-control" id="event_loc" />
                                </div>

                                <div class="form-group ">
                                    <label class="control-label">Remarks</label>
                                    <textarea class="form-control" id="event_remarks"></textarea>
                                </div>

                                <input type="checkbox" id="chkallday"> <span style="color: #abadb3;">All day?</span>
                                
                                <br><br>
                                <a href="javascript:;" id="event_add" class="btn green"> Add Event </a>
                            </form>
                            <br>
                            <div id="event_box" class="margin-bottom-10"></div>
                            <hr class="visible-xs" />
                            <hr>
                            <!-- <div id="calendarTrash" class="calendar-trash">
                                <b><i class="glyphicon glyphicon-trash"></i> &nbsp;Drag here to remove</b>
                                <hr>
                            </div> -->
                            </div>
                        <!-- END DRAGGABLE EVENTS PORTLET-->
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <div id="calendar" class="has-toolbar"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bs-example-modal-sm" tabindex="-1" id="divEvent" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Event Details</h4>
            </div>
            <div class="modal-body">
                <div class="form-group ">
                    <label class="control-label">Event Title</label>
                    <input type="text" class="form-control" id="edit_event_title" />
                    <span class="small" id="erreventtitle"></span>
                </div>

                <div class="form-group ">
                    <label class="control-label">Event Url</label>
                    <input type="text" class="form-control" id="edit_event_url" />
                </div>

                <div class="form-group ">
                    <label class="control-label">Event Start</label>
                    <div class='input-group date' name='eventstart' id='div_edit_eventstart'>
                        <input type='text' class="form-control" id='edit_eventstart' />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <div class="form-group ">
                    <label class="control-label">Event End</label>
                    <div class='input-group date' name='eventend' id='div_edit_eventend'>
                        <input type='text' class="form-control" id='edit_eventend' />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <input type="checkbox" id="edit_chkallday"> <span style="color: #abadb3;">All day?</span>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" id="btnRemoveEvent" data-toggle="modal" data-target="#deletemodal">Delete <span class="glyphicon glyphicon-trash"></span></button>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnEditEvent">Save <span class="glyphicon glyphicon-pencil"></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-sm" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <input type="hidden" id="eventid" name="eventid">
            Are you sure you want to delete this event?
            <br><br>
            <button type="button" data-dismiss="modal" class="btn btn-success btn-sm pull-right" id="btnsubmitdelete">Okay </button>
            <br>
        </div>
    </div>
  </div>
</div>

<script src="<?=base_url('assets/production/plugins/jquery.min.js')?>" type="text/javascript"></script>

<script src="<?=base_url('assets/production/plugins/moment.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/production/plugins/fullcalendar/fullcalendar.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/production/plugins/jquery-ui/jquery-ui.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/production/js/app.min.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/production/js/calendar.js')?>" type="text/javascript"></script>
<script src="<?=base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js')?>"></script>

<script src="<?=base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
<script src="<?=base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
<script src="<?=base_url('assets/production/js/bootstrap-datetimepicker.js')?>"></script>
<script>
    $(document).ready(function() {

        $('#visitdate').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#eventstart, #edit_eventstart').datetimepicker({
            format: 'YYYY-MM-DD hh:mm A'
        });
        $('#eventend, #edit_eventend').datetimepicker({
            useCurrent: false, //Important! See issue #1075
            format: 'YYYY-MM-DD hh:mm A'
        });
        $("#eventstart, #edit_eventstart").on("dp.change", function (e) {
            $('#eventend, #edit_eventend').data("DateTimePicker").minDate(e.date);
        });
        $("#eventend, #edit_eventend").on("dp.change", function (e) {
            $('#eventstart, #edit_eventstart').data("DateTimePicker").maxDate(e.date);
        });

    });
</script>
