
<link href="<?=base_url('assets/production/plugins/simple-line-icons/simple-line-icons.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/plugins/fullcalendar/fullcalendar.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/css/components.min.css')?>" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url('assets/production/css/plugins.min.css')?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style type="text/css">
    .input-group.bootstrap-timepicker.timepicker {
        width: 120px !important;
    }
    #calendarTrash {
        border: 1px solid #ccc;
        padding: 10px;
        height: 200px;
    }
    span.small {
        color: red;
        font-size: x-small;
    }
    .control-label {
        font-weight: bold;
    }
    input.form-control.specify {
        border-top: 0;
        border-left: 0;
        border-right: 0;
    }
    .daterangepicker{z-index:1151 !important;}
    
    table tr {
        cursor: pointer;
    }
    table tbody tr.highlight td {
        background-color: #ccc;
    }
    table tbody tr:hover{
        background-color: #ccc !important; 
    }
    table tbody td:last-child{
        background-color: #fff !important; 
    }
    td.sorting {
        display: none; 
    }
</style>

<div class="row" id="trackingSheet" v-cloak>
    <div class="portlet light portlet-fit bordered calendar">
        <div class="portlet-title">
            <h3>
                <i class=" fa fa-file-o font-green"></i>
                <span class="caption-subject font-green sbold uppercase">Tracking Sheet</span>
            </h3>
        </div>

        <div class="portlet-body">
            <?php $concerns = array('Others', 'Approval', 'Legal Clearance', 'Evaluation'); ?>
            <?php if(count($trackingsheet) > 0): ?>
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Scientist</th>
                        <th>Host Institution</th>
                        <th>Concern</th>
                        <th>Proposed Duration of Visit</th>
                        <th>Forward To</th>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php $no=1; foreach($trackingsheet as $ts): ?>
                    <tr>
                        <td><?=$no++?></td>
                        <td><?=$ts['ts_scientistname']?></td>
                        <td><?=$ts['ins_desc']?></td>
                        <td><?php
                            if($ts['ts_concernid'] !=null):
                                $conc = array();
                                foreach(explode(',', $ts['ts_concernid']) as $c){ array_push($conc, $concerns[$c]);}
                                echo join(',', $conc);
                            else:
                                echo '';
                            endif;
                        ?></td>
                        <td><?=$ts['ts_proposed_duration_startdate']?> to <?=$ts['ts_proposed_duration_enddate']?></td>
                        <td><?=$ts['recs_name']?></td>
                        <?php if($_SESSION['sessUserId'] == $ts['ts_addedby']): ?>
                        <td style="white-space: nowrap; width: 10px !important;" align="center">
                            <a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a>
                            <a data-toggle="modal" href="<?=base_url('trackingSheet/view')?>/<?=$ts['ts_id']?>"><i style="font-size: 20px;" class="fa fa-search">
                        </td>
                        <?php else: ?>
                            <td style="white-space: nowrap; width: 10px !important;" align="center"><a data-toggle="modal" href="<?=base_url('trackingSheet/view')?>/<?=$ts['ts_id']?>"><i style="font-size: 20px;" class="fa fa-search"></i></a></td>
                        <?php endif; ?>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: ?>
                <b>NO RECORDS FOUND</b>
            <?php endif; ?>
            <br><br>
        </div>
    </div>
</div>


<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/others.js')?>"></script>
<script src="<?=base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
<script>
    $(document).ready(function() {
        $('#specconcern').hide();
        $("#chkothers").change(function() {
            if(this.checked) {
                $('#specconcern').show();
            }else{
                $('#specconcern').hide();
            }
        });

        $('#txtaddtlreq').hide();
        $("#chkreqothers").change(function() {
            if(this.checked) {
                $('#txtaddtlreq').show();
            }else{
                $('#txtaddtlreq').hide();
            }
        });

        $('#visitdate').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        setTimeout(function(){$('#example,  #example1').DataTable();}, 0);
        setTimeout(function() { $(".alert").alert('close'); }, 2000);
    });
</script>
