<link href="<?=base_url('assets/production/plugins/simple-line-icons/simple-line-icons.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/plugins/fullcalendar/fullcalendar.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/css/components.min.css')?>" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url('assets/production/css/plugins.min.css')?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style type="text/css">
    .input-group.bootstrap-timepicker.timepicker {
        width: 120px !important;
    }
    #calendarTrash {
        border: 1px solid #ccc;
        padding: 10px;
        height: 200px;
    }
    span.small {
        color: red;
        font-size: x-small;
    }
    .control-label {
        font-weight: bold;
    }
    input.form-control.specify {
        border-top: 0;
        border-left: 0;
        border-right: 0;
    }
    .daterangepicker{z-index:1151 !important;}
    
    table tr {
        cursor: pointer;
    }
    table tbody tr.highlight td {
        background-color: #ccc;
    }
    table tbody tr:hover{
        background-color: #ccc !important; 
    }
    table tbody td:last-child{
        background-color: #fff !important; 
    }
    td.sorting {
        display: none; 
    }
    tr.unread {
        background-color: #ccc !important;
        font-weight: bolder;
    }
    .small {
        font-size: 80% !important;
    }
</style>

<div class="row col-md-4">
    <div class="portlet light portlet-fit bordered calendar">
        <div class="portlet-title">
            <h3>
                <i class=" fa fa-file-o font-green"></i>
                <span class="caption-subject font-green sbold uppercase">Tracking Sheet</span>
            </h3>
        </div>

        <div class="portlet-body">
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">CY <?=date('Y')?></label>
                </div>
            </div>
            <br>

            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Scientist</label>
                </div>
                <div class="col-md-8">
                    <?=$trackingsheet['ts_scientistname']?>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Host Institution</label>
                </div>
                <div class="col-md-8">
                    <?=$trackingsheet['ins_desc']?>
                </div>
            </div>
            <hr>

            <?php $concerns = array('Others', 'Approval', 'Legal Clearance', 'Evaluation'); ?>
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Concern</label>
                </div>
                <div class="col-md-8">
                    <?php
                        if($trackingsheet['ts_concernid']!=null):
                            $conc = array();
                            foreach(explode(',', $trackingsheet['ts_concernid']) as $c){ array_push($conc, $concerns[$c]);}
                            echo join(',', $conc);
                        else:
                            echo '';
                        endif;
                    ?>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Proposed Duration of Visit</label>
                </div>
                <div class="col-md-8">
                    <?=$trackingsheet['ts_proposed_duration_startdate']?> to <?=$trackingsheet['ts_proposed_duration_enddate']?>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Assigned Secretariat</label>
                </div>
                <div class="col-md-8">
                    <?=getFullname($trackingsheet['usr_lname'], $trackingsheet['usr_fname'], $trackingsheet['usr_mname'], '')?> (<b><?=$trackingsheet['cil_code']?></b>)
                </div>
            </div>
            <hr>

            <?php $typeofaward = array('', 'Short Term', 'Long Term', 'New', 'Subsequent'); ?>
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Type of Award</label>
                </div>
                <div class="col-md-8">
                    <?php
                        if($trackingsheet['ts_typeofaward']!=null):
                            $award = array();
                            foreach(explode(',', $trackingsheet['ts_typeofaward']) as $awrd){ array_push($award, $typeofaward[$awrd]);}
                            echo join(',', $award);
                        endif;
                    ?>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Application Package</label>
                </div>
                <div class="col-md-8">
                    <?php
                        $app_package = array();
                        foreach(explode(',', $trackingsheet['ts_applicationpackage']) as $pack){
                            $key = array_search($pack, array_column($apppackage, 'app_id'));
                            array_push($app_package, '<li>'.$apppackage[$key]['app_label']);
                        }
                        echo join('<br>', $app_package);
                    ?>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Additional Requirements for Subsequent</label>
                </div>
                <div class="col-md-8">
                    <?php
                        $addtl_reqs = array();
                        foreach(explode(',', $trackingsheet['ts_addtionalreq']) as $req){
                            $key = array_search($req, array_column($reqs, 'req_id'));
                            array_push($addtl_reqs, '<li>'.$reqs[$key]['req_label']);
                        }
                        echo join('<br>', $addtl_reqs);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $complete = 0; ?>

<div class="row col-md-8" id="trackaction" v-cloak>
    <?php if($trackingsheet['ts_iscomplete'] == 1): ?>
    <div class="alert alert-success">
        <strong>Alert!</strong> This tracking sheet already completed.
    </div>
    <?php endif; ?>
    
    <?php if($trackingsheet['ts_iscomplete']==0): ?>
    <div class="portlet light portlet-fit bordered calendar">
        <div class="portlet-body">
        <form class="form" action="<?=base_url()?>{{ actionname }}/<?=$this->uri->segment(3)?>" method="post" novalidate>
            <input type="hidden" value="<?=base_url()?>" v-model='baseUrl'>
            <div class="row">
                <div class="form-group {{ haserror.visitdate }} col-md-4">
                    <label class="control-label">Date</label>
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" style="background-color: rgba(255, 255, 255, 0);" name="visitdate" readonly="readonly" id="visitdate" class="form-control" />
                    </div>
                    <span class="help-block" v-if="ErrorValidation.visitdate">{{ Error.visitdate }}</span>
                </div>
                
                <div class="form-group {{ haserror.recipient }} col-md-4">
                    <label class="control-label">Forward To</label>
                    <select v-model="recipient" name="recipient" class="form-control" >
                        <option value=""></option>
                        <?php foreach($recievers as $reciever): ?><option value="<?=$reciever['recs_id']?>"><?=$reciever['recs_name']?></option><?php endforeach; ?>
                    </select>
                    <span class="help-block" v-if="ErrorValidation.recipient">{{ Error.recipient }}</span>
                </div>
            </div>
            <div class="row">
                <div class="form-group {{ haserror.remarks }} col-md-8">
                    <label class="control-label">Action Taken / Remarks</label>
                    <textarea v-model="remarks" name="remarks" class="form-control" ></textarea>
                    <span class="help-block" v-if="ErrorValidation.remarks">{{ Error.remarks }}</span>
                </div>
            </div>
            <div class="row">
                <div class="form-actions">
                    <button type="submit" class="btn blue" v-on="click: getValidate" >Submit</button>
                    <a href="" class="btn default">Cancel</a>
                </div>
            </div>
        </form>
        </div>
    </div>
    <?php endif; ?>
    <div class="portlet light portlet-fit bordered calendar">
        <div class="portlet-body">
            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Date</th>
                        <th>Assigned Secretariat</th>
                        <th>Forward To</th>
                        <th>Action Taken / Remarks</th>
                        <th nowrap>No. of Days</th>
                        <th>Date Sent /<br>Received</th>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php if(count($actions) > 0): ?>
                    <?php 
                        $no=1;
                        foreach($actions as $act):
                            $complete = $complete + $act['action_isReceived'];
                    ?>
                        <!-- tr opening -->
                        <tr 
                            <?php 
                                if($_SESSION['sessUserId'] == $act['action_secretariat'])
                                    echo 'v-on="click: getRowValue('.$act['action_id'].')"';
                                else
                                    echo 'v-on="click: getRowId('.$act['action_id'].')"';

                                $check = false;
                                if(checkAccess($_SESSION['sessAccessLevel'], 'limit_scientist')>0){
                                    if($act['usr_council'] == $_SESSION['sessCouncilid']){
                                        #notAdmin
                                        if($act['recs_cil_id'] == ''){
                                            $check = true;
                                        }else{
                                            $check = false;
                                        }
                                    }else{
                                        if($act['recs_cil_id'] == $_SESSION['sessCouncilid']){
                                            $check = true;
                                        }else{
                                            $check = false;
                                        }
                                    }
                                }else{
                                    #Admin
                                    if($act['recs_cil_id'] == '')
                                        $check = true;
                                    else
                                        $check = false;
                                }

                                if($check && $act['action_isReceived'] == 0){
                                    echo 'class="unread"';
                                }

                                $trash = false;
                                if($act['usr_council'] == $_SESSION['sessCouncilid']){
                                    $trash = true;
                                }else{
                                    $trash = false;
                                }
                             ?>
                        ><!-- tr closing -->
                            <td nowrap><?=$no++?></td>
                            <?php if($act['action_dateFrom'] == $act['action_dateTo']): ?>
                                <td nowrap><?=date('M. d, Y', strtotime($act['action_dateFrom']))?></td>
                            <?php else: ?>
                                <td nowrap><?=date('M. d, Y', strtotime($act['action_dateFrom']))?> - <br><?=date('M. d, Y', strtotime($act['action_dateTo']))?></td>
                            <?php endif; ?>
                            <td nowrap><?=getFullname($act['usr_lname'], $act['usr_fname'], $act['usr_mname'], '')?><br>(<?=$act['cil_code']?>)</td>
                            <td><?=$act['recs_name']?></td>
                            <td><?=$act['action_remarks']?></td>
                            <?php
                                $secs = strtotime($act['action_dateTo']) - strtotime($act['action_dateFrom']);
                                if($act['action_dateTo'] == $act['action_dateFrom']){
                                    $secs = strtotime(date('Y-m-d')) - strtotime($act['action_dateFrom']);
                                }
                                $days = $secs / 86400;
                             ?>
                            <td><?=$days?></td>
                            <td><?php
                                    $sentby = getUserById($act['action_createdby']);
                                    $receivedby = $act['action_receivedBy']==null ? null : getUserById($act['action_receivedBy']);
                                    if($receivedby == null){
                                        $receivedby['usr_fname'] = '';
                                        $receivedby['usr_lname'] = '';
                                    }
                                ?>
                                <small>Sent: <?=$sentby['usr_fname'].' '.$sentby['usr_lname']?><br><?=date('M-d-y', strtotime($act['action_createddate']))?></small>
                                <br>
                                <small>Received: <?=$receivedby['usr_fname'].' '.$receivedby['usr_lname']?><br><?=($act['action_receivedDate'] == null) ? '' : date('M-d-y', strtotime($act['action_receivedDate']))?></small>
                            </td>
                            <td style="width:20px;white-space: nowrap;">
                                 <?php if($check && $act['action_isReceived'] == 0): ?>
                                    <a title="Click to Receive" data-toggle="modal" data-target="#receivedModal" href="#"><i style="font-size: 20px;" class="fa fa-check"></i></a></i></a>
                                 <?php endif; ?>
                                 <?php if($trash && $act['action_isReceived'] == 0 && $trackingsheet['ts_iscomplete']==0): ?>
                                    <a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></i></a>
                                 <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php else: ?>
                        <tr><td colspan="7">NO RECORDS FOUND</td></tr>
                    <?php endif; ?>
                </tbody>
            </table>
        <br>
        </div>
        <?php if($trackingsheet['ts_iscomplete']==0 && $complete > 0): ?>
        <button class="btn btn-success pull-left" style="margin-left: 20px;" data-toggle="modal" data-target="#processcomplete">PROCESS COMPLETE</button>
        <?php endif; ?>
        <br><br>
    </div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('trackingSheet/delete_action/'.$this->uri->segment(3))?>" method="post">
                <input type="hidden" name="txtdelfield" id="txtdelfield"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div> 
</div>
<!-- End Delete Modal -->

<!-- Begin Received Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="receivedModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('trackingSheet/receive_action/'.$this->uri->segment(3))?>" method="post">
                <input type="hidden" name="txtrecaction" id="txtrecaction"></input>
                Action Received
                <br><br>
                <button type="submit" class="btn btn-primary" id="btndupdate">Okay </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">Cancel </button>
            </form>
        </div>
    </div>
  </div> 
</div>
<!-- End Received Modal -->

<!-- Begin process Complete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="processcomplete">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('trackingSheet/complete/'.$this->uri->segment(3))?>" method="post">
                Are you sure you want to complete this process? 
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div> 
</div>
<!-- End process Complete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/others.js')?>"></script>
<script src="<?=base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
<script>
    $(document).ready(function() {
        $('#specconcern').hide();
        $("#chkothers").change(function() {
            if(this.checked) {
                $('#specconcern').show();
            }else{
                $('#specconcern').hide();
            }
        });

        $('#txtaddtlreq').hide();
        $("#chkreqothers").change(function() {
            if(this.checked) {
                $('#txtaddtlreq').show();
            }else{
                $('#txtaddtlreq').hide();
            }
        });

        $('#visitdate').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        setTimeout(function(){$('#example,  #example1').DataTable();}, 0);  
    });
</script>
