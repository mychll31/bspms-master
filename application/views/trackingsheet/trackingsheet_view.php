<link href="<?=base_url('assets/production/plugins/simple-line-icons/simple-line-icons.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/plugins/fullcalendar/fullcalendar.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/production/css/components.min.css')?>" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?=base_url('assets/production/css/plugins.min.css')?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style type="text/css">
    .input-group.bootstrap-timepicker.timepicker {
        width: 120px !important;
    }
    #calendarTrash {
        border: 1px solid #ccc;
        padding: 10px;
        height: 200px;
    }
    span.small {
        color: red;
        font-size: x-small;
    }
    .control-label {
        font-weight: bold;
    }
    input.form-control.specify {
        border-top: 0;
        border-left: 0;
        border-right: 0;
    }
    .daterangepicker{z-index:1151 !important;}
    
    table tr {
        cursor: pointer;
    }
    table tbody tr.highlight td {
        background-color: #ccc;
    }
    table tbody tr:hover{
        background-color: #ccc !important; 
    }
    table tbody td:last-child{
        background-color: #fff !important; 
    }
    td.sorting {
        display: none; 
    }
</style>

<div id="trackingSheet" v-cloak>
<div class="row">
    <div class="portlet light portlet-fit bordered calendar">
        <div class="portlet-title">
            <h3>
                <i class=" fa fa-file-o font-green"></i>
                <span class="caption-subject font-green sbold uppercase">Tracking Sheet</span>
            </h3>
        </div>

        <div class="portlet-body">
            <form class="form" action="<?=base_url()?>{{ actionname }}" method="post" novalidate>
            <input type="hidden" value="<?=base_url()?>" v-model='baseUrl'>
            <div class="row">
                <div class="form-group">
                    <div class="col-md-6 col-md-4">
                        <label class="control-label">CY <?=date('Y')?></label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group {{ haserror.scientist }} col-md-4">
                    <label class="control-label">Scientist</label>
                    <input type="text" name="scientist" v-model="scientist" class="form-control">
                    <span class="help-block" v-if="ErrorValidation.scientist">{{ Error.scientist }}</span>
                </div>

                <div class="form-group {{haserror.host}} col-md-4">
                    <label class="control-label">Host Institution</label>
                    <select class="form-control" v-model="host" name="host">
                        <option value=""></option>
                        <?php foreach($hosts as $host): ?>
                            <option value="<?=$host['ins_id']?>"><?=$host['ins_desc']?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="help-block" v-if="ErrorValidation.host">{{ Error.host }}</span>
                </div>

                <div class="form-group {{haserror.concern}} col-md-4">
                    <label class="control-label">Concern</label>
                    <div class="checkbox-list" id="divconcern">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox21"><span class="checked"><input type="checkbox" id="chkapproval" value="1"></span></div> Approval </label>
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox22"><span class="checked"><input type="checkbox" id="chklegclearance" value="2"></span></div> Legal Clearance </label>
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox22"><span class="checked"><input type="checkbox" id="chkeval" value="3"></span></div> Evaluation </label>
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox22"><span class="checked"><input type="checkbox" id="chkothers" value="0"></span></div> Others </label>
                    </div>
                    <input type="hidden" id="chkconcern" name="chkconcern">
                    <input type="text" class="form-control specify" id="specconcern" placeholder="Please specify" v-model="txtconcern" name="txtconcern_others">
                    <span class="help-block" v-if="ErrorValidation.concern">{{ Error.concern }}</span><br>
                </div>
            </div>

            <div class="row">
                <div class="form-group {{haserror.visitdate}} col-md-4">
                    <label class="control-label">Proposed Duration of Visit</label>
                    <div class="input-prepend input-group">
                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" style="background-color: rgba(255, 255, 255, 0);" name="visitdate" readonly="readonly" id="visitdate" class="form-control" />
                    </div>
                    <span class="help-block" v-if="ErrorValidation.visitdate">{{ Error.visitdate }}</span>
                </div>

                <div class="form-group {{haserror.asssec}} col-md-4">
                    <label class="control-label">Assigned Secretariat</label>
                    <select class="form-control" v-model="asssec" name="asssec">
                        <option value=""></option>
                        <?php foreach($users as $user): ?>
                            <option value="<?=$user['usr_user_id']?>"><?=getFullname($user['usr_lname'], $user['usr_fname'], $user['usr_mname'], '').' - '.$user['cil_code']?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="help-block" v-if="ErrorValidation.asssec">{{ Error.asssec }}</span>
                </div>

                <div class="form-group col-md-4">
                    <label class="control-label">&nbsp;</label>
                    <div class="checkbox-list" id="divtypeofaward">
                        &nbsp;&nbsp;&nbsp;
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox21"><span class="checked"><input type="checkbox" id="chkshortterm" value="1"></span></div> Short Term </label>
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox22"><span class="checked"><input type="checkbox" id="chklongterm" value="2"></span></div> Long Term </label>
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox22"><span class="checked"><input type="checkbox" id="chknew" value="3"></span></div> New </label>
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox22"><span class="checked"><input type="checkbox" id="chksubsequent" value="4"></span></div> Subsequent </label>
                    </div>
                    <input type="hidden" id="chktypeofaward" name="chktypeofaward">
                </div>
            </div>

            <div class="row">
                <div class="form-group col-xs-6">
                    <label class="control-label">Application Package</label>
                    <div class="checkbox-list" id="applicationPackage">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <?php foreach($apppackage as $ts): ?>
                            <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox21"><span class="checked"><input type="checkbox" id="chkapp<?=$ts['app_id']?>" value="<?=$ts['app_id']?>"></span></div> <?=$ts['app_label']?> </label><br>
                        <?php endforeach; ?>
                    </div>
                        <input type="hidden" id="chkapppackage" name="chkapppackage">
                        <span class="help-block"></span>

                </div>
                <div class="form-group {{haserror.reqs}} col-xs-6">
                    <label class="control-label">Additional Requirements for Subsequent</label>
                    <div class="checkbox-list" id="addtlrequirements">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <?php foreach($reqs as $req): ?>
                            <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox21"><span class="checked"><input type="checkbox" id="chkreq<?=$req['req_id']?>" value="<?=$req['req_id']?>"></span></div> <?=$req['req_label']?> </label><br>
                        <?php endforeach; ?>
                        <label class="checkbox-inline"><div class="checker" id="uniform-inlineCheckbox21"><span class="checked"><input type="checkbox" id="chkreqothers" value="0"></span></div> Others </label>
                    </div>
                    <input type="text" class="form-control specify" id="txtaddtlreq" placeholder="Please specify" name="txtaddtlreq_others">
                    <input type="hidden" id="chkaddtlreqs" name="chkaddtlreqs">
                    <span class="help-block" v-if="ErrorValidation.reqs">{{ Error.reqs }}</span>
                </div>
            </div>

            <div class="row">
                <div class="form-actions">
                    <button type="submit" class="btn blue" v-on="click: getValidate">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>

<?php $concerns = array('Others', 'Approval', 'Legal Clearance', 'Evaluation'); ?>
<div class="x_panel">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Scientist</th>
                <th>Host Institution</th>
                <th>Concern</th>
                <th>Proposed Duration of Visit</th>
                <td></td>
            </tr>
        </thead>
        <tbody>
            <?php $no=1; foreach($trackingsheet as $ts): ?>
                <tr v-on="click: getRowValue(<?=$ts['ts_id']?>)">
                    <td><?=$no++?></td>
                    <td><?=$ts['ts_scientistname']?></td>
                    <td><?=$ts['ins_desc']?></td>
                    <td><?php
                        if($ts['ts_concernid'] !=null):
                            $conc = array();
                            foreach(explode(',', $ts['ts_concernid']) as $c){ array_push($conc, $concerns[$c]);}
                            echo join(',', $conc);
                        else:
                            echo '';
                        endif;
                    ?></td>
                    <td><?=$ts['ts_proposed_duration_startdate']?> to <?=$ts['ts_proposed_duration_enddate']?></td>
                    <?php if($_SESSION['sessUserId'] == $ts['ts_addedby']): ?>
                        <td style="width:20px;white-space: nowrap;">
                            <a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a>
                            <a data-toggle="modal" href="<?=base_url('trackingSheet/view')?>/<?=$ts['ts_id']?>"><i style="font-size: 20px;" class="fa fa-search">
                        </td>
                    <?php else: ?>
                         <td style="width:20px;white-space: nowrap;">
                            <a data-toggle="modal" href="<?=base_url('trackingSheet/view')?>/<?=$ts['ts_id']?>"><i style="font-size: 20px;" class="fa fa-search">
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
</div>
<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('trackingSheet/delete')?>" method="post">
                <input type="hidden" name="txtdelfield" id="txtdelfield"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div> 
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/others.js')?>"></script>
<script src="<?=base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js')?>"></script>
<script>
    $(document).ready(function() {
        $('#specconcern').hide();
        $("#chkothers").change(function() {
            if(this.checked) {
                $('#specconcern').show();
            }else{
                $('#specconcern').hide();
            }
        });

        $('#txtaddtlreq').hide();
        $("#chkreqothers").change(function() {
            if(this.checked) {
                $('#txtaddtlreq').show();
            }else{
                $('#txtaddtlreq').hide();
            }
        });

        $('#visitdate').daterangepicker(null, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        setTimeout(function(){$('#example,  #example1').DataTable();}, 0);
        setTimeout(function() { $(".alert").alert('close'); }, 2000);
    });
</script>
