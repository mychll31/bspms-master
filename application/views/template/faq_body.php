<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	 <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingOne">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
	          1. How can I add Service Award?
	        </a>
	      </h4>
	    </div>
	    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
	      <div class="panel-body">
	        <h2>How to Add Balik Scientist Service Awards</h2>
			This section allows user/s to manage general information, target deliverables, accomplishments, and service awards information of balik scientist awardee. To proceed to this section, click <strong>Add Service Award</strong> button under Service Awards submenu.
			<h3>How to Add General Information</h3>
			<ol>
				<li>Fill-up the entry boxes that constitute the parameters for adding new general information for service award.</li>
				<li>Type in the <strong>Date of Approval</strong> (in the following format: YYYY-MM-DD) on the entry box provided.</li>
				<li>Specify the <strong>Country of Origin</strong>, <strong>Citizenship</strong>, <strong>Type of Award</strong>, <strong>Monitoring Council</strong>, <strong>Status</strong>, and <strong>Type of Contract</strong> from the drop-down lists.</li>
				<li>To add Country, click <strong>Add Country </strong>button (See Figure 0-0). Fill-up the entry boxes that constitute the parameters for adding country.</li>
			</ol>
				<ol style="list-style-type: lower-alpha; padding-left:60px;">
					<li>Specify the <strong>Continent</strong> and <strong>Country</strong> from the drop-down lists.</li>
					<li>When done, click <strong>Save changes</strong> button to add the country. Click <strong>Close</strong> button &nbsp;to close the window.</li>
				</ol>
			<ol start="5">
				<li>To add Citizenship, click <strong>Add Citizenship</strong> button (See Figure 0-0). Fill-up the entry boxes that constitute the parameters for adding citizenship.</li>
			</ol>
				<ol style="list-style-type: lower-alpha; padding-left:60px;">
					<li>Specify the <strong>Type </strong>of citizenship from the drop-down list.</li>
					<li>Type in the <strong>Nationality(ties)</strong> on the entry box provided.</li>
					<li>When done, click <strong>Save changes</strong> button to add the country. Click <strong>Close</strong> button &nbsp;to close the window.</li>
				</ol>
			<ol start="6">
				<li>Type in the <strong>Phases</strong> (Date From and Date To (in the following format: <em>YYYY-MM-DD</em>)), and <strong>Approved No. of Days/Years</strong> on the entry boxes provided.</li>
				<li>Specify the <strong>Host Institution</strong>,<strong> Dost Outcomes</strong>, and <strong>Dost Priority Areas </strong>from the drop-down lists. Click <strong>Add Host Institution </strong>button for additional host institution.</li>
				<li>When done, click <strong>Save</strong> button to save entries. Click <strong>Reset </strong>button &nbsp;to reset parameters.</li>
			</ol>

	      </div>
	    </div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingTwo">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
	          2. How can I add Accomplishments?

	        </a>
	      </h4>
	    </div>
	    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
	      <div class="panel-body">
	        <h2>How to Add Accomplishments</h2>
				<ol>
					<li>To add accomplishments, click the column corresponding to the type of accomplishments to add, and then fill up the entry boxes that constitute the parameters for adding new accomplishment (See Figure 0-0).</li>
				</ol>
				<p>&nbsp;</p>
				
			<h3>How to Add Seminars/Lectures/Forums Attended</h3>
				<ol>
					<li>Click the <strong>Add Seminar/Lecture/Forum</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new seminars/lectures/forums attended (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>from the drop-down list.</li>
					<li>Type in the <strong>Title</strong>, <strong>Date</strong>, and <strong>Venue</strong> of seminars/lectures/forums attended on the entry boxes provided.</li>
					<li>Specify the <strong>Type of Participants</strong> from the drop-down list.</li>
					<li>Type in the <strong> of Participants </strong>(if applicable) and <strong>Output </strong>on the entry boxes provided.</li>
					<li>When done, click <strong>Save </strong>button to save entries. Click <strong>Cancel </strong>button &nbsp;to do otherwise.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new seminars/lectures/forums attended details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>

			<h3>How to Add Trainings/Workshops/Demonstrations Attended</h3>
				<ol>
					<li>Click the <strong>Add Training/Workshop/Demonstration</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new trainings/workshops/demonstrations attended (See Figure 0-0).</li>
					<li>Refer to section 4.1.1.3.4.1.1 <em>How to Add Seminars/Lectures/Forums Attended</em> of this manual.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new trainings/workshops/demonstrations attended details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Projects Assisted</h3>
				<ol>
					<li>Click the <strong>Add Project</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new projects assisted (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>from the drop-down list.</li>
					<li>Type in the <strong>Title</strong>, <strong>Objectives</strong>, and <strong>Implementing Agency</strong> of projects assisted on the entry boxes provided.</li>
					<li>Specify the <strong>Project Status</strong> from the drop-down list.</li>
					<li>Type in the <strong>Contribution to the Project </strong>on the entry box provided.</li>
					<li>When done, click <strong>Save </strong>button to save entries. Click <strong>Cancel </strong>button &nbsp;to do otherwise.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new projects assisted details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
			
			<h3>How to Add Papers Submitted for Publication</h3>
				<ol>
					<li>Click the <strong>Add Paper</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new papers submitted for publication (See Figure -0).</li>
					<li>Specify the <strong>Field</strong> from the drop-down list.</li>
					<li>Type in the <strong>Name of journal/magazine/newsletter/ newspaper/book</strong>, <strong>ISBN No.</strong>/<strong>volume</strong>/<strong>issue</strong>/<strong>edition</strong>, <strong>Title of paper</strong>/<strong>article</strong>/<strong>chapter</strong>/<strong>book</strong>, <strong>Date Submitted</strong> <strong>for Publication</strong>, If published, <strong>Date of Publication</strong>, <strong> of Pages</strong>, and <strong>Authors</strong> of papers submitted for publication on the entry boxes provided.</li>
					<li>When done, click <strong>Save </strong>button to save entries. Click <strong>Cancel </strong>button &nbsp;to do otherwise.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new papers submitted for publication details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Students Mentored</h3>
				<ol>
					<li>Click the <strong>Add Students Mentoring</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new students mentored (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>and <strong>Host Institution </strong>from the drop-down lists.</li>
					<li>Type in the <strong> of Students</strong>, <strong>Course</strong>, and <strong>Output</strong> on the entry boxes provided.</li>
					<li>When done, click <strong>Save </strong>button to save entries. Click <strong>Cancel </strong>button &nbsp;to do otherwise.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new students&rsquo; mentored details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Curriculum/Course Developed</h3>
				<ol>
					<li>Click the <strong>Add Curriculum/Course Development</strong> button shown in Figure 0-0. Fill-up the entry boxes that constitute the parameters for adding new curriculum/course developed (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>and <strong>Host Institution </strong>from the drop-down lists.</li>
					<li>Type in the <strong>Output</strong> on the entry box provided.</li>
					<li>When done, click <strong>Save </strong>button to save entries. Click <strong>Cancel </strong>button &nbsp;to do otherwise.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new curriculum/course developed details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Network and Linkages</h3>
				<ol>
					<li>Click the <strong>Add Network and Linkages</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new network and linkages (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>from the drop-down list.</li>
					<li>Type in the <strong>Output </strong>on the entry box provided.</li>
					<li>When done, click <strong>Save </strong>button to save entries. Click <strong>Cancel </strong>button &nbsp;to do otherwise.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new network and linkages details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Research and Development</h3>
				<ol>
					<li>Click the <strong>Add Research and Development</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new research and development (See Figure 0-0).</li>
					<li>Refer to section 4.1.1.3.4.1.7 <em>How to Add Network and Linkages</em> of this manual.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new research and development details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Others</h3>
				<ol>
					<li>Fill up the entry boxes that constitute the parameters for adding other accomplished activities (See Figure 0-0).</li>
					<li>Refer to section 4.1.1.3.4.1.7 <em>How to Add Network and Linkages</em> of this manual.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new accomplished activity details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
	      </div>
	    </div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingThree">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
	          3. How can I add Target Delivarables?
	        </a>
	      </h4>
	    </div>
	    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
	      <div class="panel-body">
	        <h2>How to Add Target Deliverables</h2>
				<ol>
					<li>To add target deliverables, click the column corresponding to the type of target deliverables to add, and then fill-up the entry boxes that constitute the parameters for adding new target deliverables (See Figure 0-0). /*---image of table with column---*/</li>
				</ol>
				<p>&nbsp;</p>
				
			<h3>How to Add Seminars/Lectures/Forums</h3>
				<ol>
					<li>Click the <strong>Add Seminar/Lecture/Forum</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new seminar/lecture/forum (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>from the drop-down list.</li>
					<li>Type in the <strong>Title</strong> of seminar/lecture/forum to be attended on the entry box provided.</li>
					<li>Specify the <strong>Type of Participants</strong> from the drop-down list.</li>
					<li>Type in the <strong> of Target Participants </strong>(if applicable) and <strong>Expected Output </strong>on the entry boxes provided.</li>
					<li>When done, click <strong>Save </strong>button to add the new seminar/lecture/forum. Click <strong>Cancel </strong>button &nbsp;to cancel the operation.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new seminar/lecture/forum details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Trainings/Workshops/Demonstrations</h3>
				<ol>
					<li>Click the <strong>Add Training/Workshop/Demonstration</strong> button shown in Figure 0-0, and then refer to section 4.1.1.3.3.1.1 <em>How to Add Seminars/Lectures/Forums</em> of this manual (See Figure 0-0).</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new training/workshop/ demonstration details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Projects to be Assisted</h3>
				<ol>
					<li>Click the <strong>Add Project</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new projects to be assisted (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>from the drop-down list.</li>
					<li>Type in the <strong>Title</strong>, <strong>Objectives</strong>, and <strong>Implementing Agency</strong> of projects to be assisted on the entry boxes provided.</li>
					<li>Specify the <strong>Project Status</strong> from the drop-down list.</li>
					<li>Type in the <strong>Expected Contribution </strong>on the entry box provided.</li>
					<li>When done, click <strong>Save </strong>button to add the new project to be assisted. Click <strong>Cancel </strong>button &nbsp;to cancel the operation.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new projects to be assisted details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Papers to be Submitted</h3>
				<ol>
					<li>Click the <strong>Add Paper</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new paper to be submitted (See Figure 0-0).</li>
					<li>Type in the <strong>Title</strong> of paper/article/chapter/book and <strong>Name of journal/magazine/newsletter/newspaper/book</strong> to be submitted on the entry boxes provided.</li>
					<li>When done, click <strong>Save </strong>button to add the new paper to be submitted. Click <strong>Cancel </strong>button &nbsp;to cancel the operation.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new paper to be submitted details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Students Mentoring</h3>
				<ol>
					<li>Click the <strong>Add Students Mentoring</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new students to be mentored (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>and <strong>Host Institution </strong>from the drop-down lists.</li>
					<li>Type in the <strong>Course</strong>, <strong> of Participants</strong> (if applicable), and <strong>Expected Output</strong> on the entry boxes provided.</li>
					<li>When done, click <strong>Save </strong>button to add the new students to be mentored. Click <strong>Cancel </strong>button &nbsp;to cancel the operation.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new students to be mentored details will be displayed on the list below the form (See Figure -0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Curriculum/Course Development</h3>
				<ol>
					<li>Click the <strong>Add Curriculum/Course Development</strong> button shown in Figure 0-0. Fill-up the entry boxes that constitute the parameters for adding new curriculum/course development (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>and <strong>Host Institution </strong>from the drop-down lists.</li>
					<li>Type in the <strong>Expected Output</strong> on the entry box provided.</li>
					<li>When done, click <strong>Save </strong>button to add the new curriculum/course development. Click <strong>Cancel </strong>button &nbsp;to cancel the operation.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new curriculum/course development details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Network and Linkages</h3>
				<ol>
					<li>Click the <strong>Add Network and Linkages</strong> button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new network and linkages (See Figure 0-0).</li>
					<li>Specify the <strong>Field </strong>from the drop-down list.</li>
					<li>Type in the <strong>Expected Output </strong>on the entry box provided.</li>
					<li>When done, click <strong>Save </strong>button to add new network and linkages. Click <strong>Cancel </strong>button &nbsp;to cancel the operation.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new network and linkages details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Research and Development</h3>
				<ol>
					<li>Click the Add Research and Development button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new research and development (See Figure 0-0).</li>
					<li>Refer to section 4.1.1.3.3.1.7 <em>How to Add Network and Linkages</em> of this manual.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new research and development details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
				<p>&nbsp;</p>
				
			<h3>How to Add Others</h3>
				<ol>
					<li>Click the Add Others button shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding other target activities (See Figure 0-0).</li>
					<li>Refer to section 4.1.1.3.3.1.7 <em>How to Add Network and Linkages</em> of this manual.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new activity details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</p>
	      </div>
	    </div>
	  </div>
	   <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingFour">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
	          4. Can I add user accounts? If yes, what are the steps to follow?
	        </a>
	      </h4>
	    </div>
	    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
	      <div class="panel-body">
	        <h2>How to Add User Accounts</h2>
				<ol>
					<li>Fill up the entry boxes that constitute the parameters for adding new user account shown in Figure 0-0.</li>
					<li>Type in the <strong>Employee Name</strong>, <strong>Username</strong>, and<strong> Password </strong>on the entry boxes provided.</li>
					<li>Specify the <strong>Access Level </strong>from the drop-down list.</li>
					<li>When done, click <strong>Save </strong>button to save entries. Click <strong>Cancel </strong>button &nbsp;to do otherwise.</li>
				</ol>
				<p><strong>Note</strong>: Once added, the new user account details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</p>
	      </div>
	    </div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingFive">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
	          5. How can I know the reports already submitted?
	        </a>
	      </h4>
	    </div>
	    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
	      <div class="panel-body">
	        <h2>How to View Reports Submitted</h2>
				<ol>
					<li>To view the reports submitted,&nbsp;click the <strong>Reports Submitted</strong> sub-section under Service Awards tab.</li>
					<li>If <strong>N</strong> is selected, then the corresponding report was not entirely submitted. If <strong>Y</strong> is selected, then the corresponding report was already submitted. If <strong>NA yet</strong>, then the corresponding report was still inadequate or lacking some contents or section/s.</li>
				</ol>
	      </div>
	    </div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingSix">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
	          6. How can I add more than one contact number?
	        </a>
	      </h4>
	    </div>
	    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
	      <div class="panel-body">
	      	<h2>How to Add Alternative Contact Number</h2>
		        <ol>
					<li>To add more than one or alternative contact number, click <strong>Add Contact </strong>button (See Figure 0-0). Fill-up the entry box that constitutes the parameter for adding contact number.</li>
				</ol>
					<ol style="list-style-type: lower-alpha; padding-left: 60px;">
						<li>Type in the <strong>Contact Number</strong> on the entry box provided.</li>
						<li>When done, click <strong>Save changes</strong> button to add the contact number. Click <strong>Close</strong> button to close the window.</li>
					</ol>
	      </div>
	    </div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingSeven">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
	          7. How can I add more than one email address?
	        </a>
	      </h4>
	    </div>
	    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
	      <div class="panel-body">
	        <h2>How to Add Alternative Email Address</h2>
		        <ol>
					<li>To add more than one or alternative email address, click <strong>Add Email </strong>button (See Figure 0-0). Fill-up the entry box that constitutes the parameter for adding email address.</li>
				</ol>
					<ol style="list-style-type: lower-alpha; padding-left: 60px;">
						<li>Type in the <strong>Email Address</strong> on the entry box provided.</li>
						<li>When done, click <strong>Save changes</strong> button to add the email address. Click <strong>Close</strong> button to close the window.</li>
					</ol>
	      </div>
	    </div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading" role="tab" id="headingEight">
	      <h4 class="panel-title">
	        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
	          8. How do I generate system reports?
	        </a>
	      </h4>
	    </div>
	    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
	      <div class="panel-body">
	        <h2>How to Generate and Print Reports</h2>
				<ol>
					<li>To generate reports, fill-up the entry boxes it may require that constitute the parameters for generating reports.</li>
				</ol>
				<ol style="list-style-type: lower-alpha; padding-left: 60px;">
					<li>Specify the <strong>Report Type</strong> from the drop-down list.</li>
					<li>Depending on the report chosen, supply the parameter(s) it may require using the entry box(es) provided. It may be the <strong>scientist</strong>, <strong>period</strong> or <strong>year</strong>.</li>
					<li>When done, click <strong>Print </strong>button to generate report.</li>
				</ol>
				<ol start="2">
					<li>To print reports, click the <strong>Print</strong> icon located at the upper-right side of the application and click it.</li>
				</ol>
				<ol style="list-style-type: lower-alpha; padding-left: 60px;">
					<li>Specify printing options that will be prompted. Printing options vary depending on the printer.</li>
					<li>Click <strong>Ok</strong> to proceed.</li>
				</ol>
				
	      </div>
	    </div>
	  </div>
	</div>