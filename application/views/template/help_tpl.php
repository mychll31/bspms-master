<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">
		Balik Scientist Program Management System Administrator Module
	</h4>
</div>
<div class="modal-body" id="help">
	<!-- Sidebar -->
	<div class="col-xs-6 col-md-4">
		<?php include 'help_sidebar_adminModule.php';?>
	</div>
	<!-- sidebar -->

	<div class="col-xs-12 col-md-8">
		<?php include 'help_body_adminModule.php';?>
	</div>
</div>
<div class="modal-footer" style="border: none"></div>


<style type="text/css">

.wrapper, .row {
   height: 100%;
   margin-left:0;
   margin-right:0;
}

.wrapper:before, .wrapper:after,
.column:before, .column:after {
    content: "";
    display: table;
}

.wrapper:after,
.column:after {
    clear: both;
}

#sidebar {
    background-color: #eee;
    padding-left: 0;
    float: left;
    min-height: 100%;
}

#sidebar .collapse.in {
    display: inline;
}

#sidebar > .nav>li>a {
    white-space: nowrap;
    overflow: hidden;
}

#main {
    padding: 15px;
    left: 0;
}

a:active, a:focus {
	font-weight: bold;
}

/*
 * off canvas sidebar
 * --------------------------------------------------
 */
@media screen and (max-width: 768px) {
    #sidebar {
        min-width: 44px;
    }
    
    #main {
        width: 1%;
        left: 0;
    }
    
    #sidebar .visible-xs {
       display:inline !important;
    }
    
    .row-offcanvas {
       position: relative;
       -webkit-transition: all 0.4s ease-in-out;
       -moz-transition: all 0.4s ease-in-out;
       transition: all 0.4s ease-in-out;
    }
    
    .row-offcanvas-left.active {
       left: 45%;
    }
    
    .row-offcanvas-left.active .sidebar-offcanvas {
       left: -45%;
       position: absolute;
       top: 0;
       width: 45%;
    }
} 
 
 
@media screen and (min-width: 768px) {
  .row-offcanvas {
    position: relative;
    -webkit-transition: all 0.25s ease-out;
    -moz-transition: all 0.25s ease-out;
    transition: all 0.25s ease-out;
  }

  .row-offcanvas-left.active {
    left: 3%;
  }

  .row-offcanvas-left.active .sidebar-offcanvas {
    left: -3%;
    position: absolute;
    top: 0;
    width: 3%;
    text-align: center;
    min-width:42px;
  }
  
  #main {
    left: 0;
  }
}
</style>

<script type="text/javascript">
	/* off-canvas sidebar toggle */
$('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
    $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
});
</script>