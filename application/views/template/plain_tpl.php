<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo SYSTEM_NAME;?></title>
    <link href="<?=base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=base_url('assets/production/css/AdminLTE.min.css')?>" rel="stylesheet">
    <script src="<?=base_url('assets/vendors/jquery/dist/jquery.min.js')?>"></script>
  </head>

  <body>
    <style type="text/css">
      div {
        width: 50%;
        position: absolute;
        top:0;
        bottom: 0;
        left: 0;
        right: 0;

        margin: auto;
    }
    h1 {
      font-weight: bold;
    }
    table thead, table tbody, table tfoot {
    border: 1px solid #f1f1f1;
    background-color: #fefefe;
}
    </style>
    
    <div class="wrapper"><?=$contents;?></div>
  </body>
</html>