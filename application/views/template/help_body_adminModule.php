<style type="text/css">
	.bhelp{
		padding: 10px;
		border: 1px solid #ccc;
		vertical-align: top;
	}
	.bhelp:hover{
		background-color: #fff !important;
	}
	ol {
    	line-height: 25px;
	}
</style>

<div v-show="menu.login">
	<h2><b>1 Login</b></h2>
	<br>
	<p>To login to the system, the user is required to enter a valid username and password on the Login Form at the Main Page (See Figure 2).</p>
	<div align="center"><img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_bb9e3b0f.png')?>" style="width: 100%;"/>
	<br><small><i><b>Figure 1 DOST-CO BSPMS Main Page</i></b></small>
	</div>
</div>

<div v-show="menu.login21">
	<h2><b>1.1	How to Login</b></h2>
	<br>
	<ol>
		<li>Enter <b>Username</b> and <b>Password</b> on the entry boxes labeled respectively. Then click <b>Login</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_39482b0.png')?>" width="31" height="19" border="0"> (See Figure 1-1).</li>
		<li>If login is successful, user will be directed to the Default Screen.</li>
	</ol>
	<div align="center">
		<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_dcfe07e8.png')?>" width="240" height="95" />
		<br><small><i><b>Figure 1-1 Login Form</i></b></small>
	</div>
</div>

<div v-show="menu.defaultscr">
	<h2><b>2 Default Screen</b></h2>
	<br>
	<p>Once logged in to the system user will be automatically redirected to the Home Page (See Figure 2).</p>
	<div align="center">
		<img src="<?= base_url('assets/images/help/Figure 3 Default Page final.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 2 Default Page</i></b></small>
	</div>
	<br>
	<p>These are the components of the Default Screen and a brief description of what they are:</p>
	<table class="tblhelp">
		<tr class="bhelp">
			<td class="bhelp">User</td>
			<td class="bhelp">Displays the username of the logged-in user</td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp">Balik Scientist Profile Section</td>
			<td class="bhelp">Displays the list of Balik Scientist Awardees</td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp">Reports Section</td>
			<td class="bhelp">Provides link to Report sub-section</td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp">Libraries Section</td>
			<td class="bhelp">Provides links to Area of Expertise, Citizenship, Courses, DOST Outcomes, DOST Priority Areas, Educational Levels. Fields, Global Regions/Countries, Host Institutions, Local Regions, Monitoring Councils, Participants, Professions, Specializations, Titles, and User Accounts sub-sections</td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp">Quick Links Section</td>
			<td class="bhelp">Displays links to other DOST External Sites such as DOST Website, DOST BSP Website, DOST Email, DOST PCAARRD Portal, DOST PCHRD, and DOST PCIEERD</td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp">Help Section</td>
			<td class="bhelp">Provides links to Online Help and FAQs sub-sections</td>
		</tr>
	</table>
</div>

<div v-show="menu.adminmod">
	<h2><b>3 Administrator Module</b></h2>
	<br>
	<p>The Administrator is responsible for maintaining the DOST-CO BSPMS and for ensuring that the system is up and running all the time.</p>
	<br>
	<p>The Administrator Module allows user to:</p>
	<table class="tblhelp">
		<tr class="bhelp">
			<td class="bhelp">Balik Scientist Profile Section</td>
			<td class="bhelp">
				<li>Manage Balik Scientist Awardees profile information</li>
				<li>Manage Balik Scientist Awardees service awards</li>
			</td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp" nowrap>Reports Section</td>
			<td class="bhelp"><li>Print and Export DOST-CO BSPMS related reports</li></td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp" nowrap>Libraries Section</td>
			<td class="bhelp"><li>Manage Area of Expertise, Citizenship, Courses, DOST Outcomes, DOST Priority Areas, Educational Levels, Fields, Global Regions/Countries, Host Institutions, Local Regions, Monitoring Councils, Participants, Professions, Specializations, Titles, and User Accounts sub-sections</li></td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp" nowrap>Quick Links Section</td>
			<td class="bhelp"><li>Connect to DOST External Links</li></td>
		</tr>
		<tr class="bhelp">
			<td class="bhelp" nowrap>Help Section</td>
			<td class="bhelp"><li>View Online Help and FAQs</li></td>
		</tr>
	</table>
</div>

<div v-show="menu.adminmod41">
	<h2><b>3.1 Balik Scientist Profile</b></h2>	
	<br>
	<p>The Balik Scientist Profile is the default page for the Administrator, BSP Staff, and Council. To access the Balik Scientist page, click the <b>Balik Scientist Profile</b> tab on the left-side of the page (See Figure 0-0).</p>
	<div align="center">
		<img src="<?= base_url('assets/images/help/Figure 4-1 Balik Scientist Profile final.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 3-1 Balik Scientist Profile page</i></b></small>
	</div>
	<br>
	<p>These are the components of the Balik Scientist Profile page and a brief description of what they are:</p>
	<ul>
		<li><b>List of Balik Scientist Awardees</b> - displays the list of balik scientist awardees.</li>
		<li><b>Scientist’s Profile Information</b> - displays the personal, education, and employment information  of balik scientist awardees.</li>
		<li><b>Scientist’s Profile Service Awards</b> - displays the list of service awards, general information, target deliverables, accomplishments, reports submitted, and assigned secretariat by balik scientist awardees.</li>
	</ul>
</div>

<!-- <div v-show="menu.adminmod411">
	<h2><b>3.1.1 How to Create Balik Scientist Awardee Profile</b></h2>	
	<br>
	<p>
		<ol>
			<li>To create balik scientist awardee profile, click the <b>Add Scientist</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_be55aeaa.png')?>" width="63" height="19" border="0"> shown in Figure 4-1. Fill-up the entry boxes that constitute the parameters for adding balik scientist information.</li>
		</ol>
	</p>
</div> -->

<div v-show="menu.adminmod411">
	<h2><b>3.1.1 How to Add Profile Information</b></h2>	
	<br>
	<p>This section allows user/s to add personal, education, and employment information of balik scientist awardee. This page appears only when user/s add a new balik scientist awardee. To create balik scientist awardee profile, click the <b>Add Scientist</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_be55aeaa.png')?>" width="63" height="19" border="0"> shown in Figure 4-1. Fill-up the entry boxes that constitute the parameters for adding balik scientist information.</p>
</div>

<div v-show="menu.adminmod4111">
	<h2><b>3.1.1.1 How to Add Personal Information</b></h2>	
	<br>
	<div align="center">
		<img src="<?= base_url('assets/images/help/Add Scientist Personal Information.jpg')?>" style="width: 50%;" />
		<br><small><i><b>Figure 0-0 Add Personal Information form</i></b></small>
	</div>
	<br>
	<p>
		<ol>
			<li>Specify the <b>Title</b> from the drop-down list.</li>
			<li>Type in the <b>First Name</b> and <b>Last Name</b> on the entry boxes provided. <b>Middle Name</b>, <b>Middle Initial</b>, and <b>Extension Name</b> are optional.</li>
			<li>Fill-in the <b>Date of Birth</b> by typing the date on the specified textbox (in the following format: (<em>YYYY-MM-DD</em>).</li>
			<li>Specify the <b>Gender</b>, <b>Civil Status</b>, <b>Area of Expertise</b>, <b>Specialization</b>, and <b>Profession</b> from the drop-down lists.</li>
			<li>Type in the <b>Professional License</b> on the entry box provided.</li>
			<li>To add Contact Number, click <b>Add Contact</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_2c88423b.png')?>" width="60" height="19" border="0"> (See Figure 0-0). Fill-up the entry box that constitutes the parameter for adding contact number. </li>
			<br>
			<div align="center">
				<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_65e7ed05.png')?>" width="432" height="144" border="0">
				<br><small><i><b>Figure 0-0 Add Contact No. form</i></b></small>
			</div>
			<br>
				<ol type="a">
					<li>Type in the <b>Contact Number</b> on the entry box provided.</li>
					<li>When done, click <b>Save changes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_f86f6d16.png')?>" width="58" height="19" border="0"> to add the contact number. Click <b>Close</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_408b48a3.png')?>" width="34" height="21" border="0"> to close the window.</li>
				</ol>
				<br>
			<li>To add Email Address, click <b>Add Email</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ef496276.png')?>" width="54" height="19" border="0"> (See Figure 0-0). Fill-up the entry box provided that constitutes the parameter for adding email address. </li>
			<br>
			<div align="center">
				<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_e503b5bc.png')?>" width="432" height="144" border="0">
				<br><small><i><b>Figure 0-0 Add Email Address form</i></b></small>
			</div>
			<br>
				<ol type="a">
					<li>Type in the <b>Email Address</b> on the entry box provided.</li>
					<li>When done, click <b>Save changes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_f86f6d16.png')?>" width="58" height="19" border="0"> to add the new email address. Click <b>Close</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_408b48a3.png')?>" width="34" height="21" border="0"> to close the window.</li>
				</ol>
				<br>
			<li>Type in the <b>Postal Address</b> on the entry box provided.</li>
			<li>To proceed to the Scientist’s Education section, click <b>Next</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_6e116d5a.png')?>" width="33" height="19" border="0"> . Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_6868ba5c.png')?>" width="35" height="19" border="0"> to return to Home page.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4112">
	<h2><b>3.1.1.2 How to Add Education Information</b></h2>	
	<br>
	<div align="center">
		<img src="<?= base_url('assets/images/help/Add Scientist Education Information 3.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Add Education Information form</i></b></small>
		<br><br>
		<div class="well well-sm" style="width:85%;"><i><b>Note</b>: After clicking the <b>Next</b> <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_6e116d5a.png')?>" width="33" height="19" border="0"> button from Balik Scientist's Profile page, user will proceed to the Balik Scientist’s Education section.</i></div>
	</div>
	<p>
		<ol>
			<li>Specify the <b>Level</b> from the drop-down list.</li>
			<li>Type in the <b>Institute/University</b> on the entry box provided.</li>
			<li>Specify the <b>Country</b> and <b>Course</b> from the drop-down lists.</li>
			<li>Type in the <b>Year Graduated</b> (if applicable) on the entry box provided.</li>
			<li>When done, click <b>Save</b> button <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_4303750c.png')?>" width="29" height="19" border="0"> to add education information. Click <b>Reset</b> button <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_5224706.png')?>" width="32" height="19" border="0"> to reset parameters.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new entries will be displayed on the table below the form (See Figure 0-0). To add new Balik Scientist’s Education, click <b>Add Education</b> <img src="<?=base_url('assets/images/help/Add Education button.jpg')?>" width="32" height="19" border="0"> button then fill-up the entry boxes once again that constitute the parameters for adding new education.</i></div>

				<div align="center">
					<img src="<?= base_url('assets/images/help/Education page.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Education page</i></b></small>
				</div>
				
			</li>
			<li>To proceed to the Balik Scientist’s Employment section, click <b>Next</b> button <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_6e116d5a.png')?>" width="33" height="19" border="0">.</li>
			<br>
			<div class="well well-sm" style="width:85%;"><i><b>Note</b>: If no records found on file for this section, tick the <b>No record on file</b> checkbox <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_51e5cab.png')?>" width="81" height="19" border="0">.</i></div>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113">
	<h2><b>3.1.1.3 How to Add Employment Information</b></h2>	
	<br>
	<div align="center">
		<img src="<?= base_url('assets/images/help/Add Scientist Employment Information.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Add Employment Information form</i></b></small>
		<br><br>
		<div class="well well-sm" style="width:85%;"><i><b>Note</b>: After clicking the <b>Next</b> <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_6e116d5a.png')?>" width="33" height="19" border="0"> button from Balik Scientist's Education Information, user will proceed to the Balik Scientist’s Employment section.</i></div>
	</div>
	<p>
		<ol>
			<li>To add employment information, fill up the entry boxes that constitute the parameters for adding new employment information (See Figure 0-0).</li>
			<li>
				Type in the <b>Company</b>, <b>Position</b>, <b>Inclusive Dates</b> (Date From and Date To (in the following format: <i>YYYY-MM-DD</i>)) on the entry boxes provided.
			</li>
			<li>Specify the Country from the drop-down list.</li>
			<li>When done, click <b>Save</b> button <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_4303750c.png')?>" width="29" height="19" border="0"> to add employment information. Click <b>Reset</b> button <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_5224706.png')?>" width="32" height="19" border="0"> to reset parameters.</li>
			<br>
			<div class="well well-sm" style="width:85%;"><i><b>Note</b>: If no records found on file for this section, tick the <b>No record on file</b> checkbox <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_51e5cab.png')?>" width="81" height="19">.</i></div>
			<li>When done, click the <b>Finish</b> button <img src="<?=base_url('assets/images/help/i_1e1230927033688d_html_3351f924.png')?>" width="43" height="19" border="0"> to add profile information.</li>
			<br>
			<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new entries will be displayed on the Home page. A confirmation message will be displayed if the action is successful.</i></div>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod412">
	<h2><b>3.1.2 How to View & Update Profile Information</b></h2>	
	<br>
	<p>
		This section allows user/s to view and update personal, education, and employment information of balik scientist awardee. This page appears only when user/s add a new balik scientist awardee. 
	</p>
	<div align="center">
		<img src="<?= base_url('assets/images/help/view and update profile information.jpg')?>" style="width: 70%;" />
		<br><small><i><b>Figure 0-0 View & Update Profile Information page</i></b></small>
	</div>
</div>

<div v-show="menu.adminmod4121">
	<h2><b>3.1.2.1 How to View & Update Personal Information</b></h2>	
	<br>
	<p>
		<ol>
			<li>
				From Balik Scientist Profile page, click the row corresponding to the details of the balik scientist awardee found on the list. A new window will open showing Scientist’s Profile Information and Service Awards page (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_43e559df.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Personal Information tab</i></b></small>
				</div>
				<br>
			</li>
			<li>To view and update personal information, click the <b>Personal</b> tab.</li>
			<li>
				Information that can be updated includes <b>Title</b>, <b>First Name</b>, <b>Middle Name</b>, <b>Middle Initial</b>, <b>Last Name</b>, <b>Extension Name</b>, <b>Date of Birth</b>, <b>Gender</b>, <b>Status</b>, <b>Area of Expertise</b>, <b>Specialization</b>, <b>Profession</b>, <b>Professional License</b>, <b>Contact No.</b>, <b>Email Address</b>, and <b>Postal Address</b> (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_275d5575.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 View & Update Personal Information form</i></b></small>
				</div>
				<br>
			</li>
			<li>Click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8b3172c6.png')?>" width="39" height="19" /> to save entry.</li>
			<li>Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_6868ba5c.png')?>" width="35" height="19" /> to cancel operation.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4122">
	<h2><b>3.1.2.2 How to View & Update Education Information</b></h2>	
	<br>
	<p>
		<ol>
			<li>From Education tab under the Information submenu, click the row corresponding to the details of balik scientist education found on the list. A new window will open showing the Scientist’s Education Information (See Figure 0-0).</li>
			<!-- <li>To view and update education information, click the <b>Education</b> tab.</li> -->
			<li>
				Information that can be updated includes <b>Level</b>, <b>Institute/University</b>, <b>Country</b>, <b>Course</b>, and <b>Year Graduated</b>.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_c5a556b7.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 View & Update Education Information form</i></b></small>
				</div>
				<br>
			</li>
			<li>Click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8b3172c6.png')?>" width="39" height="19" /> to save entry.</li>
			<li>Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_6868ba5c.png')?>" width="39" height="19" /> to cancel the operation.</li>
			<li>Click <b>Delete</b> button <img src="<?= base_url('assets/images/help/Delete button.jpg')?>" width="39" height="19" /> to delete data. Click <b>Yes</b> button <img src="<?= base_url('assets/images/help/Yes button.jpg')?>" width="34" height="22" /> to confirm deletion or <b>No</b> button <img src="<?= base_url('assets/images/help/No button.jpg')?>" width="34" height="22" /> to do otherwise.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4123">
	<h2><b>3.1.2.3 How to View & Update Employment Information</b></h2>	
	<br>
	<p>
		<ol>
			<li>From Employment tab under the Information submenu, click the row corresponding to the details of balik scientist employment found on the list. A new window will open showing the Scientist’s Profile Information and Service Awards page (See Figure 0-0).</li>
			<!-- <li>To view and update employment information, click the <b>Employment</b> tab.</li> -->
			<li>
				Information that can be updated includes <b>Company</b>, <b>Position</b>, <b>Inclusive Dates</b> (Date From and Date To (in the following format: <i>YYYY-MM-DD</i>)), and <b>Country</b>.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_45091812.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 View & Update Employment Information form</i></b></small>
				</div>
				<br>
			</li>
			<li>Click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8b3172c6.png')?>" width="39" height="19" /> to save entry.</li>
			<li>Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_6868ba5c.png')?>" width="39" height="19" /> to cancel operation.</li>
			<li>Click <b>Delete</b> button <img src="<?= base_url('assets/images/help/Delete button.jpg')?>" width="39" height="19" /> to delete data. Click <b>Yes</b> button <img src="<?= base_url('assets/images/help/Yes button.jpg')?>" width="34" height="22" /> to confirm deletion or <b>No</b> button <img src="<?= base_url('assets/images/help/No button.jpg')?>" width="34" height="22" /> to do otherwise.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod413">
	<h2><b>3.1.3 How to Add Balik Scientist Service Awards</b></h2>	
	<br>
	<p>This section allows user/s to manage general information, target deliverables, accomplishments, reports submitted and assigned secretariat of balik scientist awardee. To proceed to this section, click <b>Add Service Award</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_fd9f2ce5.png')?>" width="70" height="19" /> under Service Awards submenu. Fill-up the entry boxes that constitute the parameters for adding balik scientist service award information.</p>
	<div align="center">
		<img src="<?= base_url('assets/images/help/service award tab.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Service Awards page</i></b></small>
	</div>
	<br>
</div>

<div v-show="menu.adminmod4131">
	<h2><b>3.1.3.1 How to Add General Information</b></h2>	
	<br>
	<div align="center">
		<img src="<?= base_url('assets/images/help/add general information service award.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Add General Information form</i></b></small>
	</div>
	<p>
		<ol>
			<li>Fill-up the entry boxes that constitute the parameters for adding new general information for service award.</li>
			<li>Type in the <b>Date of Approval</b> (in the following format: <i>YYYY-MM-DD</i>) or place a tick mark on the <b>Year</b> checkbox to indicate the <b>Year</b> on the entry box provided.</li>
			<li>Specify the <b>Country of Origin</b>, <b>Citizenship</b>, <b>Type of Award</b>, <b>Monitoring Council</b>, <b>Status</b>, and <b>Type of Contract</b> from the drop-down lists.</li>
			<li>
				To add Country, click <b>Add Country</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_15655c9e.png')?>" width="52" height="19" /> shown in Figure 0-0. Fill-up the entry boxes that constitute the parameters for adding country.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_f583778.png')?>" style="width: 50%;" />
					<br><small><i><b>Figure 0-0 Add Country form</i></b></small>
				</div>
				<br>
				<ol type="a">
					<li>Specify the <b>Continent</b> and <b>Country</b> from the drop-down lists.</li>
					<li>When done, click <b>Save changes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_f86f6d16.png')?>" width="58" height="19" /> to add the country. Click <b>Close</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_408b48a3.png')?>" width="34" height="19" /> to close the window.</li>
				</ol>
				<br>
			</li>
			<li>
				To add Citizenship, click <b>Add Citizenship</b> button <img src="<?= base_url('assets/images/help/add citizenship button.jpg')?>" width="52" height="19" /> (See Figure 0-0). Fill-up the entry boxes that constitute the parameters for adding citizenship.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_a37fd1a4.png')?>" style="width: 50%;" />
					<br><small><i><b>Figure 0-0 Add Citizenship form</i></b></small>
				</div>
				<br>
				<ol type="a">
					<li>Specify the <b>Type</b> of citizenship from the drop-down list.</li>
					<li>Type in the <b>Nationality(ties)</b> on the entry box provided.</li>
					<li>When done, click <b>Save changes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_f86f6d16.png')?>" width="58" height="19" /> to add the citizenship. Click <b>Close</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_408b48a3.png')?>" width="34" height="19" /> to close the window.</li>
					<br>
				</ol>
			</li>
			<li>Type in the <b>Phases</b> (Date From and Date To (in the following format: <em>YYYY-MM-DD</em>)), and <b>Approved No. of Days/Years</b> on the entry boxes provided.</li>
			<li>Specify the <b>Host Institution</b>, <b>Dost Outcomes</b>, and <b>Dost Priority Areas</b> from the drop-down lists. </li>
			<li>
				To add Host Institution, click <b>Add Host Institution</b> button <img src="<?= base_url('assets/images/help/Add host institution button.jpg')?>" width="55" height="19" /> shown in Figure 0-0. Fill-up the entry boxes that constitute the parameters for adding host institution.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/Add host institution.jpg')?>" style="width: 50%;" />
					<br><small><i><b>Figure 0-0 Add Host Institution form</i></b></small>
				</div>
				<br>
				<ol type="a">
					<li>Specify the <b>Institution Type</b> and <b>Local Region</b> from the drop-down lists.</li>
					<li>Type in the <b>Code</b> and <b>Description</b> on the entry boxes provided.</li>
					<li>Click <b>Save changes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_f86f6d16.png')?>" width="58" height="19" /> to add the host institution. Click <b>Close</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_408b48a3.png')?>" width="34" height="19" /> to close the window.</li>
				</ol>
				<br>
			</li>
			<li>When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8b3172c6.png')?>" width="29" height="19" /> to add the general information details. Click <b>Reset</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_5224706.png')?>" width="32" height="19" /> to reset parameters.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4132">
	<h2><b>3.1.3.2 How to View & Update General Information</b></h2>	
	<br>
	<p>
		<ol>
			<li>From Service Awards submenu, click the row corresponding to the details of balik scientist service award found on the list. A new window will open showing Scientist’s Service Award General Information</li>
			<li>
				Information that can be updated includes <b>Date of Approval</b>, <b>Request for Change in Schedule</b>, <b>Change Schedule Date of Approval</b>, <b>Country of Origin</b>, <b>Citizenship</b>, <b>Type of Award</b>, <b>Monitoring Council</b>, <b>Status</b>, <b>Type of Contract</b>, <b>Phases</b> (<b>Date From</b> and <b>Date To</b> (in the following format: <em>YYYY-MM-DD</em>)), <b>Approved No. of Years</b>, <b>Host Institution</b>, <b>DOST Outcomes</b>, and <b>DOST Priority Areas</b> (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/View and Update General Information 2.jpg')?>" style="width: 80%;" />
					<br><small><i><b>Figure 0-0 View and Update General Information</i></b></small>
				</div>
				<br>
			</li>
			<li>Click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8b3172c6.png')?>" width="39" height="19" /> to save entry.</li>
			<li>Click <b>Reset</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_5224706.png')?>" width="32" height="19" /> to reset parameters.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4133">
	<h2><b>3.1.3.3 How to Manage Target Deliverables</b></h2>	
	<br>
	<p>
		This tab allows user to manage balik scientist’s target deliverables. To access the Target Deliverables tab, click the <b>Target Deliverables</b> tab under Service Awards submenu (See Figure 0-0).
	</p>
	<div align="center">
		<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_788728bf.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Target Deliverables tab</i></b></small>
	</div>
	<br>
</div>

<div v-show="menu.adminmod41331">
	<h2><b>3.1.3.3.1 How to Add Target Deliverables</b></h2>
	<br>
	<p>
		<ol>
			<li>
				To add target deliverables, click the column corresponding to the type of target deliverables to add, and then fill-up the entry boxes that constitute the parameters for adding new target deliverables (See Figure 0-0). 
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/Add Target Deliverables.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Target Deliverables page</i></b></small>
				</div>
				<br>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod413311">
	<h2><b>3.1.3.3.1.1 How to Add Seminars/Lectures/Forums</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Seminar/Lecture/Forum</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_3d48ea5f.png')?>" width="125" height="22" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new seminar/lecture/forum (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ca649895.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Seminars/Lectures/Forums form</i></b></small>
				</div>
				<br>
			</li>
			<li>Specify the <b>Field</b> from the drop-down list.</li>
			<li>Type in the <b>Title</b> of seminar/lecture/forum to be attended on the entry box provided.</li>
			<li>Specify the <b>Type of Participants</b> from the drop-down list.</li>
			<li>Type in the <b>No. of Target Participants</b> (if applicable) and <b>Expected Output</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new seminar/lecture/forum. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new seminar/lecture/forum details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod413312">
	<h2><b>3.1.3.3.1.2 How to Add Trainings/Workshops/Demonstrations</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Training/Workshop/Demonstration</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_e3cf90b.png')?>" width="160" height="22" /> shown in Figure 0-0, and then refer to section 3.1.3.3.1.1 <em>How to Add Seminars/Lectures/Forums</em> of this manual (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_c315bd35.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Trainings/Workshops/Demonstrations form</i></b></small>
				</div>
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new training/workshop/ demonstration details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod413313">
	<h2><b>3.1.3.3.1.3 How to Add Projects to be Assisted</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Project</b> button <img src="<?= base_url('assets/images/help/Add Project button.jpg')?>" width="60" height="22" />shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new projects to be assisted (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/How to Add Project to be Assisted form.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Project to be Assisted form</i></b></small>
				</div>
				<br>
			</li>
			<li>Specify the <b>Field</b> from the drop-down list.</li>
			<li>Type in the <b>Title</b>, <b>Objectives</b>, and <b>Implementing Agency</b> of projects to be assisted on the entry boxes provided.</li>
			<li>Specify the <b>Project Status</b> from the drop-down list.</li>
			<li>Type in the <b>Expected Contribution</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new project to be assisted. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new projects to be assisted details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod413314">
	<h2><b>3.1.3.3.1.4 How to Add Papers to be Submitted</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Paper</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_7de84014.png')?>" width="55" height="19" />  shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new paper to be submitted (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/How to Add Papers to be Submitted form.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Paper to be Submitted form</i></b></small>
				</div>
				<br>
			</li>
			<li>Type in the <b>Title</b> of paper/article/chapter/book and <b>Name</b> of journal/magazine/newsletter/newspaper/book to be submitted on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new paper to be submitted. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new paper to be submitted details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod413315">
	<h2><b>3.1.3.3.1.5 How to Add Students Mentoring</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Students Mentoring</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_7b8fc34.png')?>" width="103" height="20" />  shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new students to be mentored (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/How to Add Students Mentoring form.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Students Mentoring form</i></b></small>
				</div>
				<br>
			</li>
			<li>Specify the <b>Field</b> and <b>Host Institution</b> from the drop-down lists.</li>
			<li>Type in the <b>Course</b>, <b>No. of Target Participants</b> (if applicable), and <b>Expected Output</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new students to be mentored. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new students to be mentored details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod413316">
	<h2><b>3.1.3.3.1.6 How to Add Curriculum/Course Development</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Curriculum/Course Development</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_d2a10791.png')?>" width="140" height="19" /> shown in Figure 0-0. Fill-up the entry boxes that constitute the parameters for adding new curriculum/course development (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_9442f95e.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Curriculum/Course Development form</i></b></small>
				</div>
				<br>
			</li>
			<li>Specify the <b>Field</b> and <b>Host Institution</b> from the drop-down lists.</li>
			<li>Type in the <b>Expected Output</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new curriculum/course development. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new curriculum/course development details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113317">
	<h2><b>3.1.1.3.3.1.7 How to Add Network and Linkages</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Network and Linkages</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_a105c99d.png')?>" width="110" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new network and linkages (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8a0a1a8d.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Network and Linkages form</i></b></small>
				</div>
				<br>
			</li>
			<li>Specify the <b>Field</b> from the drop-down list.</li>
			<li>Type in the <b>Expected Output</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add new network and linkages. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new network and linkages details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113318">
	<h2><b>3.1.1.3.3.1.8 How to Add Research and Development</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Research and Development</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_dfecbcca.png')?>" width="124" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new research and development (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_93da27f4.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Research and Development form</i></b></small>
				</div>
				<br>
			</li>
			<li>
				Refer to section 3.1.1.3.3.1.7 <i>How to Add Network and Linkages</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new research and development details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113319">
	<h2><b>3.1.1.3.3.1.9 How to Add Others</b></h2>
	<br>
	<p>
		<ol>
			<li>
				Click the <b>Add Others</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_d370993b.png')?>" width="60" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding other target activities (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/How to Add Others form.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Others form</i></b></small>
				</div>
				<br>
			</li>
			<li>
				Refer to section 3.1.1.3.3.1.7 <i>How to Add Network and Linkages</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new activity details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod411332">
	<h2><b>3.1.1.3.3.2 How to View & Update Target Deliverables</b></h2>
	<br>
	<p>
		<ol>
			<li>To view and update target deliverables, click the column corresponding to the type of target deliverables to view and update shown in Figure 0-0.
			<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/View and update target deliverables.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 View and Update Target Deliverables Page</i></b></small>
				</div>
				<br>
			</li>
		</ol>
	</p> 
</div>

<div v-show="menu.adminmod4113321">
	<h2><b>3.1.1.3.3.2.1 How to View & Update Seminars/Lectures/Forums</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of seminars/lectures/forums to view and update shown in Figure 0-0.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Title</b>, <b>Type of Participants</b>, <b>No. of Target Participants</b>, and <b>Expected Output</b> (See Figure 0-0).
			<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/view and update seminar lecture forum.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 View and Update Seminars/Lectures/Forums form</i></b></small>
				</div>
				<br>
			</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new seminar/lecture/forum details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113322">
	<h2><b>3.1.1.3.3.2.2 How to View & Update Trainings/Workshops/ Demonstrations</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of trainings/workshops/demonstrations to view and update.</li>
			<li>
				Refer to section 4.1.1.3.3.2.1 <i>How to View & Update Seminars/Lectures/Forums</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new training/workshop/demonstration details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113323">
	<h2><b>3.1.1.3.3.2.3 How to View & Update Projects to be Assisted</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of projects to be assisted to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Title</b>, <b>Objectives</b>, <b>Implementing Agency</b>, <b>Project Status</b>, and <b>Expected Contribution</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new projects to be assisted details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113324">
	<h2><b>3.1.1.3.3.2.4 How to View & Update Papers to be Submitted</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of papers to be submitted to view and update.</li>
			<li>Information that can be updated includes <b>Title of paper/article/chapter/book</b> and <b>Name of journal/ magazine/newsletter/newspaper/book</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new paper to be submitted details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113325">
	<h2><b>3.1.1.3.3.2.5 How to View & Update Students Mentoring</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of students to be mentored to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Host Institution</b>, <b>Course</b>, <b>No. of Target Participants</b>, and <b>Expected Output</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new students mentoring details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113326">
	<h2><b>3.1.1.3.3.2.6 How to View & Update Curriculum/Course Development</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of curriculum/course development to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Host Institution</b>, and <b>Expected Output</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new curriculum/course development details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113327">
	<h2><b>3.1.1.3.3.2.7 How to View & Update Network and Linkages</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of network and linkages to view and update.</li>
			<li>Information that can be updated includes <b>Field</b> and <b>Expected Output</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new network and linkages details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113328">
	<h2><b>3.1.1.3.3.2.8 How to View & Update Research and Development</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of research and development to view and update.</li>
			<li>
				Refer to section 4.1.1.3.3.2.7 <i>How to View & Update Network and Linkages</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new research and development details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113329">
	<h2><b>3.1.1.3.3.2.9 How to View & Update Others</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of other target activity to view and update.</li>
			<li>Refer to section 4.1.1.3.3.2.7 <i>How to View & Update Network and Linkages</i> of this manual.</li>
			<br>
			<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new activity details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod411333">
	<h2><b>3.1.1.3.3.3 How to Search Target Deliverables</b></h2>
	<br>
	<p>
		<ol>
			<li>
				To search target deliverables, click the column corresponding to the type of target deliverables to search (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_e09165cd.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Search Target Deliverables page</i></b></small>
				</div>
				<br>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113331">
	<h2><b>3.1.1.3.3.3.1 How to Search Seminars/Lectures/Forums</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular seminar/lecture/forum, enter a keyword on the <b>Search</b> textbox shown in Figure 0-0.</li>
			<li>
				Type in any of the following fields: <b>Field</b>, <b>Title</b>, <b>Type of Participants</b>, <b>No. of Target Participants</b>, and <b>Expected Output</b>.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4bc11ea4.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Search results</i></b></small>
				</div>
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113332">
	<h2><b>3.1.1.3.3.3.2 How to Search Trainings/Workshops/ Demonstrations</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular training/workshop/ demonstration, refer to section 3.1.1.3.3.3.1 <i>How to Search Seminars/Lectures/Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113333">
	<h2><b>3.1.1.3.3.3.3 How to Search Projects to be Assisted</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular project to be assisted, enter a keyword on the <b>Search</b> textbox shown in Figure 0-0.</li>
			<li>
				Type in any of the following fields: <b>Field</b>, <b>Title</b>, <b>Objectives</b>, <b>Implementing Agency</b>, <b>Project Status</b> and <b>Expected Output</b>.
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113334">
	<h2><b>3.1.1.3.3.3.4 How to Search Papers to be Submitted for Publication</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular paper to be submitted for publication, enter a keyword on the Search textbox shown in Figure 0-0.</li>
			<li>Type in any of the following fields:  <b>Title of paper/ article/chapter/book</b> and <b> Name of journal/magazine/newsletter/ newspaper/book</b>.
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113335">
	<h2><b>3.1.1.3.3.3.5 How to Search Students Mentoring</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular student mentoring, enter a keyword on the Search textbox shown in Figure 0-0.</li>
			<li>Type in any of the following fields: <b>Field</b>, <b>Host Institution</b>, <b>Course</b>, <b>No. of Target Participants</b>, and <b>Expected Output</b>.
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113336">
	<h2><b>3.1.1.3.3.3.6 How to Search Curriculum/Course Development</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular curriculum/course development, enter a keyword on the Search textbox shown in Figure 0-0.</li>
			<li>Type in any of the following fields: <b>Field</b>, <b>Host Institution</b>, and <b>Expected Output</b>. </b>.
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113337">
	<h2><b>3.1.1.3.3.3.7 How to Search Network and Linkages</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular network and linkages, enter a keyword on the Search textbox shown in Figure 0-0.</li>
			<li>Type in any of the following fields: <b>Field</b>, and <b>Expected Output</b>.
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113338">
	<h2><b>3.1.1.3.3.3.8 How to Search Research and Development</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for particular research development, refer to section 3.1.1.3.3.3.7 <i>How to Search Network and Linkages</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113339">
	<h2><b>3.1.1.3.3.3.9 How to Search Others</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for other activities, refer to section 3.1.1.3.3.3.7 <i>How to Search Network and Linkages</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod411334">
	<h2><b>3.1.1.3.3.4	How to Delete Target Deliverables</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target deliverables, click the column corresponding to the type of target deliverables to delete shown in Figure 0-0.
			<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/Delete Target Deliverables.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Delete Target Deliverables page</i></b></small>
				</div>
				<br>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113341">
	<h2><b>3.1.1.3.3.4.1 How to Delete Seminars/Lectures/Forums</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target seminar/lecture/forum, click the <b>Delete</b> icon <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_b4b8ebc1.png')?>" width="20" height="19" /> corresponding to the details of the item to delete.</li>
			<li>A message prompt will appear confirming the seminar/lecture/forum details deletion. Click the <b>Yes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_390ec87e.png')?>" width="25" height="19" /> to proceed or the <b>No</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ec138e9b.png')?>" width="23" height="19" /> to do otherwise.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113342">
	<h2><b>3.1.1.3.3.4.2 How to Delete Trainings/Workshops/ Demonstrations</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target training/workshop/demonstration, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113343">
	<h2><b>3.1.1.3.3.4.3 How to Delete Projects to be Assisted</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target project to be assisted, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113344">
	<h2><b>3.1.1.3.3.4.4 How to Delete Papers to be Submitted for Publication</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target paper to be submitted for publication, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/ Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113345">
	<h2><b>3.1.1.3.3.4.5 How to Delete Students Mentoring</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target students mentoring, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/ Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113346">
	<h2><b>3.1.1.3.3.4.6 How to Delete Curriculum/Course Development</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target curriculum/course development, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/ Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113347">
	<h2><b>3.1.1.3.3.4.7 How to Delete Network and Linkages</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target network and linkages, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/ Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113348">
	<h2><b>3.1.1.3.3.4.8 How to Delete Research and Development</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete target research and development, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/ Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113349">
	<h2><b>3.1.1.3.3.4.9 How to Delete Others</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete other target activity/ies, refer to section 3.1.1.3.3.4.1 <i>How to Delete Seminars/Lectures/ Forums</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod41134">
	<h2><b>3.1.1.3.4 How to Manage Accomplishments</b></h2>
	<br>
	<p>This tab allows user to manage balik scientist’s accomplishments. To access the Accomplishments tab, click the <b>Accomplishments</b> tab under Service Awards submenu (See Figure 0-0).</p>
	<br>
	<div align="center">
		<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ef010c5f.png')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Manage Accomplishments Tab</i></b></small>
	</div>
</div>

<div v-show="menu.adminmod411341">
	<h2><b>3.1.1.3.4.1 How to Add Accomplishments</b></h2>
	<br>
	<p>
		<ol>
			<li>To add accomplishments, click the column corresponding to the type of accomplishments to add, and then fill up the entry boxes that constitute the parameters for adding new accomplishment (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/Accomplishment page.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Accomplishment page</i></b></small>
				</div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113411">
	<h2><b>3.1.1.3.4.1.1 How to Add Seminars/Lectures/Forums Attended</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the <b>Add Seminar/Lecture/Forum</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_3d48ea5f.png')?>" width="120" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new seminars/lectures/forums attended (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_d688a6cf.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Seminars/Lectures/Forums Attended form</i></b></small>
				</div>
			</li>
			<li>Specify the <b>Field</b> from the drop-down list.</li>
			<li>Type in the <b>Title</b>, <b>Date</b>, and <b>Venue</b> of seminars/lectures/forums attended on the entry boxes provided.</li>
			<li>Specify the <b>Type of Participants</b> from the drop-down list.</li>
			<li>To upload attachments, click the <b>Upload</b> button <img src="<?= base_url('assets/images/help/Upload button.jpg')?>" width="42" height="19" />. Select the file to upload by clicking <b>Open</b> button <img src="<?= base_url('assets/images/help/Open button.jpg')?>" width="46" height="19" />.</li>
			<li>Type in the <b> No. of Participants</b> (if applicable) and <b>Output</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new seminars/lectures/forums attended details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113412">
	<h2><b>3.1.1.3.4.1.2 How to Add Trainings/Workshops/Demonstrations Attended</b></h2>
	<br>
		<div align="center">
			<img src="<?= base_url('assets/images/help/Training - Accomplishment page.jpg')?>" style="width: 100%;" />
			<br><small><i><b>Figure 0-0 Training/Workshop/Demonstration – Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>Click the <b>Add Training/Workshop/Demonstration</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_e3cf90b.png')?>" width="153" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new trainings/workshops/demonstrations attended (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/Add Training2 - Accomplishment form.jpg')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Training - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>
				Refer to section 3.1.1.3.4.1.1 <i>How to Add Seminars/Lectures/Forums Attended</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new trainings/workshops/demonstrations attended details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113413">
	<h2><b>3.1.1.3.4.1.3 How to Add Projects Assisted</b></h2>
	<br>
		<div align="center">
				<img src="<?= base_url('assets/images/help/Projects - Accomplishment page.jpg')?>" style="width: 100%;" />
				<br><small><i><b>Figure 0-0 Projects – Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>Click the <b>Add Project</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_498cbd6b.png')?>" width="59" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new projects assisted (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_45f11415.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Project - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>Specify the <b>Field</b> from the drop-down list.</li>
			<li>Type in the <b>Title</b>, <b>Objectives</b>, and <b>Implementing Agency</b> of projects assisted on the entry boxes provided.</li>
			<li>Specify the <b>Project Status</b> from the drop-down list.</li>
			<li>Type in the <b>Contribution to the Project</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new projects assisted details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113414">
	<h2><b>3.1.1.3.4.1.4 How to Add Papers Submitted for Publication</b></h2>
	<br>
		<div align="center">
				<img src="<?= base_url('assets/images/help/Paper - Accomplishment page.jpg')?>" style="width: 100%;" />
				<br><small><i><b>Figure 0-0 Paper – Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>Click the <b>Add Paper</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_7de84014.png')?>" width="55" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new papers submitted for publication (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_82216f01.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Paper - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>Specify the <b>Field</b> from the drop-down list.</li>
			<li>Type in the <b>Name of journal/magazine/newsletter/ newspaper/book</b>, <b>ISBN No./volume/issue/edition</b>, <b>Title of paper/article/chapter/book</b>, <b>Date Submitted for Publication</b>, If published, <b>Date of Publication</b>, <b>No. of Pages</b>, and <b>Authors</b> of papers submitted for publication on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new papers submitted for publication details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113415">
	<h2><b>3.1.1.3.4.1.5 How to Add Students Mentored</b></h2>
	<br>
		<div align="center">
				<img src="<?= base_url('assets/images/help/Students Mentoring - Accomplishment page.jpg')?>" style="width: 100%;" />
				<br><small><i><b>Figure 0-0 Students Mentoring – Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>Click the <b>Add Students Mentoring</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_7b8fc34.png')?>" width="70" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new students mentored (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_9f2e97c1.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Students Mentored - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>Specify the <b>Field</b> and <b>Host Institution</b> from the drop-down lists.</li>
			<li>Type in the <b>No. of Students</b>, <b>Course</b>, and <b>Output</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new students mentored details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113416">
	<h2><b>3.1.1.3.4.1.6 How to Add Curriculum/Course Developed</b></h2>
	<br>
		<div align="center">
				<img src="<?= base_url('assets/images/help/Curriculum Course Development - Accomplishment page.jpg')?>" style="width: 100%;" />
				<br><small><i><b>Figure 0-0 Curriculum Course Development - Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>Click the <b>Add Curriculum/Course Development</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_d2a10791.png')?>" width="90" height="19" /> shown in Figure 0-0. Fill-up the entry boxes that constitute the parameters for adding new curriculum/course developed (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_65a2b9dd.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Curriculum/Course Developed - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>Specify the <b>Field</b> and <b>Host Institution</b> from the drop-down lists.</li>
			<li>Type in the <b>Output</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new curriculum/course developed details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113417">
	<h2><b>3.1.1.3.4.1.7 How to Add Network and Linkages</b></h2>
	<br>
		<div align="center">
				<img src="<?= base_url('assets/images/help/Network and Linkages - Accomplishment page.jpg')?>" style="width: 100%;" />
				<br><small><i><b>Figure 0-0 Network and Linkages - Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>Click the <b>Add Network and Linkages</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_a105c99d.png')?>" width="90" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new network and linkages (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_6f60e6af.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Network and Linkages - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>Specify the <b>Field</b> from the drop-down list.</li>
			<li>Type in the <b>Output</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new network and linkages details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113418">
	<h2><b>3.1.1.3.4.1.8 How to Add Research and Development</b></h2>
	<br>
		<div align="center">
				<img src="<?= base_url('assets/images/help/Research and Development - Accomplishment page.jpg')?>" style="width: 100%;" />
				<br><small><i><b>Figure 0-0 Research and Development - Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>Click the <b>Add Research and Development</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_dfecbcca.png')?>" width="90" height="19" /> shown in Figure 0-0. Fill up the entry boxes that constitute the parameters for adding new research and development (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_71b79bd3.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Research and Development - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>
				Refer to section 4.1.1.3.4.1.7 <i>How to Add Network and Linkages</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new research and development details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113419">
	<h2><b>3.1.1.3.4.1.9 How to Add Others</b></h2>
	<br>
		<div align="center">
				<img src="<?= base_url('assets/images/help/Research and Development - Accomplishment page.jpg')?>" style="width: 100%;" />
				<br><small><i><b>Figure 0-0 Others - Accomplishment page</i></b></small>
		</div>
	<p>
		<ol>
			<li>
				Fill up the entry boxes that constitute the parameters for adding other accomplished activities (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_460e3887.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Add Others - Accomplishment form</i></b></small>
				</div>
			</li>
			<li>
				Refer to section 4.1.1.3.4.1.7 <i>How to Add Network and Linkages</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new accomplished activity details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod411342">
	<h2><b>3.1.1.3.4.2 How to View & Update Accomplishments</b></h2>
	<br>
	<p>
		<ol>
			<li>
				To view and update accomplishments, click the column corresponding to the type of accomplishments to view and update in Figure 0-0.
				<br>
					<div align="center">
						<img src="<?= base_url('assets/images/help/View and Update Accomplishments.jpg')?>" style="width: 100%;" />
						<br><small><i><b>Figure 0-0 View and Update Accomplishments Page</i></b></small>
					</div>
				<br>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113421">
	<h2><b>3.1.1.3.4.2.1 How to View & Update Seminars/Lectures/Forums Attended</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of seminars/lectures/forums attended to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Title</b>, <b>Date</b>, <b>Venue</b>, <b>Type of Participants</b>,<b> No. of Participants</b>, and <b>Output</b>.
				<br>
					<div align="center">
						<img src="<?= base_url('assets/images/help/view and update seminar lecture forum - accomplishment.jpg')?>" style="width: 70%;" />
						<br><small><i><b>Figure 0-0 View and Update Seminar/Lecture/Forum Attended form</i></b></small>
					</div>
			</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entry. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the seminars/lectures/forums attended details will be displayed on the list below the form (See Figure 0-0). A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113422">
	<h2><b>3.1.1.3.4.2.2 How to View & Update Trainings/Workshops/ Demonstrations Attended</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of trainings/workshops/demonstrations attended to view and update.</li>
			<li>
				Refer to section 3.1.1.3.4.2.1 <i>How to View & Update Seminars/Lectures/Forums Attended</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the trainings/workshops/demonstrations attended details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113423">
	<h2><b>3.1.1.3.4.2.3 How to View & Update Projects Assisted</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of projects assisted to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Title</b>, <b>Objectives</b>, <b>Implementing Agency</b>, <b>Project Status</b>, and <b>Contribution to the Project</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entry. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the projects assisted details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113424">
	<h2><b>3.1.1.3.4.2.4 How to View & Update Papers Submitted for Publication</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of papers submitted to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Name of journal/magazine/newsletter/newspaper/book</b>, <b>ISBN No./volume/issue/edition</b>, <b>Title of paper/ article/chapter/book</b>, <b>Date Submitted for Publication</b>. If published, <b>Date of Publication</b>, <b>No. of Pages</b>, and <b>Authors</b> of papers submitted for publication.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entry. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the papers submitted for publication details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113425">
	<h2><b>3.1.1.3.4.2.5 How to View & Update Students Mentored</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of students mentored to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Host Institution</b>, <b>No. of Students</b>, <b>Course</b>, and <b>Output</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entry. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the students mentored details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113426">
	<h2><b>3.1.1.3.4.2.6 How to View & Update Curriculum/Course Developed</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of curriculum/course developed to view and update.</li>
			<li>Information that can be updated includes <b>Field</b>, <b>Host Institution</b>, and <b>Expected Output</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entry. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new curriculum/course development details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113427">
	<h2><b>3.1.1.3.4.2.7 How to View & Update Network and Linkages</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of network and linkages to view and update.</li>
			<li>Information that can be updated includes <b>Field</b> and <b>Expected Output</b> (See Figure 0-0).</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entry. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="32" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new network and linkages details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113428">
	<h2><b>3.1.1.3.4.2.8 How to View & Update Research and Development</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of research and development to view and update.</li>
			<li>
				Refer to section 3.1.1.3.3.2.7 <i>How to View & Update Network and Linkages</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the research and development details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113429">
	<h2><b>3.1.1.3.4.2.9 How to View & Update Other Accomplishments</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the details of other accomplishment to view and update.</li>
			<li>
				Refer to section 3.1.1.3.4.2.1 <i>How to View & Update Network and Linkages</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the other accomplishment details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod411343">
	<h2><b>3.1.1.3.4.3 How to Search Accomplishments</b></h2>
	<br>
	<p>
		<ol>
			<li>To search accomplishments, click the column corresponding to the type of accomplishment to search in Figure 0-0.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_1f956d0a.png')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Search – Accomplishments page</i></b></small>
				</div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113431">
	<h2><b>3.1.1.3.4.3.1 How to Search Seminars/Lectures/Forums Attended</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular seminar/lecture/forum, enter a keyword on the <b>Search</b> textbox in Figure 0-0.</li>
			<li>
				Type in any of the following fields: <b>Field</b>, <b>Title</b>, <b>Date</b>, <b>Venue</b>, <b>Type of Participants</b>, <b>No. of Participants</b>, and <b>Output</b>.
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/Search Results - Accomplishment.jpg')?>" style="width: 100%;" />
					<br><small><i><b>Figure 0-0 Search Results - Accomplishment</i></b></small>
				</div>
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113432">
	<h2><b>3.1.1.3.4.3.2 How to Search Trainings/Workshops/ Demonstrations Attended</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular training/workshop/ demonstration, refer to section 3.1.1.3.4.3.1 <i>How to Search Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113433">
	<h2><b>3.1.1.3.4.3.3 How to Search Projects Assisted</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular project assisted, enter a keyword on the <b>Search</b> textbox in Figure 0-0.</li>
			<li>
				Type in any of the following fields: <b>Field</b>, <b>Title</b>, <b>Objectives</b>, <b>Implementing Agency</b>, <b>Project Status</b>, and <b>Contribution</b> to the Project.
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113434">
	<h2><b>3.1.1.3.4.3.4 How to Search Papers Submitted for Publication</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular paper submitted for publication, enter a keyword on the <b>Search</b> textbox in Figure 0-0.</li>
			<li>
				Type in any of the following fields: <b>Field</b>, <b>Name of journal/magazine/newsletter/newspaper/book</b>, <b>ISBN No./volume/issue/edition</b>, <b>Title of paper/ article/chapter/book</b>, <b>Date Submitted for Publication</b>, If published, <b>Date of Publication</b>, <b>No. of Pages</b>, and <b>Authors</b>.
			</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113435">
	<h2><b>3.1.1.3.4.3.5 How to Search Students Mentored</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular student mentored, enter a keyword on the <b>Search</b> textbox in Figure 0-0.</li>
			<li>Type in any of the following fields: <b>Field</b>, <b>Host Institution</b>, <b>No. of Students</b>, <b>Course</b>, and <b>Output</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113436">
	<h2><b>3.1.1.3.4.3.6 How to Search Curriculum/Course Developed</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular curriculum/course developed, enter a keyword on the <b>Search</b> textbox in Figure 0-0.</li>
			<li>Type in any of the following fields: <b>Field</b>, <b>Host Institution</b>, and <b>Output</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113437">
	<h2><b>3.1.1.3.4.3.7 How to Search Network and Linkages</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular curriculum/course developed, enter a keyword on the <b>Search</b> textbox in Figure 0-0.</li>
			<li>Type in any of the following fields: <b>Field</b>, and <b>Output</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113438">
	<h2><b>3.1.1.3.4.3.8 How to Search Research and Development</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for particular research and development, refer to section 4.1.1.3.4.3.7 <i>How to Search Network and Linkages</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113439">
	<h2><b>3.1.1.3.4.3.9 How to Search Others</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for other accomplishments, refer to section 4.1.1.3.4.3.7 <i>How to Search Network and Linkages</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod411344">
	<h2><b>3.1.1.3.4.4 How to Delete Accomplishments</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete accomplishment, click the column corresponding to the type of accomplishment to delete in Figure 0-0.
				<br>
					<div align="center">
						<img src="<?= base_url('assets/images/help/Delete Accomplishments.jpg')?>" style="width: 100%;" />
						<br><small><i><b>Figure 0-0 Delete Accomplishments page</i></b></small>
					</div>
				<br>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113441">
	<h2><b>3.1.1.3.4.4.1 How to Delete Seminars/Lectures/Forums Attended</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete seminar/lecture/forum attended, click the <b>Delete</b> icon <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_b4b8ebc1.png')?>" width="20" height="19" /> corresponding to the details of the item to delete.</li>
			<li>A message prompt will appear confirming the seminar/lecture/forum attended details deletion. Click the <b>Yes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_390ec87e.png')?>" width="25" height="19" /> to proceed or the <b>No</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ec138e9b.png')?>" width="23" height="19" /> to do otherwise.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113442">
	<h2><b>3.1.1.3.4.4.2 How to Delete Trainings/Workshops/ Demonstrations Attended</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete training/workshop/demonstration attended, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113443">
	<h2><b>3.1.1.3.4.4.3 How to Delete Projects Assisted</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete project assisted, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113444">
	<h2><b>3.1.1.3.4.4.4 How to Delete Papers Submitted for Publication</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete papers submitted for publication, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113445">
	<h2><b>3.1.1.3.4.4.5 How to Delete Students Mentored</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete students mentored, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113446">
	<h2><b>3.1.1.3.4.4.6 How to Delete Curriculum/Course Developed</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete curriculum/course developed, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113447">
	<h2><b>3.1.1.3.4.4.7 How to Delete Network and Linkages</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete network and linkages, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113448">
	<h2><b>3.1.1.3.4.4.8 How to Delete Research and Development</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete research and development, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4113449">
	<h2><b>3.1.1.3.4.4.9 How to Delete Others</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete other accomplishment, refer to section 3.1.1.3.4.4.1 <i>How to Delete Seminars/Lectures/Forums Attended</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod41135">
	<h2><b>3.1.1.3.5 How to Add Reports Submitted</b></h2>
	<br>
	<p>
		<ol>
			<li>
				To add reports submitted, select the option corresponding to the details of reports submitted (See Figure 0-0).
				<br><br>
				<div align="center">
					<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d142196.png')?>" style="width: 70%;" />
					<br><small><i><b>Figure 0-0 Reports Submitted page</i></b></small>
				</div>
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>:  If the reports were already submitted, select <b>Y</b> from the options and type in the <b>Date</b> on the textbox (in the following format: YYYY-MM-DD). Select <b>N</b> if the reports are not been submitted, and <b>NA yet</b> if some of the reports are not yet submitted.</i></div>
			</li>
			<li>Type in the <b>Comments</b> (if any) on the entry box provided.</li>
			<li>When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. A confirmation message will be displayed if the action is successful</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod41136">
	<h2><b>3.1.1.3.6 How to Update Reports Submitted</b></h2>
	<br>
	<p>
		<ol>
			<li>To update reports submitted, select the option corresponding to the details of reports submitted to update.</li>
			<li>Information that can be updated includes <b>Date</b> the reports were submitted and <b>Comments</b> (if any).</li>
			<li>Click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. A confirmation message will be displayed if the action is successful</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod41137">
	<h2><b>3.1.1.3.7 How to Add Assigned Secretariat</b></h2>
	<br>
	<div align="center">
		<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_16083f64.png')?>" style="width: 50%;" />
		<br><small><i><b>Figure 0-0 Add Assigned Secretariat form</i></b></small>
	</div>
	<br>
	<p>
		<ol>
			<li>Fill-up the entry boxes that constitute the parameters for adding assigned secretariat.</li>
			<li>Specify the <b>Processed By</b> from the drop-down list.</li>
			<li>Type in the <b>Date of Approval</b> (in the following format: <i>YYYY-MM-DD</i>) on the entry box provided.</li>
			<li>When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add assigned secretariat.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod42">
	<h2><b>3.2 Reports</b></h2>
	<br>
	<p>This section allows user to generate, print, and export DOST-CO BSPMS reports. This can be access by the Administrator, BSP Staff, and Council. To access the Reports page, click the <b>Reports</b> tab under the main menu (See Figure 0-0).</p>
	<br>
	<div align="center">
		<img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ef8d2e35.png')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Reports page</i></b></small>
	</div>
</div>

<div v-show="menu.adminmod421">
	<h2><b>3.2.1 How to Generate and Print Reports</b></h2>
	<br>
	<p>
		<ol>
			<li>
				To generate reports, fill-up the entry boxes that constitute the parameters for generating reports.
				<ol type="a">
					<li>Specify the <b>Report Type</b> from the drop-down list.</li>
					<li>Depending on the report chosen, supply the parameter(s) it may require using the entry box(es) provided. It may be the <b>scientist</b>, <b>period</b> or <b>year</b>.</li>
					<li>When done, click <b>Print</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_2f85e682.png')?>" width="39" height="19" /> to generate report.</li>
				</ol>
			</li>
			<li>
				To print reports, click the <b>Print</b> icon <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_36387b5d.png')?>" width="22" height="19" /> located at the upper-right side of the application and click it.
				<ol type="a">
					<li>Specify printing options that will be prompted. Printing options vary depending on the printer.</li>
					<li>Click <b>Ok</b> to proceed.</li>
				</ol>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod422">
	<h2><b>3.2.2 How to Save Reports</b></h2>
	<br>
	<p>
		<ol>
			<li>Press <b>Ctrl+Shift+S</b> or look for the <b>Save</b> icon <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ed547f3.png')?>" width="22" height="19" /> on the application and click it.</li>
			<li>User will be prompted to enter the name of the file. Specify directory path and type desired name for the file.</li>
			<li>Click <b>Save</b> to proceed.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43">
	<h2><b>3.3 Libraries</b></h2>
	<br>
	<p>This section allows user to manage libraries and its sub-sections. This can only be access by the Administrator. To access the Libraries page, click the <b>Libraries</b> tab under the main menu (See Figure 0-0).</p>
	<div align="center">
		<img src="<?= base_url('assets/images/help/Libraries4.jpg')?>" style="width: 100%;" />
		<br><small><i><b>Figure 0-0 Libraries page</i></b></small>
	</div>
</div>

<div v-show="menu.adminmod431">
	<h2><b>3.3.1 How to Manage Area of Expertise</b></h2>
	<br>
	<p>This section allows user to manage area of expertise. User can add, view, update, search, and delete area of expertise. This section can only be access by the Administrator. To access the area of expertise section, click the <b>Area of Expertise</b> tab under the Libraries menu (See Figure 0-0).</p>
</div>

<div v-show="menu.adminmod431001">
	<h2><b>3.3.1.1 How to Add Area of Expertise</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new area of expertise shown in Figure 0-0.</li>
			<li>Type in the <b>Code</b> and <b>Description</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new area of expertise details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod431002">
	<h2><b>3.3.1.2 How to View & Update Area of Expertise</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the area of expertise details to view and update.</li>
			<li>Information that can be updated includes <b>Code</b> and <b>Description</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new area of expertise details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod431003">
	<h2><b>3.3.1.3 How to Search Area of Expertise</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular area of expertise, enter a keyword on the Search textbox in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Type in any of the following fields: <b>Code</b> and <b>Description</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod431004">
	<h2><b>3.3.1.4 How to Delete Area of Expertise</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete area of expertise, click the <b>Delete</b> icon <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_b4b8ebc1.png')?>" width="20" height="19" /> corresponding to the details of the item to delete.</li>
			<li>A message prompt will appear confirming the area of expertise details deletion. Click the <b>Yes</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_390ec87e.png')?>" width="25" height="19" /> to proceed or the <b>No</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_ec138e9b.png')?>" width="23" height="19" /> to do otherwise.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod432">
	<h2><b>3.3.2 How to Manage Citizenship</b></h2>
	<br>
	<p>This section allows user to manage citizenship. User can add, view, update, search, and delete citizenship. This section can only be access by the Administrator. To access the citizenship section, click the <b>Citizenship</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4321">
	<h2><b>3.3.2.1 How to Add Citizenship</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new citizenship shown in Figure -0 /*--4.3.1--*/.</li>
			<li>Specify the <b>Type</b> from the drop-down list.</li>
			<li>Type in the <b>Nationaly(ties)</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new citizenship details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4322">
	<h2><b>3.3.2.2 How to View & Update Citizenship</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the citizenship details to view and update.</li>
			<li>Information that can be updated includes <b>Type</b> and <b>Nationality(ties)</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new citizenship details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4323">
	<h2><b>3.3.2.3 How to Search Citizenship</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular citizenship, enter a keyword on the <b>Search</b> textbox in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Type in any of the following fields: <b>Type</b> and <b>Nationality(ties)</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4324">
	<h2><b>3.3.2.4 How to Delete Citizenship</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete citizenship, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod433">
	<h2><b>3.3.3 How to Manage Courses</b></h2>
	<br>
	<p>This section allows user to manage courses. User can add, view, update, search, and delete courses. This section can only be access by the Administrator. To access the courses section, click the <b>Courses</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4331">
	<h2><b>3.3.3.1 How to Add Courses</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new courses.</li>
			<li>
				Refer to section 4.3.1.1 <i>How to Add Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new courses details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4332">
	<h2><b>3.3.3.2 How to View & Update Courses</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the courses details to view and update.</li>
			<li>
				Refer to section 4.3.1.1 <i>How to Add Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>:  Once updated, the new courses details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4333">
	<h2><b>3.3.3.3 How to Search Courses</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular course, refer to section 4.3.1.3 <i>How to Search Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4334">
	<h2><b>3.3.3.4 How to Delete Courses</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete courses, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod434">
	<h2><b>3.3.4 How to Manage DOST Outcomes</b></h2>
	<br>
	<p>This section allows user to manage DOST outcomes. User can add, view, update, search, and delete DOST outcomes. This section can only be access by the Administrator. To access the DOST outcomes section, click the <b>DOST Outcomes</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4341">
	<h2><b>3.3.4.1 How to Add DOST Outcomes</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new DOST outcomes shown in Figure 0-0.</li>
			<li>Type in the <b>Order</b> and <b>Description</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new DOST outcomes details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4342">
	<h2><b>3.3.4.2 How to View & Update DOST Outcomes</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the DOST outcomes details to view and update.</li>
			<li>Information that can be updated includes <b>Order</b> and <b>Description</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new DOST outcomes details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4343">
	<h2><b>3.3.4.3 How to Search DOST Outcomes</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular DOST outcome, enter a keyword on the <b>Search</b> textbox in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Type in any of the following fields: <b>Order</b> and <b>Description</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4344">
	<h2><b>3.3.4.4 How to Delete DOST Outcomes</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete DOST outcomes, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod435">
	<h2><b>3.3.5 How to Manage DOST Priority Areas</b></h2>
	<br>
	<p>This section allows user to manage DOST priority areas. User can add, view, update, search, and delete DOST priority areas. This section can only be access by the Administrator. To access the DOST priority areas section, click the <b>DOST Priority Areas</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4351">
	<h2><b>3.3.5.1 How to Add DOST Priority Areas</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new DOST priority areas shown in Figure -0 /*-4.3.1--*/</li>
			<li>
				Refer to section 4.3.4.1 <i>How to Add DOST Outcomes</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new DOST priority areas details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4352">
	<h2><b>3.3.5.2 How to View & Update DOST Priority Areas</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the DOST priority areas details to view and update.</li>
			<li>
				Refer to section 4.3.4.2 <i>How to View & Update DOST Outcomes</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new DOST priority areas details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4353">
	<h2><b>3.3.5.3 How to Search DOST Priority Areas</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular DOST priority areas, enter a keyword on the Search textbox in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Refer to section 4.3.4.3 <i>How to Search DOST Outcomes</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4354">
	<h2><b>3.3.5.4 How to Delete DOST Priority Areas</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete DOST priority areas, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod436">
	<h2><b>3.3.6 How to Manage Educational Levels</b></h2>
	<br>
	<p>This section allows user to manage educational levels. User can add, view, update, search, and delete educational levels. This section can only be access by the Administrator. To access the educational levels section, click the <b>Educational Levels</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4361">
	<h2><b>3.3.6.1 How to Add Educational Levels</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new educational levels shown in Figure -0 /*--4.3.1--*/.</li>
			<li>
				Refer to section 4.3.1.1 <i>How to Add Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new educational levels details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4362">
	<h2><b>3.3.6.2 How to View & Update Educational Levels</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the educational levels details to view and update.</li>
			<li>
				Refer to section 4.3.1.2 <i>How to View & Update Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new educational levels details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4363">
	<h2><b>3.3.6.3 How to Search Educational Levels</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular educational level, enter a keyword on the <b>Search</b> textbox in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Refer to section 4.3.1.3 <i>How to Search Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4364">
	<h2><b>3.3.6.4 How to Delete Educational Levels</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete educational levels, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod437">
	<h2><b>3.3.7 How to Manage Fields (in Activities and Accomplishments)</b></h2>
	<br>
	<p>This section allows user to manage fields. User can add, view, update, search, and delete fields. This section can only be access by the Administrator. To access the fields section, click the <b>Fields</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4371">
	<h2><b>3.3.7.1 How to Add Fields (in Activities and Accomplishments)</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new fields shown in Figure -0 /*--4.3.1--*/.</li>
			<li>
				Refer to section 4.3.1.1 <i>How to Add Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new fields’ details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4372">
	<h2><b>3.3.7.2 How to View & Update Fields (in Activities and Accomplishments)</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the fields’ details to view and update.</li>
			<li>
				Refer to section 4.3.1.2 <i>How to View & Update Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new fields’ details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4373">
	<h2><b>3.3.7.3 How to Search Fields (in Activities and Accomplishments)</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular field, enter a keyword on the <b>Search</b> textbox in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Refer to section 4.3.1.3 <i>How to Search Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4374">
	<h2><b>3.3.7.4 How to Delete Fields (in Activities and Accomplishments)</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete fields, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod438">
	<h2><b>3.3.8 How to Manage Global Regions</b></h2>
	<br>
	<p>This section allows user to manage global regions. User can add, view, update, search, and delete global regions. This section can only be access by the Administrator. To access the global regions section, click the <b>Global Regions</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4381">
	<h2><b>3.3.8.1 How to Add Global Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new global regions shown in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Specify the <b>Continent</b> from the drop-down list.</li>
			<li>Type in the <b>Country</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new global regions details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4382">
	<h2><b>3.3.8.2	How to View & Update Global Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the global regions details to view and update.</li>
			<li>Information that can be updated includes <b>Continent</b> and <b>Country</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new global regions details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4383">
	<h2><b>3.3.8.3	How to Search Global Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular global region, enter a keyword on the <b>Search</b> textbox in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Type in any of the following fields: <b>Continent</b> and <b>Country</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4384">
	<h2><b>3.3.8.4	How to Delete Global Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete global regions, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod439">
	<h2><b>3.3.9	How to Manage Host Institution</b></h2>
	<br>
	<p>This section allows user to manage host institution. User can add, view, update, search, and delete host institution. This section can only be access by the Administrator. To access the host institution section, click the <i>Host Institutions</i> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod4391">
	<h2><b>3.3.9.1	How to Add Host Institution</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new host institution shown in Figure -0 /*–put image on 4.3.1--*/.</li>
			<li>Specify the <b>Institution Type</b> and <b>Local Region</b> from the drop-down lists.</li>
			<li>Type in the <b>Code</b> and <b>Description</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new host institution details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4392">
	<h2><b>3.3.9.2	How to View & Update Host Institution</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the host institution details to view and update.</li>
			<li>Information that can be updated includes Institution Type, Local Region, Code, and Description.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new host institution details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4393">
	<h2><b>3.3.9.3	How to Search Host Institution</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular host institution, enter a keyword on the <b>Search</b> textbox in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Type in any of the following fields: <b>Institution Type</b>, <b>Local Region</b>, <b>Code</b>, and <b>Description</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4394">
	<h2><b>3.3.9.4	How to Delete Host Institution</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete host institution, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4310">
	<h2><b>3.3.10	How to Manage Local Regions</b></h2>
	<br>
	<p>This section allows user to manage local regions. User can add, view, update, search, and delete local regions. This section can only be access by the Administrator. To access the local regions section, click the <b>Local Regions</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod43101">
	<h2><b>3.3.10.1	How to Add Local Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new local regions shown in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Specify the <b>Region</b> from the drop-down list.</li>
			<li>Type in the <b>Province</b> on the entry box provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new local regions details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43102">
	<h2><b>3.3.10.2	How to View & Update Local Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the local regions details to view and update.</li>
			<li>Information that can be updated includes <b>Region</b> and <b>Province</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to save entries. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to do otherwise.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new local regions details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43103">
	<h2><b>3.3.10.3	How to Search Local Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular local region, enter a keyword on the <b>Search</b> textbox in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Type in any of the following fields: <b>Region</b> and <b>Province</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43104">
	<h2><b>3.3.10.4	How to Delete Local Regions</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete local regions, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4311">
	<h2><b>3.3.11	How to Manage Monitoring Councils</b></h2>
	<br>
	<p>This section allows user to manage monitoring councils. User can add, view, update, search, and delete monitoring councils. This section can only be access by the Administrator. To access the monitoring councils section, click the <b>Monitoring Councils</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod43111">
	<h2><b>3.3.11.1	How to Add Monitoring Councils</b></h2>
	<br>
	<p>
		<ol>
			<li>1.	Fill up the entry boxes that constitute the parameters for adding new monitoring councils shown in Figure -0 /*put image on 4.3.1*/.</li>
			<li>
				Refer to section 4.3.1.1 <i>How to Add Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new monitoring council details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43112">
	<h2><b>3.3.11.2	How to View & Update Monitoring Councils</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the monitoring council details to view and update.</li>
			<li>
				Refer to section 4.3.1.2 <i>How to View & Update Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new monitoring council details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43113">
	<h2><b>3.3.11.3	How to Search Monitoring Councils</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular monitoring council, enter a keyword on the <i>Search</i> textbox in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Refer to section 4.3.1.3 <i>How to Search Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43114">
	<h2><b>3.3.11.4	How to Delete Monitoring Councils</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete monitoring councils, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4312">
	<h2><b>3.3.12	How to Manage Participants</b></h2>
	<br>
	<p>This section allows user to manage participants. User can add, view, update, search, and delete participants. This section can only be access by the Administrator. To access the participants section, click the <b>Participants</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod43121">
	<h2><b>3.3.12.1	How to Add Participants</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new participants shown in Figure -0 /*put image on 4.3.1*/.</li>
			<li>
				Refer to section 4.3.1.1 <i>How to Add Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new participant’s details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43122">
	<h2><b>3.3.12.2	How to View & Update Participants</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the participant’s details to view and update.</li>
			<li>
				Refer to section 4.3.1.2 <i>How to View & Update Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new participant’s details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43123">
	<h2><b>3.3.12.3	How to Search Participants</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular participant, enter a keyword on the <b>Search</b> textbox in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Refer to section 4.3.1.3 <i>How to Search Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43124">
	<h2><b>3.3.12.4	How to Delete Participants</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete participants, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4313">
	<h2><b>3.3.13	How to Manage Professions</b></h2>
	<br>
	<p>This section allows user to manage professions. User can add, view, update, search, and delete professions. This section can only be access by the Administrator. To access the professions section, click the <b>Professions</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod43131">
	<h2><b>3.3.13.1	How to Add Professions</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new professions shown in Figure -0 /*put image on 4.3.1*/.</li>
			<li>
				Refer to section 4.3.1.1 <i>How to Add Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new professions details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43132">
	<h2><b>3.3.13.2	How to View & Update Professions</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the professions details to view and update.</li>
			<li>
				Refer to section 4.3.1.2 <i>How to View & Update Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new professions details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43133">
	<h2><b>3.3.13.3	How to Search Professions</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular profession, enter a keyword on the <b>Search</b> textbox in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Refer to section 4.3.1.3 <i>How to Search Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43134">
	<h2><b>3.3.13.4	How to Delete Professions</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete professions, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4314">
	<h2><b>3.3.14	How to Manage Specializations</b></h2>
	<br>
	<p>This section allows user to manage specializations. User can add, view, update, search, and delete specializations. This section can only be access by the Administrator. To access the specializations section, click the <b>Specialization</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod43141">
	<h2><b>3.3.14.1	How to Add Specialization</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new specializations shown in Figure -0 /*put image on 4.3.1*/.</li>
			<li>
				Refer to section 4.3.1.1 How to Add Area of Expertise of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new specializations details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43142">
	<h2><b>3.3.14.2	How to View & Update Specializations</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the specializations details to view and update.</li>
			<li>
				Refer to section 4.3.1.2 <i>How to View & Update Area of Expertise</i> of this manual.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new specializations details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43143">
	<h2><b>3.3.14.3	How to Search Specializations</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular specialization, enter a keyword on the <b>Search</b> textbox in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Refer to section 4.3.1.3 How to <i>Search Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43144">
	<h2><b>3.3.14.4	How to Delete Specializations</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete specializations, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4315">
	<h2><b>3.3.15	How to Manage Titles</b></h2>
	<br>
	<p>This section allows user to manage titles. User can add, view, update, search, and delete titles. This section can only be access by the Administrator. To access the titles section, click the <i>Titles</i> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod43151">
	<h2><b>3.3.15.1	How to Add Titles</b></h2>
	<br>
	<p>
		<ol>
			<li>Fill up the entry boxes that constitute the parameters for adding new titles shown in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Type in the <b>Abbreviation</b> and <b>Description</b> on the entry boxes provided.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new seminar/lecture/forum. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new titles details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful. </i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43152">
	<h2><b>3.3.15.2	How to View & Update Titles</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the titles details to view and update.</li>
			<li>Information that can be updated includes <b>Abbreviation</b> and <b>Description</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new seminar/lecture/forum. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new titles details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43153">
	<h2><b>3.3.15.3	How to Search Titles</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular title, enter a keyword on the Search textbox in Figure -0 /*put image on 4.3.1*/.</li>
			<li>Type in any of the following fields: <b>Abbreviation</b> and <b>Description</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43154">
	<h2><b>3.3.15.4	How to Delete Titles</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete titles, refer to section 4.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod4316">
	<h2><b>3.3.16	How to Manage User Accounts</b></h2>
	<br>
	<p>This section allows user to manage user accounts. User can add, view, update, search, and delete user accounts. This section can only be access by the Administrator. To access the user accounts section, click the <b>User Accounts</b> tab under the Libraries menu (See Figure -0).</p>
</div>

<div v-show="menu.adminmod43161">
	<h2><b>3.3.16.1	How to Add User Accounts</b></h2>
	<br>
	<p>
		<ol>
			<li>1.	Fill up the entry boxes that constitute the parameters for adding new user account shown in Figure -0 /*put image on 4.3.1*/.</li>
			<li>2.	Type in the <b>Employee Name</b>, <b>Username</b>, and <b>Password</b> on the entry boxes provided.</li>
			<li>3.	Specify the <b>Access Level</b> from the drop-down list.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new seminar/lecture/forum. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once added, the new user account details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43162">
	<h2><b>3.3.16.2	How to View & Update User Accounts</b></h2>
	<br>
	<p>
		<ol>
			<li>Click the row corresponding to the user account details to view and update.</li>
			<li>Information that can be updated includes <b>Employee Name</b>, <b>Username</b>, <b>Password</b>, and <b>Access Level</b>.</li>
			<li>
				When done, click <b>Save</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_4678b773.png')?>" width="29" height="19" /> to add the new seminar/lecture/forum. Click <b>Cancel</b> button <img src="<?= base_url('assets/images/help/i_1e1230927033688d_html_8d99330b.png')?>" width="34" height="19" /> to cancel the operation.
				<br><br>
				<div class="well well-sm" style="width:85%;"><i><b>Note</b>: Once updated, the new user account details will be displayed on the list below the form. A confirmation message will be displayed if the action is successful.</i></div>
			</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43163">
	<h2><b>3.3.16.3	How to Search User Accounts</b></h2>
	<br>
	<p>
		<ol>
			<li>To search for a particular user account, enter a keyword on the <b>Search</b> textbox in Figure -0 /*put image on 3.3.1*/.</li>
			<li>Type in any of the following fields: <b>Employee Name</b>, <b>Username</b>, and <b>Access Level</b>.</li>
			<li>Search results will automatically appear on a list.</li>
		</ol>
	</p>
</div>

<div v-show="menu.adminmod43164">
	<h2><b>3.3.16.4	How to Delete User Accounts</b></h2>
	<br>
	<p>
		<ol>
			<li>To delete user accounts, refer to section 3.3.1.4 <i>How to Delete Area of Expertise</i> of this manual.</li>
		</ol>
	</p>
</div>