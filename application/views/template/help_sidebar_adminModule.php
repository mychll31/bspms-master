<div class="nav-side-menu">
	<div class="menu-list">
		<ul id="menu-content" class="menu-content collapse in">
		    <li data-toggle="collapse" data-target="#menlogin" class="collapsed active">
		    	<a href="#" v-on="click: getli('login')">1 Login<span class="arrow"></span></a>
		    </li>
			    <ul class="sub-menu collapse" id="menlogin">
			    	<li class="active"><a href="#" v-on="click: getli('login21')">1.1 How to Login</a></li>
			    </ul>
		    <li data-toggle="collapse" data-target="#products" class="collapsed active">
		    	<a href="#" v-on="click: getli('defaultscr')">2 Default Screen <span class="arrow"></span></a>
		    </li>
		    <li data-toggle="collapse" data-target="#menadmin" class="collapsed active">
		    	<a href="#" v-on="click: getli('adminmod')">3 Administrator Module <span class="arrow"></span></a>
		    </li>
			    <ul class="sub-menu collapse" id="menadmin">
			    	<li class="active" data-toggle="collapse" data-target="#submenu41" class="collapsed"><a href="#" v-on="click: getli('adminmod41')">3.1 Balik Scientist Profile</a></li>
					    <ul class="sub-menu collapse" id="submenu41">
					    	 	<li class="active" data-toggle="collapse" data-target="#submenu411" class="collapsed"><a href="#" v-on="click: getli('adminmod411')">3.1.1	How to Add Profile Information</a></li>
						    	 	<ul class="sub-menu collapse" id="submenu411">
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4111')">3.1.1.1	How to Add Personal Information</a></li>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4112')">3.1.1.2	How to Add Education Information</a></li>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113')">3.1.1.3	How to Add Employment Information</a></li>
						    	 	</ul>
					    	 	<li class="active" data-toggle="collapse" data-target="#submenu412" class="collapsed"><a href="#" v-on="click: getli('adminmod412')">3.1.2	How to View & Update Profile Information</a></li>
						    	 	<ul class="sub-menu collapse" id="submenu412">
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4121')">3.1.2.1	How to View & Update Personal Information</a></li>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4122')">3.1.2.2	How to View & Update Education Information</a></li>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4123')">3.1.2.3	How to View & Update Employment Information</a></li>
						    	 	</ul>
					    	 	<li class="active" data-toggle="collapse" data-target="#submenu413" class="collapsed"><a href="#" v-on="click: getli('adminmod413')">3.1.3	How to Add Balik Scientist Service Awards</a></li>
						    	 	<ul class="sub-menu collapse" id="submenu413">
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4131')">3.1.3.1	How to Add General Information</a></li>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4132')">3.1.3.2	How to View & Update General Information</a></li>
						    	 		<li class="active" data-toggle="collapse" data-target="#submenu4133" class="collapsed"><a href="#" v-on="click: getli('adminmod4133')">3.1.3.3	How to Manage Target Deliverables</a></li>
								    	 	<ul class="sub-menu collapse" id="submenu4133">
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu41331" class="collapsed"><a href="#" v-on="click: getli('adminmod41331')">3.1.3.3.1	How to Add Target Deliverables</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu41331">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod413311')">3.1.3.3.1.1	How to Add Seminars/Lectures/Forums</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod413312')">3.1.3.3.1.2	How to Add Trainings/Workshops/Demonstrations</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod413313')">3.1.3.3.1.3	How to Add Projects to be Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod413314')">3.1.3.3.1.4	How to Add Papers to be Submitted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod413315')">3.1.3.3.1.5	How to Add Students Mentoring</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod413316')">3.1.3.3.1.6	How to Add Curriculum/Course Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113317')">3.1.1.3.3.1.7	How to Add Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113318')">3.1.1.3.3.1.8	How to Add Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113319')">3.1.1.3.3.1.9	How to Add Others</a></li>
										    	 	</ul>
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu411332" class="collapsed"><a href="#" v-on="click: getli('adminmod411332')">3.1.1.3.3.2	How to View & Update Target Deliverables</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu411332">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113321')">3.1.1.3.3.2.1	How to View & Update Seminars/Lectures/Forums</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113322')">3.1.1.3.3.2.2	How to View & Update Trainings/Workshops/ Demonstrations</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113323')">3.1.1.3.3.2.3	How to View & Update Projects to be Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113324')">3.1.1.3.3.2.4	How to View & Update Papers to be Submitted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113325')">3.1.1.3.3.2.5	How to View & Update Students Mentoring</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113326')">3.1.1.3.3.2.6	How to View & Update Curriculum/Course Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113327')">3.1.1.3.3.2.7	How to View & Update Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113328')">3.1.1.3.3.2.8	How to View & Update Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113329')">3.1.1.3.3.2.9	How to View & Update Others</a></li>
										    	 	</ul>
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu411333" class="collapsed"><a href="#" v-on="click: getli('adminmod411333')">3.1.1.3.3.3	How to Search Target Deliverables</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu411333">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113331')">3.1.1.3.3.3.1	How to Search Seminars/Lectures/Forums</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113332')">3.1.1.3.3.3.2	How to Search Trainings/Workshops/ Demonstrations</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113333')">3.1.1.3.3.3.3	How to Search Projects to be Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113334')">3.1.1.3.3.3.4	How to Search Papers to be Submitted for Publication</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113335')">3.1.1.3.3.3.5	How to Search Students Mentoring</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113336')">3.1.1.3.3.3.6	How to Search Curriculum/Course Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113337')">3.1.1.3.3.3.7	How to Search Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113338')">3.1.1.3.3.3.8	How to Search Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113339')">3.1.1.3.3.3.9	How to Search Others</a></li>
										    	 	</ul>
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu411334" class="collapsed"><a href="#" v-on="click: getli('adminmod411334')">3.1.1.3.3.4	How to Delete Target Deliverables</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu411334">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113341')">3.1.1.3.3.4.1	How to Delete Seminars/Lectures/Forums</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113342')">3.1.1.3.3.4.2	How to Delete Trainings/Workshops/ Demonstrations</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113343')">3.1.1.3.3.4.3	How to Delete Projects to be Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113344')">3.1.1.3.3.4.4	How to Delete Papers to be Submitted for Publication</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113345')">3.1.1.3.3.4.5	How to Delete Students Mentoring</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113346')">3.1.1.3.3.4.6	How to Delete Curriculum/Course Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113347')">3.1.1.3.3.4.7	How to Delete Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113348')">3.1.1.3.3.4.8	How to Delete Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113349')">3.1.1.3.3.4.9	How to Delete Others</a></li>
										    	 	</ul>
								    	 	</ul>
						    	 		<li class="active" data-toggle="collapse" data-target="#submenu41134" class="collapsed"><a href="#" v-on="click: getli('adminmod41134')">3.1.1.3.4	How to Manage Accomplishments</a></li>
								    	 	<ul class="sub-menu collapse" id="submenu41134">
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu411341" class="collapsed"><a href="#" v-on="click: getli('adminmod411341')">3.1.1.3.4.1	How to Add Accomplishments</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu411341">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113411')">3.1.1.3.4.1.1	How to Add Seminars/Lectures/Forums Attended</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113412')">3.1.1.3.4.1.2	How to Add Trainings/Workshops/Demonstrations Attended</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113413')">3.1.1.3.4.1.3	How to Add Projects Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113414')">3.1.1.3.4.1.4	How to Add Papers Submitted for Publication</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113415')">3.1.1.3.4.1.5	How to Add Students Mentored</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113416')">3.1.1.3.4.1.6	How to Add Curriculum/Course Developed</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113417')">3.1.1.3.4.1.7	How to Add Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113418')">3.1.1.3.4.1.8	How to Add Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113419')">3.1.1.3.4.1.9	How to Add Others</a></li>
										    	 	</ul>
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu411342" class="collapsed"><a href="#" v-on="click: getli('adminmod411342')">3.1.1.3.4.2	How to View & Update Accomplishments</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu411342">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113421')">3.1.1.3.4.2.1	How to View & Update Seminars/Lectures/Forums Attended</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113422')">3.1.1.3.4.2.2	How to View & Update Trainings/Workshops/ Demonstrations Attended</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113423')">3.1.1.3.4.2.3	How to View & Update Projects Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113424')">3.1.1.3.4.2.4	How to View & Update Papers Submitted for Publication</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113425')">3.1.1.3.4.2.5	How to View & Update Students Mentored</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113426')">3.1.1.3.4.2.6	How to View & Update Curriculum/Course Developed</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113427')">3.1.1.3.4.2.7	How to View & Update Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113428')">3.1.1.3.4.2.8	How to View & Update Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113429')">3.1.1.3.4.2.9	How to View & Update Other Accomplishments</a></li>
										    	 	</ul>
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu411343" class="collapsed"><a href="#" v-on="click: getli('adminmod411343')">3.1.1.3.4.3	How to Search Accomplishments</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu411343">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113431')">3.1.1.3.4.3.1	How to Search Seminars/Lectures/Forums Attended</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113432')">3.1.1.3.4.3.2	How to Search Trainings/Workshops/ Demonstrations Attended</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113433')">3.1.1.3.4.3.3	How to Search Projects Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113434')">3.1.1.3.4.3.4	How to Search Papers Submitted for Publication</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113435')">3.1.1.3.4.3.5	How to Search Students Mentored</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113436')">3.1.1.3.4.3.6	How to Search Curriculum/Course Developed</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113437')">3.1.1.3.4.3.7	How to Search Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113438')">3.1.1.3.4.3.8	How to Search Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113439')">3.1.1.3.4.3.9	How to Search Others</a></li>
										    	 	</ul>
								    	 		<li class="active" data-toggle="collapse" data-target="#submenu411344" class="collapsed"><a href="#" v-on="click: getli('adminmod411344')">3.1.1.3.4.4	How to Delete Accomplishments</a></li>
										    	 	<ul class="sub-menu collapse" id="submenu411344">
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113441')">3.1.1.3.4.4.1	How to Delete Seminars/Lectures/Forums Attended</a></li>
										    	 		<li class="active">	<a href="#" v-on="click: getli('adminmod4113442')">3.1.1.3.4.4.2	How to Delete Trainings/Workshops/ Demonstrations Attended</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113443')">3.1.1.3.4.4.3	How to Delete Projects Assisted</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113444')">3.1.1.3.4.4.4	How to Delete Papers Submitted for Publication</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113445')">3.1.1.3.4.4.5	How to Delete Students Mentored</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113446')">3.1.1.3.4.4.6	How to Delete Curriculum/Course Developed</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113447')">3.1.1.3.4.4.7	How to Delete Network and Linkages</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113448')">3.1.1.3.4.4.8	How to Delete Research and Development</a></li>
										    	 		<li class="active"><a href="#" v-on="click: getli('adminmod4113449')">3.1.1.3.4.4.9	How to Delete Others</a></li>
										    	 	</ul>
								    	 	</ul>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod41135')">3.1.1.3.5	How to Add Reports Submitted</a></li>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod41136')">3.1.1.3.6	How to Update Reports Submitted</a></li>
						    	 		<li class="active"><a href="#" v-on="click: getli('adminmod41137')">3.1.1.3.7	How to Add Assigned Secretariat</a></li>
						    	 	</ul>
					    </ul>
					    
				    <li class="active" data-toggle="collapse" data-target="#submenu42" class="collapsed"><a href="#" v-on="click: getli('adminmod42')">3.2 Reports</a></li>
				    	<ul class="sub-menu collapse" id="submenu42">
				    		<li><a href="#" v-on="click: getli('adminmod421')">3.2.1 How to Generate and Print Reports</a></li>
				    		<li><a href="#" v-on="click: getli('adminmod422')">3.2.2 How to Save Reports</a></li>
				    	</ul>
				    <li class="active" data-toggle="collapse" data-target="#submenu43" class="collapsed"><a href="#" v-on="click: getli('adminmod43')">3.3 Libraries</a></li>
				    	<ul class="sub-menu collapse" id="submenu43">
				    		<li class="active" data-toggle="collapse" data-target="#submenu431" class="collapsed"><a href="#" v-on="click: getli('adminmod431')">3.3.1	How to Manage Area of Expertise</a></li>
						    	<ul class="sub-menu collapse" id="submenu431">
						    		<li><a href="#" v-on="click: getli('adminmod431001')">3.3.1.1	How to Add Area of Expertise</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod431002')">3.3.1.2	How to View & Update Area of Expertise</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod431003')">3.3.1.3	How to Search Area of Expertise</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod431004')">3.3.1.4	How to Delete Area of Expertise</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu432" class="collapsed"><a href="#" v-on="click: getli('adminmod432')">3.3.2	How to Manage Citizenship</a></li>
						    	<ul class="sub-menu collapse" id="submenu432">
						    		<li><a href="#" v-on="click: getli('adminmod4321')">3.3.2.1	How to Add Citizenship</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4322')">3.3.2.2	How to View & Update Citizenship</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4323')">3.3.2.3	How to Search Citizenship</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4324')">3.3.2.4	How to Delete Citizenship</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu433" class="collapsed"><a href="#" v-on="click: getli('adminmod433')">3.3.3	How to Manage Courses</a></li>
						    	<ul class="sub-menu collapse" id="submenu433">
						    		<li><a href="#" v-on="click: getli('adminmod4331')">3.3.3.1	How to Add Courses</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4332')">3.3.3.2	How to View & Update Courses</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4333')">3.3.3.3	How to Search Courses</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4334')">3.3.3.4	How to Delete Courses</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu434" class="collapsed"><a href="#" v-on="click: getli('adminmod434')">3.3.4	How to Manage DOST Outcomes</a></li>
						    	<ul class="sub-menu collapse" id="submenu434">
						    		<li><a href="#" v-on="click: getli('adminmod4341')">3.3.4.1	How to Add DOST Outcomes</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4342')">3.3.4.2	How to View & Update DOST Outcomes</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4343')">3.3.4.3	How to Search DOST Outcomes</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4344')">3.3.4.4	How to Delete DOST Outcomes</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu435" class="collapsed"><a href="#" v-on="click: getli('adminmod435')">3.3.5	How to Manage DOST Priority Areas</a></li>
						    	<ul class="sub-menu collapse" id="submenu435">
						    		<li><a href="#" v-on="click: getli('adminmod4351')">3.3.5.1	How to Add DOST Priority Areas</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4352')">3.3.5.2	How to View & Update DOST Priority Areas</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4353')">3.3.5.3	How to Search DOST Priority Areas</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4354')">3.3.5.4	How to Delete DOST Priority Areas</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu436" class="collapsed"><a href="#" v-on="click: getli('adminmod436')">3.3.6	How to Manage Educational Levels</a></li>
						    	<ul class="sub-menu collapse" id="submenu436">
						    		<li><a href="#" v-on="click: getli('adminmod4361')">3.3.6.1	How to Add Educational Levels</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4362')">3.3.6.2	How to View & Update Educational Levels</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4363')">3.3.6.3	How to Search Educational Levels</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4364')">3.3.6.4	How to Delete Educational Levels</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu437" class="collapsed"><a href="#" v-on="click: getli('adminmod437')">3.3.7	How to Manage Fields (in Activities and Accomplishments)</a></li>
						    	<ul class="sub-menu collapse" id="submenu437">
						    		<li><a href="#" v-on="click: getli('adminmod4371')">3.3.7.1	How to Add Fields (in Activities and Accomplishments)</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4372')">3.3.7.2	How to View & Update Fields (in Activities and Accomplishments)</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4373')">3.3.7.3	How to Search Fields (in Activities and Accomplishments)</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4374')">3.3.7.4	How to Delete Fields (in Activities and Accomplishments)</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu438" class="collapsed"><a href="#" v-on="click: getli('adminmod438')">3.3.8	How to Manage Global Regions</a></li>
						    	<ul class="sub-menu collapse" id="submenu438">
						    		<li><a href="#" v-on="click: getli('adminmod4381')">3.3.8.1	How to Add Global Regions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4382')">3.3.8.2	How to View & Update Global Regions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4383')">3.3.8.3	How to Search Global Regions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4384')">3.3.8.4	How to Delete Global Regions</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu439" class="collapsed"><a href="#" v-on="click: getli('adminmod439')">3.3.9	How to Manage Host Institution</a></li>
						    	<ul class="sub-menu collapse" id="submenu439">
						    		<li><a href="#" v-on="click: getli('adminmod4391')">3.3.9.1	How to Add Host Institution</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4392')">3.3.9.2	How to View & Update Host Institution</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4393')">3.3.9.3	How to Search Host Institution</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod4394')">3.3.9.4	How to Delete Host Institution</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu4310" class="collapsed"><a href="#" v-on="click: getli('adminmod4310')">3.3.10 How to Manage Local Regions</a></li>
						    	<ul class="sub-menu collapse" id="submenu4310">
						    		<li><a href="#" v-on="click: getli('adminmod43101')">3.3.10.1 How to Add Local Regions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43102')">3.3.10.2 How to View & Update Local Regions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43103')">3.3.10.3 How to Search Local Regions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43104')">3.3.10.4 How to Delete Local Regions</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu4311" class="collapsed"><a href="#" v-on="click: getli('adminmod4311')">3.3.11 How to Manage Monitoring Councils</a></li>
						    	<ul class="sub-menu collapse" id="submenu4311">
						    		<li><a href="#" v-on="click: getli('adminmod43111')">3.3.11.1 How to Add Monitoring Councils</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43112')">3.3.11.2 How to View & Update Monitoring Councils</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43113')">3.3.11.3 How to Search Monitoring Councils</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43114')">3.3.11.4 How to Delete Monitoring Councils</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu4312" class="collapsed"><a href="#" v-on="click: getli('adminmod4312')">3.3.12 How to Manage Participants</a></li>
						    	<ul class="sub-menu collapse" id="submenu4312">
						    		<li><a href="#" v-on="click: getli('adminmod43121')">3.3.12.1 How to Add Participants</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43122')">3.3.12.2 How to View & Update Participants</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43123')">3.3.12.3 How to Search Participants</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43124')">3.3.12.4 How to Delete Participants</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu4313" class="collapsed"><a href="#" v-on="click: getli('adminmod4313')">3.3.13 How to Manage Professions</a></li>
						    	<ul class="sub-menu collapse" id="submenu4313">
						    		<li><a href="#" v-on="click: getli('adminmod43131')">3.3.13.1 How to Add Professions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43132')">3.3.13.2 How to View & Update Professions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43133')">3.3.13.3 How to Search Professions</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43134')">3.3.13.4 How to Delete Professions</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu4314" class="collapsed"><a href="#" v-on="click: getli('adminmod4314')">3.3.14 How to Manage Specializations</a></li>
						    	<ul class="sub-menu collapse" id="submenu4314">
						    		<li><a href="#" v-on="click: getli('adminmod43141')">3.3.14.1 How to Add Specialization</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43142')">3.3.14.2 How to View & Update Specializations</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43143')">3.3.14.3 How to Search Specializations</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43144')">3.3.14.4 How to Delete Specializations</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu4315" class="collapsed"><a href="#" v-on="click: getli('adminmod4315')">3.3.15 How to Manage Titles</a></li>
						    	<ul class="sub-menu collapse" id="submenu4315">
						    		<li><a href="#" v-on="click: getli('adminmod43151')">3.3.15.1 How to Add Titles</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43152')">3.3.15.2 How to View & Update Titles</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43153')">3.3.15.3 How to Search Titles</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43154')">3.3.15.4 How to Delete Titles</a></li>
						    	</ul>
						    <li class="active" data-toggle="collapse" data-target="#submenu4316" class="collapsed"><a href="#" v-on="click: getli('adminmod4316')">3.3.16 How to Manage User Accounts</a></li>
						    	<ul class="sub-menu collapse" id="submenu4316">
						    		<li><a href="#" v-on="click: getli('adminmod43161')">3.3.16.1 How to Add User Accounts</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43162')">3.3.16.2 How to View & Update User Accounts</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43163')">3.3.16.3 How to Search User Accounts</a></li>
						    		<li><a href="#" v-on="click: getli('adminmod43164')">3.3.16.4 How to Delete User Accounts</a></li>
						    	</ul>
				    	</ul>
			    </ul>
		</ul>
	</div>
</div>