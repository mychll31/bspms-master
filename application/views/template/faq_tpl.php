<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h4 class="modal-title" id="myModalLabel">Frequently Asked Questions (FAQs)</h4>
</div>
<div class="modal-body">
	
	<div>
		<?php include 'faq_body.php';?>
	</div>

</div>
<div class="modal-footer">
	<!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
</div>