
    <!-- Bootstrap -->
    <link href="<?=base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css')?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?=base_url('assets/vendors/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?=base_url('assets/production/css/custom.css')?>" rel="stylesheet">
    <style type="text/css">
    .help-block{
        color: #a94442;
    }
    h1 {
      font-family: "Brush Script MT", cursive;
      font-size: 55px;
      font-style: normal;
      font-variant: normal;
      font-weight: bolder;
      line-height: 26.4px;
      text-align: center;
      line-height: 1em;
      color: #4a67ae;
      text-shadow: -3px 0 #f9f9f9, 0 3px #cecece;
    }
    body{
  		background-image:url('../assets/images/Login.png') !important;
  		background-repeat: no-repeat !important;
  		background-position: right bottom !important;
  		background-attachment: fixed !important;
		}
    input.form-control {
      border-radius: 0 !important;
    }
    .loginForm {
      background-color: white;
      padding: 20px;
      margin: 0 auto;
      width: 450px;
    }
    div {
	    margin: auto;
	    font-size: 20px;
	    width: 50%;
	}
    </style>
  </head>

  <body style="background:#F7F7F7;">
  	<br><br><br>
  	<br><br>
  	<div align="center">
  		<h1>OOPS!</h1>
  		<div>We can't seem to find the page you're looking for. Please try again, and if it still doesn't work, let us know.</div>
  		<br>
      <h1><?php echo $heading; ?></h1>
      <?php echo $message; ?>
  		<a href="javascript:history.go(-1)" class="btn btn-warning btn-lg">Go Back</a>
  		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  		<a href="<?=base_url('logout')?>" class="btn btn-default btn-lg">Logout</a>
  	</div>
</body>