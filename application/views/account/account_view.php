<?php

echo $strNotification;

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">

</head>
<body>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
	<div class="x_content">
	    <form class="form-horizontal form-label-left" action="<?=base_url('account/update')?>" method="post" novalidate>
    	    <span class="section">Profile</span>
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Name <span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                	<input id="txtName" class="form-control col-md-7 col-xs-12" name="txtName" value="" required type="text">
                </div>
            </div>
            
            <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>          
         </form>
     </div>
   </div>
</div>
</body>
</html>