<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
table tbody td:last-child{
    background-color: #fff !important; 
}
td.sorting {
    display: none;
}
</style>
<div id="userAccount" v-cloak>
<input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
<input type="hidden" v-model="userid" value="<?=$this->uri->segment(3)?>">
<?php echo isset($strNotification)?$strNotification:""; ?>
<?php
$this->load->library('encrypt'); 

if(count($usersEdit)>0):
	$form =  base_url('users/edit');
	$button = '<button id="send" type="submit" class="btn btn-primary" name="btnSubmitUsers" value="edit" v-on="click: getValidate">Save</button>';
	// $usrname = $usersEdit[0]['usr_user'];
  $fname = $usersEdit[0]['usr_fname'];
  $mname = $usersEdit[0]['usr_mname'];
  $lname = $usersEdit[0]['usr_lname'];
	$usrlogin = $usersEdit[0]['usr_user_login']; 
	$usrpassword = $usersEdit[0]['usr_user_passwd'];
	$usrlevel = $usersEdit[0]['usr_user_level'];
  $usercouncil = $usersEdit[0]['usr_council'];
  $active = $usersEdit[0]['usr_isactive']; 
	

	$hidden = '<input type="hidden" name="txtUsrId" value="'.$usersEdit[0]['usr_user_id'].'">';
else:
	$form = base_url('users/add');
	$button = '<button id="send" type="submit" class="btn btn-primary" name="btnSubmitUsers" value="add" v-on="click: getValidate">Save</button>';
	// $usrname = "";
  $fname = '';
  $mname = '';
  $lname = '';
	$usrlogin = "";
	$usrpassword = "";
	$usrlevel = "";
  $usercouncil = "";
	$hidden = "";
endif; 
?>
  <div class="col-md-12 col-sm-12 col-xs-12">  
    <div class="x_panel">
  	<div class="x_content">
  	    <form class="form-horizontal form-label-left" action="<?=$form?>" method="post" novalidate>
      	    <span class="section">User Accounts</span>

              <div class="item form-group {{ haserror.fname }}">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fname">Firstname <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="txtfname" class="form-control col-md-7 col-xs-12" v-model="fname" name="txtfname" maxlength="150"  value="<?=$fname?>" type="text">
                    <span class="help-block" v-if="ErrorValidation.fname">{{ Error.fname }}</span>
                  </div>
              </div>

              <div class="item form-group {{ haserror.mname }}">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="mname">Middlename <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="txtmname" class="form-control col-md-7 col-xs-12" v-model="mname" name="txtmname" maxlength="2"  value="<?=$mname?>" type="text">
                    <span class="help-block" v-if="ErrorValidation.mname">{{ Error.mname }}</span>
                  </div>
              </div>

              <div class="item form-group {{ haserror.lname }}">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lname">Lastname <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="txtlname" class="form-control col-md-7 col-xs-12" v-model="lname" name="txtlname" maxlength="150"  value="<?=$lname?>" type="text">
                    <span class="help-block" v-if="ErrorValidation.lname">{{ Error.lname }}</span>
                  </div>
              </div>			
              <hr>
              <div class="item form-group {{ haserror.username }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                  	<input id="txtUserLogin" class="form-control col-md-7 col-xs-12" v-model="username" name="txtUserLogin" maxlength="20"  value="<?=$usrlogin?>" type="text">
                    <span class="help-block" v-if="ErrorValidation.username">{{ Error.username }}</span>
                  </div>
              </div>			

              <div class="item form-group {{ haserror.password }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Password <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12"> 
				  
                  	<input id="txtUserPassword" type="password" class="form-control col-md-7 col-xs-12" v-model="password" name="txtUserPassword" maxlength="20"  value="<?=$this->encrypt->decode($usrpassword); ?>" >
					<?php // echo "Encrypt: " .$usrpassword; ?>
					<?php // echo "Decrypt: " .$this->encrypt->decode($usrpassword); ?>
                    <span class="help-block" v-if="ErrorValidation.password">{{ Error.password }}</span>
                  </div> 
              </div>		
  			
  			<?php if(count($usersEdit)<0) {	?> 
              <div class="item form-group">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Re-type Password<span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                  	<input id="txtUserPassword2" type="password" class="form-control col-md-7 col-xs-12" name="txtUserPassword2" maxlength="50"  value="<?=$usrpassword2?>" >
                  </div> 
              </div>			
  			<?php } ?>

  			 <div class="form-group {{ haserror.accesslevel }}">
              	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Access Level <span class="required">*</span></label>
                   <div class="col-sm-6">
                      <select name="cmbUserLevel" class="form-control" v-model="accesslevel">
                          <option value="">Choose Option</option>
                          <option value="1" <?php echo $usrlevel == "1"?"selected":"";?>>System Administrator</option>
                          <option value="2" <?php echo $usrlevel == "2"?"selected":"";?>>BSP Staff</option>
                          <option value="3" <?php echo $usrlevel == "3"?"selected":"";?>>Councils</option>   
                      </select>
                      <span class="help-block" v-if="ErrorValidation.accesslevel">{{ Error.accesslevel }}</span>
                   </div> 
              </div>

          <div class="form-group {{ haserror.council }}" v-if="accesslevel==3">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Monitoring Council <span class="required">*</span></label>
              <div class="col-sm-6">
                <select name="cmbCouncil" class="form-control" v-model="council">
                  <option value="">Choose Option</option>
                  <?php foreach($this->arrTemplateData['councils'] as $council): ?>
                  <option value="<?=$council['cil_id']?>" <?=($council['cil_id']==$usercouncil) ? 'selected' : ''?>><?=$council['cil_desc']?></option>
                  <?php endforeach; ?>
                </select>
                <span class="help-block" v-if="ErrorValidation.council">{{ Error.council }}</span>
              </div> 
          </div>

          <?php if(count($usersEdit)>0): ?>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"></label>
              <div class="col-sm-6">
                <input type="checkbox" name="active" <?=($active == 1 ? 'checked' : '')?>> &nbsp;<b>Active</b>
              </div> 
          </div>
          <?php endif; ?>
               
  	
              <div class="ln_solid"></div>
                        <div class="form-group"> 
                          <div class="col-md-6 col-md-offset-3">
  						  <a href="<?=base_url().'users/index'?>" class="btn btn-default">Cancel</a>                          
                            <?=$button?>
                            <?=$hidden?> 
                          </div>
                        </div>          
           </form>
       </div>
     </div>
      <div class="x_panel"> 
      <h4>User Accounts</h4> 
      <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
              <th>No.</th> 
              <th>Employee Name</th>
              <th>Username</th> 
              <th>Access Level</th>
              <th>Monitoring Council</th>
              <th>Status</th>
              <td></td>
          </tr>
        </thead>
        <tbody>
            <?php $ctr = 1;	foreach($arrUsers as $users): if($this->uri->segment(3) != $users['usr_user_id']): ?>
              <tr data-id="<?=$users['usr_user_id']?>">
                  <td style="width: 10px"><?=$ctr++?></td>
                  <td><?=getFullname($users['usr_fname'], $users['usr_lname'], '', $users['usr_mname'])?></td>
                  <td><?=$users['usr_user_login'];?></td>
                  <td><?php if ($users['usr_user_level'] == 1)      echo "System Administrator";
                            elseif ($users['usr_user_level'] == 2)  echo "BSP Staff";
                            elseif ($users['usr_user_level'] == 3) 	echo "Council";
                            else 	  echo " - ";
                      ?>
                  </td>
                  <td><?=$users['cil_code'];?></td>
                  <td>
                    <?php
                      if($users['usr_isactive'] == 1)
                        echo '<span class="label label-primary">Active</span>';
                      else
                        echo '<span class="label label-warning">Inactive</span>';
                    ?>
                  </td>
                  <td nowrap style="width: 60px;" align="center">
                    <a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a>
                  </td>
              </tr>
            <?php endif; endforeach; ?>
        </tbody>
      </table>
    </div>   
  </div>
</div>
<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('users/delete')?>" method="post">
                <input type="hidden" name="txtempid" id="txtempid"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_useraccount.js')?>"></script>

<script>
  $(document).ready(function() {
    // delete scientist
    $("tr td:last-child").click(function(){
        $('#txtempid').val($(this).closest("tr").data("id"));
    });
    // data tables
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
    setTimeout(function() { $(".alert").alert('close'); }, 3000);
    $("tr td:not(:last-child)").click(function(){
        window.location = "<?=base_url().'users/index/'?>" + $(this).closest("tr").data("id");
        // console.log($(this).closest("tr").data("id"));
    });
    
  });
</script>
</script>
