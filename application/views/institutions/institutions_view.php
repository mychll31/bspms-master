<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
table tbody td:last-child{
    background-color: #fff !important; 
}
td.sorting {
    display: none; 
}
.help-block {
	color: ;
}
</style>
<?=isset($strNotification) ? $strNotification : ""; ?>
<?php 
	$local_regions = array(
                ''   => 'Choose Option',
                '1'  => 'NCR',
                '2'  => 'Region I',
                '3'  => 'CAR',
                '4'  => 'Region II',
                '5'  => 'Region III',
                '6'  => 'Region IV-A',
                '7'  => 'Region IV-B',
                '8'  => 'Region V',
                '9'  => 'Region VI',
                '10' => 'Region VII',
                '11' => 'Region VIII',
                '12' => 'Region IX',
                '13' => 'Region X',
                '14' => 'Region XI',
                '15' => 'Region XII',
                '16' => 'Region XIII',
                '17' => 'Region NIR',
                '18' => 'ARMM',
  	);
	$itype 	= '';
	$locreg = '';
	$code 	= '';
	$desc 	= '';
	if($this->uri->segment(2) == 'edit'){
		$itype 	 = $arrInst[0]['ins_itype_id'];
		$locreg  = $arrInst[0]['ins_loc_id'];
		$code 	 = $arrInst[0]['ins_code'];
		$desc 	 = $arrInst[0]['ins_desc'];
	}

 ?>
<div id="institutions" v-cloak>
	<div class="col-md-12 col-sm-12 col-xs-12">  
	  <div class="x_panel">
	    <div class="x_content">
	      <form class="form-horizontal form-label-left" action="{{ action }}" method="post">
	        <span class="section">Host Institutions</span>
	        <input type="hidden" v-model="insid" name="txtinsid">
	        <div class="form-group {{ haserror.insType }}">
	        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Institution Type<span class="required">*</span></label>
	        	<div class="col-sm-3">
	        	  <select name="cmbInsType" class="form-control" v-model="insType" >
	        	  	<option value="">Choose Option</option>
	        	    <?php foreach($arrInstitutionTypes as $instypes): ?>
	        	      <option value="<?=$instypes["itype_id"]?>" <?=($instypes['itype_id']==$itype)?'selected':''?>><?=$instypes["itype_desc"];?></option>
	        	    <?php endforeach; ?>
	        	  </select>
	        	  <span class="help-block" v-if="ErrorValidation.insType">{{ Error.insType }}</span>
	        	</div>
	       	</div>

	       	<div class="form-group {{ haserror.locRegion }}">
	        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Local Region<span class="required">*</span></label>
	        	<div class="col-sm-3">
	        	  <select name="cmbLocRegion" class="form-control" v-model="locRegion">
	        	    <?php foreach($local_regions as $key=>$region): ?>
	        	      <option value="<?=$key?>"><?=$region?></option>
	        	    <?php endforeach; ?>
	        	  </select>
	        	  <span class="help-block" v-if="ErrorValidation.locRegion">{{ Error.locRegion }}</span>
	        	</div>
	       	</div>

	       	<div class="form-group {{ haserror.code }}" style="padding-left: 100px;">
	        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Code<span class="required">*</span></label>
	        	<div class="col-sm-6">
	        	  <input id="txtCode" class="form-control" name="txtCode" v-model="code" maxlength="50" type="text">
	        	  <span class="help-block" v-if="ErrorValidation.code">{{ Error.code }}</span>
	        	</div>
	       	</div>

	       	<div class="form-group {{ haserror.desc }}" style="padding-left: 100px;">
	        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description<span class="required">*</span></label>
	        	<div class="col-sm-6">
	        	  <input id="txtDesc" class="form-control" name="txtDesc" v-model="desc" type="text">
	        	  <span class="help-block" v-if="ErrorValidation.desc">{{ Error.desc }}</span>
	        	</div>
	       	</div>

	          <div class="ln_solid"></div>
	          <div class="form-group">
	            <div class="col-md-6 col-md-offset-3">
	              <a class="btn btn-default" href="<?=base_url().'institutions'?>">Cancel</a>
	              <button type="submit" class="btn btn-primary" name="btnSubmitInstitution" v-on="click: getValidate">Save</button>
	            </div>
	          </div>

	      </form>
	     </div>
	   </div>
	</div>

	<input type="hidden" value="<?=base_url()?>" v-model="baseUrl">
	<div class="x_panel">
		<h4>Host Institutions</h4> 
	    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
	      <thead>
	        <tr>
	        	<th>No.</th>
	        	<th>Type</th>
	        	<th>Code</th>
	        	<th>Description</th>
	        	<th>Local Region</th>
	        	<td></td>
	        </tr> 
	      </thead>
	      <tbody>
	      	<?php $no=1; foreach($this->arrTemplateData['institutionEdit'] as $inst): ?>
	      	<tr v-on="click: getRowValue(<?=$inst['ins_id']?>)">
	      		<td style="width:20px"><?=$no++?></td>
	      		<td><?=$inst['itype_desc']?></td>
	      		<td><?=$inst['ins_code']?></td>
	      		<td><?=$inst['ins_desc']?></td>
	      		<td><?=$inst['loc_reg_name']?></td>
	      		<td style="width:20px" align="center"><a data-toggle="modal" data-target="#deleteModal" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
	      	</tr>
	      	<?php endforeach; ?>
	      </tbody>
		</table>
	</div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form method="post" action="<?=base_url('institutions/delete')?>">
                <input type="hidden" id="target_id" name="target_id">
                Are you sure you want to delete this data <span id="atachment"></span>?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_library.js')?>"></script>

<script>
  $(document).ready(function() { 
    // delete scientist
    $("tr td:last-child").click(function(){
        $('#target_id').val($(this).closest("tr").data("id"));
        console.log('asdf')
    });
    // data tables
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
    setTimeout(function() { $(".alert").alert('close'); }, 2000);
    
  });
</script>