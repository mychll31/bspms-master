<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
table tbody td:last-child{
    background-color: #fff !important; 
}
td.sorting {
    display: none;
}
</style>
<?php echo isset($strNotification)?$strNotification:""; ?>
<?php 
if(count($globalregionsEdit)>0):
	$form =  base_url('globalregions/edit');
	$button = '<button id="send" type="submit" class="btn btn-primary" name="btnSubmitGlobalregions" value="edit" v-on="click: getValidate">Save</button>';
	$continent = $globalregionsEdit[0]['glo_continent'];
	$country = $globalregionsEdit[0]['glo_country'];
	$hidden = '<input type="hidden" name="txtGloId" value="'.$globalregionsEdit[0]['glo_id'].'">';
else:
	$form = base_url('globalregions/add');
	$button = '<button id="send" type="submit" class="btn btn-primary" name="btnSubmitGlobalregions" value="add" v-on="click: getValidate">Save</button>';
	$continent = "";
	$country = '';
	$hidden = "";
endif;

$continents = array(
    ''  => 'Choose Option',
    '1' => 'Africa',
    '2' => 'Asia',
    '3' => 'Australia',
    '4' => 'Europe',
    '5' => 'Middle East',
    '6' => 'North America',
    '7' => 'Oceania',
    '8' => 'South America'
  );
?>
<div id="globalregions" v-cloak>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_content">
      <form class="form-horizontal form-label-left" action="{{ action }}" method="post" novalidate>
        <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
        <input type="hidden" v-model='globalid' name="txtGloId">
        <span class="section">Global Regions</span>
        <div class="form-group {{ haserror.continent }}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Continent<span class="required">*</span></label>
          <div class="col-sm-3">
            <select name="cmbContinent" class="form-control" v-model="continent" >
              <?php foreach($continents as $key=>$cont): ?>
                <option value="<?=$key?>" <?=('{{ textType }}' == $cont) ? 'selected' : ''?>><?=$cont?></option>
              <?php endforeach; ?>
            </select>
            <span class="help-block" v-if="ErrorValidation.continent">{{ Error.continent }}</span>
          </div>
        </div>
        <div class="item form-group {{ haserror.country }}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country<span class="required">*</span></label>
          <div class="col-sm-3">
            <input id="txtCountry" class="form-control col-md-7 col-xs-12"  maxlength="50" v-model="country" name="txtCountry" maxlength="50" type="text">
            <span class="help-block" v-if="ErrorValidation.country">{{ Error.country }}</span>
          </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <a class="btn btn-default" href="<?=base_url().'globalregions'?>">Cancel</a>
              <button type="submit" class="btn btn-primary" name="btnSubmitGlobalregions" value="edit" v-on="click: getValidate">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="x_panel">
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>No.</th>
          <th>Continent</th>
          <th>Country</th>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; foreach($this->arrTemplateData['arrGlobalregions'] as $global): ?>
          <tr v-on="click: getRowValue(<?=$global['glo_id']?>)">
                <td style="width:10px"><?=$no++?></td>
                <td><?=$global['glo_cont_name']?></td>
                <td><?=$global['glo_country']?></td>
                <td style="width: 10px !important;" align="center"><a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
              </tr>
          <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('globalregions/delete')?>" method="post">
                <input type="hidden" name="txtdelglobal" id="txtdelglobal"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_library.js')?>"></script>

<script>
  $(document).ready(function() {
    // delete scientist
    // $("tr td:last-child").click(function(){
    //     $('#txtdelglobal').val($(this).closest("tr").data("id"));
    // });
    // data tables
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
    setTimeout(function() { $(".alert").alert('close'); }, 2000);
    // $("tr td:not(:last-child)").click(function(){
    //     window.location = "<?=base_url().'globalregions/index/'?>" + $(this).closest("tr").data("id");
    // });
    
  });
</script>