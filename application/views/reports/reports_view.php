<div id="elreport" v-cloak>
	<form id="frmReport" method="get">
	<input type="hidden" value="<?=base_url()?>" v-model="baseUrl" name="">
	<div class="form-group {{ haserror.report }}">
		<label class="control-label">Report Type</label>
		<?php $arrReports[''][''] = 'Choose Option'; ?>
	    <?=form_dropdown('txtReport',$arrReports,'','id="txtReport" class="form-control" v-model="report"')?>
	    <span class="help-block" v-if="ErrorValidation.report">{{ Error.report }}</span>
	</div>
	<div class="form-group opt-radbox-2 {{ haserror.optrad2 }}">
		<label class="radio-inline"><input type="radio" name="optrad2" v-model="optrad2" id="chrono" value="bychrono">Chronological Order</label>
	    <label class="radio-inline"><input type="radio" name="optrad2" v-model="optrad2" id="perphase" value="byperphase">Alphabetical Order per Phase</label>
	    <label class="radio-inline"><input type="radio" name="optrad2" v-model="optrad2" id="percateg" value="bypercateg">Alphabetical Order per Category</label>
	    <span class="help-block" v-if="ErrorValidation.optrad2">{{ Error.optrad2 }}</span>
	</div>
	<div class="form-group opt-radbox {{ haserror.radyrper }}">
		<label class="radio-inline"><input type="radio" name="optradio" id="byyr" value="byyear" v-model="radyrper">By Year</label>
	    <label class="radio-inline"><input type="radio" name="optradio" id="byper" value="byperiod" v-model="radyrper">By Period</label>
	    <span class="help-block" v-if="ErrorValidation.radyrper">{{ Error.radyrper }}</span>
	</div>
	<div class="form-group opt-scientist {{ haserror.scientistname }}">
		<label class="control-label">Scientist</label>
	    <select id="txtScientist" v-model="scientistname" name="txtScientist" class="form-control">
	    	<option value="">Choose Option</option>
	    	<?php foreach($arrScientists as $sciname): ?>
	    		<option value="<?=$sciname['sci_id']?>" data-council="<?=$sciname['usr_council']?>"><?=getFullname($sciname['sci_last_name'], $sciname['sci_first_name'], $sciname['sci_middle_initial'], $sciname['sci_middle_initial']).$sciname['sci_ext_name']?></option>
	    	<?php endforeach; ?>
	    </select>
	    <span class="help-block" v-if="ErrorValidation.scientistname">{{ Error.scientistname }}</span>
	</div>
	<div class="div-council">
		<?php if($_SESSION['sessCouncilid']==''): ?>
		<label class="control-label">ADMIN </label>
	    <input type="checkbox" name="chkadmin" id="chkadmin">
	    &nbsp;
		<?php endif; ?>
		<?php if($_SESSION['sessCouncilid']=='1' or $_SESSION['sessCouncilid']==''): ?>
	    <label class="control-label">PCAARD </label>
	    <input type="checkbox" name="chkpcaard" id="chkpcaard">
	    &nbsp;
	    <?php endif; ?>
		<?php if($_SESSION['sessCouncilid']=='2' or $_SESSION['sessCouncilid']==''): ?>
	    <label class="control-label">PCHRD </label>
	    <input type="checkbox" name="chkpchrd" id="chkpchrd">
	    &nbsp;
	    <?php endif; ?>
		<?php if($_SESSION['sessCouncilid']=='3' or $_SESSION['sessCouncilid']==''): ?>
	    <label class="control-label">PCIEERD </label>
	    <input type="checkbox" name="chkpcieerd" id="chkpcieerd">
		<?php endif; ?>
	</div>
	<div class="form-group opt-year {{ haserror.yr }}">
		<label class="control-label">Year</label>
	    <select id="txtYear1" name="txtYear" id="txtYear" class="form-control" v-model="yr">
	    	<option value="">Choose year</option>
	    	<option v-repeat="yr: edate" value="{{ yr }}">{{ yr }}</option>
	    </select>
	    <span class="help-block" v-if="ErrorValidation.yr">{{ Error.yr }}</span>
	</div>
	<div class="form-group opt-year2 {{ haserror.yr1 }}">
		<label class="control-label">Period</label>
	    <label class="control-label">From</label>
	    <select id="txtYear1" name="txtYear1" class="form-control" v-model="yr1">
	    	<option value="">Choose year</option>
	    	<option v-repeat="yr: edate" value="{{ yr }}">{{ yr }}</option>
	    </select>
	    <span class="help-block" v-if="ErrorValidation.yr1">{{ Error.yr1 }}</span>
	</div>
	<div class="form-group opt-year2 {{ haserror.yr2 }}">
	    <label class="control-label">To</label>
	    <select id="txtYear2" name="txtYear2" class="form-control" v-model="yr2">
	    	<option value="">Choose year</option>
	    	<option v-repeat="yr: approval_date" value="{{ yr }}">{{ yr }}</option>
	    </select>
	    <span class="help-block" v-if="ErrorValidation.yr2">{{ Error.yr2 }}</span>
	</div>
	<div class="div-council">
	    <label class="control-label">Export to Excel </label>
	    <input type="checkbox" name="excel" id="excel">
	</div>
	<div class="form-group">
		<a class="btn btn-primary" href="#" id="btnPrint" target="_new" v-on="click: getValidate"><i class="fa fa-print"></i> Print</a>
	</div>
	</form>
</div>
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/others.js')?>"></script>

<script>
$(document).ready(function(e) {
	var council = "<?=$_SESSION['sessCouncilid']?>";
	$('.opt-scientist,.opt-year,.opt-year2, .opt-institution, .opt-radbox, .opt-radbox-2').hide();
	
    $('#btnPrint').click(function(){
		$rpt=$('#txtReport').val();
		$data='?timestamp=<?=time()?>';
		//$data=$('#frmReport').serializeArray();
		//var values = {};
		$.each($('#frmReport').serializeArray(), function(i, field) {
			//values[field.name] = field.value;
			$data+='&'+field.name+'='+field.value;
		});
		if($('#excel').is(':checked')){
			$('#btnPrint').attr('href','<?=base_url('reports/exportToExcel')?>/'+$rpt+$data);
		}else{
			$('#btnPrint').attr('href','<?=base_url('reports/generate')?>/'+$rpt+$data);
		}
	});

    if(council=='')
    	$('#chkadmin, #chkpcaard, #chkpcieerd, #chkpchrd').attr('checked','checked');

    $('#chkadmin').click(function() {
		if($(this).is(':checked')){
			$('#txtScientist [data-council=""]').show();
		}else{
			$('#txtScientist [data-council=""]').hide();
		}
	});

	$('#chkpcaard').click(function() {
		if($(this).is(':checked')){
			if(council=='1')
				$('#txtScientist [data-council!="1"]').hide();
			else
				$('#txtScientist [data-council="1"]').show();

		}else{
			if(council=='1')
				$('#txtScientist [data-council!="1"]').show();
			else
				$('#txtScientist [data-council="1"]').hide();
		}
	});

	$('#chkpchrd').click(function() {
		if($(this).is(':checked')){
			if(council=='2')
				$('#txtScientist [data-council!="2"]').hide();
			else
				$('#txtScientist [data-council="2"]').show();

		}else{
			if(council=='2')
				$('#txtScientist [data-council!="2"]').show();
			else
				$('#txtScientist [data-council="2"]').hide();
		}
	});

	$('#chkpcieerd').click(function() {
		if($(this).is(':checked')){
			if(council=='3')
				$('#txtScientist [data-council!="3"]').hide();
			else
				$('#txtScientist [data-council="3"]').show();

		}else{
			if(council=='3')
				$('#txtScientist [data-council!="3"]').show();
			else
				$('#txtScientist [data-council="3"]').hide();
		}
	});

});
</script>
