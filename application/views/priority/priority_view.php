<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
table tbody td:last-child{
    background-color: #fff !important; 
}
td.sorting {
    display: none; 
}
</style>
<div id="priorities" v-cloak>

<?php echo isset($strNotification)?$strNotification:""; ?>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_content">
      <form class="form-horizontal form-label-left" action="{{ action }}" method="post" novalidate>
        <input type="hidden" v-model="baseUrl" value="<?=base_url()?>">
        <input type="hidden" v-model='priid' name="txtSpeId">
        <span class="section">DOST Priority Areas</span>
          <div class="form-group {{ haserror.order }}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Order<span class="required">*</span></label>
            <div class="col-sm-3">
              <input id="txtCode" class="form-control col-md-7 col-xs-12" v-model="order" name="txtCode" maxlength="3" type="text">
              <span class="help-block" v-if="ErrorValidation.order">{{ Error.order }}</span>
            </div> 
          </div>
          <div class="item form-group {{ haserror.desc }}">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Description<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="txtDesc" class="form-control col-md-7 col-xs-12" v-model="desc" name="txtDesc" maxlength="50" type="text">
              <span class="help-block" v-if="ErrorValidation.desc">{{ Error.desc }}</span>
            </div>
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
              <a class="btn btn-default" href="<?=base_url().'priority'?>">Cancel</a>
              <button type="submit" class="btn btn-primary" name="btnSubmitCitizen" value="edit" v-on="click: getValidate">Save</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="x_panel"> 
    <h4>DOST Priority Areas</h4> 
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th width="10px">No.</th>
          <th>Order</th>
          <th>Description</th>
          <td></td>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; foreach($this->arrTemplateData['arrPriority'] as $priority): ?>
        <tr v-on="click: getRowValue(<?=$priority['pri_id']?>)">
          <td style="width:10px"><?=$no++?></td>
          <td style="width:10px"><?=$priority['pri_number']?></td>
          <td><?=$priority['pri_desc']?></td>
          <td style="width: 10px !important;" align="center"><a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('priority/delete')?>" method="post">
                <input type="hidden" name="txtdelprio" id="txtdelprio"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_library.js')?>"></script>

<script>
  $(document).ready(function() { 
    // delete scientist
    // $("tr td:last-child").click(function(){
    //     $('#txtdelprio').val($(this).closest("tr").data("id"));
    // });
    // data tables
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
    setTimeout(function() { $(".alert").alert('close'); }, 2000);
    // $("tr td:not(:last-child)").click(function(){
    //     window.location = "<?=base_url().'priority/index/'?>" + $(this).closest("tr").data("id");
    // });
    
  });
</script>
