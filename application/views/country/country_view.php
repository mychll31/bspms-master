<?php echo isset($strNotification)?$strNotification:""; ?>
<?php
if(count($countryEdit)>0):
	$form =  base_url('country/edit');
	$button = '<button id="send" type="submit" class="btn btn-success" name="btnSubmitCountry" value="edit">Update</button>';
	$continent = $countryEdit[0]['glo_continent'];
	$country = $countryEdit[0]['glo_country'];
	$hidden = '<input type="hidden" name="txtGloId" value="'.$countryEdit[0]['glo_id'].'">';
else:
	$form = base_url('country/add');
	$button = '<button id="send" type="submit" class="btn btn-success" name="btnSubmitCountry" value="add">Add</button>';
	$continent = "";
	$country = '';
	$hidden = "";

endif; 
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8"> 

</head>
<body> 
<div class="col-md-12 col-sm-12 col-xs-12"> 
  <div class="x_panel">
	<div class="x_content">
	    <form class="form-horizontal form-label-left" action="<?=$form?>" method="post" novalidate>
    	    <span class="section">Country of Origins</span>

			 <div class="form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Continent<span class="required">*</span></label>
                 <div class="col-md-9 col-sm-9 col-xs-12">
                    <select name="cmbContinent" class="form_control" required>
                        <option></option>
                        <option value="Africa" <?php echo $continent == "Africa"?"selected":"";?>>Africa</option>
                        <option value="Asia" <?php echo $continent == "Asia"?"selected":"";?>>Asia</option>
                        <option value="Australia" <?php echo $continent == "Australia"?"selected":"";?>>Australia</option>   
                        <option value="Europe" <?php echo $continent == "Europe"?"selected":"";?>>Europe</option>
                        <option value="Middle East" <?php echo $continent == "Middle East"?"selected":"";?>>Middle East</option>
                        <option value="North America" <?php echo $continent == "North America"?"selected":"";?>>North America</option>
                        <option value="Oceania" <?php echo $continent == "Oceania"?"selected":"";?>>Oceania</option>
                        <option value="South America" <?php echo $continent == "North America"?"selected":"";?>>South America</option>
                    </select>
                 </div> 
            </div>
			 
	       
            <div class="item form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Country<span class="required">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                	<input id="txtCountry" class="form-control col-md-7 col-xs-12" name="txtCountry" maxlength="50"  value="<?=$country?>" required type="text">
                </div>
            </div>			
             
	
            <div class="ln_solid"></div>
                      <div class="form-group"> 
                        <div class="col-md-6 col-md-offset-3">
                          <?=$button?>
                          <?=$hidden?> 
						  <button type="submit" class="btn btn-primary">Cancel</button>                          
                        </div>
                      </div>          
         </form>
     </div>
   </div>
    <div class="x_panel"> 
    <h4>Country of Origins</h4> 
    <table class="table table-striped"> 
     <tr> 
      <th><strong>Continent</strong></th>
      <th><strong>Country</strong></th> 
      <th></th>
      <th></th>
    </tr>
	<tbody>
     <?php foreach($arrCountry as $country)
	 {?>
     <tr>
	 
	 
        <td ><?php echo $country['glo_continent'];?></td>
        <td><?php echo $country['glo_country'];?></td>
		<td><a href="<?=base_url('country/index/'.$country["glo_id"])?>"><i class="fa fa-edit"></i></a></td>
		<td><a href="#" onClick="deleteCountry(<?=$country["glo_id"]?>,'<?=base_url('')?>','<?=$country["glo_country"]?>');"><i class="fa fa-trash"></i></a></td>
 
      </tr>    
     <?php }?>  
	</tbody>  
   </table>

   </div>
   
</div>
</body>
</html>

<script>
function deleteCountry(id,strUrl,name)
{
	var ans = confirm('Are you sure you want to delete '+name+'?');
	if(ans)
	{
		//alert(strUrl+"project/deleteinstaller/"+id);
		$.ajax ({
			type: "GET",
			url: strUrl+"country/delete/"+id,
			cache: false,
			success: function(html)
				{
					//alert(html);
					window.location.replace(strUrl+"country");
					//$(location).attr(strUrl+'/user/');
				} 
		});	
	}
}
</script>
</script>