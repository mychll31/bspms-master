<script src="<?=base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js')?>"></script>
<link href="<?=base_url('assets/production/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
<style>
.daterangepicker{z-index:1151 !important;}
table tr {
    cursor: pointer;
}
table tbody tr.highlight td {
    background-color: #ccc;
}
table tbody tr:hover{
    background-color: #ccc !important; 
}
table tbody td:last-child{
    background-color: #fff !important; 
}
td.sorting {
    display: none;
}
</style>
<?php
echo isset($strNotification)?$strNotification:"";
$local_regions = array(
                ''   => 'Choose Option',
                '1'  => 'NCR',
                '2'  => 'Region I',
                '3'  => 'CAR',
                '4'  => 'Region II',
                '5'  => 'Region III',
                '6'  => 'Region IV-A',
                '7'  => 'Region IV-B',
                '8'  => 'Region V',
                '9'  => 'Region VI',
                '10' => 'Region VII',
                '11' => 'Region VIII',
                '12' => 'Region IX',
                '13' => 'Region X',
                '14' => 'Region XI',
                '15' => 'Region XII',
                '16' => 'Region XIII',
                '17' => 'Region NIR',
                '18' => 'ARMM',
  );
?>
<div id="localregions" v-cloak>
<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_content">
      <form class="form-horizontal form-label-left" action="{{ action }}" method="post" novalidate>
        <span class="section">Local Regions</span>
        <input type="hidden" v-model="locregid" name="txtLocId">
        <div class="form-group {{ haserror.region }}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Regions<span class="required">*</span></label>
          <div class="col-sm-3">
            <select name="cmbRegion" class="form-control" v-model="region">
              <?php foreach($local_regions as $key=>$local):?>
                  <option value="<?=$key?>"><?=$local?></option>
              <?php endforeach;?>
            </select>
            <span class="help-block" v-if="ErrorValidation.region">{{ Error.region }}</span>
          </div>
        </div>
        <div class="item form-group {{ haserror.city }}">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Province<span class="required">*</span></label>
          <div class="col-sm-3">
            <input id="txtProvince" class="form-control col-md-7 col-xs-12" name="txtProvince" maxlength="50" type="text" v-model="city">
            <span class="help-block" v-if="ErrorValidation.city">{{ Error.city }}</span>
          </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-3">
            <a class="btn btn-default" href="<?=base_url().'localregions'?>">Cancel</a>
            <button type="submit" class="btn btn-primary" name="btnSubmitCitizen" value="edit" v-on="click: getValidate">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="x_panel">
    <h4>Local Regions</h4>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th>No.</th>
        <th>Region</th>
        <th>Province</th>
        <td></td>
      </tr>
    </thead>
    <tbody>
      <?php $no=1; foreach($this->arrTemplateData['arrLocalregions'] as $localregion): ?>
      <tr v-on="click: getRowValue(<?=$localregion['loc_id']?>)">
        <td style="width:10px"><?=$no++?></td>
        <td><?=$localregion['loc_reg_name']?></td>
        <td><?=$localregion['loc_province']?></td>
        <td style="width: 10px" align="center"><a data-toggle="modal" data-target="#deleteModalEmp" href="#"><i style="font-size: 20px;" class="fa fa-trash-o"></i></a></td>
      </tr>
    <?php endforeach; ?>
	</tbody>
</table>
</div>
</div>
</div>

<!-- Begin Delete Modal -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="deleteModalEmp">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-body">
            <form action="<?=base_url('localregions/delete')?>" method="post">
                <input type="hidden" name="txtdelregion" id="txtdelregion"></input>
                Are you sure you want to delete this data?
                <br><br>
                <button type="submit" class="btn btn-success" id="btndupdate">Yes </button>
                <button class="btn btn-default" id="btndupdate" data-dismiss="modal">No </button>
            </form>
        </div>
    </div>
  </div>
</div>
<!-- End Delete Modal -->

<!-- vuejs -->
<script src="<?=base_url('assets/vuejs/vendor.js')?>"></script>
<script src="<?=base_url('assets/vuejs/validation_library.js')?>"></script>

<script>
  $(document).ready(function() {
    // delete scientist
    // $("tr td:last-child").click(function(){
    //     $('#txtdelregion').val($(this).closest("tr").data("id"));
    // });
    // data tables
    setTimeout(function(){$('#example, #example1').DataTable();}, 0);
    setTimeout(function() { $(".alert").alert('close'); }, 2000);
    // $("tr td:not(:last-child)").click(function(){
    //     window.location = "<?=base_url().'localregions/index/'?>" + $(this).closest("tr").data("id");
    // });
    
  });
</script>