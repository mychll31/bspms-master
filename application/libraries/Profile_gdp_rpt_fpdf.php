<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    require_once APPPATH.'third_party/fpdf/fpdf-1.7.php';

    class Profile_gdp_rpt_fpdf extends FPDF {

        public function __construct()
        {
            parent::__construct();
        }

        function Footer()
        {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,$this->PageNo(),0,0,'R');
        }

    }