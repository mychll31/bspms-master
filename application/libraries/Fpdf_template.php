<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    require_once APPPATH.'third_party/fpdf/fpdf-1.7.php';

    class Fpdf_template extends FPDF {

        function Header()
        {
            $CI =& get_instance();
            $CI->load->model(array('reports_model'));

            $yearDur = ($_GET['txtYear1'] == $_GET['txtYear2']) ? $_GET['txtYear1'] : $_GET['txtYear1'].' - '.$_GET['txtYear2'];
            $this->SetFont('Arial','B',11);

            $this->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
            if($_GET['txtReport']==14){
                $this->Cell(0,5,"List of BSP Awardees with Completed Service Obligations",0,1,'C');
                $this->Cell(0,5," (Total of Short Term + Long Term) in CY (".$yearDur.")",0,1,'C');
            }else{
                $this->Cell(0,5,getReportName($_GET['txtReport'])." (".$yearDur.")",0,1,'C');
            }

            $this->Ln(5);

            // COLUMN HEADER
            $this->setFillColor(230,230,230);
            $this->SetFont('Arial','B',9);

            if($_GET['txtReport']==3){
                $this->Cell(20,5,"Count",1,0,'C',1);
                if(!empty($this->headerFemale)){  
                    $this->Cell(60,5,"Name of Female Balik Scientist",'LTB',0,'R',1);
                    $this->SetFont('Arial','',6);
                    $this->Cell(10,5,"[2]",'RTB',0,'L',1);
                }else{
                    $this->Cell(60,5,"Name of Male Balik Scientist",'LTB',0,'R',1);
                    $this->SetFont('Arial','',6);
                    $this->Cell(10,5,"[1]",'RTB',0,'L',1);
                }
                $this->SetFont('Arial','B',9);
                $this->Cell(50,5,"Area of Expertise",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(20,5,"[3]",'RTB',1,'L',1);
            }if($_GET['txtReport']==4){
                $this->SetX(45);
                $this->Cell(35,5,"Profession",'LT',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(35,5,"[1]",'RT',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(20,5,'Total No.','TL',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(10,5,"[2]",'RT',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(10,5,'%','TL',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(10,5,"[3]",'RT',1,'L',1);

                $this->SetFont('Arial','B',9);
                $this->SetX(45);
                $this->Cell(70,5,'','BLR',0,'C',1);
                $this->Cell(30,5,'of Awardees','BLR',0,'C',1);
                $this->Cell(20,5,'','BLR',1,'C',1);
                $this->SetX(45);
            }if($_GET['txtReport']==5 or $_GET['txtReport']==7){
                $this->Cell(20,5,'Count',1,0,'C',1);
                $this->Cell(60,5,"Name of Balik Scientist",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(15,5,"[1]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(45,5,'Profession','TLB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(25,5,"[2]",'RTB',1,'L',1);
            }if($_GET['txtReport']==6){
                $this->SetX(45);
                $this->Cell(55,5,"Professional License",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(15,5,"[1]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(20,5,"Total",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(10,5,"[2]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(15,5,"%",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(15,5,"[3]",'RTB',1,'L',1);
                $this->SetX(45);
            }if($_GET['txtReport']==8){
                $this->Cell(45,5,"Area of Expertise",'LT',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(15,5,"[1]",'RT',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(36,5,'No. of Balik Scientists','TL',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(5,5,"[2]",'RT',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(50,5,'Name of Balik Scientist','TL',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(10,5,"[3]",'RT',1,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(60,5,'','BLR',0,'C',1);
                $this->Cell(41,5,'per Area of Expertise','BLR',0,'C',1);
                $this->Cell(60,5,'','BLR',1,'C',1);
            }if($_GET['txtReport']==9){
                $this->Cell(50,5,"Local Region - Province",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(20,5,"[1]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(45,5,'No. of Balik Scientists','TLB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(15,5,"[2]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(10,5,'%','TLB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(10,5,"[3]",'RTB',1,'L',1);
            }if($_GET['txtReport']==10){
                $this->Cell(50,5,"Global Region",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(20,5,"[1]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(45,5,'No. of Balik Scientists','TLB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(15,5,"[2]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',9);
                $this->Cell(10,5,'%','TLB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(10,5,"[3]",'RTB',1,'L',1);
            }if($_GET['txtReport']==11){
                $this->SetFont('Arial','B',7);
                $this->Cell(16,5,"Date of",'LTR',0,'C',1);
                $this->Cell(24,5,"Name of",'LTR',0,'C',1);
                $this->Cell(24,5,"Host",'LTR',0,'C',1);

                $this->SetFont('Arial','B',7);
                $this->Cell(50,5,"Type of Host Institution (classification 1)",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(6,5,"[4]",'RTB',0,'L',1);

                $this->SetFont('Arial','B',7);
                $this->Cell(50,5,"Type of Host Institution (classification 2)",'LTB',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(6,5,"[5]",'RTB',0,'L',1);
                $this->Ln();

                $this->SetFont('Arial','B',7);
                $this->Cell(16,5,"Approval",'RL',0,'C',1);
                $this->Cell(24,5,"Balik",'RL',0,'C',1);
                $this->Cell(18,5,"Institution",'RL',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(6,5,"[3]",'',0,'L',1);
                $this->SetFont('Arial','B',7);

                $this->Cell(11,5,"Up",'RL',0,'C',1);
                $this->Cell(10,5,"SUCs",'RL',0,'C',1);
                $this->Cell(10,5,"LGUs/",'RL',0,'C',1);
                $this->Cell(11,5,"PUCs",'RL',0,'C',1);
                $this->Cell(14,5,"NGOs/",'RL',0,'C',1);
                $this->Cell(11,5,"Up+",'RL',0,'C',1);
                $this->Cell(16,5,"LGUs/",'RL',0,'C',1);
                $this->Cell(11,5,"PUCs",'RL',0,'C',1);
                $this->Cell(18,5,"NGOs/",'RL',0,'C',1);
                $this->Ln();

                $this->Cell(11,5,"letter",'L',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(5,5,"[1]",'',0,'L',1);
                $this->SetFont('Arial','B',7);

                $this->Cell(18,5,"Scientist",'L',0,'R',1);
                $this->SetFont('Arial','',6);
                $this->Cell(6,5,"[2]",'',0,'L',1);
                $this->SetFont('Arial','B',7);

                $this->Cell(24,5,"",'RL',0,'C',1);
                $this->Cell(11,5,"System",'RL',0,'C',1);
                $this->Cell(10,5,"",'RL',0,'C',1);
                $this->Cell(10,5,"GS",'RL',0,'C',1);
                $this->Cell(11,5,"",'RL',0,'C',1);
                $this->Cell(14,5,"Founda",'RL',0,'C',1);
                $this->Cell(11,5,"SUCs",'RL',0,'C',1);
                $this->Cell(16,5,"Govern",'RL',0,'C',1);
                $this->Cell(11,5,"",'RL',0,'C',1);
                $this->Cell(18,5,"Foundation/",'RL',0,'C',1);
                $this->Ln();

                $this->Cell(16,5,"",'LR',0,'C',1);
                $this->Cell(24,5,"",'LR',0,'C',1);
                $this->Cell(24,5,"",'LR',0,'C',1);
                $this->Cell(11,5,"",'LR',0,'C',1);
                $this->Cell(10,5,"",'LR',0,'C',1);
                $this->Cell(10,5,"",'LR',0,'C',1);
                $this->Cell(11,5,"",'LR',0,'C',1);
                $this->Cell(14,5,"tion/",'LR',0,'C',1);
                $this->Cell(11,5,"",'LR',0,'C',1);
                $this->Cell(16,5,"ment",'LR',0,'C',1);
                $this->Cell(11,5,"",'LR',0,'C',1);
                $this->Cell(18,5,"Private",'LR',0,'C',1);
                $this->Ln();

                $this->Cell(16,5,"",'LRB',0,'C',1);
                $this->Cell(24,5,"",'LRB',0,'C',1);
                $this->Cell(24,5,"",'LRB',0,'C',1);
                $this->Cell(11,5,"",'LRB',0,'C',1);
                $this->Cell(10,5,"",'LRB',0,'C',1);
                $this->Cell(10,5,"",'LRB',0,'C',1);
                $this->Cell(11,5,"",'LRB',0,'C',1);
                $this->Cell(14,5,"Private",'LRB',0,'C',1);
                $this->Cell(11,5,"",'LRB',0,'C',1);
                $this->Cell(16,5,"Sector",'LRB',0,'C',1);
                $this->Cell(11,5,"",'LRB',0,'C',1);
                $this->Cell(18,5,"",'LRB',0,'C',1);
                $this->Ln();
            }if($_GET['txtReport']==12){
                if($this->PageNo() !== 1 and $this->isnewPage() !== 2):
                    $this->SetFont('Arial','B',8);
                    $this->Cell(12,5,'Count','LTR',0,'C',1);
                    $this->Cell(22,5,'Date Approved','LTR',0,'C',1);
                    $this->Cell(22,5,'Name of Balik','LTR',0,'C',1);
                    $this->Cell(23,5,'Area of','LTR',0,'C',1);
                    $this->Cell(19,5,'DOST','LTR',0,'C',1);
                    $this->Cell(15,5,'DOST','LTR',0,'C',1);
                    $this->Cell(18,5,'Host','LTR',0,'C',1);
                    $this->Cell(30,5,'Duration','LTR',0,'C',1);
                    $this->Cell(18,5,'Council','LTR',1,'C',1);
                    $this->Cell(12,5,'','LRB',0,'C',1);
                    $this->Cell(22,5,'','LRB',0,'C',1);
                    $this->Cell(22,5,'Scientist','LRB',0,'C',1);
                    $this->Cell(23,5,'Expertise','LRB',0,'C',1);
                    $this->Cell(19,5,'Priority Area','LRB',0,'C',1);
                    $this->Cell(15,5,'Outcomes','LRB',0,'C',1);
                    $this->Cell(18,5,'Institution/s','LRB',0,'C',1);
                    $this->Cell(30,5,'','LRB',0,'C',1);
                    $this->Cell(18,5,'','LRB',1,'C',1);
                endif;
            }if($_GET['txtReport']==13){
                if($this->PageNo() !== 1 and $this->isnewPage() !== 2):
                    $this->SetFont('Arial','B',8);
                    $this->Cell(11,5,'Count','LTR',0,'C',1);
                    $this->Cell(32,5,'Name of','LTR',0,'C',1);
                    $this->Cell(30,5,'Area of Expertise','LTR',0,'C',1);
                    $this->Cell(21,5,'DOST Priority','LTR',0,'C',1);
                    $this->Cell(19,5,'DOST','LTR',0,'C',1);
                    $this->Cell(23,5,'Host','LTR',0,'C',1);
                    $this->Cell(38,5,'Duration','LTR',1,'C',1);
                    $this->Cell(11,5,'','LBR',0,'C',1);
                    $this->Cell(32,5,'Balik Scientis','LBR',0,'C',1);
                    $this->Cell(30,5,'','LBR',0,'C',1);
                    $this->Cell(21,5,'Area','LBR',0,'C',1);
                    $this->Cell(19,5,'Outcomes','LBR',0,'C',1);
                    $this->Cell(23,5,'Institution/s','LBR',0,'C',1);
                    $this->Cell(38,5,'','LBR',1,'C',1);
                endif;
            }if($_GET['txtReport']==14){
                if($this->PageNo() !== 1 and $this->isnewPage() !== 2):
                    $this->SetFont('Arial','B',8);
                    $this->Cell(12,5,'Count','TLR',0,'C',1);
                    $this->Cell(28,5,'Name of Balik','TLR',0,'C',1);
                    $this->Cell(24,5,'Area of','TLR',0,'C',1);
                    $this->Cell(21,5,'DOST','TLR',0,'C',1);
                    $this->Cell(19,5,'DOST','TLR',0,'C',1);
                    $this->Cell(23,5,'Host','TLR',0,'C',1);
                    $this->Cell(36,5,'Duration','TLR',1,'C',1);
                    $this->Cell(12,5,'','BLR',0,'C',1);
                    $this->Cell(28,5,'Scientist','BLR',0,'C',1);
                    $this->Cell(24,5,'Expertise','BLR',0,'C',1);
                    $this->Cell(21,5,'Priority Area','BLR',0,'C',1);
                    $this->Cell(19,5,'Outcomes','BLR',0,'C',1);
                    $this->Cell(23,5,'Institution/s','BLR',0,'C',1);
                    $this->Cell(36,5,'','BLR',1,'C',1);
                endif;
            }
        }

        function Footer()
        {
           $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,$this->PageNo(),0,0,'R');
        }

    }