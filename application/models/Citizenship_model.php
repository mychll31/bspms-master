<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Citizenship_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database(); 
	}
	
	public function getCitizenship($citizenId)
	{
		$this->db->where("cit_id", $citizenId);
		$query = $this->db->get('tblcitizenship');
		return $query->result_array();
	}
	 
 
   	public function getAll()
	{
		$query = $this->db->order_by('cit_type asc');
		$this->db->order_by("if(cit_particulars = 'Others',1,0),cit_particulars");
		$query = $this->db->get('tblcitizenship');
		return $query->result_array();
	}
  	

  	public function getValidCit()
	{ 
		$citid = $this->uri->segment(3);
		$this->db->select("concat(cit_type, '-', cit_particulars) as valcit");
		$this->db->from('tblcitizenship');
		if($citid!='')
			$this->db->where('cit_id !=', $citid);
		$query = $this->db->get();
		return $query->result_array(); 
	}

 	public function addCitizenship($arrCitizenship)
	{
		$this->db->insert('tblcitizenship', $arrCitizenship);
		return $this->db->insert_id();	
		
	} 
	
	public function editCitizenship($citizenId,$arrCitizen)
	{
		$this->db->where('cit_id', $citizenId);
		$this->db->update('tblcitizenship', $arrCitizen);
		return $this->db->affected_rows(); 	
	}
	
	public function deleteCitizenship($citizenId)
	{
		$this->db->where('cit_id', $citizenId);
		$this->db->delete('tblcitizenship'); 
		return $this->db->affected_rows(); 	
	}

	public function getCitizenCode($citizenId)
	{
		$this->db->select('cit_particulars');
		$this->db->from('tblcitizenship'); 
		$this->db->where('cit_id !=', $citizenId);
		$query = $this->db->get();
		return $query->result_array();
	}

 
  	
}