<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function getCountry($countryId=0)
	{

		$this->db->where("glo_id", $countryId); 
		$this->db->order_by('glo_id','ASC'); 
		$query = $this->db->get('tblglobalregions');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{ 
		$this->db->order_by('glo_country','ASC');
		$query = $this->db->get('tblglobalregions'); 	 	
		return $query->result_array(); 
		 
	}

	public function getCountriesList()
	{ 
		$query = $this->db->get('tblcountries'); 	 	
		return $query->result_array(); 
	}

	public function getCountCon()
	{ 
		$this->db->select("concat(glo_country, '-', glo_continent) as councon");
		$this->db->from('tblglobalregions'); 	 	
		$query = $this->db->get();
		return $query->result_array(); 
		 
	}
 
 	public function addCountry($arrCountry)
	{ 
		$this->db->insert('tblglobalregions', $arrCountry);
		return $this->db->insert_id();	
		 
	}
	
	public function editCountry($countryId,$arrCountry)
	{
		$this->db->where('glo_id', $countryId);  
		$this->db->update('tblglobalregions', $arrCountry);
		return $this->db->affected_rows(); 	
	}
	
	public function deleteCountry($countryId)
	{
		$this->db->where('glo_id', $countryId);
		$this->db->delete('tblglobalregions');
		return $this->db->affected_rows(); 	
	}
 
  	
}