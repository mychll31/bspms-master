<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expertise_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function getExpertise($expertiseId)
	{

		$this->db->where("exp_id", $expertiseId);  
		$this->db->order_by('exp_desc','ASC'); 
		$query = $this->db->get('tblexpertise');
		return $query->result_array();
	}
	 
  
   public function getAll()
	{
		$this->db->order_by('exp_desc','ASC');
		$query = $this->db->get('tblexpertise'); 	 	
		return $query->result_array();
		
	}
  
 	public function addExpertise($arrExpertise)
	{
		$this->db->insert('tblexpertise', $arrExpertise); 
		return $this->db->insert_id();	
		 
	}
	
	public function editExpertise($expertiseId,$arrExpertise)
	{
		$this->db->where('exp_id', $expertiseId);  
		$this->db->update('tblexpertise', $arrExpertise);
		return $this->db->affected_rows(); 	
	}
	 
	public function deleteExpertise($expertiseId)
	{
		$this->db->where('exp_id', $expertiseId);
		$this->db->delete('tblexpertise');
		return $this->db->affected_rows(); 	
	}

	public function getExpertiseNumber($expertiseId)
	{
		$this->db->select('exp_desc');
		$this->db->from('tblexpertise');
		$this->db->where('exp_id !=', $expertiseId);
		$query = $this->db->get();
		return $query->result_array();
	}

 
  	
}