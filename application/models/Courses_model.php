<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Courses_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
 	}
	
	public function getCourse($courseId=0)
	{
		$this->db->order_by('cou_desc','ASC');
		$this->db->where("cou_id", $courseId); 
		$query = $this->db->get('tblcourses');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{
		$this->db->order_by('cou_id','ASC');
		$query = $this->db->get('tblcourses'); 	 	
		return $query->result_array(); 
		 
	}
 
 	public function addCourse($arrCourse)
	{
		$this->db->insert('tblcourses', $arrCourse);
		return $this->db->insert_id();	
		 
	}
	
	public function editCourse($courseId,$arrCourse)
	{
		$this->db->where('cou_id', $courseId);  
		$this->db->update('tblcourses', $arrCourse);
		return $this->db->affected_rows(); 	
	}
	
	public function deleteCourse($courseId)
	{
		$this->db->where('cou_id', $courseId);
		$this->db->delete('tblcourses');
		return $this->db->affected_rows(); 	
	}
 	
 	public function getCourdesc($couid)
	{
		$this->db->select('cou_desc');
		$this->db->from('tblcourses');
		$this->db->where('cou_id !=', $couid);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidCourse()
	{ 
		$this->db->select("concat(cou_code,'-',cou_desc) as valcou");
		$this->db->from('tblcourses');
		$query = $this->db->get();
		return $query->result_array(); 
	}
  	
}