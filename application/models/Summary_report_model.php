<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary_report_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function serviceCount($year){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		if($query->num_rows()>0)
			return $query->num_rows();
		else
			return '';
	}

	public function HeadCount($year){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->group_by('srv_sci_id');
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		if($query->num_rows()>0)
			return $query->num_rows();
		else
			return '';
	}

	public function term($year, $term, $status){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->where('srv_typeofaward', $term);
		$this->db->where('srv_status', $status);
		$this->db->group_by('srv_sci_id');
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		if($query->num_rows()>0)
			return $query->num_rows();
		else
			return '';
	}

	public function status_noterm($year, $status){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->where('srv_status', $status);
		$this->db->group_by('srv_sci_id');
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		if($query->num_rows()>0)
			return $query->num_rows();
		else
			return '';
	}

	public function status_changesched($year, $status){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->where('srv_change_sched', $status);
		$this->db->group_by('srv_sci_id');
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function gender($year, $gender){
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('sci_gender', $gender);
		$this->db->where('srv_isdeleted', 0);
		$this->db->join('tblsciservice', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$query = $this->db->get('tblscientist');
		return $query->num_rows();
	}

	public function count_summary_iter($year, $id, $field){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		if($field=='itype_id'){
			$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tblsciservice.srv_ins_id', 'left');
			$this->db->join('tblinstitutiontypes', 'tblinstitutiontypes.itype_id = tblinstitutions.ins_itype_id', 'left');
		}
		$this->db->where($field, $id);
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function totalCount_summary_iter($intyr1, $intyr2, $id, $field){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intyr1."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intyr2."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->where($field, $id);
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function class_new($year, $term){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->where('srv_typeofaward', $term);
		$this->db->group_by('srv_sci_id');
		$this->db->order_by('created_at asc');
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getStaff(){
		$this->db->from('tblusers');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function class_subs($year, $term){
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$year."-01-01'");
		$this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$year."-12-31'");
		$this->db->where('srv_isdeleted', 0);
		$this->db->where('srv_typeofaward', $term);
		$this->db->from('tblsciservice');
		$query = $this->db->get();
		
		if($query->num_rows()>0)
			return $query->num_rows()-1;
		else
			return '';
	}
  	
}