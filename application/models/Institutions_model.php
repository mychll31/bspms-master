<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Institutions_model extends CI_Model { 
 
	public function __construct()
	{
           $this->load->database();  
 	}
	
	public function getInstitutionTypes()
	{
		$this->db->order_by('itype_desc','ASC'); 
		$query = $this->db->get('tblinstitutiontypes');
		return $query->result_array(); 
	}

	public function getInstitutionList()
	{
		$this->db->order_by("if(ins_desc = 'Others',1,0),ins_desc"); 
		$query = $this->db->get('tblinstitutions');
		return $query->result_array(); 
	}

	public function getLocalRegions()
	{
		$this->db->order_by('loc_region','ASC');
		$this->db->order_by('loc_province','ASC');
		$query = $this->db->get('tbllocalregions'); 
		return $query->result_array(); 
	}
 
   public function getAll() 
	{

			$sql = "SELECT tblinstitutions.*, tblinstitutiontypes.itype_desc, tbllocalregions.loc_region, tbllocalregions.loc_province
						FROM tblinstitutions
							INNER JOIN tblinstitutiontypes ON tblinstitutions.ins_itype_id = tblinstitutiontypes.itype_id
							INNER JOIN tbllocalregions ON tblinstitutions.ins_loc_id = tbllocalregions.loc_id
						ORDER BY tblinstitutions.ins_desc ASC";	

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getInstitutions($id=0)
	{ 
	
		if($id==0):
			$sql = "SELECT tblinstitutions.*, tblinstitutiontypes.itype_desc, tbllocalregions.loc_region, tbllocalregions.loc_province
						FROM tblinstitutions
							INNER JOIN tblinstitutiontypes ON tblinstitutions.ins_itype_id = tblinstitutiontypes.itype_id
							INNER JOIN tbllocalregions ON tblinstitutions.ins_loc_id = tbllocalregions.loc_id";	
		else:   
			$sql = "SELECT tblinstitutions.*, tblinstitutiontypes.itype_desc, tbllocalregions.loc_region, tbllocalregions.loc_province
						FROM tblinstitutions
							INNER JOIN tblinstitutiontypes ON tblinstitutions.ins_itype_id = tblinstitutiontypes.itype_id
							INNER JOIN tbllocalregions ON tblinstitutions.ins_loc_id = tbllocalregions.loc_id 
						WHERE tblinstitutions.ins_id='".$id."'";
		endif;		
		$query = $this->db->query($sql);
		return $query->result_array();
		
	}
	
 
 	public function addInstitution($arrInstitution)
	{
		$this->db->insert('tblinstitutions', $arrInstitution);
		return $this->db->insert_id();	
		 
	}
	
	public function editInstitution($institutionId,$arrInstitution)
	{
		$this->db->where('ins_id', $institutionId);  
		$this->db->update('tblinstitutions', $arrInstitution);
		return $this->db->affected_rows(); 	
	}
	
	public function deleteInstitution($institutionId)
	{
		$this->db->where('ins_id', $institutionId); 
		$this->db->delete('tblinstitutions');
		return $this->db->affected_rows(); 	
	}
 	
 	public function getValidHost()
	{ 
		$this->db->select("concat(ins_code, '-', ins_desc) as valhost");
		$this->db->from('tblinstitutions');
		$query = $this->db->get();
		return $query->result_array(); 
	}
  	
}