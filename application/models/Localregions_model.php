<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Localregions_model extends CI_Model { 
 
	public function __construct()
	{
           $this->load->database();   
	}
	
	public function getLocalregions($localregionsId) 
	{ 

		$this->db->where("loc_id", $localregionsId); 
		$this->db->order_by('loc_region','ASC'); 
		$this->db->order_by('loc_province','ASC'); 
		$query = $this->db->get('tbllocalregions');
		return $query->result_array();
	}
	 
 
   public function getAll()  
	{ 
		$this->db->order_by('loc_region','ASC');
		$this->db->order_by('loc_province','ASC'); 		
		$query = $this->db->get('tbllocalregions'); 	 	
		return $query->result_array(); 
		 
	}
 
 	public function addLocalregions($arrLocalregions)	{ 
		$this->db->insert('tbllocalregions', $arrLocalregions);
		return $this->db->insert_id();	
		 
	}
	
	public function editLocalregions($localregionsId,$arrLocalregions)
	{
		$this->db->where('loc_id', $localregionsId);   
		$this->db->update('tbllocalregions', $arrLocalregions);
		return $this->db->affected_rows(); 	
	}
	  
	public function deleteLocalregions($localregionsId)
	{
		$this->db->where('loc_id', $localregionsId);
		$this->db->delete('tbllocalregions');
		return $this->db->affected_rows(); 	
	}

	public function getValidLocal()
	{ 
		$this->db->select("concat(loc_region, '-', loc_province) as vallocal");
		$this->db->from('tbllocalregions');
		$query = $this->db->get();
		return $query->result_array(); 
	}
 
  	
}