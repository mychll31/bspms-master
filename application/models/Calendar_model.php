<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function addEvent($arrCalendar)
	{
		$this->db->insert('tblcalendar', $arrCalendar);
		return $this->db->insert_id();
	}


	public function updateEvent($cal_id, $arrCalendar)
	{
		$this->db->where('cal_id', $cal_id);  
		$this->db->update('tblcalendar', $arrCalendar);
		return $this->db->affected_rows(); 	
	}

	public function getAllEvents()
	{
		$this->db->where('cal_isRemove', 0);  
		$this->db->order_by('cal_dateStart', 'ASC'); 
		$query = $this->db->get('tblcalendar');
		return $query->result_array();	
	}

	public function getLastInsertedEvent($calid)
	{
		$this->db->where('cal_isRemove', 0);  
		$this->db->where('cal_id', $calid);
		$query = $this->db->get('tblcalendar');
		return $query->result_array();		
	}

	public function getEventList()
	{
		$this->db->where('cal_isRemove', 0);
		$this->db->where('cal_dateStart >= CURDATE( )');
		$this->db->order_by('cal_dateStart', 'ASC'); 
		$query = $this->db->get('tblcalendar');
		return $query->result_array();	
	}

	public function getAllEventsByMonth($dateStart)
	{
		$this->db->where('cal_isRemove', 0);
		$this->db->where('cal_dateStart >= DATE_SUB(CURDATE(), INTERVAL -1 WEEK)');
		$this->db->order_by('cal_dateStart', 'ASC');
		$query = $this->db->get('tblcalendar');
		return $query->result_array();

	}

	// public function deleteEvent($cal_id)
	// {
	// 	$this->db->where('cal_id', $cal_id);
	// 	$this->db->delete('tblcalendar');
	// 	return $this->db->affected_rows();
	// }

}