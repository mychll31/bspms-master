<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class License_model extends CI_Model {
 
	public function __construct() 
	{
           $this->load->database();   
	}
	
	public function getLicense($licenseId=0)
	{

		$this->db->where("lic_id", $licenseId);  
		$this->db->order_by('lic_desc','ASC'); 
		$query = $this->db->get('tbllicenses');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{
		$this->db->order_by('lic_desc','ASC');
		$query = $this->db->get('tbllicenses'); 	 	
		return $query->result_array();
		
	}
 
 	public function addLicense($arrLicense)
	{
		$this->db->insert('tbllicenses', $arrLicense);
		return $this->db->insert_id();	
		 
	} 
	
	public function editLicense($licenseId,$arrLicense)
	{
		$this->db->where('lic_id', $licenseId);  
		$this->db->update('tbllicenses', $arrLicense);
		return $this->db->affected_rows(); 
	}
	 
	public function deleteLicense($licenseId)
	{
		$this->db->where('lic_id', $licenseId);
		$this->db->delete('tbllicenses');
		return $this->db->affected_rows(); 	
	}

	public function getLicDesc($licid)
	{
		$this->db->select('lic_desc');
		$this->db->from('tbllicenses');
		$this->db->where('lic_id !=', $licid);
		$query = $this->db->get();
		return $query->result_array();
	}
 
  	
}