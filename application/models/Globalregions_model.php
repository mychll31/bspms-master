<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Globalregions_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();   
	}
	
	public function getGlobalregions($globalregionsId) 
	{ 

		$this->db->where("glo_id", $globalregionsId); 
		$this->db->order_by('glo_id','ASC'); 
		$query = $this->db->get('tblglobalregions');
		return $query->result_array();
	}
	 
 
   	public function getAll()
	{ 
		$this->db->order_by('glo_continent, glo_country','ASC');
		$query = $this->db->get('tblglobalregions'); 	 	
		return $query->result_array(); 
		 
	}

	public function getcountryByContinent($continent)
	{ 
		$this->db->where("glo_continent", $continent); 
		$this->db->order_by('glo_country','ASC'); 
		$query = $this->db->get('tblglobalregions');
		return $query->result_array();
	}
 
 	public function addGlobalregions($arrGlobalregions)	{ 
		$this->db->insert('tblglobalregions', $arrGlobalregions);
		return $this->db->insert_id();	
		 
	}
	
	public function editGlobalregions($globalregionsId,$arrGlobalregions)
	{
		$this->db->where('glo_id', $globalregionsId);   
		$this->db->update('tblglobalregions', $arrGlobalregions);
		return $this->db->affected_rows(); 	
	}
	  
	public function deleteGlobalregions($globalregionsId)
	{
		$this->db->where('glo_id', $globalregionsId);
		$this->db->delete('tblglobalregions');
		return $this->db->affected_rows(); 	
	}

	public function getGlobalCountry($gloid)
	{
		$this->db->select('glo_country');
		$this->db->from('tblglobalregions');
		$this->db->where('glo_id !=', $gloid);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidGlobal()
	{ 
		$this->db->select("concat(glo_continent, '-', glo_country) as valglobal");
		$this->db->from('tblglobalregions');
		$query = $this->db->get();
		return $query->result_array(); 
	}
 
  	
}