<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();   
	}
	
	public function getUsers($usersId=0) 
	{ 

		$this->db->where("usr_user_id", $usersId); 
		$this->db->order_by('usr_fname','ASC');  
		$query = $this->db->get('tblusers');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{ 
		$this->db->where('usr_user_id != 12'); 
		if ($_SESSION['sessAccessLevel'] != 1 )	
			$this->db->where('usr_user_id', $_SESSION['sessUserId']); 

 		$this->db->join('tblcouncils', 'tblcouncils.cil_id = tblusers.usr_council', 'left');
		$this->db->order_by('usr_fname','ASC');
		$query = $this->db->get('tblusers'); 	 	
		return $query->result_array(); 
		 
	}
 
 	public function addUsers($arrUsers)	{ 
		$this->db->insert('tblusers', $arrUsers);
		return $this->db->insert_id();	
		 
	}
	
	public function editUsers($usersId,$arrUsers)
	{
		
		$this->db->where('usr_user_id', $usersId);   
		$this->db->update('tblusers', $arrUsers);
		return $this->db->affected_rows(); 	
	}
	  
	public function deleteUsers($usersId)
	{
		$this->db->where('usr_user_id', $usersId);
		$this->db->delete('tblusers');
		return $this->db->affected_rows(); 	
	}

	public function getUsername($userid)
	{
		$this->db->select('usr_user_login');
		$this->db->from('tblusers');
		$this->db->where('usr_user_login !=', '');
		$this->db->where('usr_user_id !=', $userid);
		$query = $this->db->get();
		return $query->result_array();
	}
  	
}
