<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mancom_model extends CI_Model {
 
	public function __construct() 
	{
           $this->load->database();  
	}
  
   	public function getAll() 
	{	
		$this->db->join('tbltitles', 'tbltitles.tit_id = tblmancom.man_title', 'left');
		$this->db->where("man_isRemove", 0);
		$this->db->order_by('man_lastname','ASC');
		$query = $this->db->get('tblmancom'); 	 	
		return $query->result_array();
	}
 
	public function addMember($arrMancom) 
	{
		$this->db->insert('tblmancom', $arrMancom);
		return $this->db->insert_id();
	}

	public function getMancom($manid)
	{
		$this->db->where("man_id", $manid);
		$query = $this->db->get('tblmancom');
		return $query->result_array();
	}

	public function editMancom($manid,$arrMancom)
	{
		$this->db->where('man_id', $manid);
		$this->db->update('tblmancom', $arrMancom);
		return $this->db->affected_rows(); 	
	} 
  	
}