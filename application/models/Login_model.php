<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends CI_Model {
	public function __construct() 
	{
           	$this->load->database();
			

	}
	
	public function authenticate($strUsername,$strPassword)
	{
		$this->db->where('usr_user_login', $strUsername);
		//$this->db->where('usr_user_passwd', $strPassword);
		$this->db->join('tblcouncils', 'tblcouncils.cil_id = tblusers.usr_council', 'left');
		$query = $this->db->get('tblusers');
		
		return $query->result_array();
	}
	
}