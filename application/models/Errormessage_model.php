<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Errormessage_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function getMessage()
	{
		$this->db->select('err_fieldname, err_message');
		$this->db->from('tblerrormessages');
		$query = $this->db->get();
		return $query->result_array();
	}
  	
}