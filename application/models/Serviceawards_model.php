<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Serviceawards_model extends CI_Model {
	public function __construct()
	{
           $this->load->database();
	}


	public function getServiceAwards($scientistId,$serviceAwardsId=0)
	{
		if($serviceAwardsId==0):
			$this->db->where('srv_sci_id' , $scientistId);
		else:
			$this->db->where('srv_id' , $serviceAwardsId);
		endif;
		$this->db->order_by("if (srv_isyearonly=1, srv_approval_yr, if(srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate)) desc");
		$query = $this->db->get('tblsciservice');
		return $query->result_array();
	}

	public function getServiceAwardsReport($scientistId,$serviceAwardsId=0)
	{
		$this->db->select('*');
		$this->db->from('tblsciservice');
		if($serviceAwardsId==0):
			$this->db->where('srv_sci_id' , $scientistId);
		else:
			$this->db->where('srv_id' , $serviceAwardsId);
		endif;
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tblsciservice.srv_ins_id', 'left');
		$this->db->join('tblcouncils', 'tblcouncils.cil_id = tblsciservice.srv_cil_id', 'left');
		$this->db->join('tblsrvreports', 'tblsrvreports.srp_srv_id = tblsciservice.srv_id', 'left');
		$this->db->order_by("srv_approval_date, srv_approval_yr, srv_dateCreated asc");
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getScientistServiceAwards($serviceAwardsId)
	{
		$this->db->where('srv_id' , $serviceAwardsId);
		$query = $this->db->get('tblsciservice');
		return $query->result_array();
	}
	
	public function countClassification($serviceAwardsId){
		$this->db->select('max(srv_classification) as lastinsertedClass');
		$this->db->where('srv_sci_id' , $serviceAwardsId);
		$query = $this->db->get('tblsciservice');
		return $query->result_array();
	}
	
	public function getContractDates($serviceAwardsId){
		$this->db->where('con_srv_id ', $serviceAwardsId);
		$query = $this->db->get('tblsrvcontractdates');
		return $query->result_array();
	}

	public function getServiceReport($serviceAwardsId)
	{
		$this->db->where('srp_srv_id' , $serviceAwardsId);
		$query = $this->db->get('tblsrvreports');
		return $query->result_array();
		
	}

	// begin Seminars
	public function addSeminar($arrSeminar)
	{
		$this->db->insert('tblsrvseminars',$arrSeminar);
		return $this->db->insert_id();
	}

	public function editSeminar($arrSeminarData,$semId)
	{
		$this->db->where('sem_id', $semId);
		$this->db->update('tblsrvseminars', $arrSeminarData);
		return $this->db->affected_rows(); 
	}

	public function deleteSeminar($semid)
	{
		$this->db->where('sem_id', $semid);
		$this->db->delete('tblsrvseminars');
		return $this->db->affected_rows(); 	
	}

	public function getSeminars($serviceId,$target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvseminars');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvseminars.sem_field', 'left');
		$this->db->join('tblparticipants', 'tblparticipants.par_id = tblsrvseminars.sem_participantstype', 'left');
		$this->db->where('sem_srv_id', $serviceId);
		if($target < 3)
			$this->db->where('sem_target', $target);
		$this->db->order_by('tblsrvseminars.sem_id');
		$query = $this->db->get();
		return $query->result_array();
	}
	// end Seminars

	// begin Training
	public function addTraining($arrTraining)
	{
		$this->db->insert('tblsrvtrainings',$arrTraining);
		return $this->db->insert_id();
	}

	public function editTraining($arrTraining,$traid)
	{
		$this->db->where('tra_id', $traid);
		$this->db->update('tblsrvtrainings', $arrTraining);
		return $this->db->affected_rows(); 
	}

	public function deleteTraining($traid)
	{
		$this->db->where('tra_id', $traid);
		$this->db->delete('tblsrvtrainings');
		return $this->db->affected_rows(); 	
	}

	public function getTrainings($serviceId,$target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvtrainings');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvtrainings.tra_field', 'left');
		$this->db->join('tblparticipants', 'tblparticipants.par_id = tblsrvtrainings.tra_participantstype', 'left');
		$this->db->where('tblsrvtrainings.tra_srv_id', $serviceId);
		if($target < 3)
			$this->db->where('tblsrvtrainings.tra_target', $target);
		$this->db->order_by('tblsrvtrainings.tra_id');
		$query = $this->db->get();
		return $query->result_array();
	}
	// end Training

	// begin Project
	public function getProjects($scientistId, $target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvprojects');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvprojects.prj_field', 'left');
		$this->db->where('tblsrvprojects.prj_srv_id', $scientistId);
		if($target < 3)
			$this->db->where('tblsrvprojects.prj_target', $target);
		$this->db->order_by('tblsrvprojects.prj_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function addProject($arrProject)
	{
		$this->db->insert('tblsrvprojects',$arrProject);
		return $this->db->insert_id();
	}

	public function editProject($arrProjectData,$prjid)
	{
		$this->db->where('prj_id', $prjid);
		$this->db->update('tblsrvprojects', $arrProjectData);
		return $this->db->affected_rows(); 
	}

	public function deleteProject($prjid)
	{
		$this->db->where('prj_id', $prjid);
		$this->db->delete('tblsrvprojects');
		return $this->db->affected_rows(); 	
	}
	// end Project

	// begin paper
	public function getPaper($serviceId, $target)
	{
		$this->db->where('pap_srv_id' , $serviceId);
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvpaper.pap_field', 'left');
		if($target < 3)
			$this->db->where('tblsrvpaper.pap_target', $target);
		$query = $this->db->get('tblsrvpaper');
		return $query->result_array();
	}

	public function addPaper($arrPaper)
	{
		$this->db->insert('tblsrvpaper',$arrPaper);
		return $this->db->insert_id();
	}

	public function editPaper($arrPaper,$pap_id)
	{
		$this->db->where('pap_id', $pap_id);
		$this->db->update('tblsrvpaper', $arrPaper);
		return $this->db->affected_rows(); 
	}

	public function deletePaper($pap_id)
	{
		$this->db->where('pap_id', $pap_id);
		$this->db->delete('tblsrvpaper');
		return $this->db->affected_rows(); 	
	}

	// end paper

	// begin Mentoring
	public function getMentoring($scientistId, $target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvmentoring');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvmentoring.men_field', 'left');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tblsrvmentoring.men_institution', 'left');
		$this->db->where('tblsrvmentoring.men_srv_id', $scientistId);
		if($target < 3)
			$this->db->where('men_target', $target);
		$this->db->order_by('tblsrvmentoring.men_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function addMentor($arrMentor)
	{
		$this->db->insert('tblsrvmentoring',$arrMentor);
		return $this->db->insert_id();
	}

	public function editMentor($arrMentor,$men_id)
	{
		$this->db->where('men_id', $men_id);
		$this->db->update('tblsrvmentoring', $arrMentor);
		return $this->db->affected_rows(); 
	}

	public function deleteMentor($men_id)
	{
		$this->db->where('men_id', $men_id);
		$this->db->delete('tblsrvmentoring');
		return $this->db->affected_rows(); 	
	}

	// end Mentoring

	// begin Curiculum
	public function getCurriculum($serviceId, $target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvcurriculum');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvcurriculum.cur_field', 'left');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tblsrvcurriculum.cur_institution', 'left');
		$this->db->where('cur_srv_id' , $serviceId);
		if($target < 3)
			$this->db->where('cur_target', $target);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function addCurr($arrCurr)
	{
		$this->db->insert('tblsrvcurriculum',$arrCurr);
		return $this->db->insert_id();
	}

	public function editCurr($arrCurr,$cur_id)
	{
		$this->db->where('cur_id', $cur_id);
		$this->db->update('tblsrvcurriculum', $arrCurr);
		return $this->db->affected_rows(); 
	}

	public function deleteCurr($cur_id)
	{
		$this->db->where('cur_id', $cur_id);
		$this->db->delete('tblsrvcurriculum');
		return $this->db->affected_rows(); 	
	}
	// end Curiculum

	// begin Networks
	public function getNetworks($scientistId, $target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvnetworks');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvnetworks.net_field', 'left');
		$this->db->where('tblsrvnetworks.net_srv_id', $scientistId);
		if($target < 3)
			$this->db->where('net_target', $target);
		$this->db->order_by('tblsrvnetworks.net_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function addNetwork($arrNetwork)
	{
		$this->db->insert('tblsrvnetworks',$arrNetwork);
		return $this->db->insert_id();
	}

	public function editNetwork($arrNetwork,$net_id)
	{
		$this->db->where('net_id', $net_id);
		$this->db->update('tblsrvnetworks', $arrNetwork);
		return $this->db->affected_rows(); 
	}

	public function deleteNetwork($net_id)
	{
		$this->db->where('net_id', $net_id);
		$this->db->delete('tblsrvnetworks');
		return $this->db->affected_rows(); 	
	}
	// end Networks

	// begin Research
	public function getResearch($scientistId, $target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvresearches');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvresearches.res_field', 'left');
		$this->db->where('tblsrvresearches.res_srv_id', $scientistId);
		if($target < 3)
			$this->db->where('tblsrvresearches.res_target', $target);
		$this->db->order_by('tblsrvresearches.res_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function addResearch($arrNetwork)
	{
		$this->db->insert('tblsrvresearches',$arrNetwork);
		return $this->db->insert_id();
	}

	public function editResearch($arrNetwork,$net_id)
	{
		$this->db->where('res_id', $net_id);
		$this->db->update('tblsrvresearches', $arrNetwork);
		return $this->db->affected_rows(); 
	}

	public function deleteResearch($net_id)
	{
		$this->db->where('res_id', $net_id);
		$this->db->delete('tblsrvresearches');
		return $this->db->affected_rows(); 	
	}
	// end Research

	// begin other
	public function getOthers($scientistId, $target)
	{
		$this->db->select('*');
		$this->db->from('tblsrvothers');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvothers.oth_field', 'left');
		$this->db->where('tblsrvothers.oth_srv_id', $scientistId);
		if($target < 3)
			$this->db->where('tblsrvothers.oth_target', $target);
		$this->db->order_by('tblsrvothers.oth_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function addOther($arrOther)
	{
		$this->db->insert('tblsrvothers',$arrOther);
		return $this->db->insert_id();
	}

	public function editOther($arrOther,$oth_id)
	{
		$this->db->where('oth_id', $oth_id);
		$this->db->update('tblsrvothers', $arrOther);
		return $this->db->affected_rows(); 
	}

	public function deleteOther($oth_id)
	{
		$this->db->where('oth_id', $oth_id);
		$this->db->delete('tblsrvothers');
		return $this->db->affected_rows(); 	
	}
	// end other

	// begin files
	public function getFiles($serviceId)
	{
		$this->db->select('*');
		$this->db->from('tblsrvseminarsattachments');
		$this->db->where('attch_srv_id', $serviceId);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getFilesBySemId($semid)
	{
		$this->db->select('*');
		$this->db->from('tblsrvseminarsattachments');
		$this->db->where('attch_sem_id', $semid);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function addFile($arrNewFile)
	{
		$this->db->insert('tblsrvseminarsattachments',$arrNewFile);
		return $this->db->insert_id();
	}

	public function deleteFile($file_id)
	{
		$this->db->where('attch_id', $file_id);
		$this->db->delete('tblsrvseminarsattachments');
		return $this->db->affected_rows(); 	
	}
	// end files
	
	public function getFields()
	{
		$query = $this->db->get('tblfields');
		return $query->result_array();
	}
	
	public function getParticipants()
	{
		$query = $this->db->get('tblparticipants');
		return $query->result_array();
	}

	public function addServiceAward($arrServiceAward)
	{
		$this->db->insert('tblsciservice',$arrServiceAward);
		return $this->db->insert_id();
	}

	public function editServiceAward($srvID ,$arrServiceAward)
	{
		$this->db->where('srv_id', $srvID);
		$this->db->update('tblsciservice', $arrServiceAward);
		return $this->db->affected_rows(); 
	}
	
	public function removeServiceAwards($srvID, $arrServiceAward)
	{
		$this->db->where('srv_id', $srvID);
		$this->db->update('tblsciservice', $arrServiceAward);
		return $this->db->affected_rows(); 
	}

	public function addContractDates($arrContract)
	{
		$this->db->insert('tblsrvcontractdates',$arrContract);
		return $this->db->insert_id();
	}

	public function deleteContractDates($id)
	{
		$this->db->where('con_srv_id', $id);
		$this->db->delete('tblsrvcontractdates');
		return $this->db->affected_rows(); 	
	}
	
	public function report_isExist($srvid){
		$this->db->where('srp_srv_id' , $srvid);
		$query = $this->db->get('tblsrvreports');
		return $query->num_rows();
	}

	public function addReport($arrReport)
	{
		$this->db->insert('tblsrvreports',$arrReport);
		return $this->db->insert_id();
	}

	public function editReport($arrReport,$strId)
	{
		$this->db->where('srp_id', $strId);  
		$this->db->update('tblsrvreports', $arrReport);
		return $this->db->affected_rows(); 	
	}

	public function getAllStat()
	{
		$query = $this->db->get('tblsrvstatus');
		return $query->result_array();
	}

	public function getEmptyReports()
	{
		return array(
				array(
					'srp_id'				  => '',
					'srp_progress'            => '',
					'srp_progress_date'       => '',
					'srp_terminal'            => '',
					'srp_terminal_date'       => '',
					'srp_bspfeedback'         => '',
					'srp_bspfeedback_date'    => '',
					'srp_feedback'            => '',
					'srp_feedback_date'       => '',
					'srp_evaluation'          => '',
					'srp_evaluation_date'     => '',
					'srp_implementation'      => '',
					'srp_implementation_date' => '',
					'srp_comments'            => '',
					),
				);
	}

	public function getEmptyServiceAwards()
	{
		return array(
				array(
					'srv_classification'		=> '',
					'srv_approval_date'			=> '',
					'srv_change_sched'			=> '',
					'srv_sched_approvaldate'	=> '',
					'srv_glo_id'				=> '',
					'srv_cit_id'				=> '',
					'srv_typeofaward'			=> '',
					'srv_cil_id'				=> '',
					'srv_status'				=> '',
					'srv_type_contract'			=> '',
					'srv_ins_id'				=> '',
					'srv_approved_days'			=> '',
					'srv_out_id'				=> '',
					'srv_pri_id'				=> '',
					'srv_cont_startDate'		=> '',
					'srv_cont_endDate'			=> '',
					'srv_year_completed'       	=> '',
			        'srv_with_erp'           	=> '',
			        'srv_erp_date'           	=> '',
			        'srv_erp_venue'          	=> '',
			        'srv_erp_participants'      => '',
			        'srv_erp_reason'         	=> '',
			        'srv_year_repatriated'     	=> '',
			        'srv_processed_by'     		=> '',
			        'srv_processed_date'     	=> '',
			        'srv_processed_remarks'		=> '',
			        'srv_isyearonly'			=> '',
			        'srv_approval_yr'			=> '',
					),
			);
	}

}