<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model {
	public function __construct()
	{
           $this->load->database();
	}
	
	public function getReports()
	{
		$this->db->where('rep_stat', 1);
		$query = $this->db->get('tblreports');
		return $query->result_array();
	}

	public function getreportDate()
	{
		$sql = "SELECT max(srvdate) as enddate, min(srvdate) as startdate from ( SELECT case when srv_approval_date >= srv_sched_approvaldate then srv_approval_date else srv_sched_approvaldate end as srvdate FROM tblsciservice ) as a";	

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getreportYear()
	{
		$sql = "select min(NULLIF(date2, 0)) as sdate, max(date2) as edate from( select *, IF(date1 = '0000-00-00', srv_approval_yr, year(date1)) AS date2 from ( SELECT srv_approval_yr,srv_approval_date, srv_sched_approvaldate, IF(srv_sched_approvaldate IS NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) AS date1 FROM `tblsciservice` ) as a ) as b";	

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getReportName($reportid)
	{
		$this->db->where('rep_id', $reportid);
		$query = $this->db->get('tblreports');
		$res = $query->result_array();
		return $res[0]['rep_desc'];
	}

	// BEGIN - Balik Scientist Program - Gender Data by Period
	function getScientistGender($strGender='', $intYearFrom='', $intYearTo='', $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);
		
		$strSQL = "SELECT COUNT(DISTINCT tblscientist.sci_id) AS intScientistCount FROM tblscientist
							LEFT JOIN tblsciservice ON tblscientist.sci_id = tblsciservice.srv_sci_id
							LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
							WHERE tblscientist.sci_gender = '$strGender' 
							AND tblscientist.sci_isdeleted = 0
							AND if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '$intYearFrom-01-01'
							AND if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '$intYearTo-12-31' ".$sqlAccess;

		$objQuery = $this->db->query($strSQL);
		$rs = $objQuery->result_array();
		return count($rs)>0? $rs[0]['intScientistCount']: 0;
	}
	// END - Balik Scientist Program - Gender Data by Period

	// BEGIN - Balik Scientist Program - List of Balik Scientist by Gender by Area of Expertise
	function getScientistByGenderByExpertise($strGender='', $intYearFrom='', $intYearTo='', $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$strSQL = "SELECT DISTINCT tblscientist.* FROM tblscientist
							LEFT JOIN tblsciservice ON tblscientist.sci_id = tblsciservice.srv_sci_id
							LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
							WHERE tblscientist.sci_gender = '$strGender' 
							AND tblscientist.sci_isdeleted = 0
							AND if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '$intYearFrom-01-01'
							AND if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '$intYearTo-12-31' ".$sqlAccess;

		$objQuery = $this->db->query($strSQL);
		$rs = $objQuery->result_array();
		
		return $rs;
	}
	// END - Balik Scientist Program - List of Balik Scientist by Gender by Area of Expertise

	// BEGIN - Distribution of BSP Awardees According to Profession by Year
	function getbspAwardee($intYearFrom='', $intYearTo='', $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$sql = "SELECT *,
					(SELECT count(*) FROM tblscientist 
						LEFT JOIN tblsciservice on tblscientist.sci_id=tblsciservice.srv_sci_id 
						LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
						WHERE sci_profession_id=prof_id 
							AND sci_isdeleted=0 
							$sqlAccess
							AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '$intYearFrom-01-01'
							AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '$intYearTo-12-31') AS profcount,
							(SELECT count(*) AS profcount FROM tblscientist 
								LEFT JOIN tblsciservice on tblscientist.sci_id=tblsciservice.srv_sci_id 
								LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
									WHERE sci_isdeleted=0 
									$sqlAccess
									AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '$intYearFrom-01-01' 
									AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '$intYearTo-12-31') AS totalprofcount FROM tblprofessions";

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	// END - Distribution of BSP Awardees According to Profession by Year

	// BEGIN - List of BSP Awardees According to Profession by Year
	function getBSPListAwardeesByProfession($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$sql = "SELECT * FROM tblscientist
					LEFT JOIN tblsciservice ON tblscientist.sci_id=tblsciservice.srv_sci_id
					LEFT JOIN tblprofessions ON tblscientist.sci_profession_id = tblprofessions.prof_id
					LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
					WHERE tblscientist.sci_isdeleted=0
						AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '$intYearFrom-01-01'
						AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '$intYearTo-12-31'
						$sqlAccess
					GROUP BY tblscientist.sci_id
					ORDER BY tblprofessions.prof_id,tblscientist.sci_last_name";

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	// END - List of BSP Awardees According to Profession by Year

	// BEGIN - Distribution of BSP Awardees According to Professional License by Year
	function getBSPDistAwardeesByProfessionLicense($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$sql = "SELECT *,
					(SELECT count(*) FROM tblscientist 
						LEFT JOIN tblsciservice on tblscientist.sci_id=tblsciservice.srv_sci_id 
						LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
						WHERE sci_profession_id=prof_id 
							AND sci_isdeleted=0 
							AND (tblscientist.sci_license is not null AND tblscientist.sci_license != '')
							$sqlAccess
							AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '$intYearFrom-01-01'
							AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '$intYearTo-12-31') AS profcount,
							(SELECT count(*) AS profcount FROM tblscientist 
								LEFT JOIN tblsciservice on tblscientist.sci_id=tblsciservice.srv_sci_id 
								LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
									WHERE sci_isdeleted=0 
									AND (tblscientist.sci_license is not null AND tblscientist.sci_license != '')
									$sqlAccess
									AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '$intYearFrom-01-01' 
									AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '$intYearTo-12-31') AS totalprofcount FROM tblprofessions";


		$query = $this->db->query($sql);
		return $query->result_array();
	}
	// END - Distributionf BSP Awardees According to Professional License by Year

	// BEGIN - List of BSP Awardees According to Professional License by Year
	function getBSPListAwardeesByProfessionLicense($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$sql = "SELECT * FROM tblscientist
					LEFT JOIN tblsciservice ON tblscientist.sci_id=tblsciservice.srv_sci_id
					LEFT JOIN tblprofessions ON tblscientist.sci_profession_id = tblprofessions.prof_id
					LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
					WHERE tblscientist.sci_isdeleted = 0
						AND (tblscientist.sci_license is not null AND tblscientist.sci_license != '')
						AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '$intYearFrom-01-01'
						AND IF(srv_sched_approvaldate = '' OR srv_sched_approvaldate is NULL OR srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '$intYearTo-12-31'
						$sqlAccess
					GROUP BY tblscientist.sci_id
					ORDER BY tblprofessions.prof_id,tblscientist.sci_last_name";

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	// END - List of BSP Awardees According to Professional License by Year

	// BEGIN - Distribution of BSP Awardees According to Area of Expertise 
	public function getDistScientistByExpertise($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd, $expertise)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$sql = "SELECT * FROM tblsciservice
				left join tblscientist on tblscientist.sci_id=tblsciservice.srv_sci_id
				LEFT JOIN tblusers ON tblusers.usr_user_id = tblscientist.created_by
				where if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYearFrom."-01-01'
				and if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYearTo."-12-31'
				and (tblscientist.sci_expertise_id = '".$expertise."'
					or tblscientist.sci_expertise_id like '".$expertise."|%'
					or tblscientist.sci_expertise_id like '%|".$expertise."|%')
				and tblscientist.sci_isdeleted=0
				$sqlAccess
				group by tblscientist.sci_id";

		$query = $this->db->query($sql);
		return $query->result_array();
	}
	// END - Distribution of BSP Awardees According to Area of Expertise 

	// BEGIN -  Distribution of BSP Awardees per Local Region
	public function countserviceAward_byregion($locid, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);
		$sql = "SELECT * FROM `tblsciservice`
				left join tblinstitutions on tblinstitutions.ins_id = tblsciservice.srv_ins_id
				LEFT JOIN tblusers ON tblusers.usr_user_id = tblsciservice.created_by
				where tblinstitutions.ins_loc_id=".$locid."
					and if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'
					and if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear2."-12-31' ".$sqlAccess;
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	public function countallserviceAward_byregion($intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$sql = "SELECT * FROM `tblsciservice`
				left join tblinstitutions on tblinstitutions.ins_id = tblsciservice.srv_ins_id
				LEFT JOIN tblusers ON tblusers.usr_user_id = tblsciservice.created_by
				where if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'
					and if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear2."-12-31' ".$sqlAccess;
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	// END -  Distribution of BSP Awardees per Local Region

	// BEGIN - Distribution of BSP Awardees per Global Region
	public function countAwards_bycountry($gloid, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);
		$sql = "SELECT * FROM tblsciservice
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id=tblsciservice.srv_glo_id
					LEFT JOIN tblusers ON tblusers.usr_user_id = tblsciservice.created_by
					WHERE tblsciservice.srv_glo_id=$gloid
					AND IF(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '$intYear1-01-01'
					AND IF(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '$intYear2-12-31' $sqlAccess";
		$query = $this->db->query($sql);

		return $query->num_rows();
	}

	public function countAwards_bycontinent($glocont, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);
	
		$sql = "SELECT * FROM tblsciservice
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id=tblsciservice.srv_glo_id
					LEFT JOIN tblusers ON tblusers.usr_user_id = tblsciservice.created_by
					WHERE tblglobalregions.glo_continent=$glocont
					AND IF(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '$intYear1-01-01'
					AND IF(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '$intYear2-12-31' $sqlAccess";
		$query = $this->db->query($sql);
		
		return $query->num_rows();

	}

	public function countAllAwards($intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);
	
		$sql = "SELECT * FROM tblsciservice
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id=tblsciservice.srv_glo_id
					LEFT JOIN tblusers ON tblusers.usr_user_id = tblsciservice.created_by
					WHERE 1
					AND IF(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '$intYear1-01-01'
					AND IF(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '$intYear2-12-31' $sqlAccess";
		$query = $this->db->query($sql);
		
		return $query->num_rows();
	}
	// END - Distribution of BSP Awardees per Global Region

	// BEGIN - Distribution of BSP Awardee by Type of Host Institution
	function getServiceAwards($yr1, $yr2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$this->db->join('tblusers', 'tblusers.usr_user_id = tblscientist.created_by', 'left');
		$this->db->join('tblinstitutions', 'tblsciservice.srv_ins_id = tblinstitutions.ins_id', 'left');
		$this->db->join('tblinstitutiontypes', 'tblinstitutiontypes.itype_id = tblsciservice.srv_ins_id', 'left');
		$query = $this->db->where('1 '.$sqlAccess);
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$yr1."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$yr2."-12-31'");
		$query = $this->db->get();
		return $query->result_array();
	}

	function ups1($inst)
	{
		if($inst == 1) return '1'; return '';
	}

	function sucs1($inst)
	{
		if($inst == 2) return '1'; return '';
	}

	function lugs1($inst)
	{
		if($inst == 4) return '1'; return '';
	}

	function pucs1($inst)
	{
		if($inst == 3) return '1'; return '';
	}

	function ngos1($inst)
	{
		if($inst == 5) return '1'; return '';
	}

	function ups2($inst)
	{
		if($inst == 1 or $inst == 2) return '1'; return '';
	}

	function pucs2($inst)
	{
		if($inst == 3) return '1'; return '';
	}

	function lugs2($inst)
	{
		if($inst == 4) return '1'; return '';
	}

	function ngos2($inst)
	{
		if($inst == 5) return '1'; return '';
	}
	// END - Distribution of BSP Awardee by Type of Host Institution

	// BEGIN
	function getNewbspAwardee($term, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$this->db->join('tblusers', 'tblusers.usr_user_id = tblscientist.created_by', 'left');
		$query = $this->db->where("tblsciservice.srv_typeofaward = ".$term);
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblsciservice.srv_isdeleted = 0 ");
		$query = $this->db->where('1 '.$sqlAccess);
		$query = $this->db->group_by('tblsciservice.srv_sci_id');
		$query = $this->db->order_by('tblsciservice.srv_dateCreated desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getbspAwardee_Engagements($term, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$this->db->join('tblusers', 'tblusers.usr_user_id = tblscientist.created_by', 'left');
		$this->db->join('tblsrvcontractdates', 'tblsciservice.srv_con_id = tblsrvcontractdates.con_id', 'left');
		$query = $this->db->where("tblsciservice.srv_typeofaward = ".$term);
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblsciservice.srv_isdeleted = 0 ");
		$query = $this->db->where('1 '.$sqlAccess);
		$query = $this->db->group_by('tblsciservice.srv_sci_id');
		$query = $this->db->order_by('tblsciservice.srv_dateCreated desc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getbspAwardee_ServiceObligation($term, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)
	{
		$sqlAccess = getSqlAccess($padmin, $pcaard, $pcieerd, $pchrd);

		$sql = "SELECT tblscientist.sci_last_name, tblscientist.sci_middle_name, tblscientist.sci_first_name, tblscientist.sci_middle_initial, tblscientist.sci_expertise_id, tblsciservice.srv_typeofaward,tblsciservice.srv_id, tblsciservice.srv_sched_approvaldate, tblsciservice.srv_approval_date, tblsciservice.srv_pri_id, tblsciservice.srv_out_id, tblsciservice.srv_type_contract, tblsciservice.srv_id, tblsciservice.srv_cont_startDate, tblsciservice.srv_cont_endDate, tblsciservice.srv_ins_id,tblsciservice.srv_cil_id, min(tblsrvcontractdates.con_date_from), max(tblsrvcontractdates.con_date_to)
			FROM tblsciservice as tblsciservice
			RIGHT JOIN tblscientist as tblscientist on tblscientist.sci_id = tblsciservice.srv_sci_id
			RIGHT JOIN tblsrvcontractdates on tblsrvcontractdates.con_srv_id = tblsciservice.srv_id
			LEFT JOIN tblusers ON tblusers.usr_user_id = tblsciservice.created_by
			where tblsciservice.srv_isdeleted = 0
			and tblsciservice.srv_typeofaward = ".$term."
			and if(srv_type_contract = 0, srv_cont_startDate, con_date_from) >= '".$intYear1."-01-01'
			and if(srv_type_contract = 0, srv_cont_endDate, con_date_to) <= '".$intYear2."-12-31'
			".$sqlAccess."
			group by tblsciservice.srv_id";

		$query = $this->db->query($sql);
		return $query->result_array();
	}

	function getContractDates($srvid)
	{
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getExpertise($expertid)
	{
		$this->db->select('*');
		$this->db->from('tblexpertise');
		$query = $this->db->where('exp_id='.$expertid);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['exp_code'];
		endforeach;
		
	}

	function getCouncil($councilId)
	{
		$this->db->select('*');
		$this->db->from('tblcouncils');
		$query = $this->db->where('cil_id='.$councilId);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['cil_code'];
		endforeach;
		
	}

	function getPriority($priority_id)
	{
		$this->db->select('*');
		$this->db->from('tblpriorityareas');
		$query = $this->db->where('pri_id='.$priority_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['pri_number'];
		endforeach;
		
	}

	function getOutcomes($outcome_id)
	{
		$this->db->select('*');
		$this->db->from('tbloutcomes');
		$query = $this->db->where('out_id='.$outcome_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['out_number'];
		endforeach;
		
	}

	function getHostInst($inst_id)
	{
		$this->db->select('*');
		$this->db->from('tblinstitutions');
		$query = $this->db->where('ins_id='.$inst_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['ins_code'];
		endforeach;
		
	}

	function getDuration($sdate, $edate){
		// duration
		$date1 = date('d-M-Y', strtotime($sdate));
		$date2 = date('d-M-Y', strtotime($edate));

		$diff = abs(strtotime($date2) - strtotime($date1));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$dateDuration = ($days == 0 ? '' : ($days == 1 ? $days.' day ': $days.' days '));
		$dateDuration .= ($months == 0 ? '' : ($months == 1 ? $months.' month ': $months.' months '));
		$dateDuration .= ($years == 0 ? '' : ($years == 1 ? $years.' year ': $years.' years '));

		return $dateDuration;
	}
	// END - 


	


}