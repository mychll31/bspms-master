<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Priority_model extends CI_Model { 
 
	public function __construct()
	{
           $this->load->database(); 
	}
	
	
   public function getAll()
	{
		$this->db->order_by('pri_number','ASC');
		$query = $this->db->get('tblpriorityareas'); 		
		return $query->result_array();
		
	}
	
 	public function getPriority($priorityId)
	{

		$this->db->where("pri_id", $priorityId);  
		$this->db->order_by('pri_number','ASC'); 
		$query = $this->db->get('tblpriorityareas');
		return $query->result_array();
	}

 	public function addPriority($arrPriority) 
	{
		$this->db->insert('tblpriorityareas', $arrPriority);
		return $this->db->insert_id();	
		 
	}
	
	public function editPriority($priorityId,$arrPriority)
	{
		$this->db->where('pri_id', $priorityId);  
		$this->db->update('tblpriorityareas', $arrPriority);
		return $this->db->affected_rows(); 	
	}
	 
	public function deletePriority($priorityId)
	{
		$this->db->where('pri_id', $priorityId);
		$this->db->delete('tblpriorityareas');
		return $this->db->affected_rows(); 	
	}
	
	public function getPriorityNumber($priorityId)
	{
		$this->db->select('pri_number');
		$this->db->from('tblpriorityareas');
		$this->db->where('pri_id !=', $priorityId);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidPrio()
	{ 
		$this->db->select("concat(pri_number, '-', pri_desc) as valpri");
		$this->db->from('tblpriorityareas');
		$query = $this->db->get();
		return $query->result_array(); 
	}

	
  	
}