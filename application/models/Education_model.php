<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Education_model extends CI_Model {
 
	public function __construct() 
	{
           $this->load->database();  
	}
	
	public function getEducation($educationId)
	{

		$this->db->where("elev_id", $educationId); 
		$this->db->order_by('elev_code','ASC'); 
		$query = $this->db->get('tbleducationallevels');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{
		$this->db->order_by('elev_code','ASC');
		$query = $this->db->get('tbleducationallevels'); 	 	
		return $query->result_array();
		
	}
 
 	public function addEducation($arrEducation)
	{
		$this->db->insert('tbleducationallevels', $arrEducation);
		return $this->db->insert_id();	
		 
	}
	
	public function editEducation($educationId,$arrEducation)
	{
		$this->db->where('elev_id', $educationId);  
		$this->db->update('tbleducationallevels', $arrEducation);
		return $this->db->affected_rows(); 	
	}
	
	public function deleteEducation($educationId) 
	{
		$this->db->where('elev_id', $educationId);
		$this->db->delete('tbleducationallevels');
		return $this->db->affected_rows(); 	
	}
 
 	public function getEducationNumber($educationId)
	{
		$this->db->select('elev_code');
		$this->db->from('tbleducationallevels');
		$this->db->where('elev_id !=', $educationId);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidEduc()
	{ 
		$this->db->select("concat(elev_code, '-', elev_desc) as valeduc");
		$this->db->from('tbleducationallevels');
		$query = $this->db->get();
		return $query->result_array(); 
	}
  	
}