<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Council_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function getCouncil($councilId)
	{

		$this->db->where("cil_id", $councilId);  
		$this->db->order_by('cil_id','ASC'); 
		$query = $this->db->get('tblcouncils');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{
		$this->db->order_by('cil_id','ASC');
		$query = $this->db->get('tblcouncils'); 	 	
		return $query->result_array();
		
	}
 
 	public function addCouncil($arrCouncil)
	{
		$this->db->insert('tblcouncils', $arrCouncil);
		return $this->db->insert_id();	
		 
	}
	
	public function editCouncil($councilId,$arrCouncil)
	{
		$this->db->where('cil_id', $councilId);  
		$this->db->update('tblcouncils', $arrCouncil);
		return $this->db->affected_rows(); 	
	}
	 
	public function deleteCouncil($councilId)
	{
		$this->db->where('cil_id', $councilId);
		$this->db->delete('tblcouncils');
		return $this->db->affected_rows(); 	
	}
 	
 	public function getCouncildesc($couid)
	{
		$this->db->select('cil_desc');
		$this->db->from('tblcouncils');
		$this->db->where('cil_id !=', $couid);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidCouncil()
	{ 
		$this->db->select("concat(cil_code, '-', cil_desc) as valcouncil");
		$this->db->from('tblcouncils');
		$query = $this->db->get();
		return $query->result_array(); 
	}
  	
}