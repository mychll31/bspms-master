<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TrackingSheet_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
   	public function getAll()
	{
		$this->db->order_by('app_label','ASC');
		$query = $this->db->get('tblapplicationpackage'); 	 	
		return $query->result_array();
	
	}

	public function getAllTracks($user_level)
	{
		$this->db->order_by('ts_id','ASC');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tbltrackingsheet.ts_hostistitution', 'left');
		$this->db->join('tblusers', 'tblusers.usr_user_id = tbltrackingsheet.ts_addedby', 'left');
		if($user_level!=1)
			$this->db->where('tblusers.usr_council', $_SESSION['sessCouncilid']);
		$this->db->where('tbltrackingsheet.ts_isRemove', 0);
		$query = $this->db->get('tbltrackingsheet');
		return $query->result_array();
	
	}

	public function getAllTracksById($tsid)
	{
		$this->db->where('ts_id', $tsid);
		$this->db->where('tbltrackingsheet.ts_isRemove', 0);
		$this->db->order_by('ts_id','ASC');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tbltrackingsheet.ts_hostistitution', 'left');	
		$this->db->join('tblusers', 'tblusers.usr_user_id = tbltrackingsheet.ts_assignedSecretariat', 'left');
		$this->db->join('tblcouncils', 'tblcouncils.cil_id = tblusers.usr_council', 'left');
		$query = $this->db->get('tbltrackingsheet');
		$res = $query->result_array();
		return $res[0];
	
	}

	public function getAllAddtlReqs()
	{
		$this->db->order_by("if(req_label like 'Other%',1,0),req_label");
		$query = $this->db->get('tbladdtlreq'); 	 	
		return $query->result_array();
	
	}

	public function addTrackSheet($arrTrackSheet)
	{
		$this->db->insert('tbltrackingsheet', $arrTrackSheet);
		return $this->db->insert_id();
	}

	public function editTrackSheet($arrTrackSheet, $tsid)
	{
		$this->db->where('ts_id', $tsid);
		$this->db->update('tbltrackingsheet', $arrTrackSheet);
		return $this->db->insert_id();
	}

	public function addTrackSheet_action($arrTrackSheet)
	{
		$this->db->insert('tblactions', $arrTrackSheet);
		return $this->db->insert_id();
	}

	public function getAllTracksAction($trackid)
	{
		$this->db->where('action_trackid', $trackid);
		$this->db->where('action_isRemove', 0);
		$this->db->join('tblusers', 'tblusers.usr_user_id = tblactions.action_secretariat', 'left');
		$this->db->join('tblcouncils', 'tblcouncils.cil_id = tblusers.usr_council', 'left');
		$this->db->join('tblrecievers', 'tblrecievers.recs_id = tblactions.action_forwardTo', 'left');
		$this->db->order_by('action_createddate','DESC');
		$query = $this->db->get('tblactions');
		return $query->result_array();
	
	}

	public function getActionById($actionid)
	{
		$this->db->where('action_id', $actionid);
		$this->db->order_by('action_id','ASC');
		$query = $this->db->get('tblactions');
		$res = $query->result_array();
		return $res[0];
	
	}
  	
  	public function editTrackSheet_action($arrTrackAction, $actionid)
	{
		$this->db->where('action_id', $actionid);
		$this->db->update('tblactions', $arrTrackAction);
		return $this->db->insert_id();
	}

	public function getAllrecievers()
	{
		$this->db->order_by("recs_name");
		$query = $this->db->get('tblrecievers'); 	 	
		return $query->result_array();
	
	}

	public function getReceiveTracks($isRecieved=0)
	{
		$this->db->where('tblactions.action_isRemove', 0);
		$this->db->where('tblactions.action_isReceived', $isRecieved);
		$this->db->where('tbltrackingsheet.ts_isRemove', 0);
		$this->db->where('tblrecievers.recs_cil_id', $_SESSION['sessCouncilid']);
		$this->db->join('tblrecievers', 'tblrecievers.recs_id = tblactions.action_forwardTo', 'left');
		$this->db->join('tbltrackingsheet', 'tbltrackingsheet.ts_id = tblactions.action_trackid', 'left');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tbltrackingsheet.ts_hostistitution', 'left');
		$this->db->group_by('action_trackid');
		$this->db->order_by('action_createddate','DESC');
		$query = $this->db->get('tblactions');
		return $query->result_array();

	}

	public function getCompleteTracks()
	{
		$this->db->where('tblactions.action_isRemove', 0);
		$this->db->where('tbltrackingsheet.ts_iscomplete', 1);
		$this->db->where('tbltrackingsheet.ts_isRemove', 0);
		$this->db->where('tblrecievers.recs_cil_id', $_SESSION['sessCouncilid']);
		$this->db->join('tblrecievers', 'tblrecievers.recs_id = tblactions.action_forwardTo', 'left');
		$this->db->join('tbltrackingsheet', 'tbltrackingsheet.ts_id = tblactions.action_trackid', 'left');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tbltrackingsheet.ts_hostistitution', 'left');
		$this->db->group_by('action_trackid');
		$this->db->order_by('action_createddate','DESC');
		$query = $this->db->get('tblactions');
		return $query->result_array();

	}

}