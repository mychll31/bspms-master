
<?php
class D_bsp_curriculumCourseDev extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('Curriculum/Course Development by Balik Scientist Awardees');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $arrData['txtYear1'];
		$intYear2 = $arrData['txtYear2'];

		if($arrData['optradio']=='byyear'){
			$intYear1 = $arrData['txtYear'];
			$intYear2 = $arrData['txtYear'];
		}

		$yrcaption = ($intYear1 == $intYear2) ? $intYear1 : $intYear1.'-'.$intYear2;
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"Accomplishments",0,1,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(130,5,"Curriculum/Course Development by Balik Scientist Awardees in CY",'',0,'R',0);
		$this->fpdf->SetFont('Arial','UB',11);
		$this->fpdf->Cell(20,5,'  '.$yrcaption.'  ','',0,'L',0);
		$this->fpdf->Ln();
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','B',8);
		$widths = array(18,38,25,45,42);
		$border = array(1,1,1,1,1);
		$align = array('C','C','C','C','C','C');
		$caption = array('Field','Name of Balik Scientist Awardee','Host Institution','No. of Curriculum/Course Developed per Awardee','Output');
		
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);

		$this->fpdf->SetFont('Arial','',8);
		$awardees = $this->getbspAwardee($intYear1, $intYear2, 0, 0);
		$temp_srvid = '';
		$temp_srvfield = '';
		$countSci = '';
		$totalCountSci = 0;
		foreach($awardees as $awardee):
			$totalCountSci++;
			$fullname = getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']);
			$field = $awardee['fld_desc'];
			$countSci = $this->getbspAwardee($intYear1, $intYear2, $awardee['sci_id'], 1);

			if($temp_srvid!=$awardee['sci_id']){
				$temp_srvid = $awardee['sci_id'];
			}else{
				$fullname = '';
				$countSci = '';
			}

			if($temp_srvfield!=$awardee['cur_field']){
				$temp_srvfield = $awardee['cur_field'];
			}else{
				if($fullname=='')
					$field = '';
			}

			$widths = array(18,38,25,45,42);
			$border = array(1,1,1,1,1);
			$align = array('C','L','C','C','C','L');
			$caption = array(
				$field,
				$fullname,
				$awardee['ins_code'],
				$countSci,
				$awardee['cur_output']);				
			
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($caption,$border,$align);
		endforeach;

		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(0,5,'Total number of Curriculum/Course Developed: '.number_format($totalCountSci),'',0,'L',0);
	}

	function getbspAwardee($intYear1, $intYear2, $sciid, $stat)
	{
		$this->db->select('*');
		$this->db->from('tblsrvcurriculum');
		$this->db->join('tblsciservice', 'tblsciservice.srv_id = tblsrvcurriculum.cur_srv_id', 'right');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'right');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tblsrvcurriculum.cur_institution', 'right');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvcurriculum.cur_field', 'right');
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblscientist.sci_isdeleted = 0");
		$query = $this->db->where("tblsrvcurriculum.cur_target = 1");
		if($stat!=0)
			$query = $this->db->where("tblscientist.sci_id = ".$sciid);
		$query = $this->db->order_by('tblscientist.sci_last_name');
		$query = $this->db->get();
		if($stat==0)
			return $query->result_array();
		else
			return $query->num_rows();
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */