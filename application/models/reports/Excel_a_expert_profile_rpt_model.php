<?php
class Excel_a_expert_profile_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model('Serviceawards_model');		
	}


	function generateExcel(){
		die('generateExcel');
	}


	// function generate($arrData)
	// {
	// 	$this->fpdf->SetTitle("Expert's Profile");
	// 	$this->fpdf->SetLeftMargin(20);
	// 	$this->fpdf->SetRightMargin(20);
	// 	$this->fpdf->SetTopMargin(20);
	// 	$this->fpdf->SetAutoPageBreak("on",20);
	// 	$this->fpdf->AddPage('P','','A4');
		
	// 	$this->fpdf->SetFont('Arial','B',12);
		
	// 	$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
	// 	$this->fpdf->Cell(0,5,"Expert's Profile",0,1,'C');		
		
	// 	$intScId = $arrData['txtScientist'];
	// 	$arrSc = $this->getScientist($intScId);
	// 	// dd($arrSc);
	// 	$this->fpdf->Ln(5);
	// 	$this->fpdf->SetFont('Arial','',9);
	// 	$widths = array(70, 100);
	// 	$border = array(1,1);
	// 	$caption = array('', '');
	// 	$align = array('L', 'C');
	// 	$style = array('','');
	// 	$empty = array('','');
	// 	$this->fpdf->SetWidths($widths);
	// 	$strScName = getFullname($arrSc[0]['sci_last_name'], $arrSc[0]['sci_first_name'], $arrSc[0]['sci_middle_name'], $arrSc[0]['sci_middle_initial']);	
	// 	$this->fpdf->FancyRow(array('Title',strtoupper($arrSc[0]['tit_desc'])),$border,$align);
	// 	$this->fpdf->Cell(70,5,"Name of Expert",1,0,'L');
	// 	$this->fpdf->SetFont('Arial','B',9);
	// 	$this->fpdf->Cell(100,5,strtoupper($strScName),1,1,'C');	
	// 	$this->fpdf->SetFont('Arial','',9);	
	// 	$this->fpdf->FancyRow(array('Birthday (DD/Month/YYYY)',date("d/F/Y", strtotime($arrSc[0]['sci_birthdate']))),$border,$align);
	// 	$this->fpdf->FancyRow(array('Gender',($arrSc[0]['sci_gender']=='F') ? 'Female' : 'Male'),$border,$align);
	// 	$this->fpdf->FancyRow(array('Civil Status',$arrSc[0]['sci_status']),$border,$align);
	// 	$this->fpdf->FancyRow(array('Area of Expertise',$this->listExpertise($arrSc[0]['sci_expertise_id'])),$border,$align);
	// 	$this->fpdf->FancyRow(array('Specialization',$this->listSpecialization($arrSc[0]['sci_specialization_id'])),$border,$align);
	// 	$this->fpdf->FancyRow(array('Profession',$arrSc[0]['prof_desc']),$border,$align);
	// 	$this->fpdf->FancyRow(array('Professional License (if applicable)',$arrSc[0]['sci_license']),$border,$align);
	// 	$this->fpdf->FancyRow(array('Contact Number',rtrim($arrSc[0]['sci_contact'], ';')),$border,$align);
	// 	$this->fpdf->FancyRow(array('Email Address',rtrim($arrSc[0]['sci_email'], ';')),$border,$align);
	// 	$this->fpdf->FancyRow(array('Postal Address',$arrSc[0]['sci_postal_address']),$border,$align);
	// 	$this->fpdf->Ln();

	// 	//Service Awards
	// 	$serviceAwards = $this->Serviceawards_model->getServiceAwardsReport($intScId);
	// 	$ctrsrvawards = 1;
	// 	$subs = '';
	// 	foreach($serviceAwards as $srvaward):
	// 		$widths = array(70, 100);
	// 		$this->fpdf->SetWidths($widths);
	// 		$subs = ($ctrsrvawards==1) ? 'First Service Award' : 'Subsequent ('.$ctrsrvawards.getNumbering($ctrsrvawards).')';
	// 		$this->fpdf->SetFont('Arial','B',9);
	// 		$this->fpdf->setFillColor(230,230,230);
	// 		$this->fpdf->Cell(70,5,$ctrsrvawards.getNumbering($ctrsrvawards).' BSP Service Award',1,0,'L',1);
	// 		$this->fpdf->Cell(100,5,$subs,1,1,'C',1);
	// 		$this->fpdf->SetFont('Arial','',9);
	// 		$this->fpdf->FancyRow(array('Type of Award(ST/LT)',($srvaward['srv_typeofaward']==0) ? 'Short Term' : 'Long Term'),$border,$align);
	// 		$this->fpdf->FancyRow(array('Type of Contract(CONT/STG)',($srvaward['srv_type_contract']==0) ? 'Continous' : 'Staggered / Non-Continous'),$border,$align);
	// 		$this->fpdf->FancyRow(array(($srvaward['srv_typeofaward']==0) ? 'Day(s)' : 'Year(s)',$srvaward['srv_approved_days']),$border,$align);
	// 		//Phases
	// 		$date_phase = $this->getContractDates($srvaward['srv_id']);
	// 		$phaseno=1;
	// 		$days = 0;
	// 		$months = 0;
	// 		$years = 0;
	// 		foreach($date_phase as $phase):
	// 			$phase_cap = ($phaseno==1) ? 'Inclusive Date(s) of Contract' : '';
	// 			$widths = array(70, 70, 30);
	// 			$this->fpdf->SetWidths($widths);

	// 			$dmy = getDuration($phase['con_date_from'],$phase['con_date_to']);
	// 			$ddays = $dmy['days'];
	// 			$mmonths = $dmy['months'];
	// 			$yyears = $dmy['years'];
	// 			$duration = getDuration_caption($ddays, $mmonths, $yyears);
	// 			$this->fpdf->FancyRow(array($phase_cap,'Phase '.$phaseno.': '.date('y M-d',strtotime($phase['con_date_from'])).' '.date('y M-d',strtotime($phase['con_date_to'])),$duration),array(1,1,1),array('L','C','C'));
	// 			$days = $days + $ddays;
	// 			$months = $months + $mmonths;
	// 			$years = $years + $yyears;

	// 			$phaseno++;
	// 		endforeach;
	// 		$widths = array(70, 70, 30);
	// 		$this->fpdf->SetWidths($widths);
	// 		$this->fpdf->FancyRow(array('Approved no of Days','',getDuration_caption($days, $months, $years)),array(1,1,1),array('L','C','C'));
	// 		$widths = array(70, 100);
	// 		$this->fpdf->SetWidths($widths);
	// 		$app_date = ($srvaward['srv_sched_approvaldate'] == '0000-00-00' or $srvaward['srv_sched_approvaldate'] == '') ? $srvaward['srv_approval_date'] : $srvaward['srv_sched_approvaldate'];
	// 		$this->fpdf->FancyRow(array('Date of Approval',$app_date),$border,$align);
	// 		$widths = array(170,0);
	// 		$this->fpdf->SetWidths($widths);
	// 		$this->fpdf->FancyRow(array('Remarks(If any):',''),$border,$align);
	// 		$ctrsrvawards++;
	// 		$widths = array(70, 100);
	// 		$this->fpdf->SetWidths($widths);
	// 		$this->fpdf->FancyRow(array('Host Institution',$srvaward['ins_code'].'-'.$srvaward['ins_desc']),$border,$align);
	// 		$this->fpdf->FancyRow(array('Monitoring Council',$srvaward['cil_code']),$border,$align);
	// 		$priorityArea = explode('|', $srvaward['srv_pri_id']);
	// 		$priorities = '';
	// 		foreach($priorityArea as $priority):
	// 			$priorities.=$this->getPriorityArea($priority).'; ';
	// 		endforeach;
	// 		$this->fpdf->FancyRow(array('DOST Priority Area',trim($priorities, "; ")),$border,$align);
	// 		$dostoutcomes = explode('|', $srvaward['srv_out_id']);
	// 		$outcomes = '';
	// 		foreach($dostoutcomes as $outcome):
	// 			$outcomes.=$this->getDostOutcomes($outcome).',';
	// 		endforeach;
	// 		$this->fpdf->FancyRow(array('DOST Outcome',trim($outcomes, ",")),$border,$align);
	// 		$this->fpdf->Ln();
			
	// 		// REPORTS
	// 		$widths = array(90,30,50);
	// 		$border = array(1,1,1);
	// 		$align = array('L','C','C');
	// 		$this->fpdf->SetWidths($widths);
			
	// 		$this->fpdf->SetFont('Arial','B',9);
	// 		$this->fpdf->Cell(0,5,'Reports',0,1,'L');
	// 		$this->fpdf->setFillColor(230,230,230);
	// 		$this->fpdf->Cell(90,5,'',1,0,'C',1);
	// 		$this->fpdf->Cell(30,5,'Y/N/NA',1,0,'C',1);
	// 		$this->fpdf->Cell(50,5,'Date Submitted',1,1,'C',1);

	// 		$this->fpdf->SetFont('Arial','',9);
	// 		$this->fpdf->FancyRow(array('Balik Scientist:  Submitted Progress Report',strtoupper($srvaward['srp_progress']),($srvaward['srp_progress']!= 'y') ? '' : ($srvaward['srp_progress_date']!='0000-00-00') ? $srvaward['srp_progress_date'] : ''),$border,$align);
	// 		$this->fpdf->FancyRow(array('Balik Scientist:  Submitted Terminal Report',strtoupper($srvaward['srp_terminal']),($srvaward['srp_terminal']!= 'y') ? '' : ($srvaward['srp_terminal_date']!='0000-00-00') ? $srvaward['srp_terminal_date'] : ''),$border,$align);
	// 		$this->fpdf->FancyRow(array('Balik Scientist:  Submitted BSP Feedback Form',strtoupper($srvaward['srp_bspfeedback']),($srvaward['srp_bspfeedback']!= 'y') ? '' : ($srvaward['srp_bspfeedback_date']!='0000-00-00') ? $srvaward['srp_bspfeedback_date'] : ''),$border,$align);
	// 		$this->fpdf->FancyRow(array('Host Institution: Submitted BSP Feedback Form',strtoupper($srvaward['srp_feedback']),($srvaward['srp_feedback']!= 'y') ? '' : ($srvaward['srp_feedback_date']!='0000-00-00') ? $srvaward['srp_feedback_date'] : ''),$border,$align);
	// 		$this->fpdf->FancyRow(array('Host Institution: Submitted Host Evaluation Form',strtoupper($srvaward['srp_evaluation']),($srvaward['srp_evaluation']!= 'y') ? '' : ($srvaward['srp_evaluation_date']!='0000-00-00') ? $srvaward['srp_evaluation_date'] : ''),$border,$align);
	// 		$this->fpdf->FancyRow(array('Council: Submitted Post-Implementation Evaluation Report',strtoupper($srvaward['srp_implementation']),($srvaward['srp_implementation']!= 'y') ? '' : ($srvaward['srp_implementation_date']!='0000-00-00') ? $srvaward['srp_implementation_date'] : ''),$border,$align);
	// 		$widths = array(0,170);
	// 		$border = array(1,1);
	// 		$align = array('L','L');
	// 		$this->fpdf->SetWidths($widths);
	// 		$this->fpdf->FancyRow(array('','Comments(If any): '.$srvaward['srp_comments']),$border,$align);
	// 		$this->fpdf->Ln();
	// 		$this->fpdf->Ln();
	// 	endforeach;

	// 	// EDUCATION		
	// 	$this->fpdf->SetFont('Arial','B',9);
	// 	$this->fpdf->Cell(0,5,'Education',0,1,'L');	
	// 	$this->fpdf->SetFont('Arial','',9);
	// 	$widths = array(15,55,25,55,20);
	// 	$border = array(1,1,1,1,1);
	// 	$align = array('C', 'C', 'C', 'C', 'C');
	// 	$this->fpdf->SetWidths($widths);
		
	// 	$this->fpdf->setFillColor(230,230,230);
	// 	$this->fpdf->SetFont('Arial','B',9);
	// 	$this->fpdf->Cell(15,5,'Level','TLR',0,'C',1);
	// 	$this->fpdf->Cell(55,5,'Name of Institution','TLR',0,'C',1);
	// 	$this->fpdf->Cell(25,5,'Country','TLR',0,'C',1);
	// 	$this->fpdf->Cell(55,5,'Course','TLR',0,'C',1);
	// 	$this->fpdf->Cell(20,5,'Year','TLR',1,'C',1);
	// 	$this->fpdf->Cell(15,5,'','BLR',0,'C',1);
	// 	$this->fpdf->Cell(55,5,'','BLR',0,'C',1);
	// 	$this->fpdf->Cell(25,5,'','BLR',0,'C',1);
	// 	$this->fpdf->Cell(55,5,'','BLR',0,'C',1);
	// 	$this->fpdf->Cell(20,5,'Graduated','BLR',1,'C',1);
	// 	$this->fpdf->SetFont('Arial','',9);

	// 	$arrScEduc = $this->getEducations($intScId);
		
	// 	for($intCtr = 0; $intCtr < count($arrScEduc); $intCtr++)
	// 	{
	// 		//tbleducationallevels.elev_desc, tblglobalregions.glo_country, tblcourses.cou_desc 
	// 		$this->fpdf->FancyRow(array($arrScEduc[$intCtr]['elev_code'], $arrScEduc[$intCtr]['educ_school'], $arrScEduc[$intCtr]['glo_country'], $arrScEduc[$intCtr]['cou_desc'], $arrScEduc[$intCtr]['educ_year_graduated']),$border,$align);
			
	// 	}
		
	// 	$this->fpdf->Ln();

	// 	// EMPLOYMENT HISTORY		
	// 	$this->fpdf->SetFont('Arial','B',9);
	// 	$this->fpdf->Cell(0,5,'Employment History',0,1,'L');	
	// 	$this->fpdf->SetFont('Arial','',9);
	// 	$widths = array(55,30,55,30);
	// 	$border = array(1,1,1,1);
	// 	$align = array('C', 'C', 'C', 'C');
	// 	$this->fpdf->SetWidths($widths);
		
	// 	$this->fpdf->setFillColor(230,230,230);
	// 	$this->fpdf->SetFont('Arial','B',9);
	// 	$this->fpdf->Cell(55,5,'Position/Designation','TLR',0,'C',1);
	// 	$this->fpdf->Cell(30,5,'Inclusive Years','TLR',0,'C',1);
	// 	$this->fpdf->Cell(55,5,'Company','TLR',0,'C',1);
	// 	$this->fpdf->Cell(30,5,'Country of','TLR',1,'C',1);
	// 	$this->fpdf->Cell(55,5,'','BLR',0,'C',1);
	// 	$this->fpdf->Cell(30,5,'','BLR',0,'C',1);
	// 	$this->fpdf->Cell(55,5,'','BLR',0,'C',1);
	// 	$this->fpdf->Cell(30,5,'Employment','BLR',1,'C',1);
	// 	$this->fpdf->SetFont('Arial','',9);

	// 	$arrScEmploy = $this->getEmployments($intScId);
		
	// 	for($intCtr = 0; $intCtr < count($arrScEmploy); $intCtr++)
	// 	{
		
	// 		$strInclusiveYears = substr($arrScEmploy[$intCtr]['emp_datefrom'],0,4).' - '.substr($arrScEmploy[$intCtr]['emp_dateto'],0,4);
	// 		$this->fpdf->FancyRow(array($arrScEmploy[$intCtr]['emp_position'], $strInclusiveYears, $arrScEmploy[$intCtr]['emp_company'], $arrScEmploy[$intCtr]['glo_country']),$border,$align);
			
	// 	}
			
	// 	$this->fpdf->Ln();
		
	// }
	
	public function getScientist($id)
	{
		$this->db->from('tblscientist');
		$this->db->join('tblprofessions', 'tblprofessions.prof_id = tblscientist.sci_profession_id', 'left');
		$this->db->join('tbltitles', 'tbltitles.tit_id = tblscientist.sci_title', 'left');
		$this->db->where('sci_id' , $id);
		$query = $this->db->get();
		return $query->result_array();
	}

	function getPriorityArea($priority)
	{
		$this->db->where('pri_id ', $priority);
		$query = $this->db->get('tblpriorityareas');
		foreach($query->result_array() as $res):
			return $res['pri_desc'];
		endforeach;
	}

	function getDostOutcomes($outcome)
	{
		$this->db->where('out_id ', $outcome);
		$query = $this->db->get('tbloutcomes');
		foreach($query->result_array() as $res):
			return $res['out_number'];
		endforeach;
	}

	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getReport($srvid)
	{
		$this->db->from('tblsrvreports');
		$this->db->where('srp_srv_id',$srvid);
		$query = $this->db->get();
		return $query->result_array();
	}
		
	function listExpertise($strExpertise) // $arrSc[0]['sci_expertise_id']
	{
		$strItemList = '';	
		//$strExpertise = '1|2|3|4|';
		
		$array =  explode('|', $strExpertise);
		foreach ($array as $item) {
			if($item!='')
				$strItemList .= $this->getExpertiseName($item).", ";
				
		}	
		return substr($strItemList,0,strlen($strItemList)-2);
		
	}
	
	function getExpertiseName($intExpertiseId=0)
	{
		/*
		$this->db->where('exp_id' , $intExpertiseId);
		$query = $this->db->get('tblexpertise');
		$row = $query->result_array();
		*/
		$strSQL = "SELECT * FROM tblexpertise 
					WHERE exp_id=".$intExpertiseId;

		$objQuery = $this->db->query($strSQL);
		$row = $objQuery->result_array();
		
		if($row)
			return $row[0]['exp_desc'];
		else
			return '';
	}
	
	function listSpecialization($strSpecialization)
	{
		$strItemList = '';	
		//$strExpertise = '1|2|3|4|';
		
		$array =  explode('|', $strSpecialization);
		foreach ($array as $item) {
			if($item!='')
				$strItemList .= $this->getSpecializationName($item).", ";
				
		}	
		return substr($strItemList,0,strlen($strItemList)-2);
	}
	
	function getSpecializationName($intSpecializationId=0)
	{
		$this->db->where('spe_id' , $intSpecializationId);
		$query = $this->db->get('tblspecializations');
		$row = $query->result_array();
		
		return $row[0]['spe_desc'];
	}
	
	function getProfessionName($intProfessionId=0)
	{
		$this->db->where('prof_id' , $intProfessionId);
		$query = $this->db->get('tblprofessions');
		$row = $query->result_array();
		
		return $row[0]['prof_desc'];
	}
	
	function getEducations($id=0)
	{
	
		$strSQL = "SELECT tblscieducations.*, tbleducationallevels.elev_code, tblglobalregions.glo_country, tblcourses.cou_desc FROM tblscieducations 
					LEFT JOIN tbleducationallevels ON tbleducationallevels.elev_id = tblscieducations.educ_level_id
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id = tblscieducations.educ_countryorigin_id
					LEFT JOIN tblcourses ON tblcourses.cou_id = tblscieducations.educ_course_id
					WHERE tblscieducations.educ_sci_id=".$id." ORDER BY tblscieducations.educ_year_graduated DESC";

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
		
	}
	
	function getEmployments($id=0)
	{
	
		$strSQL = "SELECT tblsciemployment.*, tblglobalregions.glo_country FROM tblsciemployment 
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id = tblsciemployment.emp_countryorigin_id
					WHERE tblsciemployment.emp_sci_id=".$id." ORDER BY tblsciemployment.emp_datefrom DESC";
;

		$objQuery = $this->db->query($strSQL);
		return $objQuery->result_array();
		
	}
}

/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */
?>

<table border=1>
	<tr></tr>
</table>