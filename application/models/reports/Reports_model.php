<?php

class Reports_model extends CI_Model {
	
	public function __construct()
	{
		$this->load->library('fpdf_gen');
		$this->fpdf = new FPDF();
	}
	
	public function displayHeader($strReportTitle='')
	{
		$this->fpdf->SetFont('Arial','',14);
		
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');	
		$this->fpdf->Cell(0,5,$strReportTitle,0,1,'C');	
	}
	
}


?>