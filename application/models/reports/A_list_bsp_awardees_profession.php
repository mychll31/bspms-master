
<?php
class A_list_bsp_awardees_profession extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model(array('Reports_model'));
	}

	function generate()
	{
		$intYearFrom = $_GET['txtYear1'];
		$intYearTo = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$scientists = $this->Reports_model->getBSPListAwardeesByProfession($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd);
		$ctr = 1;
		$sci_ctr = 0;
		$temp_profid = 0;

		# BEGIN Export to Excel
		if(isset($_GET['excel'])){
			 header("Content-type: application/vnd.ms-excel");
			 header("Content-Disposition: attachment; filename=List of BSP Awardees According to Profession by Year.xls");
			?>
			<table>
					<tr>
						<th colspan="3">BALIK SCIENTIST PROGRAM</th>
					</tr>
					<tr>
						<th colspan="3">List of BSP Awardees According to Profession by Year (<?=$intYearFrom?> - <?=$intYearTo?>)</th>
					</tr>
			</table><br>

			<table border="1">
				<tr>
					<th>Count</th>
					<th>Name of Balik Scientist [1]</th>
					<th>Profession [2]</th>
				</tr>
				<?php
					foreach($scientists as $scientist):
						$sci_ctr++;
						if($temp_profid != $scientist['sci_profession_id']){
							if($sci_ctr>1){
								$ctr = $ctr -1;
								echo '<tr>';
									echo '<td>Total: '.$ctr.'</td>';
									echo '<td></td>';
									echo '<td></td>';
								echo '</tr>';
								echo '<tr><td colspan=3 bgcolor="#CCC"></td></tr>';
							}
							$ctr = 1;
						}

						echo '<tr>';
							echo '<td>'.$ctr++.'</td>';
							echo '<td>'.getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial']).'</td>';
							echo '<td>'.$scientist['prof_desc'].'</td>';
						echo '</tr>';		

						$temp_profid = $scientist['sci_profession_id'];
					endforeach;
					$ctr = $ctr -1;
					echo '<tr>';
						echo '<td>Total: '.$ctr.'</td>';
						echo '<td></td>';
						echo '<td></td>';
					echo '</tr>';
				?>
			</table>
			<!-- BEGIN FOOTNOTE -->
			<br><br><br><br>
			[1] Name of Balik Scientist Awardees for the year <?=getYearDuration($intYearFrom, $intYearTo)?> based on their approval date.<br>
			[2] The corresponding Profession of Balik Scientist Awardees [2].

		<?php
		# END Export to Excel; BEGIN Export to PDF
		}else{
			$this->fpdf->SetTitle('List of BSP Awardees According to Profession by Year');
			$this->fpdf->SetLeftMargin(20);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			$this->fpdf->AddPage('P','','A4');
				
			$this->fpdf->SetFont('Arial','B',11);

			$widths = array(20,75,70);
			$border = array(1,1,1);
			$align = array('C', 'C', 'C');
			$this->fpdf->SetWidths($widths);

			$this->fpdf->SetFont('Arial','',9);

			foreach($scientists as $scientist):
				$sci_ctr++;
				if($temp_profid != $scientist['sci_profession_id']){
					if($sci_ctr>1){
						$ctr = $ctr -1;
						$this->fpdf->FancyRow(array(
							'Total: '.$ctr,
							'',
							''),$border,$align);
						$this->drawColoredBox();
					}
					$ctr = 1;
				}
				$this->fpdf->FancyRow(array(
						$ctr++,
							getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial']),
						$scientist['prof_desc']
						),$border,$align);
				$temp_profid = $scientist['sci_profession_id'];
			endforeach;
			$ctr = $ctr -1;
			$this->fpdf->FancyRow(array(
				'Total: '.$ctr,
				'',
				''),$border,$align);

			## FOOTNOTE
			$this->fpdf->SetY(260);
			$this->fpdf->Cell(100,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Name of Balik Scientist Awardees for the year '.getYearDuration($intYearFrom, $intYearTo).' based on their approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'The corresponding Profession of Balik Scientist Awardees [2].',0,0,'L');
		}
	}

	function drawColoredBox(){
		$widths = array(20,75,70);
		$border = array(1,1,1);
		$align = array('C', 'C', 'C');
		$caption = array('','','');
		//output
		$this->fpdf->SetFillColor(227,227,227);
		for ($i=0; $i < 3 ; $i++) { 
			$this->fpdf->Cell($widths[$i],3,$caption[$i],'TLRB',0,$align[$i],1);
		}
		$this->fpdf->Ln();
	}


}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */