<?php
class C_listofContinuing_bsp_awardees_and_engagements_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate()
	{
		if(!isset($_GET['excel'])){
			$this->fpdf->SetTitle('List of Continuing BSP Awardees and Engagements For Implementation by Year');
			$this->fpdf->SetLeftMargin(20);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			$this->fpdf->AddPage('P','','A4');
			
			$this->fpdf->SetFont('Arial','B',11);
		}		
		$intYear1 = $_GET['txtYear1'];
		$intYear2 = $_GET['txtYear2'];
	
		$this->generatePage(0, $intYear1, $intYear2);
		$this->fpdf->Ln(5);
		$this->generatePage(1, $intYear1, $intYear2);

		$totalCount = count($this->getbspAwardee(0, $intYear1, $intYear2)) + count($this->getbspAwardee(1, $intYear1, $intYear2));

		if(!isset($_GET['excel'])){	
			$this->fpdf->Ln(5);
			$this->fpdf->Ln(5);

			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(67,5,"Total No. of New BSP Awardees in CY Year ".$intYear1.': ','',0,'L',0);
			$this->fpdf->SetFont('Arial','UB',8);
			$this->fpdf->Cell(0,5,'  '.$totalCount.'  ','',0,'L',0);
			}else{
				echo '<tr>
					<th colspan="9" align="center">Total No. of New BSP Awardees in CY Year '.$intYear1.': '.$totalCount.'</th>
				  </tr>';
		}
	}

	function getbspAwardee($term, $intYear1, $intYear2)
	{
		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'right');
		$this->db->join('tblsrvcontractdates', 'tblsciservice.srv_con_id = tblsrvcontractdates.con_id', 'right');
		$query = $this->db->where("tblsciservice.srv_typeofaward = ".$term);
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblsciservice.srv_isdeleted = 0 ");
		$query = $this->db->group_by('tblsciservice.srv_sci_id');
		$query = $this->db->order_by('tblsciservice.srv_dateCreated desc');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getExpertise($expertid)
	{
		$this->db->select('*');
		$this->db->from('tblexpertise');
		$query = $this->db->where('exp_id='.$expertid);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['exp_code'];
		endforeach;
		
	}

	function getPriority($priority_id)
	{
		$this->db->select('*');
		$this->db->from('tblpriorityareas');
		$query = $this->db->where('pri_id='.$priority_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['pri_number'];
		endforeach;
		
	}

	function getOutcomes($outcome_id)
	{
		$this->db->select('*');
		$this->db->from('tbloutcomes');
		$query = $this->db->where('out_id='.$outcome_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['out_number'];
		endforeach;
		
	}

	function getHostInst($inst_id)
	{
		$this->db->select('*');
		$this->db->from('tblinstitutions');
		$query = $this->db->where('ins_id='.$inst_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['ins_code'];
		endforeach;
		
	}

	function getDuration($sdate, $edate){
		// duration
		$date1 = date('d-M-Y', strtotime($sdate));
		$date2 = date('d-M-Y', strtotime($edate));

		$diff = abs(strtotime($date2) - strtotime($date1));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$dateDuration = ($days == 0 ? '' : ($days == 1 ? $days.' day ': $days.' days '));
		$dateDuration .= ($months == 0 ? '' : ($months == 1 ? $months.' month ': $months.' months '));
		$dateDuration .= ($years == 0 ? '' : ($years == 1 ? $years.' year ': $years.' years '));

		return $dateDuration;
	}

	function generatePage($intTerm, $intYear1, $intYear2){
		
		$awardees = $this->getbspAwardee($intTerm, $intYear1, $intYear2);
		if(isset($_GET['excel'])){
			//header("Content-type: application/vnd.ms-excel");
			//header("Content-Disposition: attachment; filename=Distribution of BSP Awardees by Type of Host Institution.xls");
		?>
				<table>
					<tr>
						<th colspan="3">BALIK SCIENTIST PROGRAM</th>
					</tr>
					<tr>
						<th colspan="3">List of Continuing BSP Awardees and Engagements For Implementation by Year (<?=$intYear1?> - <?=$intYear2?>)</th>
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
					<br>
				<table border="1">
						<tr>
							<th>Count</th>
							<th>Name of Balik Scientist</th>
							<th>Area of Expertise</th>
							<th>DOST Priority Area</th>
							<th>DOST Outcomes</th>
							<th>HOST Institution/s</th>
							<th>Duration</th>
						</tr>
					<?
					$count = 1;
					foreach ($awardees as $awardee) {
						$duration = '';
						$fullname = getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']);
						
						//Expertise
						$expertise = explode('|', $awardee['sci_expertise_id']);
						$expertDesc = '';
						foreach ($expertise as $expert) {
							if($expert != ''){
								$expertDesc.=$this->getExpertise($expert).', ';
							}
						}

						//Priority Areas
						$priorityAreas = explode('|', $awardee['srv_pri_id']);
						$priorityDesc = '';
						foreach ($priorityAreas as $priority) {
							if($priority){
								$priorityDesc.=$this->getPriority($priority).', ';
							}
						}

						//Outcomes
						$outcomes = explode('|', $awardee['srv_out_id']);
						$outcomeDesc = '';
						foreach ($outcomes as $outcome) {
							if($outcome){
								$outcomeDesc.=$this->getOutcomes($outcome).', ';
							}
						}

						// duration
						if($awardee['srv_type_contract'] == 1){
							$contractDates = $this->getContractDates($awardee['srv_id']);
							$ctrphase = 1;
							foreach ($contractDates as $contract) {
								$dateDuration = $this->getDuration(date('d-M-Y', strtotime($contract['con_date_from'])), date('d-M-Y', strtotime($contract['con_date_to'])));
								$duration.='Phase'.$ctrphase++.':'. date('d-M-Y', strtotime($contract['con_date_from'])).' to '. date('d-M-Y', strtotime($contract['con_date_to'])).' ('.$dateDuration.');';
							}
						}else{
							$dateDuration = $this->getDuration(date('d-M-Y', strtotime($awardee['srv_cont_startDate'])), date('d-M-Y', strtotime($awardee['srv_cont_endDate'])));
							$duration.=date('d-M-Y', strtotime($awardee['srv_cont_startDate'])).' to '. date('d-M-Y', strtotime($awardee['srv_cont_endDate'])).' ('.$dateDuration.')';
						}

						$widths = array(11,32,30,21,19,23,38);
						$border = array(1,1,1,1,1,1,1,1);
						$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'L');
						$this->fpdf->SetWidths($widths);
						$hostinst = explode('|', $awardee['srv_ins_id']);
						$hostinstitutions = '';
						foreach ($hostinst as $host) {
							$hostval = $this->getHostInst($host);
							$hostinstitutions.=$hostval.';';
						}
						echo '<tr>';
								echo '<td>'.$count++.'</td>';
								echo '<td>'.$fullname.'</td>';
								echo '<td>'.implode(getExpertiseNameToArray($awardee['sci_expertise_id'])).'</td>';
								echo '<td>'.trim($priorityDesc, ', ').'</td>';
								echo '<td>'.trim($outcomeDesc, ', ').'</td>';
								echo '<td>'.rtrim($hostinstitutions, ";").'</td>';
								echo '<td>'.$duration.'</td>';
						echo '</tr>';
						}
					?>
				</table>
					<br>
					<br>
					<br>
					<br>
					<br>
				<!--footnote-->
				
		<?
		}else{				
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(0,5,"A. ".($intTerm == 0 ? 'Short-Term Category' : 'Long-Term Category')." (".count($awardees).")",0,1,'L');
		$this->fpdf->Ln();

		$widths = array(11,32,30,21,19,23,38);
		$border = array(1,1,1,1,1,1,1,1);
		$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C');
		$this->fpdf->SetWidths($widths);

		$this->fpdf->setFillColor(230,230,230);
		$this->fpdf->Cell(11,5,'Count','LTR',0,'C',1);
		$this->fpdf->Cell(32,5,'Name of','LTR',0,'C',1);
		$this->fpdf->Cell(30,5,'Area of Expertise','LTR',0,'C',1);
		$this->fpdf->Cell(21,5,'DOST Priority','LTR',0,'C',1);
		$this->fpdf->Cell(19,5,'DOST','LTR',0,'C',1);
		$this->fpdf->Cell(23,5,'Host','LTR',0,'C',1);
		$this->fpdf->Cell(38,5,'Duration','LTR',1,'C',1);
		$this->fpdf->Cell(11,5,'','LBR',0,'C',1);
		$this->fpdf->Cell(32,5,'Balik Scientis','LBR',0,'C',1);
		$this->fpdf->Cell(30,5,'','LBR',0,'C',1);
		$this->fpdf->Cell(21,5,'Area','LBR',0,'C',1);
		$this->fpdf->Cell(19,5,'Outcomes','LBR',0,'C',1);
		$this->fpdf->Cell(23,5,'Institution/s','LBR',0,'C',1);
		$this->fpdf->Cell(38,5,'','LBR',1,'C',1);

		$this->fpdf->SetFont('Arial','',8);
		$count = 1;

		// dd($awardees);
		foreach ($awardees as $awardee) {
			$duration = '';
			$fullname = getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']);
			
			//Expertise
			$expertise = explode('|', $awardee['sci_expertise_id']);
			$expertDesc = '';
			foreach ($expertise as $expert) {
				if($expert != ''){
					$expertDesc.=$this->getExpertise($expert).', ';
				}
			}

			//Priority Areas
			$priorityAreas = explode('|', $awardee['srv_pri_id']);
			$priorityDesc = '';
			foreach ($priorityAreas as $priority) {
				if($priority){
					$priorityDesc.=$this->getPriority($priority).', ';
				}
			}

			//Outcomes
			$outcomes = explode('|', $awardee['srv_out_id']);
			$outcomeDesc = '';
			foreach ($outcomes as $outcome) {
				if($outcome){
					$outcomeDesc.=$this->getOutcomes($outcome).', ';
				}
			}

			// duration
			if($awardee['srv_type_contract'] == 1){
				$contractDates = $this->getContractDates($awardee['srv_id']);
				$ctrphase = 1;
				foreach ($contractDates as $contract) {
					$dateDuration = $this->getDuration(date('d-M-Y', strtotime($contract['con_date_from'])), date('d-M-Y', strtotime($contract['con_date_to'])));
					$duration.='Phase'.$ctrphase++.':'. date('d-M-Y', strtotime($contract['con_date_from'])).' to '. date('d-M-Y', strtotime($contract['con_date_to'])).' ('.$dateDuration.');';
				}
			}else{
				$dateDuration = $this->getDuration(date('d-M-Y', strtotime($awardee['srv_cont_startDate'])), date('d-M-Y', strtotime($awardee['srv_cont_endDate'])));
				$duration.=date('d-M-Y', strtotime($awardee['srv_cont_startDate'])).' to '. date('d-M-Y', strtotime($awardee['srv_cont_endDate'])).' ('.$dateDuration.')';
			}

			$widths = array(11,32,30,21,19,23,38);
			$border = array(1,1,1,1,1,1,1,1);
			$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'L');
			$this->fpdf->SetWidths($widths);
			$hostinst = explode('|', $awardee['srv_ins_id']);
			$hostinstitutions = '';
			foreach ($hostinst as $host) {
				$hostval = $this->getHostInst($host);
				$hostinstitutions.=$hostval.';';
			}
			$caption = array(
							$count++,
							$fullname,
							implode(getExpertiseNameToArray($awardee['sci_expertise_id']), ', '),
							trim($priorityDesc, ', '),
							trim($outcomeDesc, ', '),
							trim($hostinstitutions, ';'), //$this->getHostInst($awardee['srv_ins_id']),
							$duration);
			$this->fpdf->FancyRow($caption,$border,$align);

			}// end foreach awardees
		}
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */