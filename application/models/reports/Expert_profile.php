<?php
class Expert_profile extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model(array('scientists_model','serviceawards_model'));		
		$this->load->library('tcpdf_gen');	
	}

	function generate()
	{
		// create new PDF document
		
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetTitle('Scientist\'s Profile');
		
		// set margins
		$pdf->SetMargins(20, 20, 20);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetPrintHeader(false);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetPrintFooter(false);

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 9);

		// add a page
		$pdf->AddPage();

		// create some HTML content
		$pdf->SetFont('helvetica', 'B', 10);
		$pdf->Cell(0, 15, 'EXPERT\'S PROFILE', 0, false, 'C', 0, '', 0, false, 'M', 'M');

		$scientist = $this->scientists_model->getScientist($this->uri->segment(3));
		$scientist = $scientist[0];
		// Image
		if($scientist['sci_picturename']!='')
			$pdf->Image(base_url('data/sci_images').'/'.$scientist['sci_picturename'], 140, 30, 40, 40, 'JPG', '', 'R', false, 150, '', false, false, 0, false, false, false);
		$pdf->ln(10);

		// Profile
		$pdf->SetFont('helvetica', 'B', 9);
		$pdf->MultiCell(40, 5, 'Name of Grantee', 0, 'L', 0,0, '', '', true);
		$pdf->MultiCell(65, 5, getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial']), 0, 'L', 0,1, '', '', true);
		$pdf->SetFont('helvetica', '', 9);
		
		$expertises = getExpertiseNameToArray($scientist['sci_expertise_id']);
		foreach($expertises as $key=>$expertise):
			$pdf->MultiCell(40, 5, ($key==0) ? 'Area of Expertise' : '', 0, 'L', 0,0, '', '', true);
			$pdf->SetFont('ZapfDingbats','',6);
			$pdf->Cell(5, 6, 'n', 0, false, 'C', 0, '', 0, false, 'M', 'B');
			$pdf->SetFont('helvetica','',9);
			$pdf->MultiCell(60, 5, $expertise, 0, 'L', 0,1, '', '', true);
		endforeach;

		$pdf->ln(3);
		$pdf->MultiCell(40, 5, 'Inclusive Date of Contract as BSP Awardee', 0, 'L', 0,0, '', '', true);
		$contract = $this->serviceawards_model->getServiceAwards($scientist['sci_id']);
		$contractDate = null;
		if($contract[0]['srv_type_contract']==1){
			$contractDate = $this->serviceawards_model->getContractDates($contract[0]['srv_id']);
		}
		$cont_duration = getDuration($contractDate[0]['con_date_from'], $contractDate[count($contractDate)-1]['con_date_to']);
		$contract_date = date('d M Y', strtotime($contractDate[0]['con_date_from'])).' - '. date('d M Y', strtotime($contractDate[count($contractDate)-1]['con_date_to'])).' ('.getDuration_caption($cont_duration['days'], $cont_duration['months'], $cont_duration['years']).')';
		$pdf->Cell(3, 6, '', 0, false, 'C', 0, '', 0, false, 'M', 'B');
		$pdf->MultiCell(60, 5, ($contract[0]['srv_typeofaward']==0) ? 'Short-Term  '.$contract_date : 'Long-Term  '.$contract_date, 0, 'L', 0,1, '', '', true);
		$pdf->ln(5);

		$tbahost = $this->scientists_model->getSciTba($scientist['sci_id']);
		foreach($tbahost as $key=>$host):
			$pdf->MultiCell(40, 5, ($key==0) ? 'Host Institution' : '', 0, 'L', 0,0, '', '', true);
			$pdf->SetFont('ZapfDingbats','',6);
			$pdf->Cell(5, 6, 'n', 0, false, 'C', 0, '', 0, false, 'M', 'B');
			$pdf->SetFont('helvetica','',9);
			$pdf->MultiCell(60, 5, $host['ins_desc'], 0, 'L', 0,1, '', '', true);
		endforeach;
		
		$pdf->ln(2);
		$pdf->MultiCell(40, 5, 'E-mail Address', 0, 'L', 0,0, '', '', true);
		$pdf->MultiCell(65, 5, trim($scientist['sci_email'], ';'), 0, 'L', 0,1, '', '', true);

		$pdf->ln(10);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(0, 15, 'EDUCATIONAL BACKGROUND', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$pdf->ln(5);
		$pdf->SetFont('helvetica','',9);
		$scieduc = $this->scientists_model->getScientistEducation($scientist['sci_id']);
		foreach($scieduc as $educ):
			$pdf->SetFont('ZapfDingbats','',6);
			$pdf->Cell(5, 6, 'n', 0, false, 'C', 0, '', 0, false, 'M', 'B');
			$pdf->SetFont('helvetica','',9);
			$pdf->MultiCell(0, 5, implode(array($educ['cou_desc'],$educ['educ_year_graduated'],ucwords($educ['educ_school']),$educ['glo_country']), ', '), 0, 'L', 0,1, '', '', true);
		endforeach;

		$pdf->ln(10);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(0, 15, 'WORK EXPERIENCES', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$pdf->ln(5);
		$pdf->SetFont('helvetica','',9);
		$employment = $this->scientists_model->getScientistEmployment($scientist['sci_id']);
		foreach($employment as $employ):
			$pdf->SetFont('ZapfDingbats','',6);
			$pdf->Cell(5, 6, 'n', 0, false, 'C', 0, '', 0, false, 'M', 'B');
			$pdf->SetFont('helvetica','',9);
			$pdf->MultiCell(50, 5, date('Y', strtotime($employ['emp_datefrom'])).' - '.date('Y', strtotime($employ['emp_dateto'])), 0, 'L', 0,0, '', '', true);
			$pdf->SetFont('helvetica','B',9);
			$pdf->MultiCell(120, 5, ucwords($employ['emp_company']).', '.ucwords($employ['glo_country']), 0, 'L', 0,1, '', '', true);
			$pdf->SetFont('helvetica','',9);
			$pdf->MultiCell(5, 5, '', 0, 'R', 0,0, '', '', true);
			$pdf->MultiCell(50, 5, '', 0, 'L', 0,0, '', '', true);
			$pdf->MultiCell(120, 5, ucwords($employ['emp_position']), 0, 'L', 0,1, '', '', true);
			$pdf->ln(2);
		endforeach;
		
		$pdf->ln(8);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(0, 15, 'To be Accomplished as a BSP Awardee', 0, false, 'L', 0, '', 0, false, 'M', 'M');
		$pdf->ln(5);
		foreach($tbahost as $tba):
			$tba['tba_text'] = str_replace('<div>', '', $tba['tba_text']);
			$tba['tba_text'] = str_replace('</div>', '', $tba['tba_text']);
			$tba['tba_text'] = cleanHtml($tba['tba_text']);
			// echo '<hr>';
			// print_r(htmlentities($tba['tba_text']));
			
			$pdf->SetFont('helvetica','UB',9);
			$pdf->MultiCell(170, 5, $tba['ins_desc'], 0, 'L', 0,1, '', '', true);
			
			// output the HTML content
			$pdf->SetFont('helvetica','',9);
			$pdf->writeHTML($tba['tba_text'], true, 0, true, 0);
			$pdf->ln(5);
		endforeach;
		// ---------------------------------------------------------
		// die();
		//Close and output PDF document
		$pdf->Output('example_021.pdf', 'I');
	}


}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */
