
<?php
class A_distribution_bsp_globalRegion extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model(array('globalregions_model', 'Reports_model'));
	}

	function generate()
	{
		$intYear1 = $_GET['txtYear1'];
		$intYear2 = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$continents = array(
			    '1' => 'Africa',
			    '2' => 'Asia',
			    '3' => 'Australia',
			    '4' => 'Europe',
			    '5' => 'Middle East',
			    '6' => 'North America',
			    '7' => 'Oceania',
			    '8' => 'South America'
			);

		$totalcount = $this->Reports_model->countAllAwards($intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
		$percentage = 0;
		
		## BEGIN Export to Excel	
		if(isset($_GET['excel'])){
			 header("Content-type: application/vnd.ms-excel");
			 header("Content-Disposition: attachment; filename=Distribution of BSP Awardees per Global Region.xls");
			?>
			<table>
				<tr>
					<th colspan="3">BALIK SCIENTIST PROGRAM</th>
				</tr>
				<tr>
					<th colspan="3">Distribution of BSP Awardees per Global Region (<?=$intYear1?> - <?=$intYear2?>)</th>
				</tr>
			</table><br>

			<table border="1">
				<tr>
					<th>Global Region [1]</th>
					<th>No. of Balik Scientists [2]</th>
					<th>% [3]</th>
				</tr>
				<?php
					foreach($continents as $key=>$continent):
						$awards_bycontinent = $this->Reports_model->countAwards_bycontinent($key, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
						if($awards_bycontinent<1)
							$percentage = 0;
						else
							$percentage = round(($awards_bycontinent/$totalcount)*100,2);
							
						echo '<tr>';
							echo '<td><b> '.$continent.'</b></td>';
							echo '<td>'.$awards_bycontinent.'</td>';
							echo '<td>'.$percentage.'</td>';
						echo '</tr>';

						$globalregions = $this->globalregions_model->getcountryByContinent($key);
						foreach ($globalregions as $glo):
							$awards_bycountry = $this->Reports_model->countAwards_bycountry($glo['glo_id'], $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
							if($awards_bycountry > 0){
								echo '<tr>';
									echo '<td>&nbsp;&nbsp;&nbsp; '.$glo['glo_country'].'</td>';
									echo '<td>'.$awards_bycountry.'</td>';
									echo '<td></td>';
								echo '</tr>';
							}
						endforeach;
					endforeach;
					?>
				<tr>
					<th>TOTAL</th>
					<th><?=$totalcount?></th>
					<th></th>
				</tr>
			</table>

			<!-- BEGIN FOOTNOTE -->
			<br><br><br><br><br>
			[1] List of Global Regions.<br>
			[2] Number of Balik Scientist per area of Region for the year <?=getYearDuration($intYear1, $intYear2)?> based on their approval date.<br>
			[3] Percentage of [2] over the Total Number of Balik Scientist Awardees per Region for the year.
			<!-- END FOOTNOTE -->

		<?php
		# END Export to Excel, BEGIN Export to PDF
		}else{
				
			$this->fpdf->SetTitle('Distribution of BSP Awardees per Global Region');
			$this->fpdf->SetLeftMargin(30);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			$this->fpdf->AddPage('P','','A4');
				
			$this->fpdf->SetFont('Arial','B',11);

			$this->fpdf->SetFont('Arial','B',8);
			$widths = array(70,60,20);
			$border = array(1,1,1);
			$align = array('C', 'C', 'C');
			$this->fpdf->SetWidths($widths);

			$this->fpdf->SetFont('Arial','',8);

			$align = array('L', 'C', 'C');
			$totalcount = $this->Reports_model->countAllAwards($intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
			$percentage = 0;
			$ctrrow = 1;
			foreach($continents as $key=>$continent):
				$ctrrow ++ ;
				$this->fpdf->SetFont('Arial','B',8);
				$awards_bycontinent = $this->Reports_model->countAwards_bycontinent($key, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
				if($awards_bycontinent<1)
					$percentage = 0;
				else
					$percentage = round(($awards_bycontinent/$totalcount)*100,2);
				$caption = array(
							'   '.$continent,
							$awards_bycontinent,
							$percentage.'%');
				$this->fpdf->FancyRow($caption,$border,$align);
				$globalregions = $this->globalregions_model->getcountryByContinent($key);
				$this->fpdf->SetFont('Arial','',8);
				foreach ($globalregions as $glo):
					$awards_bycountry = $this->Reports_model->countAwards_bycountry($glo['glo_id'], $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
					if($awards_bycountry > 0){
						$ctrrow ++;
						$caption = array(
								'     '.$glo['glo_country'],
								$awards_bycountry,
								'');
						$this->fpdf->FancyRow($caption,$border,$align);
					}
					if($ctrrow == 40){
						$this->fpdf->AddPage();
						$ctrrow = 0;
					}
				endforeach;
			endforeach;

			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(70,5,'TOTAL','LTBR',0,'C',0);
			$this->fpdf->Cell(60,5,$totalcount,'LTBR',0,'C',0);	
			$this->fpdf->Cell(20,5,'','LTBR',0,'C',0);

			## FOOTNOTE
			$this->fpdf->SetY(250);
			$this->fpdf->Cell(100,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'List of Global Regions.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Number of Balik Scientist per area of Region for the year '.getYearDuration($intYear1, $intYear2).' based on their approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[3]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Percentage of [2] over the Total Number of Balik Scientist Awardees per Region for the year.',0,0,'L');
		} ## END Export to PDF
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */