<?php
class Profile_gdp_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model(array('Reports_model'));	
	}

	function generate()
	{
		$intYearFrom = $_GET['txtYear1'];
		$intYearTo = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$council = getCouncil($_SESSION['sessAccessLevel'] == 1 ? 1 : 0, $pcaard, $pcieerd, $pchrd);
		## BEGIN Export to Excel
		if(isset($_GET['excel'])){
			 header("Content-type: application/vnd.ms-excel");
			 header("Content-Disposition: attachment; filename=Balik Scientist Program - Gender Data.xls");
			?>
				<table>
					<tr>
						<th colspan="<?=($intYearTo - $intYearFrom) + 2?>">BALIK SCIENTIST PROGRAM</td>
					</tr>
					<tr>
						<th colspan="<?=($intYearTo - $intYearFrom) + 2?>">Gender Data for the Year (<?=$intYearFrom?> - <?=$intYearTo?>)</td>
					</tr>
				</table><br>

				<table border=1>
					<tr>
						<td></td>
						<?php 
							for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++){
								echo '<td><b>'.$intCtr.'</b></td>';
							}
						 ?>
					</tr>
					<tr>
						<td bgcolor="#ccc"><b>Female [1]</b></td>
						<?php 
							for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++){
								$arrFemaleYear[$intCtr] = $this->Reports_model->getScientistGender('F',$intCtr, $intCtr, $padmin, $pcaard, $pcieerd, $pchrd);
								echo '<td>'.$arrFemaleYear[$intCtr].'</td>';
							}
						 ?>
					</tr>
					<tr>
						<td bgcolor="#ccc"><b>Male</b></td>
						<?php 
							for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++){
								$arrMaleYear[$intCtr] = $this->Reports_model->getScientistGender('M',$intCtr, $intCtr, $padmin, $pcaard, $pcieerd, $pchrd);	
								echo '<td>'.$arrMaleYear[$intCtr].'</td>';
							}
						 ?>
					</tr>
					<tr>
						<td bgcolor="#ccc"><b>Total</b></td>
						<?php 
							for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++){
								$arrTotalYear[$intCtr] = $arrMaleYear[$intCtr] + $arrFemaleYear[$intCtr];	
								echo '<td>'.$arrTotalYear[$intCtr].'</td>';
							}
						 ?>
					</tr>
					<br>
				<tr>
					<td bgcolor="#ccc"><b>Female %[2]</b></td>
					<?php
					for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++){
						if($arrFemaleYear[$intCtr]!=0)
							$arrPercentFemaleYear[$intCtr] = ($arrFemaleYear[$intCtr] / $arrTotalYear[$intCtr]) * 100;	
						else
							$arrPercentFemaleYear[$intCtr] = 0;		
						$this->fpdf->Cell(15,5,number_format($arrPercentFemaleYear[$intCtr]),1,0,'C');
						echo '<td>'.number_format($arrPercentFemaleYear[$intCtr]).'</td>';
					}
					?>
				</tr>
				<tr>
					<td bgcolor="#ccc"><b>Male %</b></td>
					<?php
					for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++){
						if($arrMaleYear[$intCtr]!=0)
							$arrPercentMaleYear[$intCtr] = ($arrMaleYear[$intCtr] / $arrTotalYear[$intCtr]) * 100;	
						else	
							$arrPercentMaleYear[$intCtr] = 0;
						echo '<td>'.number_format($arrPercentMaleYear[$intCtr]).'</td>';
					}
					?>
				</tr>
				</table>
				<!--BEGIN FOOTNOTE-->
				<br><br><br><br><br>
				[1] Number of new Female Scientists for the year<br>
				[2] Number of Female Scientists over the Total number of Scientists for the year<br>
				[3] Scientist\'s approval year	
				<!-- END FOOTNOTE -->
		<?php
		## END Export to Excel, ## BEGIN Export to PDF
		}else{
			$this->fpdf->SetTitle('Balik Scientist Program - Gender Data');
			$this->fpdf->SetLeftMargin(20);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);

			$this->fpdf->AddPage('P','','A4');
			$this->fpdf->SetFont('Arial','B',11);
			
			$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM".$council,0,1,'C');
			$this->fpdf->Cell(0,5,"Gender Data for the Year (".$intYearFrom." - ".$intYearTo.")",0,1,'C');
			$this->fpdf->Ln();
			$this->fpdf->Ln();
			if($intYearTo - $intYearFrom > 4)
				$xaxis = 75;
			else
				$xaxis = 85;

			$this->fpdf->setFillColor(230,230,230);

			$this->fpdf->SetX($xaxis-(($intYearTo - $intYearFrom)*5) - 5);
			$this->fpdf->Cell(20,5,"",1,0,'C',1);
			$this->fpdf->SetFont('Arial','B',9);
			for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++){
				$this->fpdf->Cell(15,5,$intCtr,1,0,'C',1);
				$xaxis = $xaxis - 5;
			}
			$this->fpdf->Ln();
			
			$this->fpdf->SetX($xaxis);
			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(15,5,"Female",'L',0,'R',1);
			$this->fpdf->SetFont('Arial','',6);
			$this->fpdf->Cell(5,5,"[1]",'R',0,'L',1);
			for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++)
			{
				$this->fpdf->SetFont('Arial','',9);
				$arrFemaleYear[$intCtr] = $this->Reports_model->getScientistGender('F',$intCtr, $intCtr, $padmin, $pcaard, $pcieerd, $pchrd);
				$this->fpdf->Cell(15,5,$arrFemaleYear[$intCtr],1,0,'C');
			}
			$this->fpdf->Ln();

			$this->fpdf->SetX($xaxis);
			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(20,5,"Male",1,0,'C',1);	
			for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++)
			{
				$this->fpdf->SetFont('Arial','',9);
				$arrMaleYear[$intCtr] = $this->Reports_model->getScientistGender('M',$intCtr, $intCtr, $padmin, $pcaard, $pcieerd, $pchrd);	
				$this->fpdf->Cell(15,5,$arrMaleYear[$intCtr],1,0,'C');	
			}
			$this->fpdf->Ln();
			
			$this->fpdf->SetX($xaxis);
			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(20,5,"Total",1,0,'C',1);	
			for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++)
			{
				$this->fpdf->SetFont('Arial','',9);
				$arrTotalYear[$intCtr] = $arrMaleYear[$intCtr] + $arrFemaleYear[$intCtr];	
				$this->fpdf->Cell(15,5,$arrTotalYear[$intCtr],1,0,'C');
			}
			$this->fpdf->Ln(10);

			$this->fpdf->SetX($xaxis);
			$this->fpdf->SetFont('Arial','B',9);
			// $this->fpdf->Cell(20,5,"Female %",1,0,'C',1);	
			$this->fpdf->Cell(15,5,"Female%",'LT',0,'R',1);
			$this->fpdf->SetFont('Arial','',6);
			$this->fpdf->Cell(5,5,"[2]",'RT',0,'L',1);
			for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++)
			{
				$this->fpdf->SetFont('Arial','',9);
				if($arrFemaleYear[$intCtr]!=0)
					$arrPercentFemaleYear[$intCtr] = ($arrFemaleYear[$intCtr] / $arrTotalYear[$intCtr]) * 100;	
				else
					$arrPercentFemaleYear[$intCtr] = 0;		
				$this->fpdf->Cell(15,5,number_format($arrPercentFemaleYear[$intCtr]),1,0,'C');
			}
			$this->fpdf->Ln();	
			
			$this->fpdf->SetX($xaxis);
			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(20,5,"Male%",1,0,'C',1);	
			for($intCtr = $intYearFrom; $intCtr <= $intYearTo; $intCtr++)
			{
				$this->fpdf->SetFont('Arial','',9);
				if($arrMaleYear[$intCtr]!=0)
					$arrPercentMaleYear[$intCtr] = ($arrMaleYear[$intCtr] / $arrTotalYear[$intCtr]) * 100;	
				else	
					$arrPercentMaleYear[$intCtr] = 0;
				$this->fpdf->Cell(15,5,number_format($arrPercentMaleYear[$intCtr]),1,0,'C');
			}
			$this->fpdf->Ln();		
			
			## FOOTNOTE
			$this->fpdf->SetY(260);
			$this->fpdf->Cell(100,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Number of new Female Scientists for the year',0,0,'L');
			$this->fpdf->SetFont('Arial','',7);
			$this->fpdf->Cell(0,5,'[3]',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(0,5,'Number of Female Scientists over the Total number of Scientists for the year',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[3]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(0,5,'Scientist\'s approval year',0,0,'L');
		} ## END Export to PDF
	}
	
}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */