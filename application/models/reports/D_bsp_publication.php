
<?php
class D_bsp_publication extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('Publication by Balik Scientist Awardees to their BSP Service');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('L','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $arrData['txtYear1'];
		$intYear2 = $arrData['txtYear2'];

		if($arrData['optradio']=='byyear'){
			$intYear1 = $arrData['txtYear'];
			$intYear2 = $arrData['txtYear'];
		}

		$yrcaption = ($intYear1 == $intYear2) ? $intYear1 : $intYear1.'-'.$intYear2;
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"Accomplishments",0,1,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(170,5,"Publication by Balik Scientist Awardees in CY",'',0,'R',0);
		$this->fpdf->SetFont('Arial','UB',11);
		$this->fpdf->Cell(20,5,'  '.$yrcaption.'  ','',0,'L',0);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->generatePage($intYear1, $intYear2, 1, 'pap_published_date');

		$this->fpdf->Ln(15);

		$this->fpdf->SetFont('Arial','B',11);
		$this->fpdf->Cell(170,5,"Papers Submitted for Publication by the Balik Scientist Awardees in CY",'',0,'R',0);
		$this->fpdf->SetFont('Arial','UB',11);
		$this->fpdf->Cell(20,5,'  '.$yrcaption.'  ','',0,'L',0);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->generatePage($intYear1, $intYear2, 0, 'pap_submission_date');
	}

	function getpublication($intYear1, $intYear2, $target, $sciid, $stat, $field){
		$this->db->select('*');
		$this->db->from('tblsrvpaper');
		$this->db->join('tblsciservice', 'tblsciservice.srv_id = tblsrvpaper.pap_srv_id', 'left');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvpaper.pap_field', 'left');
		$query = $this->db->where("tblsrvpaper.".$field." >= '".$intYear1."-01-01'");
		$query = $this->db->where("tblsrvpaper.".$field." <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblscientist.sci_isdeleted = 0");
		$query = $this->db->where("tblsrvpaper.pap_target = ".$target);
		if($stat!=0)
			$query = $this->db->where("tblscientist.sci_id = ".$sciid);
		$query = $this->db->order_by('tblscientist.sci_last_name');
		$query = $this->db->order_by('tblsrvpaper.pap_field');
		$query = $this->db->get();
		if($stat==0)
			return $query->result_array();
		else
			return $query->num_rows();
	}
	
	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_date_from asc');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getMaxPhase(){
		$sql = "select max(count) as totalphase from (
					select con_srv_id, count(*) as count FROM (
						select * FROM `tblsrvcontractdates`
							where con_srv_id in (
								select srv_id from tblsciservice 
									where srv_sched_approvaldate >= '2013-01-01' and srv_sched_approvaldate <= '2016-12-31') 
								) as tbla group by con_srv_id
						) as tblb";
		$query = $this->db->query($sql);
		foreach($query->result_array() as $res):
			return $res['totalphase'];
		endforeach;
	}

	function generatePage($intYear1, $intYear2, $target, $field){
		$this->fpdf->SetFont('Arial','B',8);
		$widths = ($target==1) ? array(25,38,23,20,40,40,40,18,20) : array(25,38,35,35,45,0,40,18,28);
		$border = array(1,1,1,1,1,1,1,1,1);
		$align = array('C','C','C','C','C','C','C','C','C');
		$caption = array('Field',
				'Name of Balik Scientist Awardee',
				($target==1)? 'No. of Publication per Awardee' : 'No. of Papers submitted for publication per Awardee',
				($target==1)?'Date of Publication' : 'Date submitted for Publication',
				'Name of Journal/ Magazine/ Newsletter/ Newspaper/ Book',
				($target==1)?'ISBN No./ Volume/ Issue/ Edition':'',
				'Title of Paper/ Article/ Chapter/ Book',
				'No. of Pages',
				'Author/s');
		
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);

		$this->fpdf->SetFont('Arial','',8);
		$pub_paper = $this->getpublication($intYear1, $intYear2, $target, 0, 0, $field);
		$temp_srvid = '';
		$temp_srvfield = '';
		$countSci = '';
		$totalCountSci = 0;
		foreach($pub_paper as $pub_paper):
			$totalCountSci++;
			$fullname = getFullname($pub_paper['sci_last_name'], $pub_paper['sci_first_name'], $pub_paper['sci_middle_name'], $pub_paper['sci_middle_initial']);
			$flddesc = $pub_paper['fld_desc'];
			$countSci = $this->getpublication($intYear1, $intYear2, $target, $pub_paper['sci_id'], 1, $field);

			if($temp_srvid!=$pub_paper['sci_id']){
				$temp_srvid = $pub_paper['sci_id'];
			}else{
				$fullname = '';
				$countSci = '';
			}

			if($temp_srvfield!=$pub_paper['pap_field']){
				$temp_srvfield = $pub_paper['pap_field'];
			}else{
				if($fullname=='')
					$flddesc = '';
			}

			$widths = ($target==1) ? array(25,38,23,20,40,40,40,18,20) : array(25,38,35,35,45,0,40,18,28);
			$border = array(1,1,1,1,1,1,1,1,1);
			$align = array('C','C','C','C','C','C','C','C','C');
			$caption = array(
				$flddesc,
				$fullname,
				$countSci,
				date('Y-m-d',strtotime($pub_paper['pap_published_date'])),
				$pub_paper['pap_name'],
				($target==1) ? $pub_paper['pap_volume'] : '',
				$pub_paper['pap_title'],
				$pub_paper['pap_nopages'],
				$pub_paper['pap_authors']);
			
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($caption,$border,$align);
		endforeach;

		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',10);
		$this->fpdf->Cell(0,5,($target==1) ? 'Total number of Publications: '.number_format($totalCountSci) : 'Total number of Papers submitted for Publication: '.number_format($totalCountSci),'',0,'L',0);
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */