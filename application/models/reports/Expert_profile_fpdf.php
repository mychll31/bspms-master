<?php
class Expert_profile extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model('scientists_model');		
	}

	function generate()
	{
		$this->fpdf->SetTitle("Expert's Profile");
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P', 'A4');
		
		$this->fpdf->SetFont('Arial','B',9);
		
		$this->fpdf->Cell(0,5,"EXPERT'S PROFILE",0,1,'C');
		$this->fpdf->Ln(5);

		$scientist = $this->scientists_model->getScientist($this->uri->segment(3))[0];
		$this->fpdf->Cell(40,5,'Name of Grantee',0,0,'L',0);
		$this->fpdf->Cell(75,5,getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial']),0,1,'L',0);

		$this->fpdf->Ln(2);
		$this->fpdf->SetFont('Arial','',9);
		$this->fpdf->Cell(40,5,'Area of Expertise',0,0,'L',0);
		$expertises = getExpertiseNameToArray($scientist['sci_expertise_id']);
		foreach($expertises as $expertise):
			$this->fpdf->MultiCellBlt(75,6,chr(149),$expertise);
		endforeach;

		// $this->fpdf->Cell(55,5,'Inclusive Date of Contract','',1,'L',0);
		// $this->fpdf->Cell(55,5,'Inclusive Date of Contract','',0,'L',0);
		// $this->fpdf->Cell(75,5,getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial']),'',0,'L',0);

		$this->fpdf->Ln(2);
		$this->fpdf->Cell(40,5,'Host Institution',0,0,'L',0);
		$tbahost = $this->scientists_model->getSciTba($scientist['sci_id']);
		foreach($tbahost as $key => $tba):
			$host = getHostInstitution($tba['tba_host_institution']);
			if($key==0)
				$this->fpdf->Cell(75,5,$host[0]['ins_desc'],0,1,'L',0);
			else{
				$this->fpdf->Cell(40,5,'',0,0,'L',0);
				$this->fpdf->Cell(75,5,$host[0]['ins_desc'],0,1,'L',0);
				}
		endforeach;
		$this->fpdf->Ln(2);
		$this->fpdf->Cell(40,5,'E-mail Address',0,0,'L',0);
		$this->fpdf->Cell(75,5,trim($scientist['sci_email'], ';'),0,0,'L',0);
		//IMAGE
		$image1 = "data/sci_images/".$scientist['sci_picturename'];
		$this->fpdf->Cell(40, 40, $this->fpdf->Image($image1, $this->fpdf->GetX(), $this->fpdf->GetY()-38, 40), 0, 0, 'L', false );

		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',9);
		$this->fpdf->Cell(0,5,'EDUCATIONAL BACKGROUND',0,1,'L',0);
		$scieduc = $this->scientists_model->getScientistEducation($scientist['sci_id']);
		foreach($scieduc as $educ):
			$this->fpdf->SetFont('ZapfDingbats','',5);
			$this->fpdf->Cell(8,5,'n',0,0,'R',0);
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(140,5,implode(array($educ['cou_desc'],$educ['educ_year_graduated'],ucwords($educ['educ_school']),$educ['glo_country']), ', '),0,0,'L',0);
			$this->fpdf->Ln();
		endforeach;

		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',9);
		$this->fpdf->Cell(0,5,'WORK EXPERIENCES',0,1,'L',0);
		$employment = $this->scientists_model->getScientistEmployment($scientist['sci_id']);
		foreach($employment as $employ):
			$this->fpdf->SetFont('ZapfDingbats','',5);
			$this->fpdf->Cell(8,5,'n',0,0,'R',0);
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(20,5,date('Y', strtotime($employ['emp_datefrom'])).' - '.date('Y', strtotime($employ['emp_dateto'])),0,0,'L',0);
			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(140,5,ucwords($employ['emp_company']).', '.ucwords($employ['glo_country']),0,0,'L',0);
			$this->fpdf->Ln();
			$this->fpdf->Cell(28,5,'',0,0,'L',0);
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(140,5,ucwords($employ['emp_position']),0,0,'L',0);
			$this->fpdf->Ln();
		endforeach;

		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',9);
		$this->fpdf->Cell(0,5,'To be Accomplished as a BSP Awardee',0,1,'L',0);
		$this->fpdf->Ln(2);
		foreach($tbahost as $tba):
			$this->fpdf->SetFont('Arial','UB',9);
			$this->fpdf->Cell(0,5,$tba['ins_desc'],0,1,'L',0);
			$this->fpdf->SetFont('Arial','',9);
			// $this->fpdf->WriteHTML('<ol><li>asdfer</li></ol>','');
			$this->fpdf->Cell(160,5,$tba['tba_text'],1,1,'L',0);
			$this->fpdf->Ln();
		endforeach;
		// $widths = array(55,75);
		// $border = array(1,1);
		// $align = array('L', 'L');
		// $this->fpdf->SetWidths($widths);

		// $caption = array('Level', getExpertiseName($scientist['sci_expertise_id']));
		// $this->fpdf->FancyRow($caption,$border,$align);

		// $caption = array('Level', ': Name of Institution');
		// $this->fpdf->FancyRow($caption,$border,$align);
		// $this->fpdf->Ln();

		

	}
}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */
