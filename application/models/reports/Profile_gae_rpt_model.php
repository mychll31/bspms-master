<?php
class Profile_gae_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model(array('Reports_model'));
	}

	function generate()
	{
		$intYearFrom = $_GET['txtYear1'];
		$intYearTo = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$arrMaleYear = $this->Reports_model->getScientistByGenderByExpertise('M', $intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd);

		## BEGIN Export to Excel
		if(isset($_GET['excel'])){
			 header("Content-type: application/vnd.ms-excel");
			 header("Content-Disposition: attachment; filename=List of Balik Scientist by Gender by Area of Expertise by Year.xls");
			?>
				<table>
					<tr>
						<th colspan=3>BALIK SCIENTIST PROGRAM</th>
					</tr>
					<tr>
						<th colspan=3>List of Balik Scientists by Gender by Area of Expertise by Year (<?=$intYearFrom?> - <?=$intYearTo?>)</th>
					</tr>
				</table>

				<br/>
				<table border=1>
					<tr>
						<th>Count</th>
						<th>Name of Male Balik Scientist [1] </th>
						<th>Area of Expertise [3]</th>
					</tr>
					<?php
					for($intCtr = 0; $intCtr < count($arrMaleYear); $intCtr++)
						{
							$strScName = getFullname($arrMaleYear[$intCtr]['sci_last_name'], $arrMaleYear[$intCtr]['sci_first_name'], $arrMaleYear[$intCtr]['sci_middle_name'], $arrMaleYear[$intCtr]['sci_middle_initial']);	
							$intTotalMale = $intCtr+1;
							echo '<tr>';
								echo '<td>'.$intTotalMale.'</td>';
								echo '<td>'.$strScName.'</td>';
								echo '<td>'.$this->listExpertise($arrMaleYear[$intCtr]['sci_expertise_id']).'</td>';
							echo '</tr>';

						}
					?>
					<tr><th colspan=3></th></tr>
					<tr><th colspan=3>Total No. of Male Balik Scientists: <?=$intTotalMale?></th></tr>
				</table>
				<br><br><br><br><br>
				<!-- BEGIN FOOTNOTE -->
				[1] Number of new Female Scientists for the year<br>
				[2] Number of Female Scientists over the Total number of Scientists for the year<br>
				[3] Scientist\'s approval year	
				<!-- END FOOTNOTE -->

		<?php
		# END Export To Excel; BEGIN Export to PDF
		}else{
				$this->fpdf->SetTitle('List of Balik Scientist by Gender by Area of Expertise by Year');
				$this->fpdf->SetLeftMargin(20);
				$this->fpdf->SetRightMargin(20);
				$this->fpdf->SetTopMargin(20);
				$this->fpdf->SetAutoPageBreak("on",20);
				
				$this->fpdf->AddPage('P','','A4');
				$this->fpdf->SetFont('Arial','B',11);
				$this->fpdf->setFillColor(230,230,230);
				
				if(count($arrMaleYear)>0)
				{
					$this->fpdf->SetFont('Arial','',9);
					$widths = array(20,70,70);
					$border = array(1,1,1);
					$align = array('C', 'C', 'C', 'C');
					$this->fpdf->SetWidths($widths);
					
					for($intCtr = 0; $intCtr < count($arrMaleYear); $intCtr++)
					{
						$strScName = getFullname($arrMaleYear[$intCtr]['sci_last_name'], $arrMaleYear[$intCtr]['sci_first_name'], $arrMaleYear[$intCtr]['sci_middle_name'], $arrMaleYear[$intCtr]['sci_middle_initial']);	
						
						$intTotalMale = $intCtr+1;
						$caption = array($intTotalMale, $strScName, $this->listExpertise($arrMaleYear[$intCtr]['sci_expertise_id'])); 
						$this->fpdf->FancyRow($caption,$border,$align);
					}
					
					$this->fpdf->Ln();	
					$this->fpdf->SetFont('Arial','B',9);
					$this->fpdf->Cell(160,5,"Total No. of Male Balik Scientists: ".$intTotalMale,0,0,'C');
					$this->fpdf->Ln(5);		
				}
				
				$this->fpdf->headerFemale = 1;
				$this->fpdf->AddPage();
				$arrFemaleYear = $this->Reports_model->getScientistByGenderByExpertise('F', $intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd);
				
				if(count($arrFemaleYear)>0)
				{
					$this->fpdf->SetFont('Arial','',9);
					$widths = array(20,70,70);
					$border = array(1,1,1);
					$align = array('C', 'C', 'C', 'C');
					$this->fpdf->SetWidths($widths);
					
					for($intCtr = 0; $intCtr < count($arrFemaleYear); $intCtr++)
					{	
						$strScName = getFullname($arrFemaleYear[$intCtr]['sci_last_name'], $arrFemaleYear[$intCtr]['sci_first_name'], $arrFemaleYear[$intCtr]['sci_middle_name'], $arrFemaleYear[$intCtr]['sci_middle_initial']);
						$intTotalFemale = $intCtr+1;
						$caption = array($intTotalFemale, $strScName, $this->listExpertise($arrFemaleYear[$intCtr]['sci_expertise_id'])); 
						$this->fpdf->FancyRow($caption,$border,$align);
					}
					
					$this->fpdf->Ln();	
					$this->fpdf->SetFont('Arial','B',9);
					$this->fpdf->Cell(160,5,"Total No. of Female Balik Scientists: ".$intTotalFemale,0,0,'C');
					$this->fpdf->Ln();		
				}

				## FOOTNOTE
				$this->fpdf->SetY(260);
				$this->fpdf->Cell(100,1,'','T',0,'R');
				$this->fpdf->ln(1);
				$this->fpdf->SetFont('Arial','B',8);
				$this->fpdf->Cell(5,5,'[1]',0,0,'R');
				$this->fpdf->SetFont('Arial','',9);
				$this->fpdf->Cell(65,5,'Name of Male Balik Scientist for the year '.getYearDuration($_GET['txtYear1'], $_GET['txtYear2']).' based on their approval date.',0,0,'L');
				$this->fpdf->ln(4);
				$this->fpdf->SetFont('Arial','B',8);
				$this->fpdf->Cell(5,5,'[2]',0,0,'R');
				$this->fpdf->SetFont('Arial','',9);
				$this->fpdf->Cell(65,5,'Name of Female Balik Scientist for the year '.getYearDuration($_GET['txtYear1'], $_GET['txtYear2']).' based on their approval date.',0,0,'L');
				$this->fpdf->ln(4);
				$this->fpdf->SetFont('Arial','B',8);
				$this->fpdf->Cell(5,5,'[3]',0,0,'R');
				$this->fpdf->SetFont('Arial','',9);
				$this->fpdf->Cell(0,5,'Scientist\'s Area of Expertise based on their approval date.',0,0,'L');
		
		} #END Export to PDF
		
	}
	
	function listExpertise($strExpertise) // $arrSc[0]['sci_expertise_id']
	{
		$strItemList = '';	
		//$strExpertise = '1|2|3|4|';
		
		$array =  explode('|', $strExpertise);
		foreach ($array as $item) {
			if($item!='')
				$strItemList .= $this->getExpertiseName($item).", ";
				
		}	
		return substr($strItemList,0,strlen($strItemList)-2);
		
	}
	
	function getExpertiseName($intExpertiseId=0)
	{
		
		$strSQL = "SELECT * FROM tblexpertise 
					WHERE exp_id=".$intExpertiseId;

		$objQuery = $this->db->query($strSQL);
		$row = $objQuery->result_array();
		
		if($row)
			return $row[0]['exp_desc'];
		else
			return '';
	}

	 function Header()
        {
            $this->fpdf->Cell(0,5,'Number of Female Scientists over the Total number of Scientists for the year',0,0,'L');
        }
}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */