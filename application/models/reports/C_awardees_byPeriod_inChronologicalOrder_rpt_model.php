
<?php
class C_awardees_byPeriod_inChronologicalOrder_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('List of Balik Scientist Awardees by Period in Chronological Order');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $arrData['txtYear1'];
		$intYear2 = $arrData['txtYear2'];

		$yrcaption = ($intYear1 == $intYear2) ? $intYear1 : $intYear1.'-'.$intYear2;
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"List of Balik Scientist Awardees (".$yrcaption.")",0,1,'C');
		$this->fpdf->Ln();
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','B',8);

		$this->fpdf->SetX(60);
		$widths = array(12,15,70);
		$border = array(1,1,1);
		$align = array('C', 'C', 'C');
		$this->fpdf->SetWidths($widths);
		
		$caption = array('',
						 'Year',
						 'Name of Balik Scientist');
		$this->fpdf->FancyRow($caption,$border,$align);

		$ctr = 1;
		for ($i=$intYear1; $i <= $intYear2 ; $i++) {
			
			$subtotal = 0;
			$this->fpdf->SetX(60);
			
			$this->fpdf->Cell(12,5,'',1,0,'C');
			$this->fpdf->Cell(85,5,$i,1,0,'L');
			$this->fpdf->ln();

			$awardees = $this->getbspAwardee($i);

			$ctr_awardee = 1;
			foreach($awardees as $awardee):
				$this->fpdf->SetX(60);

				$this->fpdf->Cell(12,5,$ctr++,1,0,'C');
				$this->fpdf->Cell(15,5,$ctr_awardee++,1,0,'C');
				$this->fpdf->Cell(70,5,getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']),1,0,'L');
				$this->fpdf->ln();
				$subtotal++;
			endforeach;

			$this->fpdf->SetX(60);

			$this->fpdf->Cell(12,5,'',1,0,'C');
			$this->fpdf->Cell(85,5,$subtotal.'  Subtotal ('.$i.')',1,0,'L');
			$this->fpdf->ln();

		}
		$this->fpdf->SetX(60);

		$this->fpdf->Cell(97,5,($ctr - 1).' Grand Total ('.$yrcaption.')',1,0,'L');
		$this->fpdf->ln();
		
	}

	function getbspAwardee($intYear)
	{
		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'right');
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '".$intYear."-01-01'");
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '".$intYear."-12-31'");
		$query = $this->db->where("tblsciservice.srv_isdeleted = 0 ");
		$query = $this->db->order_by('tblsciservice.srv_dateCreated');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getExpertise($expertid)
	{
		$this->db->select('*');
		$this->db->from('tblexpertise');
		$query = $this->db->where('exp_id='.$expertid);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['exp_code'];
		endforeach;
		
	}

	function getPriority($priority_id)
	{
		$this->db->select('*');
		$this->db->from('tblpriorityareas');
		$query = $this->db->where('pri_id='.$priority_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['pri_number'];
		endforeach;
		
	}

	function getOutcomes($outcome_id)
	{
		$this->db->select('*');
		$this->db->from('tbloutcomes');
		$query = $this->db->where('out_id='.$outcome_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['out_number'];
		endforeach;
		
	}

	function getHostInst($inst_id)
	{
		$this->db->select('*');
		$this->db->from('tblinstitutions');
		$query = $this->db->where('ins_id='.$inst_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['ins_code'];
		endforeach;
		
	}

	function getDuration($sdate, $edate){
		// duration
		$date1 = date('d-M-Y', strtotime($sdate));
		$date2 = date('d-M-Y', strtotime($edate));

		$diff = abs(strtotime($date2) - strtotime($date1));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$dateDuration = ($days == 0 ? '' : ($days == 1 ? $days.' day ': $days.' days '));
		$dateDuration .= ($months == 0 ? '' : ($months == 1 ? $months.' month ': $months.' months '));
		$dateDuration .= ($years == 0 ? '' : ($years == 1 ? $years.' year ': $years.' years '));

		return $dateDuration;
	}

	function generatePage($intTerm, $intYear1, $intYear2){
		
		$awardees = $this->getbspAwardee($intTerm, $intYear1, $intYear2);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(0,5,"A. ".($intTerm == 0 ? 'Short-Term Category' : 'Long-Term Category')." (".count($awardees).")",0,1,'L');
		$this->fpdf->Ln();

		$widths = array(12,28,24,21,19,23,35);
		$border = array(1,1,1,1,1,1,1);
		$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C');
		$this->fpdf->SetWidths($widths);
				
		$caption = array('Count',
						 'Name of Balik Scientis',
						 'Area of Expertise',
						 'DOST Priority Area',
						 'DOST Outcomes',
						 'Host Institution/s',
						 'Duration');
		$this->fpdf->FancyRow($caption,$border,$align);

		$this->fpdf->SetFont('Arial','',8);
		$count = 1;

		// dd($awardees);
		foreach ($awardees as $awardee) {
			$duration = '';
			$approvedDate = ($awardee['srv_sched_approvaldate'] == '0000-00-00') ? date('d-M-Y', strtotime($awardee['srv_approval_date'])) : date('d-M-Y', strtotime($awardee['srv_sched_approvaldate']));
			$fullname = $awardee['sci_last_name'].', '.$awardee['sci_first_name'];
			
			//Expertise
			$expertise = explode('|', $awardee['sci_expertise_id']);
			$expertDesc = '';
			foreach ($expertise as $expert) {
				if($expert != ''){
					$expertDesc.=$this->getExpertise($expert).', ';
				}
			}

			//Priority Areas
			$priorityAreas = explode('|', $awardee['srv_pri_id']);
			$priorityDesc = '';
			foreach ($priorityAreas as $priority) {
				if($priority){
					$priorityDesc.=$this->getPriority($priority).', ';
				}
			}

			//Outcomes
			$outcomes = explode('|', $awardee['srv_out_id']);
			$outcomeDesc = '';
			foreach ($outcomes as $outcome) {
				if($outcome){
					$outcomeDesc.=$this->getOutcomes($outcome).', ';
				}
			}

			// duration
			if($awardee['srv_type_contract'] == 1){
				$contractDates = $this->getContractDates($awardee['srv_id']);
				$ctrphase = 1;
				foreach ($contractDates as $contract) {
					$dateDuration = $this->getDuration(date('d-M-Y', strtotime($contract['con_date_from'])), date('d-M-Y', strtotime($contract['con_date_to'])));
					$duration.='Phase'.$ctrphase++.':'. date('d-M-Y', strtotime($contract['con_date_from'])).' to '. date('d-M-Y', strtotime($contract['con_date_to'])).' ('.$dateDuration.');';
				}
			}else{
				$dateDuration = $this->getDuration(date('d-M-Y', strtotime($awardee['srv_cont_startDate'])), date('d-M-Y', strtotime($awardee['srv_cont_endDate'])));
				$duration.=date('d-M-Y', strtotime($awardee['srv_cont_startDate'])).' to '. date('d-M-Y', strtotime($awardee['srv_cont_endDate'])).' ('.$dateDuration.')';
			}

			$widths = array(12,28,24,21,19,23,35);
			$border = array(1,1,1,1,1,1,1);
			$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C');
			$this->fpdf->SetWidths($widths);
			$hostinst = explode('|', $awardee['srv_ins_id']);
			$hostinstitutions = '';
			foreach ($hostinst as $host) {
				$hostval = $this->getHostInst($host);
				$hostinstitutions.=$hostval.';';
			}
			$caption = array(
							$count++,
							$fullname,
							trim($expertDesc, ', '),
							trim($priorityDesc, ', '),
							trim($outcomeDesc, ', '),
							$hostinstitutions, //$this->getHostInst($awardee['srv_ins_id']),
							$duration);
			$this->fpdf->FancyRow($caption,$border,$align);

		}// end foreach awardees
	}


}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */