
<?php
class C_bsp_awardees_byPeriod_inAlphabeticalOrder_perPhase_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('List of Balik Scientist Awardees by Period in Alphabetical Order per Phase');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $arrData['txtYear1'];
		$intYear2 = $arrData['txtYear2'];

		$yrcaption = ($intYear1 == $intYear2) ? $intYear1 : $intYear1.'-'.$intYear2;
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"List of Balik Scientist Awardees (".$yrcaption.")",0,1,'C');
		$this->fpdf->SetFont('Arial','',11);
		$this->fpdf->Cell(0,5,"(Alphabetical Order)",0,1,'C');
		$this->fpdf->Ln();
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','B',8);

		$defX = 60;
		$widths = array(11,50,13);
		$border = array(1,1,1);
		$align = array('C', 'C', 'C');
		$caption = array('','Name of Balik Scientist','Year');
		
		for ($i=1; $i <= $this->getMaxPhase($intYear1, $intYear2); $i++) {
			array_push($widths, 16);
			array_push($border, 1);
			array_push($align, 'C');
			array_push($caption, 'Phase '.$i);
			$defX = $defX - 5;
		}
		$this->fpdf->SetWidths($widths);
		$this->fpdf->SetX($defX);
		$this->fpdf->FancyRow($caption,$border,$align);
			
		$awardees = $this->getbspAwardee($intYear1, $intYear2);
		$ctr_awardee = 1;

		foreach($awardees as $awardee):
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->SetX($defX);
			$app_date 	= ($awardee['srv_sched_approvaldate'] != '' or $awardee['srv_sched_approvaldate']!= '0000-00-00') ? $awardee['srv_approval_date'] : $awardee['srv_sched_approvaldate'];
			$phase = $this->getContractDates($awardee['srv_id']);
			$this->fpdf->Cell(11,5,$ctr_awardee++,'RLB',0,'C',0);
			$this->fpdf->Cell(50,5,getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']),'RB',0,'L',0);
			$this->fpdf->Cell(13,5,date('Y', strtotime($app_date)),'RB',0,'C',0);

			if($awardee['srv_type_contract']==0){
				if(date('Y', strtotime($app_date)) >= date('Y', strtotime($awardee['srv_cont_startDate']))){
					$this->fpdf->SetFont('ZapfDingbats','',8);
					$this->fpdf->Cell(16,5,'4','RB',0,'C',0);
				}else{
					$this->fpdf->SetFont('Arial','',8);
					$this->fpdf->Cell(16,5,date('Y', strtotime($awardee['srv_cont_startDate'])).'-'.date('Y', strtotime($awardee['srv_cont_startDate'])),'RB',0,'C',0);
				}
				for ($i=1; $i < $this->getMaxPhase($intYear1, $intYear2); $i++)
					$this->fpdf->Cell(16,5,'','RB',0,'C',0);
			}else{
				for ($i=1; $i <= $this->getMaxPhase($intYear1, $intYear2); $i++) {
					if(isset($phase[$i-1]['con_date_to'])){
						if(date('Y', strtotime($app_date)) >= date('Y', strtotime($phase[$i-1]['con_date_to']))){
							$this->fpdf->SetFont('ZapfDingbats','',8);
							$this->fpdf->Cell(16,5,'4','RB',0,'C',0);
						}else{
							$this->fpdf->SetFont('Arial','',8);
							$this->fpdf->Cell(16,5,date('Y', strtotime($phase[$i-1]['con_date_from'])).'-'.date('Y', strtotime($phase[$i-1]['con_date_to'])),'RB',0,'C',0);
						}
					}else{
						$this->fpdf->Cell(16,5,'','RB',0,'C',0);
					}
				}
			}
			$this->fpdf->Ln();
		endforeach;

		$this->fpdf->Ln(15);
		$ctr_awardee = $ctr_awardee - 1;
		$this->fpdf->SetX($defX);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(37,5,'Total No. of Balik Scientists: ','',0,'C',0);
		$this->fpdf->SetFont('Arial','UB',8);
		$this->fpdf->Cell(0,5,'  '.$ctr_awardee.'  ','',0,'L',0);
		
	}

	function getbspAwardee($intYear1, $intYear2)
	{
		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'right');
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblsciservice.srv_isdeleted = 0 ");
		$query = $this->db->order_by('tblscientist.sci_last_name');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_date_from asc');
		$query = $this->db->get();
		return $query->result_array();
	}


	function getMaxPhase($startyear, $endyear){
		$sql = "select max(count) as totalphase from (
					select con_srv_id, count(*) as count FROM (
						select * FROM `tblsrvcontractdates`
							where con_srv_id in (
								select srv_id from tblsciservice where
									if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '".$startyear."-01-01'
										and if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '".$endyear."-12-31' ) 
								) as tbla group by con_srv_id
						) as tblb";
		$query = $this->db->query($sql);
		foreach($query->result_array() as $res):
			return $res['totalphase'];
		endforeach;
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */