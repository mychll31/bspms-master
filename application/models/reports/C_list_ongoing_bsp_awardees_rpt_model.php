<?php
class C_listofContinuing_bsp_awardees_and_engagements_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
		$this->load->model('Reports_model');
	}

	function generate()
	{
		$this->fpdf->SetTitle('List of Continuing BSP Awardees and Engagements For Implementation by Year');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $_GET['txtYear1'];
		$intYear2 = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard = isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;
	
		$this->generatePage(0, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
		$this->fpdf->Ln(5);
		$this->fpdf->reset_thisnewPage();
		$this->fpdf->AddPage();
		$this->generatePage(1, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);

		$totalCount = count($this->Reports_model->getbspAwardee_Engagements(0, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)) + count($this->Reports_model->getbspAwardee_Engagements(1, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd));
		
		$this->fpdf->Ln(5);
		$this->fpdf->Ln(5);

		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(67,5,"Total No. of New BSP Awardees in CY Year ".$intYear1.': ','',0,'L',0);
		$this->fpdf->SetFont('Arial','UB',8);
		$this->fpdf->Cell(0,5,'  '.$totalCount.'  ','',0,'L',0);
		
		## FOOTNOTE
		$this->fpdf->SetFont('Arial','',8);
		$this->fpdf->ln(20);
		$this->fpdf->Cell(120,1,'','T',0,'R');
		$this->fpdf->ln(1);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(5,5,'[1]',0,0,'R');
		$this->fpdf->SetFont('Arial','',8);
		$this->fpdf->Cell(65,5,'Name of Balik Scientist for the year '.getYearDuration($intYear1, $intYear2).' based on their approval date.',0,0,'L');
		$this->fpdf->ln(4);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(5,5,'[2]',0,0,'R');
		$this->fpdf->SetFont('Arial','',8);
		$this->fpdf->Cell(65,5,'Area of Expertise of [1]',0,0,'L');
		$this->fpdf->ln(4);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(5,5,'[3]',0,0,'R');
		$this->fpdf->SetFont('Arial','',8);
		$this->fpdf->Cell(65,5,'Area of DOST Priority Area of [1]',0,0,'L');
		$this->fpdf->ln(4);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(5,5,'[4]',0,0,'R');
		$this->fpdf->SetFont('Arial','',8);
		$this->fpdf->Cell(65,5,'Area of DOST Outcomes of [1]',0,0,'L');
		$this->fpdf->ln(4);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(5,5,'[5]',0,0,'R');
		$this->fpdf->SetFont('Arial','',8);
		$this->fpdf->Cell(65,5,'Type of Host Institution of [1]',0,0,'L');
		$this->fpdf->ln(4);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(5,5,'[6]',0,0,'R');
		$this->fpdf->SetFont('Arial','',8);
		$this->fpdf->Cell(65,5,'Duration of Contract based on the Service Award of [1]',0,0,'L');
	}
	
	function generatePage($intTerm, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd){
		
		$awardees = $this->Reports_model->getbspAwardee_Engagements($intTerm, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
		
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(0,5,($intTerm == 0 ? 'A. Short-Term Category' : 'B. Long-Term Category')." (".count($awardees).")",0,1,'L');
		$this->fpdf->Ln();

		$widths = array(11,32,30,21,19,23,38);
		$border = array(1,1,1,1,1,1,1,1);
		$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C');
		$this->fpdf->SetWidths($widths);

		$this->fpdf->setFillColor(230,230,230);
		$this->fpdf->Cell(11,5,'Count','LTR',0,'C',1);
		$this->fpdf->Cell(32,5,'Name of','LTR',0,'C',1);
		$this->fpdf->Cell(30,5,'Area of Expertise','LTR',0,'C',1);
		$this->fpdf->Cell(21,5,'DOST Priority','LTR',0,'C',1);
		$this->fpdf->Cell(19,5,'DOST','LTR',0,'C',1);
		$this->fpdf->Cell(23,5,'Host','LTR',0,'C',1);
		$this->fpdf->Cell(38,5,'Duration','LTR',1,'C',1);
		$this->fpdf->Cell(11,5,'','LBR',0,'C',1);
		$this->fpdf->Cell(32,5,'Balik Scientis','LBR',0,'C',1);
		$this->fpdf->Cell(30,5,'','LBR',0,'C',1);
		$this->fpdf->Cell(21,5,'Area','LBR',0,'C',1);
		$this->fpdf->Cell(19,5,'Outcomes','LBR',0,'C',1);
		$this->fpdf->Cell(23,5,'Institution/s','LBR',0,'C',1);
		$this->fpdf->Cell(38,5,'','LBR',1,'C',1);

		$this->fpdf->SetFont('Arial','',8);
		$count = 1;

		// dd($awardees);
		foreach ($awardees as $awardee) {
			$duration = '';
			$fullname = getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']);
			
			//Expertise
			$expertise = explode('|', $awardee['sci_expertise_id']);
			$expertDesc = '';
			foreach ($expertise as $expert) {
				if($expert != ''){
					$expertDesc.=$this->Reports_model->getExpertise($expert).', ';
				}
			}

			//Priority Areas
			$priorityAreas = explode('|', $awardee['srv_pri_id']);
			$priorityDesc = '';
			foreach ($priorityAreas as $priority) {
				if($priority){
					$priorityDesc.=$this->Reports_model->getPriority($priority).', ';
				}
			}

			//Outcomes
			$outcomes = explode('|', $awardee['srv_out_id']);
			$outcomeDesc = '';
			foreach ($outcomes as $outcome) {
				if($outcome){
					$outcomeDesc.=$this->Reports_model->getOutcomes($outcome).', ';
				}
			}

			// duration
			if($awardee['srv_type_contract'] == 1){
				$contractDates = $this->Reports_model->getContractDates($awardee['srv_id']);
				$ctrphase = 1;
				foreach ($contractDates as $contract) {
					$dateDuration = $this->Reports_model->getDuration(date('d-M-Y', strtotime($contract['con_date_from'])), date('d-M-Y', strtotime($contract['con_date_to'])));
					$duration.='Phase'.$ctrphase++.':'. date('d-M-Y', strtotime($contract['con_date_from'])).' to '. date('d-M-Y', strtotime($contract['con_date_to'])).' ('.$dateDuration.');';
				}
			}else{
				$dateDuration = $this->Reports_model->getDuration(date('d-M-Y', strtotime($awardee['srv_cont_startDate'])), date('d-M-Y', strtotime($awardee['srv_cont_endDate'])));
				$duration.=date('d-M-Y', strtotime($awardee['srv_cont_startDate'])).' to '. date('d-M-Y', strtotime($awardee['srv_cont_endDate'])).' ('.$dateDuration.')';
			}

			$widths = array(11,32,30,21,19,23,38);
			$border = array(1,1,1,1,1,1,1,1);
			$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'L');
			$this->fpdf->SetWidths($widths);
			$hostinst = explode('|', $awardee['srv_ins_id']);
			$hostinstitutions = '';
			foreach ($hostinst as $host) {
				$hostval = $this->Reports_model->getHostInst($host);
				$hostinstitutions.=$hostval.';';
			}
			$caption = array(
							$count++,
							$fullname,
							implode(getExpertiseNameToArray($awardee['sci_expertise_id']), ', '),
							trim($priorityDesc, ', '),
							trim($outcomeDesc, ', '),
							trim($hostinstitutions, ';'), //$this->Reports_model->getHostInst($awardee['srv_ins_id']),
							$duration);
			$this->fpdf->FancyRow($caption,$border,$align);

		}// end foreach awardees
	}


}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */