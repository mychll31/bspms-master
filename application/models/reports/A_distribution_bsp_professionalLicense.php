
<?php
class A_distribution_bsp_professionalLicense extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model('Reports_model');
		$this->load->model('profession_model');			
	}

	function generate()
	{
		$intYearFrom = $_GET['txtYear1'];
		$intYearTo = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$totalcountScientist = $this->Reports_model->getBSPDistAwardeesByProfessionLicense($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd);

		# BEGIN Export to Excel
		if(isset($_GET['excel'])){
			 header("Content-type: application/vnd.ms-excel");
			 header("Content-Disposition: attachment; filename=Distribution of BSP Awardees According to Professional License by Year.xls");
			?>
			<table>
				<tr>
					<th colspan="3">BALIK SCIENTIST PROGRAM</th>
				</tr>
				<tr>
					<th colspan="3">Distribution of BSP Awardees According to Professional License by Year (<?=$intYearFrom?> - <?=$intYearTo?>)</th>
				</tr>
			</table><br>

			<table border=1>
				<tr>
					<th>Professional License [1]</th>
					<th>Total [2]</th>
					<th>% [3]</th>
				</tr>
				<?php
				foreach($totalcountScientist as $scientist):
					if($scientist['profcount']<1)
						$total_count = 0;
					else
						$total_count = ($scientist['profcount']/$scientist['totalprofcount'])*100;
					echo '<tr>';
						echo '<td> '.$scientist['prof_desc'].'</td>';
						echo '<td>'.$scientist['profcount'].'</td>';
						echo '<td>'.round($total_count,2).'</td>';
					echo '</tr>';
				endforeach;
				?>
			</table>
			<!-- BEGIN FOOTNOTE -->
			<br><br><br><br><br>
			[1] List of All Professional License.<br>
			[2] Total Number of Balik Scientist with Professional License for the year <?=getYearDuration($intYearFrom, $intYearTo)?> based on their approval<br>
			[3] Percentage of [2] over the Total Number of Balik Scientist Awardees with Professional License for the year.
			<!-- END FOOTNOTE -->
		<?php
		# END Export to Excel; # BEGIN Export to PDF
		}else{
			$this->fpdf->SetTitle('Distribution of BSP Awardees According to Professional License by Year');
			$this->fpdf->SetLeftMargin(20);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			
			$this->fpdf->AddPage('P','','A4');
			$this->fpdf->SetFont('Arial','B',11);

			$widths = array(70,30,30);
			$border = array(1,1,1);
			$align = array('C', 'C', 'C');
			$this->fpdf->SetWidths($widths);

			$this->fpdf->SetFont('Arial','',9);
			$profList = $this->profession_model->getAll();
			$ctr = 1;
			$align = array('L', 'C', 'C');

			foreach($totalcountScientist as $scientist):
				if($scientist['profcount']<1)
					$total_count = 0;
				else
					$total_count = ($scientist['profcount']/$scientist['totalprofcount'])*100;
				$this->fpdf->SetX(45);
				$this->fpdf->FancyRow(array(
							' '.$scientist['prof_desc'],
							$scientist['profcount'],
							round($total_count,2)),$border,$align);
			endforeach;
				
			## FOOTNOTE
			$this->fpdf->SetY(250);
			$this->fpdf->Cell(100,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'List of All Professional License.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Total Number of Balik Scientist with Professional License for the year '.getYearDuration($intYearFrom, $intYearTo).' based on theirapproval',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->Cell(60,5,'date according to their Profession/s.',0,0,'R');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[3]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(0,5,'Percentage of [2] over the Total Number of Balik Scientist Awardees with Professional License for the year',0,0,'L');
		} # END Export to PDF
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */