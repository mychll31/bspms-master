<?php
class C_awardees_accomplishments extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model('outcome_model');
		$this->load->model('priority_model');
		$this->load->model('country_model');
		$this->load->model('citizenship_model');
		$this->load->model('institutions_model');
		$this->load->model('council_model');
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('Awardees and their Accomplishments');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('L','','A4');
		
		$this->fpdf->SetFont('Arial','B',9);
		
		$intYear1 = $arrData['txtYear'];
		
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"Accomplishments of Balik Scientist Awardees in CY ".$intYear1,'',0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','B',7);

		// Header
		$caption_header = array(
				'Name of Balik Scientist Awardee',
				'Host Institution',
				'Duration',
				'Type of Accomplishment',
				'Count',
				'Field',
				'Details of the Accomplishment',
				'Output'
			);
		$this->drawOrdinaryBox(array(30,30,30,30,20,25,60,35),array(1,1,1,1,1,1,1,1),array('C','C','C','C','C','C','C','C','C','C'),$caption_header);

		$this->fpdf->SetFont('Arial','',7);
		$total_seminar = 0;
		$total_training = 0;
		$total_project = 0;
		$total_publication = 0;
		$total_submitted = 0;
		$total_mentored = 0;
		$total_curriculum = 0;
		$total_network = 0;
		$total_research = 0;
		$total_others = 0;
		foreach ($this->getbspAwardee($intYear1) as $awardee):
			$duration = $this->getDuration($awardee['srv_id']);
			// Seminars
			$this->drawBox(array(
				'', //name
				getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']),
				'', //host insitution
				$awardee['ins_desc'],
				'', //duration
				date('Y-M-d', strtotime($duration['datefrom'])).' to '.date('Y-M-d', strtotime($duration['dateto'])),
				'', //caption
				'Lectures/ Seminars/ Forums conducted','','','Title','Date','Venue',''));
			$seminars = $this->getSeminars($awardee['srv_id']);
			$ctr=1;
			foreach($seminars as $seminar):
				$total_seminar++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$seminar['fld_desc'],
					$seminar['sem_title'],
					$seminar['sem_date'],
					$seminar['sem_venue'],
					$seminar['sem_output']));
			endforeach;
			//trainings
			$this->drawBox(array('','','','','','','',
				'Trainings/ Workshops/ Demonstrations conducted','','','Title','Date','Venue',''));
			$trainings = $this->getTrainings($awardee['srv_id']);
			$ctr=1;
			foreach($trainings as $training):
				$total_training++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$training['fld_desc'],
					$training['tra_title'],
					$training['tra_date'],
					$training['tra_venue'],
					$training['tra_output']));
			endforeach;
			//Project
			$this->drawBox(array('','','','','','','',
				'Project assisted','','','Project/ Title','','',''
				));
			$projects = $this->getProjects($awardee['srv_id']);
			$ctr=1;
			foreach($projects as $project):
				$total_project++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$project['fld_desc'],
					$project['prj_title'],'','',
					$project['prj_contributions']));
			endforeach;
			//Paper Published
			$this->drawBox(array('','','','','','','',
				'Paper Published','','','Title of Paper/ Article/ Chapter/ Book','Date Publication','',''
				));
			$papers = $this->getPapers($awardee['srv_id'], 0);
			$ctr=1;
			foreach($papers as $paper):
				$total_publication++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$paper['fld_desc'],
					$paper['pap_title'],
					$paper['pap_published_date'],'',''));
			endforeach;
			//Papers submited
			$this->drawBox(array('','','','','','','',
				'Papers Submitted for Publication','','','Title of Paper/ Article/ Chapter/ Book','Date Submitted','',''
				));
			$papers = $this->getPapers($awardee['srv_id'], 1);
			$ctr=1;
			foreach($papers as $paper):
				$total_submitted++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$paper['fld_desc'],
					$paper['pap_title'],
					$paper['pap_submission_date'],'',''));
			endforeach;
			//Students Mentored
			$this->drawBox(array('','','','','','','',
				'Students Mentored','','','Name of Institution','Course of Student Mentored','',''
				));
			$mentors = $this->getStudMentors($awardee['srv_id']);
			$ctr=1;
			foreach($mentors as $mentor):
				$total_mentored++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$mentor['fld_desc'],
					$mentor['ins_desc'],
					$mentor['men_course'],'',
					$mentor['men_output']));
			endforeach;
			//Curriculum
			$this->drawBox(array('','','','','','','',
				'Curriculum/ Course developed','','','Title of Course/ Curriculum','','',''
				));
			$currs = $this->getCurriculum($awardee['srv_id']);
			$ctr=1;
			foreach($currs as $cur):
				$total_curriculum++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$cur['fld_desc'],
					'','','',
					$cur['cur_output']));
			endforeach;
			//Network
			$this->drawBox(array('','','','','','','',
				'Networks and Linkages formed','','','','','',''
				));
			$networks = $this->getNetworks($awardee['srv_id']);
			$ctr=1;
			foreach($networks as $network):
				$total_network++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$network['fld_desc'],
					'','','',
					$network['net_output']));
			endforeach;
			//Research
			$this->drawBox(array('','','','','','','',
				'Research and Development','','','','','',''
				));
			$researchs = $this->getResearch($awardee['srv_id']);
			$ctr=1;
			foreach($researchs as $research):
				$total_research++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$research['fld_desc'],
					'','','',
					$research['res_output']));
			endforeach;
			//others
			$this->drawBox(array('','','','','','','',
				'Other Accomplishments','','','','','',''
				));
			$others = $this->getOthers($awardee['srv_id']);
			$ctr=1;
			foreach($others as $other):
				$total_others++;
				$this->drawBox(array('','','','','','','','',
					$ctr++,
					$other['fld_desc'],
					'','','',
					$other['oth_output']));
			endforeach;
			$this->fpdf->Cell(260,0,"",'LRB',0,'C',0);
			$this->fpdf->Ln();
		endforeach;

		$this->fpdf->AddPage('L','','A4');
		$this->fpdf->SetX(100);
		$this->fpdf->SetFont('Arial','B',7);
		$this->drawOrdinaryBox(array(115),array(1),array('C'),array('Total Number of Accomplishments (by type of activity conducted)'));

		$this->fpdf->SetFont('Arial','',7);
		$labels = array(
			array('label'=>'Total number of Lectures/ Seminars/Forums conducted', 'total' => $total_seminar),
			array('label'=>'Total number of Trainings/ Workshops/ Demonstrations conducted', 'total' => $total_training),
			array('label'=>'Total number of Project assisted', 'total' => $total_project),
			array('label'=>'Total number of Publications', 'total' => $total_publication),
			array('label'=>'Total number of Papers submitted for Publication', 'total' => $total_submitted),
			array('label'=>'Total number of Students mentored', 'total' => $total_mentored),
			array('label'=>'Total number of Curriculum/ Course developed', 'total' => $total_curriculum),
			array('label'=>'Total number of Networks and Linkages formed', 'total' => $total_network),
			array('label'=>'Total number of accomplishments on Research & Development Activities conducted', 'total' => $total_research),
			array('label'=>'Total number of accomplishments on Other Activities conducted', 'total' => $total_others),
			);

		foreach ($labels as $label) {
			$this->fpdf->SetX(100);
			$this->fpdf->Cell(100,5,$label['label'],1,0,'C',0);
			$this->fpdf->Cell(15,5,$label['total'],1,1,'C',0);
		}
	}

	function drawBox($caption){
		$widths = array(0,30,0,30,0,30,0,30,20,25,30,15,15,35);
		$border = array(1,0,1,0,1,0,1,0,1,1,1,1,1,1);
		$align = array('C','L','C','C','C','C','C','C','C','C','C','C','C','C');
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);
	}

	function drawColoredBox($caption){
		$widths = array(10,10,10,10,10,10,10,10,10,10);
		$align = array('C','C','C','C','C','C','C','C','C','C');

		//output
		$this->fpdf->SetFillColor(227,227,227);
		for ($i=0; $i < 10 ; $i++) { 
			$this->fpdf->Cell($widths[$i],5,$caption[$i],'TLRB',0,$align[$i],1);
		}
	}

	function drawOrdinaryBox($widths,$border,$align,$caption){
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);
	}

	function drawOrdinaryBox2($caption){
		$widths = array(100,15);
		$border = array(1,1);
		$align 	= array('L','L');
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);
	}

	function getbspAwardee($intYear1)
	{
		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'right');
		$this->db->join('tblinstitutions', 'tblsciservice.srv_ins_id = tblinstitutions.ins_id', 'left');
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear1."-12-31'");
		$query = $this->db->get();
		return $query->result_array();
	}

	function getSeminars($srvid){
		$this->db->where('sem_srv_id', $srvid);
		$this->db->where('sem_target', 1);
		$this->db->join('tblfields', 'tblsrvseminars.sem_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvseminars');
		return $query->result_array();
	}

	function getTrainings($srvid){
		$this->db->where('tra_srv_id', $srvid);
		$this->db->where('tra_target', 1);
		$this->db->join('tblfields', 'tblsrvtrainings.tra_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvtrainings');
		return $query->result_array();
	}

	function getProjects($srvid){
		$this->db->where('prj_srv_id', $srvid);
		$this->db->where('prj_target', 1);
		$this->db->join('tblfields', 'tblsrvprojects.prj_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvprojects');
		return $query->result_array();
	}

	function getPapers($srvid, $target){
		$this->db->where('pap_srv_id', $srvid);
		$this->db->where('pap_target', $target);
		$this->db->join('tblfields', 'tblsrvpaper.pap_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvpaper');
		return $query->result_array();
	}

	function getStudMentors($srvid){
		$this->db->where('men_srv_id', $srvid);
		$this->db->where('men_target', 1);
		$this->db->join('tblfields', 'tblsrvmentoring.men_field = tblfields.fld_id', 'left');
		$this->db->join('tblinstitutions', 'tblsrvmentoring.men_institution = tblinstitutions.ins_id', 'left');
		$query = $this->db->get('tblsrvmentoring');
		return $query->result_array();
	}

	function getCurriculum($srvid){
		$this->db->where('cur_srv_id', $srvid);
		$this->db->where('cur_target', 1);
		$this->db->join('tblfields', 'tblsrvcurriculum.cur_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvcurriculum');
		return $query->result_array();
	}

	function getNetworks($srvid){
		$this->db->where('net_srv_id', $srvid);
		$this->db->where('net_target', 1);
		$this->db->join('tblfields', 'tblsrvnetworks.net_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvnetworks');
		return $query->result_array();
	}

	function getResearch($srvid){
		$this->db->where('res_srv_id', $srvid);
		$this->db->where('res_target', 1);
		$this->db->join('tblfields', 'tblsrvresearches.res_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvresearches');
		return $query->result_array();
	}

	function getOthers($srvid){
		$this->db->where('oth_srv_id', $srvid);
		$this->db->where('oth_target', 1);
		$this->db->join('tblfields', 'tblsrvothers.oth_field = tblfields.fld_id', 'left');
		$query = $this->db->get('tblsrvothers');
		return $query->result_array();
	}

	function getDuration($srvid)
	{
		$this->db->select('min(con_date_from) as condatefrom, max(con_date_to) as condateto');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return array('datefrom'=> $res['condatefrom'], 'dateto'=> $res['condateto']);
		endforeach;
		
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */