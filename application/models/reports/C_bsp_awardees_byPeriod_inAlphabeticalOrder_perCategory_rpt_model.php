
<?php
class C_bsp_awardees_byPeriod_inAlphabeticalOrder_perCategory_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('List of Balik Scientist Awardees by Period in Alphabetical Order per Category');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $arrData['txtYear1'];
		$intYear2 = $arrData['txtYear2'];

		$yrcaption = ($intYear1 == $intYear2) ? $intYear1 : $intYear1.'-'.$intYear2;
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"List of Balik Scientist Awardees (".$yrcaption.")",0,1,'C');
		$this->fpdf->SetFont('Arial','',11);
		$this->fpdf->Cell(0,5,"(Alphabetical Order)",0,1,'C');
		$this->fpdf->Ln();
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->setFillColor(230,230,230);

		$this->fpdf->SetX(38);
		$this->fpdf->Cell(81,5,'','',0,'C',0);
		$this->fpdf->Cell(50,5,'Category','TRL',0,'C',1);

		$this->fpdf->Ln();
		$this->fpdf->SetX(38);
		$this->fpdf->Cell(11,5,'','LTRB',0,'C',1);
		$this->fpdf->Cell(50,5,'Name of Balik Scientist','TRLB',0,'C',1);
		$this->fpdf->Cell(20,5,'Year/s','TRLB',0,'C',1);
		$this->fpdf->Cell(25,5,'Short Term','TRLB',0,'C',1);
		$this->fpdf->Cell(25,5,'Long Term','TRLB',0,'C',1);

		$awardees = $this->getbspAwardee($intYear1, $intYear2);
		$ctr_awardee = 1;
		foreach($awardees as $awardee):
			$this->fpdf->Ln();
			$this->fpdf->SetX(38);
			$this->fpdf->SetFont('Arial','',8);
			$app_date = ($awardee['srv_sched_approvaldate'] != '' or $awardee['srv_sched_approvaldate']!= '0000-00-00') ? $awardee['srv_approval_date'] : $awardee['srv_sched_approvaldate'];
			$this->fpdf->Cell(11,5,$ctr_awardee++,'TLRB',0,'C',0);
			$this->fpdf->Cell(50,5,getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']),'TRB',0,'L',0);
			$this->fpdf->Cell(20,5,date('Y', strtotime($app_date)),'TRB',0,'C',0);
			$this->fpdf->SetFont('ZapfDingbats','',8);
			$this->fpdf->Cell(25,5,($awardee['srv_typeofaward']==0) ? '4' : '','TRB',0,'C',0);
			$this->fpdf->Cell(25,5,($awardee['srv_typeofaward']==1) ? '4' : '','TRB',0,'C',0);
		endforeach;

		$this->fpdf->Ln(15);
		$this->fpdf->SetX(38);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(0,5,'Note: There are scientists with more than 1 stint/service.','',0,'L',0);
		$this->fpdf->Ln();
		$this->fpdf->SetX(46);
		$this->fpdf->Cell(0,5,'There are scientists who served under both short term and long term.','',0,'L',0);
		
	}

	function getbspAwardee($intYear1, $intYear2)
	{
		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'right');
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(srv_sched_approvaldate = '' or srv_sched_approvaldate is NULL or srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblsciservice.srv_isdeleted = 0 ");
		$query = $this->db->order_by('tblscientist.sci_last_name');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_date_from asc');
		$query = $this->db->get();
		return $query->result_array();
	}


	function getMaxPhase(){
		$sql = "select max(count) as totalphase from (
					select con_srv_id, count(*) as count FROM (
						select * FROM `tblsrvcontractdates`
							where con_srv_id in (
								select srv_id from tblsciservice 
									where srv_sched_approvaldate >= '2013-01-01' and srv_sched_approvaldate <= '2016-12-31') 
								) as tbla group by con_srv_id
						) as tblb";
		$query = $this->db->query($sql);
		foreach($query->result_array() as $res):
			return $res['totalphase'];
		endforeach;
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */