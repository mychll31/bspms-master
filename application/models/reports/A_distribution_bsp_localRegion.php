
<?php
class A_distribution_bsp_localRegion extends CI_Model {

	var $widths;
	var $aligns;

	public function __construct()
	{
		$this->load->database();
		$this->load->model('Reports_model');
	}

	function generate()
	{
		$intYear1 = $_GET['txtYear1'];
		$intYear2 = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$ctr = 1;
		$countall = $this->Reports_model->countallserviceAward_byregion($intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
		$total_count = 0;

		$local_regions = array(
		      	'1'  => 'NCR - Metro Manila',
		      	'2'  => 'Region I - Ilocos Region',
		      	'3'  => 'CAR - Benguet',
		      	'4'  => 'Region II - Cagayan Valley',
		      	'5'  => 'Region III - Central Luzon',
		      	'6'  => 'Region IV-A - CALABARZON',
		      	'7'  => 'Region IV-B - MIMAROPA',
		      	'8'  => 'Region V - Bicol Region',
		      	'9'  => 'Region VI - Western Visayas',
		      	'10' => 'Region VII - Central Visayas',
		      	'11' => 'Region VIII - Eastern Visayas',
		      	'12' => 'Region IX - Zamboanga Peninsula',
		      	'13' => 'Region X - Northern Mindanao',
		      	'14' => 'Region XI - Davao Region',
		      	'15' => 'Region XII - Mindanao',
		      	'16' => 'Region XIII - Caraga',
		      	'17' => 'Region NIR - Negros Island Region',
		      	'18' => 'ARMM - Cotobato City',
		 );

		## BEGIN Export to Excel
		if(isset($_GET['excel'])){
			 header("Content-type: application/vnd.ms-excel");
			 header("Content-Disposition: attachment; filename=Distribution of BSP Awardees per Local Region.xls");
			?>
				<table>
					<tr>
						<th colspan="3">BALIK SCIENTIST PROGRAM</th>
					</tr>
					<tr>
						<th colspan="3">Distribution of BSP Awardees per Local Region (<?=$intYear1?> - <?=$intYear2?>)</th>
					</tr>
				</table><br>
				
				<table border="1">
					<tr>
						<th>Local Region - Province [1]</th>
						<th>No. of Balik Scientists [2] </th>
						<th>% [3]</th>
					</tr>
					<?php
					foreach($local_regions as $key=>$region):
						$awardsCount = $this->Reports_model->countserviceAward_byregion($key, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
						if($awardsCount<1)
								$total_count = 0;
							else
								$total_count = $awardsCount==0? 0 : round(($awardsCount/$countall)*100,2);
							echo '<tr>';
								echo '<td>'.$region.'</td>';
								echo '<td>'.$awardsCount.'</td>';
								echo '<td>'.$total_count.'</td>';
							echo '</tr>';
						endforeach;
					?>
					<tr>
						<th>TOTAL</th>
						<th><?=$countall?></th>
						<th></th>
					</tr>

				</table>
				<!-- BEGIN FOOTNOTE -->
				<br><br><br><br><br>
				[1] List of Regions.<br>
				[2] Number of Balik Scientist per area of Region for the year <?=getYearDuration($intYear1, $intYear2)?> based on their approval date.<br>
				[3] Percentage of [2] over the Total Number of Balik Scientist Awardees per Region for the year.
				<!-- END FOOTNOTE -->

		<?php
		# END Export to Excel; # BEGIN Export to PDF
		}else{
			$this->fpdf->SetTitle('Distribution of BSP Awardees per Local Region');
			$this->fpdf->SetLeftMargin(30);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			$this->fpdf->AddPage('P','','A4');
				
			$this->fpdf->SetFont('Arial','B',11);

			$this->fpdf->SetFont('Arial','B',9);
			$widths = array(70,60,20);
			$border = array(1,1,1);
			$align = array('L', 'C', 'C');
			$this->fpdf->SetWidths($widths);

			$this->fpdf->SetFont('Arial','',9);
			$ctr = 1;
			$countall = $this->Reports_model->countallserviceAward_byregion($intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
			$total_count = 0;
			foreach($local_regions as $key=>$region):
				$awardsCount = $this->Reports_model->countserviceAward_byregion($key, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
				if($awardsCount<1)
					$total_count = 0;
				else
					$total_count = $awardsCount==0? 0 : round(($awardsCount/$countall)*100,2);
				$caption = array(' '.$ctr++.'.  '.$region,$awardsCount,$total_count);
				$this->fpdf->FancyRow($caption,$border,$align);
			endforeach;
			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(70,5,'TOTAL','LTBR',0,'C',0);
			$this->fpdf->Cell(60,5,$countall,'LTBR',0,'C',0);	
			$this->fpdf->Cell(20,5,'','LTBR',0,'C',0);

			## FOOTNOTE
			$this->fpdf->SetY(250);
			$this->fpdf->Cell(100,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'List of Regions.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Number of Balik Scientist per area of Region for the year '.getYearDuration($intYear1, $intYear2).' based on their approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[3]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Percentage of [2] over the Total Number of Balik Scientist Awardees per Region for the year.',0,0,'L');
		}
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */