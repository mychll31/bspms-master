<?php
class C_list_pending_erp extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('List of Pending Exit Report Presentations (ERPs) by Year');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $arrData['txtYear'];

		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"List of Pending Exit Report Presentations (ERPs) for CY ".$intYear1,'',0,'C',0);
		$this->fpdf->Ln();
		$this->fpdf->Ln();
		$this->fpdf->SetFont('Arial','B',9);
		$widths = array(20,40,40,35,35);
		$border = array(1,1,1,1,1);
		$align = array('C','C','C','C','C');

		$this->fpdf->setFillColor(230,230,230);
		$this->fpdf->Cell(20,5,'Count',1,0,'C',1);
		$this->fpdf->Cell(40,5,'Name of Balik Scientist',1,0,'C',1);
		$this->fpdf->Cell(40,5,'Host Institution',1,0,'C',1);
		$this->fpdf->Cell(35,5,'Date of end of service',1,0,'C',1);
		$this->fpdf->Cell(35,5,'Reason for delay',1,1,'C',1);

		$ctr=1;
		$this->fpdf->SetFont('Arial','',9);
		$erps = $this->getErp($intYear1);
		foreach($erps as $erp):
			$widths = array(20,40,40,35,35);
			$border = array(1,1,1,1,1);
			$align = array('C','C','C','C','C');
			$caption = array(
					$ctr++,
					getFullname($erp['sci_last_name'], $erp['sci_first_name'], $erp['sci_middle_name'], $erp['sci_middle_initial']),
					$erp['ins_desc'],
					$this->getEndofService($erp['srv_id']),
					$erp['srv_erp_reason'],);
			
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($caption,$border,$align);
		endforeach;

		$this->fpdf->Ln(5);

		$this->fpdf->SetFont('Arial','B',9);
		$ctr = $ctr - 1;
		$this->fpdf->Cell(38,5,'Total No. of Pending ERPs: '.$ctr,'',0,'L',0);
		
	}

	function getErp($intYear1)
	{
		$this->db->select('*');
		$this->db->from('tblsciservice');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$this->db->join('tblinstitutions', 'tblsciservice.srv_ins_id = tblinstitutions.ins_id', 'left');
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear1."-12-31'");
		$query = $this->db->where("tblsciservice.srv_with_erp", 0);
		$query = $this->db->where("tblscientist.sci_isdeleted = 0");
		$query = $this->db->order_by('tblscientist.sci_last_name');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	function getExpertise($expertid)
	{
		$this->db->select('*');
		$this->db->from('tblexpertise');
		$query = $this->db->where('exp_id='.$expertid);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['exp_code'];
		endforeach;
		
	}

	function getPriority($priority_id)
	{
		$this->db->select('*');
		$this->db->from('tblpriorityareas');
		$query = $this->db->where('pri_id='.$priority_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['pri_number'];
		endforeach;
		
	}

	function getOutcomes($outcome_id)
	{
		$this->db->select('*');
		$this->db->from('tbloutcomes');
		$query = $this->db->where('out_id='.$outcome_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['out_number'];
		endforeach;
		
	}

	function getHostInst($inst_id)
	{
		$this->db->select('*');
		$this->db->from('tblinstitutions');
		$query = $this->db->where('ins_id='.$inst_id);
		$query = $this->db->limit(1);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['ins_code'];
		endforeach;
		
	}

	function getEndofService($srvid)
	{
		$this->db->select('max(con_date_to) as condate');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->get();
		foreach($query->result_array() as $res):
			return $res['condate'];
		endforeach;
		
	}

	function getDuration($sdate, $edate){
		// duration
		$date1 = date('d-M-Y', strtotime($sdate));
		$date2 = date('d-M-Y', strtotime($edate));

		$diff = abs(strtotime($date2) - strtotime($date1));
		$years = floor($diff / (365*60*60*24));
		$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
		$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

		$dateDuration = ($days == 0 ? '' : ($days == 1 ? $days.' day ': $days.' days '));
		$dateDuration .= ($months == 0 ? '' : ($months == 1 ? $months.' month ': $months.' months '));
		$dateDuration .= ($years == 0 ? '' : ($years == 1 ? $years.' year ': $years.' years '));

		return $dateDuration;
	}

	function generatePage($intTerm, $intYear1){
		
		$awardees = $this->getbspAwardee($intTerm, $intYear1);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(0,5,"A. ".($intTerm == 0 ? 'Short-Term Category' : 'Long-Term Category')." (".count($awardees).")",0,1,'L');
		$this->fpdf->Ln();

		$widths = array(12,28,24,21,19,23,35);
		$border = array(1,1,1,1,1,1,1);
		$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C');
		$this->fpdf->SetWidths($widths);
				
		$caption = array('Count',
						 'Name of Balik Scientis',
						 'Area of Expertise',
						 'DOST Priority Area',
						 'DOST Outcomes',
						 'Host Institution/s',
						 'Duration');
		$this->fpdf->FancyRow($caption,$border,$align);

		$this->fpdf->SetFont('Arial','',8);
		$count = 1;

		// dd($awardees);
		foreach ($awardees as $awardee) {
			$duration = '';
			$approvedDate = ($awardee['srv_sched_approvaldate'] == '0000-00-00') ? date('d-M-Y', strtotime($awardee['srv_approval_date'])) : date('d-M-Y', strtotime($awardee['srv_sched_approvaldate']));
			$fullname = $awardee['sci_last_name'].', '.$awardee['sci_first_name'];
			
			//Expertise
			$expertise = explode('|', $awardee['sci_expertise_id']);
			$expertDesc = '';
			foreach ($expertise as $expert) {
				if($expert != ''){
					$expertDesc.=$this->getExpertise($expert).', ';
				}
			}

			//Priority Areas
			$priorityAreas = explode('|', $awardee['srv_pri_id']);
			$priorityDesc = '';
			foreach ($priorityAreas as $priority) {
				if($priority){
					$priorityDesc.=$this->getPriority($priority).', ';
				}
			}

			//Outcomes
			$outcomes = explode('|', $awardee['srv_out_id']);
			$outcomeDesc = '';
			foreach ($outcomes as $outcome) {
				if($outcome){
					$outcomeDesc.=$this->getOutcomes($outcome).', ';
				}
			}

			// duration
			if($awardee['srv_type_contract'] == 1){
				$contractDates = $this->getContractDates($awardee['srv_id']);
				$ctrphase = 1;
				foreach ($contractDates as $contract) {
					$dateDuration = $this->getDuration(date('d-M-Y', strtotime($contract['con_date_from'])), date('d-M-Y', strtotime($contract['con_date_to'])));
					$duration.='Phase'.$ctrphase++.':'. date('d-M-Y', strtotime($contract['con_date_from'])).' to '. date('d-M-Y', strtotime($contract['con_date_to'])).' ('.$dateDuration.');';
				}
			}else{
				$dateDuration = $this->getDuration(date('d-M-Y', strtotime($awardee['srv_cont_startDate'])), date('d-M-Y', strtotime($awardee['srv_cont_endDate'])));
				$duration.=date('d-M-Y', strtotime($awardee['srv_cont_startDate'])).' to '. date('d-M-Y', strtotime($awardee['srv_cont_endDate'])).' ('.$dateDuration.')';
			}

			$widths = array(12,28,24,21,19,23,35);
			$border = array(1,1,1,1,1,1,1);
			$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C');
			$this->fpdf->SetWidths($widths);
			$hostinst = explode('|', $awardee['srv_ins_id']);
			$hostinstitutions = '';
			foreach ($hostinst as $host) {
				$hostval = $this->getHostInst($host);
				$hostinstitutions.=$hostval.';';
			}
			$caption = array(
							$count++,
							$fullname,
							trim($expertDesc, ', '),
							trim($priorityDesc, ', '),
							trim($outcomeDesc, ', '),
							$hostinstitutions, //$this->getHostInst($awardee['srv_ins_id']),
							$duration);
			$this->fpdf->FancyRow($caption,$border,$align);

		}// end foreach awardees
	}


}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */