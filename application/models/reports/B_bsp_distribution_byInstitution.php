<?php
class B_bsp_distribution_byInstitution extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model(array('Reports_model'));	
	}

	function generate()
	{
		$intYear1 = $_GET['txtYear1'];
		$intYear2 = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$arrAwardees = $this->Reports_model->getServiceAwards($intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);

		## BEGIN Export to Excel
		if(isset($_GET['excel'])){
			 header("Content-type: application/vnd.ms-excel");
			 header("Content-Disposition: attachment; filename=Distribution of BSP Awardees by Type of Host Institution.xls");
			?>
			<table>
				<tr>
					<th colspan="12">BALIK SCIENTIST PROGRAM</th>
				</tr>
				<tr>
					<th colspan="12">Distribution of BSP Awardee by Type of Host Institution (<?=$intYear1?> - <?=$intYear2?>)</th>
				</tr>
			</table><br>

			<table border="1">
				<tr>
					<th rowspan="2">Date of Approval [1]</th>
					<th rowspan="2">Name of Balik Scientist [2]</th>
					<th rowspan="2">Host Institution [3]</th>
					<th colspan="5">Type of Host Institution(classification 1) [4]</th>
					<th colspan="4">Type of Host Institution(classification 2) [5]</th>
				</tr>
				<tr>
					<th>Up System</th>
					<th>SUCs</th>
					<th>LGUs/GS</th>
					<th>PUCs</th>
					<th>NGOs/Foundation/Private</th>
					<th>Up+SUCs</th>
					<th>LGUs/Government Sector</th>
					<th>PUCs</th>
					<th>NGOs/Foundation/Private</th>
				</tr>
			<?php
				foreach($arrAwardees as $awardee):
					echo '<tr>';
						echo '<td align="center">'.getApprovalDate($awardee['srv_approval_date'], $awardee['srv_sched_approvaldate']).'</td>';
						echo '<td>'.getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']).'</td>';
						echo '<td align="center">'.$awardee['ins_code'].'</td>';
						echo '<td align="center">'.$this->Reports_model->ups1($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->sucs1($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->lugs1($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->pucs1($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->ngos1($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->ups2($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->lugs2($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->pucs2($awardee['itype_id']).'</td>';
						echo '<td align="center">'.$this->Reports_model->ngos2($awardee['itype_id']).'</td>';
					echo '</tr>';
				endforeach;
			?>
			</table>
			<!-- BEGIN FOOTNOTE -->
			<br><br><br><br><br>
			[1] Date of Approval letter for the year <?=getYearDuration($intYear1, $intYear2)?> based on the approval date.<br>
			[2] Name of Balik Scientist for the year <?=getYearDuration($intYear1, $intYear2)?> based on their approval date.<br>
			[3] List of Host Institution of [2] based on the General Information of their Service Awards.<br>
			[4] Type of Host Institution of [3] - Classification 1.<br>
			[5] Type of Host Institution of [3] - Classification 2.<br>
			<!-- END FOOTNOTE -->
		<?php
		# END Export to Excel, BEGIN Export to PDF
		}else{
			$this->fpdf->SetTitle('Distribution of BSP Awardees by Type of Host Institution');
			$this->fpdf->SetLeftMargin(20);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			$this->fpdf->AddPage('P','','A4');
				
			$this->fpdf->SetFont('Arial','B',11);
				
			$this->fpdf->SetFont('Arial','',7);
			$widths = array(16,24,24,11,10,10,11,14,11,16,11,18);
			$border = array(1,1,1,1,1,1,1,1,1,1,1,1);
			$align = array('C','L','C','C','C','C','C','C','C','C','C','C');
			$this->fpdf->SetWidths($widths);

			foreach ($arrAwardees as $awardee):
				$caption = array(
							getApprovalDate($awardee['srv_approval_date'], $awardee['srv_sched_approvaldate']),
							getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']),
							$awardee['ins_code'],
							$this->Reports_model->ups1($awardee['itype_id']),
							$this->Reports_model->sucs1($awardee['itype_id']),
							$this->Reports_model->lugs1($awardee['itype_id']),
							$this->Reports_model->pucs1($awardee['itype_id']),
							$this->Reports_model->ngos1($awardee['itype_id']),
							$this->Reports_model->ups2($awardee['itype_id']),
							$this->Reports_model->lugs2($awardee['itype_id']),
							$this->Reports_model->pucs2($awardee['itype_id']),
							$this->Reports_model->ngos2($awardee['itype_id']));
				$this->fpdf->FancyRow($caption,$border,$align);
			endforeach;

			## FOOTNOTE
			$this->fpdf->SetY(249);
			$this->fpdf->Cell(100,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Date of Approval letter for the year '.getYearDuration($intYear1, $intYear2).' based on the approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Name of Balik Scientist for the year '.getYearDuration($intYear1, $intYear2).' based on their approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[3]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'List of Host Institution of [2] based on the General Information of their Service Awards.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[4]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Type of Host Institution of [3] - Classification 1.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[5]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Type of Host Institution of [3] - Classification 2.',0,0,'L');
		}
	}
	
	

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */