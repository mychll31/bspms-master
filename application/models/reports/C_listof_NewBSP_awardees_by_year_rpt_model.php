<?php
class C_listof_NewBSP_awardees_by_year_rpt_model extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
		$this->load->model(array('Reports_model'));
	}

	function generate()
	{
		$intYear1 = $_GET['txtYear1'];
		$intYear2 = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard = isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		if(!isset($_GET['excel'])){
			$this->fpdf->SetTitle('List of New BSP Awardees by Year');
			$this->fpdf->SetLeftMargin(20);
			$this->fpdf->SetRightMargin(20);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			$this->fpdf->AddPage('P','','A4');
			
			$this->fpdf->SetFont('Arial','B',11);
		}else{
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=List of New BSP Awardees by Year.xls");
		}

		$this->generatePage(0, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
		if(!isset($_GET['excel'])){
			$this->fpdf->Ln(10);
			$this->fpdf->reset_thisnewPage();
			$this->fpdf->AddPage();
		}
		$this->generatePage(1, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);

		$totalCount = count($this->Reports_model->getNewbspAwardee(0, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd)) + count($this->Reports_model->getNewbspAwardee(1, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd));


		if(!isset($_GET['excel'])){	
			$this->fpdf->Ln(5);
			$this->fpdf->Ln(5);

			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(67,5,"Total No. of New BSP Awardees in CY Year ".$intYear1.':  '.$totalCount,'',0,'L',0);
		}else{
			echo '<br><br><br>
					Total No. of New BSP Awardees in CY Year '.getYearDuration($intYear1, $intYear2).': <b>'.$totalCount.'</b>';
		}

		## FOOTNOTE
		if(isset($_GET['excel'])){
			?>
			<br><br><br><br><br>
			[1] Date of Approval for the year <?=getYearDuration($intYear1, $intYear2)?> based on the approval date. <br>
			[2] Name of Balik Scientist for the year <?=getYearDuration($intYear1, $intYear2)?> based on their approval date. <br>
			[3] Area of Expertise of [2] <br>
			[4] Area of DOST Priority Area of [2] <br>
			[5] Area of DOST Outcomes of [2] <br>
			[6] Type of Host Institution of [2] <br>
			[7] Duration of Contract based on the Service Award of [2] <br>
			[8] Monitoring Council of [2] <br>

		<?php
		}else{
			$this->fpdf->reset_thisnewPage();
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->ln(20);
			$this->fpdf->Cell(120,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Date of Approval for the year '.getYearDuration($intYear1, $intYear2).' based on the approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Name of Balik Scientist for the year '.getYearDuration($intYear1, $intYear2).' based on their approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[3]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Area of Expertise of [2]',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[4]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Area of DOST Priority Area of [2]',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[5]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Area of DOST Outcomes of [2]',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[6]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Type of Host Institution of [2]',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[7]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Duration of Contract based on the Service Award of [2]',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[8]',0,0,'R');
			$this->fpdf->SetFont('Arial','',8);
			$this->fpdf->Cell(65,5,'Monitoring Council of [2]',0,0,'L');
		}
	}


	function generatePage($intTerm, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd){
		
		$awardees = $this->Reports_model->getNewbspAwardee($intTerm, $intYear1, $intYear2, $padmin, $pcaard, $pcieerd, $pchrd);
		# BEGIN Export Excel
		if(isset($_GET['excel'])){
			?>
				<table>
					<tr>
						<th colspan="9">BALIK SCIENTIST PROGRAM</th>
					</tr>
					<tr>
						<th colspan="9">List of New BSP Awardees by Year (<?=$intYear1?> - <?=$intYear2?>)</th>
					</tr>
				</table><br>

				<br><?="A. ".($intTerm == 0 ? 'Short-Term Category' : 'Long-Term Category')." (".count($awardees).")"?>

				<table border="1">
					<tr>
						<th>Count</th>
						<th>Date Approved</th>
						<th>Name of Balik Scientist</th>
						<th>Area of Expertise</th>
						<th>DOST Priority Area</th>
						<th>DOST Outcomes</th>
						<th>HOST Institution/s</th>
						<th>Duration</th>
						<th>Council</th>
					</tr>
					<?php
						$count = 1;
						foreach ($awardees as $awardee) {
							$duration = '';
							$approvedDate = ($awardee['srv_sched_approvaldate'] == '0000-00-00') ? date('d-M-Y', strtotime($awardee['srv_approval_date'])) : date('d-M-Y', strtotime($awardee['srv_sched_approvaldate']));
							$fullname = getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']);
							
							//Expertise
							$expertise = explode('|', $awardee['sci_expertise_id']);
							$expertDesc = '';
							foreach ($expertise as $expert) {
								if($expert != ''){
									$expertDesc.=$this->Reports_model->getExpertise($expert).', ';
								}
							}

							//Priority Areas
							$priorityAreas = explode('|', $awardee['srv_pri_id']);
							$priorityDesc = '';
							foreach ($priorityAreas as $priority) {
								if($priority){
									$priorityDesc.=$this->Reports_model->getPriority($priority).', ';
								}
							}

							//Outcomes
							$outcomes = explode('|', $awardee['srv_out_id']);
							$outcomeDesc = '';
							foreach ($outcomes as $outcome) {
								if($outcome){
									$outcomeDesc.=$this->Reports_model->getOutcomes($outcome).', ';
								}
							}

							// duration
							if($awardee['srv_type_contract'] == 1){
								$contractDates = $this->Reports_model->getContractDates($awardee['srv_id']);
								$ctrphase = 1;
								foreach ($contractDates as $contract) {
									$dateDuration = $this->Reports_model->getDuration(date('d-M-Y', strtotime($contract['con_date_from'])), date('d-M-Y', strtotime($contract['con_date_to'])));
									$duration.='Phase'.$ctrphase++.':'. date('d-M-Y', strtotime($contract['con_date_from'])).' to '. date('d-M-Y', strtotime($contract['con_date_to'])).' ('.$dateDuration.');';
								}
							}else{
								$dateDuration = $this->Reports_model->getDuration(date('d-M-Y', strtotime($awardee['srv_cont_startDate'])), date('d-M-Y', strtotime($awardee['srv_cont_endDate'])));
								$duration.=date('d-M-Y', strtotime($awardee['srv_cont_startDate'])).' to '. date('d-M-Y', strtotime($awardee['srv_cont_endDate'])).' ('.$dateDuration.')';
							}

							$widths = array(12,22,22,23,19,15,18,30,18);
							$border = array(1,1,1,1,1,1,1,1,1);
							$align = array('C', 'C', 'L', 'C', 'C', 'C', 'C', 'L', 'C');
							$this->fpdf->SetWidths($widths);
							$hostinst = explode('|', $awardee['srv_ins_id']);
							$hostinstitutions = '';
							foreach ($hostinst as $host) {
								$hostval = $this->Reports_model->getHostInst($host);
								$hostinstitutions.=$hostval.';';
							}
							
							echo '<tr>';
								echo '<td align="center">'.$count++.'</td>';
								echo '<td align="center">'.$approvedDate.'</td>';
								echo '<td>'.$fullname.'</td>';
								echo '<td align="center">'.trim($expertDesc, ', ').'</td>';
								echo '<td align="center">'.trim($priorityDesc, ', ').'</td>';
								echo '<td align="center">'.trim($outcomeDesc, ', ').'</td>';
								echo '<td align="center">'.rtrim($hostinstitutions, ";").'</td>';
								echo '<td>'.$duration.'</td>';
								echo '<td align="center">'.$this->Reports_model->getCouncil($awardee['srv_cil_id']).'</td>';
							echo '</tr>';
						} //endforeach
						?>
				</table>
				
		<?php
		# END Export Excel, BEGIN Export PDF
		}else{
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(0,5,"A. ".($intTerm == 0 ? 'Short-Term Category' : 'Long-Term Category')." (".count($awardees).")",0,1,'L');
			$this->fpdf->Ln();

			$widths = array(12,22,22,23,19,15,18,30,18);
			$border = array(1,1,1,1,1,1,1,1,1);
			$align = array('C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C');
			$this->fpdf->SetWidths($widths);

			$this->fpdf->setFillColor(230,230,230);
			$this->fpdf->Cell(12,5,'Count','LTR',0,'C',1);
			$this->fpdf->Cell(22,5,'Date Approved','LTR',0,'C',1);
			$this->fpdf->Cell(22,5,'Name of Balik','LTR',0,'C',1);
			$this->fpdf->Cell(23,5,'Area of','LTR',0,'C',1);
			$this->fpdf->Cell(19,5,'DOST','LTR',0,'C',1);
			$this->fpdf->Cell(15,5,'DOST','LTR',0,'C',1);
			$this->fpdf->Cell(18,5,'Host','LTR',0,'C',1);
			$this->fpdf->Cell(30,5,'Duration','LTR',0,'C',1);
			$this->fpdf->Cell(18,5,'Council','LTR',1,'C',1);
			$this->fpdf->Cell(12,5,'','LRB',0,'C',1);
			$this->fpdf->Cell(22,5,'','LRB',0,'C',1);
			$this->fpdf->Cell(22,5,'Scientist','LRB',0,'C',1);
			$this->fpdf->Cell(23,5,'Expertise','LRB',0,'C',1);
			$this->fpdf->Cell(19,5,'Priority Area','LRB',0,'C',1);
			$this->fpdf->Cell(15,5,'Outcomes','LRB',0,'C',1);
			$this->fpdf->Cell(18,5,'Institution/s','LRB',0,'C',1);
			$this->fpdf->Cell(30,5,'','LRB',0,'C',1);
			$this->fpdf->Cell(18,5,'','LRB',1,'C',1);

			$this->fpdf->SetFont('Arial','',8);
			$count = 1;

			foreach ($awardees as $awardee) {
				$duration = '';
				$approvedDate = ($awardee['srv_sched_approvaldate'] == '0000-00-00') ? date('d-M-Y', strtotime($awardee['srv_approval_date'])) : date('d-M-Y', strtotime($awardee['srv_sched_approvaldate']));
				$fullname = getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']);
				
				//Expertise
				$expertise = explode('|', $awardee['sci_expertise_id']);
				$expertDesc = '';
				foreach ($expertise as $expert) {
					if($expert != ''){
						$expertDesc.=$this->Reports_model->getExpertise($expert).', ';
					}
				}

				//Priority Areas
				$priorityAreas = explode('|', $awardee['srv_pri_id']);
				$priorityDesc = '';
				foreach ($priorityAreas as $priority) {
					if($priority){
						$priorityDesc.=$this->Reports_model->getPriority($priority).', ';
					}
				}

				//Outcomes
				$outcomes = explode('|', $awardee['srv_out_id']);
				$outcomeDesc = '';
				foreach ($outcomes as $outcome) {
					if($outcome){
						$outcomeDesc.=$this->Reports_model->getOutcomes($outcome).', ';
					}
				}

				// duration
				if($awardee['srv_type_contract'] == 1){
					$contractDates = $this->Reports_model->getContractDates($awardee['srv_id']);
					$ctrphase = 1;
					foreach ($contractDates as $contract) {
						$dateDuration = $this->Reports_model->getDuration(date('d-M-Y', strtotime($contract['con_date_from'])), date('d-M-Y', strtotime($contract['con_date_to'])));
						$duration.='Phase'.$ctrphase++.':'. date('d-M-Y', strtotime($contract['con_date_from'])).' to '. date('d-M-Y', strtotime($contract['con_date_to'])).' ('.$dateDuration.');';
					}
				}else{
					$dateDuration = $this->Reports_model->getDuration(date('d-M-Y', strtotime($awardee['srv_cont_startDate'])), date('d-M-Y', strtotime($awardee['srv_cont_endDate'])));
					$duration.=date('d-M-Y', strtotime($awardee['srv_cont_startDate'])).' to '. date('d-M-Y', strtotime($awardee['srv_cont_endDate'])).' ('.$dateDuration.')';
				}

				$widths = array(12,22,22,23,19,15,18,30,18);
				$border = array(1,1,1,1,1,1,1,1,1);
				$align = array('C', 'C', 'L', 'C', 'C', 'C', 'C', 'L', 'C');
				$this->fpdf->SetWidths($widths);
				$hostinst = explode('|', $awardee['srv_ins_id']);
				$hostinstitutions = '';
				foreach ($hostinst as $host) {
					$hostval = $this->Reports_model->getHostInst($host);
					$hostinstitutions.=$hostval.';';
				}
				$caption = array(
								$count++,
								$approvedDate,
								$fullname,
								trim($expertDesc, ', '),
								trim($priorityDesc, ', '),
								trim($outcomeDesc, ', '),
								rtrim($hostinstitutions, ";"), // $this->getHostInst($awardee['srv_ins_id']),
								$duration,
								$this->Reports_model->getCouncil($awardee['srv_cil_id']));
				$this->fpdf->FancyRow($caption,$border,$align);

			}// end foreach awardees

		} #END Export PDF
	}


}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */