<?php
class C_summary_report extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();
		$this->load->model(array('outcome_model','priority_model','country_model','citizenship_model','institutions_model','council_model','summary_report_model'));
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('Summary Report of BSP by Year');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('P','','A4');
		
		$this->fpdf->SetFont('Arial','B',9);
		
		$intYear1 = $arrData['txtYear1'];
		$intYear2 = $arrData['txtYear2'];
		
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"Summary Report of BSP Awardees in CY ".$intYear1.'-'.$intYear2,'',0,'R',0);
		$this->fpdf->Ln();
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','B',7);

		$yrbox_width = 10*(($intYear2 - $intYear1)+1);

		$this->fpdf->setFillColor(230,230,230);
		$this->fpdf->Cell(60,5,"Item Description",'LTRL',0,'C',1);
		$this->fpdf->Cell($yrbox_width,5,"Total Number per Year",'TRBL',0,'C',1);
		$this->fpdf->Cell(10,5,"Grand",'TRL',0,'C',1);
		$this->fpdf->Ln();

		$this->fpdf->Cell(60,5,"",'LRB',0,'C',1);
		for ($i=$intYear1; $i <= $intYear2; $i++) {
			$this->fpdf->Cell(10,5,$i,'RBTL',0,'C',1);
		}
		$this->fpdf->Cell(10,5,"Total",'RBL',0,'C',1);
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','',7);
		//SC
		$this->drawBox($intYear1, $intYear2,'Service Count (SC)', 1, 0);
		//HC
		$this->drawBox($intYear1, $intYear2,'Head Count (HC)', 2, 0);

		//Classification
		$this->drawColoredBox($yrbox_width+60+10, 'A. Classification of Application');
		$boxlabel = array(
				array('label'=>'New',				'box'=>0, 'head' => ''),
				array('label'=>'     - Short Term',	'box'=>0, 'head' => 3),
				array('label'=>'     - Long Term',	'box'=>0, 'head' => 4),
				array('label'=>'Subsequent',		'box'=>0, 'head' => ''),
				array('label'=>'     - Short Term',	'box'=>0, 'head' => 5),
				array('label'=>'     - Long Term',	'box'=>0, 'head' => 6),
			);
		foreach ($boxlabel as $label) {
			$this->drawBox($intYear1, $intYear2,$label['label'],$label['head'], 0);
		}

		//status
		$this->drawColoredBox($yrbox_width+60+10, 'B. Status');
		$boxlabel = array(
				array('label'=>'Completed',						'box'=>0, 'head'=> ''),
				array('label'=>'     - Short Term',				'box'=>0, 'head'=> 7),
				array('label'=>'     - Long Term',				'box'=>0, 'head'=> 8),
				array('label'=>'Continuing/Approved from Year',	'box'=>0, 'head'=> ''),
				array('label'=>'     - Short Term',				'box'=>0, 'head'=> 9),
				array('label'=>'     - Long Term',				'box'=>0, 'head'=> 10),
				array('label'=>'Ongoing',						'box'=>0, 'head'=> ''),
				array('label'=>'     - Short Term',				'box'=>0, 'head'=> 11),
				array('label'=>'     - Long Term',				'box'=>0, 'head'=> 12),
				array('label'=>'Deferral',						'box'=>0, 'head'=> 13),
				array('label'=>'Disapproved',					'box'=>0, 'head'=> 14),
				array('label'=>'Extension',						'box'=>0, 'head'=> 15),
				array('label'=>'Shortening',					'box'=>0, 'head'=> 16),
				array('label'=>'Repartriated',					'box'=>0, 'head'=> 17),
			);
		foreach ($boxlabel as $label) {
			$this->drawBox($intYear1, $intYear2,$label['label'], $label['head'], 0);
		}

		//status
		$this->drawColoredBox($yrbox_width+60+10, 'C. Gender');
		$boxlabel = array(
				array('label'=>'Female','box'=>0,'head'=> 18),
				array('label'=>'Male','box'=>0,'head'=> 19),
			);
		foreach ($boxlabel as $label) {
			$this->drawBox($intYear1, $intYear2,$label['label'], $label['head'], 0);
		}

		//DOST Outcomes
		$this->drawColoredBox($yrbox_width+60+10, 'D. DOST Outcomes');
		$boxlabel_outcomes = $this->outcome_model->getAll();
		$ctr_out = 1;
		foreach ($boxlabel_outcomes as $outcomes) {
			$this->drawBox_summaryIter($intYear1, $intYear2,'     '.$ctr_out++.'. '.$outcomes['out_desc'],$outcomes['out_id'], 'srv_out_id');
		}

		//DOST Priority Areas
		$this->drawColoredBox($yrbox_width+60+10, 'E. DOST Priority Areas');
		$ctr_prio = 1;
		$boxlabel_priority = $this->priority_model->getAll();
		foreach ($boxlabel_priority as $priority) {
			$this->drawBox_summaryIter($intYear1, $intYear2,'     '.$ctr_prio++.'. '.$priority['pri_desc'], $priority['pri_id'], 'srv_pri_id');
		}

		//Countries
		$this->drawColoredBox($yrbox_width+60+10, 'F. Countries of Origin');
		$ctr_country = 1;
		$boxlabel_countries = $this->country_model->getAll();
		foreach ($boxlabel_countries as $country) {
			$totalsci_bycountries = $this->summary_report_model->totalCount_summary_iter($intYear1, $intYear2, $country['glo_id'], 'srv_glo_id');
			if($totalsci_bycountries > 0)
				$this->drawBox_summaryIter($intYear1, $intYear2,'     '.$ctr_country++.'. '.$country['glo_country'],$country['glo_id'],'srv_glo_id');
		}

		//Citizenship
		$this->drawColoredBox($yrbox_width+60+10, 'G. Citizenship');
		$boxlabel_citizens = $this->citizenship_model->getAll();
		$ctron1 = 1;
		$ctron2 = 1;
		foreach ($boxlabel_citizens as $citizen) {
			if($citizen['cit_type']==1){
				$this->drawBox_summaryIter($intYear1, $intYear2,$citizen['cit_particulars'], $citizen['cit_id'], 'srv_cit_id');
			}
			if($citizen['cit_type']==2){
				if($ctron1==1){
					$this->fpdf->SetFont('Arial','B',7);
					$this->drawBox($intYear1, $intYear2,'Foreign:', '', 0);
					$ctron1=0;
				}
				if($ctron1==0){
					$this->fpdf->SetFont('Arial','',7);
					$this->drawBox_summaryIter($intYear1, $intYear2,'     - '.$citizen['cit_particulars'], $citizen['cit_id'], 'srv_cit_id');
				}
			}
			if($citizen['cit_type']==3){
				if($ctron2==1){
					$this->fpdf->SetFont('Arial','B',7);
					$this->drawBox($intYear1, $intYear2,'Dual citizen:', '', 0);
					$ctron2=0;
				}
				if($ctron2==0){
					$this->fpdf->SetFont('Arial','',7);
					$totalsci_bycitizen = $this->summary_report_model->totalCount_summary_iter($intYear1, $intYear2, $citizen['cit_id'], 'srv_cit_id');
					if($totalsci_bycitizen > 0)
						$this->drawBox_summaryIter($intYear1, $intYear2,'     - '.$citizen['cit_particulars'], $citizen['cit_id'], 'srv_cit_id');
				}
			}
		}

		//Host Institution
		$this->drawColoredBox($yrbox_width+60+10, 'H. Host Institution');
		$ctr_ins = 1;
		$boxlabel_institution = $this->institutions_model->getInstitutionList();
		foreach ($boxlabel_institution as $institution) {
			$totalsci_bycitizen = $this->summary_report_model->totalCount_summary_iter($intYear1, $intYear2, $institution['ins_id'], 'srv_ins_id');
			if($totalsci_bycitizen > 0)
				$this->drawBox_summaryIter($intYear1, $intYear2,'     '.$ctr_ins++.'. '.$institution['ins_desc'], $institution['ins_id'], 'srv_ins_id');
		}

		//Type of Institution
		$this->drawColoredBox($yrbox_width+60+10, 'I. Type of Host Institution');
		$ctr_ins = 1;
		$boxlabel_insType = $this->institutions_model->getInstitutionTypes();
		foreach ($boxlabel_insType as $ins_type) {
			$this->drawBox_summaryIter($intYear1, $intYear2,'     '.$ctr_ins++.'. '.$ins_type['itype_desc'], $ins_type['itype_id'], 'itype_id');
		}

		//Council
		$this->drawColoredBox($yrbox_width+60+10, 'J. Recommending council (count per Council)');
		$ctr_cil = 1;
		$boxlabel_council = $this->council_model->getAll();
		foreach ($boxlabel_council as $council) {
			$this->drawBox_summaryIter($intYear1, $intYear2,'     '.$ctr_cil++.'. '.$council['cil_code'], $council['cil_id'], 'srv_cil_id');
		}

		//Secretariat
		$this->drawColoredBox($yrbox_width+60+10, 'K. Assigned Staff / Secretariat');
		$ctr_cil = 1;
		$users = $this->summary_report_model->getStaff();
		foreach ($users as $staff) {
			$totalproc = $this->summary_report_model->totalCount_summary_iter($intYear1, $intYear2, $staff['usr_user_id'], 'srv_processed_by');
			if($totalproc > 0){
				$secname = getSecretariatName($staff['usr_fname'], $staff['usr_mname'], $staff['usr_lname']);
				$this->drawBox_summaryIter($intYear1, $intYear2,'     '.$ctr_cil++.'. '.$secname, $staff['usr_user_id'], 'srv_processed_by');
			}
		}

	}

	function drawBox($intYear1, $intYear2, $label, $head, $iter){

		$widths = array(60);
		$border = array(1);
		$align = array('L');
		$caption = array($label);
		$total_head = 0;
		//foreach period
		for ($i=$intYear1; $i <= $intYear2; $i++) {
			array_push($widths, 10);
			array_push($border, 1);
			array_push($align, 'C');
			$head_label = 0;
			$head_label = $this->getHead($head, $i);
			$total_head = $total_head + $head_label;
			array_push($caption, ($head_label==0) ? '' : $head_label);
		}
		// Grand Total
		array_push($widths, 10);
		array_push($border, 1);
		array_push($align, 'C');
		array_push($caption, ($total_head==0) ? '' : $total_head);
		//output
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);
	}

	function drawColoredBox($length,$label){
		$this->fpdf->SetFillColor(204,204,204);
		$this->fpdf->Cell($length,5,$label,'TLRB',0,'L',1);
		$this->fpdf->Ln();
	}

	function getHead($head, $year){
		if($head==1)  return $this->summary_report_model->serviceCount($year);
		if($head==2)  return $this->summary_report_model->HeadCount($year);
		if($head==3)  return $this->summary_report_model->class_new($year, 0);
		if($head==4)  return $this->summary_report_model->class_new($year, 1);
		if($head==5)  return $this->summary_report_model->class_subs($year, 0);
		if($head==6)  return $this->summary_report_model->class_subs($year, 1);
		if($head==7)  return $this->summary_report_model->term($year,0, 4);
		if($head==8)  return $this->summary_report_model->term($year,1, 4);
		if($head==9)  return $this->summary_report_model->term($year,0, 3);
		if($head==10) return $this->summary_report_model->term($year,1, 3);
		if($head==11) return $this->summary_report_model->term($year,0, 2);
		if($head==12) return $this->summary_report_model->term($year,1, 2);
		if($head==13) return $this->summary_report_model->status_changesched($year, 3);
		if($head==14) return $this->summary_report_model->status_noterm($year, 5);
		if($head==15) return $this->summary_report_model->status_changesched($year, 1);
		if($head==16) return $this->summary_report_model->status_changesched($year, 2);
		if($head==17) return $this->summary_report_model->status_noterm($year, 6);
		if($head==18) return $this->summary_report_model->gender($year, 'F');
		if($head==19) return $this->summary_report_model->gender($year, 'M');
	}

	function drawBox_summaryIter($intYear1, $intYear2, $label, $iterid, $field){
		$widths = array(60);
		$border = array(1);
		$align = array('L');
		$caption = array($label);
		$total_head = 0;
		//foreach period
		for ($i=$intYear1; $i <= $intYear2; $i++) {
			array_push($widths, 10);
			array_push($border, 1);
			array_push($align, 'C');
			$outc_label = $this->summary_report_model->count_summary_iter($i, $iterid, $field);
			$total_head = $total_head + $outc_label;
			array_push($caption, ($outc_label==0) ? '' : $outc_label);
		}
		// Grand Total
		array_push($widths, 10);
		array_push($border, 1);
		array_push($align, 'C');
		array_push($caption, ($total_head==0) ? '' : $total_head);
		//output
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */