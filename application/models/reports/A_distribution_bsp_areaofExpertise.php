
<?php
class A_distribution_bsp_areaofExpertise extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
		$this->load->model('expertise_model');
		$this->load->model('Reports_model');
	}

	function generate()
	{
		$intYearFrom = $_GET['txtYear1'];
		$intYearTo = $_GET['txtYear2'];

		$padmin = isset($_GET['chkadmin']) ? 1 : 0;
		$pcaard =  isset($_GET['chkpcaard']) ? 1 : 0;
		$pcieerd = isset($_GET['chkpcieerd']) ? 1 : 0;
		$pchrd = isset($_GET['chkpchrd']) ? 1 : 0;

		$expertise = $this->expertise_model->getAll();
		$e_name = '';
		$e_no = '';
		$totalCount = 0;

		## BEGIN Export to Excel
		if(isset($_GET['excel'])){
			header("Content-type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=Distribution of BSP Awardees According to Area of Expertise.xls");
			?>
			<table>
				<tr>
					<th colspan="3">BALIK SCIENTIST PROGRAM</th>
				</tr>
				<tr>
					<th colspan="3">Distribution of BSP Awardees According to Area of Expertise (<?=$intYearFrom?> - <?=$intYearTo?>)</th>
				</tr>
			</table><br>
				
			<table border="1">
				<tr>
					<th>Area of Expertise [1]</th>
					<th>No. of Balik Scientists per Area of Expertise [2]</th>
					<th>Name of Balik Scientist [3]</th>
				</tr>
				<?php
				foreach($expertise as $expert):
					$scientists = $this->Reports_model->getDistScientistByExpertise($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd, $expert['exp_id']);
					$ctr = 1;
					foreach($scientists as $scientist):
						if($ctr==1){
							$e_name = $expert['exp_desc'];
							$e_no = count($scientists);
							$totalCount = $totalCount + count($scientists);
						}else{
							$e_name = '';
							$e_no = '';
						}
						echo '<tr>';
							echo '<td> '.ucwords($e_name).'</td>';
							echo '<td align="center">'.$e_no.'</td>';
							echo '<td align="center">'.getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial']).'</td>';
						echo '</tr>';
						$ctr++;
					endforeach;
				endforeach;
				?>
				<tr><td colspan="3"></td></tr>
				<tr>
					<th colspan="3">Total No. of Balik Scientists: <?=$totalCount?> </th>
				</tr>
			</table>
			
			<!-- BEGIN FOOTNOTE -->
			<br><br><br><br><br>
			[1] List of Area of Expertise.<br>
			[2] Number of Balik Scientist per area of expertise for the year <?=getYearDuration($intYearFrom, $intYearTo)?> based on their approval date.<br>
			[3] Name of Balik Scientist per area of expertise for the year <?=getYearDuration($intYearFrom, $intYearTo)?> based on their approval date.
			<!-- END FOOTNOTE -->

		<?php
		# END Export to Excel, BEGIN Export to PDF
		}else{
		
			$this->fpdf->SetTitle('Distribution of BSP Awardees According to Area of Expertise');
			$this->fpdf->SetLeftMargin(25);
			$this->fpdf->SetRightMargin(25);
			$this->fpdf->SetTopMargin(20);
			$this->fpdf->SetAutoPageBreak("on",20);
			$this->fpdf->AddPage('P','','A4');
			
			$this->fpdf->SetFont('Arial','B',11);

			$this->fpdf->SetFont('Arial','B',9);
			$widths = array(60,41,60);
			$border = array(1,1,1);
			$align = array('L', 'C', 'C');
			$this->fpdf->SetWidths($widths);

			$this->fpdf->SetFont('Arial','',9);
			
			foreach($expertise as $expert):
				$scientists = $this->Reports_model->getDistScientistByExpertise($intYearFrom, $intYearTo, $padmin, $pcaard, $pcieerd, $pchrd, $expert['exp_id']);
				$ctr = 1;
				foreach($scientists as $scientist):
					if($ctr==1){
						$e_name = $expert['exp_desc'];
						$e_no = count($scientists);
						$totalCount = $totalCount + count($scientists);
					}else{
						$e_name = '';
						$e_no = '';
					}
					$caption = array(' '.ucwords($e_name),$e_no,getFullname($scientist['sci_last_name'], $scientist['sci_first_name'], $scientist['sci_middle_name'], $scientist['sci_middle_initial']));
					$this->fpdf->FancyRow($caption,$border,$align);
					$ctr++;
				endforeach;
				
			endforeach;

			$this->fpdf->Ln(5);
			$this->fpdf->Ln(5);

			$this->fpdf->SetFont('Arial','B',9);
			$this->fpdf->Cell(40,5,'Total No. of Balik Scientists: '.$totalCount,'',0,'L',0);

			## FOOTNOTE
			$this->fpdf->SetY(250);
			$this->fpdf->Cell(100,1,'','T',0,'R');
			$this->fpdf->ln(1);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[1]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'List of Area of Expertise.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[2]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Number of Balik Scientist per area of expertise for the year '.getYearDuration($intYearFrom, $intYearTo).' based on their approval date.',0,0,'L');
			$this->fpdf->ln(4);
			$this->fpdf->SetFont('Arial','B',8);
			$this->fpdf->Cell(5,5,'[3]',0,0,'R');
			$this->fpdf->SetFont('Arial','',9);
			$this->fpdf->Cell(65,5,'Name of Balik Scientist per area of expertise for the year '.getYearDuration($intYearFrom, $intYearTo).' based on their approval date.',0,0,'L');
		}
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */