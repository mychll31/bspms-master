
<?php
class D_bsp_project extends CI_Model {

	var $widths;
	var $aligns;


	public function __construct()
	{
		$this->load->database();		
	}

	function generate($arrData)
	{
		$this->fpdf->SetTitle('Project Assisted by Balik Scientist Awardees');
		$this->fpdf->SetLeftMargin(20);
		$this->fpdf->SetRightMargin(20);
		$this->fpdf->SetTopMargin(20);
		$this->fpdf->SetAutoPageBreak("on",20);
		$this->fpdf->AddPage('L','','A4');
		
		$this->fpdf->SetFont('Arial','B',11);
		
		$intYear1 = $arrData['txtYear1'];
		$intYear2 = $arrData['txtYear2'];

		if($arrData['optradio']=='byyear'){
			$intYear1 = $arrData['txtYear'];
			$intYear2 = $arrData['txtYear'];
		}

		$yrcaption = ($intYear1 == $intYear2) ? $intYear1 : $intYear1.'-'.$intYear2;
		$this->fpdf->Cell(0,5,"BALIK SCIENTIST PROGRAM",0,1,'C');
		$this->fpdf->Cell(0,5,"Accomplishments",0,1,'C');
		$this->fpdf->Ln();
		$this->fpdf->Cell(170,5,"Project Assisted by Balik Scientist Awardees in CY",'',0,'R',0);
		$this->fpdf->SetFont('Arial','UB',11);
		$this->fpdf->Cell(20,5,'  '.$yrcaption.'  ','',0,'L',0);
		$this->fpdf->Ln();
		$this->fpdf->Ln();

		$this->fpdf->SetFont('Arial','B',8);
		$widths = array(25,38,33,45,30,25,20,40);
		$border = array(1,1,1,1,1,1,1,1);
		$align = array('C','C','C','C','C','C','C','C');
		$caption = array('Field','Name of Balik Scientist Awardee','No. of Projects assisted per Awardee','Project Title','Objectives','Implementing Agency','Status','Contribution to the project');
		
		$this->fpdf->SetWidths($widths);
		$this->fpdf->FancyRow($caption,$border,$align);

		$this->fpdf->SetFont('Arial','',8);
		$awardees = $this->getbspAwardee($intYear1, $intYear2, 0, 0);
		$totalprojects = 0;
		$temp_srvid = '';
		$temp_srvfield = '';
		$countSci = '';
		$totalCountSci = 0;
		$status = array(
			'0' => 'New',
			'1' => 'Ongoing',
			'2' => 'Continuing/Approved from Year',
			'3' => 'Completed',
			'4' => 'Disapproved',
			'5' => 'Repatriated');
		foreach($awardees as $awardee):
			$totalCountSci++;
			$fullname = getFullname($awardee['sci_last_name'], $awardee['sci_first_name'], $awardee['sci_middle_name'], $awardee['sci_middle_initial']);
			$field = $awardee['fld_desc'];
			$countSci = $this->getbspAwardee($intYear1, $intYear2, $awardee['sci_id'], 1);

			if($temp_srvid!=$awardee['sci_id']){
				$temp_srvid = $awardee['sci_id'];
			}else{
				$fullname = '';
				$countSci = '';
			}

			if($temp_srvfield!=$awardee['prj_field']){
				$temp_srvfield = $awardee['prj_field'];
			}else{
				if($fullname=='')
					$field = '';
			}

			$widths = array(25,38,33,45,30,25,20,40);
			$border = array(1,1,1,1,1,1,1,1);
			$align = array('C','L','C','L','C','C','C','L');
			$caption = array(
				$field,
				$fullname,
				$countSci,
				$awardee['prj_title'],
				$awardee['prj_objective'],
				$awardee['prj_agency'],
				$status[$awardee['prj_status']],
				$awardee['prj_contributions']);
			
			$this->fpdf->SetWidths($widths);
			$this->fpdf->FancyRow($caption,$border,$align);
		endforeach;

		$this->fpdf->Ln(15);
		$this->fpdf->SetFont('Arial','B',8);
		$this->fpdf->Cell(0,5,'Total number of Projects assisted: '.number_format($totalCountSci),'',0,'L',0);
	}

	function getbspAwardee($intYear1, $intYear2, $sciid, $stat)
	{
		$this->db->select('*');
		$this->db->from('tblsrvprojects');
		$this->db->join('tblsciservice', 'tblsciservice.srv_id = tblsrvprojects.prj_srv_id', 'right');
		$this->db->join('tblscientist', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'right');
		$this->db->join('tblfields', 'tblfields.fld_id = tblsrvprojects.prj_field', 'right');
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$intYear1."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$intYear2."-12-31'");
		$query = $this->db->where("tblscientist.sci_isdeleted = 0");
		$query = $this->db->where("tblsrvprojects.prj_target = 1");
		if($stat!=0)
			$query = $this->db->where("tblscientist.sci_id = ".$sciid);
		$query = $this->db->order_by('tblscientist.sci_last_name');
		$query = $this->db->get();
		if($stat==0)
			return $query->result_array();
		else
			return $query->num_rows();
	}
	
	function getContractDates($srvid){
		$this->db->select('*');
		$this->db->from('tblsrvcontractdates');
		$query = $this->db->where('con_srv_id='.$srvid);
		$query = $this->db->order_by('con_date_from asc');
		$query = $this->db->get();
		return $query->result_array();
	}


	function getMaxPhase(){
		$sql = "select max(count) as totalphase from (
					select con_srv_id, count(*) as count FROM (
						select * FROM `tblsrvcontractdates`
							where con_srv_id in (
								select srv_id from tblsciservice 
									where srv_sched_approvaldate >= '2013-01-01' and srv_sched_approvaldate <= '2016-12-31') 
								) as tbla group by con_srv_id
						) as tblb";
		$query = $this->db->query($sql);
		foreach($query->result_array() as $res):
			return $res['totalphase'];
		endforeach;
	}

}
/* End of file Bm_rpt_model.php */
/* Location: ./application/models/reports/Bm_rpt_model.php */