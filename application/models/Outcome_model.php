<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Outcome_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database(); 
	}
	
	public function getOutcome($outcomeId)
	{

		$this->db->where("out_id", $outcomeId); 
		$this->db->order_by('out_number','ASC');
		$query = $this->db->get('tbloutcomes');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{
		$this->db->order_by('out_number','ASC');
		$query = $this->db->get('tbloutcomes'); 		
		return $query->result_array();
		
	}
 
 	public function addOutcome($arrOutcome)
	{
		$this->db->insert('tbloutcomes', $arrOutcome);
		return $this->db->insert_id();	
		 
	} 
	
	public function editOutcome($outcomeId,$arrOutcome)
	{
		$this->db->where('out_id', $outcomeId);  
		$this->db->update('tbloutcomes', $arrOutcome);
		return $this->db->affected_rows(); 	
	}
	
	public function deleteOutcome($outcomeId)
	{
		$this->db->where('out_id', $outcomeId);
		$this->db->delete('tbloutcomes');
		return $this->db->affected_rows(); 	
	}
	
	public function getOutcomeNumber($outcomeId)
	{
		$this->db->select('out_number');
		$this->db->from('tbloutcomes'); 
		$this->db->where('out_id !=', $outcomeId);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidOut()
	{ 
		$this->db->select("concat(out_number, '-', out_desc) as valout");
		$this->db->from('tbloutcomes');
		$query = $this->db->get();
		return $query->result_array(); 
	}
 
  	
}