<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Participant_model extends CI_Model {
 
	public function __construct() 
	{
           $this->load->database();   
	}
	
	public function getParticipant($participantId)
	{

		$this->db->where("par_id", $participantId);  
		$this->db->order_by('par_desc','ASC'); 
		$query = $this->db->get('tblparticipants');
		return $query->result_array();
	}
	  
 
   public function getAll()
	{
		$this->db->order_by('par_desc','ASC');
		$query = $this->db->get('tblparticipants'); 	 	
		return $query->result_array();
		
	}
 
 	public function addParticipant($arrParticipant)
	{
		$this->db->insert('tblparticipants', $arrParticipant);
		return $this->db->insert_id();	
		 
	}
	
	public function editParticipant($participantId,$arrParticipant)
	{
		$this->db->where('par_id', $participantId);  
		$this->db->update('tblparticipants', $arrParticipant);
		return $this->db->affected_rows(); 	
	}
	 
	public function deleteParticipant($participantId)
	{
		$this->db->where('par_id', $participantId);
		$this->db->delete('tblparticipants');
		return $this->db->affected_rows(); 	
	}
	
	public function getParticipantDesc($participantId)
	{
		$this->db->select('par_desc');
		$this->db->from('tblparticipants'); 
		$this->db->where('par_id !=', $participantId);
		$query = $this->db->get();
		return $query->result_array(); 
	}

	public function getValidParticipant()
	{ 
		$this->db->select("par_desc as valpart");
		$this->db->from('tblparticipants');
		$query = $this->db->get();
		return $query->result_array(); 
	}
 
  	
}