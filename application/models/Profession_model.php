<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profession_model extends CI_Model {
 
	public function __construct() 
	{
           $this->load->database();   
	}
	
	public function getProfession($professionId)
	{

		$this->db->where("prof_id", $professionId);  
		$this->db->order_by('prof_desc','ASC'); 
		$query = $this->db->get('tblprofessions');
		return $query->result_array();
	}
	 
 
   	public function getAll()
	{
		$this->db->order_by('prof_desc','ASC');
		$query = $this->db->get('tblprofessions'); 	 	
		return $query->result_array();
		
	}

	public function countscientist($profid, $yr)
	{
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$yr."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$yr."-12-31'");
		$this->db->where('sci_profession_id', $profid);
		$this->db->join('tblsciservice', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$query = $this->db->get('tblscientist');
		return $query->num_rows();
	}

	public function totalcountScientist_profession($yr){
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) >= '".$yr."-01-01'");
		$query = $this->db->where("if(tblsciservice.srv_sched_approvaldate = '0000-00-00', tblsciservice.srv_approval_date, tblsciservice.srv_sched_approvaldate) <= '".$yr."-12-31'");
		$this->db->join('tblsciservice', 'tblscientist.sci_id = tblsciservice.srv_sci_id', 'left');
		$query = $this->db->get('tblscientist');
		return $query->num_rows();
	}

 	public function addProfession($arrProfession)
	{
		$this->db->insert('tblprofessions', $arrProfession);
		return $this->db->insert_id();	
		 
	}
	
	public function editProfession($professionId,$arrProfession)
	{
		$this->db->where('prof_id', $professionId);  
		$this->db->update('tblprofessions', $arrProfession);
		return $this->db->affected_rows(); 	
	}
	 
	public function deleteProfession($professionId)
	{
		$this->db->where('prof_id', $professionId);
		$this->db->delete('tblprofessions');
		return $this->db->affected_rows(); 	
	}

	public function getProfdesc($profid)
	{
		$this->db->select('prof_desc');
		$this->db->from('tblprofessions');
		$this->db->where('prof_id !=', $profid);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidProf()
	{ 
		$this->db->select("prof_desc as valprof");
		$this->db->from('tblprofessions');
		$query = $this->db->get();
		return $query->result_array(); 
	}
 
  	
}