<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Specialization_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function getSpecialization($specializationId)
	{
		$this->db->where("spe_id", $specializationId); 
		$this->db->order_by('spe_id','ASC'); 
		$query = $this->db->get('tblspecializations');
		return $query->result_array();
	}
	 
 
   public function getAll()
	{
		$this->db->order_by('spe_id','ASC');
		$query = $this->db->get('tblspecializations'); 	 	
		return $query->result_array(); 
		 
	}
 
 	public function addSpecialization($arrSpecialization)
	{
		$this->db->insert('tblspecializations', $arrSpecialization);
		return $this->db->insert_id();	
		 
	}
	
	public function editSpecialization($specializationId,$arrSpecialization)
	{
		$this->db->where('spe_id', $specializationId);  
		$this->db->update('tblspecializations', $arrSpecialization);
		return $this->db->affected_rows(); 	
	}
	
	public function deleteSpecialization($specializationId)
	{
		$this->db->where('spe_id', $specializationId);
		$this->db->delete('tblspecializations');
		return $this->db->affected_rows(); 	
	}
 	
 	public function getSpedesc($speid)
	{
		$this->db->select('spe_desc');
		$this->db->from('tblspecializations');
		$this->db->where('spe_id !=', $speid);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidSpe()
	{ 
		$this->db->select("spe_desc as valspe");
		$this->db->from('tblspecializations');
		$query = $this->db->get();
		return $query->result_array(); 
	}
  	
}