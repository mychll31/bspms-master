<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Universities_model extends CI_Model { 
 
	public function __construct()
	{
           $this->load->database();  
 	}
	
	public function getAll()
	{
		$this->db->order_by('uni_name','ASC'); 
		$query = $this->db->get('tbluniversities');
		return $query->result_array(); 
	}

	public function getUniversity($uniid)
	{
		$this->db->where("uni_id", $uniid); 
		$query = $this->db->get('tbluniversities');
		return $query->result_array();
	}

	public function addUniversity($arrUniveristy)
	{
		$this->db->insert('tbluniversities', $arrUniveristy);
		return $this->db->insert_id();	
		 
	}

	public function editUniversity($uniid,$arrUniveristy)
	{
		$this->db->where('uni_id', $uniid);  
		$this->db->update('tbluniversities', $arrUniveristy);
		return $this->db->affected_rows(); 	
	}

	public function deleteUniversity($uniid)
	{
		$this->db->where('uni_id', $uniid); 
		$this->db->delete('tbluniversities');
		return $this->db->affected_rows(); 	
	}

	public function getValidUnis()
	{ 
		$this->db->select("concat(uni_abbrv, '-', uni_name) as valunis");
		$this->db->from('tbluniversities');
		$query = $this->db->get();
		return $query->result_array(); 
	}

}