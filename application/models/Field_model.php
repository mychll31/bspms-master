<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Field_model extends CI_Model {
 
	public function __construct()
	{
           $this->load->database();  
	}
	
	public function getField($fieldId)
	{

		$this->db->where("fld_id", $fieldId);  
		$this->db->order_by('fld_desc','ASC'); 
		$query = $this->db->get('tblfields');
		return $query->result_array();
	}
	 
  
   public function getAll()
	{
		$this->db->order_by('fld_desc','ASC');
		$query = $this->db->get('tblfields'); 	 	
		return $query->result_array();
		
	}
 
 	public function addField($arrField)
	{
		$this->db->insert('tblfields', $arrField);
		return $this->db->insert_id();	
		 
	}
	
	public function editField($fieldId,$arrField)
	{
		$this->db->where('fld_id', $fieldId);  
		$this->db->update('tblfields', $arrField);
		return $this->db->affected_rows(); 	
	}
	 
	public function deleteField($fieldId)
	{
		$this->db->where('fld_id', $fieldId);
		$this->db->delete('tblfields');
		return $this->db->affected_rows(); 	
	}
 
	public function getFieldNumber($fieldId) 
	{
		$this->db->select('fld_desc');
		$this->db->from('tblfields');
		$this->db->where('fld_id !=', $fieldId);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getValidField()
	{ 
		$this->db->select("concat(fld_code, '-', fld_desc) as valfield");
		$this->db->from('tblfields');
		$query = $this->db->get();
		return $query->result_array(); 
	}

	
}