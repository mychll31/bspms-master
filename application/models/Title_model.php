<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Title_model extends CI_Model {
 
	public function __construct() 
	{
           $this->load->database();  
	}
	
  
   public function getAll() 
	{
		$this->db->order_by('tit_desc','ASC');
		$query = $this->db->get('tbltitles'); 	 	
		return $query->result_array();
		
	}
 
	public function getTitle($titleId)
	{

		$this->db->where("tit_id", $titleId);  
		$this->db->order_by('tit_desc','ASC'); 
		$query = $this->db->get('tbltitles');
		return $query->result_array();
	}

 	public function addTitle($arrTitle) 
	{
		$this->db->insert('tbltitles', $arrTitle);
		return $this->db->insert_id();	
		 
	}
	
	public function editTitle($titleId,$arrTitle)
	{
		$this->db->where('tit_id', $titleId);
		$this->db->update('tbltitles', $arrTitle);
		return $this->db->affected_rows(); 	
	}
	 
	public function deleteTitle($titleId)
	{
		$this->db->where('tit_id', $titleId);
		$this->db->delete('tbltitles');
		return $this->db->affected_rows(); 	
	}
	
	public function getTitleCode($titleId)
	{
		$this->db->select('tit_desc');
		$this->db->from('tbltitles'); 
		$this->db->where('tit_id !=', $titleId);
		$query = $this->db->get();
		return $query->result_array(); 
	}

	public function getValidTitle()
	{ 
		$this->db->select("concat(tit_abbreviation, '-', tit_desc) as valtitle");
		$this->db->from('tbltitles');
		$query = $this->db->get();
		return $query->result_array(); 
	}

 
  	
}