<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Scientists_model extends CI_Model {
	public function __construct()
	{
           $this->load->database();
	}
	
	
///////------ GET DATA ----.////////////////////////-	
	public function getScientist($id)
	{
		$this->db->where('sci_id' , $id);
		$query = $this->db->get('tblscientist');
		return $query->result_array();
	}

	public function getSciTba($id)
	{
		$this->db->select('*');
		$this->db->from('tbltba');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tbltba.tba_host_institution', 'left');
		$this->db->where('tba_sci_id' , $id);
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function getScientistEmails()
	{
		$this->db->select('sci_email');
		$this->db->from('tblscientist');
		$this->db->where('sci_email !=', '');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function countSciEduc($sciID)
	{
		$this->db->select('count(*) as educall');
		$this->db->from('tblscieducations');
		$this->db->where('educ_sci_id', $sciID);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function countSciEmp($sciID)
	{
		$this->db->select('count(*) as empall');
		$this->db->from('tblsciemployment');
		$this->db->where('emp_sci_id', $sciID);
		$query = $this->db->get();
		return $query->result_array();
	}

	public function countSciAwards($sciID)
	{
		$this->db->select('count(*) as awards');
		$this->db->from('tblsciservice');
		$this->db->where(array('srv_sci_id' => $sciID, 'srv_isdeleted' => 0));
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getScientists()
	{
		$this->db->select('*');
		$this->db->from('tblscientist');
		$this->db->join('tblprofessions', 'tblprofessions.prof_id = tblscientist.sci_profession_id', 'left');
		$this->db->join('tbltitles', 'tbltitles.tit_id = tblscientist.sci_title', 'left');
		$this->db->join('tblusers', 'tblusers.usr_user_id = tblscientist.created_by', 'left');
		$this->db->order_by('sci_last_name, sci_first_name');
		$this->db->where('sci_isdeleted=0');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function getbspSci($year, $awardtype)
	{
		$this->db->select('*');
		$this->db->from('tblscientist');
		$this->db->join('tblprofessions', 'tblprofessions.prof_id = tblscientist.sci_profession_id', 'left');
		$this->db->join('tblsciservice', 'tblsciservice.srv_sci_id = tblscientist.sci_id', 'left');
		$this->db->join('tbltitles', 'tbltitles.tit_id = tblscientist.sci_title', 'left');
		$this->db->order_by('sci_last_name, sci_first_name');
		$this->db->where('sci_isdeleted=0');
		$this->db->where('srv_typeofaward', $awardtype);
		$this->db->where("if (srv_isyearonly=1, srv_approval_yr, '')  = ".$year."
			or (if(srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) >= '".$year."-01-01' and if(srv_sched_approvaldate = '0000-00-00', srv_approval_date, srv_sched_approvaldate) <= '".$year."-12-31')");
		$query = $this->db->group_by('tblscientist.sci_id');
		$query = $this->db->order_by('sci_last_name, sci_first_name');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	public function getScientistEducation($id,$educationId=0) // $id for all education of scientist. if education ID is present, used for editing education details.
	{
		if($educationId==0):
			$sql = "SELECT tblscieducations.*, tbluniversities.uni_name, tbleducationallevels.elev_desc, tblglobalregions.glo_country, tblcourses.cou_desc FROM tblscieducations 
					LEFT JOIN tbleducationallevels ON tbleducationallevels.elev_id = tblscieducations.educ_level_id
					LEFT JOIN tbluniversities ON tbluniversities.uni_id = tblscieducations.educ_school
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id = tblscieducations.educ_countryorigin_id
					LEFT JOIN tblcourses ON tblcourses.cou_id = tblscieducations.educ_course_id
					WHERE tblscieducations.educ_sci_id=$id
					ORDER BY tblscieducations.educ_year_graduated DESC";
		else:
			$sql = "SELECT tblscieducations.*, tbluniversities.uni_name, tbleducationallevels.elev_desc, tblglobalregions.glo_country, tblcourses.cou_desc FROM tblscieducations 
					LEFT JOIN tbleducationallevels ON tbleducationallevels.elev_id = tblscieducations.educ_level_id
					LEFT JOIN tbluniversities ON tbluniversities.uni_id = tblscieducations.educ_school
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id = tblscieducations.educ_countryorigin_id
					LEFT JOIN tblcourses ON tblcourses.cou_id = tblscieducations.educ_course_id
					WHERE tblscieducations.educ_id=$educationId
					ORDER BY tblscieducations.educ_year_graduated DESC";
		endif;
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getScientistEmployment($id,$employmentId=0) // $id for all employment of scientist. if employment ID is present, used for editing employment details.)
	{
		if($employmentId==0):
			$sql = "SELECT tblsciemployment.*, tblglobalregions.glo_country FROM tblsciemployment 
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id = tblsciemployment.emp_countryorigin_id
					WHERE tblsciemployment.emp_sci_id=$id
					ORDER BY tblsciemployment.emp_dateto DESC";
		else: 
			$sql = "SELECT tblsciemployment.*, tblglobalregions.glo_country FROM tblsciemployment 
					LEFT JOIN tblglobalregions ON tblglobalregions.glo_id = tblsciemployment.emp_countryorigin_id
					WHERE tblsciemployment.emp_id=$employmentId
					ORDER BY tblsciemployment.emp_dateto DESC";
		endif;	
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function getAreaExpertise()
	{
		$query = $this->db->get('tblexpertise');
		return $query->result_array();
	}
	
	public function getSpecializations()
	{
		$query = $this->db->get('tblspecializations');
		return $query->result_array();
	}
	
	public function getProfessions()
	{
		$query = $this->db->get('tblprofessions');
		return $query->result_array();
	}
	
	public function getEducationLevels()
	{
		$this->db->order_by('elev_desc','ASC');
		$query = $this->db->get('tbleducationallevels');
		return $query->result_array();
	}
	
	public function getCountry()
	{
		$this->db->order_by('glo_country','ASC'); 
		$query = $this->db->get('tblglobalregions');
		return $query->result_array();
	}
	
	public function getCourse()
	{
		$query = $this->db->get('tblcourses');
		return $query->result_array();
	}
	
	public function getScientistServiceAward($sci_id)
	{
		$this->db->select('*,(select min(con_date_from) from tblsrvcontractdates where con_srv_id=srv_id) as "contract_fromDate", (select max(con_date_to) from tblsrvcontractdates where con_srv_id=srv_id) as "contract_fromTo"');
		$this->db->join('tblglobalregions', 'tblglobalregions.glo_id = tblsciservice.srv_glo_id', 'left');
		$this->db->join('tblcouncils', 'tblcouncils.cil_id = tblsciservice.srv_cil_id', 'left');
		$this->db->join('tblinstitutions', 'tblinstitutions.ins_id = tblsciservice.srv_ins_id', 'left');
		$this->db->order_by("srv_dateCreated desc");
		$query = $this->db->get_where('tblsciservice', array('srv_sci_id'=>$sci_id, 'srv_isdeleted' => 0));
		return $query->result_array();
	}

	
///////////---------------------ADD DATA---------------------------//////	
	
	
	public function addScientist($arrScientist)
	{
		$this->db->insert('tblscientist', $arrScientist); 	
		return $this->db->insert_id();
	}

	public function addScientistTba($arrSciTba)
	{
		$this->db->insert('tbltba', $arrSciTba); 	
		return $this->db->insert_id();
	}
	
	public function addScientistEducation($arrScientistEducation)
	{
		$this->db->insert('tblscieducations', $arrScientistEducation); 	
		return $this->db->insert_id();
	}
	
	public function addScientistEmployment($arrScientistEmployment)
	{
		$this->db->insert('tblsciemployment', $arrScientistEmployment); 	
		return $this->db->insert_id();
	}
	
	
//------------------------------------EDIT DATA-----------------------------------------///

	public function editProfile($arrProfileData,$scientistId)
	{
		$this->db->where('sci_id', $scientistId);
		$this->db->update('tblscientist', $arrProfileData);
		return $this->db->affected_rows(); 
		
	}

	public function editScientistTba($arrSciTba,$tbaid)
	{
		$this->db->where('tba_id', $tbaid);
		$this->db->update('tbltba', $arrSciTba);
		return $this->db->affected_rows(); 
		
	}	
	
	public function editEducation($arrEducationData,$educId)
	{
		$this->db->where('educ_id', $educId);
		$this->db->update('tblscieducations', $arrEducationData);
		return $this->db->affected_rows(); 
		
	}	
	
	public function editEmployment($arrEmploymentData,$empId)
	{
		$this->db->where('emp_id', $empId);
		$this->db->update('tblsciemployment', $arrEmploymentData);
		
		return $this->db->affected_rows(); 
		
	}	
	
	
//-------------------------DELETE DATA------------------------------------------------------///


	// public function deleteScientist($scientistId)
	// {
	// 	$this->db->where('sci_id', $scientistId);
	// 	$this->db->delete('tblscientist');
		
	// 	$this->db->where('educ_sci_id', $scientistId);
	// 	$this->db->delete('tblscieducations');
		
	// 	$this->db->where('emp_sci_id', $scientistId);
	// 	$this->db->delete('tblsciemployment');
		
	// 	return $this->db->affected_rows(); 	
	// }
	public function deleteScientist($scientistId)
	{
		$data = array(
               'sci_isdeleted' => 1,
        );
		$this->db->where('sci_id', $scientistId);
		$this->db->update('tblscientist', $data);
		
		return $this->db->affected_rows(); 
	}
	
	
	public function deleteScientistEducation($scientistEducationId)
	{
		$this->db->where('educ_id', $scientistEducationId);
		$this->db->delete('tblscieducations');
		return $this->db->affected_rows(); 	
	}

	public function deleteScientistTba($scitbaid)
	{
		$this->db->where('tba_id', $scitbaid);
		$this->db->delete('tbltba');
		return $this->db->affected_rows(); 	
	}
	
	public function deleteScientistEmployment($scientistEducationId)
	{
		$this->db->where('emp_id', $scientistEducationId);
		$this->db->delete('tblsciemployment');
		return $this->db->affected_rows(); 	
	}

}
