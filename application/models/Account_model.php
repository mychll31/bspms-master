<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model {
	public function __construct()
	{
           $this->load->database();
	}
	
	public function getAccount($strUserId)
	{
		$this->db->where('usr_user_id', $strUserId);
		$query = $this->db->get('tblusers');
		return $query->result_array();
	}
	
	public function updateAccount($arrAccountData,$id)
	{
		
		$this->db->where('usr_user_id', $id);
		return $this->db->update('tblusers', $arrAccountData); 
		
			
	}
}