  $(document).ready(function() {
    $("#file-upload").change(function(e) {
          var input = document.getElementById("file-upload");
          var ul = document.getElementById("fileList");
          while (ul.hasChildNodes()) {
              ul.removeChild(ul.firstChild);
            }
          for (var i = 0; i < input.files.length; i++) {
              var li = document.createElement("li");
              li.innerHTML = input.files[i].name;
              ul.appendChild(li);
          }
          if(!ul.hasChildNodes()) {
              var li = document.createElement("li");
              li.innerHTML = 'No Files Selected';
              ul.appendChild(li);
          }
    });

    
    $('.col-sm-3 #txtYearGraduated').mask("9999",{placeholder:"YYYY"});
    
    $('.col-sm-3 #txtDateFrom, .col-sm-3 #txtDateTo').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});

    $('.col-sm-6 #txterpdate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('.col-sm-3 #txtyrcompleted').mask("9999",{placeholder:"YYYY"});
    $('.col-sm-3 #txtyrrepatriated').mask("9999",{placeholder:"YYYY"});
    $('.col-sm-3 #txtprocessdate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});

    $('#txtBirthDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    
    $('#txtdfrom').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('#txtdto').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});

    $('#txtsemDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('#txtpubdate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('#txtsubdate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('.col-md-4 #progreportSubDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('.col-md-4 #termreportSubDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('.col-md-4 #bspreportSubDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('.col-md-4 #bspInsreportSubDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('.col-md-4 #evalFormSubDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('.col-md-4 #impevalSubDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('input#txtApprovalDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    $('input#txtapprovalyear').mask("9999",{placeholder:"YYYY"});
    $('.col-sm-3 #txtChangeSchedApprovalDate').mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    for (var i = 1; i <= 10; i++) {
      $('#txtContDurFrom'+i).mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
      $('#txtContDurTo'+i).mask("9999-99-99",{placeholder:"YYYY-MM-DD"});
    }
    
    // begin Phase
    for (var i = 2; i <= 10; i++) {
      $('#tblphase'+i).hide();
    }

    var totalTextbox = 1;

    $('#btnaddphase').click(function() {
        totalTextbox += 1;
        for (var i = 2; i <= 10; i++) {
          if($('#tblphase'+i).is(":visible") == false){
              $('#tblphase'+i).show();
              break;
          }
        }
        if(totalTextbox == 10){
            $('#btnaddphase').addClass('disabled');
        }
    });

    $('#btnremove2').click(function() {
        $('#tblphase2').hide();
        $('#txtContDurFrom2').val('');
        $('#txtContDurTo2').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove3').click(function() {
        $('#tblphase3').hide();
        $('#txtContDurFrom3').val('');
        $('#txtContDurTo3').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove4').click(function() {
        $('#tblphase4').hide();
        $('#txtContDurFrom4').val('');
        $('#txtContDurTo4').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove5').click(function() {
        $('#tblphase5').hide();
        $('#txtContDurFrom5').val('');
        $('#txtContDurTo5').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove6').click(function() {
        $('#tblphase6').hide();
        $('#txtContDurFrom6').val('');
        $('#txtContDurTo6').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove7').click(function() {
        $('#tblphase7').hide();
        $('#txtContDurFrom7').val('');
        $('#txtContDurTo7').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove8').click(function() {
        $('#tblphase8').hide();
        $('#txtContDurFrom8').val('');
        $('#txtContDurTo8').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove9').click(function() {
        $('#tblphase9').hide();
        $('#txtContDurFrom9').val('');
        $('#txtContDurTo9').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    $('#btnremove10').click(function() {
        $('#tblphase10').hide();
        $('#txtContDurFrom10').val('');
        $('#txtContDurTo10').val('');
        totalTextbox -= 1;
        $('#btnaddphase').removeClass('disabled');
    });
    // end Phase
});