var AppCalendar = function() {

    return {
        //main function to initiate the module
        init: function() {
            this.initCalendar();
        },

        initCalendar: function() {

            if (!jQuery().fullCalendar) {
                return;
            }

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var h = {};

            var evid = 0;

            if (App.isRTL()) {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        right: 'title, prev, next',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        right: 'title',
                        center: '',
                        left: 'agendaDay, agendaWeek, month, today, prev,next'
                    };
                }
            } else {
                if ($('#calendar').parents(".portlet").width() <= 720) {
                    $('#calendar').addClass("mobile");
                    h = {
                        left: 'title, prev, next',
                        center: '',
                        right: 'today,month,agendaWeek,agendaDay'
                    };
                } else {
                    $('#calendar').removeClass("mobile");
                    h = {
                        left: 'title',
                        center: '',
                        right: 'prev,next,today,month,agendaWeek,agendaDay'
                    };
                }
            }

            var initDrag = function(el) {
                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim(el.text()), // use the element's text as the event title
                };
                // store the Event Object in the DOM element so we can get to it later
                el.data('eventObject', eventObject);
                // make the event draggable using jQuery UI
                el.draggable({
                    zIndex: 999,
                    revert: true, // will cause the event to go back to its
                    revertDuration: 0 //  original position after the drag
                });
            };

            var addEvent = function(title) {
                title = title.length === 0 ? "Untitled Event" : title;
                var html = $('<div class="external-event label label-default" style="text-transform: capitalize;">' + title + '</div>');
                jQuery('#event_box').append(html);
                initDrag(html);
            };

            $('#external-events div.external-event').each(function() {
                initDrag($(this), 0);
            });

            $('#event_add').unbind('click').click(function() {
                title = $('#event_title').val();
                event_url = $('#event_url').val();
                sdatetime = $("#eventstart").find("input").val();
                edatetime = $("#eventend").find("input").val();
                allday = $("#chkallday").prop("checked") ? 1 : 0;
                loc = $('#event_loc').val();
                remarks = $('#event_remarks').val();
                saveUrl = 'title='+title+'&sdatetime='+sdatetime+'&edatetime='+edatetime+'&allday='+allday+'&bgcolor=&url='+event_url+'&loc='+loc+'&remarks='+remarks;
                console.log(saveUrl);

                if(title.length===0){
                    $('#erreventtitle').html('Event title is required');
                }else{
                    $('#erreventtitle').html('');
                    $.ajax ({
                        type: "GET",
                        url: "calendar/save?",
                        data: saveUrl,
                        cache: false,
                        success: function(data){
                            evid = data;
                            console.log(evid);
                            $('#calendar').fullCalendar('addEventSource', 'calendar/getLastInsertedEvent/'+evid);
                        }
                    });
                    // $('#calendar').fullCalendar('refetchEvents');
                    // $('#calendar').fullCalendar('removeEvents');

                    // addEvent(title);
                    // $('#event_add').addClass("disabled");
                    //clear form
                    $('#event_title').val('');
                    $('#event_url').val('');
                    $("#eventstart").find("input").val('');
                    $("#eventend").find("input").val('');
                    $('#chkallday').prop('checked', false);
                }
                

            });

            //predefined events
            $('#event_box').html("");

            $('#calendar').fullCalendar('destroy'); // destroy the calendar
            $('#calendar').fullCalendar({ //re-initialize the calendar
                header: h,
                defaultView: 'month', // change default view with available options from http://arshaw.com/fullcalendar/docs/views/Available_Views/ 
                slotMinutes: 15,
                editable: false, // true if allowed to drag to edit
                droppable: false, // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay, ui, resourceId, event) { // this function is called when something is dropped

                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $(this).data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);
                    
                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $(this).attr("data-class");
                    copiedEventObject.id = evid;

                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
                    

                    // is the "remove after drop" checkbox checked?
                    /*
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }*/
                    // update date!
                    // $.ajax ({
                    //     type: "GET",
                    //     url: "calendar/update_calendarDate?",
                    //     data: 'evid='+evid+'&start='+copiedEventObject.start._d+'&end='+copiedEventObject.start._d+'&format=1',
                    //     cache: false,
                    //     success: function(data){
                    //         console.log(data);
                    //     }
                    // });

                    $(this).remove(); //AutoRemove 
                    $('#event_add').removeClass("disabled");
                },

                eventRender: function(event, element) {
                    element.bind('dblclick', function() {
                        event.id = event.id;
                        if(event.id!=null) {
                            console.log(event);
                            $('#edit_event_title').val(event.title);
                            $('#edit_event_url').val(event.url);
                            $('#edit_eventstart').val(event.startdate + ' ' + event.starttime);
                            $('#edit_eventend').val(event.enddate + ' ' + event.endtime);
                            $('#divEvent').modal('show');
                        }
                        $('#btnEditEvent').click(function() {
                            allday = $("#edit_chkallday").prop("checked") ? 1 : 0;
                            updateUrl = 'evid='+event.id+'&title='+$('#edit_event_title').val()+'&start='+$("#div_edit_eventstart").find("input").val()+'&end='+$("#div_edit_eventend").find("input").val()+'&allday='+allday+'&bgcolor=&url='+$('#edit_event_url').val()+'&format=0';
                            // update date!
                            console.log(updateUrl);
                            $.ajax ({
                                type: "GET",
                                url: "calendar/update_calendarDate?",
                                data: updateUrl,
                                cache: false,
                                success: function(data){
                                    $('#calendar').fullCalendar('refetchEvents');
                                }
                            });
                        });

                        // Delete an event
                        $('#btnRemoveEvent').click(function() {
                            var eventid = event.id;
                            $('#eventid').val(event.id);
                            $('#btnsubmitdelete').click(function() {
                                $.ajax ({
                                    type: "GET",
                                    url: "calendar/deleteEvent?",
                                    data: 'evid='+$('#eventid').val(),
                                    cache: false,
                                    success: function(data){
                                        $('#divEvent').modal('hide');
                                        $('#calendar').fullCalendar('removeEvents', $('#eventid').val());
                                        $('#calendar').fullCalendar('refetchEvents');
                                    }
                                });                                
                            });
                        });
                    });
                    
                },

            //get events
            events: {
                url: 'calendar/getAllEvents',
                error: function() {
                    console.log('there was an error');
                }
            },

            });
        }

    };

}();

jQuery(document).ready(function() {    
   AppCalendar.init(); 
});