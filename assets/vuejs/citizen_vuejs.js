new Vue({
    el: '#citizenship',

    data: {
        newCitizen: { cit_type: '', cit_particulars: '' },
        subisExist: false,
        haserror: ''
    },

    ready: function() {
        // this.fetchCitizen();
    },

    methods: {

        fetchCitizen: function() {
            this.$http.get('../citizenship/fetchCitizen', function(citizenship) {
                this.$set('citizenship', citizenship);
            });
        },

        onSubmitForm: function(e) {
            e.preventDefault();
            var cit_particulars = this.newCitizen.cit_particulars;
            var isExist = false;
            this.citizenship.forEach(function(part) {
                if(part.cit_particulars == cit_particulars){
                    isExist = true;
                }
            });

            // begin isExist
            if (isExist == true){
                this.haserror = 'has-error';
                this.subisExist = true;
            }else{
                Vue.http.options.emulateJSON = true;
                this.$http.post('../citizenship/insertCitizen', this.newCitizen, function () {});
                this.citizenship.push(this.newCitizen);
                this.newCitizen = { cit_type: '', cit_particulars: '' };
                this.haserror = '';
                this.subisExist = false;
                $('#modalCitizenship').modal('hide');
            }
            // end isExist

        }
    }
});
