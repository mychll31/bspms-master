new Vue({
    el: '#expertise',

    data: {
       newExpertise: {
            code: '', desc: '',
        },
		
        allerrors: false,		
        Error: {
            code: '', desc: '',
        },
        ErrorValidation: {
            valid_code: false, valid_desc: false,
        },
        haserror: {
            code: '', desc: '',
        },
        baseUrl: '',
        expertise:  [], 
        expertiseid: '',
		submmited: false, 
    },

    ready: function(){
        this.haserror.code = '';
        this.Error.code = '';

        this.haserror.desc = '';
        this.Error.desc = '';

		// fetch 
        this.fetchAll();
    },
	
    computed: {
        //Code
        validCode: function() {
            if(this.newExpertise.code == ""){
                if(this.submmited){//				
                this.haserror.code = 'has-error';
                this.ErrorValidation.valid_code = true;
                this.Error.code = 'This field is required.';
                return true;
				}
            }else if(this.newExpertise.code == " " || (!this.newExpertise.code.replace(/\s/g, '').length)){
                this.haserror.code = 'has-error';
                this.ErrorValidation.valid_code = true;
                this.Error.code = 'Invalid input.';
                return true;
            }else{
                var matches = this.newExpertise.desc.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
                if(matches != null){
                    this.haserror.desc = 'has-error';
                    this.ErrorValidation.valid_desc = true;
                    this.Error.desc = 'Please use only alphabet characters and spaces for Expertise Description.';
                    return true;
                }
            }
            this.ErrorValidation.valid_desc = false; this.Error.desc=''; this.haserror.desc = ''; return false;
        },
 
        //desc name
        validDesc: function() {
			
            if(this.newExpertise.desc == ""){
                this.haserror.desc = 'has-error'; this.ErrorValidation.valid_desc = true;
                this.Error.desc = 'This field is required.';
                return true;
            }else{
                if(this.newExpertise.desc == " " || (!this.newExpertise.desc.replace(/\s/g, '').length)){
                    this.haserror.desc = 'has-error'; this.ErrorValidation.valid_desc = true;
                    this.Error.desc = 'Invalid input.';
                    return true;  
                }else if(this.expertise.indexOf(this.newExpertise.desc) >= 0){
                    this.haserror.desc = 'has-error'; this.ErrorValidation.valid_desc = true;
                    this.Error.desc = 'Description already exists.';
                    return true;  
                }
            }
            this.ErrorValidation.valid_desc = false; this.Error.desc=''; this.haserror.desc = ''; return false;
        },


    }, 

    methods: {
        //fetchAll
        fetchAll: function() {
            this.$http.get(this.baseUrl + 'expertise/fetchdesc/' + this.expertiseid, function(expertise) {
                this.$set('expertise', expertise);
            });
        },

        getValidate: function(e) {

			this.submmited = true; // 
            if(this.newExpertise.code == ""){
                this.haserror.code = 'has-error';
                this.Error.code    = 'This field is required.';
                this.ErrorValidation.valid_name = true;
            }

            // check desc
            if(this.newExpertise.desc == ""){
                this.haserror.desc = 'has-error';
                this.Error.desc    = 'This field is required.';
            } 
            
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
                console.log('dont continue');
            }
        }
    }
});