new Vue({
    el: '#srvseminars',

    data: {
        seminars:  [],
        participantstype: [],
        fields: [],
        sid: '',
        field: '',
        title: '',
        part: '',
        target: '',
        output: '',
        haserror: {
            field: '',
            title: '',
            part: '',
            target: '',
            output: ''
        },
        Error: {
            field: '',
            title: '',
            part: '',
            target: '',
            output: ''
        },
        ErrorValidation: {
            field:  false,
            title:  false,
            part:   false,
            target: false,
            output: false
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchSeminar();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'part': function(val) {
            if(val != ''){
                this.haserror.part         = '';
                this.Error.part            = '';
                this.ErrorValidation.part  = false;
            }else{
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
        },
        'target': function(val) {
            if(val != ''){
                var matches = val.match(/^\d+$/g);
                if(matches == null){
                    this.haserror.target         = 'has-error';
                    this.Error.target            = 'Invalid input.';
                    this.ErrorValidation.target  = true;
                }else{
                    this.haserror.target         = '';
                    this.Error.target            = '';
                    this.ErrorValidation.target  = false;
                }
            }else{
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },
    methods: {
        fetchSeminar: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchSeminar/'+$('#sciid').val()+'/0', function(seminars) {
                this.$set('seminars', seminars);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchparticipantstype', function(participantstype) {
                this.$set('participantstype', participantstype);
            });
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (sem) {
            if(sem != undefined){
                this.sid    = sem.sem_id;
                this.field  = sem.sem_field;
                this.title  = sem.sem_title;
                this.part   = sem.sem_participantstype;
                this.target = sem.sem_targetparticipants;
                this.output = sem.sem_output;
                $('#semid_edit').val(sem.sem_id);
                $('#targetPane').val('target');
                $('#target_id').val(sem.sem_id);
                $('#target_name').val('sem');
                this.action = 'edit';
                this.actionName = 'serviceawards/editSeminars/';
            }    
        },

        addSeminar: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.sid = '';
            this.field = '';
            this.title = '';
            this.part = '';
            this.target = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addSeminars/';
        },
        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.part == ''){
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
            if(this.target == ''){
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Training

new Vue({
    el: '#srvTrainings',

    data: {
        trainings:  [],
        participantstype: [],
        fields: [],
        sid: '',
        field: '',
        title: '',
        part: '',
        target: '',
        output: '',
        haserror: {
            field: '',
            title: '',
            part: '',
            target: '',
            output: ''
        },
        Error: {
            field: '',
            title: '',
            part: '',
            target: '',
            output: ''
        },
        ErrorValidation: {
            field:  false,
            title:  false,
            part:   false,
            target: false,
            output: false
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchTraining();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'part': function(val) {
            if(val != ''){
                this.haserror.part         = '';
                this.Error.part            = '';
                this.ErrorValidation.part  = false;
            }else{
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
        },
        'target': function(val) {
            if(val != ''){
                var matches = val.match(/^\d+$/g);
                if(matches == null){
                    this.haserror.target         = 'has-error';
                    this.Error.target            = 'Invalid input.';
                    this.ErrorValidation.target  = true;
                }else{
                    this.haserror.target         = '';
                    this.Error.target            = '';
                    this.ErrorValidation.target  = false;
                }
            }else{
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },
    methods: {
        fetchTraining: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchTrainings/'+$('#sciid').val()+'/0', function(trainings) {
                this.$set('trainings', trainings);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchparticipantstype', function(participantstype) {
                this.$set('participantstype', participantstype);
            });
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (tra) {
            if(tra != undefined){
                this.sid    = tra.tra_id;
                this.field  = tra.tra_field;
                this.title  = tra.tra_title;
                this.part   = tra.tra_participantstype;
                this.target = tra.tra_targetparticipants;
                this.output = tra.tra_output;
                $('#semid_edit').val(tra.tra_id);
                $('#targetPane').val('target');
                $('#target_id').val(tra.tra_id);
                $('#target_name').val('tra');
                this.action = 'edit';
                this.actionName = 'serviceawards/editTraining/';
            }    
        },

        addTraining: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.sid = '';
            this.field = '';
            this.title = '';
            this.part = '';
            this.target = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addTraining/';
        },
        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.part == ''){
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
            if(this.target == ''){
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Projects

new Vue({
    el: '#srvProjects',

    data: {
        projects:  [],
        fields: [],
        pij: '',
        field: '',
        title: '',
        obj: '',
        agency: '',
        stat: '',
        contri: '',
        haserror: {
            field: '',
            title: '',
            obj: '',
            agency: '',
            stat: '',
            contri: ''
        },
        Error: {
            field: '',
            title: '',
            obj: '',
            agency: '',
            stat: '',
            contri: ''
        },
        ErrorValidation: {
            field:  false,
            title:  false,
            obj:    false,
            agency: false,
            stat:   false,
            contri: false
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchProject();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'obj': function(val) {
            if(val == ''){
                this.haserror.obj         = 'has-error';
                this.Error.obj            = 'This field is required.';
                this.ErrorValidation.obj  = true;
            }else{
                if(!this.obj.replace(/\s/g, '').length){
                    this.haserror.obj         = 'has-error';
                    this.Error.obj            = 'Invalid input.';
                    this.ErrorValidation.obj  = true;
                }else{
                    this.haserror.obj         = '';
                    this.Error.obj            = '';
                    this.ErrorValidation.obj  = false;
                }
            }
        },
        'agency': function(val) {
            if(val == ''){
                this.haserror.agency         = 'has-error';
                this.Error.agency            = 'This field is required.';
                this.ErrorValidation.agency  = true;
            }else{
                if(!this.agency.replace(/\s/g, '').length){
                    this.haserror.agency         = 'has-error';
                    this.Error.agency            = 'Invalid input.';
                    this.ErrorValidation.agency  = true;
                }else{
                    this.haserror.agency         = '';
                    this.Error.agency            = '';
                    this.ErrorValidation.agency  = false;
                }
            }
        },
        'stat': function(val) {
            if(val == ''){
                this.haserror.stat         = 'has-error';
                this.Error.stat            = 'This field is required.';
                this.ErrorValidation.stat  = true;
            }else{
                if(!this.stat.replace(/\s/g, '').length){
                    this.haserror.stat         = 'has-error';
                    this.Error.stat            = 'Invalid input.';
                    this.ErrorValidation.stat  = true;
                }else{
                    this.haserror.stat         = '';
                    this.Error.stat            = '';
                    this.ErrorValidation.stat  = false;
                }
            }
        },
        'contri': function(val) {
            if(val == ''){
                this.haserror.contri         = 'has-error';
                this.Error.contri            = 'This field is required.';
                this.ErrorValidation.contri  = true;
            }else{
                if(!this.contri.replace(/\s/g, '').length){
                    this.haserror.contri         = 'has-error';
                    this.Error.contri            = 'Invalid input.';
                    this.ErrorValidation.contri  = true;
                }else{
                    this.haserror.contri         = '';
                    this.Error.contri            = '';
                    this.ErrorValidation.contri  = false;
                }
            }
        },
    },
    methods: {
        fetchProject: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchProjects/'+$('#sciid').val()+'/0', function(projects) {
                this.$set('projects', projects);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (proj) {
            if(proj != undefined){
                this.pij     = proj.prj_id;
                this.field   = proj.prj_field;
                this.title   = proj.prj_title;
                this.obj     = proj.prj_objective;
                this.agency  = proj.prj_agency;
                this.stat    = proj.prj_status;
                this.contri  = proj.prj_contributions;
                $('#semid_edit').val(proj.prj_id);
                $('#targetPane').val('target');
                $('#target_id').val(proj.prj_id);
                $('#target_name').val('proj');
                this.action = 'edit';
                this.actionName = 'serviceawards/editProject/';
            }    
        },

        addProject: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.pij    = '';
            this.field  = '';
            this.title  = '';
            this.obj    = '';
            this.agency = '';
            this.stat   = '';
            this.contri = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addProject/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.obj == ''){
                this.haserror.obj         = 'has-error';
                this.Error.obj            = 'This field is required.';
                this.ErrorValidation.obj  = true;
            }
            if(this.agency == ''){
                this.haserror.agency         = 'has-error';
                this.Error.agency            = 'This field is required.';
                this.ErrorValidation.agency  = true;
            }
            if(this.stat == ''){
                this.haserror.stat         = 'has-error';
                this.Error.stat            = 'This field is required.';
                this.ErrorValidation.stat  = true;
            }
            if(this.contri == ''){
                this.haserror.contri         = 'has-error';
                this.Error.contri            = 'This field is required.';
                this.ErrorValidation.contri  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Papers

new Vue({
    el: '#srvPapers',

    data: {
        papers:  [],
        fields: [],
        pij: '',
        title: '',
        name: '',
        haserror: {
            title: '',
            name: '',
        },
        Error: {
            title: '',
            name: '',
        },
        ErrorValidation: {
            title: false,
            name:  false,
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchPapers();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'name': function(val) {
            if(val == ''){
                this.haserror.name         = 'has-error';
                this.Error.name            = 'This field is required.';
                this.ErrorValidation.name  = true;
            }else{
                if(!this.name.replace(/\s/g, '').length){
                    this.haserror.name         = 'has-error';
                    this.Error.name            = 'Invalid input.';
                    this.ErrorValidation.name  = true;
                }else{
                    this.haserror.name         = '';
                    this.Error.name            = '';
                    this.ErrorValidation.name  = false;
                }
            }
        },
    },

    methods: {
        fetchPapers: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchPaper/'+$('#sciid').val()+'/0', function(papers) {
                this.$set('papers', papers);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (proj) {
            if(proj != undefined){
                this.pij    = proj.pap_id;
                this.title  = proj.pap_title;
                this.name   = proj.pap_name;
                $('#semid_edit').val(proj.pap_id);
                $('#targetPane').val('target');
                $('#target_id').val(proj.pap_id);
                $('#target_name').val('paper');
                this.action = 'edit';
                this.actionName = 'serviceawards/editPaper/';
            }    
        },

        addpaper: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.pij    = '';
            this.title  = '';
            this.name   = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addPaper/';
        },

        getValidate: function(e) {
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.name == ''){
                this.haserror.name         = 'has-error';
                this.Error.name            = 'This field is required.';
                this.ErrorValidation.name  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Mentorings

new Vue({
    el: '#srvMentorings',

    data: {
        mentorings:  [],
        fields: [],
        insts: [],
        mid: '',
        field: '',
        inst: '',
        course: '',
        target: '',
        output: '',
        haserror: {
            field: '',
            inst: '',
            course: '',
            target: '',
            output: ''
        },
        Error: {
            field: '',
            inst: '',
            course: '',
            target: '',
            output: ''
        },
        ErrorValidation: {
            field:  false,
            inst:   false,
            course: false,
            target: false,
            output: false,
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchPapers();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            console.log(val);
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'inst': function(val) {
            if(val == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }else{
                if(!this.inst.replace(/\s/g, '').length){
                    this.haserror.inst         = 'has-error';
                    this.Error.inst            = 'Invalid input.';
                    this.ErrorValidation.inst  = true;
                }else{
                    this.haserror.inst         = '';
                    this.Error.inst            = '';
                    this.ErrorValidation.inst  = false;
                }
            }
        },
        'course': function(val) {
            if(val == ''){
                this.haserror.course         = 'has-error';
                this.Error.course            = 'This field is required.';
                this.ErrorValidation.course  = true;
            }else{
                if(!this.course.replace(/\s/g, '').length){
                    this.haserror.course         = 'has-error';
                    this.Error.course            = 'Invalid input.';
                    this.ErrorValidation.course  = true;
                }else{
                    this.haserror.course         = '';
                    this.Error.course            = '';
                    this.ErrorValidation.course  = false;
                }
            }
        },
        'target': function(val) {
            if(val == ''){
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }else{
                if(!this.target.replace(/\s/g, '').length){
                    this.haserror.target         = 'has-error';
                    this.Error.target            = 'Invalid input.';
                    this.ErrorValidation.target  = true;
                }else{
                    var matches = val.match(/^\d+$/g);
                    if(matches == null){
                        this.haserror.target         = 'has-error';
                        this.Error.target            = 'Invalid input.';
                        this.ErrorValidation.target  = true;
                    }else{
                        this.haserror.target         = '';
                        this.Error.target            = '';
                        this.ErrorValidation.target  = false;
                    }
                }
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },
    methods: {
        fetchPapers: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchMentoring/'+$('#sciid').val()+'/0', function(mentorings) {
                this.$set('mentorings', mentorings);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
            this.$http.get(this.baseUrl + 'institutions/fetchInst', function(insts) {
                this.$set('insts', insts);
            });
        },

        getRowValue: function (ment) {
            if(ment != undefined){
                this.mid     = ment.men_id;
                this.field   = ment.men_field;
                this.inst    = ment.men_institution;
                this.course  = ment.men_course;
                this.target  = ment.men_notargetstudents;
                this.output  = ment.men_output;
                $('#semid_edit').val(ment.men_id);
                $('#targetPane').val('target');
                $('#target_id').val(ment.men_id);
                $('#target_name').val('stud');
                this.action = 'edit';
                this.actionName = 'serviceawards/editMentoring/';
            }    
        },

        addMentoring: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.mid     = '';
            this.field   = '';
            this.inst    = '';
            this.course  = '';
            this.target  = '';
            this.output  = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addMentoring/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.inst == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }
            if(this.course == ''){
                this.haserror.course         = 'has-error';
                this.Error.course            = 'This field is required.';
                this.ErrorValidation.course  = true;
            }
            if(this.target == ''){
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Curriculum

new Vue({
    el: '#srvCurriculum',

    data: {
        curiculums:  [],
        fields: [],
        insts: [],
        pij: '',
        inst: '',
        field: '',
        output: '',
        haserror: {
            inst: '',
            field: '',
            output: '',
        },
        ErrorValidation: {
            inst: '',
            field: '',
            output: '',
        },
        Error: {
            inst:   false,
            field:  false,
            output: false,
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchCuriculums();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'inst': function(val) {
            if(val == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }else{
                if(!this.inst.replace(/\s/g, '').length){
                    this.haserror.inst         = 'has-error';
                    this.Error.inst            = 'Invalid input.';
                    this.ErrorValidation.inst  = true;
                }else{
                    this.haserror.inst         = '';
                    this.Error.inst            = '';
                    this.ErrorValidation.inst  = false;
                }
            }
        },
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },
    methods: {
        fetchCuriculums: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchCuriculum/'+$('#sciid').val()+'/0', function(curiculums) {
                this.$set('curiculums', curiculums);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
            this.$http.get(this.baseUrl + 'institutions/fetchInst', function(insts) {
                this.$set('insts', insts);
            });
        },

        getRowValue: function (curs) {
            if(curs != undefined){
                this.pij    = curs.cur_id;
                this.inst   = curs.cur_institution;
                this.field  = curs.cur_field;
                this.output = curs.cur_output;
                $('#semid_edit').val(curs.cur_id);
                $('#targetPane').val('target');
                $('#target_id').val(curs.cur_id);
                $('#target_name').val('cur');
                this.action = 'edit';
                this.actionName = 'serviceawards/editCuriculum/';
            }    
        },

        addcuriculum: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.pij    = '';
            this.inst   = '';
            this.field  = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addCuriculum/';
        },

        getValidate: function(e) {
            if(this.inst == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Networks

new Vue({
    el: '#srvNetworks',

    data: {
        networks:  [],
        fields: [],
        nid: '',
        field: '',
        output: '',
        Error: {
            field: '',
            output: '',
        },
        ErrorValidation: {
            field: '',
            output: '',
        },
        haserror: {
            field: '',
            output: '',
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchNetworks();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetchNetworks: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchNetwork/'+$('#sciid').val()+'/0', function(networks) {
                this.$set('networks', networks);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (nets) {
            if(nets != undefined){
                this.nid    = nets.net_id;
                this.field  = nets.net_field;
                this.output  = nets.net_output;
                $('#semid_edit').val(nets.net_id);
                $('#targetPane').val('target');
                $('#target_id').val(nets.net_id);
                $('#target_name').val('net');
                this.action = 'edit';
                this.actionName = 'serviceawards/editNetwork/';
            }    
        },

        addNetwork: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.nid = '';
            this.field = '';
            this.output = '';
            console.log('add');
            this.action = 'add';
            this.actionName = 'serviceawards/addNetwork/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Research

new Vue({
    el: '#srvResearch',

    data: {
        research:  [],
        fields: [],
        nid: '',
        field: '',
        output: '',
        Error: {
            field: '',
            output: '',
        },
        ErrorValidation: {
            field: '',
            output: '',
        },
        haserror: {
            field: '',
            output: '',
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchResearch();
        this.fetchOptions();
    },

    watch: {
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetchResearch: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchResearch/'+$('#sciid').val()+'/0', function(research) {
                this.$set('research', research);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (res) {
            if(res != undefined){
                this.nid    = res.res_id;
                this.field  = res.res_field;
                this.output = res.res_output;
                $('#semid_edit').val(res.res_id);
                $('#targetPane').val('target');
                $('#target_id').val(res.res_id);
                $('#target_name').val('res');
                this.action = 'edit';
                this.actionName = 'serviceawards/editResearch/';
            }    
        },

        addResearch: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.nid = '';
            this.field = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addResearch/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Others

new Vue({
    el: '#srvOthers',

    data: {
        others:  [],
        fields: [],
        nid: '',
        field: '',
        output: '',
        Error: {
            field: '',
            output: '',
        },
        ErrorValidation: {
            field: '',
            output: '',
        },
        haserror: {
            field: '',
            output: '',
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchOther();
        this.fetchOptions();
    },

    watch: {
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetchOther: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchOther/'+$('#sciid').val()+'/0', function(others) {
                this.$set('others', others);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (oth) {
            if(oth != undefined){
                this.nid    = oth.oth_id;
                this.field  = oth.oth_field;
                this.output = oth.oth_output;
                $('#semid_edit').val(oth.oth_id);
                $('#targetPane').val('target');
                $('#target_id').val(oth.oth_id);
                $('#target_name').val('oth');
                this.action = 'edit';
                this.actionName = 'serviceawards/editOther/';
            }    
        },

        addOther: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.nid = '';
            this.field = '';
            this.output = '';
            console.log('add');
            this.action = 'add';
            this.actionName = 'serviceawards/addOther/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});
