new Vue({
    el: '#accompSeminars',

    data: {
        seminars:  [],
        files: [],
        participantstype: [],
        fields: [],
        sid: '',
        field: '',
        title: '',
        accompDate: '',
        venue: '',
        part: '',
        actual: '',
        output: '',
        haserror: {
            field: '',
            title: '',
            accompDate: '',
            venue: '',
            part: '',
            actual: '',
            output: ''
        },
        Error: {
            field: '',
            title: '',
            accompDate: '',
            venue: '',
            part: '',
            actual: '',
            output: ''
        },
        ErrorValidation: {
            field: '',
            title: '',
            accompDate: '',
            venue: '',
            part: '',
            actual: '',
            output: ''
        },
        semid: '',
        baseUrl: '',
        action: '',
        actionName: '',
        btnsave: 'Save',
        removefiles: '',
        btnClass: 'primary',
    },

    ready: function() {
        this.fetchSeminar();
        this.fetchOptions();
        $("#txtsemDate").on('keydown blur', function(){
            if($("#txtsemDate").val() != ''){
                $('#divaccompDate').removeClass('has-error');
                $('#spanaccompDate').hide();
            }
            if($('#txtsemDate').val() == 'YYYY-DD-MM'){
                $('#divaccompDate').addClass('has-error');
                $('#spanaccompDate').html('This field is required.');
                $('#spanaccompDate').show();
            }
        });
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'venue': function(val) {
            if(val == ''){
                this.haserror.venue         = 'has-error';
                this.Error.venue            = 'This field is required.';
                this.ErrorValidation.venue  = true;
            }else{
                if(!this.venue.replace(/\s/g, '').length){
                    this.haserror.venue         = 'has-error';
                    this.Error.venue            = 'Invalid input.';
                    this.ErrorValidation.venue  = true;
                }else{
                    this.haserror.venue         = '';
                    this.Error.venue            = '';
                    this.ErrorValidation.venue  = false;
                }
            }
        },
        'part': function(val) {
            if(val != ''){
                this.haserror.part         = '';
                this.Error.part            = '';
                this.ErrorValidation.part  = false;
            }else{
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
        },
        'actual': function(val) {
            if(val != ''){
                var matches = val.match(/^\d+$/g);
                if(matches == null){
                    this.haserror.actual         = 'has-error';
                    this.Error.actual            = 'Invalid input.';
                    this.ErrorValidation.actual  = true;
                }else{
                    this.haserror.actual         = '';
                    this.Error.actual            = '';
                    this.ErrorValidation.actual  = false;
                }
            }else{
                this.haserror.actual         = 'has-error';
                this.Error.actual            = 'This field is required.';
                this.ErrorValidation.actual  = true;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },
    methods: {
        fetchSeminar: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchSeminar/'+$('#sciid').val()+'/1', function(seminars) {
                this.$set('seminars', seminars);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchparticipantstype', function(participantstype) {
                this.$set('participantstype', participantstype);
            });
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
            this.$http.get(this.baseUrl + 'serviceawards/fetchFiles/'+$('#sciid').val(), function(files) {
                this.$set('files', files);
            });
        },

        getRowValue: function (sem) {
            for (var i = 1; i <= this.files.length; i++) {
                $('#fileno'+i).show();
            }
            this.removefiles = '';
            this.btnClass = 'primary';
            this.btnsave = 'Save';
            if(sem != undefined){
                this.semid = sem.sem_id;
                this.sid    = sem.sem_id;
                this.field  = sem.sem_field;
                this.title  = sem.sem_title;
                this.accompDate = sem.sem_date;
                this.venue  = sem.sem_venue;
                this.part   = sem.sem_participantstype;
                this.actual = sem.sem_actualparticipants;
                this.output = sem.sem_output;
                $('#targetPane').val('accomp');
                $('#target_id').val(sem.sem_id);
                $('#target_name').val('sem_a');
                $('#atachment').text('including the atachment/s');
                this.action = 'edit';
                this.actionName = 'serviceawards/editSeminars/';
            }    
        },

        addAccSeminar: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.sid     = '';
            this.field   = '';
            this.title   = '';
            this.accompDate = '';
            this.part    = '';
            this.actual  = '';
            this.venue   = '';
            this.output  = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addSeminars/';
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
        },

        getAttachmentID: function(semacct_id) {
            this.removefiles = this.removefiles + ';' + semacct_id;
            $('#fileno'+semacct_id).hide();
        },

        getValidate: function(e) {
            this.accompDate = $("#txtsemDate").val();
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.accompDate == ''){
                this.haserror.accompDate         = 'has-error';
                this.Error.accompDate            = 'This field is required.';
                $('#divaccompDate').addClass('has-error');
                $('#spanaccompDate').show();
                this.ErrorValidation.accompDate  = true;
            }else{
                this.ErrorValidation.accompDate = false;
            }

            if(this.venue == ''){
                this.haserror.venue         = 'has-error';
                this.Error.venue            = 'This field is required.';
                this.ErrorValidation.venue  = true;
            }
            if(this.part == ''){
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
            if(this.actual == ''){
                this.haserror.actual         = 'has-error';
                this.Error.actual            = 'This field is required.';
                this.ErrorValidation.actual  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }
            if(cont){
                e.preventDefault();
            }
            if(this.btnsave == 'Save'){
                if(this.removefiles != ''){
                    e.preventDefault();
                    cont = false;
                    this.btnClass = 'success';
                    this.btnsave = 'Continue';
                }
            }
        },
    }
});

// Training

new Vue({
    el: '#accompTrainings',

    data: {
        trainings:  [],
        files: [],
        participantstype: [],
        fields: [],
        sid: '',
        field: '',
        title: '',
        accompDate: '',
        venue: '',
        part: '',
        actual: '',
        output: '',
        ErrorValidation: {
            field: false,
            title: false,
            accompDate: false,
            venue: false,
            part: false,
            actual: false,
            output: false,
        },
        Error: {
            field: '',
            title: '',
            accompDate: '',
            venue: '',
            part: '',
            actual: '',
            output: ''
        },
        haserror: {
            field: '',
            title: '',
            accompDate: '',
            venue: '',
            part: '',
            actual: '',
            output: ''
        },
        semid: '',
        baseUrl: '',
        action: '',
        actionName: '',
        btnClass: 'primary',
        btnsave: 'Save',
        removefiles: '',
    },

    ready: function() {
        this.fetcTrainings();
        this.fetchOptions();
        $("#txtsemDate").on('keydown blur', function(){
            if($("#txtsemDate").val() != ''){
                $('#divaccompDate').removeClass('has-error');
                $('#spanaccompDate').hide();
            }
            if($('#txtsemDate').val() == 'YYYY-DD-MM'){
                $('#divaccompDate').addClass('has-error');
                $('#spanaccompDate').html('This field is required.');
                $('#spanaccompDate').show();
            }
            console.log($("#txtsemDate").val())
        });
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'venue': function(val) {
            if(val == ''){
                this.haserror.venue         = 'has-error';
                this.Error.venue            = 'This field is required.';
                this.ErrorValidation.venue  = true;
            }else{
                if(!this.venue.replace(/\s/g, '').length){
                    this.haserror.venue         = 'has-error';
                    this.Error.venue            = 'Invalid input.';
                    this.ErrorValidation.venue  = true;
                }else{
                    this.haserror.venue         = '';
                    this.Error.venue            = '';
                    this.ErrorValidation.venue  = false;
                }
            }
        },
        'part': function(val) {
            if(val != ''){
                this.haserror.part         = '';
                this.Error.part            = '';
                this.ErrorValidation.part  = false;
            }else{
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
        },
        'actual': function(val) {
            if(val != ''){
                var matches = val.match(/^\d+$/g);
                if(matches == null){
                    this.haserror.actual         = 'has-error';
                    this.Error.actual            = 'Invalid input.';
                    this.ErrorValidation.actual  = true;
                }else{
                    this.haserror.actual         = '';
                    this.Error.actual            = '';
                    this.ErrorValidation.actual  = false;
                }
            }else{
                this.haserror.actual         = 'has-error';
                this.Error.actual            = 'This field is required.';
                this.ErrorValidation.actual  = true;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetcTrainings: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchTrainings/'+$('#sciid').val()+'/1', function(trainings) {
                this.$set('trainings', trainings);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchparticipantstype', function(participantstype) {
                this.$set('participantstype', participantstype);
            });
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
            this.$http.get(this.baseUrl + 'serviceawards/fetchFiles/'+$('#sciid').val(), function(files) {
                this.$set('files', files);
            });
        },

        getRowValue: function (tra) {
            if(tra != undefined){
                this.semid = tra.tra_id;
                this.sid    = tra.tra_id;
                this.field  = tra.tra_field;
                this.title  = tra.tra_title;
                this.accompDate   = tra.tra_date;
                this.venue  = tra.tra_venue;
                this.part   = tra.tra_participantstype;
                this.actual = tra.tra_actualparticipants;
                this.output = tra.tra_output;
                $('#targetPane').val('accomp');
                $('#target_id').val(tra.tra_id);
                $('#target_name').val('tra_a');
                $('#atachment').text('including the atachment/s');
                this.action = 'edit';
                this.actionName = 'serviceawards/editTraining/';
            }    
        },

        addAccTraining: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.sid     = '';
            this.field   = '';
            this.title   = '';
            this.accompDate = '';
            this.part    = '';
            this.actual  = '';
            this.venue   = '';
            this.output  = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addTraining/';
        },

        getValidate: function(e) {
            this.accompDate = $("#txtsemDate").val();
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.accompDate == ''){
                this.haserror.accompDate         = 'has-error';
                this.Error.accompDate            = 'This field is required.';
                $('#divaccompDate').addClass('has-error');
                $('#spanaccompDate').show();
                this.ErrorValidation.accompDate  = true;
            }else{
                this.ErrorValidation.accompDate = false;
            }

            if(this.venue == ''){
                this.haserror.venue         = 'has-error';
                this.Error.venue            = 'This field is required.';
                this.ErrorValidation.venue  = true;
            }
            if(this.part == ''){
                this.haserror.part         = 'has-error';
                this.Error.part            = 'This field is required.';
                this.ErrorValidation.part  = true;
            }
            if(this.actual == ''){
                this.haserror.actual         = 'has-error';
                this.Error.actual            = 'This field is required.';
                this.ErrorValidation.actual  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }
            if(cont){
                e.preventDefault();
            }
            if(this.btnsave == 'Save'){
                if(this.removefiles != ''){
                    e.preventDefault();
                    cont = false;
                    this.btnClass = 'success';
                    this.btnsave = 'Continue';
                }
            }
        },
    }
});

// Projects

new Vue({
    el: '#accompProjects',

    data: {
        projects:  [],
        fields: [],
        pij: '',
        field: '',
        title: '',
        obj: '',
        agency: '',
        stat: '',
        contri: '',
        haserror: {
            field: '',
            title: '',
            obj: '',
            agency: '',
            stat: '',
            contri: ''
        },
        Error: {
            field: '',
            title: '',
            obj: '',
            agency: '',
            stat: '',
            contri: ''
        },
        ErrorValidation: {
            field:  false,
            title:  false,
            obj:    false,
            agency: false,
            stat:   false,
            contri: false,
        },
        baseUrl: '',
        action: '',
        actionName: '',
        btnClass: 'primary',
        btnSave: 'Save',
    },

    ready: function() {
        this.fetchProject();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'obj': function(val) {
            if(val == ''){
                this.haserror.obj         = 'has-error';
                this.Error.obj            = 'This field is required.';
                this.ErrorValidation.obj  = true;
            }else{
                if(!this.obj.replace(/\s/g, '').length){
                    this.haserror.obj         = 'has-error';
                    this.Error.obj            = 'Invalid input.';
                    this.ErrorValidation.obj  = true;
                }else{
                    this.haserror.obj         = '';
                    this.Error.obj            = '';
                    this.ErrorValidation.obj  = false;
                }
            }
        },
        'agency': function(val) {
            if(val == ''){
                this.haserror.agency         = 'has-error';
                this.Error.agency            = 'This field is required.';
                this.ErrorValidation.agency  = true;
            }else{
                if(!this.agency.replace(/\s/g, '').length){
                    this.haserror.agency         = 'has-error';
                    this.Error.agency            = 'Invalid input.';
                    this.ErrorValidation.agency  = true;
                }else{
                    this.haserror.agency         = '';
                    this.Error.agency            = '';
                    this.ErrorValidation.agency  = false;
                }
            }
        },
        'stat': function(val) {
            if(val == ''){
                this.haserror.stat         = 'has-error';
                this.Error.stat            = 'This field is required.';
                this.ErrorValidation.stat  = true;
            }else{
                if(!this.stat.replace(/\s/g, '').length){
                    this.haserror.stat         = 'has-error';
                    this.Error.stat            = 'Invalid input.';
                    this.ErrorValidation.stat  = true;
                }else{
                    this.haserror.stat         = '';
                    this.Error.stat            = '';
                    this.ErrorValidation.stat  = false;
                }
            }
        },
        'contri': function(val) {
            if(val == ''){
                this.haserror.contri         = 'has-error';
                this.Error.contri            = 'This field is required.';
                this.ErrorValidation.contri  = true;
            }else{
                if(!this.contri.replace(/\s/g, '').length){
                    this.haserror.contri         = 'has-error';
                    this.Error.contri            = 'Invalid input.';
                    this.ErrorValidation.contri  = true;
                }else{
                    this.haserror.contri         = '';
                    this.Error.contri            = '';
                    this.ErrorValidation.contri  = false;
                }
            }
        },
    },

    methods: {
        fetchProject: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchProjects/'+$('#sciid').val()+'/1', function(projects) {
                this.$set('projects', projects);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (proj) {
            if(proj != undefined){
                this.pij     = proj.prj_id;
                this.field   = proj.prj_field;
                this.title   = proj.prj_title;
                this.obj     = proj.prj_objective;
                this.agency  = proj.prj_agency;
                this.stat    = proj.prj_status;
                this.contri  = proj.prj_contributions;
                $('#targetPane').val('accomp');
                $('#target_id').val(proj.prj_id);
                $('#target_name').val('proj_a');
                this.action = 'edit';
                this.actionName = 'serviceawards/editProject/';
            }    
        },

        addAccompProj: function(e) {
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.pij    = '';
            this.field  = '';
            this.title  = '';
            this.obj    = '';
            this.agency = '';
            this.stat   = '';
            this.contri = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addProject/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.obj == ''){
                this.haserror.obj         = 'has-error';
                this.Error.obj            = 'This field is required.';
                this.ErrorValidation.obj  = true;
            }
            if(this.agency == ''){
                this.haserror.agency         = 'has-error';
                this.Error.agency            = 'This field is required.';
                this.ErrorValidation.agency  = true;
            }
            if(this.stat == ''){
                this.haserror.stat         = 'has-error';
                this.Error.stat            = 'This field is required.';
                this.ErrorValidation.stat  = true;
            }
            if(this.contri == ''){
                this.haserror.contri         = 'has-error';
                this.Error.contri            = 'This field is required.';
                this.ErrorValidation.contri  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }
            if(cont){
                e.preventDefault();
            }
            if(this.btnsave == 'Save'){
                if(this.removefiles != ''){
                    e.preventDefault();
                    cont = false;
                    this.btnClass = 'success';
                    this.btnsave = 'Continue';
                }
            }
        },
    }
});

// Papers

new Vue({
    el: '#accompPapers',

    data: {
        papers:  [],
        fields: [],
        pij: '',
        field: '',
        isbn: '',
        name: '',
        title: '',
        subdate: '',
        pubdate: '',
        nopages: '',
        authors: '',
        haserror: {
            field: '',
            isbn: '',
            name: '',
            title: '',
            subdate: '',
            pubdate: '',
            nopages: '',
            authors: '',
        },
        Error: {
            field: '',
            isbn: '',
            name: '',
            title: '',
            subdate: '',
            pubdate: '',
            nopages: '',
            authors: '',
        },
        ErrorValidation: {
            field: false,
            isbn: false,
            name: false,
            title: false,
            subdate: false,
            pubdate: false,
            nopages: false,
            authors: false,
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchPapers();
        this.fetchOptions();
        $("#txtsubdate").on('keydown blur', function(){
            if($("#txtsubdate").val() != ''){
                $('#divSubdate').removeClass('has-error');
                $('#spanSubdate').hide();
            }
            if($('#txtsubdate').val() == 'YYYY-DD-MM'){
                $('#divSubdate').addClass('has-error');
                $('#spanSubdate').html('This field is required.');
                $('#spanSubdate').show();
            }
        });
        $("#txtpubdate").on('keydown blur', function(){
            if($("#txtpubdate").val() != ''){
                $('#divPubldate').removeClass('has-error');
                $('#spanPubldate').hide();
            }
            if($('#txtpubdate').val() == 'YYYY-DD-MM'){
                $('#divPubldate').addClass('has-error');
                $('#spanPubldate').html('This field is required.');
                $('#spanPubldate').show();
            }
        });
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'name': function(val) {
            if(val == ''){
                this.haserror.name         = 'has-error';
                this.Error.name            = 'This field is required.';
                this.ErrorValidation.name  = true;
            }else{
                if(!this.name.replace(/\s/g, '').length){
                    this.haserror.name         = 'has-error';
                    this.Error.name            = 'Invalid input.';
                    this.ErrorValidation.name  = true;
                }else{
                    this.haserror.name         = '';
                    this.Error.name            = '';
                    this.ErrorValidation.name  = false;
                }
            }
        },
        'isbn': function(val) {
            if(val == ''){
                this.haserror.isbn         = 'has-error';
                this.Error.isbn            = 'This field is required.';
                this.ErrorValidation.isbn  = true;
            }else{
                if(!this.isbn.replace(/\s/g, '').length){
                    this.haserror.isbn         = 'has-error';
                    this.Error.isbn            = 'Invalid input.';
                    this.ErrorValidation.isbn  = true;
                }else{
                    this.haserror.isbn         = '';
                    this.Error.isbn            = '';
                    this.ErrorValidation.isbn  = false;
                }
            }
        },
        'title': function(val) {
            if(val == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }else{
                if(!this.title.replace(/\s/g, '').length){
                    this.haserror.title         = 'has-error';
                    this.Error.title            = 'Invalid input.';
                    this.ErrorValidation.title  = true;
                }else{
                    this.haserror.title         = '';
                    this.Error.title            = '';
                    this.ErrorValidation.title  = false;
                }
            }
        },
        'nopages': function(val) {
            if(val != ''){
                var matches = val.match(/^\d+$/g);
                if(matches == null){
                    this.haserror.nopages         = 'has-error';
                    this.Error.nopages            = 'Invalid input.';
                    this.ErrorValidation.nopages  = true;
                }else{
                    this.haserror.nopages         = '';
                    this.Error.nopages            = '';
                    this.ErrorValidation.nopages  = false;
                }
            }else{
                this.haserror.nopages         = 'has-error';
                this.Error.nopages            = 'This field is required.';
                this.ErrorValidation.nopages  = true;
            }
        },
        'authors': function(val) {
            if(val == ''){
                this.haserror.authors         = 'has-error';
                this.Error.authors            = 'This field is required.';
                this.ErrorValidation.authors  = true;
            }else{
                if(!this.authors.replace(/\s/g, '').length){
                    this.haserror.authors         = 'has-error';
                    this.Error.authors            = 'Invalid input.';
                    this.ErrorValidation.authors  = true;
                }else{
                    this.haserror.authors         = '';
                    this.Error.authors            = '';
                    this.ErrorValidation.authors  = false;
                }
            }
        },
    },

    methods: {
        fetchPapers: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchPaper/'+$('#sciid').val()+'/1', function(papers) {
                this.$set('papers', papers);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (proj) {
            if(proj != undefined){
                this.pij        = proj.pap_id;
                this.field      = proj.pap_field;
                this.name       = proj.pap_name;
                this.isbn       = proj.pap_isbn;
                this.title      = proj.pap_title;
                this.subdate    = proj.pap_submission_date;
                this.pubdate    = proj.pap_published_date;
                this.nopages    = proj.pap_nopages;
                this.authors    = proj.pap_authors;
                $('#targetPane').val('accomp');
                $('#target_id').val(proj.pap_id);
                $('#target_name').val('paper_a');
                this.action = 'edit';
                this.actionName = 'serviceawards/editPaper/';
            }    
        },

        addaccompPaper: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.pij        = '';
            this.field      = '';
            this.isbn       = '';
            this.title      = '';
            this.subdate    = '';
            this.pubdate    = '';
            this.nopages    = '';
            this.authors    = '';
            this.action     = 'add';
            this.actionName = 'serviceawards/addPaper/';
        },

        getValidate: function(e){
            this.subdate = $("#txtsubdate").val();
            this.pubdate = $("#txtpubdate").val();
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.name == ''){
                this.haserror.name         = 'has-error';
                this.Error.name            = 'This field is required.';
                this.ErrorValidation.name  = true;
            }
            if(this.isbn == ''){
                this.haserror.isbn         = 'has-error';
                this.Error.isbn            = 'This field is required.';
                this.ErrorValidation.isbn  = true;
            }
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
            if(this.subdate == ''){
                this.haserror.subdate         = 'has-error';
                this.Error.subdate            = 'This field is required.';
                $('#divSubdate').addClass('has-error');
                $('#spanSubdate').show();
                this.ErrorValidation.subdate  = true;
            }else{
                this.ErrorValidation.subdate = false;
            }

            if(this.pubdate == ''){
                this.haserror.pubdate         = 'has-error';
                this.Error.pubdate            = 'This field is required.';
                $('#divPubldate').addClass('has-error');
                $('#spanPubldate').show();
                this.ErrorValidation.pubdate  = true;
            }else{
                this.ErrorValidation.pubdate = false;
            }

            if(this.nopages == ''){
                this.haserror.nopages         = 'has-error';
                this.Error.nopages            = 'This field is required.';
                this.ErrorValidation.nopages  = true;
            }
            if(this.authors == ''){
                this.haserror.authors         = 'has-error';
                this.Error.authors            = 'This field is required.';
                this.ErrorValidation.authors  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }
            if(cont){
                e.preventDefault();
            }
            if(this.btnsave == 'Save'){
                if(this.removefiles != ''){
                    e.preventDefault();
                    cont = false;
                    this.btnClass = 'success';
                    this.btnsave = 'Continue';
                }
            }
        },
    }
});

// Mentorings

new Vue({
    el: '#accompMentorings',

    data: {
        mentorings:  [],
        files: [],
        fields: [],
        insts: [],
        mid: '',
        field: '',
        inst: '',
        target: '',
        uploadfile: '',
        course: '',
        output: '',
        haserror: {
            field: '',
            inst: '',
            target: '',
            uploadfile: '',
            course: '',
            output: ''
        },
        ErrorValidation: {
            field: '',
            inst: '',
            target: '',
            uploadfile: '',
            course: '',
            output: ''
        },
        Error: {
            field: false,
            inst: false,
            target: false,
            uploadfile: false,
            course: false,
            output: false,
        },
        baseUrl: '',
        action: '',
        actionName: '',
        btnsave: 'Save',
        removefiles: '',
        btnClass: 'primary',
    },

    ready: function() {
        this.fetchPapers();
        this.fetchOptions();
        // $("#file-upload").change(function() {
        //     if($("#file-upload").val() != ''){
        //         $('#divfileUpload').removeClass('has-error');
        //         $('#spanfileUpload').hide();
        //         $('label.custom-file-upload').css('background-color','');
        //     }else{
        //         $('#divfileUpload').addClass('has-error');
        //         $('#spanfileUpload').show();
        //         $('label.custom-file-upload').css('background-color','#bb4442');
        //     }
        // });
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val != ''){
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }else{
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
        },
        'inst': function(val) {
            if(val == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }else{
                if(!this.inst.replace(/\s/g, '').length){
                    this.haserror.inst         = 'has-error';
                    this.Error.inst            = 'Invalid input.';
                    this.ErrorValidation.inst  = true;
                }else{
                    this.haserror.inst         = '';
                    this.Error.inst            = '';
                    this.ErrorValidation.inst  = false;
                }
            }
        },
        'target': function(val) {
            if(val != ''){
                var matches = val.match(/^\d+$/g);
                if(matches == null){
                    this.haserror.target         = 'has-error';
                    this.Error.target            = 'Invalid input.';
                    this.ErrorValidation.target  = true;
                }else{
                    this.haserror.target         = '';
                    this.Error.target            = '';
                    this.ErrorValidation.target  = false;
                }
            }else{
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }
        },
        'course': function(val) {
            if(val == ''){
                this.haserror.course         = 'has-error';
                this.Error.course            = 'This field is required.';
                this.ErrorValidation.course  = true;
            }else{
                if(!this.course.replace(/\s/g, '').length){
                    this.haserror.course         = 'has-error';
                    this.Error.course            = 'Invalid input.';
                    this.ErrorValidation.course  = true;
                }else{
                    this.haserror.course         = '';
                    this.Error.course            = '';
                    this.ErrorValidation.course  = false;
                }
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetchPapers: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchMentoring/'+$('#sciid').val()+'/1', function(mentorings) {
                this.$set('mentorings', mentorings);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
            this.$http.get(this.baseUrl + 'serviceawards/fetchFiles/'+$('#sciid').val(), function(files) {
                this.$set('files', files);
            });
            this.$http.get(this.baseUrl + 'institutions/fetchInst', function(insts) {
                this.$set('insts', insts);
            });
        },

        getRowValue: function (ment) {
            // for (var key in this.files) {
            //     if(this.files[key]['attch_sem_id'] == ment.men_id){
            //         this.fileattached.push(this.files[key]['attch_id']);
            //     }
            // }
            if(ment != undefined){
                this.mid     = ment.men_id;
                this.field   = ment.men_field;
                this.inst    = ment.men_institution;
                this.course  = ment.men_course;
                this.target  = ment.men_noactualstudents;
                this.output  = ment.men_output;
                $('#targetPane').val('accomp');
                $('#target_id').val(ment.men_id);
                $('#target_name').val('stud_a');
                this.action = 'edit';
                this.actionName = 'serviceawards/editMentoring/';
            }    
        },

        addAccompMentoring: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }
            this.mid     = '';
            this.field   = '';
            this.inst    = '';
            this.course  = '';
            this.target  = '';
            this.output  = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addMentoring/';
        },

        getAttachmentID: function(semacct_id) {
            this.removefiles = this.removefiles + ';' + semacct_id;
            $('#fileno'+semacct_id).hide();
            // this.fileattached.$remove(semacct_id);
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.inst == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }
            if(this.target == ''){
                this.haserror.target         = 'has-error';
                this.Error.target            = 'This field is required.';
                this.ErrorValidation.target  = true;
            }
            
            // if(this.action == 'add'){
            //     if($("#file-upload").val() == ''){
            //         this.haserror.uploadfile         = 'has-error';
            //         this.Error.uploadfile            = 'This field is required.';
            //         $('label.custom-file-upload').css('background-color','#bb4442');
            //         $('#divfileUpload').addClass('has-error');
            //         $('#spanfileUpload').show();
            //         this.ErrorValidation.uploadfile  = true;
            //     }else{
            //         this.ErrorValidation.uploadfile = false;
            //     }
            // }else{
            //     if(this.fileattached.length == 0){
            //         this.haserror.uploadfile         = 'has-error';
            //         this.Error.uploadfile            = 'This field is required.';
            //         $('label.custom-file-upload').css('background-color','#bb4442');
            //         $('#divfileUpload').addClass('has-error');
            //         $('#spanfileUpload').show();
            //         this.ErrorValidation.uploadfile  = true;
            //     }else{
            //         this.ErrorValidation.uploadfile = false;
            //     }
            // }

            if(this.course == ''){
                this.haserror.course         = 'has-error';
                this.Error.course            = 'This field is required.';
                this.ErrorValidation.course  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }
            if(cont){
                e.preventDefault();
            }
            if(this.btnsave == 'Save'){
                if(this.removefiles != ''){
                    e.preventDefault();
                    cont = false;
                    this.btnClass = 'success';
                    this.btnsave = 'Continue';
                }
            }
        },
    }
});

// Curriculum

new Vue({
    el: '#accompCurriculum',

    data: {
        curiculums:  [],
        fields: [],
        fields: [],
        insts: [],
        pij: '',
        inst: '',
        field: '',
        output: '',
        haserror: {
            inst: '',
            field: '',
            output: '',
        },
        Error: {
            inst: '',
            field: '',
            output: '',
        },
        ErrorValidation: {
            inst: '',
            field: '',
            output: '',
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchCuriculums();
        this.fetchOptions();
    },

    watch: {
        'inst': function(val) {
            if(val == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }else{
                if(!this.inst.replace(/\s/g, '').length){
                    this.haserror.inst         = 'has-error';
                    this.Error.inst            = 'Invalid input.';
                    this.ErrorValidation.inst  = true;
                }else{
                    this.haserror.inst         = '';
                    this.Error.inst            = '';
                    this.ErrorValidation.inst  = false;
                }
            }
        },
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    methods: {
        fetchCuriculums: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchCuriculum/'+$('#sciid').val()+'/1', function(curiculums) {
                this.$set('curiculums', curiculums);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
            this.$http.get(this.baseUrl + 'institutions/fetchInst', function(insts) {
                this.$set('insts', insts);
            });
        },

        getRowValue: function (curs) {
            if(curs != undefined){
                this.pij    = curs.cur_id;
                this.inst   = curs.cur_institution;
                this.field  = curs.cur_field;
                this.output = curs.cur_output;
                $('#targetPane').val('accomp');
                $('#target_id').val(curs.cur_id);
                $('#target_name').val('cur_a');
                this.action = 'edit';
                this.actionName = 'serviceawards/editCuriculum/';
            }    
        },

        addAccompcuriculum: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }

            this.pij    = '';
            this.inst   = '';
            this.field  = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addCuriculum/';
        },

        getValidate: function(e) {
            if(this.inst == ''){
                this.haserror.inst         = 'has-error';
                this.Error.inst            = 'This field is required.';
                this.ErrorValidation.inst  = true;
            }
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Networks

new Vue({
    el: '#accompNetworks',

    data: {
        networks:  [],
        fields: [],
        field: '',
        output: '',
        haserror: {
            field: '',
            output: '',
        },
        Error: {
            field: '',
            output: '',
        },
        ErrorValidation: {
            field: '',
            output: '',
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchNetworks();
        this.fetchOptions();
    },

    filters: {
        truncate: function(string, value) {
            return string.substring(0, value) + '...';
        }
    },

    watch: {
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetchNetworks: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchNetwork/'+$('#sciid').val()+'/1', function(networks) {
                this.$set('networks', networks);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (nets) {
            if(nets != undefined){
                this.nid    = nets.net_id;
                this.field  = nets.net_field;
                this.output  = nets.net_output;
                $('#targetPane').val('accomp');
                $('#target_id').val(nets.net_id);
                $('#target_name').val('net_a');
                this.action = 'edit';
                this.actionName = 'serviceawards/editNetwork/';
            }    
        },

        addAccompNetwork: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }

            this.nid = '';
            this.field = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addNetwork/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Research

new Vue({
    el: '#accompResearch',

    data: {
        research:  [],
        fields: [],
        nid: '',
        field: '',
        output: '',
        haserror: {
            field: '',
            output: '',
        },
        Error: {
            field: '',
            output: '',
        },
        ErrorValidation: {
            field: '',
            output: '',
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchResearch();
        this.fetchOptions();
    },

    watch: {
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetchResearch: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchResearch/'+$('#sciid').val()+'/1', function(research) {
                this.$set('research', research);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (res) {
            if(res != undefined){
                this.nid    = res.res_id;
                this.field  = res.res_field;
                this.output = res.res_output;
                $('#targetPane').val('accomp');
                $('#target_id').val(res.res_id);
                $('#target_name').val('res_a');
                this.action = 'edit';
                this.actionName = 'serviceawards/editResearch/';
            }    
        },

        addAccompResearch: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }

            this.nid = '';
            this.field = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addResearch/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

// Others

new Vue({
    el: '#accompOthers',

    data: {
        others:  [],
        fields: [],
        nid: '',
        field: '',
        output: '',
        haserror: {
            field: '',
            output: '',
        },
        Error: {
            field: '',
            output: '',
        },
        ErrorValidation: {
            field: '',
            output: '',
        },
        baseUrl: '',
        action: '',
        actionName: ''
    },

    ready: function() {
        this.fetchOther();
        this.fetchOptions();
    },

    watch: {
        'field': function(val) {
            if(val == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }else{
                this.haserror.field         = '';
                this.Error.field            = '';
                this.ErrorValidation.field  = false;
            }
        },
        'output': function(val) {
            if(val == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }else{
                if(!this.output.replace(/\s/g, '').length){
                    this.haserror.output         = 'has-error';
                    this.Error.output            = 'Invalid input.';
                    this.ErrorValidation.output  = true;
                }else{
                    this.haserror.output         = '';
                    this.Error.output            = '';
                    this.ErrorValidation.output  = false;
                }
            }
        },
    },

    methods: {
        fetchOther: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchOther/'+$('#sciid').val()+'/1', function(others) {
                this.$set('others', others);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'serviceawards/fetchfields', function(fields) {
                this.$set('fields', fields);
            });
        },

        getRowValue: function (oth) {
            if(oth != undefined){
                this.nid    = oth.oth_id;
                this.field  = oth.oth_field;
                this.output = oth.oth_output;
                $('#targetPane').val('accomp');
                $('#target_id').val(oth.oth_id);
                $('#target_name').val('oth_a');
                this.action = 'edit';
                this.actionName = 'serviceawards/editOther/';
            }    
        },

        addAccompOther: function(e) {
            for (var key in this.ErrorValidation) {
                this.ErrorValidation[key] = false;
            }
            for (var key in this.haserror) {
                this.haserror[key] = '';
            }
            for (var key in this.Error) {
                this.Error[key] = '';
            }

            this.nid = '';
            this.field = '';
            this.output = '';
            this.action = 'add';
            this.actionName = 'serviceawards/addOther/';
        },

        getValidate: function(e) {
            if(this.field == ''){
                this.haserror.field         = 'has-error';
                this.Error.field            = 'This field is required.';
                this.ErrorValidation.field  = true;
            }
            if(this.output == ''){
                this.haserror.output         = 'has-error';
                this.Error.output            = 'This field is required.';
                this.ErrorValidation.output  = true;
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});

//Reports
new Vue({
    el: '#reports_sa',

    data: {
        progreport: '',
        progreportSubDate: '',
        termreport: '',
        termreportSubDate: '',
        bspreport: '',
        bspreportSubDate: '',
        bspInsreport: '',
        bspInsreportSubDate: '',
        evalForm: '',
        evalFormSubDate: '',
        impeval: '',
        impevalSubDate: '',
        comments: '',
        Error: {
            progress: '',
            progressDate: '',
            termreport: '',
            termDate: '',
            bspreport: '',
            bspDate: '',
            bspInsreport: '',
            bspInsDate: '',
            evalForm: '',
            evalFormDate: '',
            impeval: '',
            impevalSubDate: '',
            comments: '',
        },
        ErrorValidation: {
            progress: false,
            progressDate: false,
            termreport: false,
            termDate: false,
            bspreport: false,
            bspDate: false,
            bspInsreport: false,
            bspInsDate: false,
            evalForm: false,
            evalFormDate: false,
            impeval: false,
            impevalSubDate: false,
            comments: false,
        },
        commenthaserror: '',
        urlAction: '',
        actionName: '',
    },

    ready: function() {
        $('#prdate').hide();
        $('#trdate').hide();
        $('#bspdate').hide();
        $('#bspInsdate').hide();
        $('#evalFormdate').hide();
        $('#impevaldate').hide();
        if(this.urlAction == 'addServiceAward')
            this.actionName = 'serviceawards/addReportSubmitted/';
        else
            this.actionName = 'serviceawards/editReportSubmitted/';

        $("#progreportSubDate").on('keydown blur', function(){
            if($("#progreportSubDate").val() != ''){
                $('#prdate').css('color', '');
                $('#progreportSubDate').css('border', '');
                $('#spanProgress').hide();
            }
        });

        $("#termreportSubDate").on('keydown blur', function(){
            if($("#termreportSubDate").val() != ''){
                $('#trdate').css('color', '');
                $('#termreportSubDate').css('border', '');
                $('#spanTerminal').hide();
            }
        });

        $("#bspreportSubDate").on('keydown blur', function(){
            if($("#bspreportSubDate").val() != ''){
                $('#bspdate').css('color', '');
                $('#bspreportSubDate').css('border', '');
                $('#spanbspfeedback').hide();
            }
        });

        $("#bspInsreportSubDate").on('keydown blur', function(){
            if($("#bspInsreportSubDate").val() != ''){
                $('#bspInsdate').css('color', '');
                $('#bspInsreportSubDate').css('border', '');
                $('#spanbspinsfeedback').hide();
            }
        });

        $("#evalFormSubDate").on('keydown blur', function(){
            if($("#evalFormSubDate").val() != ''){
                $('#evalFormdate').css('color', '');
                $('#evalFormSubDate').css('border', '');
                $('#spanevalform').hide();
            }
        });
        $("#impevalSubDate").on('keydown blur', function(){
            if($("#impevalSubDate").val() != ''){
                $('#impevaldate').css('color', '');
                $('#impevalSubDate').css('border', '');
                $('#spanimpeval').hide();
            }
        });

    },

    watch: {
        'progreport': function(val) {
            if(val!=''){
                this.Error.progress = '';
                $('#divProgress').css('color', '');
                this.ErrorValidation.progress = false;
                if(val == 'y')
                    $('#prdate').show();
                else
                    $('#prdate').hide(); 
            }
        },
        'termreport': function(val) {
            if(val!=''){
                this.Error.termreport = '';
                $('#divTerminal').css('color', '');
                this.ErrorValidation.termreport = false;
                if(val == 'y')
                    $('#trdate').show();
                else
                    $('#trdate').hide(); 
            }         
        },
        'bspreport': function(val) {
            if(val!=''){
                this.Error.bspreport = '';
                $('#divSubfeedback').css('color', '');
                this.ErrorValidation.bspreport = false;
                if(val == 'y')
                    $('#bspdate').show();
                else
                    $('#bspdate').hide();
            }          
        },
        'bspInsreport': function(val) {
            if(val!=''){
                this.Error.bspInsreport = '';
                $('#divInsfeedback').css('color', '');
                this.ErrorValidation.bspInsreport = false;
                if(val == 'y')
                    $('#bspInsdate').show();
                else
                    $('#bspInsdate').hide();
            }
        },
        'evalForm': function(val) {
            if(val!=''){
                this.Error.evalForm = '';
                $('#divEvalform').css('color', '');
                this.ErrorValidation.evalForm = false;
                if(val == 'y')
                    $('#evalFormdate').show();
                else
                    $('#evalFormdate').hide();
            }
        },
        'impeval': function(val) {
            if(val!=''){
                this.Error.impeval = '';
                $('#divimpeval').css('color', '');
                this.ErrorValidation.impeval = false;
                if(val == 'y')
                    $('#impevaldate').show();
                else
                    $('#impevaldate').hide();
            }
        },
        'comments': function(val){
            if(val != ''){
                if(!this.comments.replace(/\s/g, '').length){
                    this.commenthaserror         = 'has-error';
                    this.Error.comments            = 'Invalid input.';
                    this.ErrorValidation.comments  = true;
                }else{
                    this.commenthaserror         = '';
                    this.Error.comments            = '';
                    this.ErrorValidation.comments  = false;
                }
            }else{
                this.commenthaserror         = '';
                this.Error.comments            = '';
                this.ErrorValidation.comments  = false;
            }
        },
    },

    methods: {
        getValidate: function(e) {
            if(this.progreport == ''){
                $('#divProgress').css('color', '#bb4442');
                this.Error.progress = 'This field is required.';
                this.ErrorValidation.progress = true;
            }else{
                $('#divProgress').css('color', '');
                this.Error.progress = '';
                this.ErrorValidation.progress = false;
                if(this.progreport == 'y'){
                    if($('#progreportSubDate').val()==''){
                        $('#prdate').css('color', '#bb4442');
                        $('#progreportSubDate').css('border', '1px solid #bb4442');
                        this.Error.progressDate = 'This field is required.';
                        $('#spanProgress').show();
                        this.ErrorValidation.progressDate = true;
                    }else{
                        if(Date.parse($('#progreportSubDate').val()) > Date.parse('1901-01-01')==false){
                            $('#prdate').css('color', '#bb4442');
                            $('#progreportSubDate').css('border', '1px solid #bb4442');
                            this.Error.progressDate = 'Invalid Date.';
                            $('#spanProgress').show();
                            this.ErrorValidation.progressDate = true;
                        }else{
                            $('#prdate').css('color', '');
                            $('#progreportSubDate').css('border', '');
                            this.Error.progressDate = '';
                            this.ErrorValidation.progressDate = false;
                        }
                    }
                }
            }
            if(this.termreport == ''){
                $('#divTerminal').css('color', '#bb4442');
                this.Error.termreport = 'This field is required.';
                this.ErrorValidation.termreport = true;
            }else{
                $('#divTerminal').css('color', '');
                this.Error.termreport = '';
                this.ErrorValidation.termreport = false;
                if(this.termreport == 'y'){
                    if($('#termreportSubDate').val()==''){
                        $('#trdate').css('color', '#bb4442');
                        $('#termreportSubDate').css('border', '1px solid #bb4442');
                        this.Error.termDate = 'This field is required.';
                        $('#spanTerminal').show();
                        this.ErrorValidation.termDate = true;
                    }else{
                        if(Date.parse($('#termreportSubDate').val()) > Date.parse('1901-01-01')==false){
                            $('#trdate').css('color', '#bb4442');
                            $('#termreportSubDate').css('border', '1px solid #bb4442');
                            this.Error.termDate = 'Invalid Date.';
                            $('#spanTerminal').show();
                            this.ErrorValidation.termDate = true;
                        }else{
                            $('#trdate').css('color', '');
                            $('#termreportSubDate').css('border', '');
                            this.Error.termDate = '';
                            this.ErrorValidation.termDate = false;
                        }
                    }
                }
            }

            if(this.bspreport == ''){
                $('#divSubfeedback').css('color', '#bb4442');
                this.Error.bspreport = 'This field is required.';
                this.ErrorValidation.bspreport = true;
            }else{
                $('#divSubfeedback').css('color', '');
                this.Error.bspreport = '';
                this.ErrorValidation.bspreport = false;
                if(this.bspreport == 'y'){
                    if($('#bspreportSubDate').val()==''){
                        $('#bspdate').css('color', '#bb4442');
                        $('#bspreportSubDate').css('border', '1px solid #bb4442');
                        this.Error.bspDate = 'This field is required.';
                        $('#spanbspfeedback').show();
                        this.ErrorValidation.bspDate = true;
                    }else{
                        if(Date.parse($('#bspreportSubDate').val()) > Date.parse('1901-01-01')==false){
                            $('#bspdate').css('color', '#bb4442');
                            $('#bspreportSubDate').css('border', '1px solid #bb4442');
                            this.Error.bspDate = 'Invalid Date.';
                            $('#spanbspfeedback').show();
                            this.ErrorValidation.bspDate = true;
                        }else{
                            $('#bspdate').css('color', '');
                            $('#bspreportSubDate').css('border', '');
                            this.Error.bspDate = '';
                            this.ErrorValidation.bspDate = false;
                        }
                    }
                }
            }

            if(this.bspInsreport == ''){
                $('#divInsfeedback').css('color', '#bb4442');
                this.Error.bspInsreport = 'This field is required.';
                this.ErrorValidation.bspInsreport = true;
            }else{
                $('#divInsfeedback').css('color', '');
                this.Error.bspInsreport = '';
                this.ErrorValidation.bspInsreport = false;
                if(this.bspInsreport == 'y'){
                    if($('#bspInsreportSubDate').val()==''){
                        $('#bspInsdate').css('color', '#bb4442');
                        $('#bspInsreportSubDate').css('border', '1px solid #bb4442');
                        this.Error.bspInsDate = 'This field is required.';
                        $('#spanbspinsfeedback').show();
                        this.ErrorValidation.bspInsDate = true;
                    }else{
                        if(Date.parse($('#bspInsreportSubDate').val()) > Date.parse('1901-01-01')==false){
                            $('#bspInsdate').css('color', '#bb4442');
                            $('#bspInsreportSubDate').css('border', '1px solid #bb4442');
                            this.Error.bspInsDate = 'Invalid Date.';
                            $('#spanbspinsfeedback').show();
                            this.ErrorValidation.bspInsDate = true;
                        }else{
                            $('#bspInsdate').css('color', '');
                            $('#bspInsreportSubDate').css('border', '');
                            this.Error.bspInsDate = '';
                            this.ErrorValidation.bspInsDate = false;
                        }
                    }
                }
            }

            if(this.evalForm == ''){
                $('#divEvalform').css('color', '#bb4442');
                this.Error.evalForm = 'This field is required.';
                this.ErrorValidation.evalForm = true;
            }else{
                $('#divEvalform').css('color', '');
                this.Error.evalForm = '';
                this.ErrorValidation.evalForm = false;
                if(this.evalForm == 'y'){
                    if($('#evalFormSubDate').val()==''){
                        $('#evalFormdate').css('color', '#bb4442');
                        $('#evalFormSubDate').css('border', '1px solid #bb4442');
                        this.Error.evalFormDate = 'This field is required.';
                        $('#spanevalform').show();
                        this.ErrorValidation.evalFormDate = true;
                    }else{
                        if(Date.parse($('#evalFormSubDate').val()) > Date.parse('1901-01-01')==false){
                            $('#evalFormdate').css('color', '#bb4442');
                            $('#evalFormSubDate').css('border', '1px solid #bb4442');
                            this.Error.evalFormDate = 'Invalid Date.';
                            $('#spanevalform').show();
                            this.ErrorValidation.evalFormDate = true;
                        }else{
                            $('#evalFormdate').css('color', '');
                            $('#evalFormSubDate').css('border', '');
                            this.Error.evalFormDate = '';
                            this.ErrorValidation.evalFormDate = false;
                        }
                    }
                }
            }

            if(this.impeval == ''){
                $('#divimpeval').css('color', '#bb4442');
                this.Error.impeval = 'This field is required.';
                this.ErrorValidation.impeval = true;
            }else{
                $('#divimpeval').css('color', '');
                this.Error.impeval = '';
                this.ErrorValidation.impeval = false;
                if(this.impeval == 'y'){
                    if($('#impevalSubDate').val()==''){
                        $('#impevaldate').css('color', '#bb4442');
                        $('#impevalSubDate').css('border', '1px solid #bb4442');
                        this.Error.impevalSubDate = 'This field is required.';
                        $('#spanimpeval').show();
                        this.ErrorValidation.impevalSubDate = true;
                    }else{
                        if(Date.parse($('#impevalSubDate').val()) > Date.parse('1901-01-01')==false){
                            $('#impevaldate').css('color', '#bb4442');
                            $('#impevalSubDate').css('border', '1px solid #bb4442');
                            this.Error.impevalSubDate = 'Invalid Date.';
                            $('#spanimpeval').show();
                            this.ErrorValidation.impevalSubDate = true;
                        }else{
                            $('#impevaldate').css('color', '');
                            $('#impevalSubDate').css('border', '');
                            this.Error.impeval = '';
                            this.ErrorValidation.impevaldate = false;
                        }
                    }
                }
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
                // console.log(key+'=>'+this.ErrorValidation[key]);
            }

            if(cont){
                e.preventDefault();
            }
        },
    },

});


new Vue({
    el: '#assignsec',
    data: {
        processby: '',
        processdate: '',
        baseUrl: '',
        defuser: '',

        haserror: {
            processdate: '',
            processby: ''
        },

        Error: {
            processdate: '',
            processby: ''
        },

        ErrorValidation: {
            processdate: false,
            processby: false
        },
    },

    ready: function() {
        // $("#txtprocessdate").on('keydown blur keyup', function(){
        //     if($("#txtprocessdate").val() != ''){
        //         $('#processdatediv').removeClass('has-error');
        //         $('#processdatespan').hide();
        //     }else{
        //         $('#processdatediv').addClass('has-error');
        //         $('#processdatespan').html('This field is required.');
        //         $('#processdatespan').show();
        //     }
        // });
    },

    watch: {
        'processby': function(val) {
            if(val == ''){
                this.haserror.processby         = 'has-error';
                this.Error.processby            = 'This field is required.';
                this.ErrorValidation.processby  = true;
            }else{
                this.haserror.processby         = '';
                this.Error.processby            = '';
                this.ErrorValidation.processby  = false;
            }
        },
    },

    methods: {

        getValidate: function(e){
            // this.processdate = $("#txtprocessdate").val();
            // if(this.processby == ''){
            //     this.haserror.processby         = 'has-error';
            //     this.Error.processby            = 'This field is required.';
            //     this.ErrorValidation.processby  = true;
            // }
            // if($("#txtprocessdate").val() == ''){
            //     this.ErrorValidation.processdate = true;
            //     $('#processdatediv').addClass('has-error');
            //     $('#processdatespan').html('This field is required.');
            //     $('#processdatespan').show();
            // }else{
            //     this.ErrorValidation.processdate = false;
            // }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }

});
