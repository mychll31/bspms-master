// universities
new Vue({
    el: '#universities',

    data: {
        Error: {
            abbrv: '', name: ''
        },
        ErrorValidation: {
            abbrv: false, name: false
        },
        haserror: {
            abbrv: '', name: ''
        },
        baseUrl: '',

        validUnis: {},
        universities:[],
        courseArr: {},
        uniid: '',
        abbrv: '',
        name: '',

        action: 'universities/add',
    },

    ready: function(){
        this.fetchValidUnis();
    },

    watch: {
        'abbrv': function() {
            var matches = this.abbrv.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.abbrv == ""){
                this.haserror.abbrv          = 'has-error';
                this.Error.abbrv             = 'This field is required.';
                this.ErrorValidation.abbrv   = true;
            }else if(this.abbrv == " " || (!this.abbrv.replace(/\s/g, '').length)){
                this.haserror.abbrv          = 'has-error';
                this.Error.abbrv             = 'Invalid input.';
                this.ErrorValidation.abbrv = true;
            }else if(matches != null){
                this.haserror.abbrv          = 'has-error';
                this.Error.abbrv             = 'Invalid input.';
                this.ErrorValidation.abbrv = true;
            }else{
                this.haserror.abbrv          = '';
                this.Error.abbrv             = '';
                this.ErrorValidation.abbrv = false;
            }
        },

        'name': function() {
            var matches = this.name.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.name == ""){
                this.haserror.name          = 'has-error';
                this.Error.name             = 'This field is required.';
                this.ErrorValidation.name   = true;
            }else if(this.name == " " || (!this.name.replace(/\s/g, '').length)){
                this.haserror.name          = 'has-error';
                this.Error.name             = 'Invalid input.';
                this.ErrorValidation.name = true;
            }else if(matches != null){
                this.haserror.name          = 'has-error';
                this.Error.name             = 'Invalid input.';
                this.ErrorValidation.name = true;
            }else{
                this.haserror.name          = '';
                this.Error.name             = '';
                this.ErrorValidation.name = false;
            }
        },

    },

    methods: {
        fetchValidUnis: function() {
            this.$http.get(this.baseUrl + 'universities/fetchvalidUnis', function(validUnis) {
                this.$set('validUnis', validUnis);
            });
        },

        getRowValue: function(uni) {
            this.$http.get(this.baseUrl + 'universities/getUniversity/' + uni, function(unisArr) {
                this.$set('unisArr', unisArr);

                this.abbrv = this.unisArr[0].uni_abbrv;
                this.name = this.unisArr[0].uni_name;
                $('#txtdeluni').val(this.unisArr[0].uni_id);
                this.uniid = this.unisArr[0].uni_id;
            });
            this.action = 'universities/edit';
        },

        getValidate: function(e) {

            if(this.abbrv == ""){
                this.haserror.abbrv          = 'has-error';
                this.Error.abbrv             = 'This field is required.';
                this.ErrorValidation.abbrv   = true;
            }

            if(this.name == ""){
                this.haserror.name          = 'has-error';
                this.Error.name             = 'This field is required.';
                this.ErrorValidation.name   = true;
            }

            var couvalid = '';
            if(this.abbrv!= '' && this.name!=''){
                couvalid = this.abbrv + '-' + this.name;
                couvalid = (couvalid + '|').toLowerCase();
                if((this.validUnis.split(couvalid).length - 1) >= 1){
                    this.haserror.name           = 'has-error';
                    if(this.action == 'universities/edit')
                        this.Error.abbrv = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.abbrv = 'This data is already exists.';
                    this.ErrorValidation.name = true;
                    this.haserror.abbrv = 'has-error';
                    if(this.action == 'universities/edit')
                        this.Error.name = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.name = 'This data is already exists.';
                    this.ErrorValidation.abbrv    = true;
                }else{
                    this.haserror.name            = '';
                    this.Error.name               = '';
                    this.ErrorValidation.name     = false;
                    this.haserror.abbrv           = '';
                    this.Error.abbrv              = '';
                    this.ErrorValidation.abbrv    = false;
                }
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// specialization
new Vue({
    el: '#specialization',

    data: {
        Error: {
            code: '', desc: ''
        },
        ErrorValidation: {
            code: false, desc: false
        },
        haserror: {
            code: '', desc: ''
        },
        baseUrl: '',
        specializations:[],
        speid: '',
        code: '',
        desc: '',
        validspe: {},
        speArr: {},
        action: 'specialization/add',
    },

    ready: function(){
        this.fetchDesc();
    },

    watch: {
        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },
    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'specialization/fetchdesc', function(specializations) {
                this.$set('specializations', specializations);
            });
            this.$http.get(this.baseUrl + 'specialization/fetchvalidSpe', function(validspe) {
                this.$set('validspe', validspe);
            });
        },

        getRowValue: function(prof) {
            this.$http.get(this.baseUrl + 'specialization/getSpe/' + prof, function(speArr) {
                this.$set('speArr', speArr);

                this.desc = this.speArr[0].spe_desc;
                $('#txtdelspe').val(this.speArr[0].spe_id);
                this.speid = this.speArr[0].spe_id;
            });
            this.action = 'specialization/edit';
        },

        getValidate: function(e) {
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }
            
            var spevalid = '';
            if(this.desc!=''){
                spevalid = this.desc;
                spevalid = ('|' + spevalid + '|').toLowerCase();
                if((this.validspe.split(spevalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'specialization/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                }
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// professions
new Vue({
    el: '#professions',

    data: {
        Error: {
            code: '', desc: ''
        },
        ErrorValidation: {
            code: false, desc: false
        },
        haserror: {
            code: '', desc: ''
        },
        baseUrl: '',
        professions:[],
        validProf: {},
        profArr: {},
        proid: '',
        code: '',
        desc: '',

        action: 'professions/add',
    },

    ready: function(){
        this.fetchDesc();
    },

    watch: {

        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'professions/fetchdesc', function(professions) {
                this.$set('professions', professions);
            });
            this.$http.get(this.baseUrl + 'professions/fetchvalidProf', function(validProf) {
                this.$set('validProf', validProf);
            });
        },

        getRowValue: function(prof) {
            this.$http.get(this.baseUrl + 'professions/getProf/' + prof, function(profArr) {
                this.$set('profArr', profArr);

                this.desc = this.profArr[0].prof_desc;
                $('#txtdelpro').val(this.profArr[0].prof_id);
                this.proid = this.profArr[0].prof_id;
            });
            this.action = 'professions/edit';
        },

        getValidate: function(e) {
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }

            var profvalid = '';
            if(this.desc!=''){
                profvalid = this.desc;
                profvalid = (profvalid + '|').toLowerCase();
                if((this.validProf.split(profvalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'professions/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                }
            }
            
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// courses
new Vue({
    el: '#courses',

    data: {
        Error: {
            code: '', desc: ''
        },
        ErrorValidation: {
            code: false, desc: false
        },
        haserror: {
            code: '', desc: ''
        },
        baseUrl: '',

        validCourse: {},
        courses:[],
        courseArr: {},
        couid: '',
        code: '',
        desc: '',

        action: 'courses/add',
    },

    ready: function(){
        this.fetchDesc();
    },
    watch: {
        'code': function() {
            var matches = this.code.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.code == ""){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'This field is required.';
                this.ErrorValidation.code   = true;
            }else if(this.code == " " || (!this.code.replace(/\s/g, '').length)){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'Invalid input.';
                this.ErrorValidation.code = true;
            }else if(matches != null){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'Invalid input.';
                this.ErrorValidation.code = true;
            }else{
                this.haserror.code          = '';
                this.Error.code             = '';
                this.ErrorValidation.code = false;
            }
        },

        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'courses/fetchdesc', function(courses) {
                this.$set('courses', courses);
            });
            this.$http.get(this.baseUrl + 'courses/fetchvalidCourse', function(validCourse) {
                this.$set('validCourse', validCourse);
            });
        },

        getRowValue: function(course) {
            this.$http.get(this.baseUrl + 'courses/getCourses/' + course, function(courseArr) {
                this.$set('courseArr', courseArr);

                this.code = this.courseArr[0].cou_code;
                this.desc = this.courseArr[0].cou_desc;
                $('#txtempid').val(this.courseArr[0].cou_id);
                this.couid = this.courseArr[0].cou_id;
            });
            this.action = 'courses/edit';
        },

        getValidate: function(e) {

            if(this.code == ""){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'This field is required.';
                this.ErrorValidation.code   = true;
            }

            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }

            var couvalid = '';
            if(this.code!= '' && this.desc!=''){
                couvalid = this.code + '-' + this.desc;
                // couvalid = couvalid.replace(',', '');
                // couvalid = couvalid.replace(' ', '');
                couvalid = (couvalid + '|').toLowerCase();
                if((this.validCourse.split(couvalid).length - 1) >= 1){
                    this.haserror.desc           = 'has-error';
                    if(this.action == 'courses/edit')
                        this.Error.code = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.code = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                    this.haserror.code = 'has-error';
                    if(this.action == 'courses/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.code           = true;
                }else{
                    this.haserror.desc            = '';
                    this.Error.desc               = '';
                    this.ErrorValidation.desc     = false;
                    this.haserror.code                  = '';
                    this.Error.code                     = '';
                    this.ErrorValidation.code           = false;
                }
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// globalregions
new Vue({
    el: '#globalregions',

    data: {
        Error: {
            continent: '', country: ''
        },
        ErrorValidation: {
            continent: false, country: false
        },
        haserror: {
            continent: '', country: ''
        },
        baseUrl: '',
        regions:[],
        validglobals: {},
        globalArr: {},
        action: 'globalregions/add',

        globalid: '',
        continent: '',
        country: '',
    },

    ready: function(){
        this.fetchDesc();
    },

    watch: {
        'continent': function() {
           if(this.continent == ""){
                this.haserror.continent          = 'has-error';
                this.Error.continent             = 'This field is required.';
                this.ErrorValidation.continent   = true;
          }else{
                this.haserror.continent          = '';
                this.Error.continent             = '';
                this.ErrorValidation.continent = false;
          }
        },

        'country': function() {
          var matches = this.country.match(/[~!@#$%^&*_+=?><:;"]/g);

          if(this.country == ""){
                this.haserror.country          = 'has-error';
                this.Error.country             = 'This field is required.';
                this.ErrorValidation.country   = true;
          }else if(this.country == " " || (!this.country.replace(/\s/g, '').length)){
                this.haserror.country          = 'has-error';
                this.Error.country             = 'Invalid input.';
                this.ErrorValidation.country = true;
          }else if(matches != null){
                this.haserror.country          = 'has-error';
                this.Error.country             = 'Invalid input.';
                this.ErrorValidation.country = true;
          }else{
                this.haserror.country          = '';
                this.Error.country             = '';
                this.ErrorValidation.country = false;
          }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'globalregions/fetchdesc', function(regions) {
                this.$set('regions', regions);
            });
            this.$http.get(this.baseUrl + 'globalregions/fetchvalidglobal', function(validglobals) {
                this.$set('validglobals', validglobals);
            });
        },

        getRowValue: function(gloid) {
            this.$http.get(this.baseUrl + 'globalregions/getGlobals/' + gloid, function(globalArr) {
                this.$set('globalArr', globalArr);

                this.continent = this.globalArr[0].glo_continent;
                this.country = this.globalArr[0].glo_country;
                $('#txtdelglobal').val(this.globalArr[0].glo_id);
                this.globalid = this.globalArr[0].glo_id;
            });
            this.action = 'globalregions/edit';
        },

        getValidate: function(e) {
            if(this.continent == ""){
                this.haserror.continent          = 'has-error';
                this.Error.continent             = 'This field is required.';
                this.ErrorValidation.continent   = true;
            }

            if(this.country == ""){
                this.haserror.country          = 'has-error';
                this.Error.country             = 'This field is required.';
                this.ErrorValidation.country   = true;
            }

            var globalvalid = '';
            if(this.continent!= '' && this.country!=''){
                globalvalid = this.continent + '-' + this.country;
                globalvalid = (globalvalid + '|').toLowerCase();
                if((this.validglobals.split(globalvalid).length - 1) >= 1){
                    this.haserror.country = 'has-error';
                    if(this.action == 'globalregions/edit')
                        this.Error.continent = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.continent = 'This data is already exists.';
                    this.ErrorValidation.country = true;
                    this.haserror.continent = 'has-error';
                    if(this.action == 'globalregions/edit')
                        this.Error.country = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.country = 'This data is already exists.';
                    this.ErrorValidation.continent = true;
                }else{
                    this.haserror.country = '';
                    this.Error.country = '';
                    this.ErrorValidation.country = false;
                    this.haserror.continent = '';
                    this.Error.continent = '';
                    this.ErrorValidation.continent = false;
                }
            }
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// localregions
new Vue({
    el: '#localregions',

    data: {
        Error: {
            region: '', city: ''
        },
        ErrorValidation: {
            region: false, city: false
        },
        haserror: {
            region: '', city: ''
        },
        baseUrl: '',
        regions:[],
        validLocal: {},
        regArr: {},
        locregid: '',
        region: '',
        city: '',
        action: 'localregions/add',
    },

    ready: function(){
        this.fetchDesc();
    },

    watch: {

        'region': function() {
            if(this.region == ""){
                this.haserror.region          = 'has-error';
                this.Error.region             = 'This field is required.';
                this.ErrorValidation.region   = true;
            }else{
                this.haserror.region          = '';
                this.Error.region             = '';
                this.ErrorValidation.region = false;
          }
        },

        'city': function() {
          var matches = this.city.match(/[~!@#$%^&*_+=?><:;"]/g);

          if(this.city == ""){
                this.haserror.city          = 'has-error';
                this.Error.city             = 'This field is required.';
                this.ErrorValidation.city   = true;
          }else if(this.city == " " || (!this.city.replace(/\s/g, '').length)){
                this.haserror.city          = 'has-error';
                this.Error.city             = 'Invalid input.';
                this.ErrorValidation.city = true;
          }else if(matches != null){
                this.haserror.city          = 'has-error';
                this.Error.city             = 'Invalid input.';
                this.ErrorValidation.city = true;
          }else{
                this.haserror.city          = '';
                this.Error.city             = '';
                this.ErrorValidation.city = false;
          }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'localregions/fetchLocalRegs', function(regions) {
                this.$set('regions', regions);
            });
            this.$http.get(this.baseUrl + 'localregions/fetchvalidlocal', function(validLocal) {
                this.$set('validLocal', validLocal);
            });
        },

        getRowValue: function(reg) {
            console.log(reg);
            this.$http.get(this.baseUrl + 'localregions/getLocal/' + reg, function(regArr) {
                this.$set('regArr', regArr);

                this.region = this.regArr[0].loc_region;
                this.city = this.regArr[0].loc_province;
                $('#txtdelregion').val(this.regArr[0].loc_id);
                this.locregid = this.regArr[0].loc_id;
            });
            this.action = 'localregions/edit';
        },

        getValidate: function(e) {
            if(this.region == ""){
                this.haserror.region          = 'has-error';
                this.Error.region             = 'This field is required.';
                this.ErrorValidation.region   = true;
            }

            if(this.city == ""){
                this.haserror.city          = 'has-error';
                this.Error.city             = 'This field is required.';
                this.ErrorValidation.city   = true;
            }

            var localvalid = '';
            if(this.region!= '' && this.city!=''){
                localvalid = this.region + '-' + this.city;
                localvalid = (localvalid + '|').toLowerCase();
                if((this.validLocal.split(localvalid).length - 1) >= 1){
                    this.haserror.city = 'has-error';
                    if(this.action == 'localregions/edit')
                        this.Error.region = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.region = 'This data is already exists.';
                    this.ErrorValidation.city = true;
                    this.haserror.region = 'has-error';
                    if(this.action == 'localregions/edit')
                        this.Error.city = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.city = 'This data is already exists.';
                    this.ErrorValidation.region = true;
                }else{
                    this.haserror.city = '';
                    this.Error.city = '';
                    this.ErrorValidation.city = false;
                    this.haserror.region = '';
                    this.Error.region = '';
                    this.ErrorValidation.region = false;
                }
            }
            
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// councils
new Vue({
    el: '#councils',

    data: {
        Error: {
            code: '', desc: ''
        }, 
        ErrorValidation: {
            code: false, desc: false
        },
        haserror: {
            code: '', desc: ''
        },
        baseUrl: '',
        regions:[],
        valCouncil: {},
        couArr: {},
        couid: '',
        code: '',
        desc: '',

        action: 'council/add',
    },

    ready: function(){
        this.fetchDesc();
    },

    watch: {
        
        'code': function() {
            var matches = this.code.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.code == ""){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'This field is required.';
                this.ErrorValidation.code   = true;
            }else if(this.code == " " || (!this.code.replace(/\s/g, '').length)){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'Invalid input.';
                this.ErrorValidation.code = true;
            }else if(matches != null){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'Invalid input.';
                this.ErrorValidation.code = true;
            }else{
                this.haserror.code          = '';
                this.Error.code             = '';
                this.ErrorValidation.code = false;
            }
        },

        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        //fetchDesc
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'council/fetchdesc', function(regions) {
                this.$set('regions', regions);
            });
            this.$http.get(this.baseUrl + 'council/fetchvalidCouncil', function(valCouncil) {
                this.$set('valCouncil', valCouncil);
            });
        },

        getRowValue: function(cou) {
            this.$http.get(this.baseUrl + 'council/getCounc/' + cou, function(couArr) {
                this.$set('couArr', couArr);

                this.code = this.couArr[0].cil_code;
                this.desc = this.couArr[0].cil_desc;
                $('#txtdelcouncil').val(this.couArr[0].cil_id);
                this.couid = this.couArr[0].cil_id;
            });
            this.action = 'council/edit';
        },

        getValidate: function(e) {
            if(this.code == ""){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'This field is required.';
                this.ErrorValidation.code   = true;
            }

            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }

            var couvalid = '';
            if(this.code!= '' && this.desc!=''){
                couvalid = this.code + '-' + this.desc;
                couvalid = (couvalid + '|').toLowerCase();
                if((this.valCouncil.split(couvalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'council/edit')
                        this.Error.code = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.code = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                    this.haserror.code = 'has-error';
                    if(this.action == 'council/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.code = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                    this.haserror.code = '';
                    this.Error.code = '';
                    this.ErrorValidation.code = false;
                }
            }
            
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// Outcomes
new Vue({
    el: '#outcomes', 

    data: {
        Error: {
            order: '', desc: ''
        },
        ErrorValidation: {
            order: false, desc: false
        },
        haserror: {
            order: '', desc: ''
        },
        baseUrl: '',
        outcomes:[],
        outArr: {},
        validOut: {},
        outid: '',
        order: '',
        desc: '',

        action: 'outcome/add',
    },

    ready: function(){
        this.fetchDesc();
    },
	
    watch: {
        'order': function() {
            var matches = this.order.match(/[A-Za-z~!@#$%^&*()_+=?><:;"']/g);
            if(this.order == ""){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'This field is required.';
                this.ErrorValidation.order   = true;
            }else if(this.order == " " || (!this.order.replace(/\s/g, '').length)){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'Invalid input.';
                this.ErrorValidation.order = true;
            }else if(matches != null){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'Invalid input.';
                this.ErrorValidation.order = true;
            }else{
                this.haserror.order          = '';
                this.Error.order             = '';
                this.ErrorValidation.order = false;
            }
        },

        'desc': function() {
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'outcome/fetchdesc', function(outcomes) {
                this.$set('outcomes', outcomes);
            });
            this.$http.get(this.baseUrl + 'outcome/fetchvalidout', function(validOut) {
                this.$set('validOut', validOut);
            });
        },

        getRowValue: function(out) {
            this.$http.get(this.baseUrl + 'outcome/getOutcome/' + out, function(outArr) {
                this.$set('outArr', outArr);

                this.order           = this.outArr[0].out_number;
                this.desc    = this.outArr[0].out_desc;
                $('#txtTempId').val(this.outArr[0].out_id);
                this.outid          = this.outArr[0].out_id;
            });
            this.action             = 'outcome/edit';
        },

        getValidate: function(e) {
            if(this.order == ""){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'This field is required.';
                this.ErrorValidation.order   = true;
            }
 
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }
            
            var outvalid = '';
            if(this.order!= '' && this.desc!=''){
                outvalid = this.order + '-' + this.desc;
                outvalid = (outvalid + '|').toLowerCase();
                if((this.validOut.split(outvalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'outcome/edit')
                        this.Error.order = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.order = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                    this.haserror.order = 'has-error';
                    if(this.action == 'outcome/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.order = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                    this.haserror.order = '';
                    this.Error.order = '';
                    this.ErrorValidation.order = false;
                }
            }

            var cont = false; 
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});



// Priority Areas
new Vue({
    el: '#priorities', 

    data: {
       newSpe: {
            order: '', desc: ''
        },
        Error: {
            order: '', desc: ''
        },
        ErrorValidation: {
            order: false, desc: false
        },
        haserror: {
            order: '', desc: ''
        },
        baseUrl: '',
        priorities:[],
        validPrios: {},
        prioArr: {},
        priid: '',
        order: '',
        desc: '',

        action: 'priority/add',
    },

    ready: function(){
        this.fetchDesc();
    },
	
    watch: {
        'order': function() {
            var matches = this.order.match(/[A-Za-z~!@#$%^&*()_+=?><:;"']/g);
            if(this.order == ""){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'This field is required.';
                this.ErrorValidation.order   = true;
            }else if(this.order == " " || (!this.order.replace(/\s/g, '').length)){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'Invalid input.';
                this.ErrorValidation.order = true;
            }else if(matches != null){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'Invalid input.';
                this.ErrorValidation.order = true;
            }else{
                this.haserror.order          = '';
                this.Error.order             = '';
                this.ErrorValidation.order = false;
            }
        },

        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc   = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'priority/fetchdesc', function(priorities) {
                this.$set('priorities', priorities);
            });
            this.$http.get(this.baseUrl + 'priority/fetchvalidprio', function(validPrios) {
                this.$set('validPrios', validPrios);
            });
        },

        getRowValue: function(prio) {
            this.$http.get(this.baseUrl + 'priority/getPriorities/' + prio, function(prioArr) {
                this.$set('prioArr', prioArr);

                this.order = this.prioArr[0].pri_number;
                this.desc = this.prioArr[0].pri_desc;
                $('#txtdelprio').val(this.prioArr[0].pri_id);
                this.priid = this.prioArr[0].pri_id;
            });
            this.action = 'priority/edit';
        },

        getValidate: function(e) {
            if(this.order == ""){
                this.haserror.order          = 'has-error';
                this.Error.order             = 'This field is required.';
                this.ErrorValidation.order   = true;
            }

            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }

            var priovalid = '';
            if(this.order!= '' && this.desc!=''){
                priovalid = this.order + '-' + this.desc;
                priovalid = (priovalid + '|').toLowerCase();
                if((this.validPrios.split(priovalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'priority/edit')
                        this.Error.order = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.order = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                    this.haserror.order = 'has-error';
                    if(this.action == 'priority/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.order = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                    this.haserror.order = '';
                    this.Error.order = '';
                    this.ErrorValidation.order = false;
                }
            }
            
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});




// Educational Levels
new Vue({
    el: '#educationallevels',  

    data: {
        Error: {
            code: '', desc: ''
        },
        ErrorValidation: {
            code: false, desc: false
        },
        haserror: {
            code: '', desc: ''
        },
        baseUrl: '',
        educations:[],
        validEduc: {},
        educArr: {},
        eduid: '',
        code: '',
        desc: '',

        action: 'education/add',
    },

    ready: function(){
        this.fetchDesc();
    },
	
    watch: {
        'code': function() {
            var matches = this.code.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.code == ""){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'This field is required.';
                this.ErrorValidation.code   = true;
            }else if(this.code == " " || (!this.code.replace(/\s/g, '').length)){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'Invalid input.';
                this.ErrorValidation.code = true;
            }else if(matches != null){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'Invalid input.';
                this.ErrorValidation.code = true;
            }else{
                this.haserror.code          = '';
                this.Error.code             = '';
                this.ErrorValidation.code = false;
            }
        },

        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'education/fetchdesc', function(educations) {
                this.$set('educations', educations);
            });
            this.$http.get(this.baseUrl + 'education/fetchValidEduc', function(validEduc) {
                this.$set('validEduc', validEduc);
            });
        },

        getRowValue: function(educ) {
            this.$http.get(this.baseUrl + 'education/getEducations/' + educ, function(educArr) {
                this.$set('educArr', educArr);

                this.code = this.educArr[0].elev_code;
                this.desc = this.educArr[0].elev_desc;
                $('#txtdeledu').val(this.educArr[0].elev_id);
                this.eduid = this.educArr[0].elev_id;
            });
            this.action = 'education/edit';
        },

        getValidate: function(e) {
            if(this.code == ""){
                this.haserror.code          = 'has-error';
                this.Error.code             = 'This field is required.';
                this.ErrorValidation.code   = true;
            }
 
           if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }

            var educvalid = '';
            if(this.code!= '' && this.desc!=''){
                educvalid = this.code + '-' + this.desc;
                educvalid = (educvalid + '|').toLowerCase();
                if((this.validEduc.split(educvalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'education/edit')
                        this.Error.code = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.code = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                    this.haserror.code = 'has-error';
                    if(this.action == 'education/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.code = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                    this.haserror.code = '';
                    this.Error.code = '';
                    this.ErrorValidation.code = false;
                }
            }
            
            var cont = false; 
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});



// Fields (Accomplishments and Activities)
new Vue({
    el: '#fields',  

    data: {
        Error: {
            code: '', desc: ''
        },
        ErrorValidation: {
            code: false, desc: false
        },
        haserror: {
            code: '', desc: ''
        },
        baseUrl: '',
        fields:[],
        
        fieid: '',
        code: '',
        desc: '',

        validfield: {},
        fieldArr: {},
        action: 'fields/add',
    },

    ready: function(){
        this.fetchDesc();
    },
	
    watch: {
        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'fields/fetchdesc', function(fields) {
                this.$set('fields', fields);
            });
            this.$http.get(this.baseUrl + 'fields/fetchvalidfield', function(validfield) {
                this.$set('validfield', validfield);
            });
        },

        getRowValue: function(field) {
            this.$http.get(this.baseUrl + 'fields/getFields/' + field, function(fieldArr) {
                this.$set('fieldArr', fieldArr);

                this.desc = this.fieldArr[0].fld_desc;
                $('#txtdelfield').val(this.fieldArr[0].fld_id);
                this.fieid = this.fieldArr[0].fld_id;
            });
            this.action = 'fields/edit';
        },

        getValidate: function(e) {

            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }
            
            var fieldvalid = '';
            if(this.desc!=''){
                fieldvalid = this.desc;
                fieldvalid = (fieldvalid + '|').toLowerCase();
                if((this.validfield.split(fieldvalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'fields/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                }
            }

            var cont = false; 
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});


// Titles (Name)
new Vue({
    el: '#titles', 

    data: {
        Error: {
            abbv: '', desc: ''
        },
        ErrorValidation: {
            abbv: false, desc: false
        },
        haserror: {
            abbv: '', desc: ''
        },
        baseUrl: '',
        titles:[],
        titleid: '',
        validtitle: {},
        titleArr: {},
        abbv: '',
        desc: '',

        action: 'titles/add',
    },

    ready: function(){
        this.fetchDesc();
    },
	
    watch: {
        'abbv': function() {
            var matches = this.abbv.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.abbv == ""){
                this.haserror.abbv          = 'has-error';
                this.Error.abbv             = 'This field is required.';
                this.ErrorValidation.abbv   = true;
            }else if(this.abbv == " " || (!this.abbv.replace(/\s/g, '').length)){
                this.haserror.abbv          = 'has-error';
                this.Error.abbv             = 'Invalid input.';
                this.ErrorValidation.abbv = true;
            }else if(matches != null){
                this.haserror.abbv          = 'has-error';
                this.Error.abbv             = 'Invalid input.';
                this.ErrorValidation.abbv = true;
            }else{
                this.haserror.abbv          = '';
                this.Error.abbv             = '';
                this.ErrorValidation.abbv = false;
            }
        },

        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'titles/fetchdesc', function(titles) {
                this.$set('titles', titles);
            });
            this.$http.get(this.baseUrl + 'titles/getValidTitle', function(validtitle) {
                this.$set('validtitle', validtitle);
            });
        },

        getRowValue: function(prof) {
            this.$http.get(this.baseUrl + 'titles/getTitle/' + prof, function(titleArr) {
                this.$set('titleArr', titleArr);

                this.abbv = this.titleArr[0].tit_abbreviation;
                this.desc = this.titleArr[0].tit_desc;
                $('#txtdeltitle').val(this.titleArr[0].tit_id);
                this.titleid = this.titleArr[0].tit_id;
            });
            this.action = 'titles/edit';
        },


        getValidate: function(e) {
            if(this.abbv == ""){
                this.haserror.abbv          = 'has-error';
                this.Error.abbv             = 'This field is required.';
                this.ErrorValidation.abbv   = true;
            }

            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }

            var titlevalid = '';
            if(this.abbv!= '' && this.desc!=''){
                titlevalid = this.abbv + '-' + this.desc;
                titlevalid = (titlevalid + '|').toLowerCase();
                if((this.validtitle.split(titlevalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'titles/edit')
                        this.Error.abbv = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.abbv = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                    this.haserror.abbv = 'has-error';
                    if(this.action == 'titles/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.abbv = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                    this.haserror.abbv = '';
                    this.Error.abbv = '';
                    this.ErrorValidation.abbv = false;
                }
            }
            
            var cont = false; 
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});




// Citizenships
new Vue({
    el: '#citizenships',  

    data: {
        type: '', particular: '',
        Error: {
            type: '', particular: ''
        },
        ErrorValidation: {
            type: false, particular: false
        },
        haserror: {
            type: '', particular: ''
        },
        baseUrl: '',
        citizenships:[],
        citArr: {},
        validCit: [],
        speid: '',
        action: 'citizenship/add',
    },

    ready: function(){
        // fetch Desciption
        this.type = '';
        this.particular = '';
        this.fetchDesc();
    },
	
    watch: {

        'type': function() {
		   if(this.type == ""){
                this.haserror.type          = 'has-error';
                this.Error.type             = 'This field is required.';
                this.ErrorValidation.type   = true;
		  }else{
                this.haserror.type          = '';
                this.Error.type             = '';
                this.ErrorValidation.type = false;
          }
        },

        'particular': function() {
          var matches = this.particular.match(/[~!@#$%^&*_+=?><:;"]/g);

          if(this.particular == ""){
                this.haserror.particular          = 'has-error';
                this.Error.particular             = 'This field is required.';
                this.ErrorValidation.particular   = true;
          }else if(this.particular == " " || (!this.particular.replace(/\s/g, '').length)){
                this.haserror.particular          = 'has-error';
                this.Error.particular             = 'Invalid input.';
                this.ErrorValidation.particular = true;
          }else if(matches != null){
                this.haserror.particular          = 'has-error';
                this.Error.particular             = 'Invalid input.';
                this.ErrorValidation.particular = true;
          }else{
                this.haserror.particular          = '';
                this.Error.particular             = '';
                this.ErrorValidation.particular = false;
          }
        },
    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'citizenship/fetchCitizen', function(citizenships) {
                this.$set('citizenships', citizenships);
            });
            this.$http.get(this.baseUrl + 'citizenship/fetchvalidCit', function(validCit) {
                this.$set('validCit', validCit);
            });
        },

        getRowValue: function(cit) {
            this.$http.get(this.baseUrl + 'citizenship/getCitizen/' + cit, function(citArr) {
                this.$set('citArr', citArr);

                this.type           = this.citArr[0].cit_type;
                this.particular    = this.citArr[0].cit_particulars;
                $('#txtTempId').val(this.citArr[0].cit_id);
                this.speid          = this.citArr[0].cit_id;
            });
            this.action             = 'citizenship/edit';
        },

        getValidate: function(e) {
            if(this.type == ""){
                this.haserror.type = 'has-error';
                this.Error.type    = 'This field is required.';
                this.ErrorValidation.type = true;
            }

            if(this.particular == ""){
                this.haserror.particular = 'has-error';
                this.Error.particular    = 'This field is required.';
                this.ErrorValidation.particular = true;
            }

            var citvalid = '';
            if(this.type!= '' && this.particular!=''){
                citvalid = this.type + '-' + this.particular;
                citvalid = citvalid.replace(',', '');
                citvalid = citvalid.replace(' ', '');
                citvalid = (citvalid + '|').toLowerCase();
                if((this.validCit.split(citvalid).length - 1) >= 1){
                    this.haserror.particular           = 'has-error';
                    if(this.action == 'citizenship/edit')
                        this.Error.type                 = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.type                 = 'This data is already exists.';
                    this.ErrorValidation.particular     = true;
                    this.haserror.type                  = 'has-error';
                    if(this.action == 'citizenship/edit')
                        this.Error.particular           = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.particular           = 'This data is already exists.';
                    this.ErrorValidation.type           = true;
                }else{
                    this.haserror.particular            = '';
                    this.Error.particular               = '';
                    this.ErrorValidation.particular     = false;
                    this.haserror.type                  = '';
                    this.Error.type                     = '';
                    this.ErrorValidation.type           = false;
                }
            }

            var cont = false; 
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});



// Area of Expertise
new Vue({
    el: '#expertises',  

    data: {
       newSpe: {
            code: '', description: ''
        },
        Error: {
            code: '', description: ''
        },
        ErrorValidation: {
            code: false, description: false
        },
        haserror: {
            code: '', description: ''
        },
        code: '',
        description: '',

        baseUrl: '',
        action: 'expertise/add',
        expertises:[],
        expertArr: {},
        validExpertises:[],
        speid: '',
    },

    ready: function(){
        this.fetchDesc();
    },
	
    watch: {

        'description': function() {
            var matches = this.description.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.description == ""){
                this.haserror.description          = 'has-error';
                this.Error.description             = 'This field is required.';
                this.ErrorValidation.description   = true;
            }else if(this.description == " " || (!this.description.replace(/\s/g, '').length)){
                this.haserror.description          = 'has-error';
                this.Error.description             = 'Invalid input.';
                this.ErrorValidation.description = true;
            }else if(matches != null){
                this.haserror.description          = 'has-error';
                this.Error.description             = 'Invalid input.';
                this.ErrorValidation.description = true;
            }else{
                this.haserror.description          = '';
                this.Error.description             = '';
                this.ErrorValidation.description = false;
            }
        },

    },

    methods: {
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'expertise/fetchdesc', function(expertises) {
                this.$set('expertises', expertises);
            });
            this.$http.get(this.baseUrl + 'expertise/fetchValidExpert', function(validExpertises) {
                this.$set('validExpertises', validExpertises);
            });
        },

        getRowValue: function(expert) {
            this.$http.get(this.baseUrl + 'expertise/getExpertise/' + expert, function(expertArr) {
                this.$set('expertArr', expertArr);

                // this.code           = this.expertArr[0].exp_code;
                this.description    = this.expertArr[0].exp_desc;
                $('#txtTempId').val(this.expertArr[0].exp_id);
                this.speid          = this.expertArr[0].exp_id;
            });
            this.action         = 'expertise/edit';
        },

        getValidate: function(e) {

            if(this.description == ""){
                this.haserror.description = 'has-error';
                this.Error.description    = 'This field is required.';
                this.ErrorValidation.description = true;
            }
            
            var deco = '';
            if(this.description != ''){
                deco = (this.description).toLowerCase();
                if((this.validExpertises.split(deco).length - 1) >= 1){
                    this.haserror.description           = 'has-error';
                    if(this.action == 'expertise/edit')
                        this.Error.description                     = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.description                     = 'This data is already exists.';
                    this.ErrorValidation.description    = true;
                }else{
                    this.haserror.description           = '';
                    this.Error.description              = '';
                    this.ErrorValidation.description    = false;
                }
            }
            var cont = false; 
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});


// Participants
new Vue({
    el: '#participants',   

    data: {
        Error: {
            code: '', desc: ''
        },
        ErrorValidation: {
            code: false, desc: false
        },
        haserror: {
            code: '', desc: ''
        },
        baseUrl: '',
        participants:[],
        validPart: {},
        partArr: {},
        partid: '',
        code: '',
        desc: '',

        action: 'participants/add',
    },

    ready: function(){
        this.fetchDesc();
    },
	
    watch: {

        'desc': function() {
            var matches = this.desc.match(/[~!@#$%^&*_+=?><:;"]/g);
            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }else if(this.desc == " " || (!this.desc.replace(/\s/g, '').length)){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else if(matches != null){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'Invalid input.';
                this.ErrorValidation.desc = true;
            }else{
                this.haserror.desc          = '';
                this.Error.desc             = '';
                this.ErrorValidation.desc = false;
            }
        },

    },


    methods: {
        //fetchDesc
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'participants/fetchdesc', function(participants) {
                this.$set('participants', participants);
            });
            this.$http.get(this.baseUrl + 'participants/fetchvalidParticipant', function(validPart) {
                this.$set('validPart', validPart);
            });
        },

        getRowValue: function(part) {
            this.$http.get(this.baseUrl + 'participants/getPartic/' + part, function(partArr) {
                this.$set('partArr', partArr);

                this.desc = this.partArr[0].par_desc;
                $('#txtdelpart').val(this.partArr[0].par_id);
                this.partid = this.partArr[0].par_id;
            });
            this.action = 'participants/edit';
        },

        getValidate: function(e) {

            if(this.desc == ""){
                this.haserror.desc          = 'has-error';
                this.Error.desc             = 'This field is required.';
                this.ErrorValidation.desc   = true;
            }


            var partvalid = '';
            if(this.desc!=''){
                partvalid = this.desc;
                partvalid = (partvalid + '|').toLowerCase();
                if((this.validPart.split(partvalid).length - 1) >= 1){
                    this.haserror.desc = 'has-error';
                    if(this.action == 'participants/edit')
                        this.Error.desc = 'Nothing change, click cancel to clear.';
                    else
                        this.Error.desc = 'This data is already exists.';
                    this.ErrorValidation.desc = true;
                }else{
                    this.haserror.desc = '';
                    this.Error.desc = '';
                    this.ErrorValidation.desc = false;
                }
            }
            
            var cont = false; 
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true; 
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});



// professional licenses
new Vue({
    el: '#licenses',

    data: {
       newSpe: {
            code: '', description: ''
        },
        Error: {
            code: '', description: ''
        },
        ErrorValidation: {
            code: false, description: false
        },
        haserror: {
            code: '', description: ''
        },
        baseUrl: '',
        licenses:[],
        speid: '',
    },

    ready: function(){
        this.haserror.code = '';
        this.Error.code = '';

        this.haserror.description = '';
        this.Error.description = '';

        // fetch Desciption
        this.fetchDesc();
    },
    computed: {
        //Code
        validCode: function() {
            if(this.newSpe.code == ""){
                this.haserror.code = 'has-error';
                this.Error.code = 'This field is required.';
                this.ErrorValidation.code = true;
                return true;
            }else{
                if(!this.newSpe.code.replace(/\s/g, '').length){
                    this.haserror.code = 'has-error';
                    this.Error.code = 'Invalid input.';
                    this.ErrorValidation.code = true;
                    return true;
                }
            }
            this.ErrorValidation.code=false; this.Error.code=''; this.haserror.code = ''; return false;
        },

         //description
        validDesc: function() {
            if(this.newSpe.description == ""){
                this.haserror.description = 'has-error';
                this.Error.description = 'This field is required.';
                this.ErrorValidation.description = true;
                return true;
            }else if(this.licenses.indexOf(this.newSpe.description) >= 0){
                this.haserror.description = 'has-error';
                this.Error.description = 'This description already exists.';
                this.ErrorValidation.description = true;
                return true;
            }else{
                if(!this.newSpe.description.replace(/\s/g, '').length){
                    this.haserror.description = 'has-error';
                    this.Error.description = 'Invalid input.';
                    this.ErrorValidation.description = true;
                    return true;
                }
            }
            this.ErrorValidation.description=false; this.Error.description=''; this.haserror.description = ''; return false;
        },

    },

    methods: {
        //fetchDesc
        fetchDesc: function() {
            this.$http.get(this.baseUrl + 'licenses/fetchdesc/' + this.speid, function(licenses) {
                this.$set('licenses', licenses);
            });
        },

        getValidate: function(e) {
            this.submmited = true;
            // check code
            if(this.newSpe.code == ""){ 
                this.haserror.code = 'has-error';
                this.Error.code    = 'This field is required.';
                this.ErrorValidation.code = true;
            }

            // check description
            if(this.newSpe.description == ""){
                this.haserror.description = 'has-error';
                this.Error.description    = 'This field is required.';
                this.ErrorValidation.description = true;
            }
            
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }
    }
});

// institutions
new Vue({
    el: '#institutions',

    data: {
        institutions:  [],
        participantstype: [],
        validInst: [],
        insArr: {},

        fields: [],
        sid: '',
        insType: '',
        locRegion: '',
        code: '',
        desc: '',
        insid: '',
        haserror: {
            insType: '',
            locRegion: '',
            code: '',
            desc: '',
        },
        Error: {
            insType: '',
            locRegion: '',
            code: '',
            desc: '',
        },
        ErrorValidation: {
            insType:  false,
            locRegion:  false,
            code:   false,
            desc: false,
        },
        baseUrl: '',
        action: 'institutions/add',
        actionName: ''
    },

    ready: function() {
        this.fetchInst();
    },
    watch: {
        'insType': function(val) {
            if(val != ''){
                this.haserror.insType         = '';
                this.Error.insType            = '';
                this.ErrorValidation.insType  = false;
            }else{
                this.haserror.insType         = 'has-error';
                this.Error.insType            = 'This field is required.';
                this.ErrorValidation.insType  = true;
            }
        },

        'locRegion': function(val) {
            if(val != ''){
                this.haserror.locRegion         = '';
                this.Error.locRegion            = '';
                this.ErrorValidation.locRegion  = false;
            }else{
                this.haserror.locRegion         = 'has-error';
                this.Error.locRegion            = 'This field is required.';
                this.ErrorValidation.locRegion  = true;
            }
        },

        'code': function(val) {
            if(val == ''){
                this.haserror.code         = 'has-error';
                this.Error.code            = 'This field is required.';
                this.ErrorValidation.code  = true;
            }else{
                if(!this.code.replace(/\s/g, '').length){
                    this.haserror.code         = 'has-error';
                    this.Error.code            = 'Invalid input.';
                    this.ErrorValidation.code  = true;
                }else{
                    this.haserror.code         = '';
                    this.Error.code            = '';
                    this.ErrorValidation.code  = false;
                }
            }
        },

        'desc': function(val) {
            if(val == ''){
                this.haserror.desc         = 'has-error';
                this.Error.desc            = 'This field is required.';
                this.ErrorValidation.desc  = true;
            }else{
                if(!this.desc.replace(/\s/g, '').length){
                    this.haserror.desc         = 'has-error';
                    this.Error.desc            = 'Invalid input.';
                    this.ErrorValidation.desc  = true;
                }else{
                    this.haserror.desc         = '';
                    this.Error.desc            = '';
                    this.ErrorValidation.desc  = false;
                }
            }
        },

    },
    methods: {

        fetchInst: function() {
            this.$http.get(this.baseUrl + 'institutions/fetchInst' , function(institutions) {
                this.$set('institutions', institutions);
            });
            this.$http.get(this.baseUrl + 'institutions/fetchValidhostInst' , function(validInst) {
                this.$set('validInst', validInst);
            });
        },

        getRowValue: function(prof) {
            this.$http.get(this.baseUrl + 'institutions/getInst/' + prof, function(insArr) {
                this.$set('insArr', insArr);
                this.insid = this.insArr[0].ins_id;
                this.insType = this.insArr[0].ins_itype_id;
                this.locRegion = this.insArr[0].ins_loc_id;
                this.code = this.insArr[0].ins_code;
                this.desc = this.insArr[0].ins_desc;
                $('#target_id').val(this.insArr[0].ins_itype_id);
                this.titleid = this.insArr[0].ins_itype_id;
            });
            this.action = 'institutions/edit';
        },

        getValidate: function(e) {
            if(this.insType == ''){
                this.haserror.insType         = 'has-error';
                this.Error.insType            = 'This field is required.';
                this.ErrorValidation.insType  = true;
            }

            if(this.locRegion == ''){
                this.haserror.locRegion         = 'has-error';
                this.Error.locRegion            = 'This field is required.';
                this.ErrorValidation.locRegion  = true;
            }

            if(this.code == ''){
                this.haserror.code         = 'has-error';
                this.Error.code            = 'This field is required.';
                this.ErrorValidation.code  = true;
            }

            if(this.desc == ''){
                this.haserror.desc         = 'has-error';
                this.Error.desc            = 'This field is required.';
                this.ErrorValidation.desc  = true;
            }

            // var instvalid = '';
            // if(this.ErrorValidation.code != true && this.ErrorValidation.desc != true){
            //     instvalid = this.code + '-' + this.desc;
            //     instvalid = (instvalid + '|').toLowerCase();
            //     if((this.validInst.split(instvalid).length - 1) >= 1){
            //         this.haserror.desc           = 'has-error';
            //         if(this.action == 'institutions/edit')
            //             this.Error.code                 = 'Nothing change, click cancel to clear.';
            //         else
            //             this.Error.code                 = 'This data is already exists.';
            //         this.ErrorValidation.desc     = true;
            //         this.haserror.code                  = 'has-error';
            //         if(this.action == 'institutions/edit')
            //             this.Error.desc           = 'Nothing change, click cancel to clear.';
            //         else
            //             this.Error.desc           = 'This data is already exists.';
            //         this.ErrorValidation.code           = true;
            //     }else{
            //         this.haserror.desc            = '';
            //         this.Error.desc               = '';
            //         this.ErrorValidation.desc     = false;
            //         this.haserror.code                  = '';
            //         this.Error.code                     = '';
            //         this.ErrorValidation.code           = false;
            //     }
            // }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    }
});
