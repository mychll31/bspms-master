//tba
new Vue({
    el: '#eltba',
    data: {
        action: '',
        baseUrl: '',
        tbas: [],
        text: '',
        host: '',
        actionName: '',

        haserror: { text: '', host: '' },
        Error: { text: '', host: '' },
        ErrorValidation: { text: false, host: false },
    },

    ready: function() {
        this.getTba();
        this.getRowValue();
    },

    methods: {
        getTba: function() {
            this.$http.get(this.baseUrl + 'scientists/fetchTba/'+$('#sciid').val(), function(tbas) {
                this.$set('tbas', tbas);
            });
        },

        addtba: function(e) {
            this.action = 'add';
            this.text = '';
            this.host = '';
            this.actionName = 'scientists/addScientistTba/'+$('#sciid').val();
        },

        getRowValue: function(tba) {
            if(tba != undefined){
                this.action = 'edit';
                this.text = tba.tba_text;
                this.host = tba.tba_host_institution;
                this.actionName = 'scientists/editScientistTba/'+tba.tba_id+'/'+tba.tba_sci_id;
                $('#txttba').val(tba.tba_id);
            }
        },

        getValidateTba: function(e) {
            $('#descr').val($('#editor').html());
            if(this.host == '') {
                e.preventDefault();
                this.haserror.host = 'has-error';
                this.Error.host = 'This field is required.';
                this.ErrorValidation.host = true;
            }else{
                this.haserror.host = '';
                this.Error.host = '';
                this.ErrorValidation.host = false;
            }

            if($('#descr').val() == '') {
                e.preventDefault();
                this.haserror.text = 'border: 1px solid #dd4b39';
                this.Error.text = 'This field is required.';
                this.ErrorValidation.text = true;
            }else{
                this.haserror.text = '';
                this.Error.text = '';
                this.ErrorValidation.text = false;
            }

        },
    },

});

new Vue({
    el: '#srvContractDur',
    data: {
        messAppDy: 'Approved Number of Days / Years',
        approvalyear: '',
        txtapprovalyear: '',
        ctrTextbox: 2,
        typecontract: '',
        dateofApproval: '',
        changeSched: '',
        changeSchedApproval: '',
        country: '',
        awardType: '',
        approvedDY: '',
        council: '',
        status: '',
        witherp: '',
        outcomes: '',
        priority: '',
        phases: '',
        arrinst: [],
        defInstitution: '',
        erpdate: '',
        erpvenue: '',
        erpnopart: '',
        erpreason: '',
        yrrepatriated: '',
        yrcompleted: '',

        erp_error: '',
        condatesTotal: '',

        haserror: {
            dateofApproval: '',
            changeSched: '',
            changeSchedApproval: '',
            country: '',
            awardType: '',
            approvedDY: '',
            council: '',
            status: '',
            witherp: '',
            contractType: '',
            institution: '',
            outcomes: '',
            priority: '',
            phases: '',
            country: '',
            citizen: '',
            erpdate: '',
            erpvenue: '',
            erpnopart: '',
            erpreason: '',
            yrrepatriated: '',
            yrcompleted: '',
        },

        Error: {
            dateofApproval: '',
            changeSched: '',
            changeSchedApproval: '',
            country: '',
            awardType: '',
            approvedDY: '',
            council: '',
            status: '',
            witherp: '',
            contractType: '',
            institution: '',
            outcomes: '',
            priority: '',
            phases: '',
            country: '',
            citizen: '',
            erpdate: '',
            erpvenue: '',
            erpnopart: '',
            erpreason: '',
            yrrepatriated: '',
            yrcompleted: '',
        },

        ErrorValidation: {
            dateofApproval: false,
            changeSched: false,
            changeSchedApproval: false,
            country: false,
            awardType: false,
            approvedDY: false,
            council: false,
            status: false,
            witherp: false,
            contractType: false,
            institution: false,
            outcomes: false,
            priority: false,
            phases: false,
            country: true,
            citizen: true,
            erpdate: false,
            erpvenue: false,
            erpnopart: false,
            erpreason: false,
            yrrepatriated: false,
            yrcompleted: false,
        },
    },

    ready: function() {
        $('#btnaddphase').hide();

        for (var i = 2; i <= 10; i++) {
            $('#tblphase'+i).hide();
        }
        if(this.txtisyronly == 1){
            $('#divapryear').show();
            $('#divaprdate').hide();
        }else{
            $('#divapryear').hide();
            $('#divaprdate').show();
        }

        $('#cmbCountry').change(function () {
            if($(this).val()!=''){
                $('#cmbcountrydiv').removeClass('has-error');
                $('#cmbcountryspan').html('');
            }else{
                $('#cmbcountrydiv').addClass('has-error');
                $('#cmbcountryspan').show();
                $('#cmbcountryspan').html('This field is required.');
            }
        });

        $('#cmbcitizen').change(function () {
            if($(this).val()!=''){
                $('#cmbcitizendiv').removeClass('has-error');
                $('#cmbcitizenspan').html('');
            }else{
                $('#cmbcitizendiv').addClass('has-error');
                $('#cmbcitizenspan').show();
                $('#cmbcitizenspan').html('This field is required.');
            }
        });

        $("#txtyrrepatriated").on('keydown blur keyup', function(){
            if($("#txtyrrepatriated").val() != ''){
                $('#yrrepatriateddiv').removeClass('has-error');
                $('#yrrepatriatedspan').hide();
            }
            if($('#txtyrrepatriated').val() == 'YYYY'){
                $('#yrrepatriateddiv').addClass('has-error');
                $('#yrrepatriatedspan').html('This field is required.');
                $('#yrrepatriatedspan').show();
            }
        });
        $("#txtyrcompleted").on('keydown blur keyup', function(){
            if($("#txtyrcompleted").val() != ''){
                $('#yrcompleteddiv').removeClass('has-error');
                $('#yrcompletedspan').hide();
            }
            if($('#txtyrcompleted').val() == 'YYYY'){
                $('#yrcompleteddiv').addClass('has-error');
                $('#yrcompletedspan').html('This field is required.');
                $('#yrcompletedspan').show();
            }
        });

        $('#cmbInstitutions').on("change", function(e) {
            if($("#cmbInstitutions").val() != null){
                $('#cmbHostInstitutiondiv').removeClass('has-error');
                $('#spancmbHostInstitution').hide();
                $('#cmbHostInstitutiondiv span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
            }else{
                $('#cmbHostInstitutiondiv span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#cmbHostInstitutiondiv').addClass('has-error');
                $('#spancmbHostInstitution').html('This field is required.');
                $('#spancmbHostInstitution').show();
            }
        });

        $('#txtApprovalDate').on("change", function(e) {
            if($("#txtApprovalDate").val() != null && $("#txtApprovalDate").val() != ''){
                $('#txtApprovalDatediv').removeClass('has-error');
                $('#txtApprovalDatespan').html('');
                $('#txtApprovalDatespan').hide();
                $('#txtApprovalDatediv span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
            }else{
                $('#txtApprovalDatediv span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#txtApprovalDatediv').addClass('has-error');
                $('#txtApprovalDatespan').html('This field is required.');
                $('#txtApprovalDatespan').show();
            }
        });

        $('#txtapprovalyear').on("change", function(e) {
            if($("#txtapprovalyear").val() != null && $("#txtapprovalyear").val() != ''){
                $('#txtApprovalDatediv').removeClass('has-error');
                $('#txtApprovalDatespan').html('');
                $('#txtApprovalDatespan').hide();
                $('#txtApprovalDatediv span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
            }else{
                $('#txtApprovalDatediv span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#txtApprovalDatediv').addClass('has-error');
                $('#txtApprovalDatespan').html('This field is required.');
                $('#txtApprovalDatespan').show();
            }
        });

    },

    watch: {
        'typecontract': function(val){
            if(val != ''){
                if(val==1){
                    $('#btnaddphase').show();
                }else{
                    for (var i = this.ctrTextbox; 0 < i; i--)
                        $("#txtBoxDiv" + i).remove();
                    $('#btnaddphase').hide();
                }
                this.haserror.contractType         = '';
                this.Error.contractType            = '';
                this.ErrorValidation.contractType  = false;
            }else{
                this.haserror.contractType         = 'has-error';
                this.Error.contractType            = 'This field is required.';
                this.ErrorValidation.contractType  = true;
            }
        },
        // Classification Date of Approval
        'dateofApproval': function(val){
            if(val != ''){
                var matches = this.dateofApproval.match(/^\d{4}-\d{2}-\d{2}$/g);
                if(matches == null){
                    this.haserror.dateofApproval         = 'has-error';
                    this.Error.dateofApproval            = 'Invalid Date.';
                    this.ErrorValidation.dateofApproval  = true;
                }else{
                    this.haserror.dateofApproval         = '';
                    this.Error.dateofApproval            = '';
                    this.ErrorValidation.dateofApproval  = false;
                }
            }else{
                this.haserror.dateofApproval         = 'has-error';
                this.Error.dateofApproval            = 'This field is required.';
                this.ErrorValidation.dateofApproval  = true;
            }
        },
        // Country of Origin
        'country': function(val){
          if(val != ''){
                this.haserror.country         = '';
                this.Error.country            = '';
                this.ErrorValidation.country  = false;
            }else{
                this.haserror.country         = 'has-error';
                this.Error.country            = 'This field is required.';
                this.ErrorValidation.country  = true;
            }
        },
        // Type of Award
        'awardType': function(val){
          if(val != ''){
                if(val == 0){
                    this.messAppDy = 'Approved Number of Days'
                }else{
                    this.messAppDy = 'Approved Number of Years'
                }
                this.haserror.awardType         = '';
                this.Error.awardType            = '';
                this.ErrorValidation.awardType  = false;
            }else{
                this.haserror.awardType         = 'has-error';
                this.Error.awardType            = 'This field is required.';
                this.ErrorValidation.awardType  = true;
            }
        },
        // Council
        'council': function(val){
          if(val != ''){
                this.haserror.council         = '';
                this.Error.council            = '';
                this.ErrorValidation.council  = false;
            }else{
                this.haserror.council         = 'has-error';
                this.Error.council            = 'This field is required.';
                this.ErrorValidation.council  = true;
            }
        },
        // status
        'status': function(val){
          if(val != ''){
                this.haserror.status         = '';
                this.Error.status            = '';
                this.ErrorValidation.status  = false;
            }else{
                this.haserror.status         = 'has-error';
                this.Error.status            = 'This field is required.';
                this.ErrorValidation.status  = true;
            }
        },
        // // witherp
        // 'witherp': function(val){
        //     if(val != ''){
        //         $('#erpmodal').modal('show');
        //     }
        // },
        // Approved Number of Days / Years
        // 'approvedDY': function(val){
        //   if(val != ''){
        //         var matches = val.match(/^\d+$/g);
        //         if(matches == null){
        //             this.haserror.approvedDY         = 'has-error';
        //             this.Error.approvedDY            = 'Invalid input.';
        //             this.ErrorValidation.approvedDY  = true;
        //         }else{
        //             this.haserror.approvedDY         = '';
        //             this.Error.approvedDY            = '';
        //             this.ErrorValidation.approvedDY  = false;
        //         }
        //     }else{
        //         this.haserror.approvedDY         = 'has-error';
        //         this.Error.approvedDY            = 'This field is required.';
        //         this.ErrorValidation.approvedDY  = true;
        //     }
        // },
        // DOST Outcomes
        'outcomes': function(val){
            if(val != ''){
                this.haserror.outcomes         = '';
                this.Error.outcomes            = '';
                this.ErrorValidation.outcomes  = false;
            }else{
                this.haserror.outcomes         = 'has-error';
                this.Error.outcomes            = 'This field is required.';
                this.ErrorValidation.outcomes  = true;
            }
        },
        // DOST Priority Areas
        'priority': function(val){
          if(val != ''){
                this.haserror.priority         = '';
                this.Error.priority            = '';
                this.ErrorValidation.priority  = false;
            }else{
                this.haserror.priority         = 'has-error';
                this.Error.priority            = 'This field is required.';
                this.ErrorValidation.priority  = true;
            }
        },
    },
    methods: {
        savewitherp: function(e){
            if(this.witherp=='yes'){
                this.erpdate = $('#txterpdate').val();
                if(this.erpdate == ''){
                    this.haserror.erpdate         = 'has-error';
                    this.Error.erpdate            = 'This field is required.';
                    this.ErrorValidation.erpdate  = true;
                }else{
                    this.haserror.erpdate         = '';
                    this.Error.erpdate            = '';
                    this.ErrorValidation.erpdate  = false;
                }
                if(this.erpvenue == ''){
                    this.haserror.erpvenue         = 'has-error';
                    this.Error.erpvenue            = 'This field is required.';
                    this.ErrorValidation.erpvenue  = true;
                }else{
                    this.haserror.erpvenue         = '';
                    this.Error.erpvenue            = '';
                    this.ErrorValidation.erpvenue  = false;
                }
                if(this.erpnopart == ''){
                    this.haserror.erpnopart         = 'has-error';
                    this.Error.erpnopart            = 'This field is required.';
                    this.ErrorValidation.erpnopart  = true;
                }else{
                    this.haserror.erpnopart         = '';
                    this.Error.erpnopart            = '';
                    this.ErrorValidation.erpnopart  = false;
                }
            }
            if(this.witherp=='no'){
                if(this.erpreason == ''){
                    this.haserror.erpreason         = 'has-error';
                    this.Error.erpreason            = 'This field is required.';
                    this.ErrorValidation.erpreason  = true;
                }else{
                    this.haserror.erpreason         = '';
                    this.Error.erpreason            = '';
                    this.ErrorValidation.erpreason  = false;
                }
            }
            if(this.ErrorValidation.erpdate==false && this.ErrorValidation.erpvenue==false && this.ErrorValidation.erpnopart==false && this.ErrorValidation.erpreason==false){
                // $('#erpmodal').modal('hide');
                this.haserror.witherp = '';
                this.Error.witherp = '';
                this.ErrorValidation.witherp = false;
                this.erp_error = '';
            }

        },
        closeerp: function(e){
            this.witherp = '';
        },

        getvalidate: function(e){
            this.dateofApproval = $('#txtApprovalDate').val();
            var phasesInput = "";
            $("#phasesContainer :input").each(function () {
                phasesInput += $(this).val() + ';';
            });
            this.phases = phasesInput;
            var matches_phase = this.phases.match(/^[;]+$/g);
            if(matches_phase != null){
                this.haserror.phases        = 'has-error';
                this.Error.phases           = 'This field is required.';
                this.ErrorValidation.phases = true;
            }else{
                this.haserror.phases        = '';
                this.Error.phases           = '';
                this.ErrorValidation.phases = false;
            }

            if(this.approvalyear){
                if($('#txtapprovalyear').val()==''){
                    this.haserror.dateofApproval         = 'has-error';
                    $('#txtApprovalDatespan').html('This field is required.');
                    $('txtApprovalDatespan').show();
                    this.ErrorValidation.dateofApproval  = true;
                }else{
                    $('txtApprovalDatespan').hide();
                }
                this.dateofApproval = $('#txtapprovalyear').val();
            }else{
                if($('#txtApprovalDate').val()==''){
                    this.haserror.dateofApproval         = 'has-error';
                    $('#txtApprovalDatespan').html('This field is required.');
                    $('txtApprovalDatespan').show();
                    this.ErrorValidation.dateofApproval  = true;
                }else{
                    $('txtApprovalDatespan').hide();
                }
                this.dateofApproval = $('#txtApprovalDate').val();
            }

            if($('#cmbCountry').val() == ''){
                this.haserror.country         = 'has-error';
                this.Error.country            = 'This field is required.';
                this.ErrorValidation.country  = true;
            }else{
                this.ErrorValidation.country  = false;
            }

            if($('#cmbcitizen').val() == ''){
                this.haserror.citizen         = 'has-error';
                this.Error.citizen            = 'This field is required.';
                this.ErrorValidation.citizen  = true;
            }else{
                this.ErrorValidation.citizen  = false;
            }

            if($('#cmbHostInstitution').val() == ''){
                this.haserror.institution         = 'has-error';
                this.Error.institution            = 'This field is required.';
                this.ErrorValidation.institution  = true;
            }else{
                this.ErrorValidation.institution  = false;
            }

            if(this.awardType == ''){
                this.haserror.awardType         = 'has-error';
                this.Error.awardType            = 'This field is required.';
                this.ErrorValidation.awardType  = true;
            }

            if(this.council == ''){
                this.haserror.council         = 'has-error';
                this.Error.council            = 'This field is required.';
                this.ErrorValidation.council  = true;
            }

            if(this.status == ''){
                this.haserror.status         = 'has-error';
                this.Error.status            = 'This field is required.';
                this.ErrorValidation.status  = true;
            }

            if(this.typecontract == ''){
                this.haserror.contractType         = 'has-error';
                this.Error.contractType            = 'This field is required.';
                this.ErrorValidation.contractType  = true;
            }

            // if(this.approvedDY == ''){
            //     this.haserror.approvedDY         = 'has-error';
            //     this.Error.approvedDY            = 'This field is required.';
            //     this.ErrorValidation.approvedDY  = true;
            // }

            if(this.outcomes == ''){
                this.haserror.outcomes         = 'has-error';
                this.Error.outcomes            = 'This field is required.';
                this.ErrorValidation.outcomes  = true;
            }

            if(this.priority == ''){
                this.haserror.priority         = 'has-error';
                this.Error.priority            = 'This field is required.';
                this.ErrorValidation.priority  = true;
            }

            // repatriated
            if(this.status==6){
                this.yrrepatriated = $('#txtyrrepatriated').val();
                if(this.yrrepatriated == ''){
                    this.haserror.yrrepatriated         = 'has-error';
                    this.Error.yrrepatriated            = 'This field is required.';
                    this.ErrorValidation.yrrepatriated  = true;
                }else{
                    this.haserror.yrrepatriated         = '';
                    this.Error.yrrepatriated            = '';
                    this.ErrorValidation.yrrepatriated  = false;
                }
            }
            
            // completed
            if(this.status==4){
                this.yrcompleted = $('#txtyrcompleted').val();
                if(this.yrcompleted == ''){
                    this.haserror.yrcompleted         = 'has-error';
                    this.Error.yrcompleted            = 'This field is required.';
                    this.ErrorValidation.yrcompleted  = true;
                }else{
                    this.haserror.yrcompleted         = '';
                    this.Error.yrcompleted            = '';
                    this.ErrorValidation.yrcompleted  = false;
                }   
                if(this.witherp==''){
                    this.haserror.witherp         = 'has-error';
                    this.Error.witherp            = 'This field is required.';
                    this.ErrorValidation.witherp  = true;
                    this.erp_error                = 'erp_error';
                }else{
                    this.haserror.witherp         = '';
                    this.Error.witherp            = '';
                    this.ErrorValidation.witherp  = false;
                    this.erp_error                = '';
                }
            }

            if($("#cmbInstitutions").val() != null){
                $('#cmbHostInstitutiondiv').removeClass('has-error');
                $('#spancmbHostInstitution').hide();
                $('#cmbHostInstitutiondiv span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
                this.ErrorValidation.institution = false;
            }else{
                this.ErrorValidation.institution = true;
                $('#cmbHostInstitutiondiv span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#cmbHostInstitutiondiv').addClass('has-error');
                $('#spancmbHostInstitution').html('This field is required.');
                $('#spancmbHostInstitution').show();
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
                console.log(key+' = '+this.ErrorValidation[key]);
            }
            console.log('cont? '+cont);

            if(cont){
                if(this.dateofApproval != ''){
                    $('#contentmodal').html('Information Incomplete. Do you want to continue?');
                    $('#savesrvinfo').modal('show');
                }else{
                    $('#notifsavesrv').modal('show');
                    e.preventDefault();
                }
            }else{
                $('#btndupdate').hide();
                if($('#modalaction').val()=='editServiceAward')
                    $('#contentmodal').html('Update Service Award.');
                else
                    $('#contentmodal').html('Save new service award.');
                $('#savesrvinfo').modal('show');
            }

        },
    }
});