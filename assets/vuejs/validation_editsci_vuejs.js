// add profile
new Vue({
    el: '#sciProfileEdit',

    data: {
       newProfile: {
            title: '',
            fname: '', lname: '', mname: '',
            bday: '',
            gender: '',
            status: '',
            license: '', expertise: '',
            contactno: '',
            email: '',
            postal: '',
        },
        other: {
            minitial: '',
            extname: '',
        },
        haserrorfname: '',
        haserrorlname: '',
        allerrors: false,
        Error: {
            title: '',
            fname: '', lname: '', mname: '', extname: '',
            bday: '',
            gender: '',
            status: '',
            license: '', expertise: '', profession: '',
            contactno: '',
            email: '',
            postal: '', mini:'',
            validGender: false, validStatus: false, validExpertise: false, validspecialization: false, validprofession: false,
        },
        haserror: {
            title: '',
            fname: '', lname: '', mname: '', extname: '',
            bday: '',
            gender: '',
            status: '',
            license: '', expertise: '', specialization: '', profession: '',
            contactno: '',
            email: '', minitial:'',
            postal: '',
        },
        emailaddress: [],
        baseUrl: '',
    },
    ready: function(){
        this.haserror.title = '';
        this.Error.title = '';

        this.haserrorfname = '';
        this.Error.fname = '';

        this.haserrorlname = '';
        this.Error.lname = '';

        this.haserror.bday = '';
        this.Error.bday = '';

        this.haserror.gender = '';
        this.Error.gender = '';

        this.haserror.status = '';
        this.Error.status = '';

        this.haserror.license = '';
        this.Error.license = '';

        this.haserror.contactno = '';
        this.Error.contactno = '';

        this.haserror.email = '';
        this.Error.email = '';

        this.haserror.postal = '';
        this.Error.postal = '';

        //fetch email address for checking
        this.fetchEmailaddress();
    },
    computed: {
        errors: function() {
            for (var key in this.newProfile) {
                if ( ! this.newProfile[key]) return true;
            }
            return false;
        },
        //Firstname
        validTitle: function() {
            if(this.newProfile.title == ""){
                this.haserror.title = 'has-error'; this.allerrors = true;
                this.Error.title = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.title = ''; return false;
        },
        //Firstname
        validFname: function() {
            // Numeric and special character is not allowed
            var matches = this.newProfile.fname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
            if(this.newProfile.fname != ""){
                if(matches != null){
                    this.haserrorfname = 'has-error'; this.allerrors = true;
                    this.Error.fname = 'Please use only alphabet characters and spaces for First Name.';
                    return true;
                }
            }else{
                this.haserrorfname = 'has-error'; this.allerrors = true;
                this.Error.fname = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserrorfname = ''; return false;
        },
        //Lastname
        validLname: function() {
            var matches = this.newProfile.lname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
            if(this.newProfile.lname != ""){
                if(matches != null){
                    this.haserrorlname = 'has-error'; this.allerrors = true;
                    this.Error.lname = 'Please use only alphabet characters and spaces for Last Name.';
                    return true;
                }
            }else{
                this.haserrorlname = 'has-error'; this.allerrors = true;
                this.Error.lname = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserrorlname = ''; return false;
        },
        //MiddleInitial
        validMini: function() {
            // Numeric and special character is not allowed
            var matches = this.other.minitial.match(/[0-9!@#\$%\^\&*\)\(+=._ ]+$/g);
            if(this.other.minitial != ""){
                if(matches != null){
                    this.haserror.minitial = 'has-error'; this.allerrors = true;
                    this.Error.minitial = 'Please use only alphabet for Middle Initial.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.minitial = ''; return false;
        },
        //Extension
        validExt: function() {
            // Numeric and special character is not allowed
            var matches = this.other.extname.match(/[0-9!@#\$%\^\&*\)\(+=._ ]+$/g);
            if(this.other.extname != ""){
                if(matches != null){
                    this.haserror.extname = 'has-error'; this.allerrors = true;
                    this.Error.extname = 'Please use only alphabet  characters for Extension Name.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.extname = ''; return false;
        },
        //Middlename
        validMname: function() {
            // Numeric and special character is not allowed
            var matches = this.newProfile.mname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
            if(this.newProfile.mname != ""){
                if(matches != null){
                    this.haserror.mname = 'has-error'; this.allerrors = true;
                    this.Error.mname = 'Please use only alphabet characters and spaces for Middle Name.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.mname = ''; return false;
        },
        //birthday
        validBday: function() {
            var matches = this.newProfile.bday.match(/^\d{4}-\d{2}-\d{2}$/g);
            if(this.newProfile.bday == ""){
                this.haserror.bday = 'has-error'; this.allerrors = true;
                this.Error.bday = 'This field is required.';
                return true;
            }else{
                if(matches == null || this.newProfile.bday == "0000-00-00"){
                    this.haserror.bday = 'has-error'; this.allerrors = true;
                    this.Error.bday = 'Invalid Birthday.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.bday = ''; return false;
        },
        //Gender
        valGender: function() {
            if(this.newProfile.gender == ""){
                this.haserror.gender = 'has-error'; this.allerrors = true;
                this.Error.gender = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.gender = ''; return false;
        },

        //License
        valLicense: function() {
            if(this.newProfile.license == ""){
                this.haserror.license = 'has-error'; this.allerrors = true;
                this.Error.license = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.license = ''; return false;
        },

        //Status
        valStatus: function() {
            if(this.newProfile.status == ""){
                this.haserror.status = 'has-error'; this.allerrors = true;
                this.Error.status = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.status = ''; return false;
        },
        //contactno
        validContactno: function() {
            // alphanumeric and special character is not allowed
            var matches = this.newProfile.contactno.match(/[A-Za-z!@#\$%\^\&*\)\(+=._]+$/g);
            if(matches != null){
                this.haserror.contactno = 'has-error'; this.allerrors = true;
                this.Error.contactno = 'Invalid contact number.';
                return true;
            }else{
                if(this.newProfile.contactno == ""){
                    this.haserror.contactno = 'has-error'; this.allerrors = true;
                    this.Error.contactno = 'This field is required.';
                    return true;
                }else if(this.newProfile.contactno.length < 7){
                    this.haserror.contactno = 'has-error'; this.allerrors = true;
                    this.Error.contactno = 'Invalid input.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.contactno = ''; return false;
        },
        //email
        validEmail: function() {
            // Email address format
            var matches = this.newProfile.email.match(/[A-Za-z0-9._]@[A-Za-z0-9._]{1,}$/g);
            if(this.newProfile.email == ""){
                this.haserror.email = 'has-error'; this.allerrors = true;
                this.Error.email = 'This field is required.';
                return true;
            }else{
                if(matches == null){
                    this.haserror.email = 'has-error'; this.allerrors = true;
                    this.Error.email = 'Invalid email address.';
                    return true;
                }else{
                    if(this.emailaddress.indexOf(this.newProfile.email) > 1){
                        this.haserror.email = 'has-error'; this.allerrors = true;
                        this.Error.email = 'Email address exists.';
                        return true;
                    }
                }
            }
            this.allerrors = false; this.haserror.email = ''; return false;
        },
        //postal
        validPostal: function() {
            if(this.newProfile.postal == ""){
                this.haserror.postal = 'has-error'; this.allerrors = true;
                this.Error.postal = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.postal = ''; return false;
        },
        //profLicense
        validProfLicense: function() {
            if(this.newProfile.license == ""){
                this.haserror.license = 'has-error'; this.allerrors = true;
                this.Error.license = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.license = ''; return false;
        },
    },

    methods: {
        fetchEmailaddress: function() {
            this.$http.get(this.baseUrl + 'scientists/fetchEmailaddress', function(emailaddress) {
                this.$set('emailaddress', emailaddress);
            });
        },
        getValidate: function(e) {
            submitted = true;
            var totalError = 0;
            // check title
            if(this.newProfile.title == ""){
                this.haserror.title = 'has-error';
                this.Error.title    = 'This field is required.';
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.title = '';
                this.Error.title    = '';
            }

            // check firstname
            if(this.newProfile.fname == ""){
                this.haserrorfname = 'has-error';
                this.haserror.fname = 'has-error';
                this.Error.fname    = 'This field is required.';
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserrorfname = '';
                this.haserror.fname = '';
                this.Error.fname    = '';
            }

            // check lastname
            if(this.newProfile.lname == ""){
                this.haserrorlname = 'has-error';
                this.haserror.lname = 'has-error';
                this.Error.lname    = 'This field is required.';
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserrorlname = '';
                this.haserror.lname = '';
                this.Error.lname    = '';
            }

            //check dateofbirth
            if(this.newProfile.bday == ""){
                this.haserror.bday = 'has-error';
                this.Error.bday    = 'This field is required.';
                $('#spanbday').hide();
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.bday = '';
                this.Error.bday    = '';
            }

            //check gender
            if(this.newProfile.gender == ""){
                this.haserror.gender = 'has-error';
                this.Error.gender    = 'This field is required.';
                this.Error.validGender= true;
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.gender   = '';
                this.Error.gender      = '';
                this.Error.validGender = false;
            }

            //check civilstatus
            if(this.newProfile.status == ""){
                this.haserror.status   = 'has-error';
                this.Error.status      = 'This field is required.';
                this.Error.validStatus = true;
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.status   = '';
                this.Error.status      = '';
                this.Error.validStatus = false;
            }

            //check expertise
            if($('#cmbAreaOfExpertise').val() == null){
                this.haserror.expertise   = 'has-error';
                this.Error.expertise      = 'This field is required.';
                this.Error.validExpertise = true;
                $('#expertiseClass span.select2-selection.select2-selection').css('border', '1px solid #a94442');
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.expertise   = '';
                this.Error.expertise      = '';
                $('#expertiseClass span.select2-selection.select2-selection').css('border', '1px solid rgb(204, 204, 204)');
                this.Error.validExpertise = false;
            }

            //check specialization
            if($('#cmbSpecialization').val() == null){
                this.haserror.specialization   = 'has-error';
                this.Error.specialization      = 'This field is required.';
                this.Error.validspecialization = true;
                $('#specializationClass span.select2-selection.select2-selection').css('border', '1px solid #a94442');
                this.Error.validspecialization = true;
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.specialization   = '';
                this.Error.specialization      = '';
                $('#specializationClass span.select2-selection.select2-selection').css('border', '1px solid rgb(204, 204, 204)');
                this.Error.validspecialization = false;
            }

            //check profession
            if($('#cmbProfession').val() == ""){
                this.haserror.profession   = 'has-error';
                this.Error.profession      = 'This field is required.';
                this.Error.validprofession = true;
                $('#professionClass span.select2-selection.select2-selection').css('border', '1px solid #a94442');
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.profession   = '';
                this.Error.profession      = '';
                $('#professionClass span.select2-selection.select2-selection').css('border', '1px solid rgb(204, 204, 204)');
                this.Error.validprofession = false;
            }

            //check License
            if(this.newProfile.license == ""){
                this.haserror.license   = 'has-error';
                this.Error.license      = 'This field is required.';
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.license   = '';
                this.Error.license      = '';
            }

            //check Contactno
            if(this.newProfile.contactno == ""){
                this.haserror.contactno   = 'has-error';
                this.Error.contactno      = 'This field is required.';
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.contactno   = '';
                this.Error.contactno      = '';
            }

            //check email
            if(this.newProfile.email == ""){
                this.haserror.email   = 'has-error';
                this.Error.email      = 'This field is required.';
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.email   = '';
                this.Error.email      = '';
            }

            //check postal
            if(this.newProfile.postal == ""){
                this.haserror.postal   = 'has-error';
                this.Error.postal      = 'This field is required.';
                this.allerrors = true;
            }else{
                this.allerrors = false;
                this.haserror.postal   = '';
                this.Error.postal      = '';
            }

            
            //prevent default when there is errors
            // e.preventDefault();
            if(this.allerrors){ e.preventDefault(); }
        }
    }
});

// add education
new Vue({
    el: '#sciEducation',

    data: {
        newEducation: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },
        haserror: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },
        Error: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },

        ErrorValidation: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },
    },
    ready: function(){
        this.haserror.level = '';
        this.Error.level = '';

        this.haserror.school = '';
        this.Error.school = '';

        this.haserror.country = '';
        this.Error.country = '';

        this.haserror.course = '';
        this.Error.course = '';

        this.haserror.yrgraduate = '';
        this.Error.yrgraduate = '';
    },
    computed: {
        //level
        validLevel: function() {
            if(this.newEducation.level == ""){
                this.haserror.level = 'has-error'; this.ErrorValidation.level = true;
                this.Error.level = 'This field is required.';
                return true;
            }
            this.ErrorValidation.level = false;
            this.Error.level = ''; this.haserror.level = ''; return false;
        },

        //school
        validSchool: function() {
            if(this.newEducation.school == ""){
                this.haserror.school = 'has-error'; this.ErrorValidation.school = true;
                this.Error.school = 'This field is required.';
                return true;
            }
            this.ErrorValidation.school = false;
            this.Error.school = ''; this.haserror.school = ''; return false;
        },

        //country
        validCountry: function() {
            if(this.newEducation.country == ""){
                this.haserror.country = 'has-error'; this.ErrorValidation.country = true;
                this.Error.country = 'This field is required.';
                return true;
            }
            this.ErrorValidation.country = false;
            this.Error.country = ''; this.haserror.country = ''; return false;
        },

        //course
        validCourse: function() {
            if(this.newEducation.course == ""){
                this.haserror.course = 'has-error'; this.ErrorValidation.course = true;
                this.Error.course = 'This field is required.';
                return true;
            }
            this.ErrorValidation.course = false;
            this.Error.course = ''; this.haserror.course = ''; return false;
        },

        //yrgraduate
        validYrgraduate: function() {
            if(this.newEducation.yrgraduate == ""){
                this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                this.Error.yrgraduate = 'This field is required.';
                return true;
            }else{
                if(this.newEducation.yrgraduate == " " || (!this.newEducation.yrgraduate.replace(/\s/g, '').length)){
                    this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                    this.Error.yrgraduate = 'Invalid input.';
                    return true;
                }else{
                    var matches = this.newEducation.yrgraduate.match(/[A-Za-z@#\$%\^\&*\)\(+=._]+$/g);
                    if(matches != null){
                        this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                        this.Error.yrgraduate = 'Invalid input.';
                        return true;
                    }else if(this.newEducation.yrgraduate < 1960){
                        this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                        this.Error.yrgraduate = 'Invalid year.';
                        return true;
                    }
                }
            }
            this.ErrorValidation.yrgraduate = false;
            this.Error.yrgraduate = ''; this.haserror.yrgraduate = ''; return false;
        },
    },
    methods: {
        getvalidate: function(e) {
            if(this.newEducation.level == ""){
                this.haserror.level = 'has-error'; this.Error.level = "This field is required";
                this.ErrorValidation.level = true
            }

            if(this.newEducation.school == ""){
                this.haserror.school = 'has-error'; this.Error.school = "This field is required";
                this.ErrorValidation.school = true
            }

            if(this.newEducation.country == ""){
                this.haserror.country = 'has-error'; this.Error.country = "This field is required";
                this.ErrorValidation.country = true
            }

            if(this.newEducation.course == ""){
                this.haserror.course = 'has-error'; this.Error.course = "This field is required";
                this.ErrorValidation.course = true
            }

            if(this.newEducation.yrgraduate == ""){
                this.haserror.yrgraduate = 'has-error'; this.Error.yrgraduate = "This field is required";
                this.ErrorValidation.yrgraduate = true
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
                console.log('dont continue');
            }
        },
    },
});

// add employment
new Vue({
    el: '#sciEmployment',

    data: {
        newEmployment: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },
        haserror: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },
        Error: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },

        ErrorValidation: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },
    },
    ready: function(){
        this.haserror.company = '';
        this.Error.company = '';

        this.haserror.position = '';
        this.Error.position = '';

        this.haserror.datefrom = '';
        this.Error.datefrom = '';

        this.haserror.dateto = '';
        this.Error.dateto = '';

        this.haserror.country = '';
        this.Error.country = '';
    },
    computed: {
        //Company
        validCompany: function() {
            if(this.newEmployment.company == ""){
                this.haserror.company = 'has-error'; this.ErrorValidation.company = true;
                this.Error.company = 'This field is required.';
                return true;
            }
            this.ErrorValidation.company = false;
            this.Error.company = ''; this.haserror.company = ''; return false;
        },

        //Position
        validPosition: function() {
            if(this.newEmployment.position == ""){
                this.haserror.position = 'has-error'; this.ErrorValidation.position = true;
                this.Error.position = 'This field is required.';
                return true;
            }
            this.ErrorValidation.position = false;
            this.Error.position = ''; this.haserror.position = ''; return false;
        },

        //dateFrom
        validDateFrom: function() {
            var matches = this.newEmployment.datefrom.match(/^\d{4}-\d{2}-\d{2}$/g);
            if(this.newEmployment.datefrom == ""){
                this.haserror.datefrom = 'has-error'; this.allerrors = true;
                this.Error.datefrom = 'This field is required.';
                return true;
            }else{
                if(matches == null || this.newEmployment.datefrom == "0000-00-00"){
                    this.haserror.datefrom = 'has-error'; this.allerrors = true;
                    this.Error.datefrom = 'Invalid date.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.datefrom = ''; return false;
        },

        //dateTo
        validDateTo: function() {
            this.newEmployment.dateto = $('#txtDateTo').val();
            var matches = this.newEmployment.dateto.match(/^\d{4}-\d{2}-\d{2}$/g);
            if(this.newEmployment.dateto == ""){
                this.haserror.dateto = 'has-error'; this.allerrors = true;
                this.Error.dateto = 'This field is required.';
                return true;
            }else{
                if(matches == null || this.newEmployment.dateto == "0000-00-00"){
                    this.haserror.dateto = 'has-error'; this.allerrors = true;
                    this.Error.dateto = 'Invalid date.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.dateto = ''; return false;
        },
        //country
        validCountry: function() {
            if(this.newEmployment.country == ""){
                this.haserror.country = 'has-error'; this.ErrorValidation.country = true;
                this.Error.country = 'This field is required.';
                return true;
            }
            this.ErrorValidation.country = false;
            this.Error.country = ''; this.haserror.country = ''; return false;
        },
    },
    methods: {
        getvalidate: function(e) {
            if(this.newEmployment.company == ""){
                this.haserror.company = 'has-error'; this.Error.company = "This field is required";
                this.ErrorValidation.company = true
            }

            if(this.newEmployment.position == ""){
                this.haserror.position = 'has-error'; this.Error.position = "This field is required";
                this.ErrorValidation.position = true
            }

            if($('#txtDateFrom').val() == ""){
                this.haserror.datefrom = 'has-error'; this.Error.datefrom = "This field is required";
                this.ErrorValidation.datefrom = true
            }

            if($('#txtDateTo').val() == ""){
                this.haserror.dateto = 'has-error'; this.Error.dateto = "This field is required";
                this.ErrorValidation.dateto = true
            }

            if(this.newEmployment.country == ""){
                this.haserror.country = 'has-error'; this.Error.country = "This field is required";
                this.ErrorValidation.country = true
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    },
});