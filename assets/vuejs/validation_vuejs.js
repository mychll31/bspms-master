new Vue({
    el: '#sciProfile',

    data: {
       newProfile: {
            title: '',
            fname: '', lname: '', mname: '',
            bday: '',
            gender: '',
            status: '',
            license: '', expertise: '',
            contactno: '',
            email: '',
            postal: '',
        },
        haserrorfname: '',
        haserrorlname: '',
        allerrors: false,
        Error: {
            title: '',
            fname: '', lname: '', mname: '',
            bday: '',
            gender: '',
            status: '',
            license: '', expertise: '', profession: '',
            contactno: '',
            email: '',
            postal: '',
            validGender: false, validStatus: false, validExpertise: false, validspecialization: false, validprofession: false,
        },
        haserror: {
            title: '',
            fname: '', lname: '', mname: '',
            bday: '',
            gender: '',
            status: '',
            license: '', expertise: '', specialization: '', profession: '',
            contactno: '',
            email: '',
            postal: '',
        },
        baseUrl: '',
    },
    ready: function() {

    },
    computed: {
        errors: function() {
            for (var key in this.newProfile) {
                if ( ! this.newProfile[key]) return true;
            }
            return false;
        },
        //Firstname
        validFname: function() {
            // Numeric and special character is not allowed
            var matches = this.newProfile.fname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
            if(matches != null){
                this.haserrorfname = 'has-error'; this.allerrors = true;
                this.Error.fname = 'Please use only alphabet characters and spaces for firstname.';
                return true;
            }
            this.allerrors = false; this.haserrorfname = ''; return false;
        },
        //Lastname
        validLname: function() {
            var matches = this.newProfile.lname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
            if(matches != null){
                this.haserrorlname = 'has-error'; this.allerrors = true;
                this.Error.lname = 'Please use only alphabet characters and spaces for lastname.';
                return true;
            }
            this.allerrors = false; this.haserrorlname = ''; return false;
        },
        //Middlename
        validMname: function() {
            // Numeric and special character is not allowed
            var matches = this.newProfile.mname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
            if(this.newProfile.mname != ""){
                if(matches != null){
                    this.haserror.mname = 'has-error'; this.allerrors = true;
                    this.Error.mname = 'Please use only alphabet characters and spaces for middlename.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.mname = ''; return false;
        },
        //birthday
        validBday: function() {
            var matches = this.newProfile.bday.match(/^\d{4}-\d{2}-\d{2}$/g);
            if(this.newProfile.bday == ""){
                this.haserror.bday = 'has-error'; this.allerrors = true;
                this.Error.bday = 'This field is required.';
                return true;
            }else{
                if(matches == null || this.newProfile.bday == "0000-00-00"){
                    this.haserror.bday = 'has-error'; this.allerrors = true;
                    this.Error.bday = 'Invalid birthday.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.bday = ''; return false;
        },
        //contactno
        validContactno: function() {
            // alphanumeric and special character is not allowed
            var matches = this.newProfile.contactno.match(/[A-Za-z!@#\$%\^\&*\)\(+=._]+$/g);
            if(matches != null){
                this.haserror.contactno = 'has-error'; this.allerrors = true;
                this.Error.contactno = 'Invalid contact number.';
                return true;
            }else{
                if(this.newProfile.contactno == ""){
                    this.haserror.contactno = 'has-error'; this.allerrors = true;
                    this.Error.contactno = 'This field is required.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.contactno = ''; return false;
        },
        //email
        validEmail: function() {
            // Email address format
            var matches = this.newProfile.email.match(/[A-Za-z0-9._]@[A-Za-z0-9._]{1,}$/g);
            if(this.newProfile.email == ""){
                this.haserror.email = 'has-error'; this.allerrors = true;
                this.Error.email = 'This field is required.';
                return true;
            }else{
                if(matches == null){
                    this.haserror.email = 'has-error'; this.allerrors = true;
                    this.Error.email = 'Invalid email address.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.email = ''; return false;
        },
        //postal
        validPostal: function() {
            if(this.newProfile.postal == ""){
                this.haserror.postal = 'has-error'; this.allerrors = true;
                this.Error.postal = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.postal = ''; return false;
        },
        //profLicense
        validProfLicense: function() {
            if(this.newProfile.license == ""){
                this.haserror.license = 'has-error'; this.allerrors = true;
                this.Error.license = 'This field is required.';
                return true;
            }
            this.allerrors = false; this.haserror.license = ''; return false;
        },

    },

    methods: {
        getValidate: function(e) {
            var totalError = 0;
            // check title
            if(this.newProfile.title == ""){
                this.haserror.title = 'has-error';
                this.Error.title    = 'This field is required.';
                totalError += 1;
            }else{
                this.haserror.title = '';
                this.Error.title    = '';
            }

            // check firstname
            if(this.newProfile.fname == ""){
                this.haserror.fname = 'has-error';
                this.Error.fname    = 'This field is required.';
                totalError += 1;
            }else{
                this.haserror.fname = '';
                this.Error.fname    = '';
            }

            //check dateofbirth
            if(this.newProfile.bday == ""){
                this.haserror.bday = 'has-error';
                this.Error.bday    = 'This field is required.';
                totalError += 1;
            }else{
                this.haserror.bday = '';
                this.Error.bday    = '';
            }

            //check gender
            if(this.newProfile.gender == ""){
                this.haserror.gender = 'has-error';
                this.Error.gender    = 'This field is required.';
                this.Error.validGender= true;
                totalError += 1;
            }else{
                this.haserror.gender   = '';
                this.Error.gender      = '';
                this.Error.validGender = false;
            }

            //check civilstatus
            if(this.newProfile.status == ""){
                this.haserror.status   = 'has-error';
                this.Error.status      = 'This field is required.';
                this.Error.validStatus = true;
                totalError += 1;
            }else{
                this.haserror.status   = '';
                this.Error.status      = '';
                this.Error.validStatus = false;
            }

            //check expertise
            if($('#cmbAreaOfExpertise').val() == null){
                this.haserror.expertise   = 'has-error';
                this.Error.expertise      = 'This field is required.';
                this.Error.validExpertise = true;
                $('#expertiseClass span.select2-selection.select2-selection').css('border', '1px solid #a94442');
                totalError += 1;
            }else{
                this.haserror.expertise   = '';
                this.Error.expertise      = '';
                $('#expertiseClass span.select2-selection.select2-selection').css('border', '1px solid rgb(204, 204, 204)');
                this.Error.validExpertise = false;
            }

            //check specialization
            if($('#cmbSpecialization').val() == null){
                this.haserror.specialization   = 'has-error';
                this.Error.specialization      = 'This field is required.';
                this.Error.validspecialization = true;
                $('#specializationClass span.select2-selection.select2-selection').css('border', '1px solid #a94442');
                totalError += 1;
            }else{
                this.haserror.specialization   = '';
                this.Error.specialization      = '';
                $('#specializationClass span.select2-selection.select2-selection').css('border', '1px solid rgb(204, 204, 204)');
                this.Error.validspecialization = false;
            }

            //check profession
            if($('#cmbProfession').val() == ""){
                this.haserror.profession   = 'has-error';
                this.Error.profession      = 'This field is required.';
                this.Error.validprofession = true;
                $('#professionClass span.select2-selection.select2-selection').css('border', '1px solid #a94442');
                totalError += 1;
            }else{
                this.haserror.profession   = '';
                this.Error.profession      = '';
                $('#professionClass span.select2-selection.select2-selection').css('border', '1px solid rgb(204, 204, 204)');
                this.Error.validprofession = false;
            }

            //prevent default when there is errors
            if(totalError > 0 || this.allerrors){ e.preventDefault(); }
        }
    }
});