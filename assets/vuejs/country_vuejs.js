// Country
new Vue({
    el: '#country',

    data: {
        newCountry: { glo_country: '', glo_continent: '' },
        subisExist: false,
        haserror: ''
    },

    ready: function() {
        // this.fetchCountry();
    },

    methods: {

        fetchCountry: function() {
            this.$http.get('../country/fetchCountry', function(countries) {
                this.$set('countries', countries);
            });
        },

        onSubmitForm: function(e) {
            e.preventDefault();
            var country_name = this.newCountry.glo_country;
            var isExist = false;
            this.countries.forEach(function(ctryname) {
                if(ctryname.glo_country == country_name){
                    isExist = true;
                }
            });

            // begin isExist
            if (isExist == true){
                this.haserror = 'has-error';
                this.subisExist = true;
            }else{
                Vue.http.options.emulateJSON = true;
                this.$http.post('../country/insertCountry', this.newCountry, function () {});
                this.countries.push(this.newCountry);
                this.newCountry = { glo_country: '', glo_continent: '' };
                this.haserror = '';
                this.subisExist = false;
                $('#myModal').modal('hide');
            }
            // end isExist

        }
    }
});