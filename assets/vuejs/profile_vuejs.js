
//education
new Vue({
    el: '#profEduc',

    data: {
        courses: [],
        countries: [],
        levels: [],
        educations:  [],
        neweduc: {
            eid: '',
            level: '',
            school: '',
            country: '',
            course: '',
            yrGrd: ''
        },
        haserror: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrGrd: ''
        },
        Error: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrGrd: ''
        },
        ErrorValidation: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrGrd: ''
        },
        action: '',
        actionName: '',
        baseUrl: '',
    },

    ready: function() {
        this.haserror.level = '';
        this.Error.level = '';

        this.haserror.school = '';
        this.Error.school = '';

        this.haserror.country = '';
        this.Error.country = '';

        this.haserror.course = '';
        this.Error.course = '';

        this.haserror.yrGrd = '';
        this.Error.yrGrd = '';

        this.getEducation();
        this.fetchOptions();
        this.getRowValue();
    },

    computed: {
        validLevel: function() {
            if(this.neweduc.level == ""){
                this.haserror.level = 'has-error';
                this.Error.level = 'This field is required.';
                this.ErrorValidation.level = true;
                return true;
            }
            this.ErrorValidation.level = false;
            this.Error.level = '';
            this.haserror.level = '';
            return false;
        },

        validSchool: function() {
            if(this.neweduc.school == ""){
                this.haserror.school = 'has-error';
                this.Error.school = 'This field is required.';
                this.ErrorValidation.school = true;
                return true;
            }
            this.ErrorValidation.school = false;
            this.Error.school = '';
            this.haserror.school = '';
            return false;
        },

        validCountry: function() {
            if(this.neweduc.country == ""){
                this.haserror.country = 'has-error';
                this.Error.country = 'This field is required.';
                this.ErrorValidation.country = true;
                return true;
            }
            this.ErrorValidation.country = false;
            this.Error.country = '';
            this.haserror.country = '';
            return false;
        },

        validCourse: function() {
            if(this.neweduc.course == ""){
                this.haserror.course = 'has-error';
                this.Error.course = 'This field is required.';
                this.ErrorValidation.course = true;
                return true;
            }
            this.ErrorValidation.course = false;
            this.Error.course = '';
            this.haserror.course = '';
            return false;
        },

        validYrGrd: function() {
            var matches = this.neweduc.yrGrd.match(/^[0-9]+$/g);
            if(this.neweduc.yrGrd != ""){
                if(matches == null){
                    this.haserror.yrGrd = 'has-error';
                    this.Error.yrGrd = 'Invalid input.';
                    this.ErrorValidation.yrGrd = true;
                    return true;
                }else{
                    if(!this.neweduc.yrGrd.replace(/\s/g, '').length){
                        this.haserror.yrGrd = 'has-error';
                        this.Error.yrGrd = 'Invalid input.';
                        this.ErrorValidation.yrGrd = true;
                        return true;  
                    }else{
                        if(this.neweduc.yrGrd < 1901){
                            this.haserror.yrGrd = 'has-error';
                            this.Error.yrGrd = 'Invalid year.';
                            this.ErrorValidation.yrGrd = true;
                            return true;  
                        }
                    }
                }
            }else{
                // this.haserror.yrGrd = 'has-error';
                // this.Error.yrGrd = 'This field is required.';
                // this.ErrorValidation.yrGrd = true;
                // return true;
            }
            this.ErrorValidation.yrGrd = false;
            this.Error.yrGrd = '';
            this.haserror.yrGrd = '';
            return false;
        },
    },

    methods: {
        getEducation: function() {
            this.$http.get(this.baseUrl + 'scientists/fetchEducation/'+$('#sciid').val(), function(educations) {
                this.$set('educations', educations);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'scientists/fetchCourse', function(courses) {
                this.$set('courses', courses);
            });
            this.$http.get(this.baseUrl + 'scientists/fetchCountry', function(countries) {
                this.$set('countries', countries);
            });
            this.$http.get(this.baseUrl + 'scientists/fetchLevel', function(levels) {
                this.$set('levels', levels);
            });
        },
        getRowValue: function (educ) {
            if(educ != undefined){
                this.neweduc.eid = educ.educ_id;
                this.neweduc.level = educ.educ_level_id;
                this.neweduc.school = educ.educ_school;
                this.neweduc.country = educ.educ_countryorigin_id;
                this.neweduc.course = educ.educ_course_id;
                if(educ.educ_year_graduated == '0'){
                    educ.educ_year_graduated = '';
                }
                this.neweduc.yrGrd = educ.educ_year_graduated;
                $('#txteducid_edit').val(educ.educ_id);
                $('#txteducid').val(educ.educ_id);
                this.action = 'edit';
                this.actionName = 'scientists/editEducation/';
            }
            
        },
        addEducation: function(e) {
            this.neweduc.eid = '';
            this.neweduc.level = '';
            this.neweduc.school = '';
            this.neweduc.country = '';
            this.neweduc.course = '';
            this.neweduc.yrGrd = '';
            this.action = 'add';
            this.actionName = 'scientists/addEducation/';
        },

        getValidateEducation: function(e) {
            // check level
            if(this.neweduc.level == ""){
                this.haserror.level = 'has-error';
                this.Error.level    = 'This field is required.';
                this.ErrorValidation.level = true;
            }

            // check school
            if(this.neweduc.school == ""){
                this.haserror.school = 'has-error';
                this.Error.school    = 'This field is required.';
                this.ErrorValidation.school = true;
            }

            // check country
            if(this.neweduc.country == ""){
                this.haserror.country = 'has-error';
                this.Error.country    = 'This field is required.';
                this.ErrorValidation.country = true;
            }

            // check course
            if(this.neweduc.course == ""){
                this.haserror.course = 'has-error';
                this.Error.course    = 'This field is required.';
                this.ErrorValidation.course = true;
            }

            // check yrGrd
            if(this.neweduc.yrGrd == ""){
                this.haserror.yrGrd = '';
                this.Error.yrGrd    = '';
                this.ErrorValidation.yrGrd = false;
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }

    }
});

//education
new Vue({
    el: '#profEmp',

    data: {
        employment: [],
        countries: [],
        
        eid: '',
        co: '',
        pos: '',
        dfrom: '',
        dto: '',
        country: '',

        haserror: {
            co: '',
            pos: '',
            dfrom: '',
            dto: '',
            country: ''
        },
        Error: {
            co: '',
            pos: '',
            dfrom: '',
            dto: '',
            country: ''
        },
        ErrorValidation: {
            co: false,
            pos: false,
            dfrom: false,
            dto: false,
            country: false,
        },
        action: 'add',
        actionName: '',
        baseUrl: '',
    },

    ready: function() {
        // $("#txtdfrom").on('keydown blur', function(){
        //     if($("#txtdfrom").val() != ''){
        //         $('#divdfrom').removeClass('has-error');
        //         $('#spandfrom').hide();
        //     }
        //     if($('#txtdfrom').val() == 'YYYY-DD-MM'){
        //         $('#divdfrom').addClass('has-error');
        //         $('#spandfrom').html('This field is required.');
        //         $('#spandfrom').show();
        //     }
        // });

        // $("#txtdto").on('keydown blur', function(){
        //     if($("#txtdto").val() != ''){
        //         $('#divdto').removeClass('has-error');
        //         $('#spandto').hide();
        //     }
        //     if($('#txtdto').val() == 'YYYY-DD-MM'){
        //         $('#divdto').addClass('has-error');
        //         $('#spandto').html('This field is required.');
        //         $('#spandto').show();
        //     }
        // });

        this.getEmployment();
        this.fetchOptions();
        this.getRowValue();
    },

    watch: {
        'co': function() {
            var matches = this.co.match(/[~!@#$%^&*()_+=?><:;"]/g);
            if(this.co == ""){
                this.haserror.co          = 'has-error';
                this.Error.co             = 'This field is required.';
                this.ErrorValidation.co   = true;
            }else if(this.co == " " || (!this.co.replace(/\s/g, '').length)){
                this.haserror.co          = 'has-error';
                this.Error.co             = 'Invalid input.';
                this.ErrorValidation.co = true;
            }else if(matches != null){
                this.haserror.co          = 'has-error';
                this.Error.co             = 'Invalid input.';
                this.ErrorValidation.co = true;
            }else{
                this.haserror.co          = '';
                this.Error.co             = '';
                this.ErrorValidation.co = false;
            }
        },

        'pos': function() {
            var matches = this.pos.match(/[~!@#$%^&*()_+=?><:;"]/g);
            if(this.pos == ""){
                this.haserror.pos          = 'has-error';
                this.Error.pos             = 'This field is required.';
                this.ErrorValidation.pos   = true;
            }else if(this.pos == " " || (!this.pos.replace(/\s/g, '').length)){
                this.haserror.pos          = 'has-error';
                this.Error.pos             = 'Invalid input.';
                this.ErrorValidation.pos = true;
            }else if(matches != null){
                this.haserror.pos          = 'has-error';
                this.Error.pos             = 'Invalid input.';
                this.ErrorValidation.pos = true;
            }else{
                this.haserror.pos          = '';
                this.Error.pos             = '';
                this.ErrorValidation.pos = false;
            }
        },

        'country': function() {
            if(this.country == ""){
                this.haserror.country          = 'has-error';
                this.Error.country             = 'This field is required.';
                this.ErrorValidation.country   = true;
            }else{
                this.haserror.country          = '';
                this.Error.country             = '';
                this.ErrorValidation.country = false;
            }
        },

    },

    methods: {
        getEmployment: function() {
            this.$http.get(this.baseUrl + 'scientists/fetchEmployment/'+$('#sciid').val(), function(employment) {
                this.$set('employment', employment);
            });
        },
        fetchOptions: function() {
            this.$http.get(this.baseUrl + 'scientists/fetchCountry', function(countries) {
                this.$set('countries', countries);
            });
        },
        getRowValue: function (emp) {
            if(emp != undefined){
                this.eid = emp.emp_id;
                this.co = emp.emp_company;
                this.pos = emp.emp_position;
                if(emp.emp_datefrom == '0000-00-00')
                    emp.emp_datefrom = '';
                this.dfrom = emp.emp_datefrom;
                if(emp.emp_dateto == '0000-00-00')
                    emp.emp_dateto = '';
                this.dto = emp.emp_dateto;
                this.country = emp.emp_countryorigin_id;
                $('#txtempid_edit').val(emp.emp_id);
                $('#txtempid').val(emp.emp_id);
                this.action = 'edit';
                this.actionName = 'scientists/editEmployment/';
            }
            
        },
        addEmployment: function() {
            this.eid = '';
            this.co = '';
            this.pos = '';
            this.dfrom = '';
            this.dto = '';
            this.country = '';
            this.action = 'add';
            this.actionName = 'scientists/addEmployment/';
        },

        getValidateEmployment: function(e) {
            this.dfrom = $("#txtdfrom").val();
            this.dto = $("#txtdto").val();

            if(this.co == ""){
                this.haserror.co          = 'has-error';
                this.Error.co             = 'This field is required.';
                this.ErrorValidation.co   = true;
            }

            if(this.pos == ""){
                this.haserror.pos          = 'has-error';
                this.Error.pos             = 'This field is required.';
                this.ErrorValidation.pos   = true;
            }

            // if(this.dfrom == ""){
            //     $('#divdfrom').addClass('has-error');
            //     $('#spandfrom').html('This field is required.');
            //     $('#spandfrom').show();
            // }else{
            //     $('#divdfrom').removeClass('has-error');
            //     $('#spandfrom').hide();
            // }

            // if(this.dto == ""){
            //     $('#divdto').addClass('has-error');
            //     $('#spandto').html('This field is required.');
            //     $('#spandto').show();
            // }else{
            //     $('#divdto').removeClass('has-error');
            //     $('#spandto').hide();
            // }

            if(this.country == ""){
                this.haserror.country          = 'has-error';
                this.Error.country             = 'This field is required.';
                this.ErrorValidation.country   = true;
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        }

    }

});
