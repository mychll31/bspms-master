// add profile
new Vue({
    el: '#sciProfile',

    data: {
        title: '',
        fname: '',
        middlename: '',
        middleinitial: '',
        lname: '',
        extname: '',
        gender: '',
        status: '',
        profession: '',
        license: '',
        postal: '',
        birthday: '',

        modalContact: '',
        contact_haserror: '',
        contact_ErrorValidation: false,
        contact_Error: '',
        contacts: [],

        modalEmail: '',
        email_haserror: '',
        email_ErrorValidation: false,
        email_Error: '',
        emails: [],

        ErrorValidation: {
            title: false,
            fname: false,
            middlename: false,
            middleinitial: false,
            lname: false,
            extname: false,
            gender: false,
            status: false,
            profession: false,
            license: false,
            postal: false,
            allContacts: false,
            allEmails: false,
            birthday: false,

            specialization: false,
            areaexpertise: false,
        },

        Error: {
            title: '',
            fname: '',
            middlename: '',
            middleinitial: '',
            lname: '',
            extname: '',
            gender: '',
            status: '',
            profession: '',
            license: '',
            postal: '',
            allContacts: '',
            allEmails: '',
            birthday: '',
        },

        haserror: {
            title: '',
            fname: '',
            middlename: '',
            middleinitial: '',
            lname: '',
            extname: '',
            gender: '',
            status: '',
            profession: '',
            license: '',
            postal: '',
            allContacts: '',
            allEmails: '',
            birthday: '',
        },
    },

    ready: function(){
        $("#txtBirthDate").on('keydown blur', function(){
            if($("#txtBirthDate").val() != ''){
                $('#divbirthday').removeClass('has-error');
                $('#spanbirthday').hide();
            }
            if($('#txtBirthDate').val() == 'YYYY-DD-MM'){
                $('#divbirthday').addClass('has-error');
                $('#spanbirthday').html('This field is required.');
                $('#spanbirthday').show();
            }
        });

        $('#cmbAreaOfExpertise').on("change", function(e) {
            if($("#cmbAreaOfExpertise").val() != null){
                $('#divexpertise').removeClass('has-error');
                $('#spanexpertise').hide();
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
            }else{
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divexpertise').addClass('has-error');
                $('#spanexpertise').html('This field is required.');
                $('#spanexpertise').show();
            }
        });

        $('#cmbSpecialization').on("change", function(e) {
            if($("#cmbSpecialization").val() != null){
                $('#divspecialization').removeClass('has-error');
                $('#spanspecialization').hide();
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
            }else{
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divspecialization').addClass('has-error');
                $('#spanspecialization').html('This field is required.');
                $('#spanspecialization').show();
            }
        });

    },

    watch: {
        'title': function(val) {
            if(val != ''){
                this.haserror.title         = '';
                this.Error.title            = '';
                this.ErrorValidation.title  = false;
            }else{
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
        },

        'fname': function(val) {
            if(val == ''){
                this.haserror.fname         = 'has-error';
                this.Error.fname            = 'This field is required.';
                this.ErrorValidation.fname  = true;
            }else{
                if(!this.fname.replace(/\s/g, '').length){
                    this.haserror.fname         = 'has-error';
                    this.Error.fname            = 'Invalid input.';
                    this.ErrorValidation.fname  = true;
                }else{
                    this.haserror.fname         = '';
                    this.Error.fname            = '';
                    this.ErrorValidation.fname  = false;
                }
            }
        },

        'middlename': function(val) {
            if(val != ''){
                if(!this.middlename.replace(/\s/g, '').length){
                    this.haserror.middlename         = 'has-error';
                    this.Error.middlename            = 'Invalid input.';
                    this.ErrorValidation.middlename  = true;
                }else{
                    this.haserror.middlename         = '';
                    this.Error.middlename            = '';
                    this.ErrorValidation.middlename  = false;
                }
            }else{
                this.haserror.middlename         = '';
                this.Error.middlename            = '';
                this.ErrorValidation.middlename  = false;
            }
        },

        'middleinitial': function(val) {
            if(val != ''){
                if(!this.middleinitial.replace(/\s/g, '').length){
                    this.haserror.middleinitial         = 'has-error';
                    this.Error.middleinitial            = 'Invalid input.';
                    this.ErrorValidation.middleinitial  = true;
                }else{
                    this.haserror.middleinitial         = '';
                    this.Error.middleinitial            = '';
                    this.ErrorValidation.middleinitial  = false;
                }
            }else{
                this.haserror.middleinitial         = '';
                this.Error.middleinitial            = '';
                this.ErrorValidation.middleinitial  = false;
            }
        },

        'lname': function(val) {
            if(val == ''){
                this.haserror.lname         = 'has-error';
                this.Error.lname            = 'This field is required.';
                this.ErrorValidation.lname  = true;
            }else{
                if(!this.lname.replace(/\s/g, '').length){
                    this.haserror.lname         = 'has-error';
                    this.Error.lname            = 'Invalid input.';
                    this.ErrorValidation.lname  = true;
                }else{
                    this.haserror.lname         = '';
                    this.Error.lname            = '';
                    this.ErrorValidation.lname  = false;
                }
            }
        },

        'extname': function(val) {
            if(val != ''){
                if(!this.extname.replace(/\s/g, '').length){
                    this.haserror.extname         = 'has-error';
                    this.Error.extname            = 'Invalid input.';
                    this.ErrorValidation.extname  = true;
                }else{
                    this.haserror.extname         = '';
                    this.Error.extname            = '';
                    this.ErrorValidation.extname  = false;
                }
            }else{
                this.haserror.extname         = '';
                this.Error.extname            = '';
                this.ErrorValidation.extname  = false;
            }
        },

        'gender': function(val) {
            if(val != ''){
                this.haserror.gender         = '';
                this.Error.gender            = '';
                this.ErrorValidation.gender  = false;
            }else{
                this.haserror.gender         = 'has-error';
                this.Error.gender            = 'This field is required.';
                this.ErrorValidation.gender  = true;
            }
        },

        'status': function(val) {
            if(val != ''){
                this.haserror.status         = '';
                this.Error.status            = '';
                this.ErrorValidation.status  = false;
            }else{
                this.haserror.status         = 'has-error';
                this.Error.status            = 'This field is required.';
                this.ErrorValidation.status  = true;
            }
        },

        'profession': function(val) {
            if(val != ''){
                this.haserror.profession         = '';
                this.Error.profession            = '';
                this.ErrorValidation.profession  = false;
            }else{
                this.haserror.profession         = 'has-error';
                this.Error.profession            = 'This field is required.';
                this.ErrorValidation.profession  = true;
            }
        },

        'license': function(val) {
            if(val == ''){
                this.haserror.license         = 'has-error';
                this.Error.license            = 'This field is required.';
                this.ErrorValidation.license  = true;
            }else{
                if(!this.license.replace(/\s/g, '').length){
                    this.haserror.license         = 'has-error';
                    this.Error.license            = 'Invalid input.';
                    this.ErrorValidation.license  = true;
                }else{
                    this.haserror.license         = '';
                    this.Error.license            = '';
                    this.ErrorValidation.license  = false;
                }
            }
        },

        'postal': function(val) {
            if(val == ''){
                this.haserror.postal         = 'has-error';
                this.Error.postal            = 'This field is required.';
                this.ErrorValidation.postal  = true;
            }else{
                if(!this.postal.replace(/\s/g, '').length){
                    this.haserror.postal         = 'has-error';
                    this.Error.postal            = 'Invalid input.';
                    this.ErrorValidation.postal  = true;
                }else{
                    this.haserror.postal         = '';
                    this.Error.postal            = '';
                    this.ErrorValidation.postal  = false;
                }
            }
        },

        'modalContact': function(val) {
            if(val == ''){
                this.contact_haserror         = 'has-error';
                this.contact_Error            = 'This field is required.';
                this.contact_ErrorValidation  = true;
            }else{
                if(!this.modalContact.replace(/\s/g, '').length){
                    this.contact_haserror         = 'has-error';
                    this.contact_Error            = 'Invalid input.';
                    this.contact_ErrorValidation  = true;
                }else{
                    this.contact_haserror         = '';
                    this.contact_Error            = '';
                    this.contact_ErrorValidation  = false;
                }
            }
        },

        'modalEmail': function(val) {
            if(val == ''){
                this.email_haserror         = 'has-error';
                this.email_Error            = 'This field is required.';
                this.email_ErrorValidation  = true;
            }else{
                if(!this.modalEmail.replace(/\s/g, '').length){
                    this.email_haserror         = 'has-error';
                    this.email_Error            = 'Invalid input.';
                    this.email_ErrorValidation  = true;
                }else{
                    this.email_haserror         = '';
                    this.email_Error            = '';
                    this.email_ErrorValidation  = false;
                }
            }
        },

    },

    methods: {

        // email

        btnaddEmail: function(){
            this.email_haserror = '';
            this.email_ErrorValidation = false;
            this.email_Error = '';
        },

        clearEmail: function(){
            this.modalEmail = '';
        },

        removeEmail: function(email){
            this.emails.$remove(email);
            if(this.emails.length == 0){
                this.haserror.allEmails   = 'has-error';
                this.Error.allEmails      = 'Email/s must not be empty.';
                this.ErrorValidation.allEmails = true;
            }
        },

        getEmail: function(e){
            this.modalEmail = $('#txtmodalEmail').val();
            var matches_validEmail = this.modalEmail.match(/[A-Za-z0-9._]@[A-Za-z0-9._]{1,}$/g);
            if(this.modalEmail == ''){
                this.email_haserror         = 'has-error';
                this.email_Error            = 'This field is required.';
                this.email_ErrorValidation  = true;
            }else if(matches_validEmail == null){
                this.email_haserror         = 'has-error';
                this.email_Error            = 'Invalid input.';
                this.email_ErrorValidation  = true;
            }else{
                this.emails.push(this.modalEmail);
                this.modalEmail = '';
                $('#divAddEmail').modal('hide');
            }
            if(this.emails.length > 0){
                this.haserror.allEmails = '';
                this.ErrorValidation.allEmails = false;
                this.Error.allEmails = '';
            }
        },

        // contact

        btnaddContact: function(){
            this.contact_haserror = '';
            this.contact_ErrorValidation = false;
            this.contact_Error = '';
        },

        clearcontact: function(){   
            this.modalContact = '';
        },

        removeContact: function(contact){
            this.contacts.$remove(contact);
            if(this.contacts.length == 0){
                this.haserror.allContacts   = 'has-error';
                this.Error.allContacts      = 'Contact/s must not be empty.';
                this.ErrorValidation.allContacts = true;
            }
        },

        getContact: function(e){
            this.modalContact = $('#txtmodalContact').val();
            if(this.modalContact == ''){
                this.contact_haserror         = 'has-error';
                this.contact_Error            = 'This field is required.';
                this.contact_ErrorValidation  = true;
            }else if(!this.modalContact.replace(/\s/g, '').length){
                this.contact_haserror         = 'has-error';
                this.contact_Error            = 'Invalid input.';
                this.contact_ErrorValidation  = true;
            }else{
                this.contacts.push(this.modalContact);
                this.modalContact = '';
                $('#divAddContact').modal('hide');
            }
            if(this.contacts.length > 0){
                this.haserror.allContacts = '';
                this.ErrorValidation.allContacts = false;
                this.Error.allContacts = '';
            }
        },

        getValidate: function(e) {

            var valContact = true;
            $('input[name="arrContact[]"]').each(function(){
                if(!this.value.replace(/\s/g, '').length){
                    valContact = false;
                }
            });

            if(!valContact){
                this.haserror.allContacts   = 'has-error';
                this.Error.allContacts      = 'Invalid input.';
                this.ErrorValidation.allContacts = true;
            }else{
                this.haserror.allContacts   = '';
                this.Error.allContacts      = '';
                this.ErrorValidation.allContacts = false;
            }

            var valemail = true;
            $('input[name="arrEmail[]"]').each(function(){
                var matches_validEmail = this.value.match(/[A-Za-z0-9._]@[A-Za-z0-9._]{1,}$/g);
                if(!this.value.replace(/\s/g, '').length || matches_validEmail == null){
                    valemail = false;
                }
            });

            if(!valemail){
                this.haserror.allEmails   = 'has-error';
                this.Error.allEmails      = 'Invalid input.';
                this.ErrorValidation.allEmails = true;
            }else{
                this.haserror.allEmails   = '';
                this.Error.allEmails      = '';
                this.ErrorValidation.allEmails = false;
            }
            
            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }

            if(this.fname == ''){
                this.haserror.fname         = 'has-error';
                this.Error.fname            = 'This field is required.';
                this.ErrorValidation.fname  = true;
            }

            if(this.lname == ''){
                this.haserror.lname         = 'has-error';
                this.Error.lname            = 'This field is required.';
                this.ErrorValidation.lname  = true;
            }

            if(this.gender == ''){
                this.haserror.gender         = 'has-error';
                this.Error.gender            = 'This field is required.';
                this.ErrorValidation.gender  = true;
            }

            if(this.status == ''){
                this.haserror.status         = 'has-error';
                this.Error.status            = 'This field is required.';
                this.ErrorValidation.status  = true;
            }

            if(this.profession == ''){
                this.haserror.profession         = 'has-error';
                this.Error.profession            = 'This field is required.';
                this.ErrorValidation.profession  = true;
            }

            if(this.license == ''){
                this.haserror.license         = 'has-error';
                this.Error.license            = 'This field is required.';
                this.ErrorValidation.license  = true;
            }

            if(this.postal == ''){
                this.haserror.postal         = 'has-error';
                this.Error.postal            = 'This field is required.';
                this.ErrorValidation.postal  = true;
            }

            if(this.contacts.length == 0){
                this.haserror.allContacts   = 'has-error';
                this.Error.allContacts      = 'Contact/s must not be empty.';
                this.ErrorValidation.allContacts = true;
            }

            if(this.emails.length == 0){
                this.haserror.allEmails   = 'has-error';
                this.Error.allEmails      = 'Email/s must not be empty.';
                this.ErrorValidation.allEmails = true;
            }

            if($("#txtBirthDate").val() != ''){
                $('#divbirthday').removeClass('has-error');
                $('#spanbirthday').hide();
                this.ErrorValidation.birthday = false;
            }else{
                this.ErrorValidation.birthday = true;
                $('#divbirthday').addClass('has-error');
                $('#spanbirthday').html('This field is required.');
                $('#spanbirthday').show();
            }

            if($("#cmbAreaOfExpertise").val() != null){
                $('#divexpertise').removeClass('has-error');
                $('#spanexpertise').hide();
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
                this.ErrorValidation.areaexpertise = false;
            }else{
                this.ErrorValidation.areaexpertise = true;
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divexpertise').addClass('has-error');
                $('#spanexpertise').html('This field is required.');
                $('#spanexpertise').show();
            }

            if($("#cmbSpecialization").val() != null){
                $('#divspecialization').removeClass('has-error');
                $('#spanspecialization').hide();
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
                this.ErrorValidation.specialization = false;
            }else{
                this.ErrorValidation.specialization = true;
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divspecialization').addClass('has-error');
                $('#spanspecialization').html('This field is required.');
                $('#spanspecialization').show();
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }

        },
    }
});

// edit profile
new Vue({
    el: '#editsciProfile',

    data: {
        title: '',
        fname: '',
        middlename: '',
        middleinitial: '',
        lname: '',
        extname: '',
        gender: '',
        status: '',
        profession: '',
        license: '',
        postal: '',
        birthday: '',

        modalContact: '',
        contact_haserror: '',
        contact_ErrorValidation: false,
        contact_Error: '',
        contacts: [],

        modalEmail: '',
        email_haserror: '',
        email_ErrorValidation: false,
        email_Error: '',
        emails: [],

        editcontacts: '',
        editemails: '',

        ErrorValidation: {
            title: false,
            fname: false,
            middlename: false,
            middleinitial: false,
            lname: false,
            extname: false,
            gender: false,
            status: false,
            profession: false,
            license: false,
            postal: false,
            allContacts: false,
            allEmails: false,
            birthday: false,

            specialization: false,
            areaexpertise: false,
        },

        Error: {
            title: '',
            fname: '',
            middlename: '',
            middleinitial: '',
            lname: '',
            extname: '',
            gender: '',
            status: '',
            profession: '',
            license: '',
            postal: '',
            allContacts: '',
            allEmails: '',
            birthday: '',
        },

        haserror: {
            title: '',
            fname: '',
            middlename: '',
            middleinitial: '',
            lname: '',
            extname: '',
            gender: '',
            status: '',
            profession: '',
            license: '',
            postal: '',
            allContacts: '',
            allEmails: '',
            birthday: '',
        },
    },

    ready: function(){

        this.getMisc();

        $("#txtBirthDate").on('keydown blur', function(){
            if($("#txtBirthDate").val() != ''){
                $('#divbirthday').removeClass('has-error');
                $('#spanbirthday').hide();
            }
            if($('#txtBirthDate').val() == 'YYYY-DD-MM'){
                $('#divbirthday').addClass('has-error');
                $('#spanbirthday').html('This field is required.');
                $('#spanbirthday').show();
            }
        });

        $('#cmbAreaOfExpertise').on("change", function(e) {
            if($("#cmbAreaOfExpertise").val() != null){
                $('#divexpertise').removeClass('has-error');
                $('#spanexpertise').hide();
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
            }else{
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divexpertise').addClass('has-error');
                $('#spanexpertise').html('This field is required.');
                $('#spanexpertise').show();
            }
        });

        $('#cmbSpecialization').on("change", function(e) {
            if($("#cmbSpecialization").val() != null){
                $('#divspecialization').removeClass('has-error');
                $('#spanspecialization').hide();
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
            }else{
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divspecialization').addClass('has-error');
                $('#spanspecialization').html('This field is required.');
                $('#spanspecialization').show();
            }
        });

    },

    watch: {
        'title': function(val) {
            if(val != ''){
                this.haserror.title         = '';
                this.Error.title            = '';
                this.ErrorValidation.title  = false;
            }else{
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }
        },

        'fname': function(val) {
            if(val == ''){
                this.haserror.fname         = 'has-error';
                this.Error.fname            = 'This field is required.';
                this.ErrorValidation.fname  = true;
            }else{
                if(!this.fname.replace(/\s/g, '').length){
                    this.haserror.fname         = 'has-error';
                    this.Error.fname            = 'Invalid input.';
                    this.ErrorValidation.fname  = true;
                }else{
                    this.haserror.fname         = '';
                    this.Error.fname            = '';
                    this.ErrorValidation.fname  = false;
                }
            }
        },

        'middlename': function(val) {
            if(val != ''){
                if(!this.middlename.replace(/\s/g, '').length){
                    this.haserror.middlename         = 'has-error';
                    this.Error.middlename            = 'Invalid input.';
                    this.ErrorValidation.middlename  = true;
                }else{
                    this.haserror.middlename         = '';
                    this.Error.middlename            = '';
                    this.ErrorValidation.middlename  = false;
                }
            }else{
                this.haserror.middlename         = '';
                this.Error.middlename            = '';
                this.ErrorValidation.middlename  = false;
            }
        },

        'middleinitial': function(val) {
            if(val != ''){
                if(!this.middleinitial.replace(/\s/g, '').length){
                    this.haserror.middleinitial         = 'has-error';
                    this.Error.middleinitial            = 'Invalid input.';
                    this.ErrorValidation.middleinitial  = true;
                }else{
                    this.haserror.middleinitial         = '';
                    this.Error.middleinitial            = '';
                    this.ErrorValidation.middleinitial  = false;
                }
            }else{
                this.haserror.middleinitial         = '';
                this.Error.middleinitial            = '';
                this.ErrorValidation.middleinitial  = false;
            }
        },

        'lname': function(val) {
            if(val == ''){
                this.haserror.lname         = 'has-error';
                this.Error.lname            = 'This field is required.';
                this.ErrorValidation.lname  = true;
            }else{
                if(!this.lname.replace(/\s/g, '').length){
                    this.haserror.lname         = 'has-error';
                    this.Error.lname            = 'Invalid input.';
                    this.ErrorValidation.lname  = true;
                }else{
                    this.haserror.lname         = '';
                    this.Error.lname            = '';
                    this.ErrorValidation.lname  = false;
                }
            }
        },

        'extname': function(val) {
            if(val != ''){
                if(!this.extname.replace(/\s/g, '').length){
                    this.haserror.extname         = 'has-error';
                    this.Error.extname            = 'Invalid input.';
                    this.ErrorValidation.extname  = true;
                }else{
                    this.haserror.extname         = '';
                    this.Error.extname            = '';
                    this.ErrorValidation.extname  = false;
                }
            }else{
                this.haserror.extname         = '';
                this.Error.extname            = '';
                this.ErrorValidation.extname  = false;
            }
        },

        'gender': function(val) {
            if(val != ''){
                this.haserror.gender         = '';
                this.Error.gender            = '';
                this.ErrorValidation.gender  = false;
            }else{
                this.haserror.gender         = 'has-error';
                this.Error.gender            = 'This field is required.';
                this.ErrorValidation.gender  = true;
            }
        },

        'status': function(val) {
            if(val != ''){
                this.haserror.status         = '';
                this.Error.status            = '';
                this.ErrorValidation.status  = false;
            }else{
                this.haserror.status         = 'has-error';
                this.Error.status            = 'This field is required.';
                this.ErrorValidation.status  = true;
            }
        },

        'profession': function(val) {
            if(val != ''){
                this.haserror.profession         = '';
                this.Error.profession            = '';
                this.ErrorValidation.profession  = false;
            }else{
                this.haserror.profession         = 'has-error';
                this.Error.profession            = 'This field is required.';
                this.ErrorValidation.profession  = true;
            }
        },

        'license': function(val) {
            if(val == ''){
                this.haserror.license         = 'has-error';
                this.Error.license            = 'This field is required.';
                this.ErrorValidation.license  = true;
            }else{
                if(!this.license.replace(/\s/g, '').length){
                    this.haserror.license         = 'has-error';
                    this.Error.license            = 'Invalid input.';
                    this.ErrorValidation.license  = true;
                }else{
                    this.haserror.license         = '';
                    this.Error.license            = '';
                    this.ErrorValidation.license  = false;
                }
            }
        },

        'postal': function(val) {
            if(val == ''){
                this.haserror.postal         = 'has-error';
                this.Error.postal            = 'This field is required.';
                this.ErrorValidation.postal  = true;
            }else{
                if(!this.postal.replace(/\s/g, '').length){
                    this.haserror.postal         = 'has-error';
                    this.Error.postal            = 'Invalid input.';
                    this.ErrorValidation.postal  = true;
                }else{
                    this.haserror.postal         = '';
                    this.Error.postal            = '';
                    this.ErrorValidation.postal  = false;
                }
            }
        },

        'modalContact': function(val) {
            if(val == ''){
                this.contact_haserror         = 'has-error';
                this.contact_Error            = 'This field is required.';
                this.contact_ErrorValidation  = true;
            }else{
                if(!this.modalContact.replace(/\s/g, '').length){
                    this.contact_haserror         = 'has-error';
                    this.contact_Error            = 'Invalid input.';
                    this.contact_ErrorValidation  = true;
                }else{
                    this.contact_haserror         = '';
                    this.contact_Error            = '';
                    this.contact_ErrorValidation  = false;
                }
            }
        },

        'modalEmail': function(val) {
            if(val == ''){
                this.email_haserror         = 'has-error';
                this.email_Error            = 'This field is required.';
                this.email_ErrorValidation  = true;
            }else{
                if(!this.modalEmail.replace(/\s/g, '').length){
                    this.email_haserror         = 'has-error';
                    this.email_Error            = 'Invalid input.';
                    this.email_ErrorValidation  = true;
                }else{
                    this.email_haserror         = '';
                    this.email_Error            = '';
                    this.email_ErrorValidation  = false;
                }
            }
        },

    },

    methods: {

        getMisc: function(){
            this.editcontacts = this.editcontacts.substring(0, this.editcontacts.length-1);
            this.contacts = this.editcontacts.split(';');
            this.editemails = this.editemails.substring(0, this.editemails.length-1);
            this.emails = this.editemails.split(';');
        },

        // email
        btnaddEmail: function(){
            this.email_haserror = '';
            this.email_ErrorValidation = false;
            this.email_Error = '';
        },

        clearEmail: function(){
            this.modalEmail = '';
        },

        removeEmail: function(email){
            this.emails.$remove(email);
            if(this.emails.length == 0){
                this.haserror.allEmails   = 'has-error';
                this.Error.allEmails      = 'Email/s must not be empty.';
                this.ErrorValidation.allEmails = true;
            }
        },

        getEmail: function(e){
            this.modalEmail = $('#txtmodalEmail').val();
            var matches_validEmail = this.modalEmail.match(/[A-Za-z0-9._]@[A-Za-z0-9._]{1,}$/g);
            if(this.modalEmail == ''){
                this.email_haserror         = 'has-error';
                this.email_Error            = 'This field is required.';
                this.email_ErrorValidation  = true;
            }else if(matches_validEmail == null){
                this.email_haserror         = 'has-error';
                this.email_Error            = 'Invalid input.';
                this.email_ErrorValidation  = true;
            }else{
                this.emails.push(this.modalEmail);
                this.modalEmail = '';
                $('#divAddEmail').modal('hide');
            }
            if(this.emails.length > 0){
                this.haserror.allEmails = '';
                this.ErrorValidation.allEmails = false;
                this.Error.allEmails = '';
            }
        },

        // contact

        btnaddContact: function(){
            this.contact_haserror = '';
            this.contact_ErrorValidation = false;
            this.contact_Error = '';
        },

        clearcontact: function(){   
            this.modalContact = '';
        },

        removeContact: function(contact){
            this.contacts.$remove(contact);
            if(this.contacts.length == 0){
                this.haserror.allContacts   = 'has-error';
                this.Error.allContacts      = 'Contact/s must not be empty.';
                this.ErrorValidation.allContacts = true;
            }
        },

        getContact: function(e){
            this.modalContact = $('#txtmodalContact').val();
            if(this.modalContact == ''){
                this.contact_haserror         = 'has-error';
                this.contact_Error            = 'This field is required.';
                this.contact_ErrorValidation  = true;
            }else if(!this.modalContact.replace(/\s/g, '').length){
                this.contact_haserror         = 'has-error';
                this.contact_Error            = 'Invalid input.';
                this.contact_ErrorValidation  = true;
            }else{
                this.contacts.push(this.modalContact);
                this.modalContact = '';
                $('#divAddContact').modal('hide');
            }
            if(this.contacts.length > 0){
                this.haserror.allContacts = '';
                this.ErrorValidation.allContacts = false;
                this.Error.allContacts = '';
            }
        },

        getValidate: function(e) {
            var valContact = true;
            $('input[name="arrContact[]"]').each(function(){
                if(!this.value.replace(/\s/g, '').length){
                    valContact = false;
                }
            });

            if(!valContact){
                this.haserror.allContacts   = 'has-error';
                this.Error.allContacts      = 'Invalid input.';
                this.ErrorValidation.allContacts = true;
            }else{
                this.haserror.allContacts   = '';
                this.Error.allContacts      = '';
                this.ErrorValidation.allContacts = false;
            }

            var valemail = true;
            $('input[name="arrEmail[]"]').each(function(){
                var matches_validEmail = this.value.match(/[A-Za-z0-9._]@[A-Za-z0-9._]{1,}$/g);
                if(!this.value.replace(/\s/g, '').length || matches_validEmail == null){
                    valemail = false;
                }
            });

            if(!valemail){
                this.haserror.allEmails   = 'has-error';
                this.Error.allEmails      = 'Invalid input.';
                this.ErrorValidation.allEmails = true;
            }else{
                this.haserror.allEmails   = '';
                this.Error.allEmails      = '';
                this.ErrorValidation.allEmails = false;
            }

            if(this.title == ''){
                this.haserror.title         = 'has-error';
                this.Error.title            = 'This field is required.';
                this.ErrorValidation.title  = true;
            }

            if(this.fname == ''){
                this.haserror.fname         = 'has-error';
                this.Error.fname            = 'This field is required.';
                this.ErrorValidation.fname  = true;
            }

            if(this.lname == ''){
                this.haserror.lname         = 'has-error';
                this.Error.lname            = 'This field is required.';
                this.ErrorValidation.lname  = true;
            }

            if(this.gender == ''){
                this.haserror.gender         = 'has-error';
                this.Error.gender            = 'This field is required.';
                this.ErrorValidation.gender  = true;
            }

            if(this.status == ''){
                this.haserror.status         = 'has-error';
                this.Error.status            = 'This field is required.';
                this.ErrorValidation.status  = true;
            }

            if(this.profession == ''){
                this.haserror.profession         = 'has-error';
                this.Error.profession            = 'This field is required.';
                this.ErrorValidation.profession  = true;
            }

            if(this.license == ''){
                this.haserror.license         = 'has-error';
                this.Error.license            = 'This field is required.';
                this.ErrorValidation.license  = true;
            }

            if(this.postal == ''){
                this.haserror.postal         = 'has-error';
                this.Error.postal            = 'This field is required.';
                this.ErrorValidation.postal  = true;
            }

            if(this.contacts.length == 0){
                this.haserror.allContacts   = 'has-error';
                this.Error.allContacts      = 'Contact/s must not be empty.';
                this.ErrorValidation.allContacts = true;
            }

            if(this.emails.length == 0){
                this.haserror.allEmails   = 'has-error';
                this.Error.allEmails      = 'Email/s must not be empty.';
                this.ErrorValidation.allEmails = true;
            }

            if($("#txtBirthDate").val() != ''){
                $('#divbirthday').removeClass('has-error');
                $('#spanbirthday').hide();
                this.ErrorValidation.birthday = false;
            }else{
                this.ErrorValidation.birthday = true;
                $('#divbirthday').addClass('has-error');
                $('#spanbirthday').html('This field is required.');
                $('#spanbirthday').show();
            }
            
            if($("#cmbAreaOfExpertise").val() != null){
                $('#divexpertise').removeClass('has-error');
                $('#spanexpertise').hide();
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
                this.ErrorValidation.areaexpertise = false;
            }else{
                this.ErrorValidation.areaexpertise = true;
                $('#divexpertise span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divexpertise').addClass('has-error');
                $('#spanexpertise').html('This field is required.');
                $('#spanexpertise').show();
            }

            if($("#cmbSpecialization").val() != null){
                $('#divspecialization').removeClass('has-error');
                $('#spanspecialization').hide();
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #ccc');
                this.ErrorValidation.specialization = false;
            }else{
                this.ErrorValidation.specialization = true;
                $('#divspecialization span.select2-selection.select2-selection--multiple').css('border', '1px solid #a94442');
                $('#divspecialization').addClass('has-error');
                $('#spanspecialization').html('This field is required.');
                $('#spanspecialization').show();
            }

            // var cont = false;
            // for (var key in this.ErrorValidation) {
            //     if(this.ErrorValidation[key]) cont = true;
            // }

            // if(cont){
            //     e.preventDefault();
            // }

        },
    }
});

// add education
new Vue({
    el: '#sciEducation',

    data: {
        newEducation: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },
        haserror: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },
        Error: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },

        ErrorValidation: {
            level: '',
            school: '',
            country: '',
            course: '',
            yrgraduate: ''
        },

        addEducCount: 0,
        sci_id: '',
        baseUrl: '',
        nofile: false,
    },
    ready: function(){
        this.haserror.level = '';
        this.Error.level = '';

        this.haserror.school = '';
        this.Error.school = '';

        this.haserror.country = '';
        this.Error.country = '';

        this.haserror.course = '';
        this.Error.course = '';

        this.haserror.yrgraduate = '';
        this.Error.yrgraduate = '';

        this.fetchEducCount();
    },
    computed: {
        //level
        validLevel: function() {
            if(this.newEducation.level == ""){
                this.haserror.level = 'has-error'; this.ErrorValidation.level = true;
                this.Error.level = 'This field is required.';
                return true;
            }
            this.ErrorValidation.level = false;
            this.Error.level = ''; this.haserror.level = ''; return false;
        },

        //school
        validSchool: function() {
            if(this.newEducation.school == ""){
                this.haserror.school = 'has-error'; this.ErrorValidation.school = true;
                this.Error.school = 'This field is required.';
                return true;
            }
            this.ErrorValidation.school = false;
            this.Error.school = ''; this.haserror.school = ''; return false;
        },

        //country
        validCountry: function() {
            if(this.newEducation.country == ""){
                this.haserror.country = 'has-error'; this.ErrorValidation.country = true;
                this.Error.country = 'This field is required.';
                return true;
            }
            this.ErrorValidation.country = false;
            this.Error.country = ''; this.haserror.country = ''; return false;
        },

        //course
        validCourse: function() {
            if(this.newEducation.course == ""){
                this.haserror.course = 'has-error'; this.ErrorValidation.course = true;
                this.Error.course = 'This field is required.';
                return true;
            }
            this.ErrorValidation.course = false;
            this.Error.course = ''; this.haserror.course = ''; return false;
        },

        //yrgraduate
        validYrgraduate: function() {
            if(this.newEducation.yrgraduate == ""){
                // this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                // this.Error.yrgraduate = 'This field is required.';
                // return true;
            }else{
                if(this.newEducation.yrgraduate == " " || (!this.newEducation.yrgraduate.replace(/\s/g, '').length)){
                    this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                    this.Error.yrgraduate = 'Invalid input.';
                    return true;
                }else{
                    var matches = this.newEducation.yrgraduate.match(/[A-Za-z@#\$%\^\&*\)\(+=._]+$/g);
                    if(matches != null){
                        this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                        this.Error.yrgraduate = 'Invalid input.';
                        return true;
                    }else if(this.newEducation.yrgraduate < 1960){
                        this.haserror.yrgraduate = 'has-error'; this.ErrorValidation.yrgraduate = true;
                        this.Error.yrgraduate = 'Invalid year.';
                        return true;
                    }
                }
            }
            this.ErrorValidation.yrgraduate = false;
            this.Error.yrgraduate = ''; this.haserror.yrgraduate = ''; return false;
        },
    },
    methods: {
        fetchEducCount: function(){
            this.$http.get(this.baseUrl + 'scientists/fetchEducCount/' + this.sci_id, function(addEducCount) {
                this.$set('addEducCount', addEducCount);
            });
        },

        getvalidate: function(e) {
            if(this.newEducation.level == ""){
                this.haserror.level = 'has-error'; this.Error.level = "This field is required.";
                this.ErrorValidation.level = true
            }

            if(this.newEducation.school == ""){
                this.haserror.school = 'has-error'; this.Error.school = "This field is required.";
                this.ErrorValidation.school = true
            }

            if(this.newEducation.country == ""){
                this.haserror.country = 'has-error'; this.Error.country = "This field is required.";
                this.ErrorValidation.country = true
            }

            if(this.newEducation.course == ""){
                this.haserror.course = 'has-error'; this.Error.course = "This field is required.";
                this.ErrorValidation.course = true
            }

            if(this.newEducation.yrgraduate == ""){
                this.haserror.yrgraduate = ''; this.Error.yrgraduate = "";
                this.ErrorValidation.yrgraduate = false
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                console.log(key+' '+this.ErrorValidation[key]);
                if(this.ErrorValidation[key]) cont = true;
            }

            if(cont){
                e.preventDefault();
            }
        },
    },
});


// add employment
new Vue({
    el: '#sciEmployment',

    data: {
        newEmployment: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },
        haserror: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },
        Error: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },

        ErrorValidation: {
            company: '',
            position: '',
            datefrom: '',
            dateto: '',
            country: ''
        },

        addEmpCount: 0,
        sci_id: '',
        baseUrl: '',
        nofile: false,
    },
    ready: function(){
        this.haserror.company = '';
        this.Error.company = '';

        this.haserror.position = '';
        this.Error.position = '';

        this.haserror.datefrom = '';
        this.Error.datefrom = '';

        this.haserror.dateto = '';
        this.Error.dateto = '';

        this.haserror.country = '';
        this.Error.country = '';

        this.fetchEmpCount();
    },
    computed: {
        //Company
        validCompany: function() {
            if(this.newEmployment.company == ""){
                this.haserror.company = 'has-error'; this.ErrorValidation.company = true;
                this.Error.company = 'This field is required.';
                return true;
            }
            this.ErrorValidation.company = false;
            this.Error.company = ''; this.haserror.company = ''; return false;
        },

        //Position
        validPosition: function() {
            if(this.newEmployment.position == ""){
                this.haserror.position = 'has-error'; this.ErrorValidation.position = true;
                this.Error.position = 'This field is required.';
                return true;
            }
            this.ErrorValidation.position = false;
            this.Error.position = ''; this.haserror.position = ''; return false;
        },

        //dateFrom
        validDateFrom: function() {
            this.newEmployment.datefrom = $('#txtDateFrom').val();
            console.log(this.newEmployment.datefrom);
            var matches = this.newEmployment.datefrom.match(/^\d{4}-\d{2}-\d{2}$/g);
            if(this.newEmployment.datefrom == ""){
                // this.haserror.datefrom = 'has-error'; this.allerrors = true;
                // this.Error.datefrom = 'This field is required.';
                // return true;
            }else{
                if(matches == null || this.newEmployment.datefrom == "0000-00-00"){
                    this.haserror.datefrom = 'has-error'; this.allerrors = true;
                    this.Error.datefrom = 'Invalid date.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.datefrom = ''; return false;
        },

        //dateTo
        validDateTo: function() {
            this.newEmployment.dateto = $('#txtDateTo').val();
            var matches = this.newEmployment.dateto.match(/^\d{4}-\d{2}-\d{2}$/g);
            if(this.newEmployment.dateto == ""){
                // this.haserror.dateto = 'has-error'; this.allerrors = true;
                // this.Error.dateto = 'This field is required.';
                // return true;
            }else{
                if(matches == null || this.newEmployment.dateto == "0000-00-00"){
                    this.haserror.dateto = 'has-error'; this.allerrors = true;
                    this.Error.dateto = 'Invalid date.';
                    return true;
                }
            }
            this.allerrors = false; this.haserror.dateto = ''; return false;
        },
        //country
        validCountry: function() {
            if(this.newEmployment.country == ""){
                this.haserror.country = 'has-error'; this.ErrorValidation.country = true;
                this.Error.country = 'This field is required.';
                return true;
            }
            this.ErrorValidation.country = false;
            this.Error.country = ''; this.haserror.country = ''; return false;
        },
    },
    methods: {
        fetchEmpCount: function(){
            this.$http.get(this.baseUrl + 'scientists/fetchEmpCount/' + this.sci_id, function(addEmpCount) {
                this.$set('addEmpCount', addEmpCount);
            });
        },

        getvalidate: function(e) {
            if(this.newEmployment.company == ""){
                this.haserror.company = 'has-error'; this.Error.company = "This field is required.";
                this.ErrorValidation.company = true
            }

            if(this.newEmployment.position == ""){
                this.haserror.position = 'has-error'; this.Error.position = "This field is required.";
                this.ErrorValidation.position = true
            }

            // if($('#txtDateFrom').val() == ""){
            //     this.haserror.datefrom = 'has-error'; this.Error.datefrom = "This field is required.";
            //     this.ErrorValidation.datefrom = true
            // }

            // if($('#txtDateTo').val() == ""){
            //     this.haserror.dateto = 'has-error'; this.Error.dateto = "This field is required.";
            //     this.ErrorValidation.dateto = true
            // }

            if(this.newEmployment.country == ""){
                this.haserror.country = 'has-error'; this.Error.country = "This field is required.";
                this.ErrorValidation.country = true
            }

            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }
            if(cont){
                e.preventDefault();
            }
        },
    },
});