new Vue({
    el: '#userAccount',

    data: {
        fname:'', mname:'', lname:'', username: '', password: '', accesslevel: '', council: '',
        allerrors: false,
        Error: {
            fname:'', mname:'', lname:'', username: '', password: '', accesslevel: '', council: '',
        },
        ErrorValidation: {
            fname:false, mname:false, lname:false, username: false, password: false, accesslevel: false, council: false,
        },
        haserror: {
            fname:'', mname:'', lname:'', username: '', password: '', accesslevel: '', council: '',
        },
        baseUrl: '',
        users:  [],
        userid: '',
        submmited: false,
    },

    ready: function(){
        // fetch user
        this.fetchUsers();
    },
    watch: {
        //Employee name
        'fname': function() {
            if(this.fname == ""){
                this.haserror.fname = 'has-error';
                this.ErrorValidation.fname = true;
                this.Error.fname = 'This field is required.';
            }else if(this.fname == " " || (!this.fname.replace(/\s/g, '').length)){
                this.haserror.fname = 'has-error';
                this.ErrorValidation.fname = true;
                this.Error.fname = 'Invalid input.';
            }else{
                var matches = this.fname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
                if(matches != null){
                    this.haserror.fname = 'has-error';
                    this.ErrorValidation.fname = true;
                    this.Error.fname = 'Please use only alphabet characters and spaces for Employee Name.';
                }else{
                    this.haserror.fname = '';
                    this.ErrorValidation.fname = false;
                    this.Error.fname = '';
                }
            }
        },
        
        'mname': function() {
            if(this.mname == ""){
                this.haserror.mname = 'has-error';
                this.ErrorValidation.mname = true;
                this.Error.mname = 'This field is required.';
            }else if(this.mname == " " || (!this.mname.replace(/\s/g, '').length)){
                this.haserror.mname = 'has-error';
                this.ErrorValidation.mname = true;
                this.Error.mname = 'Invalid input.';
            }else{
                var matches = this.mname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
                if(matches != null){
                    this.haserror.mname = 'has-error';
                    this.ErrorValidation.mname = true;
                    this.Error.mname = 'Please use only alphabet characters and spaces for Employee Name.';
                }else{
                    this.haserror.mname = '';
                    this.ErrorValidation.mname = false;
                    this.Error.mname = '';
                }
            }
        },

        'lname': function() {
            if(this.lname == ""){
                this.haserror.lname = 'has-error';
                this.ErrorValidation.lname = true;
                this.Error.lname = 'This field is required.';
            }else if(this.lname == " " || (!this.lname.replace(/\s/g, '').length)){
                this.haserror.lname = 'has-error';
                this.ErrorValidation.lname = true;
                this.Error.lname = 'Invalid input.';
            }else{
                var matches = this.lname.match(/[0-9!@#\$%\^\&*\)\(+=._]+$/g);
                if(matches != null){
                    this.haserror.lname = 'has-error';
                    this.ErrorValidation.lname = true;
                    this.Error.lname = 'Please use only alphabet characters and spaces for Employee Name.';
                }else{
                    this.haserror.lname = '';
                    this.ErrorValidation.lname = false;
                    this.Error.lname = '';
                }
            }
        },

        //username name
        'username': function(val) {
            if(val == ""){
                this.haserror.username = 'has-error';
                this.ErrorValidation.username = true;
                this.Error.username = 'This field is required.';
            }else{
                if(val == " " || (!val.replace(/\s/g, '').length)){
                    this.haserror.username = 'has-error';
                    this.ErrorValidation.username = true;
                    this.Error.username = 'Invalid input.';
                }else if(this.users.indexOf(val) >= 0){
                    this.haserror.username = 'has-error';
                    this.ErrorValidation.username = true;
                    this.Error.username = 'Username already exist.';
                }else{
                    this.haserror.username = '';
                    this.ErrorValidation.username = false;
                    this.Error.username = '';
                }
            }
        },

        //Password
        'password': function(val) {
            if(val == ""){
                this.haserror.password = 'has-error';
                this.ErrorValidation.password = true;
                this.Error.password = 'This field is required.';
            }else{
                if(val == " " || (!val.replace(/\s/g, '').length)){
                    this.haserror.password = 'has-error';
                    this.ErrorValidation.password = true;
                    this.Error.password = 'Invalid input.';
                }else{
                    this.haserror.password = '';
                    this.ErrorValidation.password = false;
                    this.Error.password = '';
                }
            }
        },

        //user access level
        'accesslevel': function(val) {
            if(val == ""){
                this.haserror.accesslevel = 'has-error';
                this.ErrorValidation.accesslevel = true;
                this.Error.accesslevel = 'This field is required.';
            }else{
                this.haserror.accesslevel = '';
                this.ErrorValidation.accesslevel = false;
                this.Error.accesslevel = '';
            }
        },

        //Council
        'council': function(val){
            if(val == ""){
                this.haserror.council = 'has-error';
                this.ErrorValidation.council = true;
                this.Error.council = 'This field is required.';
            }else{
                this.haserror.council = '';
                this.ErrorValidation.council = false;
                this.Error.council = '';
            }
        },
    },

    methods: {
        //fetchuser
        fetchUsers: function() {
            this.$http.get(this.baseUrl + 'users/fetchuser/' + this.userid, function(users) {
                this.$set('users', users);
            });
        },

        getValidate: function(e) {

            // // check Employeename
            if(this.fname == ""){
                this.haserror.fname = 'has-error';
                this.Error.fname    = 'This field is required.';
                this.ErrorValidation.fname = true;
            }
            
            if(this.mname == ""){
                this.haserror.mname = 'has-error';
                this.Error.mname    = 'This field is required.';
                this.ErrorValidation.mname = true;
            }

            if(this.lname == ""){
                this.haserror.lname = 'has-error';
                this.Error.lname    = 'This field is required.';
                this.ErrorValidation.lname = true;
            }

            // check username
            if(this.username == ""){
                this.haserror.username = 'has-error';
                this.Error.username    = 'This field is required.';
                this.ErrorValidation.username = true;
            }

            // //check password
            if(this.password == ""){
                this.haserror.password = 'has-error';
                this.Error.password    = 'This field is required.';
                this.ErrorValidation.password = true;
            }

            //check accesslevel
            if(this.accesslevel == ""){
                this.haserror.accesslevel = 'has-error';
                this.Error.accesslevel    = 'This field is required.';
                this.ErrorValidation.accesslevel = true;
            }

            //monitoring council
            if(this.accesslevel==3){
                if(this.council == ""){
                    this.haserror.council = 'has-error';
                    this.Error.council    = 'This field is required.';
                    this.ErrorValidation.council = true;
                }
            }
            
            var cont = false;
            for (var key in this.ErrorValidation) {
                if(this.ErrorValidation[key]) cont = true;
            }
            if(cont){
                e.preventDefault();
            }
        }
    }
});